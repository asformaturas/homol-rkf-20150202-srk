<?php

class CancelarExtraShell extends Shell {
    
    var $uses = array(
        'Despesa',
        'CampanhasUsuario',
        'ProtocoloCheckoutFormando'
    );

    function main(){
        ini_set('memory_limit', '1024M');
        set_time_limit(0);
        $this->cancelarCampanhasNaoPagas();
    }
    
    function cancelarCampanhasNaoPagas(){
        $campanhasUsuarios = $this->Despesa->query("SELECT campanhas_usuario_id, usuario_id FROM despesas WHERE campanhas_usuario_id IN 
            (SELECT Despesa.campanhas_usuario_id FROM `despesas` AS `Despesa`
            WHERE `Despesa`.`tipo` = 'extra' AND `Despesa`.`status` = 'aberta' AND
            Despesa.parcela = 1 and `Despesa`.`data_pagamento` IS NULL AND
            `Despesa`.`data_vencimento` < NOW() - INTERVAL 11 DAY)
            AND status != 'paga' GROUP BY campanhas_usuario_id");
        $campanhasCancelar = array();
        foreach($campanhasUsuarios as $campanhaUsuario){
            if(!$this->ProtocoloCheckoutFormando->find('first', array(
                'conditions' => array(
                    'Protocolo.usuario_id' => $campanhaUsuario['despesas']['usuario_id'],
                    'Protocolo.tipo' => 'checkout')
                )))
                    $campanhasCancelar[] = $campanhaUsuario['despesas']['campanhas_usuario_id'];
        }
        foreach($campanhasCancelar as $campanhaCancelar){
            $this->Despesa->updateAll(
                array('Despesa.status' => "'cancelada'"),
                array('Despesa.campanhas_usuario_id' => $campanhaCancelar)
            );
            $this->CampanhasUsuario->updateAll(
                array('CampanhasUsuario.cancelada' => 1),
                array('CampanhasUsuario.id' => $campanhaCancelar)
            );
        }
    }
}
?>
