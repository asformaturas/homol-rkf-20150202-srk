<?php
App::import('Component', 'WpGerenciador');

class TurmasHotsitesMigracaoShell extends Shell {

	var $uses = array( 'Turma', 'TurmaSiteFoto', 'TurmaPagina', 'TurmaPaginaFoto');
	var $wpGerenciador;
	var $logFileName = 'log.txt';
	//var $baseArquivos = 'http://sistema.asformaturas.com.br/';
	var $baseArquivos;

	function main()
	{
		$this->baseArquivos = APP.'webroot/';
		//die($this->baseArquivos);

		$this->WpGerenciador = new WpGerenciadorComponent();
		$this->log('>>>> Script Iniciado');
		$result = $this->criarSites();
		$this->log('<<<< Script Finalizado');
	}

	function criarSites()
	{
		$sites = $this->Turma->find('all',
			array(
				'fields'     => array('Turma.id','Turma.site','Turma.nome','Turma.logo_site','Turma.facebook'),
				'order'      => 'Turma.id',
				'recursive'  => 0,
				'conditions' => array('Turma.site IS NOT NULL')
			)
		);

		foreach ($sites as $key => $turma) {

			$turma = (object) $turma['Turma'];
			$this->log(' >>>> Iniciado Site de Turma: '.$turma->site.' - '.$turma->nome);
			
			$this->WpGerenciador->criarSite($turma->site,$turma->id, $turma->nome);

			//caso existe facebok
			if($turma->facebook){
				//grava a fanpage
				$this->WpGerenciador->atualizaFacebookFanPage($turma->site, $turma->facebook);
			}

			//turma recebe o objeto
			//caso existe logo
			if($turma->logo_site) {
				$imagem = $this->getImagem($turma->logo_site);

				if($this->validarImagem($imagem,$turma->logo_site,$turma->nome.' | '.'Logo Site'))
					$this->WpGerenciador->atualizaLogo($turma->site,$imagem);
			}

			//faz a coleta de banners
			$homeImagens = $this->TurmaSiteFoto->find('all',
				array(
					'fields'     => array('arquivo','tipo', 'descricao'),
					'recursive'  => 0,
					'order'      => 'TurmaSiteFoto.data_cadastro ASC',
					'conditions' => array('TurmaSiteFoto.turma_id' => $turma->id)
				)
			);

			//Grava os banners
			foreach ($homeImagens as $key => $value) {
				$value = (object) $value['TurmaSiteFoto'];
				$imagem = $this->getImagem($value->arquivo);

				if($this->validarImagem($imagem,$value->arquivo,$turma->nome.' | '.'Banner'))
					$this->WpGerenciador->adicionaFotoHome($turma->site, $imagem, $value->tipo, $value->descricao);
			}

			//Faz a coleta das paginas
			$paginas = $this->TurmaPagina->find('all',
				array(
					'fields'     => array('menu', 'titulo'),
					'order'      => 'TurmaPagina.data_cadastro ASC',
					'conditions' => array('TurmaPagina.turma_id' => $turma->id)
				)
			);
			
			foreach ($paginas as $key => $value) {
				
				$pagina = (object) $value['TurmaPagina'];
				$fotos  = $value['TurmaPaginaFoto'];

				var_dump($turma->site, $pagina->menu, $pagina->titulo, $pagina->id);

				$this->WpGerenciador->criarCategoria($turma->site, $pagina->menu, $pagina->titulo, $pagina->id);

				foreach ($fotos as $key => $foto) {
					$foto = (object) $foto;
					$imagem = $this->getImagem($foto->arquivo);

					if($this->validarImagem($imagem, $foto->arquivo, $turma->nome.' | '.'Categoria'))
						$this->WpGerenciador->adicionaFotoPagina($turma->site, $imagem, $foto->descricao, $foto->turma_pagina_id);
				}
			}

			$this->log('<<<< Finalizado Site de Turma: '.$turma->site.' - '.$turma->nome);
		}
	}

	function getImagem($caminho)
	{
		$caminho = $this->baseArquivos. $caminho;
		@$file = file_get_contents($caminho);
		if ($file)
			return $imagem = base64_encode($file);
		else{
			return false;
		}
	}

	function validarImagem($imagem,$caminho,$contexto)
	{
		if(!$imagem){
			$this->log('ERRO IMAGEM - '.$contexto.' - '.$this->baseArquivos.$caminho);
			return false;
		}
		return true;
	}

	function log($message_error)
	{
		$log = '';
		$message = '';

		if(file_exists($this->logFileName))
			$log = file_get_contents('log.txt');

		$message .= $log ? "\n" : '';
		$message .= date('Y-m-d H:i:s').' - '.$message_error;

		$log .= $message;

		echo $message;
		file_put_contents($this->logFileName, $log);
	}
}
