<?php

class AvisoTelaoShell extends Shell {
    
    var $uses = array(
        'FormandoFotoTelao',
        'Usuario',
        'TurmasUsuarios',
        'Turma',
        'ViewFormandos',
        'InformativoTelaoUsuarios',
        'Evento'
    );
     
    function initialize(){
        parent::initialize();
        App::import('Component', 'mail');
        $this->Mailer = new Mail();
        $path = APP . "/webroot/img/logo_rk.jpg";
        $this->Mailer->AddEmbeddedImage($path, 'logo');
    }

    function main(){
        ini_set('memory_limit', '1024M');
        set_time_limit(0);
        $turmas = $this->buscarTurmasEventoProximo();
        foreach($turmas as $turma){
            $strToTime = strtotime(substr($turma['Evento']['data'],0,10));
            if(date("Y-m-d", strtotime("-30 days", $strToTime)) == date("Y-m-d")){
                $formandos = $this->buscarFormandosTurma($turma);
                $formandosSemFotos = $this->selecionarFormandosSemFoto($formandos);
                $mensagem = "<h2><b>Prezado(a) Formando(a),</b></h2>";
                $mensagem.= "<h3>Está chegando a sua Festa de Formatura. Precisamos que nos envie as suas fotos do telão "
                        . "o mais breve possível.</h3><br /><br />";
                $mensagem.= "<img src='cid:logo'/>";
                $this->enviarNotificacaoParaFormandos($formandosSemFotos, $diasParaEvento = false, $dataLimite = false, $mensagem);
            }
            if(date("Y-m-d", strtotime("-25 days", $strToTime)) == date("Y-m-d")){
                $formandos = $this->buscarFormandosTurma($turma);
                $formandosSemFotos = $this->selecionarFormandosSemFoto($formandos);
                $diasParaEvento = 25;
                $dataLimite = 10;
                $this->enviarNotificacaoParaFormandos($formandosSemFotos, $diasParaEvento, $dataLimite, $mensagem = false);
            }
            if(date("Y-m-d", strtotime("-20 days", $strToTime)) == date("Y-m-d")){
                $formandos = $this->buscarFormandosTurma($turma);
                $formandosSemFotos = $this->selecionarFormandosSemFoto($formandos);
                $diasParaEvento = 20;
                $dataLimite = 5;
                $this->enviarNotificacaoParaFormandos($formandosSemFotos, $diasParaEvento, $dataLimite, $mensagem = false);
            }
            if(date("Y-m-d", strtotime("-19 days", $strToTime)) == date("Y-m-d")){
                $formandos = $this->buscarFormandosTurma($turma);
                $formandosSemFotos = $this->selecionarFormandosSemFoto($formandos);
                $diasParaEvento = 19;
                $dataLimite = 4;
                $this->enviarNotificacaoParaFormandos($formandosSemFotos, $diasParaEvento, $dataLimite, $mensagem = false);
            }
            if(date("Y-m-d", strtotime("-18 days", $strToTime)) == date("Y-m-d")){
                $formandos = $this->buscarFormandosTurma($turma);
                $formandosSemFotos = $this->selecionarFormandosSemFoto($formandos);
                $diasParaEvento = 18;
                $dataLimite = 3;
                $this->enviarNotificacaoParaFormandos($formandosSemFotos, $diasParaEvento, $dataLimite, $mensagem = false);
            }
            if(date("Y-m-d", strtotime("-17 days", $strToTime)) == date("Y-m-d")){
                $formandos = $this->buscarFormandosTurma($turma);
                $formandosSemFotos = $this->selecionarFormandosSemFoto($formandos);
                $diasParaEvento = 17;
                $dataLimite = 2;
                $this->enviarNotificacaoFuncionarios($turmaId = $turma['Evento']['turma_id'], $grupo = 'planejamento', $diasParaEvento, $dataLimite);
                $this->enviarNotificacaoParaFormandos($formandosSemFotos, $diasParaEvento, $dataLimite, $mensagem = false);
            } 
            if(date("Y-m-d", strtotime("-16 days", $strToTime)) == date("Y-m-d")){
                $formandos = $this->buscarFormandosTurma($turma);
                $formandosSemFotos = $this->selecionarFormandosSemFoto($formandos);
                $mensagem = "<h2><b>Prezado(a) Formando(a),</b></h2>";
                $mensagem.= "<h3>Faltam 16 dias para sua Festa de Formatura. ";
                $mensagem.= "Hoje é o <b style='color: red'>ÚLTIMO DIA</b> para enviar suas fotos de telão.</h3><br /><br />";
                $mensagem.= "<img src='cid:logo'/>";
                $this->enviarNotificacaoFuncionarios($turmaId = $turma['Evento']['turma_id'], $grupo = 'planejamento', $diasParaEvento = 16, $dataLimite = 1);
                $this->enviarNotificacaoParaFormandos($formandosSemFotos, $diasParaEvento = false, $dataLimite = false, $mensagem);
            }
            if(date("Y-m-d", strtotime("-15 days", $strToTime)) == date("Y-m-d")){
                $this->enviarNotificacaoFuncionarios($turmaId = $turma['Evento']['turma_id'], $grupo = 'foto');
            }
        }
    }
    
    function buscarTurmasEventoProximo(){
        $this->Evento->unBindModelAll();
        $dataFinal = date("Y-m-d", strtotime("+30 days"));
        $dataInicio = date("Y-m-d");
        $turmas = $this->Evento->find('all', array(
            'conditions' => array(
                'Evento.data BETWEEN ? AND ?' => array("{$dataInicio}", "{$dataFinal}"),
                'Evento.tipos_evento_id' => 1,
                'Evento.alerta_telao' => 1
            )
        ));
        return $turmas;
    }
    
    function buscarFormandosTurma($turma){
        $this->ViewFormandos->bindModel(array(
            'hasMany' => array(
                'FormandoFotoTelao' => array(
                    'className' => 'FormandoFotoTelao',
                    'joinTable' => 'formandos_fotos_telao',
                    'foreignKey' => 'usuario_id'
                )
            )
        ),false);
        $formandos = $this->ViewFormandos->find('all', array(
            'conditions' => array(
                'ViewFormandos.turma_id' => $turma['Evento']['turma_id']
            )
        ));
        return $formandos;
    }
    
    function selecionarFormandosSemFoto($formandos){
        $semFoto = array();
        foreach($formandos as $i => $formando){
            if(sizeof($formando['FormandoFotoTelao']) == 1){
                foreach($formando['FormandoFotoTelao'] as $fotos){
                    if($fotos['tipo'] == 'crianca'){
                        $semFoto[$i] = $formando['ViewFormandos'];
                        $semFoto[$i]['fotos'] = 'Adulto';
                    }elseif($fotos['tipo'] == 'adulto'){
                        $semFoto[$i] = $formando['ViewFormandos'];
                        $semFoto[$i]['fotos'] = 'Criança';
                    }
                }
            }elseif(sizeof($formando['FormandoFotoTelao']) == 0){
                $semFoto[$i] = $formando['ViewFormandos'];
                $semFoto[$i]['fotos'] = 'Criança e Adulto';
            }
        }
        return $semFoto;
    }
    
    function enviarNotificacaoParaFormandos($formandosSemFotos, $diasParaEvento, $dataLimite, $msg){
        foreach($formandosSemFotos as $formandoSemFoto){
            $this->Mailer->IsHTML(true);
            $this->Mailer->FromName = utf8_decode("Sistema RK Formaturas");
            $this->Mailer->Subject = utf8_decode("ALERTA! Fotos do Telão...");
            if(empty($msg)){
                $mensagem = NULL;
                $mensagem = "<h2><b>Prezado(a) Formando(a),</b></h2>";
                $mensagem.= "<h3>Faltam <b style='color: red'>{$diasParaEvento}</b> dias para sua Festa de Formatura.<br />";
                $mensagem.= "Você tem até <b style='color: red'>{$dataLimite}</b> dias para enviar suas fotos de telão.</h3>";
                $mensagem.= "Foto(s) para enviar: <br />";
                $mensagem.= "- {$formandoSemFoto['fotos']}<br /><br />";
                $mensagem.= "<img src='cid:logo'/>";
                $this->Mailer->AddAddress("{$formandoSemFoto['email']}","{$formandoSemFoto['nome']}");
                $this->Mailer->Body = utf8_decode($mensagem);
            }else{
                $this->Mailer->AddAddress("{$formandoSemFoto['email']}","{$formandoSemFoto['nome']}");
                $this->Mailer->Body = utf8_decode($msg);
            }
            if($this->Mailer->Send()){
                $salvar = array(
                    'usuario_id' => $formandoSemFoto['id'],
                    'turma_id' => $formandoSemFoto['turma_id'],
                    'data_cadastro' => date("Y-m-d H:i:s")
                );
                $this->InformativoTelaoUsuarios->create();
                $this->InformativoTelaoUsuarios->save($salvar);
                $this->Mailer->ClearAddresses();
            }
        }
    }
    
    function enviarNotificacaoFuncionarios($turmaId = false, $grupo = false, $diasParaEvento = false, $dataLimite = false){
        $this->Mailer->IsHTML(true);
        if($grupo == 'planejamento'){
            $this->Mailer->FromName = utf8_decode("Sistema RK Formaturas");
            $this->Mailer->Subject = utf8_decode("ALERTA! Fotos do Telão turma {$turmaId}...");
            $this->Usuario->unbindModelAll();
            $atendentes = $this->Usuario->query("
                    SELECT `Usuario`.* FROM `usuarios` AS `Usuario` inner JOIN turmas_usuarios AS `TurmasUsuario` 
                    ON (`TurmasUsuario`.`usuario_id` = `Usuario`.`id`)  WHERE `TurmasUsuario`.`turma_id` = {$turmaId} AND `Usuario`.`grupo` 
                    IN ('atendimento', 'planejamento', 'comercial') AND `Usuario`.`ativo` = 1
                    ");
            foreach($atendentes as $atendente){
                if($atendente['Usuario']['grupo'] == 'foto'){
                    $mensagem = NULL;
                    $mensagem = "<h2><b>Prezado(a) {$atendente['Usuario']['nome']},</b></h2>";
                    $mensagem.= "<h3>Faltam <b style='color: red'>{$diasParaEvento}</b> dias para a Festa de Formatura da turma {$turmaId}.<br />";
                    $mensagem.= "Os formandos que não realizaram o upload de suas fotos tem até ";
                    $mensagem.= "<b style='color: red'>{$dataLimite}</b> dias para enviar suas fotos de telão.</h3>";
                    $mensagem.= "<img src='cid:logo'/>";
                    $this->Mailer->AddAddress("{$atendente['Usuario']['email']}","{$atendente['Usuario']['nome']}");
                    $this->Mailer->Body = utf8_decode($mensagem);
                    $this->Mailer->Send();
                    $this->Mailer->ClearAddresses();
                }
            }
        }elseif($grupo == 'video'){
            $this->Mailer->FromName = utf8_decode("Sistema RK Formaturas");
            $this->Mailer->Subject = utf8_decode("ALERTA! Upload de Fotos do Telão encerrado para a turma {$turmaId}...");
            $mensagem = "<h3>O prazo para envio de fotos terminou para a turma {$turmaId}.</h3><br />";
            $mensagem.= "Estará disponível para download em seu sistema. <br /><br />";
            $mensagem.= "<img src='cid:logo'/>";
            $this->Mailer->AddAddress("telao@rkformaturas.com.br","Foto Telão");
            $this->Mailer->Body = utf8_decode($mensagem);
            $this->Mailer->Send();
            $this->Mailer->ClearAddresses();
        }
    }
}
?>
