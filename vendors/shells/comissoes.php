<?php 

class AvisoFotoTelaoShell extends Shell {
    
    var $uses = array(
        'FormandoFotoTelao',
        'Usuario',
        'TurmasUsuarios',
        'Turma',
        'ViewFormandos',
        'InformativoTelaoUsuarios',
        'Evento'
    );
     
    function initialize(){
        parent::initialize();
        App::import('Component', 'mail');
        $this->Mailer = new Mail();
        $path = APP . "/webroot/img/frase.png";
        $this->Mailer->AddEmbeddedImage($path, 'logo');
    }

    function main(){
        ini_set('memory_limit', '1024M');
        set_time_limit(0);
        $turmas = $this->buscarTurmasEventoProximo();
        $data = new DateTime('last day of this month');
        $dia = $data->format('d');
        foreach($turmas as $turma){
            $strToTime = strtotime(substr($turma['Evento']['data'],0,10));
            if($dia == 31)
                $diaCerto = $dia;
            else
                $diaCerto = 30;
            if(date("Y-m-d", strtotime("-{$diaCerto} days", $strToTime)) == date("Y-m-d")){
                $formandos = $this->buscarFormandosTurma($turma);
                $formandosSemFotos = $this->selecionarFormandosSemFoto($formandos);
                $mensagem = "<h2><b>Prezado(a) Formando(a),</b></h2>";
                $mensagem.= "<h3>Está chegando a sua Festa de Formatura. Precisamos que nos envie as suas fotos do telão "
                        . "o mais breve possível.</h3><br /><br />";
                $mensagem.= "<img src='cid:logo'/>";
                $this->enviarNotificacaoParaFormandos($formandosSemFotos, $diasParaEvento = false, $dataLimite = false, $mensagem);
            }
            if($dia == 31)
                $diaCerto = $dia - 6;
            else
                $diaCerto = $dia - 5;
            if(date("Y-m-d", strtotime("-{$diaCerto} days", $strToTime)) == date("Y-m-d")){

                $formandos = $this->buscarFormandosTurma($turma);
                $formandosSemFotos = $this->selecionarFormandosSemFoto($formandos);
                $diasParaEvento = $diaCerto;
                $dataLimite = 10;
                $this->enviarNotificacaoParaFormandos($formandosSemFotos, $diasParaEvento, $dataLimite, $mensagem = false);
            }
            if($dia == 31)
                $diaCerto = $dia - 11;
            else
                $diaCerto = $dia - 10;
            if(date("Y-m-d", strtotime("-{$diaCerto} days", $strToTime)) == date("Y-m-d")){
                $formandos = $this->buscarFormandosTurma($turma);
                $formandosSemFotos = $this->selecionarFormandosSemFoto($formandos);
                $diasParaEvento = $diaCerto;
                $dataLimite = 5;
                $this->enviarNotificacaoParaFormandos($formandosSemFotos, $diasParaEvento, $dataLimite, $mensagem = false);
            }
            if($dia == 31)
                $diaCerto = $dia - 12;
            else
                $diaCerto = $dia - 11;
            if(date("Y-m-d", strtotime("-{$diaCerto} days", $strToTime)) == date("Y-m-d")){
                $formandos = $this->buscarFormandosTurma($turma);
                $formandosSemFotos = $this->selecionarFormandosSemFoto($formandos);
                $diasParaEvento = $diaCerto;
                $dataLimite = 4;
                $this->enviarNotificacaoParaFormandos($formandosSemFotos, $diasParaEvento, $dataLimite, $mensagem = false);
            }
            if($dia == 31)
                $diaCerto = $dia - 13;
            else
                $diaCerto = $dia - 12;
            if(date("Y-m-d", strtotime("-{$diaCerto} days", $strToTime)) == date("Y-m-d")){
                $formandos = $this->buscarFormandosTurma($turma);
                $formandosSemFotos = $this->selecionarFormandosSemFoto($formandos);
                $diasParaEvento = $diaCerto;
                $dataLimite = 3;
                $this->enviarNotificacaoParaFormandos($formandosSemFotos, $diasParaEvento, $dataLimite, $mensagem = false);
            }
            if($dia == 31)
                $diaCerto = $dia - 14;
            else
                $diaCerto = $dia - 13;
            if(date("Y-m-d", strtotime("-{$diaCerto} days", $strToTime)) == date("Y-m-d")){
                $formandos = $this->buscarFormandosTurma($turma);
                $formandosSemFotos = $this->selecionarFormandosSemFoto($formandos);
                $diasParaEvento = $diaCerto;
                $dataLimite = 2;
                $this->enviarNotificacaoFuncionarios($turmaId = $turma['Evento']['turma_id'], $grupo = 'planejamento', $diasParaEvento, $dataLimite);
                $this->enviarNotificacaoParaFormandos($formandosSemFotos, $diasParaEvento, $dataLimite, $mensagem = false);
            } 
            if($dia == 31)
                $diaCerto = $dia - 15;
            else
                $diaCerto = $dia - 14;
            if(date("Y-m-d", strtotime("-{$diaCerto} days", $strToTime)) == date("Y-m-d")){
                $formandos = $this->buscarFormandosTurma($turma);
                $formandosSemFotos = $this->selecionarFormandosSemFoto($formandos);
                $mensagem = "<h2><b>Prezado(a) Formando(a),</b></h2>";
                $mensagem.= "<h3>Faltam 16 dias para sua Festa de Formatura. ";
                $mensagem.= "Hoje é o <b style='color: red'>ÚLTIMO DIA</b> para enviar suas fotos de telão.</h3><br /><br />";
                $mensagem.= "<img src='cid:logo'/>";
                $this->enviarNotificacaoFuncionarios($turmaId = $turma['Evento']['turma_id'], $grupo = 'planejamento', $diasParaEvento = 16, $dataLimite = 1);
                $this->enviarNotificacaoParaFormandos($formandosSemFotos, $diasParaEvento = false, $dataLimite = false, $mensagem);
            }
            if($dia == 31)
                $diaCerto = $dia - 16;
            else
                $diaCerto = $dia - 15;
            if(date("Y-m-d", strtotime("-{$diaCerto} days", $strToTime)) == date("Y-m-d")){
                $this->enviarNotificacaoFuncionarios($turmaId = $turma['Evento']['turma_id'], $grupo = 'video');
            }
        }
    }
    
    function buscarTurmasEventoProximo(){
        $this->Evento->unBindModelAll();
        $data = new DateTime('last day of this month');
        if((int)$data->format('d') == 30)
            $dia = $data->format('d') + 1;
        elseif((int)$data->format('d') == 28)
            $dia = $data->format('d') + 3;
        else
            $dia = $data->format('d');
        $dataFinal = date("Y-m-d", strtotime("+{$dia} days"));
        $dataInicio = date("Y-m-d");
        $turmas = $this->Evento->find('all', array(
            'conditions' => array(
                'Evento.data BETWEEN ? AND ?' => array("{$dataInicio}", "{$dataFinal}"),
                'Evento.tipos_evento_id' => 1,
                'Evento.alerta_telao' => 1
            )
        ));
        return $turmas;
    }
    
    function buscarFormandosTurma($turma){
        $this->ViewFormandos->bindModel(array(
            'hasMany' => array(
                'FormandoFotoTelao' => array(
                    'className' => 'FormandoFotoTelao',
                    'joinTable' => 'formandos_fotos_telao',
                    'foreignKey' => 'usuario_id'
                )
            )
        ),false);
        $formandos = $this->ViewFormandos->find('all', array(
            'conditions' => array(
                'ViewFormandos.turma_id' => $turma['Evento']['turma_id']
            )
        ));
        return $formandos;
    }
    
    function selecionarFormandosSemFoto($formandos){
        $semFoto = array();
        foreach($formandos as $i => $formando){
            if(sizeof($formando['FormandoFotoTelao']) == 1){
                foreach($formando['FormandoFotoTelao'] as $fotos){
                    if($fotos['tipo'] == 'crianca'){
                        $semFoto[$i] = $formando['ViewFormandos'];
                        $semFoto[$i]['fotos'] = 'Adulto';
                    }elseif($fotos['tipo'] == 'adulto'){
                        $semFoto[$i] = $formando['ViewFormandos'];
                        $semFoto[$i]['fotos'] = 'Criança';
                    }
                }
            }elseif(sizeof($formando['FormandoFotoTelao']) == 0){
                $semFoto[$i] = $formando['ViewFormandos'];
                $semFoto[$i]['fotos'] = 'Criança e Adulto';
            }
        }
        return $semFoto;
    }
    
    function enviarNotificacaoParaFormandos($formandosSemFotos, $diasParaEvento, $dataLimite, $msg){
        foreach($formandosSemFotos as $formandoSemFoto){
            $this->Mailer->IsHTML(true);
            $this->Mailer->FromName = utf8_decode("Sistema RK Formaturas");
            $this->Mailer->Subject = utf8_decode("ALERTA! Fotos do Telão...");
            if(empty($msg)){
                $mensagem = NULL;
                $mensagem = "<h2><b>Prezado(a) Formando(a),</b></h2>";
                $mensagem.= "<h3>Faltam <b style='color: red'>{$diasParaEvento}</b> dias para sua Festa de Formatura.<br />";
                $mensagem.= "Você tem até <b style='color: red'>{$dataLimite}</b> dias para enviar suas fotos de telão.</h3>";
                $mensagem.= "Foto(s) para enviar: <br />";
                $mensagem.= "- {$formandoSemFoto['fotos']}<br /><br />";
                $mensagem.= "<img src='cid:logo'/>";
                $this->Mailer->AddAddress("{$formandoSemFoto['email']}","{$formandoSemFoto['nome']}");
                $this->Mailer->Body = utf8_decode($mensagem);
            }else{
                $this->Mailer->AddAddress("{$formandoSemFoto['email']}","{$formandoSemFoto['nome']}");
                $this->Mailer->Body = utf8_decode($msg);
            }
            if($this->Mailer->Send()){
                $salvar = array(
                    'usuario_id' => $formandoSemFoto['id'],
                    'turma_id' => $formandoSemFoto['turma_id'],
                    'data_cadastro' => date("Y-m-d H:i:s")
                );
                $this->InformativoTelaoUsuarios->create();
                $this->InformativoTelaoUsuarios->save($salvar);
                $this->Mailer->ClearAddresses();
            }
        }
    }
    
    function enviarNotificacaoFuncionarios($turmaId = false, $grupo = false, $diasParaEvento = false, $dataLimite = false){
        $this->Mailer->IsHTML(true);
        if($grupo == 'planejamento'){
            $this->Mailer->FromName = utf8_decode("Sistema RK Formaturas");
            $this->Mailer->Subject = utf8_decode("ALERTA! Fotos do Telão turma {$turmaId}...");
            $this->Usuario->unbindModelAll();
            $atendentes = $this->Usuario->query("
                    SELECT `Usuario`.* FROM `usuarios` AS `Usuario` inner JOIN turmas_usuarios AS `TurmasUsuario` 
                    ON (`TurmasUsuario`.`usuario_id` = `Usuario`.`id`)  WHERE `TurmasUsuario`.`turma_id` = {$turmaId} AND `Usuario`.`grupo` 
                    IN ('atendimento', 'planejamento', 'comercial') AND `Usuario`.`ativo` = 1
                    ");
            foreach($atendentes as $atendente){
                if($atendente['Usuario']['grupo'] == 'planejamento'){
                    $mensagem = NULL;
                    $mensagem = "<h2><b>Prezado(a) {$atendente['Usuario']['nome']},</b></h2>";
                    $mensagem.= "<h3>Faltam <b style='color: red'>{$diasParaEvento}</b> dias para a Festa de Formatura da turma {$turmaId}.<br />";
                    $mensagem.= "Os formandos que não realizaram o upload de suas fotos tem até ";
                    $mensagem.= "<b style='color: red'>{$dataLimite}</b> dias para enviar suas fotos de telão.</h3>";
                    $mensagem.= "<img src='cid:logo'/>";
                    $this->Mailer->AddAddress("{$atendente['Usuario']['email']}","{$atendente['Usuario']['nome']}");
                    $this->Mailer->Body = utf8_decode($mensagem);
                    $this->Mailer->Send();
                    $this->Mailer->ClearAddresses();
                }
            }
        }elseif($grupo == 'video'){
            $this->Mailer->FromName = utf8_decode("Sistema RK Formaturas");
            $this->Mailer->Subject = utf8_decode("ALERTA! Upload de Fotos do Telão encerrado para a turma {$turmaId}...");
            $mensagem = "<h3>O prazo para envio de fotos terminou para a turma {$turmaId}.</h3><br />";
            $mensagem.= "Estará disponível para download em seu sistema. <br /><br />";
            $mensagem.= "<img src='cid:logo'/>";
            $this->Mailer->AddAddress("atendimento@rkformaturas.com.br","Foto Telão");
            $this->Mailer->Body = utf8_decode($mensagem);
            $this->Mailer->Send();
            $this->Mailer->ClearAddresses();
        }
    }
}

/* class ComissoesShell extends Shell {

    var $uses = array(
        'ComissaoVendedor',
        'ComissaoRelatorioConsolidado',
        'ReciboComissao',
        'ViewFormandos',
        'Despesa',
        'Turma',
        'ReciboComissaoUsuario'
    );
    
    function main(){
        ini_set('memory_limit', '1024M');
        set_time_limit(0);
        $resumo = array();
        $comissoes = $this->buscarComissoes();
        App::import('Component', 'Financeiro');
        $financeiro = new FinanceiroComponent();
        foreach($comissoes as $comissao){
            $formandos = $this->buscarFormandos($comissao['Turma']['id']);
            $resumo = array(
                "usuario_id" => $comissao['ComissaoVendedor']['usuario_id'],
                "turma_id" => $comissao['ComissaoVendedor']['turma_id'],
                "data" => date("Y-m-d"),
                "data_cadastro" => date("Y-m-d H:i:s"),
                "dados" => array(
                    "turma" => "{$comissao['Turma']['id']} - {$comissao['Turma']['nome']}",
                    "contratos" => count($formandos),
                    "adesoes" => 0,
                    "ativos" => 0,
                    "este_mes" => 0,
                    "forecast" => 0,
                    "recebidos" => 0,
                    "porcentagem" => $comissao['ComissaoVendedor']['porcentagem'],
                    "valor_recebidos" => 0,
                    "valor_proximo_ciclo" => 0,
                    "valor_forecast" => 0
                )
            );
            foreach($formandos as $formando){
                if($formando['ViewFormandos']['valor_adesao'] > 0){
                    $resumo['dados']['adesoes']++;
                    if($financeiro->obterValorPagoAdesao($formando['ViewFormandos']['id']) > 0){
                        $resumo['dados']['ativos']++;
                        if($this->ComissaoVendedor->verificarPaganteEsteMes($formando['ViewFormandos']['id'])){
                            if(!$this->ReciboComissaoUsuario->verificarComissaoPaga($comissao['ComissaoVendedor']['usuario_id'], $formando['ViewFormandos']['id'])){
                                $resumo['dados']['este_mes']++;
                                $resumo['dados']['valor_proximo_ciclo'] += $formando['ViewFormandos']['valor_adesao'] *
                                    $comissao['ComissaoVendedor']['porcentagem'] / 100;
                            } 
                        }
                        if($this->ReciboComissaoUsuario->verificarComissaoPaga($comissao['ComissaoVendedor']['usuario_id'], $formando['ViewFormandos']['id'])){
                            $resumo['dados']['valor_recebidos'] += $formando['ViewFormandos']['valor_adesao'] * $comissao['ComissaoVendedor']['porcentagem'] / 100;
                            $resumo['dados']['recebidos']++;
                        }
                    }else{
                        $resumo['dados']['valor_forecast'] += $formando['ViewFormandos']['valor_adesao'] * $comissao['ComissaoVendedor']['porcentagem'] / 100;
                        $resumo['dados']['forecast']++;
                    }
                }
            }
            if($resumo['dados']['adesoes'] != 0){
                $resumo['dados'] = json_encode($resumo['dados'], JSON_UNESCAPED_UNICODE);
                $relatorio = $this->ComissaoRelatorioConsolidado->find('first', array(
                   'conditions' => array(
                       'usuario_id' => $comissao['ComissaoVendedor']['usuario_id'],
                       'turma_id' => $comissao['ComissaoVendedor']['turma_id']
                   ) 
                ));
                if($relatorio){
                    $this->ComissaoRelatorioConsolidado->updateAll(array(
                        "ComissaoRelatorioConsolidado.usuario_id" => $comissao['ComissaoVendedor']['usuario_id'],
                        "ComissaoRelatorioConsolidado.turma_id" => $comissao['ComissaoVendedor']['turma_id'],
                        "ComissaoRelatorioConsolidado.data" => "'" . date("Y-m-d") . "'",
                        "ComissaoRelatorioConsolidado.data_cadastro" => "'" . date('Y-m-d H:i:s') . "'",
                        "ComissaoRelatorioConsolidado.dados" => "'" . $resumo['dados'] . "'"
                    ), array('ComissaoRelatorioConsolidado.id' => $relatorio['ComissaoRelatorioConsolidado']['id']));
                }else{
                    $this->ComissaoRelatorioConsolidado->create();
                    $this->ComissaoRelatorioConsolidado->save($resumo);
                }
            }
        }
    }
    
    function buscarComissoes(){
        return $this->ComissaoVendedor->find('all');
    }
    
    function buscarFormandos($turmaId){
        $formandos = $this->ViewFormandos->find('all', array(
            'conditions' => array(
                    'turma_id' => $turmaId
            )
        ));
        return $formandos;
    }
    
    function pagarComissao(){
        $comissoes = $this->ComissaoVendedor->find('list', array(
            'fields' => array(
                'turma_id', 'porcentagem' ,'usuario_id'
            )
        ));
        foreach($comissoes as $usuarioId => $turmas){
            if($usuarioId == 13){
                $recibo = $this->gerarReciboComissao($usuarioId, 0, 0);
                if(!$recibo)
                    continue;
                foreach($turmas as $turmaId => $porcentagem){
                    $formandos = $this->buscarFormandos($turmaId);
                    if(!$formandos)
                        continue;
                    foreach($formandos as $formando){
                        if($this->ComissaoVendedor->verificarPaganteEsteMes($formando['ViewFormandos']['id'])){
                            if(!$this->ReciboComissaoUsuario->verificarComissaoPaga($usuarioId, $formando['ViewFormandos']['id'])){
                                $valor = $formando['ViewFormandos']['valor_adesao'] * $porcentagem / 100;
                                if($valor > 0){
                                    $recibo['ReciboComissao']['total_formandos']++;
                                    $recibo['ReciboComissao']['valor'] += $valor ;
                                    $this->ReciboComissaoUsuario->create();
                                    $this->gerarReciboComissaoUsuario($usuarioId, $formando['ViewFormandos']['id'], 
                                        $formando['ViewFormandos']['valor_adesao'], $valor);
                                }
                            }
                        }
                    }
                }
            debug($recibo);
            die();
            }
            //$this->ReciboComissao->save($recibo);
        }
    }
    
    function gerarReciboComissao($vendedorId, $totalFormandos, $valorTotalComissao){
        if($this->ReciboComissao->save(array(
            'ReciboComissao' => array(
                'id' => NULL,
                'vendedor_id' => $vendedorId,
                'total_formandos' => $totalFormandos,
                'valor' => $valorTotalComissao
            )
        )))
            return $this->ReciboComissao->read(null, $this->ReciboComissao->getLastInsertID());
        else
            return false;
    }
    
    function gerarReciboComissaoUsuario($vendedorId ,$uid, $adesao, $valorPago){
        $this->ReciboComissaoUsuario->save(array(
            'ReciboComissaoUsuario' => array(
                'recibo_comissao_id' => $this->ReciboComissao->getLastInsertID(),
                'usuario_id' => $uid,
                'vendedor_id' => $vendedorId,
                'valor_adesao' => $adesao,
                'valor_pago' => $valorPago,
                'data_cadastro' => date('Y-m-d H:i:s')
            )
        ));
    }

}

*/

?>
