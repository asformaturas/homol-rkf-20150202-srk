<?php

class FotosParceriasShell extends Shell {

    var $uses = array('Parceiro', 'Parceria', 'FotoParceria', 'FotoParceiro');
    
    function main() {
        $this->Parceiro->unbindModelAll();
        $this->Parceiro->bindModel(array(
            'hasMany' => array('FotoParceiro')
        ),false);
        $this->Parceria->unbindModelAll();
        $this->Parceria->bindModel(array(
            'hasMany' => array('FotoParceria')
        ),false);
        $parceiros = $this->Parceiro->find('all');
        $path = array(
            'parceiros' => WWW_ROOT . "img/parceiros/fotos",
            'parcerias' => WWW_ROOT . "img/parcerias/fotos"
        );
        foreach($parceiros as $parceiro) {
            foreach($parceiro["FotoParceiro"] as $foto) {
                if(file_exists("{$path['parceiros']}/{$foto["nome"]}")) {
                    if($this->criarDiretorio("{$path['parceiros']}/{$parceiro["Parceiro"]["id"]}/")) {
                        $nome = String::uuid().".".$this->Parceiro->obterExtensaoDoArquivo(array_pop(explode("/",$foto["nome"])));
                        if(rename("{$path['parceiros']}/{$foto["nome"]}","{$path['parceiros']}/{$parceiro["Parceiro"]["id"]}/{$nome}")) {
                            $old = $foto["nome"];
                            $foto["nome"] = $nome;
                            $this->FotoParceiro->save($foto);
                            //if($this->FotoParceiro->save($foto))
                            //    unlink("{$path['parceiros']}/{$old}");
                        }
                    }
                } elseif(false && !file_exists("{$path['parceiros']}/{$parceiro["Parceiro"]["id"]}/{$foto["nome"]}")) {
                    $this->FotoParceiro->delete($foto["id"]);
                }
            }
        }
        $parcerias = $this->Parceria->find('all');
        foreach($parcerias as $parceria) {
            foreach($parceria["FotoParceria"] as $foto) {
                if(file_exists("{$path['parcerias']}/{$foto["nome"]}")) {
                    if($this->criarDiretorio("{$path['parcerias']}/{$parceria["Parceria"]["id"]}/")) {
                        $nome = String::uuid().".".$this->Parceiro->obterExtensaoDoArquivo(array_pop(explode("/",$foto["nome"])));
                        if(rename("{$path['parcerias']}/{$foto["nome"]}","{$path['parcerias']}/{$parceria["Parceria"]["id"]}/{$nome}")) {
                            $old = $foto["nome"];
                            $foto["nome"] = $nome;
                            $this->FotoParceria->save($foto);
                            //if($this->FotoParceria->save($foto))
                            //    unlink("{$path['parcerias']}/{$old}");
                        }
                    }
                } elseif(false && !file_exists("{$path['parcerias']}/{$parceria["Parceria"]["id"]}/{$foto["nome"]}")) {
                    $this->FotoParceria->delete($foto["id"]);
                }
            }
        }
    }
    
    function criarDiretorio($diretorio) {
        $return = is_dir($diretorio);
        if(!$return)
            $return = mkdir($diretorio);
        return $return;
    }
}