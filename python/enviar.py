#!/usr/bin/python
# -*- coding: utf8 -*-

import sys
import mysql.connector
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
import datetime

if len(sys.argv) > 1 :
    mensagemId = int(sys.argv[1])
    db = mysql.connector.connect(user='root', password='qMf#xz?B+6?@',
                          host='127.0.0.1',
                          database='thweb_sistema',
                          charset='utf8',
                          use_unicode=True)
    cursor = db.cursor(buffered=True)
    cursor.execute("set names utf8;")
    cursor.execute('SET CHARACTER SET utf8;')
    cursor.execute('SET character_set_connection=utf8;')
    queryMensagem = ("select usuarios.nome as usuario,"
                     "itens.nome as item,"
                     "assuntos.nome as assunto,"
                     "mensagens.texto as texto "
                     "from mensagens inner join "
                     "usuarios on "
                     "usuarios.id = mensagens.usuario_id "
                     "inner join assuntos on "
                     "assuntos.id = mensagens.assunto_id "
                     "inner join itens on "
                     "itens.id = assuntos.item_id "
                     "where mensagens.id = %i" % mensagemId)
    cursor.execute(queryMensagem)
    row = cursor.fetchone()
    mensagem = row[3].encode('latin1')
    mensagem+= ("<br /><br /><i>Esta mensagem foi gerada automaticamente"
            "<br />Por favor não responda este email</i><br /><br /><br />")
    item = row[1].encode('latin1')
    assunto = row[2].encode('latin1')
    usuario = row[0].encode('latin1')
    modelo = MIMEMultipart('alternative')
    modelo['subject'] = ("%s - %s" % (item,assunto))
    modelo['From'] = ("RK Formaturas - %s <notificacoesrk@rkformaturas.com.br>" % usuario)
    body = MIMEText(mensagem, 'html')
    modelo.attach(body)
    email = modelo
    server = smtplib.SMTP('localhost',587)
    #server.set_debuglevel(1)
    server.login("notificacoesrk@rkformaturas.com.br", "ate55943043") 
    cursorUpdate = db.cursor()
    cursorUsuarios = db.cursor(buffered=True)
    queryUsuarios = ("select mu.id as id,u.email as email from "
                     "mensagens_usuarios mu inner join usuarios u "
                     "on u.id = mu.usuario_id where mu.mensagem_id = %i and "
                     "u.id in(4474)" % mensagemId)
    queryUsuarios = ("select mu.id as id,u.email as email from "
                     "mensagens_usuarios mu inner join usuarios u "
                     "on u.id = mu.usuario_id where mu.mensagem_id = %i and "
                     "mu.enviada = 0 order by id desc limit 200" % mensagemId)
    cursorUsuarios.execute(queryUsuarios)
    for(mensagemUsuarioId,endereco) in cursorUsuarios :
        
        email['To'] = endereco
        
        try:
            server.sendmail("notificacoesrk@rkformaturas.com.br",endereco, email.as_string())
            queryUpdate = ("update mensagens_usuarios set enviada = 1, data_envio = "
                           "'%s' where id = %i" % (datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                                                   mensagemUsuarioId))
            cursorUpdate.execute(queryUpdate)
        except Exception:
            queryUpdate = ("update mensagens_usuarios set enviada = 0, data_envio = "
                           "NULL where id = %i" % mensagemUsuarioId)
            #cursorUpdate.execute(queryUpdate)
    
    db.commit()
    server.quit()
    cursorUsuarios.close()
    cursorUpdate.close()
    cursor.close()
    db.close()
