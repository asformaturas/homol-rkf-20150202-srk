<?php if(sizeof($despesas) == 0) { ?>
<h2 class="fg-color-red">Nenhuma Despesa de ades&atilde;o cadastrada</h2>
<?php } else { ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('.modal-body').tooltip({ selector: '.icon-help'});
        
        $('.remover').click(function(){
            var dados = {
                data:{
                    uid: $(this).data('uid'),
                    despesa: $(this).data('despesa')
                    }
            };
            $.ajax({
                url: 'super/area_financeira/remover_despesa',
                data: dados,
                type: "POST",
                dataType: "json",
                complete: function() {
                    var url = '<?="{$this->webroot}{$this->params['prefix']}"?>/' +
                            'area_financeira/dados/<?=$formando['id']?>/adesao';
                    $("#adesao").load(url);
                }
            });
        });
        
        $('.desvincular').click(function(){
            var dados = {
                data:{
                    uid: $(this).data('uid'),
                    despesa: $(this).data('despesa')
                    }
            };
            $.ajax({
                url: 'super/area_financeira/desvincular_pagamento',
                data: dados,
                type: "POST",
                dataType: "json",
                complete: function() {
                    var url = '<?="{$this->webroot}{$this->params['prefix']}"?>/' +
                            'area_financeira/dados/<?=$formando['id']?>/adesao';
                    $("#adesao").html('<h2>Carregando</h2>');
                    $("#adesao").load(url);
                }
            });
        });
        $('.cheque').click(function(){
            var dados = {
                data:{
                    despesa: $(this).data('despesa')
                    }
            };
            $.ajax({
                url: 'atendimento/area_financeira/cheque',
                data: dados,
                type: "POST",
                dataType: "json",
                complete: function() {
                    var url = '<?="{$this->webroot}{$this->params['prefix']}"?>/' +
                            'area_financeira/dados/<?=$formando['id']?>/adesao';
                    $("#adesao").html('<h2>Carregando</h2>');
                    $("#adesao").load(url);
                }
            });
        });
    });
</script>
<table class="table table-condensed table-striped">
    <thead>
        <tr>
            <th width="1%">Parcela</th>
            <th width="10%">Valor</th>
            <th width="18%">Data<br />Vencim</th>
            <th width="12%">Data<br />Pagam</th>
            <th width="10%">Valor<br />Creditado</th>
            <th width="10%">IGPM</th>
            <th width="10%">Status</th>
            <th width="10%">Saldo</th>
            <th width="15%">&nbsp;</th>
            <th width="4%">&nbsp;</th>
            <?php if($this->params['prefix'] == 'super') : ?>
            <?php endif; ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($despesas as $despesa): ?>
        <tr data-despesa="<?=$despesa['id']?>">
            <td><?= $despesa['parcela'] . '/' . $despesa['total_parcelas']; ?></td>
            <td>R$<?=number_format($despesa['valor'], 2, ',', '.') ?></td>
            <?php if (in_array($despesa['status'],array('atrasada','aberta'))) : ?>
            <td data-vencimento="<?=date('d/m/Y', strtotime($despesa['data_vencimento']))?>">
            <?php else : ?>
            <td>
            <?php endif; ?>
                <span class="pointer">
                    <?=date('d/m/Y', strtotime($despesa['data_vencimento']))?>
                </span>
            </td>
            <td>
                <?php
                if(!empty($despesa['data_pagamento']))
                    echo date('d/m/Y', strtotime($despesa['data_pagamento']));
                else
                    echo '-';
                ?>
            </td>
            <td><?=$despesa['valor_pago'] > 0 ? "R$" . number_format($despesa['valor_pago'], 2, ',', '.') : "-" ?></td>
            <td>
                <?=$despesa['correcao_igpm'] > 0 ? "R$" . number_format($despesa['correcao_igpm'], 2, ',', '.') : "-" ?>
                <?php if ($despesa['status_igpm'] == 'recriado') : ?>
                <i class="icon-help" rel="tooltip"
                    title="Esta Parcela de IGPM foi embutida em uma parcela única"></i>
                <?php endif; ?>
            </td>
            <td><?=$despesa['status']?></td>
            <?php if(round($despesa['saldo_parcela'], 2) == 0) : ?>
            <td>-</td>
            <?php elseif(round($despesa['saldo_parcela']) > 0) : ?>
            <td class="fg-color-orange">
                R$<?= number_format(abs($despesa['saldo_parcela']), 2, ',', '.'); ?>
            </td>
            <?php else : ?>
            <td class="fg-color-green">
                R$<?= number_format(abs($despesa['saldo_parcela']), 2, ',', '.'); ?>
            </td>
            <?php endif; ?>
            <td style="text-align: center">
                <?php if($formando['realizou_checkout'] != 1) : ?>
                <?php if ($despesa['status'] == "atrasada") : ?>
                <?php if ($despesa['cheque'] == 0) : ?>
                <div class="dropdown">
                    <a class="dropdown-toggle button mini default"
                        data-toggle="dropdown" href="#">
                        Boleto
                        <i class="icon-arrow-20"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="/area_financeira/gerar_boleto/<?=$despesa['id']?>/<?=$despesa['usuario_id']?>"
                                tabindex="-1" target="_blank">
                                Gerar
                            </a>
                        </li>
                      <!--   <li>
                            <a href="/<?= $this->params['prefix'] ?>/area_financeira/gerar_boleto_com_multa/<?=$despesa['id'] ?>"
                                tabindex="-1" target="_blank">
                                Gerar Com Multa
                            </a>
                        </li> -->
                    </ul>
                </div>
                <?php else : ?>
                    <span class='button mini bg-color-red'>
                        Cheque
                    </span>
                <?php endif; ?>
                <?php elseif($despesa['status'] == "aberta") : ?>
                <?php if ($despesa['cheque'] == 0) : ?>
                <a href="/area_financeira/gerar_boleto/<?=$despesa['id'] ?>/<?=$despesa['usuario_id']?>"
                    class='button mini default' target='_blank'>
                    Gerar Boleto
                </a>
                <?php else : ?>
                    <span class='button mini bg-color-red'>
                        Cheque
                    </span>
                <?php endif; ?>
                <?php endif; ?>
                <?php endif; ?>
            </td>
            <?php if($this->params['prefix'] == 'super') : ?>
            <td>
                <button class="button mini bg-color-red remover" data-despesa="<?=$despesa['id']?>" data-uid="<?=$despesa['usuario_id']?>">
                    <i class="icon-remove"></i>
                </button>
            </td>
            <?php endif; ?>
            <?php if($this->params['prefix'] == 'atendimento') : ?>
            <td>
                <?php if($despesa['cheque'] == 0) : ?>
                <button class="button mini bg-color-greenDark cheque" data-despesa="<?=$despesa['id']?>">
                    <i class="icon-list"></i>
                </button>
                <?php else : ?>
                <button class="button mini bg-color-red cheque" data-despesa="<?=$despesa['id']?>">
                    <i class="icon-list"></i>
                </button>
                <?php endif; ?>
            </td>
            <?php endif; ?>
            <?php if($this->params['prefix'] == 'super' && $despesa['status'] == 'paga' && $despesa['correcao_igpm'] < 1) : ?>
            <td>
                <button class="button mini bg-color-orangeDark desvincular" data-despesa="<?=$despesa['id']?>" data-uid="<?=$despesa['usuario_id']?>">
                    <i class="icon-arrow-right-2"></i>
                </button>
            </td>
            <?php endif; ?>
        </tr>
        <?php endforeach; ?>
        <?php if($igpm) : ?>
        <tr>
            <td>IGPM</td>
            <td>
                R$<?=number_format($igpm['valor'], 2, ',', '.') ?>
            </td>
            <td>-</td>
            <td>-</td>
            <td>
                R$<?=number_format($igpm['valor_pago'], 2, ',', '.') ?>
            </td>
            <td>-</td>
            <td>-</td>
            <?php if($igpm['saldo_parcela'] == 0) : ?>
            <td>-</td>
            <?php elseif($igpm['saldo_parcela'] > 0) : ?>
            <td class="fg-color-orange">
                R$<?= number_format($igpm['saldo_parcela'], 2, ',', '.'); ?>
            </td>
            <?php else : ?>
            <td class="fg-color-green">
                R$<?= number_format(abs($igpm['saldo_parcela']), 2, ',', '.'); ?>
            </td>
            <?php endif; ?>
            <td></td>
        </tr>
        <?php endif; ?>
    </tbody>
</table>
<?php if($cancelamento) : ?>
<br />
<h2 class="fg-color-orangeDark">Despesas Cancelamento</h2>
<br />
<table class="table table-condensed table-striped">
    <thead>
        <tr>
            <th width="4%">Parcela</th>
            <th width="10%">Valor</th>
            <th width="18%">Data<br />Vencim</th>
            <th width="12%">Data<br />Pagam</th>
            <th width="10%">Valor<br />Creditado</th>
            <th width="10%">IGPM</th>
            <th width="10%">Status</th>
            <th width="10%">Saldo</th>
            <th width="16%">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($cancelamento as $despesaCancelamento) : ?>
        <tr data-despesa="<?=$despesaCancelamento['Despesa']['id']?>">
            <td class="fg-color-red">Boleto Cancelamento</td>
            <td>
                R$<?=number_format($despesaCancelamento['Despesa']['valor'], 2, ',', '.') ?>
            </td>
            <?php if(in_array($despesaCancelamento['Despesa']['status'],array('aberta'))) : ?>
            <td data-vencimento="<?=date('d/m/Y', strtotime($despesaCancelamento['Despesa']['data_vencimento']))?>">
            <?php else : ?>
            <td>
            <?php endif; ?>
                <span class="pointer">
                    <?=date('d/m/Y', strtotime($despesaCancelamento['Despesa']['data_vencimento']))?>
                </span>
            </td>
            <td>-</td>
            <td>
                <?php
                if(!empty($despesaCancelamento['Despesa']['data_pagamento']))
                    echo date('d/m/Y', strtotime($despesaCancelamento['Despesa']['data_pagamento']));
                else
                    echo '-';
                ?>
            </td>
            <td>-</td>
            <td><?=$despesaCancelamento['Despesa']['status']?></td>
            <td>-</td>
            <td>
                <?php if($despesaCancelamento['Despesa']['status'] == "aberta") : ?>
                <a href="/area_financeira/gerar_boleto/<?=$despesaCancelamento['Despesa']['id']?>/<?=$despesaCancelamento['Despesa']['usuario_id']?>"
                    class='button mini default' target='_blank'>
                    Gerar Boleto
                </a>
                <?php endif; ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
        <?php endif; ?>
<?php } ?>