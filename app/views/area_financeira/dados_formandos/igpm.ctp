<?php if($igpm['valor'] > 0) : ?>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<style type="text/css">
    .tooltip-inner{ max-width: 500px!important; max-height: 500px!important }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        $('.selectpicker').selectpicker({width:'100%'});
        $('.modal-body').tooltip({ selector: '.icon-help'});
        $("#criar-parcelas > div > select").change(function() {
            if($(this).val() != "")
                $("#criar-parcelas").find('.enviar').fadeIn(500);
            else
                $("#criar-parcelas").find('.enviar').fadeOut(500);
        });
        $(".enviar").click(function() {
            var parcelas = $("#criar-parcelas").find('.qtde').val();
            $("#criar-parcelas").html("<h4>Aguarde...</h4>");
            var url = "/<?= $this->params["prefix"] ?>/area_financeira/";
            $.ajax({
                url: url + "parcelar_igpm/<?=$formando['id']?>",
                data: {
                    parcelas: parcelas
                },
                type: "POST",
                dataType: "json",
                success: function(response) {
                    if(response.parcelas.length > 0) {
                        var url = '<?="{$this->webroot}{$this->params['prefix']}"?>/' +
                            'area_financeira/dados/<?=$formando['id']?>/igpm';
                        $("#igpm").html('<h2>Carregando</h2>');
                        $("#igpm").load(url);
                    } else
                        $("#criar-parcelas").html("<h4 class='fg-color-red'>Erro ao gerar parcelas</h4>");
                },
                error: function() {
                    $("#criar-parcelas").html("<h4 class='fg-color-red'>Erro ao conectar ao servidor</h4>");
                }
            });
        });
    });
</script>
<div class="row-fluid">
    <div class="span4">
        <table class="table table-condensed table-striped">
            <tbody>
                <tr style="height: 32px">
                    <td class="header">Valor Total</td>
                    <td>R$<?=number_format($igpm['valor'], 2, ',', '.') ?></td>
                </tr>
                <tr style="height: 33px">
                    <td class="header">Valor Pago</td>
                    <td>R$<?=number_format($igpm['valor_pago'], 2, ',', '.') ?></td>
                </tr>
                <tr style="height: 33px">
                    <?php if($igpm['saldo_parcela'] > 0) : ?>
                    <td class="bg-color-red">Saldo Negativo</td>
                    <?php elseif($igpm['saldo_parcela'] < 0) : ?> <i class="icon-help" rel="tooltip"
                    title="<?=$textoIGPM?>" data-placement="right"></i>
                    <td class="bg-color-greenDark">Saldo</td>
                    <?php else : ?>
                    <td class="header">Saldo</td>
                    <?php endif; ?>
                    <td>R$<?=number_format(abs($igpm['saldo_parcela']), 2, ',', '.') ?> <i class="icon-help pull-right" rel="tooltip"
                    title="<?=$textoIGPM?>" data-placement="right"></i></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="span8">
    <?php if(!empty($textoParcelas)) : ?>
        <div class="alert fade in">
            <h4 class="alert-heading">importante!</h4>
            <?=$textoParcelas?>
        </div>
    <?php endif; ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span8">
        <?php if($parcelas) : ?>
        <table class="table table-condensed table-striped">
            <thead>
                <tr>
                    <th>Valor</th>
                    <th>Status</th>
                    <th>Data de<br />Pagamento</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($parcelas as $parcela) : ?>
                <tr>
                    <td>R$<?=number_format($parcela['Pagamento']['valor_nominal'], 2, ',', '.') ?></td>
                    <td><?=ucfirst($parcela['Pagamento']['status']) ?></td>
                    <td>
                        <?php if(!empty($parcela['Pagamento']['dt_liquidacao'])) : ?>
                        <?=date('d/m/Y',strtotime($parcela['Pagamento']['dt_liquidacao']))?>
                        <?php else : ?>
                        -
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if($parcela['Pagamento']['status'] == 'aberto') : ?>
                        <a href="/<?=$this->params['prefix']?>/area_financeira/gerar_boleto_igpm/<?=$parcela['Pagamento']['id']?>"
                            class="button mini default" target="_blank">
                            Gerar Boleto
                        </a>
                        <?php endif; ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?php else : ?>
        <h3>Parcelar IGPM</h3>
        <div class="row-fluid" id="criar-parcelas">
            <div class="span4">
                <select class="selectpicker qtde" data-container="body">
                    <option value="">Selecione</option>
                    <?php for ($a = 1; $a <= ($this->params['prefix'] == "atendimento" ? 10 : 3); $a++) : ?>
                        <option value='<?= $a ?>'><?=$a?>x</option>
                    <?php endfor; ?>
                </select>
            </div>
            <div class="span8">
                <button type="button" class="bg-color-blueDark hide enviar">Criar Parcelas</button>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>
<?php else : ?>
<h2 class="fg-color-red">
    Despesa de igpm n&atilde;o encontrada
</h2>
<?php endif; ?>
