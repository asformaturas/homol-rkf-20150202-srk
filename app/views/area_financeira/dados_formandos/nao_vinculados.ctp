<?php if(sizeof($pagamentos) == 0) { ?>
<h2 class="fg-color-red">Nenhum pagamento encontrado</h2>
<?php } else { ?>
<style type="text/css">
.no-margin { margin-bottom: 0!important; }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        $(".selectpicker").selectpicker();
        $(".data-vinculo").change(function() {
            if($(this).val() != "")
                $(this).parent().next().fadeIn(500);
            else
                $(this).parent().next().fadeOut(500);
        });
        $(".vincular").click(function() {
            var pagamento = $(this).parents('[data-pagamento]').data('pagamento');
            var despesa = $(this).parent().prev().find(".data-vinculo").val();
            var td = $(this).parents('td');
            td.html('Aguarde');
            var url = '/<?=$this->params['prefix']?>/area_financeira/vincular_pagamento';
            $.ajax({
                url : url,
                type : 'POST',
                dataType : 'json',
                data : {
                    data : {
                        despesa : despesa,
                        pagamento : pagamento
                    }
                },
                success : function(response) {
                    if(response.erro)
                        td.text("Erro ao vincular pagamento");
                    else {
                        var url = '<?="{$this->webroot}{$this->params['prefix']}"?>/' +
                            'area_financeira/dados/<?=$formando['id']?>/nao_vinculados';
                        $("#nao_vinculados").html('<h2>Carregando</h2>');
                        $("#nao_vinculados").load(url);
                    }
                },
                error : function() {
                    td.text("Erro ao conectar ao servidor");
                }
            });
        });
        $(".remover").click(function() {
            var id = $(this).data('id');
            var td = $(this).parents('td');
            td.html('Aguarde');
            var url = '/<?=$this->params['prefix']?>/area_financeira/remover_pagamento/' + id;
            $.ajax({
                url : url,
                type : 'POST',
                dataType : 'json',
                success : function(response) {
                    if(response.erro == 1)
                        td.text("Erro ao remover pagamento");
                    else {
                        var url = '<?="{$this->webroot}{$this->params['prefix']}"?>/' +
                            'area_financeira/dados/<?=$formando['id']?>/nao_vinculados';
                        $("#nao_vinculados").html('<h2>Carregando</h2>');
                        $("#nao_vinculados").load(url);
                    }
                },
                error : function() {
                    td.text("Erro ao conectar ao servidor");
                }
            });
        });
    });
</script>
<table class="table table-condensed table-striped">
    <thead>
        <tr>
            <th width="15%">N&ordm; Documento</th>
            <th width="15%">Valor Nominal</th>
            <th width="15%">Valor Pago</th>
            <th width="20%">Data Pagamento</th>
            <th width="30%">&nbsp;</th>
            <?php if($this->params['prefix'] == 'super') : ?>
            <th width="5%">&nbsp;</th>
            <?php endif; ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($pagamentos as $pagamento): ?>
        <tr data-pagamento="<?=$pagamento['id']?>">
            <td>
                <?php if(empty($pagamento['codigo'])) : ?>
                N&atilde;o informado
                <?php else : ?>
                <?=$pagamento['codigo']?>
                <?php endif; ?>
            </td>
            <td>R$<?=number_format($pagamento['valor_nominal'], 2, ',', '.') ?></td>
            <td>R$<?=number_format($pagamento['valor_pago'], 2, ',', '.') ?></td>
            <td>
                <?php if(empty($pagamento['dt_liquidacao'])) : ?>
                N&atilde;o informado
                <?php else : ?>
                <?=date('d/m/Y', strtotime($pagamento['dt_liquidacao']))?>
                <?php endif; ?>
            </td>
            <td>
                <div class="row-fluid">
                    <div class="span9">
                        <?=$form->input(false,array(
                            'options' => $despesas,
                            'type' => 'select',
                            'class' => 'selectpicker no-margin data-vinculo',
                            'data-width' => '100%',
                            'data-container' => 'body',
                            'label' => false,
                            'div' => false)); ?>
                    </div>
                    <div class="span3 hide">
                        <button type="button" class="mini default vincular">
                            <i class="icon-thumbs-up"></i>
                        </button>
                    </div>
                </div>
            </td>
            <?php if($this->params['prefix'] == 'super') : ?>
            <td>
                <button type="button" class="mini bg-color-red default remover" data-id="<?=$pagamento['id']?>">
                    Remover
                </button>
            </td>
            <?php endif; ?>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php } ?>