<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/bootstrap/datepicker.css">
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/bootstrap/datepicker.js"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/jquery.meio.mask.js"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/max/jquery.maskMoney.js"></script>
<style type="text/css">
    
</style>
<script type="text/javascript">
    $(document).ready(function(){
        var context = ko.contextFor($(".metro-button.reload")[0]);
        $(".valores").maskMoney();
        $('input[alt]').setMask();
        $('.data').on('keydown', function(){
            $('input[alt]').setMask();
            $(".valores").maskMoney();
        });
        $(".adicionar-pagamento").click(function() {
            var div = $('.modal-body').find(".div-pagamentos.model").clone();
            var count = $(".div-pagamentos").size();
            $(".div-pagamentos:first").each(function(index) {
                var prefix = "Pagamento[" + count + "]";
                $(this).find("input").each(function(i) {
                    this.id = this.id.replace(this.id, "Pagamento" + count);
                    this.name = this.name.replace(this.name, prefix + "[" + i + "]");
                });
            });
            div.removeClass('model').removeClass('hide').appendTo($(".pagamentos"));
            context.$data.loaded(true);
        });
        $(".modal-body").on('click','.remover-pagamento',function(e) {
            if($(".div-pagamentos").size() > 1){
                var div = $(this).parents('.div-pagamentos');
                div.fadeOut(500,function() {
                    $(this).remove();
                });
            }
        });
        $(".enviar").on('click', function(e) {
            e.preventDefault();
            var dados = $("#form").serialize();
            var url = $("#form").attr('action');
            
            var empty = 0;
            $(".pagamentos input").each(function(i, input) {
                 if(input.value == '')
                     empty++;
            });
            if(empty > 0){
                bootbox.alert("<h2 class='fg-color-red'>Há " + empty + " campo(s) para ser(em) preenchido(s)!</h2>")
            }else{
                $('#adesao').html('<h2>Carregando</h2>');
                $.ajax({
                    url: url,
                    data: dados,
                    type: "POST",
                    dataType: "json",
                    complete: function() {
                        var url = '<?="{$this->webroot}{$this->params['prefix']}"?>/' +
                                'area_financeira/dados/<?=$formando['id']?>/inserir_pagamentos';
                            $("#inserir_pagamentos").load(url);
                    }
                });
            }
        });
    });
</script>
<?php $session->flash(); ?>
<?php echo $form->create('Pagamento', array('url' => "/{$this->params['prefix']}/area_financeira/inserir_pagamentos/" . $formando['id'], 'id' => 'form')); ?>
<div class="content">
    <div class="row-fluid">
        <button type="button" class="bg-color-blueDark adicionar-pagamento">
            Adicionar<i class="icon-plus-3" style="vertical-align: initial"></i>
        </button>
    </div>
    <div class="row-fluid pagamentos">
        <div class="row-fluid div-pagamentos model">
            <div class="span3">
                <div class="input-control text">
                    <label for="Pagamento0">Dt Vencimento</label>
                    <input name="data[Pagamento][0][0]" type="text" class="data" alt="99/99/9999" id="Pagamento0">
                </div>
            </div>
            <div class="span3">
                <div class="input-control text">
                    <label for="Pagamento0">Dt Liquidação</label>
                    <input name="data[Pagamento][0][1]" type="text" class="data" alt="99/99/9999" id="Pagamento0">
                </div>
            </div>
            <div class="span2">
                <div class="input-control text">
                    <label for="Pagamento0">Valor Nominal</label>
                    <input name="data[Pagamento][0][2]" type="text" class="valores" id="Pagamento0">
                </div>
            </div>
            <div class="span2">
                <div class="input-control text">
                    <label for="Pagamento0">Valor Pago</label>
                    <input name="data[Pagamento][0][3]" type="text" class="valores" id="Pagamento0">
                </div>

            </div>
            <div class="span2">
                <label>&nbsp;</label>
                <button type="button" class="button mini bg-color-red remover-pagamento">
                    Remover
                </button>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <button type="button" class="bg-color-greenDark enviar">
            Enviar
        </button>
    </div>
    <?=$form->end(array('label' => false, 'div' => false, 'style' => 'display:none')); ?>
</div>