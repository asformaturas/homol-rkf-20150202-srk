<?php if(count($campanhas) > 0) : ?>
<script type="text/javascript">
    $(document).ready(function() {
        $("[data-cancelada]").click(function() {
            var span = $(this);
            span.removeClass('fg-color-red').text("Aguarde");
            var campanhaUsuario = span.parents('[data-campanha-usuario]').data('campanha-usuario');
            var url = "/area_financeira/";
            if($(this).data('cancelada') == 0)
                url+= "cancelar_campanha";
            else
                url+= "descancelar_campanha";
            $.ajax({
                url : url,
                type : 'POST',
                dataType : 'json',
                data : {
                    data : {
                        campanha_usuario : campanhaUsuario
                    }
                },
                success : function(response) {
                    if(response.erro)
                        span.text("Erro ao cancelar campanha");
                    else {
                        var url = '<?="{$this->webroot}{$this->params['prefix']}"?>/' +
                            'area_financeira/dados/<?=$formando['id']?>/extras';
                        $("#extras").html('<h2>Carregando</h2>');
                        $("#extras").load(url);
                    }
                },
                error : function() {
                    span.text("Erro ao conectar ao servidor");
                }
            });
        });
    });
</script>
<?php foreach($campanhas as $campanha) : ?>
<h3><?=$campanha['nome']?></h3>
<br />
<?php foreach($campanha['compras'] as $campanhaUsuarioId => $compra) : ?>
<table class="table table-condensed table-striped" data-campanha-usuario="<?=$campanhaUsuarioId?>">
    <thead>
        <tr>
            <th colspan="8">
                Compra efetuada em <?=date('d/m/Y',strtotime($compra['data_compra']))?> 
                &agrave;s <?=date('H:i',strtotime($compra['data_compra']))?>
                &nbsp;&nbsp;&nbsp;
                <?php if($compra['cancelada'] == 1) : ?>
                <span class="strong">Cancelada</span>
                <?php else : ?>
                <span class="strong"><?=ucfirst($compra['status'])?></span>
                <?php endif; ?>
                <span class="strong pull-right pointer fg-color-red" data-cancelada="<?=$compra['cancelada']?>">
                    <?php if($compra['cancelada'] == 0) : ?>
                    Cancelar compra
                    <?php else : ?>
                    Desfazer cancelamento
                    <?php endif; ?>
                </span>
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="3">
                Valor da compra: R$<?=number_format($compra['valor_campanha'], 2, ',', '.') ?>
            </td>
            <td colspan="3">
                Valor pago: R$<?=number_format($compra['valor_pago'], 2, ',', '.') ?>
            </td>
            <td colspan="2">
                Parcelas: <?=count($compra['despesas'])?>
            </td>
        </tr>
        <?php foreach($compra['itens'] as $item) : ?>
        <tr>
            <td colspan="8"><?=$item['evento']?> - <?=$item['quantidade']?> <?=$item['nome']?></td>
        </tr>
        <?php endforeach; ?>
        <tr style="height:15px"><td class="bg-color-white" colspan="8"></td></tr>
        <tr>
            <td class="bg-color-grayDark" width="8%">Parcela</td>
            <td class="bg-color-grayDark" width="10%">Valor</td>
            <td class="bg-color-grayDark" width="18%">Data Venc</td>
            <td class="bg-color-grayDark" width="14%">Data Pagam</td>
            <td class="bg-color-grayDark" width="12%">Valor Cred</td>
            <td class="bg-color-grayDark" width="12%">Status</td>
            <td class="bg-color-grayDark" width="12%">Saldo</td>
            <td class="bg-color-grayDark" width="14%">&nbsp;</td>
        </tr>
        <?php foreach($compra['despesas'] as $despesa) : ?>
        <tr data-despesa="<?=$despesa['id']?>">
            <td><?= $despesa['parcela'] . '/' . $despesa['total_parcelas']; ?></td>
            <td>R$<?=number_format($despesa['valor'], 2, ',', '.') ?></td>
            <?php if (in_array($despesa['status'],array('atrasada','aberta'))) : ?>
            <td data-vencimento="<?=date('d/m/Y', strtotime($despesa['data_vencimento']))?>">
            <?php else : ?>
            <td>
            <?php endif; ?>
                <span class="pointer">
                    <?=date('d/m/Y', strtotime($despesa['data_vencimento']))?>
                </span>
            </td>
            <td>
                <?php
                if(!empty($despesa['data_pagamento']))
                    echo date('d/m/Y', strtotime($despesa['data_pagamento']));
                else
                    echo '-';
                ?>
            </td>
            <td>R$<?=number_format($despesa['valor_pago'], 2, ',', '.') ?></td>
            <td><?=$despesa['status']?></td>
            <?php if($despesa['saldo_parcela'] == 0) : ?>
            <td>-</td>
            <?php elseif($despesa['saldo_parcela'] > 0) : ?>
            <td class="fg-color-orange">
                R$<?= number_format(abs($despesa['saldo_parcela']), 2, ',', '.'); ?>
            </td>
            <?php else : ?>
            <td class="fg-color-green">
                R$<?= number_format(abs($despesa['saldo_parcela']), 2, ',', '.'); ?>
            </td>
            <?php endif; ?>
            <td>
                <?php if($formando['realizou_checkout'] != 1) : ?>
                <?php if($despesa['status'] == "aberta" || $despesa['status'] == "atrasada") : ?>
                <a href="/area_financeira/gerar_boleto/<?=$despesa['id'] ?>/<?=$despesa['usuario_id'] ?>"
                    class='button mini default' target='_blank'>
                    Gerar Boleto
                </a>
                <?php endif; ?>
                <?php endif; ?>
            </td>
        </tr>
        <?php endforeach; ?>
        <tr style="height:15px"><td class="bg-color-white" colspan="8"></td></tr>
    </tbody>
</table>
<br />
<?php endforeach; ?>
<?php endforeach; ?>
<?php else : ?>
<h2 class="fg-color-red">Nenhuma compra de extras encontrada</h2>
<?php endif; ?>
