<?php
    $sortOptions = array('data-bind' => 'click: loadThis');
?>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<script type="text/javascript">
$(document).ready(function() {
    $('.cancelar').click(function() {
        dir = $(this).attr('dir');
        bootbox.alert("Tem certeza que deseja cancelar?");
        $('.button').addClass('bg-color-blue');
        $('.button').text('Desistir');
        var cem = $("<button>",{
            type: 'submit',
            class: 'button bg-color-red',
            id:'cem',
            text:'Multa 100%'
        });
        $('.modal-footer').prepend(cem);
        var vinte = $("<button>",{
            type: 'submit',
            class: 'button bg-color-orangeDark',
            id:'vinte',
            text:'Multa 20%'
        });
        $('.modal-footer').prepend(vinte);
        var isento = $("<button>",{
            type: 'submit',
            class: 'button bg-color-greenDark',
            id:'isento',
            text:'Devolver 100%'
        });
        $('.modal-footer').prepend(isento);
        $('#cem').click(function() {
            var context = ko.contextFor($(".metro-button.reload")[0]);
            var url = '<?="/area_financeira/cancelar_cem/"?>'+dir;
            bootbox.hideAll();
            context.$data.showLoading(function() {
                $.ajax({
                    url : url,
                    type: "POST",
                    dataType : "json",
                    complete : function() {
                        context.$data.reload();
                    }
                });
            });
        });
        $('#vinte').click(function() {
            var context = ko.contextFor($(".metro-button.reload")[0]);
            var url = '<?="/area_financeira/cancelar_vinte/"?>'+dir;
            bootbox.hideAll();
            context.$data.showLoading(function() {
                $.ajax({
                    url : url,
                    type: "POST",
                    dataType : "json",
                    complete : function() {
                        context.$data.reload();
                    }
                });
            });
        });
        $('#isento').click(function() {
            var context = ko.contextFor($(".metro-button.reload")[0]);
            var url = '<?="/area_financeira/cancelar_isento/"?>'+dir;
            bootbox.hideAll();
            context.$data.showLoading(function() {
                $.ajax({
                    url : url,
                    type: "POST",
                    dataType : "json",
                    complete : function() {
                        context.$data.reload();
                    }
                });
            });
        });
    });
});
</script>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Solicitações de cancelamento
        </h2>
    </div>
</div>
<?php $session->flash(); ?>
<div class="row-fluid">
<?php if (sizeof($formandos) == 0 ) { ?>
    <h2 class="fg-color-red">Nenhuma solicitação de cancelamento.</h2>
<?php } else { ?>
            <table class="table table-condensed table-striped">
                <thead>
                    <tr>
                        <th scope="col"><?=$paginator->sort('Codigo Formando', 'ViewFormandos.codigo_formando',$sortOptions); ?></th>
                        <th scope="col"><?=$paginator->sort('nome', 'ViewFormandos.nome',$sortOptions); ?></th>
                        <th scope="col"><?=$paginator->sort('Observação Atendimento', 'FormandoProfile.obs_cancelamento',$sortOptions); ?></th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($formandos as $formando): ?>
                    <tr>
                        <td width="10%"><?=$formando['ViewFormandos']['codigo_formando']?></td>
                        <td width="20%"><?=$formando['ViewFormandos']['nome']; ?></td>
                        <td width="50%"><?=$formando['FormandoProfile']['obs_cancelamento']; ?></td>
                        <td width="20%" dir="<?=$formando['ViewFormandos']['id']?>" style='width: 130px; text-align: center'>
                            <button class="mini default bg-color-red cancelar" type="button" dir="<?=$formando['ViewFormandos']['id']?>">
                                Cancelar Contrato
                            </button> 
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="6" class="paginacao">
                            <?=$paginator->numbers(array('separator' => ' ','data-bind' => 'click: loadThis')); ?>
                        </td>
                    </tr>
                </tfoot>
            </table>
<?php } ?>
</div>

