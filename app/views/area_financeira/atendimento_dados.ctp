<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<style type="text/css">
td[data-vencimento] input { height:13px; min-height: 13px; display:block;
    width:75px; min-width:73px; margin: 2px 0 0 0; padding:0; float: left;
    border:none; background: transparent; font-size: 13px; line-height: 13px }
td[data-vencimento] i { width: 14px; height: 14px; font-size: 14px;
    line-height: 14px; margin: 2px 0 0 2px }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        var context = ko.contextFor($("#content-body")[0]);
        $('#tab-financeiro li a[data-toggle="tab"],.tab-show[data-toggle="tab"]').on('show', function(e) {
            var href = $(e.target).attr('href').substring(1);
            var content = $($(e.target).attr('href'));
            var url = '<?="{$this->webroot}{$this->params['prefix']}"?>/' +
                'area_financeira/dados/<?=$formando['id']?>/' + href;
            content.html('<h2>Carregando</h2>');
            content.load(url);
        });
        $(".bootbox.modal").on('click','.carregar-financeiro',function(e) {
            e.preventDefault();
            $(".modal-body").html('<h2>Carregando</h2>');
            $(".modal-body").load($(this).attr('href'));
        });
        $(".cancelar").click(function(e) {
            e.preventDefault();
            $(".modal-body").html('<h2>Carregando</h2>');
            $(".modal-body").load($(this).attr('href'));
        });
        $(".comprar-extra").click(function(e) {
            e.preventDefault();
            $(".modal-body").html('<h2>Carregando</h2>');
            $(".modal-body").load($(this).attr('href'));
        });
        $(".abrir-modal").click(function(e) {
            e.preventDefault();
            bootbox.hideAll();
            context.$data.page($(this).attr('href'));
        });
        $("#adesao,#extras").on('click','td[data-vencimento]',function() {
            var td = $(this);

            if(!td.hasClass('aberto') && $('.grupo-id').data('nivel-id') != 'basico') {
                td.addClass('aberto');
                var input = $("<input/>",{
                    type : 'text',
                    class : 'editando',
                    value : td.data('vencimento')
                });
                var enviar = $("<i/>",{
                    class : 'icon-checkmark editando fg-color-green pointer enviar'
                });
                var cancelar = $("<i/>",{
                    class : 'icon-blocked editando fg-color-red pointer cancelar'
                });
                td.children('span').fadeOut(500,function() {
                    td.append(input);
                    td.append(enviar);
                    td.append(cancelar);
                    input.focus();
                });
            }
        });
        $("#adesao,#extras").on('click','.editando.cancelar',function() {
            var td = $(this).parent();
            td.find('.editando').remove();
            td.children('span').fadeIn(500,function() {
                td.removeClass('aberto');
            });
        });
        $("#adesao,#extras").on('click','.editando.enviar',function() {
            var td = $(this).parent();
            if(td.find('input').val() == "") {
                alert('digite a data');
                td.find('input').focus();
            } else {
                var data = td.find('input').val();
                var despesa = td.parent().data('despesa');
                var url = "/<?=$this->params["prefix"]?>/area_financeira/despesa_alterar_data";
                td.find('.editando').remove();
                td.find('span').text('Aguarde').fadeIn(500,function() {
                    $.ajax({
                        url : url,
                        dataType : "json",
                        type : "POST",
                        data : {
                            despesa : despesa,
                            data : data
                        },
                        success : function(response) {
                            if(response.error) {
                                td.find('span').text('Erro');
                            } else {
                                td.find('span').text(data);
                                td.data('vencimento',data);
                            }
                        },
                        error : function(response) {
                            td.find('span').text('Erro');
                        },
                        complete : function() {
                            td.removeClass('aberto');
                        }
                    });
                });
            }
        });
        $('a[href="#resumo"]').tab('show');
    });
</script>
<div class="row-fluid grupo-id" data-nivel-id="<?=$usuario['Usuario']['nivel']?>">
    <div class="span8">
        <h3>
            <?=$formando['codigo_formando']?> - 
            <?=$formando['nome']?> 
            (<?=ucfirst($formando['grupo'])?>)
        </h3>
    </div>
    <div class="span4">
        <h6 class="fg-color-red pull-right">
            <?php if(!empty($acesso)) : ?>
                Primeiro Acesso: <?=date('d/m/Y &\a\g\r\a\v\e;\s H:i:s',strtotime($acesso[0]['UsuarioAcesso']['data_login']))?>
            <?php else : ?>
                Primeiro Acesso: Não houve
            <?php endif; ?>
        </h6>
        <?php $last = array_reverse($acesso); ?>
        <h6 class="fg-color-red pull-right">
            <?php if(!empty($acesso)) : ?>
                Último Acesso: <?=date('d/m/Y &\a\g\r\a\v\e;\s H:i:s',strtotime($last[0]['UsuarioAcesso']['data_login']))?>
            <?php else : ?>
                Último Acesso: Não houve
            <?php endif; ?>
        </h6>
    </div>
</div>
<br />
<?php if($this->params['prefix'] != 'super') :?>
<?php if($formando['situacao'] == 'cancelado') : ?>
<div class="alert alert-error">
    <h4>Contrato Cancelado</h4>
</div>
<?php else : ?>
    <?php if(!empty($protocolo)) : ?>
    <a class="button mini bg-color-greenDark" target="_blank"
        href="/<?=$this->params['prefix']?>/solicitacoes/imprimir_checkout/<?=$protocolo['Protocolo']['id']?>">
        Ver Checkout
    </a>
    <?php if(isset($temPosCheckout)) : ?>
    <a class="button mini bg-color-grey" target="_blank"
        href="/<?=$this->params['prefix']?>/solicitacoes/imprimir_pos_checkout/<?=$protocolo['Protocolo']['id']?>">
        Ver Pós Checkout
    </a>
    <?php endif; ?>
    <?php if($mapa) : ?>
    <a class="button mini bg-color-purple abrir-modal"
        href="/<?=$this->params['prefix']?>/eventos/selecionar_evento/<?=$formando['turma_id']?>/<?=$formando['id']?>/">
        Selecionar Mesas
    </a>
    <?php endif; ?>
    <a class="button mini bg-color-blueDark comprar-extra" target="_blank"
        href="/<?=$this->params['prefix']?>/checkout/comprar_itens/<?=$formando['id']?>">
        Comprar Itens Checkout
    </a>
    <!--
    <a class="button mini bg-color-blueDark comprar-extras"
        href="/<?=$this->params['prefix']?>/campanhas/comprar_extras/<?=$formando['id']?>/checkout">
        Comprar Itens Checkout
    </a>
    -->
    <?php else : ?>
    <a class="button mini bg-color-red cancelar"
        href="/<?=$this->params['prefix']?>/area_financeira/cancelamento/<?=$formando['id']?>">
        Cancelar Contrato
    </a>
    <?php if($turma['checkout'] == 'aberto' && substr($formando['codigo_formando'],-3) != "000") : ?>
    <a class="button mini bg-color-greenDark" target="_blank"
        href="/<?=$this->params['prefix']?>/checkout/realizar/<?=$formando['id']?>">
        Checkout
    </a>
    <?php endif; ?>
    <a class="button mini bg-color-blueDark" target="_blank"
       href="/principal/abrir_url/<?=base64_encode("{$this->params['prefix']}/area_financeira/renegociar/{$formando['id']}")?>">
        Renegociar Parcelas Em Atraso
    </a>
    <?php endif; ?>
    <a class="button mini bg-color-orange comprar-extras carregar-financeiro"
        href="/<?=$this->params['prefix']?>/campanhas/comprar_extras/<?=$formando['id']?>">
        Comprar Extras
    </a>
    <br />
<?php endif; ?>
<?php endif; ?>
<br />
<div class="row-fluid">
    <div class="span12">
        <ul class="nav nav-tabs" id='tab-financeiro'>
            <li>
                <a href="#resumo" data-toggle="tab">
                    Resumo
                </a>
            </li>
            <li>
                <a href="#adesao" data-toggle="tab">
                    Ades&atilde;o
                </a>
            </li>
            <li>
                <a href="#igpm" data-toggle="tab">
                    IGPM
                </a>
            </li>
            <li>
                <a href="#extras" data-toggle="tab">
                    Extras
                </a>
            </li>
            <li>
                <a href="#nao_vinculados" data-toggle="tab">
                    N&atilde;o Vinculados
                </a>
            </li>
            <?php if($this->params['prefix'] == 'super' && 
                     $usuario['Usuario']['nivel'] == 'administrador') : 
            ?>
            <li>
                <a href="#inserir_despesas" data-toggle="tab">
                   Inserir Despesas
                </a>
            </li>
            <li>
                <a href="#inserir_pagamentos" data-toggle="tab">
                   Inserir Pagamentos
                </a>
            </li>
            <?php endif; ?>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade" id="resumo">
                
            </div>
            <div class="tab-pane fade" id="adesao">
                
            </div>
            <div class="tab-pane fade" id="igpm">
                
            </div>
            <div class="tab-pane fade" id="extras">
                
            </div>
            <div class="tab-pane fade" id="nao_vinculados">
                
            </div>
            <div class="tab-pane fade" id="inserir_despesas">
                
            </div>
            <div class="tab-pane fade" id="inserir_pagamentos">
                
            </div>
        </div>
    </div>
</div>
