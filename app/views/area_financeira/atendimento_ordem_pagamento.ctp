<div style="position:relative; width:680px;margin:0 auto; margin-top:40px">
	<div class='legenda' style='width:100%'>
		<h2 style='color:red; margin-bottom:10px'>Ordem De Pagamento <strong style='color:#333; font-size:0.8em; font-weight:400'>(Cancelamento de contrato de formando)</strong></h2>
		<table style='width:48%; float:left'>
			<tr>
				<td colspan='2' style='padding-bottom:10px; color:black'><h3>Dados do Formando</h3></td>
			</tr>
			<tr>
				<td><em>Nome:</em></td>
				<td><?=$usuario['ViewFormandos']['nome']?>
			</tr>
			<tr>
				<td><em>C&oacute;digo:</em></td>
				<td><?=$usuario['ViewFormandos']['codigo_formando']?>
			</tr>
			<tr>
				<td><em>RG:</em></td>
				<td><?=$usuario['ViewFormandos']['rg']?>
			</tr>
			<tr>
				<td><em>CPF:</em></td>
				<td><?=$usuario['ViewFormandos']['cpf']?>
			</tr>
			<tr>
				<td><em>E-mail:</em></td>
				<td><?=$usuario['ViewFormandos']['email']?>
			</tr>
			<?php if($usuario['ViewFormandos']['tel_residencial'] != "") : ?>
			<tr>
				<td><em>Tel res.:</em></td>
				<td><?=$usuario['ViewFormandos']['tel_residencial']?>
			</tr>
			<?php endif; ?>
			<?php if($usuario['ViewFormandos']['tel_comercial'] != "") : ?>
			<tr>
				<td><em>Tel com.:</em></td>
				<td><?=$usuario['ViewFormandos']['tel_comercial']?>
			</tr>
			<?php endif; ?>
			<?php if($usuario['ViewFormandos']['tel_celular'] != "") : ?>
			<tr>
				<td><em>Tel cel.:</em></td>
				<td><?=$usuario['ViewFormandos']['tel_celular']?>
			</tr>
			<?php endif; ?>
			<tr>
				<td><em>Total Pago:</em></td>
				<td>R$ <?=$totalPago?>
			</tr>
		</table>
		<table style='width:48%; float:left'>
			<tr>
				<td colspan='2' style='padding-bottom:10px; color:black'><h3>Dados do Titular da Conta</h3></td>
			</tr>
			<tr>
				<td><em>Nome:</em></td>
				<td><?=$protocolo['ProtocoloCancelamento']['nome_titular']?>
			</tr>
			<tr>
				<td><em>CPF:</em></td>
				<td><?=$protocolo['ProtocoloCancelamento']['cpf_titular']?>
			</tr>
			<tr>
				<td><em>Banco:</em></td>
				<td><?=$protocolo['ProtocoloCancelamento']['banco']?>
			</tr>
			<tr>
				<td><em>Ag&ecirc;ncia:</em></td>
				<td><?=$protocolo['ProtocoloCancelamento']['agencia']?>
			</tr>
			<tr>
				<td><em style='font-weight:bold; color:red'>Valor a Pagar:</em></td>
				<td>R$ <?=$saldoCancelamento?>
			</tr>
		</table>
	</div>
</div>