<script type="text/javascript">
    $(document).ready(function() {
        $('#tab-financeiro li a[data-toggle="tab"],.tab-show[data-toggle="tab"]').on('show', function(e) {
            var href = $(e.target).attr('href').substring(1);
            var content = $($(e.target).attr('href'));
            var url = '<?="{$this->webroot}{$this->params['prefix']}"?>/' +
                'area_financeira/dados/<?=$formando['id']?>/' + href;
            content.html('<h2>Carregando</h2>');
            content.load(url,function() {
                content.addClass('carregado');
                var height = content.height() + 160;
                var minHeight = parseInt($("#conteudo").css('min-height'));
                content.css('min-height', minHeight);
                height = height < minHeight ? minHeight : height;
                var diferenca = Math.abs($("#conteudo").height() - height);
                if (diferenca > 10)
                    $("#conteudo").animate({
                        height: height
                    }, 600);
            });
        });
        $('#content-body').tooltip({ selector: '[rel=tooltip]'});
        $('a[href="#<?=$tipo ? $tipo : 'adesao'?>"]').tab('show');
    });
</script>
<?=$this->element('formando/area_financeira/area_financeira'); ?>