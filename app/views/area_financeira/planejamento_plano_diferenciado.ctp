<script type="text/javascript">
    $(document).ready(function() {
        var plano = <?=json_encode($plano);?>;
        if(plano.ParcelamentoDiferenciado.status == "processando") {
            if(plano.ParcelamentoOriginal != undefined) {
                var context = ko.contextFor($("#content-body")[0]),
                    aceitar = $("<button>",{
                    class : "bg-color-green",
                    text : "Aceitar",
                    type : "button"
                }),
                    recusar = $("<button>",{
                    class : "bg-color-red",
                    text : "Recusar",
                    type : "button"
                });
                $(".bootbox.modal").find(".modal-footer").prepend(recusar).prepend(aceitar);
                recusar.click(function() {
                    context.$data.showLoading(function() {
                        $(".modal-footer").hide();
                        $(".modal-body").html('<h2>Enviando...</h2>');
                        var dados = {
                            data : {
                                status : "recusado",
                                plano_id : '<?=$plano["ParcelamentoDiferenciado"]['id']?>'
                            }
                        };
                        $.ajax({
                            url : "/<?=$this->params["prefix"]?>/area_financeira/plano_diferenciado/",
                            type : "POST",
                            data : dados,
                            dataType : "json",
                            complete : function() {
                                bootbox.hideAll();
                                context.$data.reload();
                            }
                        });
                    });
                });
                aceitar.click(function() {
                    context.$data.showLoading(function() {
                        $(".modal-footer").hide();
                        $(".modal-body").html('<h2>Enviando...</h2>');
                        var dados = {
                            data : {
                                status : "aceito",
                                plano_id : '<?=$plano["ParcelamentoDiferenciado"]['id']?>'
                            }
                        };
                        $.ajax({
                            url : "/<?=$this->params["prefix"]?>/area_financeira/plano_diferenciado/",
                            type : "POST",
                            data : dados,
                            dataType : "json",
                            complete : function() {
                                bootbox.hideAll();
                                context.$data.reload();
                            }
                        });
                    });
                });
            }
        }
    });
</script>
<h4>Atendente: <i><?=$plano["Atendente"]["nome"]?></i></h4>
<h4>Formando: <i><?=$plano["Formando"]["codigo_formando"]?> - <?=$plano["Formando"]["nome"]?></i></h4>
<h4>Data: <i><?=date("d/m/Y",strtotime($plano["ParcelamentoDiferenciado"]["data_cadastro"]))?></i></h4>
<h4>Status: <i><?=ucfirst($plano["ParcelamentoDiferenciado"]["status"])?></i></h4>
<br />
<h4>
    Justificativa
    <br />
    <i class="fg-color-red"><?=$plano["ParcelamentoDiferenciado"]["plano"]->justificativa?></i>
</h4>
<br />
<div class="row-fluid">
<?php if(isset($plano["ParcelamentoOriginal"])) : ?>
    <div class="span11">
        <div class="row-fluid">
            <table class="table table-striped">
                <tbody>
                    <tr>
                        <td width="20%"></td>
                        <td class="header" width="40%">Plano Original</td>
                        <td class="header" width="40%">Plano Editado</td>
                    </tr>
                    <tr>
                        <td class="header">Valor</td>
                        <td>R$<?= number_format($plano["ParcelamentoOriginal"]["valor"], 2, ',', '.'); ?></td>
                        <td>R$<?= number_format($plano["ParcelamentoDiferenciado"]["plano"]->valor, 2, ',', '.'); ?></td>
                    </tr>
                    <tr>
                        <td class="header">Parcelas</td>
                        <td><?=$plano["ParcelamentoOriginal"]["parcelas"]?></td>
                        <td><?=count($plano["ParcelamentoDiferenciado"]["plano"]->despesas)?></td>
                    </tr>
                    <tr>
                        <td class="header">Valor Pago</td>
                        <td cols="2">R$<?= number_format($totalPago, 2, ',', '.'); ?></td>
                    </tr>
                </tbody>
            </table>
            <br />
            <h3 class="fg-color-red">
                Despesas Editadas
            </h3>
            <br />
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Despesa</th>
                        <th>Valor</th>
                        <th>Vencimento</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($plano["ParcelamentoDiferenciado"]["plano"]->despesas as $i => $despesa) : ?>
                    <tr>
                        <td><?=$i+1?>/<?=count($plano["ParcelamentoDiferenciado"]["plano"]->despesas)?></td>
                        <td>R$<?= number_format($despesa->valor, 2, ',', '.'); ?></td>
                        <td><?=date("d/m/Y",strtotime($despesa->data))?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
<?php else : ?>
    <h2 class="fg-color-red">
        Parcelamento original deste plano n&atilde;o encontrado.
    </h2>
<?php endif; ?>
</div>