<span id="conteudo-titulo" class="box-com-titulo-header">Dados Financeiros de Formando</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<div class="tabela-adicionar-item">
		<?=$form->create(false, array('url' => "/{$this->params['prefix']}/area_financeira/buscar_formando", 'class' => 'procurar-form-inline')); ?>
		<span>Código Formando: <?=$form->input('codigo_formando', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false , 'value' => isset($valor) ? $valor : "")); ?> </span>
		<?=$form->end(array('label' => ' ', 'div' => false, 'class' => 'submit-busca')); ?>
		<div style="clear:both;"></div>
	</div>
</div>