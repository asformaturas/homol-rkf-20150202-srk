<script type="text/javascript">
	jQuery(function($) {
 		$("#gerar-boletos").click(function() {
 	 		var html = "<div class='load-boletos'><center>";
 	 		html += "<h2 style='margin-top:200px'>Aguarde enquanto os boletos s&atilde;o gerados</h2><br /><br />";
 	 		html += "<img src='<?=$this->webroot?>img/load.gif' /></center><div>";
 			$(this).colorbox({
 	 			width:"800",
 	 			height:"500",
 	 			html:html,
 	 			escKey : false,
 	 			overlayClose : false,
 	 			close : false,
 	 			onLoad : function() { $('#cboxClose').remove(); setTimeout(function() { carregaBoletos(); },3000); }
 	 	 	});
		});
		$("#finalizar-boletos").click( function() {
			if(confirm("Deseja finalizar estes boletos?")) {
				var html = "<div class='load-boletos'><center>";
	 	 		html += "<h2 style='margin-top:200px'><span id='finalizar-boletos-aguarde'>Aguarde<span id='ret'></span></span></h2><br /><br />";
	 	 		html += "<img src='<?=$this->webroot?>img/load.gif' /></center><div>";
	 	 		$(this).colorbox({
	 	 			width:"800",
	 	 			height:"500",
	 	 			html:html,
	 	 			escKey : false,
	 	 			overlayClose : false,
	 	 			close : false,
	 	 			onLoad : function() { $('#cboxClose').remove(); finalizaBoletos(); }
	 	 	 	});
			}
		});
		$('span[id*="gerar-boletos-formando"]').click(function() {
			var formando = $(this).attr("id").substring(23);
			var html = "<div class='load-boletos'><center>";
 	 		html += "<h2 style='margin-top:200px'>Aguarde enquanto os boletos s&atilde;o gerados</h2><br /><br />";
 	 		html += "<img src='<?=$this->webroot?>img/load.gif' /></center><div>";
 			$(this).colorbox({
 	 			width:"800",
 	 			height:"500",
 	 			html:html,
 	 			escKey : false,
 	 			overlayClose : false,
 	 			close : false,
 	 			onLoad : function() { $('#cboxClose').remove(); setTimeout(function() { carregaBoletosFormando(formando); },3000); }
 	 	 	});
		});
		$('span[id*="finalizar-boletos-formando"]').click( function() {
			if(confirm("Deseja finalizar estes boletos?")) {
				var formando = $(this).attr("id").substring(27);
				var html = "<div class='load-boletos'><center>";
	 	 		html += "<h2 style='margin-top:200px'><span id='finalizar-boletos-aguarde'>Aguarde<span id='ret'></span></span></h2><br /><br />";
	 	 		html += "<img src='<?=$this->webroot?>img/load.gif' /></center><div>";
	 	 		$(this).colorbox({
	 	 			width:"800",
	 	 			height:"500",
	 	 			html:html,
	 	 			escKey : false,
	 	 			overlayClose : false,
	 	 			close : false,
	 	 			onLoad : function() { $('#cboxClose').remove(); finalizaBoletosFormando(formando); }
	 	 	 	});
			}
		});
 	});

 	function carregaBoletos() {
 	 	$("#gerar-boletos").colorbox.close();
 	 	window.open('<?="/{$this->params['prefix']}/area_financeira/gerar_boletos"?>');
 	}

 	function carregaBoletosFormando(formando) {
 		$("#gerar-boletos-formando-"+formando).colorbox.close();
 	 	window.open('<?="/{$this->params['prefix']}/area_financeira/gerar_boletos"?>/' + formando);
 	}

 	function finalizaBoletos() {
 		var intervalo = window.setInterval(ret, 1000);
 		var url = "/<?=$this->params["prefix"]?>/area_financeira/finaliza_geracao_boletos";
 		$.ajax({
 			url : url,
 			dataType : "json",
 			type : "POST",
 			success : function() {
 				clearInterval(intervalo);
 				$("#finalizar-boletos-aguarde").html('Boleto finalizados com sucesso!');
 				setTimeout(function() { window.location.reload(); },1000);
 			},
 			error : function() {
 				clearInterval(intervalo);
 				$("#finalizar-boletos-aguarde").html('Erro ao finalizar boletos. Tente novamente mais tarde');
 				setTimeout(function() { $("#finalizar-boletos").colorbox.close(); },2000);
 			}
 	 	});
 	}

 	function finalizaBoletosFormando(formando) {
 		var intervalo = window.setInterval(ret, 1000);
 		var url = "/<?=$this->params["prefix"]?>/area_financeira/finaliza_geracao_boletos/" + formando;
 		$.ajax({
 			url : url,
 			dataType : "json",
 			type : "POST",
 			success : function() {
 				clearInterval(intervalo);
 				$("#finalizar-boletos-aguarde").html('Boleto finalizados com sucesso!');
 				setTimeout(function() { window.location.reload(); },1000);
 			},
 			error : function() {
 				clearInterval(intervalo);
 				$("#finalizar-boletos-aguarde").html('Erro ao finalizar boletos. Tente novamente mais tarde');
 				setTimeout(function() { $("#finalizar-boletos").colorbox.close(); },2000);
 			}
 	 	});
 	}

 	function ret() {
 	 	var r = $("#ret");
 	 	if(r.html().length == 3)
 	 	 	r.html('');
	 	r.html(r.html() + '.');
 	}
</script>
<style type="text/css">
.load-boletos { width:100%; padding:0; background-color:white; height:466px; position:relative; overflow:hidden }
.load-boletos img { margin:0 auto }
</style>
<label id="conteudo-titulo" class="box-com-titulo-header">Lista de Formandos Para Gerar Boletos</label>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<div class="container-tabela">
		<table>
			<thead>
				<tr>
					<th scope="col">Cód. do Formando</th>
					<th scope="col">Nome</th>
					<th scope="col">Curso</th>
					<th scope="col">Turma</th>
					<th scope="col"></th>
				</tr>
			</thead>
			<tbody id='tbody'>
			<?php $isOdd = true; ?>
			<?php if(count($formandos) > 0) :?>
			<?php foreach($formandos as $formando): ?>
				<?php $codigoFormando = str_pad($formando["CursoTurma"]["turma_id"],4,'0',STR_PAD_LEFT) . str_pad($formando["FormandoProfile"]["codigo_formando"],3,'0',STR_PAD_LEFT)?>
				<?=$isOdd ? "<tr class='odd'>" : "<tr>"?>
					<td width="10%"><?=$codigoFormando; ?></td>
					<td><?=$formando['Usuario']['nome']; ?></td>
					<td><?=$formando["CursoTurma"]["Curso"]["nome"]?></td>
					<td><?=$formando["CursoTurma"]["Turma"]["nome"]?></td>
					<td width="23%">
						<span class='submit' id="gerar-boletos-formando-<?=$formando['Usuario']['id']?>">Gerar Boletos</span>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<span class='submit' id="finalizar-boletos-formando-<?=$formando['Usuario']['id']?>">Finalizar Boletos</span>
					</td>
				</tr>
			<?php $isOdd = !($isOdd)?>
			<?php endforeach; ?>
			<tr>
				<td colspan="3"></td>
				<td>
					<span class='submit button' id="gerar-boletos">Gerar Todos os Boletos</span>
					&nbsp;&nbsp;
					<span class='submit button' id="finalizar-boletos">Finalizar Todos os Boletos</span>
				</td>
			</tr>
			<?php  else : ?>
				<tr><td colspan="5"><h2>Nenhum boleto precisa ser gerado</h2></td></tr>
			<?php endif; ?>
			</tbody>
		</table>
	</div>
</div>