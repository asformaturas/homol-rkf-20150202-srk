<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css?v=0.1">
<style type="text/css">
    .input-control { width: 200px }
</style>
<script type="text/javascript">
$(document).ready(function() {
    var context = ko.contextFor($("#content-body")[0]);
    $('.preencher').click(function() {
        $('.dados-bancarios').fadeIn('slow');
        context.$data.loaded(true);
    });
    $("#form").submit(function(e) {
        id = "<?=$protocolo['Protocolo']['usuario_id']?>";
        e.preventDefault();
        var context = ko.contextFor($(".metro-button.reload")[0]);
        var dados = $("#form").serialize();
        var url = '<?="/{$this->params['prefix']}/area_financeira/dados_bancarios/"?>'+dir;
        context.$data.showLoading(function() {
            $.ajax({
                url: url,
                data: dados,
                type: "POST",
                dataType: "json",
                complete: function() {
                    context.$data.reload();
                }
            });
        });
    });
});
</script>
<?php $session->flash(); ?>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Devolucão Pagamentos
        </h2>
    </div>
</div>
<div class="row-fluid">
    <div class="span7">
        <table class="table table-condensed table-striped">
            <thead>
                <tr>
                    <th scope="col" colspan="9" style="text-align: center">Informações do Cancelamento</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td width="50%" class="strong item">Número Protocolo</td>
                    <td width="50%" style="text-align: center"><?=$protocolo['Protocolo']['protocolo']?></td>
                </tr>
                <tr>
                    <td width="50%" class="strong item">Status</td>
                    <td width="50%" style="text-align: center"><?=$protocolo['Protocolo']['status']; ?></td>
                </tr>
                <tr>
                    <td width="50%" class="strong item">Data Cadastro</td>
                    <td width="50%" style="text-align: center"><?=date('d/m/Y &\a\g\r\a\v\e;\s H:i:s',strtotime($protocolo['Protocolo']['data_cadastro']))?></td>
                </tr>
                <tr>
                    <td width="50%" class="strong item">Banco</td>
                    <td width="50%" style="text-align: center"><?=$protocolo['ProtocoloCancelamento']['banco'] == '' ? 'Campo não preenchido' : $protocolo['ProtocoloCancelamento']['banco']; ?></td>
                </tr>
                <tr>
                    <td width="50%" class="strong item">Agência</td>
                    <td width="50%" style="text-align: center"><?=$protocolo['ProtocoloCancelamento']['agencia'] == '' ? 'Campo não preenchido' : $protocolo['ProtocoloCancelamento']['agencia']; ?></td>
                </tr>
                <tr>
                    <td width="50%" class="strong item">Conta</td>
                    <td width="50%" style="text-align: center"><?=$protocolo['ProtocoloCancelamento']['conta'] == '' ? 'Campo não preenchido' : $protocolo['ProtocoloCancelamento']['conta']; ?></td>
                </tr>
                <tr>
                    <td width="50%" class="strong item">Nome Titular</td>
                    <td width="50%" style="text-align: center"><?=$protocolo['ProtocoloCancelamento']['nome_titular'] == '' ? 'Campo não preenchido' : $protocolo['ProtocoloCancelamento']['nome_titular']; ?></td>
                </tr>
                <tr>
                    <td width="50%" class="strong item">CPF Titular</td>
                    <td width="50%" style="text-align: center"><?=$protocolo['ProtocoloCancelamento']['cpf_titular'] == '' ? 'Campo não preenchido' : $protocolo['ProtocoloCancelamento']['cpf_titular']; ?></td>
                </tr>
                <tr>
                    <td width="50%" class="strong item">Valor a Devolver</td>
                    <td width="50%" style="color: green; text-align: center"><?=$protocolo['ProtocoloCancelamento']['valor_cancelamento']; ?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<br />
<?php if($protocolo['ProtocoloCancelamento']['banco'] == ''){?>
<div class="row-fluid">
    <div class="span10">
        <button class="mini default bg-color-red preencher" type="button">
            Preencher Dados
        </button> 
    </div>
</div>
<br />
<div class="row-fluid">
    <div class="span10 dados-bancarios" style="display: none" dir="<?=$protocolo['Protocolo']['usuario_id']?>">
	<?php echo $form->create('ProtocoloCancelamento', array('url' => "/{$this->params['prefix']}/area_financeira/dados_bancarios/", 'id' => 'form')); ?>
	<?=$form->hidden('ProtocoloCancelamento.protocolo_id',array('value' => $protocolo['Protocolo']['id'])); ?>
        <div class="row-fluid">
            <div class="span5">
                    <label>Banco</label>
                    <?php echo $form->input('banco', array('label' => false, 'div' => 'input-control', 'error' => false, 'maxlength' => 15, 'id' => 'banco')); ?>
            </div>
            <div class="span5">
                    <label>Agência</label>
                    <?php echo $form->input('agencia', array('label' => false, 'div' => 'input-control', 'error' => false, 'maxlength' => 5, 'id' => 'agencia')); ?>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span5">
                    <label>Conta</label>
                    <?php echo $form->input('conta', array('label' => false, 'div' => 'input-control', 'error' => false, 'maxlength' => 8, 'id' => 'conta')); ?>
            </div>
            <div class="span5">
                    <label>Nome Titular</label>
                    <?php echo $form->input('nome_titular', array('label' => false, 'div' => 'input-control', 'error' => false, 'id' => 'nome')); ?>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span5">
                    <label>CPF Titular</label>
                    <?php echo $form->input('cpf_titular', array('label' => false, 'div' => 'input-control', 'error' => false, 'maxlength' => 14, 'id' => 'cpf')); ?>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span10">
                    <?php echo $form->end(array('label' => 'Enviar', 'div' => false, 
                        'class' => 'button default bg-color-greenDark'));?>
            </div>
        </div>
    </div>
<?php } ?>
</div>




