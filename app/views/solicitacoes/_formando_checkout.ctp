<style type="text/css">
    #conteudo-container { margin:10px 0 0 0 !important; padding:0; width:100%!important }
    h2.titulo { background-color:#C7C5C6; text-align:left; color:black; padding:5px }
    #link-imprimir { display:none }
    div.break { page-break-before: always!important 
</style>
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>css/disable_view.css" media="print">
<script type="text/javascript">
    $(document).ready(function() {
        $('#salvar-checkout').click(function() {
            $.colorbox({
                width : 700,
                height: 600,
                html : '<p id="response" style="font-size:14px">Aguarde</p>',
                escKey : true,
                overlayClose : false,
                close : false,
                onLoad : function() { $('#cboxClose').hide(); },
                onComplete : function() {
                    var url = "/<?= $this->params["prefix"] ?>/checkout/marcar_itens_retirados";
                    var dados = {
                        data : {
                            usuario : '<?= $formando['id'] ?>',
                            protocolo : '<?=$protocolo['Protocolo']['id']?>',
                            itens : <?=json_encode($itens['a_retirar']); ?>
                        }
                    };
                    $.ajax({
                        url: url,
                        data: dados,
                        type: "POST",
                        dataType: "json",
                        success: function(response) {
                            if(response.erro) {
                                $("#response").html(response.mensagem);
                            } else {
                                dados.data.pagamentos = <?=json_encode($pagamentosCheckout); ?>;
                                var button = $("#link-imprimir").clone();
                                $("#response").html(button);
                                button.fadeIn(500);
                                button.click(function(e) {
                                    e.preventDefault();
                                    button.text('Aguarde');
                                    var url = "/<?= $this->params["prefix"] ?>/solicitacoes/imprimir_checkout/"+dados.data.protocolo;
                                    $.ajax({
                                        url: url,
                                        data: dados,
                                        type: "POST",
                                        success: function(response) {
                                            var url = '<?=$this->here?>';
                                            window.document.write(response);
                                            window.print();
                                            window.document.location.href = url;
                                        },
                                        error: function() {
                                            $("#response").html('Erro ao processar solicita&ccedil;&atilde;o' +
                                                "<br />Tente novamente");
                                        }
                                    });
                                });
                            }
                        },
                        error: function() {
                            $("#response").html('Erro ao processar solicita&ccedil;&atilde;o' +
                                "<br />Tente novamente");
                        },
                        complete: function() {
                            $('#cboxClose').show();
                        }
                    });
                }
            });
        });
    });
</script>
<span id="conteudo-titulo" class="box-com-titulo-header">
    Checkout de Formando
</span>
<div id="conteudo-container" style="position: relative">
    <?php
    if (count($itens['a_retirar']) > 0) :
        echo $html->link('IMPRIMIR PROTOCOLO', array($this->params['prefix'] => true, 'controller' => 'solicitacoes', 'action' =>
            'checkout_imprimir', $protocolo['Protocolo']['protocolo']), array('class' => 'button submit', 'id' => 'link-imprimir',
            'target' => '_blank'));
    endif;
    ?>
    <div style="overflow:hidden; width:100%; position:relative" class="clearfix">
        <div class="legenda" style="width:97%; margin:20px 0 30px 0; border:1px solid #989999; position:relative">
            <div style='color:#AA0000'>
                <img src="<?= empty($formando['diretorio_foto_perfil']) ? "{$this->webroot}img/uknown_user.gif" : "{$this->webroot}{$formando['diretorio_foto_perfil']}" ?>"
                     style='float:left; margin:0 5px 15px 0; width:120px' />
                <p>
                    <strong style='text-transform:uppercase'><?= $formando['nome'] ?></strong>
                    <br />
                    <em><?= $formando['grupo'] ?></em>
                    <br />
                    <em>Código <?= $formando['codigo_formando'] ?></em>
                    <br />
                    <em>RG <?= $formando['rg'] ?></em>
                    <br />
                    <em>Protocolo <?= $protocolo['Protocolo']['protocolo'] ?></em>
                </p>
            </div>
            <p>
                Eu, <em><?= $usuarioCriador['Usuario']['nome'] ?></em>, confirmo que o formando, <em><?= $formando['nome'] ?></em>,
            <br />quitou suas pendências financeiras e têm direito a retirada dos seguintes itens para festa de formatura em nome da <strong>
                RK Formaturas</strong>:
            </p>
            <table>
                <tr>
                    <td colspan="2" width="50%"><h2 class='titulo'>Informa&ccedil;&otilde;es da ades&atilde;o</h2></td>
                    <td colspan="2" width="50%"><h2 class='titulo'>Resumo financeiro</h2></td>
                </tr>
                <tr height="5">
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td width="200">Data contrato da Turma</td>
                    <td><?= date('d/m/Y', strtotime($turma['Turma']['data_assinatura_contrato'])); ?></td>
                    <td>Valor de Contrato</td>
                    <td class='valor-contrato'>R$<?= number_format($formando['valor_adesao'], 2, ',', '.'); ?></td>
                </tr>
                <tr>
                    <td>Data Adesão</td>
                    <td><?= date('d/m/Y', strtotime($formando['data_adesao'])); ?></td>
                    <td>Valor de IGPM</td>
                    <td><?= $totalIGPM > 0 ? "R$" . number_format($totalIGPM, 2, ',', '.') : "-"; ?></td>
                </tr>
                <tr>
                    <td>Data IGPM</td>
                    <td><?= !empty($turma['Turma']['data_igpm']) ?
                        date('d/m/Y', strtotime($turma['Turma']['data_igpm'])) : "IGPM não aplicado";?>
                    </td>
                    <td>Valor de Contrato c/ Correção</td>
                    <td class='total-contrato'>R$<?= number_format($formando['valor_adesao'] + $totalIGPM, 2, ',', '.'); ?></td>
                </tr>
                <tr>
                    <td>Mesas do contrato</td>
                    <td><?= $turma['Turma']['mesas_contrato']; ?></td>
                    <td>Valor de Campanhas</td>
                    <td class='total-campanha'><?= $totalDespesasExtras > 0 ? "R$" . number_format($totalDespesasExtras, 2, ',', '.') : "-" ?></td>
                </tr>
                <tr>
                    <td>Convites do contrato</td>
                    <td><?= $turma['Turma']['convites_contrato']; ?></td>
                    <td>Valor de Itens do Checkout</td>
                    <td><?= $totalItensCheckout > 0 ? "R$" . number_format($totalItensCheckout, 2, ',', '.') : "-" ?></td>
                </tr>
                <tr>
                    <td>Número de Parcelas do Contrato</td>
                    <td><?= $formando['parcelas_adesao']; ?></td>
                    <td colspan='2'>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan='2'>&nbsp;</td>
                    <td>Total Recebido Antes de Checkout</td>
                    <td><?= $totalPago > 0 ? "R$" . number_format($totalPago, 2, ',', '.') : "-" ?></td>
                </tr>
                <tr>
                    <td colspan='2'></td>
                    <td>Total Recebido no Checkout</td>
                    <td><?= $totalPagoCheckout > 0 ? "R$" . number_format($totalPagoCheckout, 2, ',', '.') : "-" ?></td>
                </tr>
                <tr>
                    <td colspan='2'>&nbsp;</td>
                    <td>Saldo Final</td>
                    <td><?= $saldoTotal != 0 ? "R$" . number_format($saldoTotal, 2, ',', '.') : "-" ?></td>
                </tr>
                <tr height="15">
                    <td colspan="4"></td>
                </tr>
                <tr>
                    <td colspan="4"><h2 class='titulo'>Itens a Retirar</h2></td>
                </tr>
                <tr height="5">
                    <td colspan="4"></td>
                </tr>
                <?php if (count($itens['a_retirar']) > 0) : ?>
                <?php foreach ($itens['a_retirar'] as $itemARetirar) : ?>
                <tr class="itens-a-retirar">
                    <td colspan="4"><?= $itemARetirar['descricao'] ?><br />&nbsp;</td>
                </tr>
                <?php endforeach; ?>
                <?php else : ?>
                <tr>
                    <td colspan="4"><h3>Todos os itens foram retirados</h3></td>
                </tr>
                <?php endif; ?>
                <tr height="20">
                    <td colspan="4"></td>
                </tr>
                <tr>
                    <td colspan="4"><h2 class='titulo'>Itens Retirados</h2></td>
                </tr>
                <tr height="5">
                    <td colspan="4"></td>
                </tr>
                <?php if(count($itens['retirados']) > 0) : ?>
                <?php foreach ($itens['retirados'] as $item) : ?>
                <tr class="itens-retirados">
                    <td colspan="4"><?= $item['descricao'] ?><br />&nbsp;</td>
                </tr>
                <?php endforeach; ?>
                <?php else : ?>
                <tr class="nenhum-item-retirado">
                    <td colspan="4"><h3>Nenhum item foi retirado</h3></td>
                </tr>
                <?php endif; ?>
                <?php if ($this->params['prefix'] == "atendimento") : ?>
                <tr>
                    <td colspan="4">
                        <br />
                        <br />
                        <?php if (count($itens['a_retirar']) > 0) : ?>
                        <a class="submit button" style='text-transform:uppercase'
                            id='salvar-checkout'>
                            Salvar e Imprimir Protocolo
                        </a>
                        <?php else : ?>
                        <a class="submit button" style='text-transform:uppercase' target="_blank"
                            href="/<?= $this->params["prefix"] ?>/solicitacoes/imprimir_checkout/<?=$protocolo['Protocolo']['id']?>">
                            Imprimir Protocolo
                        </a>
                        <?php endif; ?>
                    </td>
                </tr>
                <?php endif; ?>
            </table>
        </div>
    </div>
</div>