<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootstrap/select.css">
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/bootstrap/datepicker.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/form_validate.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/wizard.css">
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/transition.js?v=0.1"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/modal.js?v=0.1"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/bootbox.js?v=0.2"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/utils.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/bootstrap/datepicker.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/max/bootstrap/wizard.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/max/knockout/checkout.js"></script>
<style type="text/css">
.img-center {
    display:block;
    width:60%;
    height:auto;
    margin: 10px auto;
    border-radius:4px;
}
.border { border-bottom:1px solid #d4d4d4 }
.nome {
    font-size: 15px;
    text-align: center;
    margin-bottom: 5px;
}
.codigo-formando { font-size: 12px; }
.resumo-checkout {
    float:left;
    width:100%;
    margin:15px 0;
    padding:0;
    list-style-type: none;
}
.resumo-checkout li {
    float:left;
    width:50%;
    margin:2px 0;
    padding: 3px;
    box-sizing: border-box;
    font-size: 14px;
    line-height: 15px;
}
.resumo-checkout li:nth-child(odd) {
    padding-right: 10px;
    text-align: right;
    font-size: 13px;
    color:#999
}
.resumo-checkout li i { vertical-align: middle }
</style>
<script type="text/javascript">
$(document).ready(function(){
    $('#wizard').wizard();
    
    $('#salvar-checkout').click(function(){
        var url = "/<?= $this->params["prefix"] ?>/checkout/marcar_itens_retirados";
        var dados = {
            data : {
                usuario : '<?= $formando['id'] ?>',
                protocolo : '<?=$protocolo['Protocolo']['id']?>',
                itens : <?=json_encode($itens['a_retirar']); ?>
            }
        };
        $.ajax({
            url: url,
            data: dados,
            type: "POST",
            dataType: "json",
            success: function(response) {
                if(response.erro) {
                    alert('erro');
                } else {
                    var url = "/<?= $this->params["prefix"] ?>/solicitacoes/checkout/<?=$protocolo['Protocolo']['protocolo']?>";
                    window.document.location.href = url;
                }
            }
        }); 
    });
});
</script>
<div class="row-fluid" id="checkout">
    <div class="span3 wizzard">
        <div class="row-fluid border">
            <img src="/<?=$formando['diretorio_foto_perfil']?>"
                onerror="this.src='/img/no-image.gif'"
                class="img-center" />
            <p class="nome fg-color-red">
                <?=$formando['nome']?>
                <br />
                <em class="codigo-formando fg-color-gray">
                    <?=$formando['codigo_formando']?> / <?=ucfirst($formando['grupo'])?>
                </em>
            </p>
        </div>
    </div>
    <div class="span9">
        <div id="wizard" class="wizard">
            <ul class="steps">
                <li data-target="#step1" class="active cadastro">
                    Protocolo
                    <span class="chevron"></span>
                </li>
            </ul>
        </div>
        <div class="step-content">
            <div class="step-pane active" id="step1">
                <h2 class="fg-color-red">Protocolo Checkout</h2>
                <br />
                <p class="border">
                    Eu, <em class="fg-color-red"><strong><?= $usuarioCriador['Usuario']['nome'] ?></strong></em>, confirmo que o formando, <em class="fg-color-gray"><strong><?= $formando['nome'] ?></strong></em>,
                    quitou suas pendências financeiras e têm direito a retirada dos seguintes itens para festa de formatura em nome da <strong>
                    RK Formaturas</strong>:
                </p>
                <br />
                <table class="table table-condensed table-striped">
                    <tr>
                        <td colspan="2" width="50%"><h3 class="fg-color-gray">Informa&ccedil;&otilde;es da ades&atilde;o</h3></td>
                        <td colspan="2" width="50%"><h3 class="fg-color-gray">Resumo financeiro</h3></td>
                    </tr>
                    <tr>
                        <td width="200">Data contrato da Turma</td>
                        <td><?= date('d/m/Y', strtotime($turma['Turma']['data_assinatura_contrato'])); ?></td>
                        <td>Valor de Contrato</td>
                        <td class='valor-contrato'>R$<?= number_format($formando['valor_adesao'], 2, ',', '.'); ?></td>
                    </tr>
                    <tr>
                        <td>Data Adesão</td>
                        <td><?= date('d/m/Y', strtotime($formando['data_adesao'])); ?></td>
                        <td>Valor de IGPM</td>
                        <td><?= $totalIGPM > 0 ? "R$" . number_format($totalIGPM, 2, ',', '.') : "-"; ?></td>
                    </tr>
                    <tr>
                        <td>Data IGPM</td>
                        <td><?= !empty($turma['Turma']['data_igpm']) ?
                            date('d/m/Y', strtotime($turma['Turma']['data_igpm'])) : "IGPM não aplicado";?>
                        </td>
                        <td>Valor de Contrato c/ Correção</td>
                        <td class='total-contrato'>R$<?= number_format($formando['valor_adesao'] + $totalIGPM, 2, ',', '.'); ?></td>
                    </tr>
                    <tr>
                        <td>Mesas do contrato</td>
                        <td><?= $turma['Turma']['mesas_contrato']; ?></td>
                        <td>Valor de Campanhas</td>
                        <td class='total-campanha'><?= $totalDespesasExtras > 0 ? "R$" . number_format($totalDespesasExtras, 2, ',', '.') : "-" ?></td>
                    </tr>
                    <tr>
                        <td>Convites do contrato</td>
                        <td><?= $turma['Turma']['convites_contrato']; ?></td>
                        <td>Valor de Itens do Checkout</td>
                        <td><?= $totalItensCheckout > 0 ? "R$" . number_format($totalItensCheckout, 2, ',', '.') : "-" ?></td>
                    </tr>
                    <tr>
                        <td>Número de Parcelas do Contrato</td>
                        <td><?= $formando['parcelas_adesao']; ?></td>
                        <td colspan='2'>&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan='2'>&nbsp;</td>
                        <td>Total Recebido Antes do Checkout</td>
                        <td><?= $totalPago > 0 ? "R$" . number_format($totalPago, 2, ',', '.') : "-" ?></td>
                    </tr>
                    <tr>
                        <td colspan='2'></td>
                        <td>Total Recebido no Checkout</td>
                        <td><?= $totalPagoCheckout > 0 ? "R$" . number_format($totalPagoCheckout, 2, ',', '.') : "-" ?></td>
                    </tr>
                    <tr>
                        <td colspan='2'>&nbsp;</td>
                        <td>Saldo Final</td>
                        <td><?= $saldoTotal != 0 ? "R$" . number_format($saldoTotal, 2, ',', '.') : "-" ?></td>
                    </tr>
                </table>
                <br />
                <br />
                <table class="table table-condensed table-striped">
                    <tr>
                        <td colspan="4"><h3 class="fg-color-gray">Itens a Retirar:</h3></td>
                    </tr>
                    <?php if (count($itens['a_retirar']) > 0) : ?>
                    <?php foreach ($itens['a_retirar'] as $itemARetirar) : ?>
                    <tr class="itens-a-retirar">
                        <td colspan="4"><?= $itemARetirar['descricao'] ?><br />&nbsp;</td>
                    </tr>
                    <?php endforeach; ?>
                    <?php else : ?>
                    <tr>
                        <td colspan="4">Todos os itens foram retirados.</td>
                    </tr>
                    <?php endif; ?>
                </table>
                <br />
                <br />
                <table class="table table-condensed table-striped">
                    <tr>
                        <td colspan="4"><h3 class="fg-color-gray">Itens Retirados</h3></td>
                    </tr>
                    <?php if(count($itens['retirados']) > 0) : ?>
                    <?php foreach ($itens['retirados'] as $item) : ?>
                    <tr class="itens-retirados">
                        <td colspan="4"><?= $item['descricao'] ?><br />&nbsp;</td>
                    </tr>
                    <?php endforeach; ?>
                    <?php else : ?>
                    <tr class="nenhum-item-retirado">
                        <td colspan="4"><h4>Nenhum item foi retirado</h4></td>
                    </tr>
                    <?php endif; ?>
                </table>
                    <?php if ($this->params['prefix'] == "atendimento") : ?>
                        <div class="row-fluid">
                            <br />
                            <?php if (count($itens['a_retirar']) > 0) : ?>
                            <button class="button bg-color-greenDark pull-right" id="salvar-checkout">
                                Salvar
                            </button>
                            <?php else : ?>
                            <a class="button bg-color-greenDark pull-left" target="_blank"
                                href="/<?= $this->params["prefix"] ?>/solicitacoes/imprimir_checkout/<?=$protocolo['Protocolo']['id']?>/atendimento">
                                Via Atendimento
                            </a>
                            <a class="button bg-color-greenDark pull-right" target="_blank"
                                href="/<?= $this->params["prefix"] ?>/solicitacoes/imprimir_checkout/<?=$protocolo['Protocolo']['id']?>/formando">
                                Via Formando
                            </a>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
            </div>
        </div>
    </div>
</div>