
<h1 class="grid_16 first" style="margin-top: 20px;border-bottom: 1px solid #000;">Resumo financeiro<?php if ($this->params['action'] == 'planejamento_checkout' || $this->params['action'] == 'formando_checkout') { ?>	 				<a style="float:right;font-size:14px; margin-top:10px;">exibir<?php } ?></a><div style="clear: both;"></div></h1>
	
<div class="detalhes grid_16 first conteudo-container" style="margin-top:10px;padding-left: 10px;">
	<p class="grid_16 alpha omega first">
		<span class="grid_full alpha omega"> Total de despesas com Ades&atilde;o e Campanhas: <span style="color:red;">
			R$ <?php $totalDevidoFinal = $totalDeAdesao['valorFinalDeDespesas'] + $totalDeExtra['valorFinalDeDespesas']; echo $totalDeAdesao['valorFinalDeDespesas']." + R$ ".$totalDeExtra['valorFinalDeDespesas']." = R$ ".$totalDevidoFinal;?> </span></span>
	</p>
	<p class="grid_16 alpha omega first">
		<span class="grid_full alpha omega"> Total de pagamentos: <span style="color:blue;">R$ <?php echo $totalPago;?> </span></span>
	</p>
	<p class="grid_16 alpha omega first">
		<span class="grid_full alpha omega"> <b>Saldo(Total de pagamentos - Total de despesas): </b>
			<?php $saldoFinal = $totalPago - $totalDevidoFinal;?>
			<span style="color:blue;">R$ <?php echo $totalPago; ?></span> - 
			<span style="color:red;">R$ <?php echo $totalDevidoFinal; ?></span> = 
			<span style="color:<?php if ($saldoFinal >= 0) echo "blue;"; else echo "red";?>;"> 
				R$ <?php echo $saldoFinal;?> 
			</span>
		</span>
	</p>
</div>
