<script type="text/javascript">
	var indiceDePagamentos = 0;
	
	var valorAdesaoTotal 	= 0;
	var valorAdesaoPago 	= 0;
	var valorAdesaoAberto	= 0;
	var valorAdesaoCredito	= 0; <?php // valor de crédito utilizado na adesão ?>
	
	var valorExtrasTotal	= 0; <?php  // valor total de despesas com extras, exceto despesas canceladas ?>
	var valorExtrasPago		= 0;
	var valorExtrasAberto	= 0;
	var valorExtrasCredito	= 0; <?php // valor de crédito utilizado nos extras ?>
	
	var valorSaldoAberto		= 0; <?php // Total de despesas a pagar ?>
	var valorCreditoDisponivel	= 0; <?php // Total de crédito a utilizar ?>
	var valorCreditoUtilizado	= 0; <?php // Valor total de crédito a ser utilizado ?>
	var valorSaldoFinal			= 0; <?php // Saldo final, considerando crédito utilizado e pagamenos inseridos na tela ?>

	
	function roundNumber(num, dec) {
		numero = parseFloatCustom(num);
		if(numero != 0) {
			putlog("#@ arredondando : " + numero);
			numero = parseFloat(numero).toFixed(dec);
		} else {
			putlog("#!@ nao arredondando.. zero!");
		}
		return numero;
	}
	
	
	function parseFloatCustom( texto ) {
		putlog("Entrando parseFloatCustom: " + texto);
		var negativo = false;
		texto = texto.toString();
		if(texto.indexOf("-") == 0) {
			negativo = true;
			//remover sinal de negativo
			texto = texto.substr(1);
		}
		
		texto = texto.replace("R$","");
		
		if(texto.indexOf(",") != -1)
			texto = texto.replace(".","").replace(",",".");
		putlog("Fazendo parse do texto: " + texto);
		
		resp = parseFloat(texto).toFixed(2);
		
		if(negativo) resp = -resp; 
		
		putlog("Resposta = " + resp + "\n-----\n")
		return resp;
	}
	
	$(function() {
		$(".checkout-button").click(function() { 
			saldoFinal = parseFloatCustom($("#saldo-final").html());

			if (saldoFinal < 0) {
				alert('Não é possível realizar checkout pois o saldo não é maior ou igual a zero.');
				return false;
			}
			
			if (confirm("ATENÇÃO: Ao realizar o checkout todas as despesas do formando serão marcadas como PAGAS e o crédito será zerado.")) {
				$('#form-checkout').submit();
			}
			return false;
		});
		
		$(".botao-pagar-despesa").click(function() { 
			idDaDespesa = $(this).parents("tr").children("td:first-child").html();
			return confirm("Tem certeza que deseja marcar a despesa " + idDaDespesa + " como PAGA(valor base, multa e igpm)?");
		});
		
		
		$(".botao-cancelar-despesa").click(function() { 
			idDaDespesa = $(this).parents("tr").children("td:first-child").html();
			return confirm("Tem certeza que deseja CANCELAR a despesa " + idDaDespesa + "?");
		});

		$(".botao-ativar-despesa").click(function() { 
			idDaDespesa = $(this).parents("tr").children("td:first-child").html();
			return confirm("Tem certeza que deseja REATIVAR a despesa " + idDaDespesa + "?");
		});
	});

	function atualizarSaldoPorNovoValorDePagamento(inputModificado) {
		var valorAntigo = parseFloatCustom(inputModificado.defaultValue.replace(',','.'));
		var valorNovo = parseFloatCustom(inputModificado.value.replace(',','.'));
		inputModificado.defaultValue = inputModificado.value;
		
		saldoFinal = parseFloatCustom($("#saldo-final").html());

		$("#saldo-final").html(roundNumber(saldoFinal - valorAntigo + valorNovo,2));
		putlog("atualizando Saldo por Novo Valor de Pagamento " + valorAntigo + " | " + valorNovo);
		
	}
	
	function putlog(text) {
		$("#mylog")[0].value += text + "\n";
	}
	
	function adicionarPagamento(valor, data, tipo) {
		htmlDaNovaLinha = "<tr>";
		htmlDaNovaLinha += "	<td width='10%'><input type='text' name='valorPagamento[" + indiceDePagamentos + "]' class='inputValorDoPagamento' value='" + (valor + "").replace('.', ',') + "' size=6 onchange='atualizarSaldoPorNovoValorDePagamento(this)'></input></td>";
		htmlDaNovaLinha += "	<td width='10%'><input type='text' name='dataPagamento[" + indiceDePagamentos + "]' class='inputDataDoPagamento' value='" + data + "' size=12></input></td>";
		if (tipo == 'cheque') 
			htmlDaNovaLinha += "	<td width='10%'><select name='tipoPagamento[" + indiceDePagamentos + "]'><option value='dinheiro'>Dinheiro</option><option value='cheque' selected=true>Cheque</option></select></td>";
		else		
			htmlDaNovaLinha += "	<td width='10%'><select name='tipoPagamento[" + indiceDePagamentos + "]'><option value='dinheiro'>Dinheiro</option><option value='cheque'>Cheque</option></select></td>";
		
		htmlDaNovaLinha += "	<td width='65%'><textarea type='text' name='obsPagamento[" + indiceDePagamentos + "]' style='width:100%;resize: none; height: 50px;'></textarea></td>";
		htmlDaNovaLinha += "	<td width='5%'><a href='#' class='submit button' onclick='removerLinhaDePagamento(this);return false;'>remover</a></td>";
		htmlDaNovaLinha += "</tr>";
		$(htmlDaNovaLinha).appendTo('#body-tabela-pagamentos');
		indiceDePagamentos++;

 		$(".inputDataDoPagamento").datepicker( {dateFormat: 'dd/mm/yy'} );

		
 		saldoFinal = parseFloatCustom($("#saldo-final").html());
		
		$("#saldo-final").html("R$" + roundNumber(saldoFinal + parseFloatCustom(valor), 2));
		
	}
	
 	jQuery(function($) {
 		$("#botao-adicionar-pagamento").click(function() { 
 			adicionarPagamento(0, '<?php echo date('d/m/Y', strtotime("NOW")); ?>');

			return false;
		});

		$('#utilizar-valores-sugeridos').click(function() { 
			var primeiraParcela = true;
			$('.parcela-sugerida').each(function() {
				dataDaParcela = $(this).children(':nth-child(2)').html();
				valorDaParcela = parseFloatCustom($(this).children(':nth-child(3)').html()).toFixed(2);
				
				if (primeiraParcela) {
					adicionarPagamento(valorDaParcela, '<?php echo date('d/m/Y', strtotime("NOW")); ?>');
					primeiraParcela = false;
				} else {
					adicionarPagamento(valorDaParcela, dataDaParcela, 'cheque');
				}
			});
 			
			return false;
		});

		$('.utilizar-credito').click(function() { 			
			putlog("# Iniciando utilizar-credito");
			var valorDaDivida = parseFloatCustom($(this).parents('tr').children('.valor-devido').html());
			var creditoDisponivel = parseFloatCustom($("#saldo-credito-acumulado").html());
			
			if(creditoDisponivel <= 0) {
				alert("Não há crédito disponível");
				return false;
			}
			
			putlog("valorDaDivida = " + valorDaDivida + "| creditoDisponivel = " + creditoDisponivel);
			
			var novoValorDaDivida;
			var novoCredito;
			if (creditoDisponivel >= valorDaDivida) {
				putlog(" credito é maior que a divida");
				novoCredito = creditoDisponivel - valorDaDivida;
				novoValorDaDivida = 0
			} else {
				putlog(" credito é menor que a dívida");
				novoCredito = 0;
				novoValorDaDivida = valorDaDivida - creditoDisponivel;
			} 
			//atualiza credito disponivel
			putlog("# UC : 1");
			$('#saldo-credito-acumulado').html(roundNumber(novoCredito,2));
			putlog("# UC : 2");
			//atualiza divida
			$(this).parents('tr').children('.valor-devido').html(roundNumber(novoValorDaDivida, 2));
			putlog("# UC : 3");
			//atualiza o total devido
			if ($(this).parents('tr').children('.valor-devido').is('.valor-devido-igpm'))  {
				putlog("Entrou #1");
				novoSaldoTotal = parseFloatCustom($('#total-devido-adesao').attr('value'));
				putlog("novoSaldoTotal1 = " + novoSaldoTotal);
				novoSaldoTotal = novoSaldoTotal - valorDaDivida + novoValorDaDivida;
				putlog("novoSaldoTotal2 = " + novoSaldoTotal);
				$('#total-devido-adesao').attr('value', roundNumber(novoSaldoTotal,2));

			} else {
				putlog("Entrou #2");
				novoSaldoTotal = parseFloatCustom($('#total-devido-campanhas').attr('value'));
				novoSaldoTotal += parseFloatCustom($('#total-devido-adesao').attr('value'));
				putlog("novoSaldoTotal1 = " + novoSaldoTotal);
				novoSaldoTotal = novoSaldoTotal - valorDaDivida + novoValorDaDivida;
				putlog("novoSaldoTotal2 = " + novoSaldoTotal);
				
				novoTotalDevidoCampanhas = parseFloatCustom($('#total-devido-campanhas').attr('value'));
				putlog("novoTotalDevidoCampanhas = " + novoTotalDevidoCampanhas);
				novoTotalDevidoCampanhas = novoTotalDevidoCampanhas - valorDaDivida + novoValorDaDivida;
				putlog("novoTotalDevidoCampanhas2 = " + novoTotalDevidoCampanhas);
				$('#total-devido-campanhas').attr('value', novoTotalDevidoCampanhas,2);	
			}

			//atualiza a tabela de saldo em aberto
			novoSaldoTotalAberto = parseFloatCustom($('#total-devido-adesao').attr('value')) + parseFloatCustom($('#total-devido-campanhas').attr('value'));
			$("#saldo-total-em-aberto").html(roundNumber(novoSaldoTotalAberto, 2));


			// 
			saldoFinal = $("#saldo-final").html();
			putlog("saldoFinal1 = " + saldoFinal);
			
			strSaldoFinal = saldoFinal.toString();
			var floatSaldoFinal;
			
			if(strSaldoFinal.indexOf("-") == 0 ) {
				// É negativo
				strSaldoFinal = strSaldoFinal.substr(1).replace(".","");
				strSaldoFinal = strSaldoFinal.replace(",",".");
				putlog("strSaldoFinal = " + strSaldoFinal);
				floatSaldoFinal = - parseFloatCustom(strSaldoFinal);
				putlog("floatSaldoFinal = " + floatSaldoFinal);
				floatSaldoFinal += valorDaDivida - novoValorDaDivida;
				putlog("floatSaldoFinal4 = " + floatSaldoFinal);
				//floatSaldoFinal 
			}
			$("#saldo-final").text("R$" + roundNumber(floatSaldoFinal,2));

			//atualiza parcelas
			atualizarParcelasSugeridasPara($('#select-numero-de-parcelas-adesao').attr('value'));
			return false;

			
			return false;

		});
		
		$('#select-numero-de-parcelas-adesao').change(function() {
			atualizarParcelasSugeridasPara(this.value);
		});
 	 });

	 function atualizarParcelasSugeridasPara(numeroDeVezes) {
		var totalDevidoAdesao = parseFloatCustom($("#total-devido-adesao").attr('value'));
		var totalDevidoCampanhas = parseFloatCustom($("#total-devido-campanhas").attr('value'));

		var primeiraParcelaAVista = roundNumber(totalDevidoCampanhas + totalDevidoAdesao/parseFloatCustom(numeroDeVezes),2);

		var dataAtual = x = new Date ( <?php echo date('Y', strtotime("NOW")); ?>, <?php echo date('m', strtotime("NOW")); ?>, <?php echo date('d', strtotime("NOW")); ?> );
		var htmlDaNovaLinha = "";
		
		var cont = 1;
		
		$('.parcela-sugerida').remove();
		if (totalDevidoAdesao >  0)
			for (var numeroParcela = 0; numeroParcela < numeroDeVezes; numeroParcela++) {
				htmlDaNovaLinha += "<tr>";
				htmlDaNovaLinha += "<tr class='parcela-sugerida'>";	
				if (numeroParcela == 0) {
					htmlDaNovaLinha += "<td width='10%'>" + cont + "</td>";
					htmlDaNovaLinha += "<td width='40%'> À Vista</td>";
					htmlDaNovaLinha += "<td width='40%'>" + primeiraParcelaAVista + "</td>	";
				} else {
					htmlDaNovaLinha += "<td width='10%'>" + cont + "</td>";
					htmlDaNovaLinha += "<td width='40%'>" + dataAtual.getDate() + "/" + (dataAtual.getMonth() + numeroParcela) + "/" + dataAtual.getFullYear() + "</td>";
					htmlDaNovaLinha += "<td width='40%'>" + roundNumber(totalDevidoAdesao/parseFloatCustom(numeroDeVezes),2) + "</td>	";
				}
				htmlDaNovaLinha += "</tr>";
				cont++;
			}
		else {
			htmlDaNovaLinha += "<tr>";
			htmlDaNovaLinha += "<tr class='parcela-sugerida'>";	

			htmlDaNovaLinha += "<td width='60%'>À Vista</td>";
			htmlDaNovaLinha += "<td width='40%'> R$ " + primeiraParcelaAVista + "</td>	";

			htmlDaNovaLinha += "</tr>";
		}
		
		$('#body-tabela-valores-recomendados').prepend(htmlDaNovaLinha);
	 }

	function removerLinhaDePagamento(botaoDeRemocao) {
		var numeroBrasileiro = $(botaoDeRemocao).parents('tr').children('td').children('input.inputValorDoPagamento').attr('value');
		var valorDoPagamento = parseFloatCustom(numeroBrasileiro.replace(',', '.'));

		$(botaoDeRemocao).parents('tr').remove();
		
		saldoFinal = parseFloatCustom($("#saldo-final").html());
				
		$("#saldo-final").html(roundNumber(saldoFinal - valorDoPagamento,2));
		
		return false;
	}

	function clickDoBotaoSalvarPagamentos() {
		numeroDePagamentosASeremAdicionados = $('#body-tabela-pagamentos').children().length;
		if (numeroDePagamentosASeremAdicionados == 0) {
			alert('Voce nao adicionou nenhum pagamento para ser salvo.');
			return false;
		}
		return true;
	}

	jQuery(function($) {
		$('.conteudo-container').hide();
		$('#secao-flash a').click(function() {
			idDaReferencia = $(this).attr('href');
			$divDaSecaoASeExibida = $('' + idDaReferencia).parents('.conteudo-container');
			$divDaSecaoASeExibida.show();
			$divDaSecaoASeExibida.prev().children('a').html('ocultar');	
		});

		$('h1 > a').click(function () {
			var divConteudoDaSecao = $(this).parent().next();
			
			if ($(divConteudoDaSecao).is(':visible')) {
				$(divConteudoDaSecao).fadeOut();
				$(this).html('exibir');	
			} else {
				$(divConteudoDaSecao).fadeIn();
				$(this).html('ocultar');	
			}	
		  return false;
		});
	
	});


	
</script>

<?php

// Armazena a soma de todas as despesas pagas (não conta as canceladas) , usado para calcular o crédito disponível
$totalPagoGeral = 0;

?>

<span id="conteudo-titulo" class="box-com-titulo-header">Checkout</span>
	<div class="grid_16 first" id="secao-flash">	
	<?php $session->flash(); ?>
	</div>
	
<?php if ($this->params['action'] == 'planejamento_checkout' || $this->params['action'] == 'formando_checkout') { ?>
	<div style="margin:5px 0 5px 0;" >
		<?php echo $html->link('Imprimir',array($this->params['prefix'] => true, 'action' => 'checkout_imprimir', $formando['FormandoProfile']['id']),array('class' => 'submit', 'style' => 'float: right; height:10px;', 'target' => '_blank'));?>
	</div>
<?php }?>

<table class="grid_full alpha omega first" > <tr> <td>
	<?php echo $form->textarea("mylog", array('rows' => 10, 'cols' => 100)); ?>
</td></td></table>

<!--- Seção Dados Pessoais e de Adesão -->
<?php include('_checkout_dados_pessoais_e_adesao.ctp'); ?>

<!--- Seção Aderir Campanhas Finais -->
<?php 
if ($this->params['action'] == 'planejamento_checkout') {
	include('_checkout_novas_adesoes.ctp'); 
}
?>

<!-- Seção Listagem de Campanhas Aderidas -->
<?php include('_checkout_campanhas.ctp'); ?>

<!-- Secao Pagamentos -->
<?php include('_checkout_pagamentos.ctp'); ?>

<!-- Secao Saldo -->
<?php 
if ($this->params['action'] == 'planejamento_checkout') {
	include('_checkout_saldo.ctp');
}
?>

<!-- Secao Resumo Financeiro -->
<?php include('_checkout_resumo_financeiro.ctp'); ?>

<?php if ($this->params['action'] == 'planejamento_checkout') { ?>
<p class="grid_full alpha omega" style="margin: 20px 0px; text-align:right;">
 	<?php echo $html->link('Realizar Checkout', array($this->params['prefix'] => true, 'controller' => 'formandos', 'action' =>'checkout_confirmar', $formando['FormandoProfile']['id']), array('class' => 'submit button checkout-button')); ?>
</p>
<?php }?>
