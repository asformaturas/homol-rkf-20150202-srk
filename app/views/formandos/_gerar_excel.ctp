<?php 
	$listaDeFormandosParaExcel = array();
	foreach ($formandos as $formando)
		$listaDeFormandosParaExcel[] = array(
				"Codigo do formando" => $formando['ViewFormandos']['codigo_formando'],
				"Nome" =>  $formando['ViewFormandos']['nome'],
				"Email" => $formando['ViewFormandos']['email'],
				"RG" => $formando['ViewFormandos']['rg'],
				"CPF" => $formando['ViewFormandos']['cpf'],
				"Endereco" => "Rua: ".$formando['ViewFormandos']['end_rua']." - ".$formando['ViewFormandos']['end_numero']." ".$formando['ViewFormandos']['end_complemento']." - Bairro: ".$formando['ViewFormandos']['end_bairro']." - CEP: ".$formando['ViewFormandos']['end_cep']." - Cidade: ".$formando['ViewFormandos']['end_cidade']." - UF: ".$formando['ViewFormandos']['end_uf'],
				"Data Adesao" => $formando['ViewFormandos']['data_adesao'],
				"Numero Havaiana" => $formando['ViewFormandos']['numero_havaiana'],
				"Camiseta" => $formando['ViewFormandos']['tam_camiseta'],
				"Turma" => $formando['ViewFormandos']['turma_id'],
				"Curso" => ((isset($formando['Curso']['nome'])) ? $formando['Curso']['nome'] : "-"),
				"Sala" => $formando['ViewFormandos']['sala'],
				"Tel Residencial" => $formando['ViewFormandos']['tel_residencial'],
				"Tel Celular" => $formando['ViewFormandos']['tel_celular'],
				"Mae" => $formando['ViewFormandos']['nome_mae'],
				"Pai" => $formando['ViewFormandos']['nome_pai'],
				"Situacao" => $formando['ViewFormandos']['situacao'],
				"Forma de Pagamento" => ucfirst(str_replace("_"," ", $formando['ViewFormandos']['forma_pagamento'])),
				"Realizou Checkout?" => (($formando['ViewFormandos']['realizou_checkout'] == 0) ? "Não" : "Sim")
			);
	
	$excel->generate($listaDeFormandosParaExcel, 'lista_formandos_turma_'.$formando['ViewFormandos']['turma_id']);
