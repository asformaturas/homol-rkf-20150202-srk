<script type="text/javascript">
    $(document).ready(function() {
            $("#adesao-comissao").submit(function(e) {
                e.preventDefault();
                if ($("#aceitar-contrato").is(":checked")) {
                    var context = ko.contextFor($(".metro-button.back")[0]);
                    context.$data.showLoading(function() {
                        var dados = $("#adesao-comissao").serialize();
                        var url = "/<?= $this->params["prefix"] ?>/formandos/aderir";
                        $.ajax({
                            url: url,
                            data: dados,
                            type: "POST",
                            dataType: "json",
                            complete: function() {
                                context.$data.reload();
                            }
                        });
                    });
                } else {
                    alert("Você deve aceitar os termos do contrato para prosseguir com a adesão");
                }
            });
        });
 
</script>

<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button back" data-bind="click: function() { hideHomeButton() }"></a>
            Fazer Adesão
        </h2>
    </div>
</div>
<div class="span10">
    <?php $session->flash(); ?>
    <?php
    if ($mostrar_formulario) {
        echo $form->create('Usuario', array('url' => '/' . $this->params['prefix'] . '/formandos/aderir', 'id' => 'adesao-comissao'));
        if ($is_pagante) {
            ?>    
            <div class="row-fluid" id="adesao-comissao"> 
                <div class="span6">
                    <strong>Dia preferido de vencimento das parcelas</strong><br/>
                    <?php
                    for ($i = 1; $i <= cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y')); $i++)
                        $arr_data_venc[$i] = $i;
                    if($turma['Turma']['id'] == 6028){
                        $arr_data_venc = array('5' => '5', '20' => '20');
                    }
                    if($turma['Turma']['id'] == 6631){ 
                        $arr_data_venc = array('10' => '10', '20' => '20');
                    }
                    echo $form->select('FormandoProfile.dia_vencimento_parcelas', $arr_data_venc, null, array('empty' => false, 'class' => 'input-small', 'label' => false, 'div' => false, 'error' => false));
                    ?>
                    <br/>
                    <strong>Data de início do pagamento</strong><br/>
                    <?php
                    echo $form->select('FormandoProfile.data_inicio_pagamento', $select_data_inicio_pagamento, array('empty' => false,
                        'class' => 'input-small', 'label' => false, 'div' => false, 'error' => false));
                    ?>
                    <br/>

                    <div style="display:none;">
                        <strong>Planos</strong><em class='carregando-planos'></em><br/>
                        <?=
                        $form->input('FormandoProfile.parcelamentos', array('type' => 'select', 'options' => array(), 'empty' => false,
                            'class' => 'input-large pagamento-parcelamentos', 'label' => false, 'div' => false,
                            'error' => false));
                        ?>
                    </div>
                    <strong>Parcelas</strong><em class='carregando-parcelas'></em><br/>
                    <?=
                    $form->input('FormandoProfile.parcelas', array('type' => 'select', 'options' => $select_parcelas,
                        'empty' => false, 'class' => 'input-large pagamento-parcelas', 'label' => false, 'div' => false,
                        'error' => false));
                    ?>
                    <br/>
                    <br/>
                </div>
                <div class="row-fluid">
                    <div class="row-fluid" style='width:700px; height:300px; overflow:auto; border:solid 1px #999; padding:8px;'>
                        <?=$this->element('contratos/condicoes_geral'); ?>
                    </div>
                </div>
                <br/>
                <div class="row-fluid">
                    <strong>Forma de Pagamento</strong><br/>
                    <?php
                    echo $form->input('FormandoProfile.forma_pagamento', array('type' => 'select', 'options' => $select_forma_pagamento, 'empty' => false, 'class' => 'input-xxlarge', 'label' => false,
                        'div' => false, 'error' => false));
                    ?>
                    <br/>
                    <br />
                </div>
                <div class="row-fluid">
                    <label class="input-control checkbox">
                        <input type="checkbox" id="aceitar-contrato">
                        <span class="helper">&nbsp;<b>Li, aceito os temos do contrato e sou maior de 18 anos.</b></span>
                    </label>
                    <label for="aceitar-contrato">&nbsp;</label>
                </div>
            </div>
            <button id="enviar" class="default bg-color-red big pull-left">
                    <i class="icon-checkmark"></i>
                    Cadastrar
            </button>
            <?php
            $form->end(array('label' => false,
                    'div' => false, 'class' => 'hide'));
        } else {
            echo '<br />';
        }
    } else {
        echo '<h2>' . $mensagem . '</h2>';
    }
    ?>
</div>
