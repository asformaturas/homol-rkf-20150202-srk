<?php

$listaDeFormandosParaExcel = array();

foreach ($formandos as $formando)
        $listaDeFormandosParaExcel[] = array(
            "Turma" => $turmaLogada['Turma']['nome'],
            "Codigo do formando" => $formando['ViewFormandos']['codigo_formando'],
            "Nome" => $formando['ViewFormandos']['nome'],
            "Email" => $formando['ViewFormandos']['email'],
            "Tel Residencial" => $formando['ViewFormandos']['tel_residencial'],
            "Tel Celular" => $formando['ViewFormandos']['tel_celular'],
            "Nome Telão" => $formando['ViewFormandos']['nome_telao'],
            "Foto Criança" => ($formando[0]['foto_crianca'] == null ? 'Não cadastrado' : 'http://sistema.asformaturas.com.br'.$this->webroot.$formando[0]['foto_crianca']),
            "Foto Adulto" => ($formando[0]['foto_adulto'] == null ? 'Não cadastrado' : 'http://sistema.asformaturas.com.br'.$this->webroot.$formando[0]['foto_adulto']),
        );
$excel->generate($listaDeFormandosParaExcel, 'lista_fotos_telao_turma_' . $turmaLogada['Turma']['id']);
?>