<?php 
$listaDeFormandosParaExcel = array();

	foreach ($formandos as $formando)
		$listaDeFormandosParaExcel[] = array(
				"id" =>  $formando['vw_formandos']['id'],
				"Turma" => $formando['vw_formandos']['turma_id'],                    
				"Codigo do formando" => $formando['vw_formandos']['codigo_formando'],
                                "Grupo" =>  $formando['vw_formandos']['grupo'],
                                "Curso" =>  $formando['vw_formandos']['curso_nome'],
                                "Turno" =>  $formando['vw_formandos']['turno'],
				"Nome" =>  $formando['vw_formandos']['nome'],
                                "Data Nascimento" =>  date('d-m-Y', strtotime($formando['vw_formandos']['data_nascimento'])),
				"Email" => $formando['vw_formandos']['email'],
                                "Rua" => $formando['vw_formandos']['end_rua'],
                                "Numero" => $formando['vw_formandos']['end_numero'],
                                "Bairro" => $formando['vw_formandos']['end_bairro'],
                                "Cep" => $formando['vw_formandos']['end_cep'],
                                "Cidade" => $formando['vw_formandos']['end_cidade'],
                                "UF" => $formando['vw_formandos']['end_uf'],
				"RG" => $formando['vw_formandos']['rg'],
				"CPF" => $formando['vw_formandos']['cpf'],
				"Data Adesao" => $formando['vw_formandos']['data_adesao'],
                                "Valor Adesao" => $formando['vw_formandos']['valor_adesao'],
                                "Parcelas Adesao" => $formando['vw_formandos']['parcelas_adesao'],
                                "Data Adesao" => $formando['vw_formandos']['data_adesao'],
				"Tel Residencial" => $formando['vw_formandos']['tel_residencial'],
				"Tel Celular" => $formando['vw_formandos']['tel_celular'],
                                "Tel Cel Mae" => $formando['vw_formandos']['tel_cel_mae'],
                                "Tel Cel Pai" => $formando['vw_formandos']['tel_cel_pai'],
				"Situacao" => $formando['vw_formandos']['situacao'],
				"Forma de Pagamento" => str_replace("_"," ", $formando['vw_formandos']['forma_pagamento']),
				"Realizou Checkout?" => (($formando['vw_formandos']['realizou_checkout'] == 0) ? "Nao" : "Sim")
			);
	$excel->generate($listaDeFormandosParaExcel, 'lista_formandos'.date('Y_m_d'));
?>