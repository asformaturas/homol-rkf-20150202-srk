<span id="conteudo-titulo" class="box-com-titulo-header">Cadastro de Formando</span>
<div id="conteudo-container" style="margin-left:0;">
	<?php 
		if (isset($mensagem_despesa)) { ?>
		<div class='info info-sucesso grid_full first alpha'>
			<?php echo $mensagem_despesa;?>
		</div>	
	<?php	} ?>
	<?php 
		$session->flash();
	?>
	
	<p class="grid_full alpha omega">
		<h2>
	    Sua adesão foi efetuada com sucesso. <br /><br />Seu código de formando é <b><?php echo $codigo_formando_cadastro;?></b><br /><br />
	    Para acessar seu espaço de formando clique em 'Entrar como formando' na parte superior da tela. <br />
	    </h2>
	</p>
</div>
