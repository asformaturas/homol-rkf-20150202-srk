<p class="grid_12 alpha omega">
    <label class="grid_11 alpha omega">Dia preferido de vencimento das parcelas</label>
    <?php
    for ($i = 1; $i <= cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y')); $i++)
        $arr_data_venc[$i] = $i;

    echo $form->select('FormandoProfile.dia_vencimento_parcelas', $arr_data_venc, null, array('empty' => false, 'class' => 'grid_2 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10')));
    ?>
</p>

<?php
if (!isset($esconder_data_inicio_pagamento)) {
    ?>
    <p class="grid_8 alpha omega first">
        <label class="grid_12 alpha omega">Data de início do pagamento</label>
    <?php echo $form->select('FormandoProfile.data_inicio_pagamento', $select_data_inicio_pagamento, null,
            array('empty' => false, 'class' => 'grid_12 first alpha omega data-inicio-pagamento',
                'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
    </p>
    <?php
}
?>
<p class="grid_8 alpha omega first" style="display:none">
    <label class="grid_12 alpha omega">Planos</label><em class='carregando-planos'></em>
    <?= $form->input('FormandoProfile.parcelamentos', array('type' => 'select', 'options' => array(), 'empty' => false,
        'class' => 'grid_10 first alpha omega pagamento-parcelamentos', 'label' => false, 'div' => false,
        'error' => array('wrap' => 'span', 'class' => 'grid_12')));
    ?>
</p>
<p class="grid_7 alpha omega detalhar-parcelamento">
    Prezado formando, a condição abaixo permite que você monte a forma de 
    pagamento de acordo com as suas necessidades.
    <br />
    Nela, você escolhe quanto irá pagar em cada mês.
</p>
<p class="grid_8 alpha omega first">
    <label class="grid_5 alpha omega">Parcelas</label><em class='carregando-parcelas'></em>
    <?= $form->input('FormandoProfile.parcelas', array('type' => 'select', 'options' => $select_parcelas,
        'empty' => false, 'class' => 'grid_12 first alpha omega pagamento-parcelas', 'label' => false, 'div' => false,
        'error' => array('wrap' => 'span', 'class' => 'grid_10')));
    ?>
</p>
<?php if($turma['Turma']['plano_economico'] == 1 || $planoEconomico) : ?>
<p class="grid_5 alpha omega">
    <label class="grid_12 alpha omega">&nbsp;</label>
    <button style="border:none; float:none; margin:0" type="button" class="submit button detalhar-parcelamento"
            id="detalhar-parcelamento">
        Condi&ccedil;&atilde;o Especial
    </button>
</p>
<?php endif; ?>
<p class="grid_11 alpha omega first">
    <label class="grid_11 alpha omega">Forma de Pagamento</label>
<div style='float:left; margin:10px; width:700px; height:300px; overflow:auto; border:solid 1px #999; padding:8px; font-size:12px'>
    <h3 style='font-weight:bold'>Condi&ccedil;&otilde;es Contratuais</h3>
    <br />
    Adere ao Contrato de Prestação de Serviços, Representação Financeira e de Fotos firmado entre a Comissão de Formatura de seu curso e a empresa ÁS EVENTOS LTDA., 
    inscrita no CNPJ/MF sob N.º06.163.144/0001-45, com sede na Avenida José Maria Whitaker, 882 CEP: 04057-000, São Paulo – SP, 
    representada neste ato pelo sócio-diretor RACHID SADER, portador do RG Nº 26.572.035-7, 
    contrato esse assinado em <?= !empty($turma['Turma']['data_assinatura_contrato']) ? date('d/m/Y', strtotime($turma['Turma']['data_assinatura_contrato'])) : "" ?> que será denominado CONTRATO COLETIVO e regido nos seguintes moldes:
    <br />
    <br />
    1. O formando acima qualificado reconhece a Comissão de Formatura de sua turma como legítima representante para tratar dos assuntos 
    relacionados à sua Formatura, outorgando poderes para representá-lo em atos referentes à sua formatura, ratificando os termos do CONTRATO COLETIVO, 
    declarando ter plenos conhecimentos desse contrato e outorgando poderes para a Comissão celebrar aditivos contratuais que se fizerem 
    necessários à realização dos eventos.
    <br />
    <br />
    2. As informações importantes a respeito do seu contrato estarão disponibilizadas no link: http://sistema.formaturas.as. 
    Caso precise do login e senha da sua turma, entre em contato com a sua comissão ou diretamente com a ÁS Formaturas.
    <br />
    <br />
    3. A comissão de formatura não poderá tratar assuntos de cunho financeiro, tais como forma de pagamento, 
    renegociação de dívida ou cancelamento de contrato em nome do formando.
    <br />
    <br />
    4. O formando acima qualificado autoriza a ÁS Formaturas a representá-lo na contratação e pagamento de 
    fornecedores conforme descrito no CONTRATO COLETIVO. O valor pago pelo CONTRATANTE permanecerá em posse 
    da CONTRATADA de forma transitória, até o repasse aos fornecedores do pacote de formatura do quais a 
    CONTRATADA é intermediária, ficando tão-somente com a comissão que lhe faz jus.
    <br />
    <br />
    5. O(A) CONTRATANTE se obriga a pagar a CONTRATADA pelos eventos de formatura descritos no CONTRATO COLETIVO.
    <br />
    <br />
    6. No momento da retirada dos convites o formando que optar por boletos bancários deverá quitar as parcelas 
    em atraso e trocar os boletos ainda em aberto por cheques pré-datados. No mesmo momento, o formando que 
    tiver optado por cheques pré-datados e ainda não tiver os entregue, deverá entregar os mesmos.
    <br />
    <br />
    7. Os valores consignados neste contrato serão atualizados de 12 em 12 meses, a partir da assinatura do 
    Contrato de Prestação de Serviços celebrado entre a Comissão de Formatura e a ÁS EVENTOS LTDA., 
<?= $this->data['Turma']['id'] == 5148 ? '( 12/06/2012 )' : "" ?> adotando - se a variação do IGPM/FGV do período, conforme autoriza a Lei 9.069/95, ou por outro índice que venha a substituí-lo. 
    A ÁS Formaturas não é autorizada a realizar qualquer outro tipo de reajuste de valores contratuais.
    <br />
    <br />
    8. Os convites só serão entregues após feito o acerto financeiro, isto é, pendências devidamente quitadas, 
    isto inclui os valores relativos ao IGPM, cheques trocados e entregues , conforme explicitado nos itens 6 e 7.
    <br />
    <br />
    9. No caso de rescisão contratual, o aluno poderá rescindir até 90 dias antes do término do ano letivo de 
    conclusão do curso, desde que pago a título de multa o valor dos itens ou brindes já entregues ou contratados 
    para os formandos adicionado de 20% do valor total do contrato. Após este período será exigido o pagamento 
    integral do Contrato, exceto devido a motivos justificados e comprovados, tais como <?= $this->data['Turma']['id'] != 5148 ? 'reprovação ou ' : "motivos de saúde, óbito do formando e " ?>transferência 
    por motivos profissionais, nesse caso ficando o formando livre de multa.
    <br />
    <br />
    10. Constitui obrigação do(a) CONTRATANTE manter seu endereço e demais dados cadastrais atualizados, bem como a responsabilidade 
    pela grafia correta de seu nome, nos textos originais que deverão ser entregues à CONTRATADA para a execução dos serviços gráficos, 
    eximindo a CONTRATADA pela falta do nome e/ou grafia incorreta conforme previsão no Contrato Principal.
    <br />
    <br />
    11. Considerando que os convites de luxo serão montados com fotos da turma e/ou formando, a Comissão de Formatura avisará todos 
    os formandos da data/local e horário marcado junto à CONTRATADA para serem fotografados, estando o formando ciente que sua 
    ausência isentará a CONTRATADA de qualquer responsabilidade referente as fotografias no convite.
    <br />
    <br />
    12. A empresa não se responsabiliza por objetos pessoais em todos os eventos organizados pela empresa.
    <br />
    <br />
    13. Fica eleito o Foro da Capital do Estado de São Paulo por mais privilegiado que outro seja, arcando a parte vencida com 
    o ônus da sucumbência, inclusive honorários advocatícios de 20% do total da condenação.
    <br />
    <br />
    A &Aacute;s formaturas n&atilde;o cobra taxa de emiss&atilde;o de boletos. Para imprimi-los, 
    acesse o espa&ccedil;o do formando com o seu login e senha e tenha acesso &agrave; todos os seus boletos na &Aacute;rea Financeira .
    <br />
    <br />
    <br />
    Caso opte por pagar em cheque, lembramos que &eacute; sua responsabilidade a entrega dos cheques para o representante da &Aacute;s na faculdade ou diretamente em nossa sede. 
    Sempre exija o seu recibo.
    <br />
    <br />
    Lembramos que caso opte em pagar em cheque mas decida posteriormente pagar em boletos, 
    n&atilde;o h&aacute; problemas, basta acessar sua &aacute;rea financeira e imprimir os boletos.
    <br />
    <br />
    Al&eacute;m disso, a &Aacute;S disponibiliza um servi&ccedil;o adicional de envio dos boletos impressos.<br />
    Caso queira receb&ecirc;-los em casa, opte por boletos impressos.
    <br />
    Será cobrado o valor de R$ 45,00 referente ao envio dos mesmos. Lembramos que essa taxa n&atilde;o &eacute; obrigatória e voc&ecirc; facilmente pode acessar todos os seus boletos online.
    Al&eacute;m disso, caso opte em receber os boletos na sua casa, você poder&aacute; tamb&eacute;m entrar no site e imprimir uma segunda via a qualquer momento.
</div>
<?php
echo $form->input('FormandoProfile.forma_pagamento', array('type' => 'select', 'options' => $select_forma_pagamento, 'empty' => false, 'class' => 'grid_10 first alpha omega', 'label' => false,
    'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10')));
?>
</p>
<p class="grid_10 first alpha omega">
    <br />
    <input type="checkbox" id="termos-adesao" style="float:left; margin-right:5px" /> <label for="termos-adesao" style="line-height:initial">Li e aceito os termos do contrato</label>
</p>

