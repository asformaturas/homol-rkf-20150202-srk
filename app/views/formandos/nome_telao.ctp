<style type="text/css">
    .esconder{ display: none!important }
    .mostrar{ display: block }
</style>
<script type="text/javascript">
$(document).ready(function() {
    $("#nome").keyup(function() {
        if($(this).val() != ''){
            $(".enviar-nome").fadeIn(500);
            $('body').tooltip({ selector: '.input-block-level'}); 
        }else{
            $(".enviar-nome").fadeOut(500);
        }
    });
    $("#enviar-nome").click(function(e) {
        e.preventDefault();
        dir = $(this).attr('dir');
        bootbox.confirm('Confirma a alteracao do Nome?',function(response) {
            if(response) {
                var dados = {
                    data : {
                        nome : $("#nome").val()
                    }
                };
                var url = '/<?=$this->params['prefix']?>/formandos/alterar_nome_telao/'+dir;
                var context = ko.contextFor($(".metro-button.reload")[0]);
                bootbox.hideAll();
                context.$data.showLoading(function() {
                    $.ajax({
                        url : url,
                        data : dados,
                        type : "POST",
                        dataType : "json",
                        complete : function() {
                            context.$data.reload();
                        }
                    });
                });
                return false;
            }
        });
    });
});
</script>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Fotos Para Tel&atilde;o
        </h2>
    </div>
</div>
<div class="alert alert-block alert-error fade in <?=$mostrar?>">
    <a class="close" data-dismiss="alert" href="#"></a>
        <h4 class="alert-heading">Erro</h4>
        O prazo para upload de fotos do telão se esgotou.
</div>
<div class="<?=$esconder?>">
    <div class="alert alert-error">
        Antes de enviar as fotos voc&ecirc; deve confirmar o nome que dever&aacute; aparecer no tel&atilde;o
    </div>
    <br />
    <?php $session->flash(); ?>
    <div class="row-fluid">
        <h3 class='fg-color-red' style='font-family:Arial; margin-bottom:5px'>
            Nome Pra Ser Exibido No Tel&atilde;o
        </h3>
        <div class="row-fluid">
            <div class="span5">
                <input type="text" rel="tooltip" title="APENAS 25 CARACTERES" id='nome' value="<?=$formando['FormandoProfile']['nome_telao']?>" maxlength="25" placeholder="Descri&ccedil;&atilde;o Da Foto" class="input-block-level"/>
            </div>
            <div class="span3 hide enviar-nome hide">
                <a class='button mini bg-color-blue btn-file input-block-level' id="enviar-nome" dir="<?=$formando['FormandoProfile']['usuario_id']?>">
                    Confirmar Altera&ccedil;&atilde;o De Nome
                    <i class='icon-thumbs-up'></i>
                </a>
            </div>
        </div>
    </div>
</div>