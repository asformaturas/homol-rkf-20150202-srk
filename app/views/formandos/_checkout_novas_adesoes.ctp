<h1 class="grid_16 first" style="margin-top: 20px;border-bottom: 1px solid #000;">Realizar novas ades&otilde;es<?php if ($this->params['action'] == 'planejamento_checkout' || $this->params['action'] == 'formando_checkout') { ?>	 				<a style="float:right;font-size:14px; margin-top:10px;">exibir<?php } ?></a><div style="clear: both;"></div></h1>

<div class="conteudo-container grid_16 first checkout-secao-container" id='secao-aderir'>	
	<div class="container-tabela-financeiro first grid_full">
		<table class="tabela-financeiro tabela-boletos"	>
			<thead>
				<tr>
					<th scope="col">Nome</th>				
					<th scope="col">Descricao</th>
                    <th scope="col">Data Inicio</th>
                    <th scope="col">Data Fim</th>
                    <th scope="col">Status</th>
					<th scope="col"> &nbsp;</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="3">
					</td>
					<td colspan="2">Total: <?php echo count($campanhas); ?> campanha(s)</td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach($campanhas as $campanha): ?>
				<?php if($isOdd):?>
					<tr class="odd">
				<?php else:?>
					<tr>
				<?php endif;?>
					<td width="20%"><?php echo $campanha['Campanha']['nome'];?></td>
					<td width="40%"><?php echo $campanha['Campanha']['descricao'];?></td>
                    <td width="10%"><?php echo date('d/m/Y',strtotime($campanha['Campanha']['data_inicio']));?></td>
                    <td width="10%"><?php echo date('d/m/Y',strtotime($campanha['Campanha']['data_fim']));?></td>
                    <td width="10%"><?php echo $campanha['Campanha']['status']; ?></td>
					<td width="10%">
					<?php echo $html->link('Visualizar', array($this->params['prefix'] => true, 'controller' => 'campanhas', 'action' =>'checkout_visualizar', $campanha['Campanha']['id'], $formando['Usuario']['id'], $idDoFormandoProfile), array('class' => 'submit button')); ?>
					</td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
