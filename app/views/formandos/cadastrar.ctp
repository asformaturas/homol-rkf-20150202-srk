<?php $buttonAfter = '<button class="helper" onclick="return false" ' .
        'tabindex="-1" type="button"></button>'; ?>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/smart_wizard.css">
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/chosen.css">
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/bootstrap/datepicker.css">
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/bootstrap/fileupload.css?v=0.1">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/jquery.smartWizard-2.0.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/jquery-validate.js"></script>
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/bootstrap/datepicker.js"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/modal.js"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/bootbox.js"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/jquery.meio.mask.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/chosen.js"></script>
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/bootstrap/fileupload.js"></script>
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/treme.js"></script>
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/utils.js"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/jquery.mask.min.js?v=0.2"></script>
<style type="text/css">
@media print {
    body * {
        background: none    !important;
        visibility: hidden  !important;
        position:   static  !important;
        border:   0px  !important;
    }
    .area_print, .area_print * {
        visibility: visible !important;
    }
    .area_print {
        position: absolute !important;
        left:     0        !important;
        top:      10px     !important;
        width:    auto     !important;
        height:   auto     !important;
        margin:   -30px     !important;
        padding:  50px      !important;
    }
}
label.required { margin-right:0 }
label.required span { float: right; }
label.required:after {
    font-family: 'IcoMoon';
    -webkit-font-smoothing: antialiased;
    margin-left:4px;
    width:12px;
    height:12px;
    font-weight: normal;
    content: "\e126";
    speak: none;
    font-size:12px;
    line-height: 12px;
    vertical-align:5%;
    color:#666666;
}
input.error,
input.form-error { border-color:rgb(236,82,97)!important }
input.error:focus,
input.form-error:focus{
  border-color: rgba(236,82,97,0.8);
  outline: 0;
  outline: thin dotted \9;
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(236,82,97,0.6);
     -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(236,82,97,0.6);
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(236,82,97,0.6);
}
.chzn-container  { margin-bottom:10px }
.chzn-container .chzn-single { width:100%; }
.marginLeft0 { margin-left:0!important }
.modal { max-width: 560px!important; max-height:80%!important; }
</style>
<script type="text/javascript">
    $(document).ready(function(){
        $(".selectpicker").selectpicker({width:'100%'});
        $('#wizard').smartWizard({
            onShowStep: function(obj) {
                atualizaAltura($('.stepContainer:visible').height()+250);
                var passo = obj.attr('rel');
                if(passo == 3) {
                    if($("#UsuarioAderir").val() == 1) {
                        carregarPlanos();
                    }
                }
            },
            labelNext: 'Pr&oacute;ximo',
            labelPrevious: 'Anterior',
            labelFinish: 'Finalizar',
            selected: 0,
            keyNavigation: false,
            enableAllSteps : true,
            onLeaveStep: function(obj) {
                var passo = obj.attr('rel');
                div = $("#step-"+passo);
                var valido = true;
                if(!(passo == 3 && $("#UsuarioAderir").val() == 0)) {
                    div.find('[data-required]').each(function() {
                        $(this).field();
                        if($(this).hasClass('error'))
                            valido = false;
                    });
                }
                $('#wizard').smartWizard('setError',{
                    stepnum:passo,
                    iserror:!valido
                });
                if(!valido) {
                    var a = $('a[href="#step-'+passo+'"]')[0];
                    init(a);
                    rattleimage();
                    setTimeout(function() {stoprattle(a)},1000);
                }
                return true;
                return valido;
            }
        });
        
        $(".cpf-msk").mask("000.000.000-00",  {
            clearIfNotMatch: true
        });

        $('#formulario').validate({
            sendForm : true,
            onBlur: true,
            eachValidField : function() {
                $(this).removeClass('error').removeClass('form-error').addClass('success');
                var label = $('label[for="'+$(this).attr('id')+'"]');
                if(label.length > 0) {
                    if(label.children('span').length > 0)
                        label.children('span').fadeOut(500,function() { $(this).remove()});
                }
            },
            eachInvalidField : function() {
                $(this).removeClass('success').addClass('error');
            },
            description: {
                notEmpty : {
                    required : function() {
                        var label = $('label[for="'+$(this).attr('id')+'"]');
                        if(label.length > 0) {
                            if(label.children('span').length > 0)
                                label.children('span').html('').attr('class','fg-color-red');
                            else
                                label.append($('<span>',{class:'fg-color-red'}));
                            mensagem = $(this).data('error') ||
                                'Complete o campo';
                            label.children('span').html(mensagem);
                        }
                        return '';
                    },
                    conditional : function() {
                        var label = $('label[for="'+$(this).attr('id')+'"]');
                        if(label.length > 0) {
                            if(label.children('span').length > 0)
                                label.children('span').html('').attr('class','fg-color-red');
                            else
                                label.append($('<span>',{class:'fg-color-red'}));
                            mensagem = $(this).data('error-conditional') ||
                                'Complete o campo';
                            label.children('span').html(mensagem);
                        }
                        return '';
                    },
                    pattern : function() {
                        var label = $('label[for="'+$(this).attr('id')+'"]');
                        if(label.length > 0) {
                            if(label.children('span').length > 0)
                                label.children('span').html('').attr('class','fg-color-red');
                            else
                                label.append($('<span>',{class:'fg-color-red'}));
                            mensagem = $(this).data('error-pattern') ||
                                'Complete o campo';
                            label.children('span').html(mensagem);
                        }
                        return '';
                    }
                }
            },
            invalid: function() {
                $('#tab-content > .tab-pane').each(function() {
                    if($(this).find('[data-required="true"].error').length > 0) {
                        $("a[href='#"+$(this).attr('id')+"']").parent().addClass('error');
                    } else {
                        $("a[href='#"+$(this).attr('id')+"']").parent().removeClass('error');
                    }
                });
            },
            conditional: {
                senha: function() {
                    return $("#senha").val() == $("#confirmar").val();
                },
                email: function() {
                    return $("#input-conf-email").val() == $("#input-email").val();
                },
                pagamento: function(value) {
                    return !($("#UsuarioAderir").val() == 1 && value == '');
                }
            }
        });
        $("#conteudo,#content-body").css('min-height',$('#wizard').height());
        atualizaAltura($('.stepContainer:visible').height()+250);
        $("#UsuarioConfirmar").blur(function() {
            $("#senha").field();
        });
        $('input[alt]').setMask();
        
        $('.datepicker').datepicker();
        $(".chosen").chosen({width:'100%',dropdown_position: "top"});
        $.Input();
        $("#busca-cep").click(function(e) {
            e.preventDefault();
            if($('label[for="cep"]').children('span').length > 0)
                $('label[for="cep"]').children('span').text('Buscando');
            else
                $('label[for="cep"]').append($("<span>",{text:'Buscando'}));
            buscarCep($("#cep").val());
        });

        $("#cep").blur(function() {
            if($('label[for="cep"]').children('span').length > 0)
                $('label[for="cep"]').children('span').text('Buscando');
            else
                $('label[for="cep"]').append($("<span>",{text:'Buscando'}));
            buscarCep($("#cep").val());
        });
        
        $("#busca-cep").bind('resposta-busca',function(response) {
            if($('label[for="cep"]').children('span').length == 0)
                $('label[for="cep"]').append($("<span>"));
            span = $('label[for="cep"]').children('span');
            if(response.error) {
                span.text("Endereço não encontrado");
            } else {
                span.text("Endereço encontrado!");
                $("#FormandoProfileEndRua").val(unescape(response.endereco["tipo_logradouro"]) + " "
                        + unescape(response.endereco["logradouro"]));
                $("#FormandoProfileEndBairro").val(unescape(response.endereco["bairro"]));
                $("#FormandoProfileEndCidade").val(unescape(response.endereco["cidade"]));
                $('#FormandoProfileEndUf option[value="' + response.endereco["uf"] + '"]').attr("selected","selected");
                $("#FormandoProfileEndUf").trigger("liszt:updated");
                $("#FormandoProfileEndNumero").focus();
            }
        });
        
        $('#fileupload').bind('loaded',function(e) {
            var src = e.imagem.replace(/^data:image\/(gif|png|jpe?g);base64,/, "");
            if(src) {
                $('#UsuarioFotoSrc').val(src);
                $('#UsuarioFotoData').val(e.imagem);
            } else {
                $('#UsuarioFotoSrc').val('');
                $('#UsuarioFotoData').val('');
            }
            return;
        });
        
        if($('#UsuarioFotoData').val() != '') {
            $('#fileupload')
                    .removeClass('fileupload-new')
                    .addClass('fileupload-exists')
                    .prepend('<input type="hidden" value="" name="">');
            $('<img>',{src:$('#UsuarioFotoData').val()}).appendTo($("#fileupload-preview"));
        }
        
        var pagamento = {
            parcelas: '<?=$this->data['FormandoProfile']['parcelas']?>',
            planos: '<?=$this->data['FormandoProfile']['planos']?>'
        };
        
        $("body").on('change','.formando-planos',function() {
            pagamento.planos = $(this).val();
            carregarParcelas();
        })
        
        $("body").on('change','.formando-parcelas',function() {
            pagamento.parcelas = $(this).val();
        })
        
        function verificarEmail(){
            var input = $('#input-email');
            var span = input.parent().prev().children('em');
            var url = "/formando/formandos/verificaEmail/";
            if($('#input-email').val() != ''){
                $.ajax({
                    url: url,
                    data: { 
                        data: { 
                           email: $('#input-email').val()
                        }
                    },
                    type: "POST",
                    dataType: "json",
                    success: function(data) {
                        if(data.mensagem == 1)
                            span.addClass('fg-color-green').text('Email disponível.');
                        else
                            span.addClass('fg-color-red').text('Email não disponível');
                    },
                    error: function(data) {
                        span.addClass('fg-color-red').text('Erro ao verificar.');
                    }
                });
            };
        };
        
        var timer;
        
        $("#input-email").keyup(function() {
            var input = $('#input-email');
            if(input.parent().prev().children('em').length < 1)
                input.parent().prev().append('<em></em>');
            var span = input.parent().prev().children('em');
            span.attr('class', 'pull-right').text('Verificando...');
            if(timer != null)
                clearTimeout(timer);
            timer = setTimeout(function(){verificarEmail()}, 700);
        });
        
        $("#FormandoProfileDataInicioPagamento").change(function() {
            carregarPlanos();
        });
        
        $('#ler-contrato').click(function(e) {
            e.preventDefault();
            bootbox.alert($("#contrato").html());
            var imprimir = $("<button>",{
                type: 'button',
                class: 'button bg-color-red',
                id:'imprimir',
                text:'Imprimir'
            });
            $('.modal-footer').prepend(imprimir);
            //$('#contrato').removeClass('hide');
            $('#imprimir').click(function(){window.print();});
        });
        
        
        $("button.toggle-adesao").click(function(e) {
            e.preventDefault();
            exibirPagamento($(this).hasClass('com'));
        })
        
        exibirPagamento($("#UsuarioAderir").val() == 0);
        
        $('[id*="step-"]').each(function() {
            passo = parseInt($(this).attr('id').substring(5));
            if($(this).find('.form-error').length > 0) {
                $('#wizard').smartWizard('setError',{
                    stepnum:passo,
                    iserror:true
                });
            }
        });
        
        <?php if($erros) : ?>
        bootbox.alert($("#session-flash").html());
        <?php endif; ?>
        
        function carregarPlanos() {
            var url = "<?= $html->url(array('prefix' => false,
                'controller' => 'formandos', 'action' => 'planos',$turma['Turma']['id'])); ?>";
            $.get(url, function(data) {
                dados = jQuery.parseJSON(data);
                value = '';
                $(".formando-planos,.formando-parcelas").html('<option value="">Selecione um Plano</option>');
                $.each(dados.planos,function(i,plano) {
                    var titulo = plano.Parcelamento.titulo != null ?
                        plano.Parcelamento.titulo : 'Padr&atilde;o';
                    var id = plano.Parcelamento.titulo != null ?
                        plano.Parcelamento.titulo : 'null';
                    if(id == pagamento.planos)
                        value = id;
                    $(".formando-planos").append('<option value="'+id+
                        '">'+titulo+'</option>');
                })
                if(value != '')
                    $(".formando-planos").val(value)
                $("#FormandoProfilePlanos,#FormandoProfileParcelas").trigger("liszt:updated");
                if(dados.quantidade == 1) {
                    $(".formando-planos").val($(".formando-planos option:last").val());
                    pagamento.planos = $(".formando-planos").val();
                    $("#FormandoProfilePlanos").trigger("liszt:updated");
                }
                if($(".formando-planos").val() != "")
                    carregarParcelas();
            });
        }
        
        function carregarParcelas() {
            var dataInicio = $("#FormandoProfileDataInicioPagamento").val();
            var data = {
                data : dataInicio,
                plano : pagamento.planos,
                turma : '<?=$turma['Turma']['id']?>'
            };
            if(dataInicio != "") {
                $.ajax({
                    url : "/formandos/parcelamentos",
                    type : 'POST',
                    dataType : 'json',
                    data : {
                        data : data
                    },
                    success : function(response) {
                        var dados = response, value = '';
                        $(".formando-parcelas").html('<option value="">Selecione Um Parcelamento</option>');
                        $.each(dados.parcelas,function(i,parcela) {
                            if(parcela.Parcelamento.id == pagamento.parcelas)
                                value = parcela.Parcelamento.id;
                            $(".formando-parcelas").append('<option value="'+parcela.Parcelamento.id+
                                '">'+parcela.Parcelamento.texto+'</option>');
                        });
                        $("#FormandoProfileParcelas").val(value).trigger("liszt:updated");
                    },
                    complete : function() {
                        if(pagamento.parcelas == 'plano_economico')
                            $("#FormandoProfileParcelas")
                                .append('<option value="plano_economico">Condição Especial</option>')
                                .val('plano_economico')
                                .trigger("liszt:updated");
                    }
                });
            }
        }
        
        function exibirPagamento(exibir) {
            if(exibir) {
                $(".toggle-adesao.com").fadeOut(500,function() {
                    $("#UsuarioAderir").val(0);
                    $(".toggle-adesao.sem").fadeIn(500);
                })
            } else {
                $("#UsuarioAderir").val(1);
                $(".toggle-adesao.sem").fadeOut(500,function() {
                    $(".toggle-adesao.com").fadeIn(500);
                })
            }
        }
        
    });
    
</script>
<div id="session-flash" class="hide">
    <?php $session->flash(); ?>
</div>
<div class="span12">
    <?=$form->create('Usuario', array(
        'url' => "/formandos/cadastrar",
        'id' => 'formulario')); ?>
    <?=$form->hidden('Turma.id', array('value' => $turma['Turma']['id'])); ?>
    <div id="wizard" class="swMain">
        <ul class="anchor">
            <li>
                <a href="#step-1" class="selected">
                    <label class="stepNumber">1</label>
                    <span class="stepDesc">
                        In&iacute;cio<br>
                        <small>Formatura / Contatos</small>
                    </span>
                </a>
            </li>
            <li>
                <a href="#step-2">
                    <label class="stepNumber">2</label>
                    <span class="stepDesc">
                        Endere&ccedil;o<br>
                        <small>&nbsp;</small>
                    </span>
                </a>
            </li>
            <li>
                <a href="#step-3">
                    <label class="stepNumber">3</label>
                    <span class="stepDesc">
                        Ades&atilde;o<br>
                        <small>Forma de Pagamento</small>
                    </span>
                </a>
            </li>
            <li>
                <a href="#step-4">
                    <label class="stepNumber">4</label>
                    <span class="stepDesc">
                        Dados de Acesso<br>
                        <small>&nbsp;</small>
                    </span>
                </a>
            </li>
        </ul>
        <div id="step-1">
            <div class="span11">
                <?php include('cadastro/pessoais.ctp'); ?>
            </div>
        </div>
        <div id="step-2">
            <div class="span11">
                <?php include('cadastro/endereco.ctp'); ?>
            </div>
        </div>
        <div id="step-3">
            <div class="span11">
                <?php include('cadastro/pagamento.ctp'); ?>
            </div>
        </div>
        <div id="step-4">
            <div class="span11">
                <?php include('cadastro/acesso.ctp'); ?>
            </div>
        </div>
    </div>
</div>