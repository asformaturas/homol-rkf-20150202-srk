<h1 class="grid_16 first" style="margin-top: 20px;border-bottom: 1px solid #000;">Campanhas<?php if ($this->params['action'] == 'planejamento_checkout' || $this->params['action'] == 'formando_checkout') { ?>	 				<a style="float:right;font-size:14px; margin-top:10px;">exibir<?php } ?></a><div style="clear: both;"></div></h1>
<div class="conteudo-container grid_16 checkout-secao-container" id='secao-campanha'>
	
	<?php
	if( sizeof($despesas_extras) == 0 ) {
		echo 'Nenhuma ades&atilde;o foi encontrada.';
	} else {
		
		$totalDeExtra['valorBase'] = 0;
		$totalDeExtra['multa'] = 0;
		$totalDeExtra['valorFinalDeDespesas'] = 0;
		$totalDeExtra['totalPago'] = 0;
		$totalDeExtra['totalDevido'] = 0;
		
		$adesoesDoUsuario = array();
		foreach ($despesas_extras as $despesa_extra):
			if ($despesa_extra['Despesa']['status'] != "cancelada") {
				$totalDeExtra['valorBase'] += $despesa_extra['Despesa']['valor'];
				$totalDeExtra['multa'] += $despesa_extra['Despesa']['multa'];
				$totalDeExtra['valorFinalDeDespesas'] += $despesa_extra['Despesa']['valor'] + $despesa_extra['Despesa']['multa'];
				if ($despesa_extra['Despesa']['status'] == "paga")
					$totalDeExtra['totalPago'] += $despesa_extra['Despesa']['valor'] + $despesa_extra['Despesa']['multa'];
				else if ($despesa_extra['Despesa']['status'] == "aberta" || $despesa_extra['Despesa']['status'] == "atrasada")
					$totalDeExtra['totalDevido'] +=  $despesa_extra['Despesa']['valor'] + $despesa_extra['Despesa']['multa'];
			}
				if (!isset($adesoesDoUsuario[$despesa_extra['CampanhasUsuario']['id']])) {
					$arrayDeExtrasComprados = array();
					foreach ($despesa_extra['CampanhasUsuario']['CampanhasUsuariosCampanhasExtras'] as $relacaoCampanhaUCampanhaE) {
						$idDaRelacaoCampanhaExtra = $relacaoCampanhaUCampanhaE['campanhas_extra_id'];
						$quantidadeCompradaDoExtra = $relacaoCampanhaUCampanhaE['quantidade'];
						$precoPagoNoExtra = $relacaoCampanhaUCampanhaE['valor_unitario'];
						$retirouExtra = $relacaoCampanhaUCampanhaE['retirou'];
						$nomeDoEvento = $hashIdDeExtraPorNomeDoEvento[$hashIdDeCampanhaExtraPorIdDeExtra[$idDaRelacaoCampanhaExtra]];
						$nomeDoExtra = $hashIdDeExtraPorNome[$hashIdDeCampanhaExtraPorIdDeExtra[$idDaRelacaoCampanhaExtra]];
						$arrayDeExtrasComprados[] = array('campanhasUsuariosCampanhasExtrasId' => $relacaoCampanhaUCampanhaE['id'],'nome' => $nomeDoExtra,'evento' => $nomeDoEvento,'quantidade' => $quantidadeCompradaDoExtra, 'retirou' => $retirouExtra, 'valorPagoPorUnidade' => $precoPagoNoExtra);
					}
					
					$adesoesDoUsuario[$despesa_extra['CampanhasUsuario']['id']]['campanha'] = $despesa_extra['CampanhasUsuario']['Campanha'];
					$adesoesDoUsuario[$despesa_extra['CampanhasUsuario']['id']]['extras'] = $arrayDeExtrasComprados;
					$adesoesDoUsuario[$despesa_extra['CampanhasUsuario']['id']]['despesas'] = array();
					
				} 
				
				$adesoesDoUsuario[$despesa_extra['CampanhasUsuario']['id']]['despesas'][] = $despesa_extra['Despesa'];
			endforeach;
			
	?>
	<!-- Exibir resumo de extras -->

	<h3 class="grid_16 first" style="margin-top: 15px; font-size: 16px;" >Resumo de Extras</h3>
	<div class="grid_16 first container-tabela-financeiro">
		<table class="tabela-financeiro tabela-boletos">
			<thead>
				<tr>
					<th scope="col">Evento</th>
					<th scope="col">Extra</th>
					<th scope="col">Total Em Aberto</th>
					<th scope="col">Total Pago</th>
					<th scope="col">Total Cancelado</th>
					<th scope="col">Total Retirado</th>
					<th scope="col">A Retirar</th>
					<th scope="col"> </th>
				</tr>
			</thead>
			<tbody>
			<?php $isOdd = false; 
			
				foreach($arrayResumoExtras as $extraResumo) :
			?>
				<?php if($isOdd):?>
					<tr class="odd linha-despesa">
				<?php else:?>
					<tr class="linha-despesa">
				<?php endif;?>
					<td  colspan="1" width="20%"><?php echo $extraResumo['evento_nome']; ?></td>
					<td  colspan="1" width="20%"><?php echo $extraResumo['extra_nome']; ?></td>
					<td  colspan="1" width="15%"><?php echo $extraResumo['total_em_aberto']; ?></td>
					<td  colspan="1" width="10%"><?php echo $extraResumo['total_pago']; ?></td>
					<td  colspan="1" width="10%"><?php echo $extraResumo['total_cancelado']; ?></td>
					<td  colspan="1" width="15%"><?php echo $extraResumo['total_retirado']; ?></td>
					<td  colspan="1" width="15%"><?php echo ($extraResumo['total_em_aberto'] + $extraResumo['total_pago']) - $extraResumo['total_retirado']; ?></td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach;?>
			</tbody>
		</table>
	</div>
	
	<?php 
	$arrayDeCampanhasEDespesaTotal = array();
	$campanha_odd = true;
	foreach($adesoesDoUsuario as $adesaoDoUsuario) : ?>
	
	<div class="grid_full container-campanha<?php echo $campanha_odd ? "-odd" : "";?>">
	<h2 class="grid_full first" style="margin-top: 20px;" id="campanha<?php echo $adesaoDoUsuario['campanha']['id'];?>"><b>&bull; <?php echo $adesaoDoUsuario['campanha']['nome'];?></b></h2>
	
	<div class="grid_16 first container-tabela-financeiro" style="margin-top: 20px;">
		<table class="tabela-financeiro tabela-boletos">
			<thead>
				<tr>
					<th scope="col">Evento</th>
					<th scope="col">Tipo de extra</th>
					<th scope="col">Quantidade</th>
					<th scope="col">Valor Unit&aacute;rio</th>
					<th scope="col">Total</th>
				</tr>
			</thead>
			<tbody>
			<?php $isOdd = false; ?>
			<?php 
				$totalExtrasCompradosNaCampanha = 0;
				$totalGastoNaCampanha = 0;	
				$totalPagoNaCampanha = 0;
				$campanhaCancelada = false;
				
				foreach($adesaoDoUsuario['despesas'] as $despesaDaAdesao) {
					if($despesaDaAdesao['status'] == 'cancelada')
						$campanhaCancelada = true;
					if ($despesaDaAdesao['status'] == 'paga')
						$totalPagoNaCampanha += $despesaDaAdesao['valor'];
					else if($despesaDaAdesao['status'] == 'cancelada')
						$totalCanceladoNaCampanha += $despesaDaAdesao['valor'];
				} 

				foreach($adesaoDoUsuario['extras'] as $extraDaAdesao) :
				
			?>
				<?php if($isOdd):?>
					<tr class="odd linha-despesa">
				<?php else:?>
					<tr class="linha-despesa">
				<?php endif;?>
					<td  colspan="1" width="20%"><?php echo $extraDaAdesao['evento']; ?></td>
					<td  colspan="1" width="20%"><?php echo $extraDaAdesao['nome']; ?></td>
					<td  colspan="1" width="20%"><?php echo $extraDaAdesao['quantidade']; $totalExtrasCompradosNaCampanha += $extraDaAdesao['quantidade'];?></td>
					<td  colspan="1" width="20%"><?php echo $extraDaAdesao['valorPagoPorUnidade']; ?></td>					
					<td  colspan="1" width="20%"><?php echo $extraDaAdesao['valorPagoPorUnidade']*(float)$extraDaAdesao['quantidade']; 
															$totalGastoNaCampanha += $extraDaAdesao['valorPagoPorUnidade']*$extraDaAdesao['quantidade']; ?></td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach;?>
				<tr class="linha-despesa" style="font-weight:bold; border-top: 1px solid #aaa;">
					<td  colspan="2" width="40%" style="text-align: left;">Totais</td>
					<td  colspan="1" width="20%"><?php echo $totalExtrasCompradosNaCampanha; ?></td>
					<td  colspan="1" width="20%"></td>
					<td  colspan="1" width="20%" style="color: red;"><?php echo $totalGastoNaCampanha; ?></td>
				</tr>
				<tr class="linha-despesa" style="font-weight:bold;" >
					<td  colspan="4" width="40%" style="text-align: left;">Valor pago</td>
					<td  colspan="1" width="20%" style="color: green;"><?php echo $totalPagoNaCampanha; ?></td>
				</tr>
				<tr class="linha-despesa" style="font-weight:bold; border-top: 1px solid #aaa;"">
					<td  colspan="4" width="40%" style="text-align: left;">Saldo(pago - gasto)</td>
					<td  colspan="1" width="20%" style="color: <?php echo ($totalPagoNaCampanha - $totalGastoNaCampanha >= 0) ? "green" : "red"?>;"><?php echo $totalPagoNaCampanha - $totalGastoNaCampanha; ?></td>
				</tr>
			</tbody>
		</table>
	</div>
	<?php 
		if ($totalGastoNaCampanha > $totalPagoNaCampanha && !$campanhaCancelada)
			$arrayDeCampanhasEDespesaTotal[] = array('campanha' => $adesaoDoUsuario['campanha']['id'], 'valorDevido' => ($totalGastoNaCampanha - $totalPagoNaCampanha));
	?>
	<h3 class="grid_16 first" style="margin-top: 15px; font-size: 14px;" >Pagamentos</h3>
	<div class="grid_16 first container-tabela-financeiro" style="margin-top: 15px;">
		<table class="tabela-financeiro tabela-boletos">
			<thead>
				<tr>
					<th scope="col">Data de Vencimento</th>
					<th scope="col">Valor base</th>
					<th scope="col">Data de Liquida&ccedil;&atilde;o</th>
					<th scope="col">Status</th>
				</tr>
			</thead>
			<tbody>
			<?php $isOdd = false; ?>
			<?php 
				$parcelaAtual = 0;
				$campanhaUsuarioId;
				foreach($adesaoDoUsuario['despesas'] as $despesaDaAdesao) :
				$campanhaUsuarioId = $despesaDaAdesao['campanhas_usuario_id'];
				$parcelaAtual++;
				if($despesaDaAdesao['status'] == 'paga' ) $totalPagoGeral += $despesaDaAdesao['valor'];
			?>
				<?php if($isOdd):?>
					<tr class="odd linha-despesa" id = "linha-despesa-<?php echo $despesaDaAdesao['id']; ?>" name="linha-despesa-<?php echo $despesaDaAdesao['id']; ?>">
				<?php else:?>
					<tr class="linha-despesa" id = "linha-despesa-<?php echo $despesaDaAdesao['id']; ?>" name="linha-despesa-<?php echo $despesaDaAdesao['id']; ?>">
				<?php endif;?>
					<td  colspan="1" width="25%"><?php echo date('d/m/Y', strtotime($despesaDaAdesao['data_vencimento'])); ?></td>					
					<td  colspan="1" width="25%"><?php echo $despesaDaAdesao['valor']; ?></td>
					<td  colspan="1" width="25%"><?php echo date('d/m/Y', strtotime($despesaDaAdesao['data_pagamento'])); ?></td>					
					<td  colspan="1" width="25%">
						<?php 
							if ($despesaDaAdesao['status'] == 'aberta')
								echo "<span style='color:red;'>aberto";
							else if ($despesaDaAdesao['status'] == 'paga' )
								echo "<span style='color:green;'>pago";
							else if ($despesaDaAdesao['status'] == 'vencida')
								echo "<span style='color:green;'>vencido";
							else 
								echo "<span style='color:orange;'>cancelado";
							
							echo "</span>";
						?>
					</td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach;?>
			</tbody>
		</table>
		<p style="margin-top: 10px;">	
			<?php echo $html->link('Cancelar campanha', array($this->params['prefix'] => true, 'controller' => 'formandos', 'action' =>'checkout_cancelar_campanha', $formando['FormandoProfile']['id'], $adesaoDoUsuario['campanha']['id'], $campanhaUsuarioId), array('id' => 'botao-cancelar-campanha', 'style' => 'font-size: 12px;color:red;float:right;margin: 2px 10px 0 0px;')); ?>
		</p>
	</div>
	</div>
	<?php
	$campanha_odd = !$campanha_odd;
	?>
	<?php endforeach; // Acabou Foreach Campanhas?>
	<h2 class="grid_16 first" style="margin-top: 10px;margin-bottom: 5px;"><b>Resumo financeiro de extras</b></h2>

	<div class="detalhes first grid_16" style="padding-left: 10px;">
		<p class="grid_16 alpha omega first">
			<span class="grid_full alpha omega"> Total de despesas (base + multas): R$ <?php echo $totalDeExtra['valorBase'];?> + R$ <?php echo $totalDeExtra['multa'];?> = R$ <?php echo ($totalDeExtra['valorBase'] + $totalDeAdesao['multa']);?> </span>
		</p>
		<p class="grid_16 alpha omega first">
			<span class="grid_full alpha omega"> Total de despesas pagas(Valor base + multa): <span style="color:blue;">R$ <?php echo $totalDeExtra['totalPago'];?> </span></span>
		</p>
		<p class="grid_16 alpha omega first">
			<span class="grid_full alpha omega"> Total de despesas abertas ou vencidas(Valor base + multa): <span style="color:red;">R$ <?php echo $totalDeExtra['totalDevido'];?> </span></span>
		</p>
	</div>
	<?php
	// Final do if que verifica se há despesas com extras 
	}
	?>
</div>
