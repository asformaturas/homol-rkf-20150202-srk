<h1 class="grid_16 first" style="margin-top: 20px;border-bottom: 1px solid #000;">Saldo
<?php if ($this->params['action'] == 'planejamento_checkout' || $this->params['action'] == 'formando_checkout') { ?>	 				<a style="float:right;font-size:14px; margin-top:10px;">exibir<?php } ?></a><div style="clear: both;"></div></h1>


<div id='secao-pagar' class="container-tabela-financeiro grid_16 first conteudo-container checkout-secao-container" style="margin-top: 10px;">
	<?php echo $form->create('Pagamento',array('id' => 'form-checkout', 'url' => "/{$this->params['prefix']}/formandos/checkout_salvar_pagamentos/{$idDoFormandoProfile}")); ?>

	<div class="grid_16 first alpha omega">
		<table class=" tabela-boletos"	>
	
				<tbody>
					<tr>
						<td  colspan="4" width="100%" style="text-align: left; font-weight: bolder;">Dívida contratual</td>					
					</tr>
					<tr style="text-align: left; font-weight: bolder;">
						<td  colspan="1" width="25%">Valor</td>
						<td  colspan="2" width="50%">Parcelas</td>					
						<td  colspan="1" width="25%"></td>
					</tr>
					<tr>					
						<td  colspan="1" width="25%" class='valor-devido valor-devido-igpm'><?php echo number_format($totalDevidoBaseComIGPM, 2, ',' , '.'); ?></td>
						<td  colspan="2" width="50%">
							<select id="select-numero-de-parcelas-adesao">
								<option value=1>À Vista</option>
								<option value=2>2 parcelas</option>
								<option value=3>3 parcelas</option>
								<option value=4>4 parcelas</option>
								<option value=5>5 parcelas</option>
								<option value=6>6 parcelas</option>
							</select>
						</td>					
						<td  colspan="1" width="25%">	
							<?php echo $html->link('Usar credito', '#', array('class' => 'submit button utilizar-credito' )); ?>
						</td>
					</tr>
					<tr>
						<td  colspan="4" width="100%" style="text-align: left; font-weight: bolder;">Ades&otilde;es a campanhas</td>					
					</tr>
					<tr style="text-align: left; font-weight: bolder;">
						<td  colspan="1" width="25%">Nome</td>					
						<td  colspan="1" width="25%">Valor</td>
						<td  colspan="1" width="25%">Parcelas</td>					
						<td  colspan="1" width="25%"></td>
					</tr>
					<?php 
						$totalDevidoEmCampanhas = 0;
						debug($arrayDeCampanhasEDespesaTotal);
					
						foreach($arrayDeCampanhasEDespesaTotal as $campanhaEDespesa) :
						 
							$totalDevidoEmCampanhas += $campanhaEDespesa['valorDevido'];
					?>
					<?php if($isOdd):?>
						<tr class="odd">
					<?php else:?>
						<tr>
					<?php endif;?>
						<td width="25%">Campanha: <?php echo $campanhaEDespesa['campanha'];?></td>
						<td width="25%" class='valor-devido'><?php echo $campanhaEDespesa['valorDevido'];?></td>
						<td  colspan="1" width="25%">À Vista</td>	
						<td width="25%">
						<?php echo $html->link('Usar credito', '#', array('class' => 'submit button utilizar-credito')); ?>
						</td>
					</tr>
					<?php $isOdd = !($isOdd); ?>
				<?php endforeach; ?>
				</tbody>
		</table>
	</div>
	<?php 
		$totalDevidoFinal = $totalDeAdesao['valorFinalDeDespesas'] + $totalDeExtra['valorFinalDeDespesas']; 
		$saldoFinal = $totalPago - $totalDevidoFinal;

		$creditos = $totalPago - $totalDeAdesao['totalPago'] - $totalDeExtra['totalPago'];
	?>
	<div class="grid_1 first alpha " style="margin-top: 20px;"> </div>
	<div class="grid_5 first alpha " style="margin-top: 20px;">
		<input type=hidden id="total-devido-adesao" value="<?php echo $totalDevidoBaseComIGPM; ?>"/>
		<input type=hidden id="total-devido-campanhas" value="<?php echo $totalDevidoEmCampanhas; ?>"/>
				
		<table class="tabela-checkout-resumo"	>
			<thead>
				<tr>			
                    <th scope="col" colspan="2">Resumo</th>
				</tr>
			</thead>
			<tbody id="">
				<tr>	
					<td width='60%' style="font-weight: bold;" class="tabela-checkout-resumo-final-esq">Total em Aberto:</td>
					<td width='40%' id="saldo-total-em-aberto" class="tabela-checkout-resumo-final-dir"><?php echo number_format($totalDevidoBaseComIGPM + $totalDevidoEmCampanhas, 2, ',', '.') ;?></td>	
				</tr>
				<tr>	
					<td width='60%' style="font-weight: bold;" class="tabela-checkout-resumo-final-esq">Crédito Acumulado:</td>
					<td width='40%' id="saldo-credito-acumulado"><?php echo number_format($creditos, 2, ',', '.'); ?>
					</td>	
				</tr>
				
				<tr>	
					<td width='60%' style="font-weight: bold;">Saldo:</td>
					<td width='40%' id="saldo-final">R$<?php echo ($totalDevidoBaseComIGPM + $totalDevidoEmCampanhas > 0) ? "-" : ""; echo number_format($totalDevidoBaseComIGPM + $totalDevidoEmCampanhas, 2 , ',' , '.'); ?></td>	
				</tr>
			</tbody>
		</table>
		
		<table class="tabela-valores-recomendados"	>
			<thead>
				<tr>			
                    <th scope="col" colspan="3">Valores recomendados:</th>
				</tr>
				<tr>			
                    <th scope="col" colspan="1">#</th>
                    <th scope="col" colspan="1">Prazo</th>
                    <th scope="col" colspan="1">Valor (R$)</th>
				</tr>
			</thead>
			<tbody id="body-tabela-valores-recomendados">
				<tr class="parcela-sugerida">
					<td width='10%'>1</td>	
					<td width='40%'>À Vista</td>
					<td width='40%'><?php echo $totalDevidoBaseComIGPM + $totalDevidoEmCampanhas; ?></td>	
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="3" align="center"><?php echo $html->link('Utilizar valores', '#', array('id' => 'utilizar-valores-sugeridos', 'class' => 'submit button')); ?> </td>
				</tr>
			</tfoot>
		</table>
		
						
		
	</div>
	

	<div class="grid_10  omega" style="margin-top: 20px;">
		<table class="tabela-financeiro tabela-boletos"	>
			<thead>
				<tr>			
                    <th scope="col" style= 'width: 15%'>Valor (R$)</th>
					<th scope="col" style= 'width: 15%'>Data de Liquidação</th>
                    <th scope="col" style= 'width: 10%'>Tipo</th>
					<th scope="col" style= 'width: 45%'>Observação</th>
					<th scope="col" style= 'width: 15%'></th>
				</tr>
			</thead>
			<tbody id="body-tabela-pagamentos">
			</tbody>
		</table>
	</div>
	<p style="margin-top: 20px;" class="grid_full first">	
		<span style = 'font-size: 12px;float:right;margin: 2px 10px 0 0px;'>
			<?php echo $html->link('Adicionar pagamento a lista', '#', array('id' => 'botao-adicionar-pagamento')); ?>
		</span>
	</p>
	</form>
</div>