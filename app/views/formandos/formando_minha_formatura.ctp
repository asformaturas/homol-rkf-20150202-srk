<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/metroui/listview.css">
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/metroui/table.css">
<script type="text/javascript">
$(document).ready(function() {
    $('#tab-dados li a[data-toggle="tab"]').on('show', function (e) {
        atualizaAltura($($(e.target).attr('href')).height()+100);
    });
})
</script>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Minha Formatura
        </h2>
    </div>
</div>
<br />
<div class="row-fluid">
    <div class="span12">
        <ul class="nav nav-tabs" id='tab-dados'>
            <li class="active">
                <a href="#turma" data-toggle="tab">Minha Turma</a>
            </li>
            <li>
                <a href="#contrato" data-toggle="tab">Dados Do Contrato</a>
            </li>
            <li>
                <a href="#informativos" data-toggle="tab">Informativos</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active fade in" id="turma">
                <br />
                <div class="row-fluid">
                    <div class='span6'>
                        <h3><?=count($formandos['aderidos'])?> Formandos Aderidos</h3>
                        <br />
                        <?php if(count($formandos['aderidos']) > 0) : ?>
                        <ul class='listview min'>
                            <?php foreach($formandos['aderidos'] as $formando) : ?>
                            <li class='bg-color-gray fg-color-white'>
                                <div class="icon">
                                    <img src='<?=$formando['ViewFormandos']['foto_perfil']?>' />
                                </div>
                                <div class="data">
                                    <h4 class='fg-color-white'><?=$formando['ViewFormandos']['nome']?></h4>
                                    <p>
                                        <?=$formando['ViewFormandos']['curso_nome']?>
                                    </p>
                                </div>
                            </li>
                            <?php endforeach; ?>
                        </ul>
                        <?php else : ?>
                        <h3 class='fg-color-red'>Essa Turma N&atilde;o Tem Formandos Aderidos</h3>
                        <?php endif; ?>
                    </div>
                    <div class='span6'>
                        <h3><?=count($formandos['nao_aderidos'])?> Formandos N&atilde;o Aderidos</h3>
                        <br />
                        <?php if(count($formandos['nao_aderidos']) > 0) : ?>
                        <ul class='listview min'>
                            <?php foreach($formandos['nao_aderidos'] as $formando) : ?>
                            <li class='bg-color-red fg-color-white'>
                                <div class="icon">
                                    <img src='<?=$formando['ViewFormandos']['foto_perfil']?>' />
                                </div>
                                <div class="data">
                                    <h4 class='fg-color-white'><?=$formando['ViewFormandos']['nome']?></h4>
                                    <p>
                                        <?=$formando['ViewFormandos']['curso_nome']?>
                                    </p>
                                </div>
                            </li>
                            <?php endforeach; ?>
                        </ul>
                        <?php else : ?>
                        <h3 class='fg-color-red'>Essa Turma N&atilde;o Tem Formandos N&atilde;o Aderidos</h3>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="contrato">
                <br />
                <div class='row-fluid'>
                    <div class='span6'>
                        <table class="table table-condensed table-striped">
                            <tbody>
                                <tr>
                                    <td class="header">
                                        Data De Assinatura De Contrato
                                    </td>
                                    <td>
                                        <?=date('d/m/Y',strtotime($turma['Turma']['data_assinatura_contrato']))?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="header">
                                        N&ordm; Esperado De Formandos
                                    </td>
                                    <td><?=$turma['Turma']['expectativa_formandos']?></td>
                                </tr>
                                <tr>
                                    <td class="header">
                                        Data De IGPM
                                    </td>
                                    <td>
                                        <?php if(!empty($turma['Turma']['data_igpm'])) : ?>
                                        <?=date('d/m/Y',strtotime($turma['Turma']['data_igpm']))?>
                                        <?php else : ?>
                                        IGPM N&atilde;o Aplicado
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="header">
                                        Data De Ades&atilde;o
                                    </td>
                                    <td>
                                        <?php if(!empty($formandoLogado['ViewFormandos']['data_adesao'])) : ?>
                                        <?=date('d/m/Y',strtotime($formandoLogado['ViewFormandos']['data_adesao']))?>
                                        <?php else : ?>
                                        Formando N&Atilde;o Aderido
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="header">
                                        Mesas De Contrato
                                    </td>
                                    <td>
                                        <?=$formandoLogado['ViewFormandos']['mesas_contrato']?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="header">
                                        Convites Contrato
                                    </td>
                                    <td>
                                        <?=$formandoLogado['ViewFormandos']['convites_contrato']?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="informativos">
                <h3 class='fg-color-red'>
                    Em Breve
                </h3>
            </div>
        </div>
    </div>
</div>