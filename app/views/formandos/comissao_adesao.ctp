<span id="conteudo-titulo" class="box-com-titulo-header">Fazer a Ades&atilde;o</span>
<div id="conteudo-container">
    <?php echo $session->flash();?>
    <?php 
    
    if($mostrar_formulario) { 
	    echo $form->create('Usuario', array('url' => '/' . $this->params['prefix'] . '/formandos/adesao', 'class' => 'form-cadastro-formando'));
	    if($mostrar_escolher_curso) {
		    echo '<p class="grid_12 alpha omega">';
			echo '<label class="grid_11 alpha omega">Curso</label>';
		    echo $form->input('FormandoProfile.curso_turma_id', array('type' => 'select', 'options' => $select_cursos, 'empty' => false,'class' => 'grid_10 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10' )));
		    echo '</p>';
	    }
	    if($is_pagante) {
	    	include('_form_pagamento.ctp');
	    	echo '<br />';
	    	echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'submit'));	
	    } else {
	    	echo '<br />';
	    	echo $form->input('adesao', array('type' => 'hidden'));
	    	echo $form->end(array('label' => 'Clique Aqui Para Fazer Sua Adesão', 'div' => false, 'class' => 'submit'));
	    }
	    
    } else {
    	echo '<h2>' . $mensagem . '</h2>';
    }
    ?>
</div>
