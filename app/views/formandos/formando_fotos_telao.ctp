<style type="text/css">
    .fileupload-exists.thumbnail img { cursor:pointer }
</style>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>js/jquery-fileupload/style.css">
<script src="<?= $this->webroot ?>js/jquery-fileupload/jquery.knob.js"></script>
<script src="<?= $this->webroot ?>js/jquery-fileupload/jquery.ui.widget.js"></script>
<script src="<?= $this->webroot ?>js/jquery-fileupload/jquery.iframe-transport.js"></script>
<script src="<?= $this->webroot ?>js/jquery-fileupload/jquery.fileupload.js"></script>
<script src="<?= $this->webroot ?>js/jquery-fileupload/script.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    
    $("#nome").keyup(function() {
        if($(this).val() != '')
            $(".enviar-nome").fadeIn(500);
        else
            $(".enviar-nome").fadeOut(500);
    });
    
    $("#enviar-nome").click(function(e) {
        e.preventDefault();
        bootbox.confirm('Confirma a alteracao do Nome?',function(response) {
            if(response) {
                var dados = {
                    data : {
                        nome : $("#nome").val()
                    }
                };
                var url = '/<?=$this->params['prefix']?>/formandos/alterar_nome_telao';
                var context = ko.contextFor($(".metro-button.reload")[0]);
                bootbox.hideAll();
                context.$data.showLoading(function() {
                    $.ajax({
                        url : url,
                        data : dados,
                        type : "POST",
                        dataType : "json",
                        complete : function() {
                            window.location.href = '/<?=$this->params['prefix']?>/';
                        }
                    });
                });
                return false;
            }
        });
    })
});
</script>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Fotos Para Tel&atilde;o
        </h2>
    </div>
</div>
<?php $session->flash(); ?>
<div class="alert alert-error bg-color-gray" style="border:none!important">
    Selecione as fotos que ser&atilde;o exibidas no tel&atilde;o no dia da sua festa
    <br />
    <span class="label label-info">Importante!</span>
    <br />
    Envie uma foto de boa qualidade.
    <br />
    Caso tenha dúvidas, uma referência  é o tamanho do arquivo,
    <br />
    sendo que  recomendamos que o mesmo tenha pelo menos 500kb.
    <br />
    <span class="label label-success">São aceitas imagens nos formatos .jpg, .jpeg e .tiff.</span>
    <br />
    <span class="label label-important">Arquivos nos formatos .png, .bmp, .doc, .pdf e outros não serão aceitos.</span>
</div>
<?php if(!empty($formando['FormandoProfile']['nome_telao'])) : ?>
<div class="row-fluid">
    <h3 class='fg-color-red' style='font-family:Arial; margin-bottom:5px'>
        Nome Pra Ser Exibido No Tel&atilde;o
    </h3>
    <h4><?=$formando['FormandoProfile']['nome_telao']?></h4>
</div>
<br />
<div class="row-fluid">
    <div class="span4">
        <?php if($fotoCrianca) { ?>
        <div class="file-upload">
            <div class="drop img">
                <img src="<?="{$this->webroot}{$fotoCrianca['FormandoFotoTelao']['arquivo']}"?>" />
            </div>
            <div class="alert alert-success">
                Foto Criança
            </div>
        </div>
        <?php } else { ?>
        <form class="file-upload iniciar" method="post" action="/formando/formandos/upload/crianca" enctype="multipart/form-data">
            <div class="drop">
                Foto Criança
                <a>Buscar</a>

                <input type="file" name="upl" multiple accept=".tiff,.jpeg,.jpg"/>
            </div>
            <a class="confirmar hide">Confirmar Envio</a>
            <ul>
                <!-- The file uploads will be shown here -->
            </ul>
        </form>
        <?php } ?>
    </div>
    <div class="span6">
        <?php if($fotoAdulto) { ?>
        <div class="file-upload">
            <div class="drop img">
                <img src="<?="{$this->webroot}{$fotoAdulto['FormandoFotoTelao']['arquivo']}"?>" />
            </div>
            <div class="alert alert-success">
                Foto Adulto
            </div>
        </div>
        <?php } else { ?>
        <form class="file-upload iniciar" method="post" action="/formando/formandos/upload/adulto" enctype="multipart/form-data">
            <div class="drop">
                Foto Adulto
                <a>Buscar</a>

                <input type="file" name="upl" multiple accept=".tiff,.jpeg,.jpg"/>
            </div>
            <a class="confirmar hide">Confirmar Envio</a>
            <ul>
                <!-- The file uploads will be shown here -->
            </ul>
        </form>
        <?php } ?>
    </div>
</div>
<?php else : ?>
<div class="alert alert-error">
    Antes de enviar as fotos voc&ecirc; deve confirmar o nome que dever&aacute; aparecer no tel&atilde;o
</div>
<br />
<div class="row-fluid">
    <h3 class='fg-color-red' style='font-family:Arial; margin-bottom:5px'>
        Nome Pra Ser Exibido No Tel&atilde;o
    </h3>
    <div class="row-fluid">
        <div class="span5">
            <input type="text" id='nome' value="<?=$formando['FormandoProfile']['nome_telao']?>" placeholder="Descri&ccedil;&atilde;o Da Foto" class="input-block-level" />
        </div>
        <div class="span3 hide enviar-nome hide">
            <a class='button mini bg-color-blue btn-file input-block-level' id="enviar-nome">
                Confirmar Altera&ccedil;&atilde;o De Nome
                <i class='icon-thumbs-up'></i>
            </a>
        </div>
    </div>
</div>
<?php endif; ?>
