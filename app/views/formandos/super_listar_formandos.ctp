<?php 
	$listaDeFormandosParaExcel = array();

	foreach ($formandos as $formando) {
		$listaDeFormandosParaExcel[] = array(
				"usuario_id" => $formando['ViewFormandos']['id'],
				"turma_id" =>  $formando['ViewFormandos']['turma_id'],
				"codigo_formando" => $formando['ViewFormandos']['codigo_formando'],
				"nome" => $formando['ViewFormandos']['nome'],
				"email" => $formando['ViewFormandos']['email'],
				"grupo" => $formando['ViewFormandos']['grupo'],
				"curso_id" => $formando['ViewFormandos']['curso_id'],
				"curso_nome" => $formando['ViewFormandos']['curso_nome'],
				"turno" => $formando['ViewFormandos']['turno'],
				"valor_adesao" => $formando['ViewFormandos']['valor_adesao'],
				"parcelas_adesao" => $formando['ViewFormandos']['parcelas_adesao'],
				"data_adesao" => $formando['ViewFormandos']['data_adesao'],
				"dia_vencimento_parcelas" => $formando['ViewFormandos']['dia_vencimento_parcelas'],
				"realizou_checkout" => $formando['ViewFormandos']['realizou_checkout'],
				"forma_pagamento" => $formando['ViewFormandos']['forma_pagamento'],
				"ativo" => $formando['ViewFormandos']['ativo'],
				"cpf" => $formando['ViewFormandos']['cpf'],
				"rg" => $formando['ViewFormandos']['rg'],
				"data_nascimento" => $formando['ViewFormandos']['data_nascimento'],
				"sexo" => $formando['ViewFormandos']['sexo'],
				"end_rua" => $formando['ViewFormandos']['end_rua'],
				"end_numero" => $formando['ViewFormandos']['end_numero'],
				"end_cep" => $formando['ViewFormandos']['end_cep'],
				"end_bairro" => $formando['ViewFormandos']['end_bairro'],
				"tel_residencial" => $formando['ViewFormandos']['tel_residencial'],
				"tel_comercial" => $formando['ViewFormandos']['tel_comercial'],
				"tel_celular" => $formando['ViewFormandos']['tel_celular'],
				"nome_mae" => $formando['ViewFormandos']['nome_mae'],
				"nome_pai" => $formando['ViewFormandos']['nome_pai'],
				"nextel" => $formando['ViewFormandos']['nextel'],
				"tam_camiseta" => $formando['ViewFormandos']['tam_camiseta'],
				"numero_havaiana" => $formando['ViewFormandos']['numero_havaiana'],
				"sala" => $formando['ViewFormandos']['sala'],
				"curso_turma_id" => $formando['ViewFormandos']['curso_turma_id'],
			);
	}
	$linha = "";
	foreach($listaDeFormandosParaExcel as $formando) {
		foreach($formando as $f)
			$linha .= "$f,";
		$linha = substr($linha,0,-1);
		echo "$linha<br />";
		$linha = "";
	}
	//ini_set('memory_limit', '-1');
	//$excel->generate($listaDeFormandosParaExcel, 'lista_formandos_turma_'.$turmaLogada['Turma']['id']); 
?>
