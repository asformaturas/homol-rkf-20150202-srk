<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#form-filtro").submit(function(e) {
        e.preventDefault();
        var context = ko.contextFor($(".metro-button.reload")[0]);
        context.$data.showLoading(function() {
            $("#form-filtro").find('[value="false"]').attr('disabled','disabled');
            var dados = $("#form-filtro").serialize();
            var url = $("#form-filtro").attr('action');
            $.ajax({
                url : url,
                data : dados,
                type : "POST",
                dataType : "json",
                complete : function() {
                    context.$data.reload();
                }
            });
        });
    });
    $(".finalizar").click(function() {
        var context = ko.contextFor($(".metro-button.reload")[0]);
        var id = $(this).attr('data-id');
        context.$data.showLoading(function() {
            var url = 'atendimento/formandos/finalizar_protocolo/' + id;
            $.ajax({
                url : url,
                type : "POST",
                dataType : "json",
                complete : function() {
                    context.$data.reload();
                }
            });
        });
    });
})
</script>
<?php $session->flash(); ?>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Turmas
        </h2>
    </div>
</div>
<?php
$paginator->options(array(
    'url' => array($this->params['prefix'] => true)));
$sortOptions = array('data-bind' => 'click: loadThis');
$buttonAfter = '<button class="helper" onclick="return false" ' .
    'tabindex="-1" type="button"></button>';
?>
<div class="row-fluid">
    <?=$form->create('ViewFormandos',array(
        'url' => "/atendimento/formandos/devolucao/",
        'id' => 'form-filtro')) ?>
    <div class="row-fluid">
        <div class="span2">
            <?=$form->input('ViewFormandos.codigo_formando',
                array(
                    'label' => 'Código Formando',
                    'div' => 'input-control text',
                    'error' => false,
                    'type' => 'text',
                    'after' => $buttonAfter
                )); ?>
        </div>
        <div class="span3">
            <label>&nbsp;</label>
            <button type='submit' class='mini max bg-color-red'>
                Filtrar
                <i class='icon-search-2'></i>
            </button>
        </div>
    </div>
    <?= $form->end(array('label' => false,
        'div' => false, 'class' => 'hide')); ?>
<?php if (sizeof($formandos) > 0) : ?>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="5%">
                    <?=$paginator->sort('Cód Formando', 'ViewFormandos.codigo_formando',$sortOptions); ?>
                </th>
                <th scope="col" width="20%">
                    <?=$paginator->sort('Nome', 'ViewFormandos.nome',$sortOptions); ?>
                </th>
                <th scope="col" width="10%">
                    <?=$paginator->sort('Protocolo', 'Protocolo.protocolo',$sortOptions); ?>
                </th>
                <th scope="col" width="5%">
                    <?=$paginator->sort('Banco', 'ProtocolosCancelamento.banco',$sortOptions); ?>
                </th>
                <th scope="col" width="5%">
                    <?=$paginator->sort('Agência', 'ProtocolosCancelamento.agencia',$sortOptions); ?>
                </th>
                <th scope="col" width="5%">
                    <?=$paginator->sort('Conta', 'ProtocolosCancelamento.conta',$sortOptions); ?>
                </th>
                <th scope="col" width="20%">
                    <?=$paginator->sort('Nome Titular', 'ProtocolosCancelamento.nome_titular',$sortOptions); ?>
                </th>
                <th scope="col" width="10%">
                    <?=$paginator->sort('CPF Titular', 'ProtocolosCancelamento.cpf_titular',$sortOptions); ?>
                </th>
                <th scope="col" width="10%">
                    <?=$paginator->sort('Valor', 'ProtocolosCancelamento.valor_cancelamento',$sortOptions); ?>
                </th>
                <th scope="col" width="10%">
                    <?=$paginator->sort('Data Cadastro', 'Protocolo.data_cadastro',$sortOptions); ?>
                </th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($formandos as $formando) : ?>
            <tr>
                <td><?=$formando['ViewFormandos']['codigo_formando']; ?></td>
                <td><?=$formando['ViewFormandos']['nome']; ?></td>
                <td><?=$formando['Protocolo']['protocolo']; ?></td>
                <td><?=$formando['ProtocolosCancelamento']['banco']; ?></td>
                <td><?=$formando['ProtocolosCancelamento']['agencia']; ?></td>
                <td><?=$formando['ProtocolosCancelamento']['conta']; ?></td>
                <td><?=$formando['ProtocolosCancelamento']['nome_titular']; ?></td>
                <td><?=$formando['ProtocolosCancelamento']['cpf_titular']; ?></td>
                <td>R$ <?=number_format($formando['ProtocolosCancelamento']['valor_cancelamento'], 2, ',', '.');?></td>
                <td><?=date('d/m/Y',strtotime($formando['Protocolo']['data_cadastro']));?></td>
                <td style="text-align:center">
                    <button class="button mini bg-color-blueDark finalizar" data-id="<?=$formando['Protocolo']['id']?>">
                        Finalizar
                    </button>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4" class="paginacao">
                    <?=$paginator->numbers(array('separator' => ' ',
                        'data-bind' => 'click: loadThis')); ?>
                </td>
                <td colspan="2">
                    <span class="label label-info">
                    <?=$paginator->counter(array(
                        'format' => 'Total : %count% ' .  $this->name)); ?>
                    </span>
                </td>
            </tr>
        </tfoot>
    </table>
<?php else : ?>
    <h2 class="fg-color-red">Nenhum formando encontrado.</h2>
<?php endif; ?>
</div>