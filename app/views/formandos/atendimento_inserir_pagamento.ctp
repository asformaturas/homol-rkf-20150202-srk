<script type="text/javascript">
$(document).ready(function() {
    $.Input();
    $("#form-filtro").submit(function(e) {
        e.preventDefault();
        var dados = $("#form-filtro").serialize();
        var url = $("#form-filtro").attr('action');
        $.ajax({
            url : url,
            data : dados,
            type : "POST",
            dataType : "json",
            complete : function() {
                $('#conteudo').append("<h2 class='fg-color-green'>Pagamento inserido com sucesso!")
            }
        });
    })
})
</script>
<div class='row-fluid'>
    <h2>Nome : <?=$formando['ViewFormandos']['nome']?></h2><br />
    <h2>Código : <?=$formando['ViewFormandos']['codigo_formando']?></h2><br />
</div>
<?=$form->create('ViewFormandos',array(
        'url' => "/atendimento/formandos/inserir_pagamento",
        'id' => 'form-filtro')) ?>
<?=$form->hidden('usuario_id',array('value' => $formando['ViewFormandos']['id']))?>
<div class="row-fluid">
    <div class="span4">
            <label>Valor</label>
            <?php echo $form->input('valor', array('class' => 'input-control text', 'label' => false, 
                'div' => false, 'error' => false)); ?>
    </div>
    <div class="span4">
        <?=$form->end(array('label' => 'Enviar',
            'div' => false, 'class' => 'bg-color-greenDark')); ?>
    </div>
</div>