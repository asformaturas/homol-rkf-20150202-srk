<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Formandos
        </h2>
    </div>
</div>
<?php $session->flash(); ?>
<div class="row-fluid">
<?php
    $paginator->options(array(
        'url' => array($this->params['prefix'] => true)));
    $sortOptions = array('data-bind' => 'click: loadThis');
    $buttonAfter = '<button class="helper" onclick="return false" ' .
        'tabindex="-1" type="button"></button>';
?>
<style type="text/css">
.chzn-container { line-height:16px }
.chzn-container-single .chzn-single div b { top:2px!important; }
.chzn-container-single .chzn-single span { position:relative; top:2px }
label { font-size: 13px; margin-bottom:5px; line-height: 16px }
.mini.max { font-size:12px; height:28px }
</style>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.selectpicker').selectpicker({width:'100%'});
    $.Input();
    $('.exibir').click(function() {
        dir = $(this).attr('dir');
        bootbox.dialog('Carregando',[{
            label: 'Fechar'
        }],{
            remote: '<?="/{$this->params['prefix']}/formandos/info/"?>'+
                dir
        });
    })
    $("#form-filtro").submit(function(e) {
        e.preventDefault();
        var context = ko.contextFor($(".metro-button.reload")[0]);
        context.$data.showLoading(function() {
            $("#form-filtro").find('[value="false"]').attr('disabled','disabled');
            var dados = $("#form-filtro").serialize();
            var url = $("#form-filtro").attr('action');
            $.ajax({
                url : url,
                data : dados,
                type : "POST",
                dataType : "json",
                complete : function() {
                    context.$data.reload();
                }
            });
        });
    })
})
</script>
    <?=$form->create('ViewFormandos',array(
        'url' => "/{$this->params['prefix']}/formandos/listar",
        'id' => 'form-filtro')) ?>
    <div class="row-fluid">
        <div class="span2">
            <label>Codigo</label>
            <?=$form->input('codigo_formando',
                array('label' => false, 'div' => 'input-control mini text',
                    'error' => false,'type' => 'text', 'after' => $buttonAfter)); ?>
        </div>
        <div class="span3">
            <label>Cursos</label>
            <?=$form->input('curso_id',array(
                'options' => $arraySelectDeCursos,
                'type' => 'select',
                'class' => 'selectpicker',
                'label' => false,
                'div' => false)); ?>
        </div>
        <div class="span3">
            <label>Situação</label>
            <?=$form->input('situacao',array(
                'options' => $selectSituacao,
                'type' => 'select',
                'class' => 'selectpicker',
                'label' => false,
                'div' => false)); ?>
        </div>
        <div class="span4">
            <label>&nbsp;</label>
            <button type='submit' class='mini max bg-color-red'>
                Filtrar
                <i class='icon-search-2'></i>
            </button>
            <a class='button mini bg-color-green pull-right' href='<?="/{$this->params['prefix']}/formandos/gerar_excel"?>'
                target='_blank'>
                Excel
                <i class='icon-file-excel'></i>
            </a>
        </div>
    </div>
    <?= $form->end(array('label' => false,
        'div' => false, 'class' => 'hide')); ?>
<?php if (sizeof($formandos) > 0) : ?>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col"><?=$paginator->sort('Cod', 'codigo_formando',$sortOptions); ?></th>
                <th scope="col"><?=$paginator->sort('Nome', 'nome',$sortOptions); ?></th>
                <th scope="col"><?=$paginator->sort('Email', 'email',$sortOptions); ?></th>
                <th scope="col"><?=$paginator->sort('Curso', 'curso_id',$sortOptions); ?></th>
                <th scope="col"><?=$paginator->sort('Situação', 'situacao',$sortOptions); ?></th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($formandos as $formando) : ?>
            <tr>
                <td width="10%"><?=$formando['ViewFormandos']['codigo_formando']; ?></td>
                <td width="30%"><?=$formando['ViewFormandos']['nome']; ?></td>
                <td width="20%"><?=$formando['ViewFormandos']['email']; ?></td>					
                <td width="15%"><?=$formando['ViewFormandos']['curso_nome']; ?></td>
                <td width="5%"><?=$formando['ViewFormandos']['situacao']; ?></td>
                <td width="20%">
                    <button class="mini exibir" type="button" dir="<?=$formando['ViewFormandos']['id']?>">
                        Visualizar
                    </button>
                      <a href="<?="/{$this->params['prefix']}/usuarios/entrar/{$formando['ViewFormandos']['id']}"?>" class="button mini bg-color-purple entrar">
                        Entrar
                    </a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4">
                    <?=$paginator->numbers(array('separator' => ' ','data-bind' => 'click: loadThis')); ?>
                </td>
                <td colspan="2">
                    <?=$paginator->counter(array('format' => 'Total : %count% ' .  $this->name)); ?>
                </td>
            </tr>
        </tfoot>
    </table>
<?php else : ?>
    <h2 class="fg-color-red">Nenhum Formando Encontrado</h2>
<?php endif; ?>
</div>