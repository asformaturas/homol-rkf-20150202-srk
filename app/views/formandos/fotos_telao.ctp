<!--header('Content-Type: text/html; charset=utf-8');-->
<style type="text/css">
    .fileupload-exists.thumbnail img { cursor:pointer }
    .esconder{ display: none!important }
</style>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>js/jquery-fileupload/style.css">
<script src="<?= $this->webroot ?>js/jquery-fileupload/jquery.knob.js"></script>
<script src="<?= $this->webroot ?>js/jquery-fileupload/jquery.ui.widget.js"></script>
<script src="<?= $this->webroot ?>js/jquery-fileupload/jquery.iframe-transport.js"></script>
<script src="<?= $this->webroot ?>js/jquery-fileupload/jquery.fileupload.js"></script>
<script src="<?= $this->webroot ?>js/jquery-fileupload/script.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    var koContext = ko.contextFor($("#content-body")[0]);
    // Catch the form submit and upload the files
    $('input[type=file]').on('change', prepareUpload);
 
    // Grab the files and set them to our variable
    function prepareUpload(event){
        if($(this).data('tipo') == 'crianca'){
                $('.confirmar.crianca').css('display','inline-block');
                files = event.target.files;
            }else{
                $('.confirmar.adulto').css('display','inline-block');
                files = event.target.files;
            }
        koContext.$data.loaded(true);
    }
    
    $('.confirmar').click(function() {
        var tipo = $(this).data('tipo');
        var context = ko.contextFor($("#content-body")[0]);
        if(tipo == 'crianca')
            var url = $('.crianca').attr('action');
        else
            var url = $('.adulto').attr('action');
        var arr = ["image/tiff", "image/jpg", "image/jpeg"];
        if($.inArray(files[0].type, arr) !== -1){
            event.stopPropagation(); // Stop stuff happening
            event.preventDefault(); // Totally stop stuff happening
            if(files[0].size <= 1412453){ 
                bootbox.confirm('Essa foto não poderá ser alterada. Confirma o envio da Foto?',function(response) {
                    if(response) {
                        var data = new FormData();
                        $.each(files, function(key, value)
                        {
                            data.append(key, value);
                        });
                        $.ajax({
                            url: url,
                            type: 'POST',
                            data: data,
                            cache: false,
                            processData: false, // Don't process the files
                            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                            success: function(data)
                            {
                                var d = data.split("<");
                                var response = JSON.parse(d[0]);
                                var drop = $('.drop.' + tipo);
                                $('.confirmar.' + tipo).css('display','none');
                                if(response.status == 'success' && response.src != undefined) {
                                    $('.confirmar.' + tipo).next().remove();
                                    drop.addClass('img');
                                    drop.html("<img src='/"+response.src+"' />");
                                    drop.after('<div class="alert alert-success">' +
                                        "Foto inserida com sucesso</div>");
                                } else {
                                    drop.after('<div class="alert alert-error">' +
                                        "Erro ao enviar foto</div>");
                                }
                            },
                            error: function(jqXHR, textStatus, errorThrown)
                            {
                                // Handle errors here
                                console.log('ERRORS: ' + textStatus);
                                // STOP LOADING SPINNER
                            }
                        });
                    }
                });
            }else{
                bootbox.alert("<h3 class='fg-color-red'>O tamanho do arquivo não pode ultrapassar <b>1.4MB</b>.");
            }
        }else{
            bootbox.alert("<h3 class='fg-color-red'>Extensão do arquivo incorreta, favor escolher uma imagem com extensão <b>.jpg, .jpeg</b> ou <b>.tiff</b>.");
        }
    });
    $('.excluir').click(function(){
       var id = $(this).attr('data-id');
       bootbox.alert("Tem certeza que deseja excluir esta foto?");
        var excluir = $("<button>",{
            type: 'submit',
            class: 'button bg-color-red',
            id:'excluir',
            text:'Excluir'
        });
        $('.modal-footer').prepend(excluir);
        $('.modal-footer').find($('.button.default')).addClass('bg-color-orangeDark').text('Cancelar');
        $('#excluir').click(function(){
            var context = ko.contextFor($(".metro-button.reload")[0]);
            var url = '<?="/formandos/excluir_foto_telao/"?>' + id;
            bootbox.hideAll();
            context.$data.showLoading(function() {
                $.ajax({
                    url : url,
                    type: "POST",
                    dataType : "json",
                    complete : function() {
                        context.$data.reload();
                    }
                });
            });
        });
    });
    $('.salvar').click(function(){
        var uid = $(this).attr('data-id');
        var dados = {
			data : {
				nome_telao : $("#nome_telao").val()
			}
		};
        var context = ko.contextFor($(".metro-button.reload")[0]);
        var url = '<?="/formandos/alterar_nome_telao/"?>' + uid;
        bootbox.hideAll();
        context.$data.showLoading(function() {
            $.ajax({
                url : url,
				data: dados,
                type: "POST",
                dataType : "json",
                complete : function() {
                    context.$data.reload();
                }
            });
        });
    });
});
</script>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Fotos Para Tel&atilde;o
        </h2>
    </div>
</div>
<?php $session->flash(); ?>
<?php
if($this->params['prefix'] == 'atendimento')
    $prefixo = 'atendimento';
else
    $prefixo = 'formando';
?>
<div class="alert alert-error bg-color-gray" style="border:none!important">
    Selecione as fotos que ser&atilde;o exibidas no tel&atilde;o no dia da sua festa
    <br />
    <span class="label label-info">Importante!</span>
    <br />
    Envie uma foto de boa qualidade.
    <br />
    Caso tenha dúvidas, uma referência  é o tamanho do arquivo,
    <br />
    sendo que  recomendamos que o mesmo tenha pelo menos 500kb.
    <br />
    <span class="label label-success">São aceitas imagens nos formatos .jpg, .jpeg e .tiff.</span>
    <br />
    <span class="label label-important">Arquivos nos formatos .png, .bmp, .doc, .pdf e outros não serão aceitos.</span>
</div>
<div class="row-fluid">
    <div class="span6">
        <h3 class='fg-color-red' style='font-family:Arial; margin-bottom:5px'>
            Nome Pra Ser Exibido No Tel&atilde;o
        </h3>
        <h4>
            <?php if($esconder == '' || $evento['Evento']['turma_id'] == 1579) : ?>
                <?php echo $form->input('nome_telao', array('class' => 'input-control text', 'value' => $formando['FormandoProfile']['nome_telao'],'label' => false, 'div' => false, 'error' => false)); ?>
                <button class="button mini bg-color-red salvar <?=$esconder?>" data-id="<?=$formando['FormandoProfile']['id']?>">
                    Salvar
                </button>
            <?php else : ?>
                <span class="fg-color-red">
                    <?=$formando['FormandoProfile']['nome_telao']?>
                </span>
            <?php endif; ?>
        </h4>
    </div>
</div>
<br />
<div class="row-fluid">
    <div class="span4">
        <?php 
            if($evento['Evento']['turma_id'] == 1579 && date('Y-m-d') <= '2019-03-06'){

                $esconder = '';
                $mostrar = 'mostrar';
            }
        ?>
        <?php if($fotoCrianca) { ?>
        <div class="file-upload">
            <div class="drop img">
                <img src="<?="{$this->webroot}{$fotoCrianca['FormandoFotoTelao']['arquivo']}"?>" />
            </div>
            <div class="alert alert-success">
                Foto Criança
            </div>
            <br />
            <button class="button bg-color-red excluir row-fluid <?=$esconder?>" data-id="<?=$fotoCrianca['FormandoFotoTelao']['id']?>">
                Excluir
            </button>
        </div>
        <?php } else { ?>
        <form class="file-upload iniciar crianca" method="post" action="/<?=$prefixo?>/formandos/upload/crianca/<?=$formando['FormandoProfile']['usuario_id']?>" enctype="multipart/form-data">
            <div class="drop crianca">
                Foto Criança
                <a class="<?=$esconder?>">Buscar</a>

                <input type="file" name="upl" data-tipo="crianca" accept=".jpg,.jpeg,.tiff"/>
            </div>
            <a class="confirmar crianca hide" data-tipo="crianca">Confirmar Envio</a>
            <ul>
                <!-- The file uploads will be shown here -->
            </ul>
        </form>
        <?php } ?>
    </div>
    <div class="span6">
        <?php if($fotoAdulto) { ?>
        <div class="file-upload">
            <div class="drop img">
                <img src="<?="{$this->webroot}{$fotoAdulto['FormandoFotoTelao']['arquivo']}"?>" />
            </div>
            <div class="alert alert-success">
                Foto Adulto
            </div>
            <br />
            <button class="button bg-color-red excluir row-fluid <?=$esconder?>" data-id="<?=$fotoAdulto['FormandoFotoTelao']['id']?>">
                Excluir
            </button>
        </div>
        <?php } else { ?>
        <form class="file-upload iniciar adulto" method="post" action="/<?=$prefixo?>/formandos/upload/adulto/<?=$formando['FormandoProfile']['usuario_id']?>" enctype="multipart/form-data">
            <div class="drop adulto">
                Foto Adulto
                <a class="<?=$esconder?>">Buscar</a>

                <input type="file" name="upl" data-tipo="adulto" accept=".jpg,.jpeg,.tiff"/>
            </div>
            <a class="confirmar adulto hide" data-tipo="adulto">Confirmar Envio</a>
            <ul>
                <!-- The file uploads will be shown here -->
            </ul>
        </form>
        <?php } ?>
    </div>
</div>
