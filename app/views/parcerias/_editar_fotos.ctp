<script type="text/javascript">
	function remover_foto(id) {
		var apagar = confirm("Tem certeza que deseja apagar essa foto?");

		if (apagar) {
			$.post('<?php echo $html->url(array($this->params['prefix'] => true, "controller" => "parcerias", "action" => "apagar_foto")) ?>', 
					{ data: { id: id } },
					function(data) {
						if (data == "sucesso") {
							$("#foto_" + id).remove();
						} else {
							alert('Erro ao remover foto.');
						}
					});
		}
	}
</script>

<?php 
	$first = false;
	$margem = false;
	$imagensExibidas = 0;
	foreach($parceria_banco['FotoParceria'] as $foto) {
		if ($first) {
			$first = false;
?>
			<div id="foto_<?php echo $foto['id']; ?>" class="grid_4 first" style= "margin-top: 20px;">
<?php 	} else { ?>
			<div id="foto_<?php echo $foto['id']; ?>" class="grid_4">
<?php 
		}
		echo $html->image($caminho_raiz_fotos.'/'.$foto['nome'], array('style' => 'width: 40%;'));
		echo $html->image('error.png', array('onclick' => 'remover_foto('.$foto['id'].')', 'style' => 'cursor: pointer; vertical-align: top;'));
		
		if ($imagensExibidas % 3 == 0 && $imagensExibidas != 0) {
			$first = true;
			$margem = true;
		}
		
		$imagensExibidas++;
?>
	</div>
<?php
	}
?>