<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/owl/owl.carousel.css">
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/tooltip.js?v=0.1"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/modal.js?v=0.1"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/dropdown.js?v=0.2"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/bootbox.js?v=0.2"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/owl/owl.carousel.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var categorias = $('.owl-carousel.categorias');
        var context = ko.contextFor($(".metro-button.back")[0]);
        categorias.owlCarousel({
            loop:true,
            margin:17,
            nav:true,
            lazyLoad : true,
            navText: ["",""],
            pullDrag : false,
            freeDrag : false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:4
                },
                1000:{
                    items:6
                }
            }
        });
        var novidades = $('.owl-carousel.novidades');
        novidades.owlCarousel({
            loop:true,
            margin:17,
            autoplay:true,
            autoplayTimeout:3000,
            autoplayHoverPause:true,
            nav:true,
            navText: ["<i class='icon-arrow-9'></i>","<i class='icon-arrow-6'></i>"],
            items : 1
        });
        //novidades.trigger('to.owl.carousel',[0]);
        novidades.on('translate.owl.carousel',function(e) {
            var parceria = $(".novidades > .owl-stage-outer > .owl-stage > .owl-item").eq(e.item.index);
            var fotos = parceria.find(".owl-carousel.novidade-fotos");
            if(!fotos.hasClass("carregadas")) {
                var id = parceria.find("[data-parceria]:first").data('parceria');
                fotos.on('translate.owl.carousel',function(e) {
                    e.stopPropagation();
                });
                fotos.addClass("carregadas").load("/formando/parcerias/parceria_fotos/" + id,function() {
                    fotos.owlCarousel({
                        loop:true,
                        margin:10,
                        mouseDrag : false,
                        nav:true,
                        navText: ["<i class='icon-arrow-13'></i>","<i class='icon-arrow-10'></i>"],
                        items : 2,
                        onInitialized : function() {
                            fotos.fadeIn(500);
                        }
                    });
                });
            }
        });
        $("#conteudo > div:first:empty").css('min-height','60px');
        $("[data-toggle='tooltip']").tooltip({
            container : 'body'
        });
        $('[data-parceria]').click(function() {
            bootbox.dialog('Carregando',[{
                label: 'Fechar',
                class: 'bg-color-red'
            }],{
                remote: '/formando/parcerias/show/' + $(this).data("parceria")
            });
        });
        $('#titulo').dropdown();
        $(".drop-categorias > .dropdown-menu > li > a").click(function(e) {
            e.preventDefault();
            var url = 'formando/parcerias/categoria/' + $(this).data("categoria");
            context.$data.page(url);
        });
    });
</script>
<style type="text/css">
[data-parceria]:hover { cursor:pointer }
.titulo-categoria { font-size: 1.8em }
.owl-carousel.categorias {
    padding: 0 10px;
    box-sizing: border-box;
    margin-bottom:50px;
}
.owl-carousel .owl-controls .owl-nav div {
    position: absolute;
    top:0;
    height:100%;
    background:transparent;
}
.owl-carousel.categorias .owl-controls .owl-nav div {
    position: absolute;
    top:-6%;
    height:112%;
    background:transparent;
    transition-duration: .5s;
    transition-property: all;
    width:60px;
}

.owl-carousel.categorias:hover .owl-controls .owl-nav div {
    background:rgba(180,180,180,.6);
}
.owl-carousel .owl-controls .owl-nav i { color:white }
.owl-carousel .owl-controls .owl-nav .owl-prev { left:0; }
.owl-carousel .owl-controls .owl-nav .owl-next { right:0; }
.owl-carousel.novidades .owl-controls .owl-nav i { position:relative; top:46%;
    font-size:32px; }
.owl-carousel.novidade-fotos .owl-controls .owl-nav div { height:auto; }
.owl-carousel.novidade-fotos .owl-controls .owl-nav i { font-size:22px; top:10px; }
.owl-carousel.novidade-fotos img { border:solid 2px white; box-sizing: border-box; }
.owl-carousel.novidade-fotos .owl-controls .owl-nav .owl-prev { left:-30px; }
.owl-carousel.novidade-fotos .owl-controls .owl-nav .owl-next { right:-30px; }
.owl-carousel.novidades { z-index:0 }
.owl-carousel.novidades .owl-controls .owl-nav .owl-next i { right: 0 }
.novidade-fotos img { margin-bottom: 10px; }
img.logo { border:solid 2px #F2F2F2 }
.novidade { padding:20px 65px 0 65px; box-sizing: border-box;
    background-color:rgb(40,40,40) }
#novidades { position:relative; min-height:200px; display:block;
    border:solid 4px rgb(40,40,40);
    background-color:rgb(40,40,40);
    border-radius:5px;
    box-sizing: border-box }
#carregando { position:absolute; width:100%; height:100%;
    background:white; z-index:1; background-color:rgb(40,40,40); }
#carregando h2 { color:gray }
#titulo { font-size:25px }
#titulo i { font-size:.8em; vertical-align: middle }
#titulo:hover { cursor:pointer }
.drop-categorias { display:inline-block; margin: 20px 0 }
.drop-categorias > .dropdown-menu { left:102%; top:-10px }
.drop-categorias > .dropdown-menu > li.active > a { color:white }
.item.dimmer { position:relative; }
.item.dimmer img { z-index:0 }
.item.dimmer .bg { position:absolute; left:2px; top:0; width:100%;
    height:100%; background:black; z-index:1; border-radius: 6px;
    text-align: center; order:solid 2px rgb(40,40,40);
    box-sizing: border-box; opacity:0;
    -webkit-transition: opacity .2s; transition: opacity .2s; }
.item.dimmer:hover .bg { opacity:.9; }
.item.dimmer .bg span { position:relative; top:50%; margin-top:-1em;
    font-size:1.5em; padding:7px 10px; }
</style>
<div class="row-fluid">
    <div class="dropdown drop-categorias">
        <label style="font-size: 28px">
            <a class="metro-button back" data-bind="click: function() { hideHomeButton() }"></a></a>&nbsp;Voltar
        </label>
        <span id="titulo" data-toggle="dropdown">
            <span>Escolha Uma Categoria</span>
            <i class='icon-arrow-6'></i>
        </span>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
            <?php foreach($menu as $item) : ?>
            <li>
                <a href="#" data-categoria="<?=$item["id"]?>"><?=$item["nome"]?></a>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <!--
    <div class="novidades" id="novidades">
        <div id="carregando">
            <h2 class="fg-color-white">Carregando</h2>
        </div>
        <div class="owl-carousel novidades"></div>
    </div>
    -->
    <div class="novidades" id="novidades">
        <div class="owl-carousel novidades">
            <?php foreach($novidades as $novidade) : ?>
            <div class="item row-fluid novidade fg-color-white">
                <div class="span5" data-parceria="<?=$novidade["Parceria"]["id"]?>">
                    <div class="row-fluid">
                        <?php if(!empty($novidade["Parceiro"]['logo'])) : ?>
                        <div class="span3">
                            <div style="padding-right:10px">
                                <img src="/fotos/crop/<?=base64_encode("img/parceiros/logos/{$novidade["Parceiro"]["logo"]}")?>/150/150/middle"
                                    class="img-rounded logo" />
                            </div>
                        </div>
                        <?php endif; ?>
                        <div class="span<?=!empty($novidade["Parceiro"]['logo']) ? '9' : '12'?>">
                            <h2 class='fg-color-white' style="line-height: 1em; font-size:2em; padding-top: 5px; margin:0; padding-left: 10px; font-size: 1.6em; font-weight: 400; margin:15px 0 20px;">
                                <?=$novidade["Parceria"]["titulo"]?>
                                <?php if(!empty($novidade["Parceria"]["desconto"])) : ?>
                                <br />
                                <em style="font-size:.7em" class="fg-color-red">
                                    <?=$novidade["Parceria"]["desconto"]?> off
                                 </em>
                                <?php endif; ?>
                            </h2>
                        </div>
                    </div>
                    <br />
                    <p>
                        <?=$novidade["Parceria"]["descricao"]?>
                        <br />
                        <br />
                        <strong>Local </strong><?=$novidade["Parceria"]["local"]?>
                        <?php if(!empty($novidade["Parceria"]["data_fim"])) : ?>
                        <br />
                        <strong>Finaliza em </strong><?=date('d/m/Y',strtotime($novidade["Parceria"]["data_fim"]))?>
                        <?php endif; ?>
                        <?php if(!empty($novidade["Parceria"]["observacoes"])) : ?>
                        <br />
                        <br />
                        <strong>Obs </strong><?=$novidade["Parceria"]["observacoes"]?>
                        <?php endif; ?>
                    </p>
                </div>
                <?php if(count($novidade["FotoParceria"]) > 0) : ?>
                <div class="span6 offset1 owl-carousel novidade-fotos"></div>
                <?php endif; ?>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<br />
<div class="row-fluid">
    <?php foreach($categorias as $categoria) : ?>
    <h2 class="titulo-categoria" style="padding-left: 10px; font-size: 1.6em; font-weight: 400; margin:15px 0 20px;">
        <?=$categoria["Categoria"]["nome"]?>
    </h2>
    <div class="owl-carousel categorias">
        <?php foreach($categoria["parcerias"] as $parceria) : ?>
        <div class="item dimmer" data-parceria="<?=$parceria["Parceria"]["id"]?>">
            <div class="bg" data-toggle="tooltip" data-title="<?=$parceria["Parceria"]["titulo"]?>">
                <span class="label bg-color-green">
                    Abrir
                </label>
            </div>
            <img src="/fotos/crop/<?=base64_encode("img/parceiros/logos/{$parceria["Parceiro"]["logo"]}")?>/145/145/middle"
                class="img-rounded logo" />
        </div>
        <?php endforeach; ?>
    </div>
    <?php endforeach; ?>
</div>