<script type="text/javascript">
    $(function() {
        $("#datepicker_data_inicio").datepicker( $.datepicker.regional[ "pt-BR" ] );
        $("#datepicker_data_fim").datepicker( $.datepicker.regional[ "pt-BR" ] );
        toggle_data_fim();
    });

    function toggle_data_fim() {
        if ($("#ParceriaSemDataFim").is(':checked')) {
        	$("#datepicker_data_fim").attr("disabled","disabled");
        	$("#datepicker_data_fim").val("");
        } else {
        	$("#datepicker_data_fim").removeAttr("disabled");
        }
    }

    function adicionar_campo_foto() {
		$("#campos_fotos").append(gerar_elemento());
		
	}

	function gerar_elemento() {
		indice = (get_ultimo_indice() + 1).toString();
		
		return '<p id="campo_foto_'+indice+'" class="full_grid alpha omega first">' +
					'<input type="file" id="FileFoto' + 
						indice + '" value="" error="" class="grid_6 alpha omega first" name="data[Fotos][foto' + 
						indice + ']">' +
						'<img src=\"<?php echo $this->webroot ?>img/error.png\" onclick=\"remover_campo_foto(' + indice + ')\" style=\"cursor: pointer;margin-left: 20px;\" />' +
					'</p>'
	}

	function get_ultimo_indice() {
		max = 0;

		$("#campos_fotos").children("p").each(function(index) {
		    id = $(this).attr('id');
		    idx = parseInt(id.charAt(id.length-1));
		    if (idx > max)
		        max = idx;
		});

		return max;
	}

	function remover_campo_foto(indice) {
		$("#campo_foto_" + indice).remove();
	}
</script>

<?php echo $form->input('id',  array('hiddenField' => true)); ?>

<p class="grid_11 alpha omega">
	<label class="full_grid alpha omega">Título</label>
	<?php echo $form->input('titulo', array(
			'class' => 'grid_8 alpha omega first', 'label' => false, 'div' => false, 
			'error' => array('wrap' => 'span', 'class' => 'grid_8 first')));
	?>
</p>

<p class="grid_11 alpha omega">
	<label class="full_grid alpha omega">Parceiro</label>
	<?php 
		echo $form->select('parceiro_id', $parceiros, empty($parceiro_id) ? null : $parceiro_id, array(
			'class' => 'grid_8 alpha omega first', 'label' => false, 'div' => false, 
			'empty' => (stristr($this->params['action'], 'editar') !== false) ? false : '-- Selecione um Parceiro --',
			'error' => array('wrap' => 'span', 'class' => 'grid_10')));
		
		echo $form->error('parceiro_id', array('wrap' => 'span', 'class' => 'grid_8 first'));
	?>
		
</p>

<p class="grid_11 alpha omega">
	<label class="full_grid alpha omega">Valor</label>
	<?php echo $form->input('valor', array(
			'class' => 'grid_4 alpha omega first', 'label' => false, 'div' => false, 
			'error' => array('wrap' => 'span', 'class' => 'grid_8 first')));
	?>
</p>

<p class="grid_11 alpha omega">
	<label class="full_grid alpha omega">Desconto</label>
	<?php echo $form->input('desconto', array(
			'class' => 'grid_6 alpha omega first', 'label' => false, 'div' => false, 
			'error' => array('wrap' => 'span', 'class' => 'grid_8 first')));
	?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Descrição</label>
	<?php echo $form->input('descricao', array(
			'class' => 'grid_8 first alpha omega', 
			'label' => false, 'div' => false, 
			'error' => array('wrap' => 'span', 'class' => 'grid_12 first')));
	?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Observações</label>
	<?php echo $form->input('observacoes', array(
			'class' => 'grid_8 first alpha omega', 
			'label' => false, 'div' => false, 
			'error' => array('wrap' => 'span', 'class' => 'grid_12 first')));
	?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha omega">Local</label>
	<?php echo $form->input('local', array(
			'class' => 'grid_8 alpha omega first', 
			'label' => false, 'div' => false, 
			'error' => array('wrap' => 'span', 'class' => 'grid_8 first')));
	?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Data de Início</label>
	<?php echo $form->input('data-inicio', array(
			'class' => 'grid_4 first alpha omega', 'label' => false, 'div' => false, 
			'error' => array('wrap' => 'span', 'class' => 'grid_10 first'), 
			'default' => empty($data_inicio) ? '' : $data_inicio,
			'id' => 'datepicker_data_inicio'));
	?>
</p>
<div class="grid_11 alpha omega">
	<p class="grid_4 alpha omega">
		<label class="full_grid alpha">Data de Fim</label>
		<?php echo $form->input('data-fim', array(
				'class' => 'grid_16 first alpha omega', 'label' => false, 'div' => false, 
				'error' => array('wrap' => 'span', 'class' => 'full_grid first'), 
				'default' => empty($data_fim) ? null : $data_fim,
				'id' => 'datepicker_data_fim'));
		?>
	</p>
	
	<p class="grid_5 omega">
		<label class="full_grid alpha">&nbsp;</label>
		<?php echo $form->checkbox('sem_data_fim', array('onchange' => 'toggle_data_fim();')); ?>
		
		Sem data de fim
	</p>
</div>

<p class="grid_11 alpha omega">
	<label class="grid_2 alpha">Pública</label>
	<span class="grid_2 alpha omega">
	<?php echo $form->checkbox('publica', array(
			'label' => false, 'div' => false, 
			'error' => array('wrap' => 'span', 'class' => 'grid_10')));
	?>
	</span>
	<span class="grid_10 first alpha" style="font-size: 8pt; display: block;">
		*Ao marcar a parceria como pública, ela estará disponível a todos os formandos, sem a necessidade de vincular uma turma.
	</span>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha omega">Status</label>
	<?php echo $form->select('ativa', array(1 => 'Ativa', 0 => 'Não Ativa'), null, array(
			'empty' => false, 
			'class' => 'grid_4 first alpha omega', 
			'label' => false, 'div' => false, 
			'error' => array('wrap' => 'span', 'class' => 'grid_8 first'))); 
	?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha omega">Fotos</label>
	
	<?php if (stristr($this->params['action'], 'editar') !== false) { ?>
		<div id="fotos" class="full_grid alpha omega first">
			<?php include('_editar_fotos.ctp'); ?>
		</div>
	<?php } ?>
	
	<div id="campos_fotos" class="full_grid alpha omega first" style="margin-top: 20px;">
		<p id="campo_foto_0" class="full_grid alpha omega first">
			<?php 
				echo $form->file('Fotos.foto0', array('class' => 'grid_6 alpha omega first', 'error' => array('wrap' => 'span', 'class' => 'grid_8 first')));
				echo $form->error('Fotos.foto', array('wrap' => 'span', 'class' => 'grid_8 first'));
				echo $html->image('error.png', array('onclick' => 'remover_campo_foto(0)', 'style' => 'cursor: pointer; vertical-align: top;margin-left: 20px;'))
			?>
		</p>
		<div style="clear:both"></div>
	</div>
	
	<?php echo $form->button('', array('type'=>'button', 'value' => 'Adicionar', 'class' => 'grid_3 first', 'onclick' => 'adicionar_campo_foto()'));  ?>
	
</p>