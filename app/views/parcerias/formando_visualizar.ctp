<script type="text/javascript">
 $(function() {
	$('#gallery a').lightBox();
 });
</script>

<span id="conteudo-titulo" class="box-com-titulo-header">Informações da Parceria</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<div class="detalhes container-parcerias">
	
		<div class="grid_4 alpha omega">
			<p class="grid_full alpha omega first">
				<?php if (!empty($parceria['Parceiro']['logo'])) {
						echo $html->image($caminho_raiz_logos.'/'.$parceria['Parceiro']['logo'], 
								array('style' => 'width: 85%; max-width: 250px; max-height: 250px; float:left;'));
					}
				?>
			</p>
			<?php echo $html->link('Adquira j&aacute; o seu voucher', 
				array($this->params['prefix'] => true, 'controller' => 'parcerias', 'action' =>'voucher', $parceria['Parceria']['id']), 
				array('escape' => false, 'class' => 'fundo-vermelho', 'target' => '_blank'), false); 
			?>
			<br />			
			<p style="margin-left: 20px; margin-top: 15px;">
				<label class="grid_full alpha omega">Endere&ccedil;o:</label>
				<span class="grid_full alpha first"> <?php echo $parceria['Parceria']['local'] ?> </span>
				
			</span>
		</div>
		<div class="grid_6 alpha omega">
			<h2 class="grid_full alpha first" style="color: #D31217; font-weight: bold; margin-bottom: 10px;">
				<?php echo $parceria['Parceria']['titulo'] ?>
			</h2>
			
			<p class="grid_full alpha omega">
				<label style="color: #D31217;">PROMO&Ccedil;&Atilde;O:</label>
				<?php 
					echo $parceria['Parceria']['descricao'];
				?>
			</p>
			
			
			<p class="grid_8 alpha omega first">
				<label class="grid_full alpha omega">
					<span class="fundo-vermelho">Data de In&iacute;cio </span>
				</label>
				<span class="grid_full alpha first" style="font-weight: bold">
					<?php echo date('d/m/Y', strtotime($parceria['Parceria']['data_inicio'])); ?>
				</span>
			</p>
			<?php if(!empty($parceria['Parceria']['valor'])) : ?>
			<p class="grid_8 alpha omega">
				<label class="grid_full alpha omega">
					<span class="fundo-vermelho">Valor</span>
				</label>
				<span class="grid_full alpha first" style="font-weight: bold">
					R$ <?php echo $parceria['Parceria']['valor'] ?>
				</span>
			</p>
			<?php endif; ?>
			<p class="grid_8 alpha omega first">
				<label class="grid_full alpha omega">
					<span class="fundo-vermelho">Data de Fim</span>
				</label>
				<span class="grid_full alpha first" style="font-weight: bold"> 
					<?php echo ($parceria['Parceria']['data_fim'] == "") ? "-" : date('d/m/Y', strtotime($parceria['Parceria']['data_fim'])); ?> 
				</span>
			</p>
			
			<p class="grid_8 alpha omega">
				<label class="grid_full alpha omega">
					<span class="fundo-vermelho">Desconto</span>
				</label>
				<span class="grid_full alpha first" style="font-weight: bold">
					<?php echo $parceria['Parceria']['desconto'] ?>
				</span>
			</p>
			
			<p class="grid_full alpha omega">
				<label class="grid_full alpha omega first" style="color: #D31217">OBSERVA&Ccedil;&Otilde;ES:</label>
				<span class="grid_full alpha first"> <?php echo $parceria['Parceria']['observacoes'] ?> </span>
			</p>
			
		</div>
		
		<div class="grid_6 alpha omega">
			<p class="grid_full alpha omega first">
				<label class="grid_11 alpha omega">
					<span class="icone-mais">+</span> FOTOS
				</label>
			</p>
			
			<div id="gallery">
				<?php 
					if (!empty($parceiro['FotoParceiro']) || !empty($parceria['FotoParceria'])) {
						foreach($parceiro['FotoParceiro'] as $foto) {
							$foto_tag = $html->image(
									$caminho_raiz_fotos_parceiro.'/'.$foto['nome'], 
									array('style' => 'max-height: 80px; max-width:100px; border: 0px;'));
							$foto_caminho_completo = $this->webroot.IMAGES_URL.$caminho_raiz_fotos_parceiro;
							
							echo '<a href="'.$foto_caminho_completo.'/'.$foto['nome'].'">'.$foto_tag.'</a>';
						}
						
						foreach($parceria['FotoParceria'] as $foto) {
							$foto_tag = $html->image(
									$caminho_raiz_fotos_parceria.'/'.$foto['nome'], 
									array('style' => 'max-height: 80px; max-width:100px; border: 0px;'));
							$foto_caminho_completo = $this->webroot.IMAGES_URL.$caminho_raiz_fotos_parceria;
							
							echo '<a href="'.$foto_caminho_completo.'/'.$foto['nome'].'">'.$foto_tag.'</a>';
						}
					}
				?>
			</div>
		
		</div>
		
		<p class="grid_16 alpha omega first">
			<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'controller' => 'parcerias', 'action' => 'index') ,array('class' => 'cancel')); ?>
		</p>
		
	</div>
</div>

<script language="javascript" type="text/javascript">
$(document).ready(function() {
	$(".slider-imagens").smoothDivScroll({ 
		mousewheelScrolling: true,
		manualContinuousScrolling: false,
		hotSpotScrolling : true,
		visibleHotSpotBackgrounds: "always",
		autoScrollingMode: ""
	});

	$('.scrollingHotSpotRightVisible').css('display', 'block');
});
</script>
