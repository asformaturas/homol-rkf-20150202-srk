<div class="item">
<?php foreach($fotos as $a => $foto) : ?>
<?php if(($a+1) % 3 == 0) : ?>
</div>
<div class="item">
    <img src="/fotos/crop/<?=base64_encode("img/parcerias/fotos/{$foto["FotoParceria"]["parceria_id"]}/{$foto["FotoParceria"]["nome"]}")?>/450/520"
        style="margin:0" class="img-rounded" />
<?php if(($a+1) != count($fotos)) : ?>
</div>
<div class="item">
<?php endif; ?>
<?php else : ?>
    <img src="/fotos/crop/<?=base64_encode("img/parcerias/fotos/{$foto["FotoParceria"]["parceria_id"]}/{$foto["FotoParceria"]["nome"]}")?>/450/250"
        class="img-rounded" />
<?php endif; ?>
<?php endforeach; ?>
</div>