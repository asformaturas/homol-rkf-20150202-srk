<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootstrap/select.css">
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/bootstrap/datepicker.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<style type="text/css">
#tab-content-dados { overflow:visible; }
.image-button { padding-right:10px; margin-right:40px }
.remover-curso { position: absolute; top:0; right:-35px; min-width:35px;
        width: 35px; height: 32px }
.remover-curso i { margin-left: 0!important; position:relative; top:12px }
.remover-curso:hover { background: #333333!important }
.row-fluid.adicionar-curso { margin-bottom:10px }
</style>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/bootstrap/datepicker.js"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/jquery.meio.mask.js"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/max/jquery.maskMoney.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        var context = ko.contextFor($("#content-body")[0]);
        var cursos = <?=json_encode($arvoreCursos)?>;
        var status = $("#status-turma").val();
        $("#arrecadar").maskMoney();
        $("#arrecadado").maskMoney();
        $("#total_contrato").maskMoney();
        $('#tab-dados li a[data-toggle="tab"]').on('show', function (e) {
            atualizaAltura($($(e.target).attr('href')).height()+200);
        });
        $('.datepicker').datepicker();
        $('input[alt]').setMask();
        $('.selectpicker').selectpicker({width:'100%'});
        $(".adicionar-beneficio").click(function() {
            var div = $(this).parents('.tab-pane').find(".div-beneficio.model").clone();
            div.removeClass('model').removeClass('hide').appendTo($(this).parents('.tab-pane'));
            context.$data.loaded(true);
        });
        $(".beneficios").on('click','.remover-beneficio',function(e) {
            var div = $(this).parents('.div-beneficio');
            div.fadeOut(500,function() {
                $(this).remove();
            });
        });
        $("#cursos").on('click','#adicionar-curso',function(e) {
            var exibir = $('.adicionar-curso').not('#confirmar-curso').not(':visible');
            $('.select-curso').hide();
            $(this).fadeOut(500,function() {
                $('#universidade').val('').selectpicker('refresh');
                exibir.fadeIn(500);
            });
        });
        $("#cursos").on('click','#cancelar-curso',function(e) {
            var ocultar = $('.adicionar-curso');
            $('.adicionar-curso').fadeOut(500,function() {
                $('#adicionar-curso').fadeIn(500);
            });
        });
        $("#cursos").on('change','#universidade',function(e) {
            var universidade = $(this).val();
            $('#confirmar-curso').fadeOut(500);
            $('.select-curso.curso').fadeOut(500);
            $('.select-curso.periodo').fadeOut(500);
            $("#faculdade").html('<option value="">Selecione</option>');
            $.each(cursos[universidade], function(i,v) {
                if(parseInt(i) == i)
                    $('#faculdade').append('<option value="' + 
                        v['id'] + '">'+ 
                        v['nome'] + '</option>');
            });
            $("#faculdade").selectpicker('refresh');
            $('.select-curso.faculdade').fadeIn(500);
        });
        $("#cursos").on('change','#faculdade',function(e) {
            var universidade = $('#universidade').val();
            var faculdade = $(this).val();
            $('#confirmar-curso').fadeOut(500);
            $('.select-curso.periodo').fadeOut(500);
            $("#curso").html('<option value="">Selecione</option>');
            $.each(cursos[universidade][faculdade], function(i,v) {
                if(parseInt(i) == i)
                    $('#curso').append('<option value="' + 
                        v['id'] + '">'+ 
                        v['nome'] + '</option>');
            });
            $("#curso").selectpicker('refresh');
            $('.select-curso.curso').fadeIn(500);
        });
        $("#cursos").on('change','#curso',function(e) {
            $('#confirmar-curso').fadeOut(500);
            $('#periodo').val('').selectpicker('refresh');
            $('.select-curso.periodo').fadeIn(500);
        });
        $("#cursos").on('change','#periodo',function(e) {
            if($(this).val() != 0)
                $('#confirmar-curso').fadeIn(500);
            else
                $('#confirmar-curso').fadeOut(500);
        });
        $("#cursos").on('click','#confirmar-curso',function(e) {
            var uId = $("#universidade").val();
            var fId = $("#faculdade").val();
            var cId = $("#curso").val();
            var periodo = $("#periodo").val();
            var adicionar = true;
            $(".button-curso").each(function() {
                if($(this).data('universidade') == uId &&
                    $(this).data('faculdade') == fId &&
                    $(this).data('curso') == cId &&
                    $(this).data('periodo') == periodo) {
                    adicionar = false;
                    bootbox.alert('Curso ja existe pra essa turma');
                }
            });
            if(adicionar) {
                var universidade = cursos[uId]['nome'];
                var faculdade = cursos[uId][fId]['nome'];
                var curso = cursos[uId][fId][cId]['nome'];
                var cursoLabel = curso + ' - ' + periodo +
                    ' (' + faculdade + ',' + universidade +
                    ')';
                var button = $("<button>",{
                    type: 'button',
                    class: 'image-button hide button-curso adicionado',
                    'data-periodo': periodo,
                    'data-universidade': uId,
                    'data-faculdade': fId,
                    'data-curso': cId,
                    text: cursoLabel
                });
                <?php if($usuario['Usuario']['nivel'] == 'administrador' &&
                            $usuario['Usuario']['grupo'] == 'comercial') : ?>
                $("<span>",{
                    class:'bg-color-blue remover-curso',
                    html: '<i class="icon-close"></i>'
                }).appendTo(button);
                <?php endif; ?>
                var qtde = $('.button-curso.adicionado').length;
                $("<input>",{
                    type: 'hidden',
                    name: 'data[cursos][adicionar]['+qtde+'][curso_id]',
                    value: cId
                }).appendTo(button);
                $("<input>",{
                    type: 'hidden',
                    name: 'data[cursos][adicionar]['+qtde+'][turno]',
                    value: periodo
                }).appendTo(button);
                button.appendTo($('.lista-cursos'));
                button.fadeIn(500,function() {
                    context.$data.loaded(true);
                    $("#cancelar-curso").trigger('click');
                });
            }
            context.$data.loaded(true);
        });
        $("#cursos").on('click','.remover-curso',function(e) {
            var button = $(this).parent();
            bootbox.confirm('Tem Certeza Que Deseja Excluir Esse Curso?',function(response) {
                if(response) {
                    if(!button.hasClass('adicionado')) {
                        var qtde = $('.curso-removido').length;
                        if($(".button-curso").length > 1) {
                            var select = $("<select>",{
                                id:'formandos-curso-removido',
                                html:'<option value="">Selecione</option>'
                            });
                            $(".button-curso").each(function() {
                                if($(this).data('curso-turma') != button.data('curso-turma'))
                                    select.append('<option value="' + $(this).data('curso') +
                                        ';' + $(this).data('periodo') + '">' +
                                        $(this).text() + '</option>');
                            });
                            select.append('<option value="nenhum;nenhum">Nenhum</option>');
                            var html = $("<p>",{
                                html:'<label>Passar Formandos Desse Curso Para Qual Curso?</label>'
                            }).append(select);
                            bootbox.dialog(html,[{
                                label: 'Excluir',
                                class: 'bg-color-red',
                                callback: function() {
                                    if($("#formandos-curso-removido").val() != "") {
                                        $("<input>",{
                                            type: 'hidden',
                                            class: 'curso-removido',
                                            name: 'data[cursos][remover]['+qtde+'][id]',
                                            value: button.data('curso-turma')
                                        }).appendTo($('.lista-cursos'));
                                        $("<input>",{
                                            type: 'hidden',
                                            name: 'data[cursos][remover]['+qtde+'][formandos]',
                                            value: $("#formandos-curso-removido").val()
                                        }).appendTo($('.lista-cursos'));
                                        button.fadeOut(500,function() {
                                            button.remove();
                                        });
                                    } else {
                                        $(".modal-body").find(' p > label')
                                            .addClass('fg-color-red')
                                            .text('Selecione Um Curso');
                                        return false;
                                    }
                                }
                            },{
                                label: 'Cancelar'
                            }]);
                        } else {
                            $("<input>",{
                                type: 'hidden',
                                class: 'curso-removido',
                                name: 'data[cursos][remover]['+qtde+'][id]',
                                value: button.data('curso-turma')
                            }).appendTo($('.lista-cursos'));
                            button.fadeOut(500,function() {
                                button.remove();
                            });
                        }
                    } else {
                        button.fadeOut(500,function() {
                            button.remove();
                        });
                    }
                }
            });
        });
        $("#formulario-turma").submit(function(e) {
            e.preventDefault();
            if(status != $("#status-turma").val() && $("#status-turma").val() == 'descartada') {
                var div = $("<div>",{
                    html: "<h2>Selecione a concorrente vencedora do contrato</h2><br />"
                });
                $("#concorrente-vencedor").clone().attr({
                    id: 'concorrente-vencedor-box',
                    class : ''
                }).appendTo(div);
                bootbox.dialog(div.html(),[{
                    label: 'Confirmar',
                    class: 'bg-color-blue',
                    callback: function() {
                        if($("#concorrente-vencedor-box").val() == "") {
                            $("#concorrente-vencedor-box").focus();
                            return false;
                        } else {
                            $("#concorrente-vencedor").val($("#concorrente-vencedor-box").val());
                            atualizarTurma();
                        }
                    }
                },{
                    label: 'Cancelar',
                    class: 'bg-color-red'
                }]);
            } else {
                if(status != $("#status-turma").val() && $("#status-turma").val() == 'fechada') {
                    var data = new Date();
                    var hoje = data.getDate() + "/" + (data.getMonth()+1) + "/" + data.getFullYear();
                    $("#data-assinatura").val(hoje);
                }
                atualizarTurma();
            }
            return false;
        });
        
        $('#select-usuarios').change(function(){
            var div = $("#div-usuarios");
            div.html('');
            if($(this).val() != null)
                $.each($(this).val(),function(i,u) {
                    div.append($("<span>",{
                        class : "label",
                        text : $('#select-usuarios').find("option[value='"+u+"']").text()
                    }));
                });
        });
        
        $('#usuarios').on('click','.remover-usuario',function() {
            var tr = $(this).parents("tr");
            if($(this).attr("data-removido") == 0) {
                tr.find("td:not(:last)").css("text-decoration","line-through");
                $("#formulario-turma").append($("<input>",{
                    type : "hidden",
                    name : "data[remover_usuarios][]",
                    value : $(this).data("id"),
                    class : "usuario-" + $(this).data("id")
                }));
                $(this).attr("data-removido",1).text("Não Remover").removeClass("bg-color-red");
            } else {
                tr.find("td:not(:last)").css("text-decoration","none");
                $(".usuario-" + $(this).data("id")).remove();
                $(this).attr("data-removido",0).text("Remover").addClass("bg-color-red");
            }
        });
        
        $('.tipo-contrato').on('change', function(){
            var id = $(this).val();
            $('.info-contratos').removeClass('hide').load('/turmas/obterLayoutContrato/' + id).fadeIn(800);
            atualizaAltura($('#conteudo').height()+300);
        });

        function atualizarTurma() {
            div = $('#vendedores');
            
            div.find('tbody > tr').each(function(i){
                $("#vendedores").append($("<input>",{
                    type : "hidden",
                    name : "data[adicionar_comissao]["+ i +"][usuario_id]",
                    value : $(this).children('td').find('input').attr('data-id')
                }));
                $("#vendedores").append($("<input>",{
                    type : "hidden",
                    name : "data[adicionar_comissao]["+ i +"][porcentagem]",
                    value : $(this).children('td').find('input').val()
                }));
            });
            var context = ko.contextFor($(".metro-button.reload")[0]);
            context.$data.showLoading(function() {
                var dados = $("#formulario-turma").serialize();
                var url = $("#formulario-turma").attr('action');
                $.ajax({
                    url : url,
                    data : dados,
                    type : "POST",
                    dataType : "json",
                    complete : function() {
                        context.$data.reload();
                    }
                });
            });
        }
    })
</script>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            <?="{$turma['Turma']['nome']} - {$turma['Turma']['id']}"?>
        </h2>
    </div>
</div>
<?php $session->flash(); ?>
<?=$form->create('Turma', array(
    'url' => "/{$this->params['prefix']}/turmas/editar_dados",
    'id' => 'formulario-turma'
)); ?>
<?=$form->hidden('Turma.id'); ?>
<div class="row-fluid">
    <ul class="nav nav-tabs" id='tab-dados'>
        <li class="active">
            <a href="#turma" data-toggle="tab">Dados Da Turma</a>
        </li>
        <li>
            <a href="#cursos" data-toggle="tab">Cursos</a>
        </li>
        <li>
            <a href="#beneficios-formandos" data-toggle="tab">Benef&iacute;cios Formando</a>
        </li>

        <li>
            <a href="#beneficios-comissao" data-toggle="tab">Benef&iacute;cios Comiss&atilde;o</a>
        </li>
        <?php if($this->params['prefix'] == in_array($usuario['Usuario']['grupo'], array('comercial', 'planejamento', 'super')) &&
                 $usuario['Usuario']['nivel'] != 'basico') : ?>
        <li>
            <a href="#usuarios" data-toggle="tab">Usuários Vinculados</a>
        </li>
        <?php endif; ?>
    </ul>
    <div class="tab-content" id="tab-content-dados">
        <div class="tab-pane active fade in" id="turma">
            <?php if($this->data['Turma']['status'] == 'descartada') : ?>
            <h3>Essa turma foi fechada com a concorr&ecirc;cia</h3>
            <h4 class="fg-color-red">
            <?php if(!empty($this->data['Concorrente']['nome'])) : ?>
                Concorrente: <?=$this->data['Concorrente']['nome']?>
            <?php else : ?>
                Concorrente n&atilde;o cadastrado
            <?php endif; ?>
            </h4>
            <br />
            <?php else : ?>
            <?=$form->input('Turma.concorrente_id',
                array(
                    'id' => 'concorrente-vencedor',
                    'options' => $concorrentes,
                    'type' => 'select',
                    'class' => 'hide',
                    'label' => false,
                    'div' => 'input-control text',
                    'error' => false
                )); ?>
            <?php endif; ?>
            <div class="row-fluid">
                <div class="span3">
                    <?=$form->input('Turma.adesoes', array(
                        'options' => $adesoes,
                        'type' => 'select',
                        'class' => 'selectpicker',
                        'label' => 'Adesoes',
                        'error' => false,
                        'div' => 'input-control text')); ?>
                </div>
                <div class="span9">
                    <?=$form->input('Turma.nome', array(
                        'label' => 'Nome',
                        'error' => false,
                        'div' => 'input-control text')); ?>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span3">
                    <?=$form->input('Turma.status', array(
                        'options' => $statuses,
                        'type' => 'select',
                        'class' => 'selectpicker',
                        'label' => 'Status',
                        'id' => 'status-turma',
                        'error' => false,
                        'div' => 'input-control text')); ?>
                </div>
                <div class="span3">
                    <?=$form->input('Turma.ano_formatura', array(
                        'options' => $anoFormatura,
                        'type' => 'select',
                        'class' => 'selectpicker',
                        'label' => 'Ano Formatura',
                        'error' => false,
                        'div' => 'input-control text')); ?>
                </div>
                <div class="span3">
                    <?=$form->input('Turma.semestre_formatura', array(
                        'options' => $semestreFormatura,
                        'type' => 'select',
                        'class' => 'selectpicker',
                        'label' => 'Semestre Formatura',
                        'error' => false,
                        'div' => 'input-control text')); ?>
                </div>
                <div class="span3">
                    <?=$form->input('data-assinatura', array(
                        'class' => 'datepicker',
                        'label' => 'Data Assinatura',
                        'id' => 'data-assinatura',
                        'alt' => '99/99/9999',
                        'error' => false,
                        'div' => 'input-control text')); ?>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span6">
                    <div class="row-fluid">
                        <div class="span6">
                            <?=$form->input('Turma.expectativa_formandos', array(
                                'label' => 'Expectativa Formandos',
                                'error' => false,
                                'div' => 'input-control text')); ?>
                        </div>
                        <div class="span6">
                            <?=$form->input('Turma.como_chegou_id', array(
                                'options' => $como_chegous,
                                'type' => 'select',
                                'class' => 'selectpicker',
                                'label' => 'Como Chegou',
                                'error' => false,
                                'div' => 'input-control text')); ?>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span6">
                            <?=$form->input('Turma.tipos_contrato_id', array(
                                'options' => $tiposContrato,
                                'type' => 'select',
                                'class' => 'selectpicker tipo-contrato',
                                'label' => 'Tipo de Contrato',
                                'error' => false,
                                'div' => 'input-control text')); ?>
                        </div>
                        <div class="span6">
                            <?=$form->input('Turma.valor_total_contrato', array(
                                'label' => 'Val Total Contrato',
                                'div' => 'input-control text',
                                'id' => 'total_contrato',
                                'error' => false)); ?>
                        </div>
                    </div>
                </div>
                <div class="span6">
                    <div class="row-fluid">
                        <div class="span6">
                            <?=$form->input('Turma.checkout', array(
                                'options' => $checkout,
                                'type' => 'select',
                                'class' => 'selectpicker',
                                'label' => 'Checkout',
                                'error' => false,
                                'div' => 'input-control text')); ?>
                        </div>
                        <div class="span6">
                            <?=$form->input('Turma.plano_economico', array(
                                'options' => $cascata,
                                'type' => 'select',
                                'class' => 'selectpicker',
                                'label' => 'Plano Cascata',
                                'error' => false,
                                'div' => 'input-control text')); ?>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span6">
                            <?=$form->input('Turma.convites_contrato', array(
                                'label' => 'Convites Contrato',
                                'div' => 'input-control text',
                                'error' => false)); ?>
                        </div>
                        <div class="span6">
                            <?=$form->input('Turma.mesas_contrato', array(
                                'label' => 'Mesas Contrato',
                                'div' => 'input-control text',
                                'error' => false)); ?>
                        </div>
                    </div>
                </div>
                <div class="info-contratos">
                    <?php if(!empty($layout)) : 
                            include("{$layout}");
                          endif; 
                    ?>
                </div>
                <div class="row-fluid">
                    <div class="span6">
                        <?=$form->input('Turma.multa_por_atraso', array(
                            'label' => 'Multa',
                            'div' => 'input-control text',
                            'error' => false)); ?>
                    </div>
                    <div class="span6">
                        <?=$form->input('Turma.juros_mora', array(
                            'label' => 'Mora',
                            'div' => 'input-control text',
                            'error' => false)); ?>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span6">
                    <div class="row-fluid">
                        <div class="span12">
                            <?=$form->input('Turma.como_chegou_detalhes', array(
                                'label' => 'Detalhes de Como Chegou',
                                'error' => false,
                                'rows' => '5',
                                'div' => 'input-control textarea',
                                'style' => 'height: 114px')); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="cursos">
            <div class="row-fluid">
                <div class="span3">
                    <button type="button" class="default" id="adicionar-curso">
                        Adicionar Curso
                    </button>
                    <button type="button" class="bg-color-blue hide adicionar-curso" id="confirmar-curso">
                        Confirmar
                    </button>
                    <button type="button" class="bg-color-red hide adicionar-curso" id="cancelar-curso">
                        Cancelar
                    </button>
                </div>
            </div>
            <?php
                $universidades = array('' => 'Selecione Uma Universidade');
                foreach ($arvoreCursos as $universidadeId => $arrayFaculdades)
                    $universidades[$universidadeId] = $arrayFaculdades['nome'];
            ?>
            <div class="row-fluid hide adicionar-curso">
                <div class="span4">
                    <?=$form->input('universidade_id', array(
                        'options' => $universidades,
                        'type' => 'select',
                        'class' => 'selectpicker',
                        'label' => 'Universidade',
                        'data-live-search' => 'true',
                        'data-size' => '4',
                        'id' => 'universidade',
                        'error' => false,
                        'div' => 'input-control text')); ?>
                </div>
                <div class="span3 select-curso faculdade">
                    <?=$form->input('fauldade_id', array(
                        'type' => 'select',
                        'class' => 'selectpicker',
                        'label' => 'Faculdade',
                        'data-live-search' => 'true',
                        'data-size' => '5',
                        'id' => 'faculdade',
                        'error' => false,
                        'div' => 'input-control text')); ?>
                </div>
                <div class="span3 select-curso curso">
                    <?=$form->input('curso_id', array(
                        'type' => 'select',
                        'class' => 'selectpicker',
                        'label' => 'Curso',
                        'data-size' => '5',
                        'id' => 'curso',
                        'error' => false,
                        'div' => 'input-control text')); ?>
                </div>
                <div class="span2 select-curso periodo">
                    <?=$form->input('periodo', array(
                        'type' => 'select',
                        'options' => array_merge(array('Selecione'),$periodo),
                        'class' => 'selectpicker',
                        'label' => 'Período',
                        'id' => 'periodo',
                        'error' => false,
                        'div' => 'input-control text')); ?>
                </div>
            </div>
            <div class="row-fluid lista-cursos">
                <?php foreach ($cursosRelacionados as $curso): ?>
                <?php
                    $cursoLabel = $curso['Curso']['nome'];
                    if(!empty($curso['CursoTurma']['turno']))
                        $cursoLabel.= " - {$curso['CursoTurma']['turno']}";
                    $cursoLabel.= " ({$curso['Faculdade']['nome']},{$curso['Universidade']['nome']})";
                ?>
                <button type="button" class="image-button button-curso"
                    data-periodo="<?=$curso['CursoTurma']['turno']?>"
                    data-universidade="<?=$curso['Universidade']['id']?>"
                    data-faculdade="<?=$curso['Faculdade']['id']?>"
                    data-curso="<?=$curso['CursoTurma']['curso_id']?>"
                    data-curso-turma="<?=$curso['CursoTurma']['id']?>">
                    <?=$cursoLabel?>
                    <span class="bg-color-blue remover-curso">
                        <i class="icon-close"></i>
                    </span>
                </button>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="tab-pane beneficios fade" id="beneficios-formandos">
            <h3>Benef&iacute;cios dos formandos</h3>
            <br />
            <div class="row-fluid">
                <button type="button" class="default adicionar-beneficio">
                    Adicionar
                </button>
            </div>
            <div class="row-fluid div-beneficio model hide">
                <div class="span10">
                    <div class="input-control text">
                        <textarea name="data[Turma][beneficios_formandos][]" rows="3"
                            class="texto-beneficio"
                            style="min-height:initial"></textarea>
                    </div>
                </div>
                <div class="span2">
                    <button type="button" class="button mini default remover-beneficio">
                        Remover
                    </button>
                </div>
            </div>
            <?php foreach($this->data['Turma']['beneficios_formandos'] as $beneficio) : ?>
            <div class="row-fluid div-beneficio">
                <div class="span10">
                    <div class="input-control text">
                        <textarea name="data[Turma][beneficios_formandos][]" rows="3"
                            class="texto-beneficio"
                            style="min-height:initial"><?=$beneficio?></textarea>
                    </div>
                </div>
                <div class="span2">
                    <button type="button" class="button mini default remover-beneficio">
                        Remover
                    </button>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
        <div class="tab-pane beneficios fade" id="beneficios-comissao">
            <div class="row-fluid">
                <button type="button" class="default adicionar-beneficio">
                    Adicionar
                </button>
            </div>
            <div class="row-fluid div-beneficio model hide">
                <div class="span10">
                    <div class="input-control text">
                        <textarea name="data[Turma][beneficios_comissao][]" rows="3"
                            class="texto-beneficio"
                            style="min-height:initial"></textarea>
                    </div>
                </div>
                <div class="span2">
                    <button type="button" class="button mini default remover-beneficio">
                        Remover
                    </button>
                </div>
            </div>
            <?php foreach($this->data['Turma']['beneficios_comissao'] as $beneficio) : ?>
            <div class="row-fluid div-beneficio">
                <div class="span10">
                    <div class="input-control text">
                        <textarea name="data[Turma][beneficios_comissao][]" rows="3"
                            class="texto-beneficio"
                            style="min-height:initial"><?=$beneficio?></textarea>
                    </div>
                </div>
                <div class="span2">
                    <button type="button" class="button mini default remover-beneficio">
                        Remover
                    </button>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
        <div class="tab-pane beneficios fade" id="anexo-g">
            <div class="row-fluid">
                <div class="span4">
                    <?=$form->input('Turma.valor_arrecadado_comissao', array(
                        'label' => 'Valor Arrecadado pela Comissão',
                        'error' => false,
                        'id' => 'arrecadado',
                        'div' => 'input-control')); ?>
                </div>
                <div class="span4">
                    <?=$form->input('Turma.valor_arrecadar_formandos', array(
                        'label' => 'Valor Arrecadar pelos Formandos',
                        'error' => false,
                        'id' => 'arrecadar',
                        'div' => 'input-control')); ?>
                </div>
            </div>
        </div>
        <div class="tab-pane beneficios fade" id="anexo-d">
            <div class="row-fluid">
                <div class="span6">
                    <div class="row-fluid">
                        <div class="span12">
                            <?=$form->input('Turma.forma_pagamento_anexo_d', array(
                                'label' => 'Forma de Pagamento - Anexo D',
                                'error' => false,
                                'rows' => '5',
                                'div' => 'input-control textarea',
                                'style' => 'height: 114px')); ?>
                        </div>
                    </div>
                </div>
                <div class="span6">
                    <div class="row-fluid">
                        <div class="span12">
                            <?=$form->input('Turma.itens_adicionais_anexo_d', array(
                                'label' => 'Itens Adicionais - Anexo D',
                                'error' => false,
                                'rows' => '5',
                                'div' => 'input-control textarea',
                                'style' => 'height: 114px')); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span6">
                    <div class="row-fluid">
                        <div class="span12">
                            <?=$form->input('Turma.informacoes_adicionais_anexo_e', array(
                                'label' => 'Informações Adicionais - Anexo E',
                                'error' => false,
                                'rows' => '5',
                                'div' => 'input-control textarea',
                                'style' => 'height: 114px')); ?>
                        </div>
                    </div>
                </div>
                <div class="span6">
                    <div class="row-fluid">
                        <div class="span12">
                            <?=$form->input('Turma.eventos_anexo_a', array(
                                'label' => 'Eventos - Anexo A',
                                'error' => false,
                                'rows' => '5',
                                'div' => 'input-control textarea',
                                'style' => 'height: 114px')); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="usuarios">
            <div class="row-fluid">
                <div class='span7'>
                    <table class="table table-condensed table-striped">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Departamento</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach($turma['Usuario'] as $u): ?>
                            <tr>
                                <td><?="{$u['nome']} - {$u['email']}"; ?></td>
                                <td><?="{$u['grupo']}"; ?></td>
                                <td>
                                    <button class="mini default bg-color-red remover-usuario"
                                        type="button"
                                        data-removido="0"
                                        data-id="<?=$u['id']?>">
                                        Remover
                                    </button>
                                </td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
                <div class='span5'>
                    <?php
                    $selectUsuarios = array();
                    foreach ($usuarios as $i => $usuario) {
                        if(empty($selectUsuarios[$usuario['grupo']]))
                            $selectUsuarios[$usuario['grupo']] = array();
                        $selectUsuarios[$usuario['grupo']][$usuario['id']] = "{$usuario['nome']} - {$usuario['email']}";
                    }
                    ?>
                    <?=$form->input('Turma.adicionar_usuarios', array(
                        'options' => $selectUsuarios,
                        'type' => 'select',
                        'id' => 'select-usuarios',
                        'class' => 'selectpicker select-usuarios',
                        'data-container' => 'body',
                        'multiple' => 'multiple',
                        'label' => 'Adicionar',
                        'div' => 'input-control text')); ?>
                    <div id="div-usuarios">
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="vendedores">
            <div class="row-fluid">
                <div class='span7'>
                    <table class="table table-condensed table-striped">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Porcentagem</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach($usuariosComercial as $u): ?>
                            <tr>
                                <td><?="{$u['Usuario']['nome']} - {$u['Usuario']['email']}"; ?></td>
                                <td><input data-id="<?=$u['Usuario']['id']?>" type="number" value="<?=!empty($u['ComissaoVendedor']['porcentagem']) ? $u['ComissaoVendedor']['porcentagem'] : 0?>" step="0.01"/></td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>  
    <br />
    <button type="submit" class="bg-color-blue">
        Atualizar
    </button>
</div>
<?=$form->end(array('label' => false, 'div' => false, 'style' => 'display:none')); ?>
