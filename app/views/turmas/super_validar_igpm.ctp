<span id="conteudo-titulo" class="box-com-titulo-header">
    Turmas
</span>
<div id="conteudo-container">
    <?php $session->flash(); ?>
    <?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
    <div class="tabela-adicionar-item">
        <?=$form->create(false,array(
            'url' => "/{$this->params['prefix']}/turmas/validar_igpm",
            'class' => 'procurar-form-inline'
        )); ?>
        M&ecirc;s: <?=$form->input('mes',array(
            'options' => $meses,
            'type' => 'select',
            'empty' => 'Mês',
            'class' => 'grid4 alpha omega',
            'label' => false,
            'div' => false
        )); ?>
        &nbsp;&nbsp;&nbsp;De: <?=$form->input('periodo-ini',array(
            'options' => $periodo,
            'type' => 'select',
            'empty' => 'Selecione',
            'class' => 'grid6 alpha omega',
            'label' => false,
            'div' => false
        )); ?>
        At&eacute;: <?=$form->input('periodo-fim',array(
            'options' => $periodo,
            'type' => 'select',
            'empty' => 'Selecione',
            'class' => 'grid6 alpha omega',
            'label' => false,
            'div' => false
        )); ?>
        <?=$form->end(array('label' => ' ', 'div' => false, 'class' => 'submit-busca')); ?>
        <strong style="float:right">Clique no id para ver os formandos da Turma</strong>
        <div style="clear:both;"></div>
    </div>
    <div class="container-tabela">
        <table>
            <thead>
                <tr>
                    <th scope="col" width="10%"><?= $paginator->sort('Id', 'id'); ?></th>
                    <th scope="col" width="25%"><?= $paginator->sort('Nome', 'nome'); ?></th>
                    <th scope="col" width="15%"><?= $paginator->sort('Data Contrato', 'data_assinatura_contrato'); ?></th>
                    <th scope="col" width="10%"><?= $paginator->sort('Ano', 'ano'); ?></th>
                    <th scope="col" width="10%"><?= $paginator->sort('Formandos', 'formandos'); ?></th>
                    <th scope="col" width="20%"><?= $paginator->sort('Data Ultimo IGPM', 'data_ultima_aplicacao'); ?></th>
                    <th scope="col" width="10%"><?= $paginator->sort('Despesas Com IGPM', 'despesas_igpm'); ?></th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="5"><?= $paginator->counter(array('format' => 'Total : %count% ' . $this->name)); ?></td>
                </tr>
            </tfoot>
            <tbody>
                <?php $isOdd = false; ?>
                <?php foreach ($turmas as $turma): ?>
                    <tr>
                        <td><?=$turma[0]['formandos'] > 0 ? $html->link($turma['Turma']['id'],
                                array($this->params['prefix'] => true,
                                    'controller' => 'turmas',
                                    'action' =>'formandos_igpm',
                                    $turma['Turma']['id']
                                )) : '';?></td>
                        <td><?= $turma['Turma']['nome']; ?></td>	
                        <td>
                            <?php
                            if ($paginator->validarData($turma['Turma']['data_assinatura_contrato'], 'Y-m-d'))
                                echo date('d/m/Y', strtotime($turma['Turma']['data_assinatura_contrato']));
                            else
                                echo "Data invalida";
                            ?>
                        </td>
                        <td><?= "{$turma['Turma']['ano_formatura']}.{$turma['Turma']['semestre_formatura']}"; ?></td>
                        <td><?= $turma[0]['formandos']; ?></td>
                        <td><?= !empty($turma[0]['data_ultima_aplicacao']) ? date('d/m/Y', strtotime($turma[0]['data_ultima_aplicacao'])) : 'Não Aplicado' ?></td>
                        <td><?= $turma[0]['despesas_igpm'] ?></td>
                    </tr>
                    <?php $isOdd = !($isOdd); ?>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
