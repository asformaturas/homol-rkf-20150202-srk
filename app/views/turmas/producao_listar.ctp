<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.selectpicker').selectpicker();
    $.Input();
    $('#content-body').tooltip({ selector: '[rel=tooltip]'});
    $("#form-filtro").submit(function(e) {
        e.preventDefault();
        var context = ko.contextFor($(".metro-button.reload")[0]);
        context.$data.showLoading(function() {
            $("#form-filtro").find('[value="false"]').attr('disabled','disabled');
            var dados = $("#form-filtro").serialize();
            var url = $("#form-filtro").attr('action');
            $.ajax({
                url : url,
                data : dados,
                type : "POST",
                dataType : "json",
                complete : function() {
                    context.$data.reload();
                }
            });
        });
    });
})
</script>
<?php $session->flash(); ?>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Turmas
        </h2>
    </div>
</div>
<?php
$paginator->options(array(
    'url' => array('producao'=> true)));
$sortOptions = array('data-bind' => 'click: loadThis');
$buttonAfter = '<button class="helper" onclick="return false" ' .
    'tabindex="-1" type="button"></button>';
?>
<div class="row-fluid">
    <?=$form->create('Turma',array(
        'url' => "/producao/turmas/listar",
        'id' => 'form-filtro')) ?>
    <div class="row-fluid">
        <div class="span2">
            <?=$form->input('Turma.id',
                array(
                    'label' => 'ID',
                    'div' => 'input-control text',
                    'error' => false,
                    'type' => 'text',
                    'after' => $buttonAfter
                )); ?>
        </div>
        <div class="span4">
            <?=$form->input('Turma.nome',
                array(
                    'label' => 'Nome',
                    'div' => 'input-control text',
                    'error' => false,
                    'type' => 'text',
                    'after' => $buttonAfter
                )); ?>
        </div>
        <div class="span3">
            <label>&nbsp;</label>
            <button type='submit' class='mini max bg-color-red'>
                Filtrar
                <i class='icon-search-2'></i>
            </button>
        </div>
    <?= $form->end(array('label' => false,
        'div' => false, 'class' => 'hide')); ?>
    </div>
<?php if (sizeof($turmas) > 0) : ?>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="10%">
                    <?=$paginator->sort('ID', 'Turma.id',$sortOptions); ?>
                </th>
                <th scope="col" width="30%">
                    <?=$paginator->sort('Nome', 'Turma.nome',$sortOptions); ?>
                </th>
                <th scope="col" width="10%">
                    <?=$paginator->sort('Ano', 'ano',$sortOptions); ?>
                </th>
                <th scope="col" width="10%">
                    <?=$paginator->sort('Comissoes', 'comissoes',$sortOptions); ?>
                </th>
                <th scope="col" width="10%">
                    <?=$paginator->sort('Formandos', 'formandos',$sortOptions); ?>
                </th>
                <th scope="col" width="10%">
                    <?=$paginator->sort('Status', 'Turma.status',$sortOptions); ?>
                </th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($turmas as $turma) : ?>
            <tr>
                <td><?=$turma['Turma']['id']; ?></td>
                <td><?=$turma['Turma']['nome']; ?></td>
                <td><?="{$turma['Turma']['ano_formatura']}.{$turma['Turma']['semestre_formatura']}"; ?></td>
                <td><?=$turma[0]['comissoes']; ?></td>
                <td><?=$turma[0]['formandos']; ?></td>
                <td><?=$turma['Turma']['status']; ?></td>
                <td style="text-align:center">
                    <a class="button mini visualizar default bg-color-red" type="button"
                        href="/turmas/selecionar/<?=$turma['Turma']['id']; ?>">
                        Selecionar
                    </a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="6" class="paginacao">
                    <?=$paginator->numbers(array('separator' => ' ',
                        'data-bind' => 'click: loadThis')); ?>
                </td>
                <td colspan="5">
                    <span class="label label-info">
                    <?=$paginator->counter(array(
                        'format' => 'Total : %count% ' .  $this->name)); ?>
                    </span>
                </td>
            </tr>
        </tfoot>
    </table>
<?php else : ?>
    <h2 class="fg-color-red">Nenhuma Turma Encontrada.</h2>
<?php endif; ?>
</div>