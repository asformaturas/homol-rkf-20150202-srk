<style type="text/css">
    .mapa { position: fixed; z-index: 1000!important; width: 425px; height: 350px; margin-left: 180px }
    .icon-help { margin-right: 4px; margin-top: 0px; }
    .icon-close { margin-right: -10px!important; cursor: pointer }
    iframe { margin-left: -12px; height: 350px; width: 425px }
    h5 { text-align: center; padding-top: 5px}
    h3 { color: white; padding: 5px}
    label { text-align: center }
    .label-danger { background-color: #b91d1d!important; color: white }
    .table thead tr *, tr.header>*, td.header { background-color: black!important }
</style>
<script type="text/javascript">
$(document).ready(function(){
    $( ".modal-body" ).on('mouseenter', '.icon-help', function() {
        $(".mapa").fadeIn( 800 );
    });
    $( ".modal-body" ).on('click', '.icon-close', function() {
        $(".mapa").fadeOut( 800 );
    });
});
</script>
<div class="mapa hide">
    <div class="span5">
        <div class="well">
            <ul class="nav nav-list">
                <li class="active">
                    <a>Mapa<i class="icon-close pull-right"></i></a>
                </li>
            </ul>
            <ul class="nav nav-list scrollable">
                <?=$evento['Evento']['local_mapa'];?>
            </ul>
        </div>
    </div>
</div>
<div class="row-fluid" style="background-color: black">
    <h3 align="center">
        <?=$turma['Turma']['id'] . " - " . $turma['Turma']['nome'] . " - " . $turma['Turma']['ano_formatura'] .
                "." . $turma['Turma']['semestre_formatura'];?>
    </h3>
</div>
<br />
<br />
<div class="row-fluid">
    <div class="span12">
        <div class="span3">
            <label class="label-danger">
                Expectativa Formandos
            </label>
        </div>
        <div class="span3">
            <h5>
                <?=empty($turma['Turma']['expectativa_formandos']) ? "<em class='fg-color-red'>Não Cadastrado</em>" : $turma['Turma']['expectativa_formandos']?>
            </h5>
        </div>
        <div class="span3">
            <label class="label-danger">
                Tipo de Evento
            </label>
        </div>
        <div class="span3">
            <h5>
                <?=$evento['TiposEvento']['nome'];?>
            </h5>    
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="span3">
            <label class="label-danger">
                Comissões Ativas
            </label>
        </div>
        <div class="span3">
            <h5>
                <?=$comissoes?>
            </h5> 
        </div>
        <div class="span3">
            <label class="label-danger">
                Data do Evento
            </label>
        </div>
        <div class="span3">
            <h5>
                <?=$evento['Evento']['data'] > 0 ? date('d/m/Y', strtotime($evento['Evento']['data'])) : "<em class='fg-color-red'>Não Cadastrado</em>";?>
            </h5> 
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="span3">
            <label class="label-danger">
                Formandos Aderidos
            </label>
        </div>
        <div class="span3">
            <h5>
                <?=$formandos?>
            </h5> 
        </div>
        <div class="span3">
            <label class="label-danger">
                Hora do Início
            </label>
        </div>
        <div class="span3">
            <h5>
                <?=date("H:i", strtotime($evento['Evento']['data'])) > 0 ? date('H:i', strtotime($evento['Evento']['data'])) : "<em class='fg-color-red'>Não Cadastrado</em>";?>
            </h5> 
        </div>        
    </div>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="span3">
            <label class="label-danger">
                Realizaram Checkout
            </label>
        </div>
        <div class="span3">
            <h5>
                <?=!empty($checkout) ? $checkout : 0;?>
            </h5> 
        </div>
        <div class="span3">
            <label class="label-danger">
                Hora do Fim
            </label>
        </div>
        <div class="span3">
            <h5>
                <?=$evento['Evento']['duracao'] > 0 ? date('H:i', strtotime($evento['Evento']['duracao'])) : "<em class='fg-color-red'>Não Cadastrado</em>";?>
            </h5> 
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="span3">
            <label class="label-danger">
                Total Mesas Checkout
            </label>
        </div>
        <div class="span3">
                <h5>
                    <?=$totalExtras['mesas']?>
                </h5> 
        </div>  
        <div class="span3">
            <label class="label-danger">
                Total Convite Checkout
            </label>
        </div>
        <div class="span3">
            <h5>
                <?=$totalExtras['convites']?>
            </h5> 
        </div> 
    </div>
</div>
<br />
<div class="row-fluid">
    <div class="span12">
        <label class="label-danger">
            Informações
        </label>
        <h5>
            <?=(!empty($evento['Evento']['outras_informacoes'])) ?  nl2br($evento['Evento']['outras_informacoes']) : "<em class='fg-color-red'>Não Cadastrado</em>";?>
        </h5>    
    </div>
</div>
<br />
<div class="row-fluid">
    <div class="span12">
        <label class="label-danger">
            Local<?=($evento['Evento']['local_mapa'] != '') ? "<i class='icon-help pull-right'></i>" : ""?>
        </label>
        <h5>
            <?=empty($evento['Evento']['local_endereco']) ? "<em class='fg-color-red'>Não Cadastrado</em>" : $evento['Evento']['local_endereco'];?>
        </h5> 
    </div>
</div>
<br />
<div class="row-fluid">
    <div class="span12">
        <label class="label-danger">
            Buffet
        </label>
        <h5>
            <?=empty($evento['Evento']['buffet']) ? "<em class='fg-color-red'>Não Cadastrado</em>" : $evento['Evento']['buffet'];?>
        </h5>    
    </div>
</div>
<br />
<div class="row-fluid">
    <div class="span12">
        <label class="label-danger">
            Banda/Atrações
        </label>
        <h5>
            <?=empty($evento['Evento']['banda']) ?  "<em class='fg-color-red'>Não Cadastrado</em>" : $evento['Evento']['banda'] . " / " . 
               empty($evento['Evento']['atracoes']) ? "<em class='fg-color-red'>Não Cadastrado</em>" : $evento['Evento']['atracoes'];?>
        </h5>   
    </div>
</div>
<br />
<div class="row-fluid">
    <div class="span12">
        <label class="label-danger">
            Informações Fotógrafos
        </label>
        <h5>
            <?=empty($evento['Evento']['informacoes_fotografos']) ?  "<em class='fg-color-red'>Não Cadastrado</em>" : nl2br($evento['Evento']['informacoes_fotografos']); ?>
        </h5>   
    </div>
</div>
<br />
<div class="row-fluid">
    <div class="span12">
        <label class="label-danger">
            Responsáveis
        </label>
        <table class="table table-condensed table-striped">
            <thead>
                <tr>
                    <th scope="col" width="70%">
                        Nome
                    </th>
                    <th scope="col" width="30%">
                        Departamento
                    </th>
                </tr>
            </thead>
            <?php foreach($atendentes as $atendente) : ?>
            <tr>
                <td width="70%">
                    <?=$atendente['Usuario']['nome'];?>
                </td>
                <td width="30%">
                    <?=$atendente['Usuario']['grupo'];?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>