<?php

$listaTurmas = array();

foreach ($turmas as $turma)
    $listaTurmas[] = array(
        "ID" => $turma['Turma']['id'],
        "Nome" => $turma['Turma']['nome'],
        "Expectativa Formandos" => $turma['Turma']['expectativa_formandos'],
        "Formandos" => $turma[0]['formandos'],
        "Aderidos" => $turma[0]['aderidos'],
        "Checkout" => $turma[0]['checkout'],
        "Data Assinatura" => empty($turma['Turma']['data_assinatura_contrato']) ? "Indefinida" : date('d/m/Y',strtotime($turma['Turma']['data_assinatura_contrato'])),
        "Ano/Sem Conclusao" => "{$turma['Turma']['ano_formatura']}.{$turma['Turma']['semestre_formatura']}",
        "Consultores" => str_replace("<br />",",",$turma[0]['consultores'])
    );
$excel->generate($listaTurmas, 'lista_fechamento_turmas_' . date('Y_m_d'));
?>