
<span id="conteudo-titulo" class="box-com-titulo-header">Informações da Turma</span>
<?php $session->flash(); ?>
<div id="conteudo-container">
    <div class="detalhes">
        <p class="grid_11 alpha omega">
            <label class="grid_5 alpha">Código:</label>
            <span class="grid_2 alpha first"><?php echo $turma['Turma']['codigo']; ?></span>
        </p>

        <p class="grid_11 alpha omega">
            <label class="grid_11 alpha">Cursos:</label>
            <?php foreach ($turma['CursoTurma'] as $cursoturma) : ?>
                <span class="grid_11 alpha first">
                    <?php
                    $universidade = $cursoturma['Curso']['Faculdade']['Universidade']['sigla'];
                    if ($universidade == '')
                        $universidade = $cursoturma['Curso']['Faculdade']['Universidade']['nome'];

                    $faculdade = $cursoturma['Curso']['Faculdade']['sigla'];
                    if ($faculdade == '')
                        $faculdade = $cursoturma['Curso']['Faculdade']['nome'];
                    echo $universidade . ' - ' .
                    $faculdade . ' - ' .
                    $cursoturma['Curso']['nome'] . ' - ' .
                    $cursoturma['turno'];
                    ?>
                </span>
<?php endforeach; ?>
        </p>
        <p class="grid_11 alpha omega">
            <label class="grid_11 alpha">Expectativa de Formandos</label>
            <span class="grid_11 alpha first"><?php echo $turma['Turma']['expectativa_formandos']; ?></span>
        </p>
        <p class="grid_11 alpha omega">
            <label class="grid_11 alpha">Data de Formatura</label>
            <span class="grid_11 alpha first"><?php echo $turma['Turma']['ano_formatura'] . '/' . $turma['Turma']['semestre_formatura'] . ' semestre'; ?></span>
        </p>
        <p class="grid_11 alpha omega">
            <label class="grid_11 alpha">Consultor<?php if(count($turma['Usuario'])>1) echo 'es'; ?></label>
            <?php foreach($turma['Usuario'] as $u):
                echo '<span class="grid_11 alpha">';
                echo $u['nome'] . ' - ' . $u['email'];
                echo '</span>';
            endforeach;?>
	<p class="grid_11 alpha omega">
            <label class="grid_11 alpha">Memorando</label>
            <span class="grid_11 alpha"><?php echo $turma['Turma']['memorando']; ?></span>
        </p>
        <p class="grid_11 alpha omega">
            <label class="grid_11 alpha">Fundo de caixa utilizado</label>
            <span class="grid_11 alpha">R$ <?php echo $turma['Turma']['fundo_caixa_usado']; ?></span>
        </p>
        <p class="grid_11 alpha omega">
            <label class="grid_11 alpha">Data de Assinatura do Contrato</label>
            <span class="grid_11 alpha"><?php echo $turma['Turma']['data_assinatura_contrato']; ?></span>
        </p>
        <p class="grid_11 alpha omega">
            <label class="grid_11 alpha">IGPM</label>
            <span class="grid_11 alpha"><?php echo $turma['Turma']['igpm']; ?></span>
        </p>
        <p class="grid_11 alpha omega">
            <label class="grid_11 alpha">Data de correcao do IGPM</label>
            <span class="grid_11 alpha">
			<?php 
				if ($turma['Turma']['data_igpm'] != "0000-00-00")
					echo date('d/m/Y',strtotime($turma['Turma']['data_igpm'])); 
				else
					echo "-";
			?>
        </p>
        
        <p class="grid_11 alpha omega">
            <label class="grid_11 alpha">Convites Inclusos na Adesão</label>
            <span class="grid_11 alpha"><?php echo $turma['Turma']['convites_contrato']; ?></span>
        </p>
        
        <p class="grid_11 alpha omega">
            <label class="grid_11 alpha">Mesas Inclusas na Adesão</label>
            <span class="grid_11 alpha"><?php echo $turma['Turma']['mesas_contrato']; ?></span>
        </p>
        
        <p class="grid_6 alpha omega">
<?php echo $html->link('Editar', array($this->params['prefix'] => true, 'action' => 'editar'), array('class' => 'submit')); ?>
<?php echo $html->link('Voltar', array($this->params['prefix'] => true, 'action' => 'index'), array('class' => 'cancel')); ?>
        </p>
    </div>
</div>
