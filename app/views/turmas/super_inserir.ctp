<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/bootstrap/datepicker.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootstrap/select.css">
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/bootstrap/datepicker.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $("#TurmaDataAssinatura").datepicker();
    $('.selectpicker').selectpicker({width:'100%'});
    var salvar = $("<button>",{
        type: 'button',
        class: 'button bg-color-greenDark salvar',
        text:'Salvar'
    });
    $('.modal-footer').prepend(salvar);
    $(".salvar").click(function(e) {
        e.preventDefault();
        var context = ko.contextFor($(".metro-button.reload")[0]); 
        var dados = $("#form").serialize();
        var url = $("#form").attr('action');
        bootbox.hideAll();
        context.$data.showLoading(function() {
            $.ajax({
                url: url,
                data: dados,
                type: "POST",
                dataType: "json",
                complete: function() {
                    context.$data.reload();
                }
            });
        });
    });
});
</script>
<div class="row-fluid">
	<?php $session->flash(); ?>
	<?php echo $form->create('Turma', array('url' => "/{$this->params['prefix']}/turmas/inserir/", 'id' => 'form')); ?>
    <div class="row-fluid">
        <div class="span4">
            <?php echo $form->input('Turma.ano_formatura', array('label' => 'Ano Formatura', 'maxlength' => 4, 'div' => 'input-control text', 'error' => false)); ?>
        </div>
        <div class="span4">
            <?php echo $form->input('Turma.semestre_formatura',  array('options' => $semestre_formaturas, 'type' => 'select', 'div' => 'input-control text', 'label' => 'Semestre Formatura', 'class' => 'selectpicker')); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4">
                <?php echo $form->input('Turma.nome', array('label' => 'Nome', 'div' => 'input-control text', 'error' => false)); ?>
        </div>
        <div class="span4">
                <?php echo $form->input('Turma.expectativa_formandos', array('label' => 'Expectativa Formandos', 'maxlength' => 4, 'div' => 'input-control text', 'error' => false)); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4">
                <?php echo $form->input('Turma.expectativa_fechamento', array('options' => $expectativa_fechamentos, 'type' => 'select', 'div' => 'input-control text', 'label' => 'Expectativa Fechamento', 'class' => 'selectpicker')); ?>
        </div>
        <div class="span4">
                <?php echo $form->input('Turma.data_assinatura', array('div' => 'input-control text', 'label' => 'Data Assinatura Contrato')); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4">
                <?php echo $form->input('Turma.como_chegou', array('options' => $como_chegous, 'type' => 'select', 'div' => 'input-control text', 'label' => 'Como Chegou', 'class' => 'selectpicker')); ?>
        </div>
        <div class="span4">
                <?php echo $form->input('Turma.pretensao', array('options' => $pretensaos, 'type' => 'select', 'div' => 'input-control text', 'label' => 'Pretensão', 'class' => 'selectpicker')); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span8">
            <?php echo $form->input('Turma.como_chegou_detalhes', array('label' => 'Detalhes Como Chegou', 'div' => 'input-control text', 'error' => false, 'style' => 'width: 548px')); ?>
        </div>
    </div>
</div>