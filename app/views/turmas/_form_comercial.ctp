<script type="text/javascript">
    $(function() {
        $( "#datepicker" ).datepicker( $.datepicker.regional[ "pt-BR" ] );
    });
</script>

<?php echo $form->input('id',  array('hiddenField' => true)); ?>


<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Código</label>
	<?php echo $form->input('codigo', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
</p>
<div class="grid_11 alpha omega crud-inline">
	<label class="grid_0 alpha omega">Cursos</label>
	<?php include('_crud_inline_cursos.ctp'); ?>
</div>
<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Expectativa de Formandos</label>
	<?php echo $form->input('expectativa_formandos', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
</p>
<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Ano de Formatura</label>
	<?php 
	$anoatual = Date('Y') + 1;
	for($i = $anoatual - 5 ; $i < $anoatual + 5 ; $i++) {
		$anos[$i] = $i;
	}
	
	echo $form->input('ano_formatura', array('options' => $anos, 'type' => 'select', 'class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
</p>
<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Semestre de Formatura</label>
	<?php 
	
	echo $form->input('semestre_formatura', array('options' => array(1 => 1 , 2 => 2), 'type' => 'select', 'class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
</p>
<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Status</label>
	<?php echo $form->input('Turma.status', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
</p>
<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Data de Assinatura do Contrato</label>
	<?php echo $form->input('data-assinatura', array( 'class' => 'grid6 alpha omega', 'label' => false, 'div' => false,'error' => array('wrap' => 'span', 'class' => 'grid_10'), 'id' => 'datepicker')); ?>
	
</p>
<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Como Chegou?</label>
	<?php echo $form->input('Turma.como_chegou', array('options' => $como_chegous, 'type' => 'select', 'class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
</p>
<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Detalhes de Como Chegou</label>
	<?php echo $form->textarea('como_chegou_detalhes', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
</p>
<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Pretensão</label>
	<?php echo $form->input('Turma.pretensao', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
</p>
