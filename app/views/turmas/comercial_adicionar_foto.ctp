<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootstrap/fileupload.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/redactor.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/fileupload.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/redactor.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var button = $("<button>",{
            type: 'button',
            class: 'hide bg-color-blue',
            id:'inserir-foto',
            text:'Enviar'
        });
        $('.modal-footer').prepend(button);
        buttons = ['formatting', '|', 'bold', 'italic', '|',
            'unorderedlist', 'orderedlist', 'outdent', 'indent', '|',
            'link', '|', 'fontcolor', 'backcolor', '|',
            'alignment', '|', 'horizontalrule'];
        $('#descricao-foto').redactor({
            buttons:buttons,
            minHeight:150
        });
        $("#formulario").submit(function(e) {
            e.preventDefault();
        });
        $('.fileupload').bind('loaded',function(e) {
            var src = e.imagem.replace(/^data:image\/(gif|png|jpe?g);base64,/, "");
            if(src)
                $("#inserir-foto").fadeIn(500);
            return;
        });
        $("#inserir-foto").click(function(e) {
            src = $("#preview-foto img").attr('src').replace(/^data:image\/(gif|png|jpe?g);base64,/, "");
            if(src) {
                var button = $('.modal-footer').find(':contains("Envia")');
                button.remove();
                $('#src-foto').val(src);
                var context = ko.contextFor($("#content-body")[0]);
                var dados = $("#formulario").serialize();
                var url = $("#formulario").attr('action');
                context.$data.showLoading(function() {
                    $.ajax({
                        url : url,
                        data : dados,
                        type : "POST",
                        dataType : "json",
                        complete : function() {
                            bootbox.hideAll();
                            context.$data.reload();
                        }
                    });
                });    
            }
        });
    });
</script>
<?=$form->create('TurmaPaginaFoto', array(
    'url' => "/{$this->params['prefix']}/turmas/adicionar_foto",
    'id' => 'formulario'
)); ?>
<?=$form->hidden('TurmaPaginaFoto.turma_pagina_id');?>
<?=$form->hidden('TurmaPaginaFoto.src',array('id' => 'src-foto'));?>
<div class="row-fluid">
    <div class="span4">
        <div class="fileupload fileupload-new row-fluid"
            data-provides="fileupload">
            <div>
                <span class="btn-file">
                    <a class="button mini input-block-level default fileupload-new">
                        <i class="icon-picture"></i>
                        Selecione a Foto
                    </a>
                    <a class="button mini bg-color-orange input-block-level fileupload-exists">
                        <i class="icon-retweet"></i>
                        Alterar
                    </a>
                    <input type="file" />
                </span>
            </div>
            <div class="fileupload-new thumbnail" style="width:100%; height: 150px; line-height: 20px;">
                <img src="<?="{$this->webroot}metro/img/no-image.gif"?>" />
            </div>
            <div class="fileupload-preview fileupload-exists thumbnail" id="preview-foto"
                style="width:100%; height: 150px; line-height: 20px;">
            </div>
        </div>
    </div>
    <div class="span7">
        <?=$form->input('TurmaPaginaFoto.descricao', array(
            'class' => 'redactor',
            'type' => 'textarea',
            'label' => false,
            'id' => 'descricao-foto',
            'placeholder' => 'Descricao da Foto',
            'error' => false,
            'div' => false));
        ?>
    </div>
</div>
<?=$form->end(array('label' => false, 'div' => false, 'style' => 'display:none')); ?>