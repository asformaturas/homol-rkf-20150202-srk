<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/max/form_validate.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/jquery-validate.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var button = $("<button>",{
            type: 'button',
            class: 'button bg-color-blue',
            id:'inserir-pagina',
            text:'Enviar'
        });
        $('.modal-footer').prepend(button);
        $("#formulario").submit(function(e) {
            e.preventDefault();
        });
        $('#formulario').validate({
            sendForm : false,
            eachValidField : function() {
                $(this).removeClass('error').removeClass('form-error').addClass('success');
                var label = $('label[for="'+$(this).attr('id')+'"]');
                if(label.length > 0) {
                    if(label.children('span').length > 0)
                        label.children('span').fadeOut(500,function() { $(this).remove()});
                }
            },
            eachInvalidField : function() {
                $(this).removeClass('success').addClass('error');
            },
            description: {
                notEmpty : {
                    required : function() {
                        var label = $('label[for="'+$(this).attr('id')+'"]');
                        if(label.length > 0) {
                            if(label.children('span').length > 0)
                                label.children('span').html('').attr('class','fg-color-red');
                            else
                                label.append($('<span>',{class:'fg-color-red'}));
                            mensagem = $(this).data('error') ||
                                'Complete o campo';
                            label.children('span').html(mensagem);
                        }
                        return '';
                    }
                }
            },
            valid: function() {
                var button = $('.modal-footer').find(':contains("Envia")');
                button.remove();
                var context = ko.contextFor($("#content-body")[0]);
                var dados = $("#formulario").serialize();
                var url = $("#formulario").attr('action');
                context.$data.showLoading(function() {
                    $.ajax({
                        url : url,
                        data : dados,
                        type : "POST",
                        dataType : "json",
                        complete : function() {
                            bootbox.hideAll();
                            context.$data.reload();
                        }
                    });
                });
            },
            invalid: function() {
                return false;
            }
        });
        $("#inserir-pagina").click(function(e) {
            $("#formulario").trigger('submit');
        });
    })
</script>
<?=$form->create('TurmaPagina', array(
    'url' => "/{$this->params['prefix']}/turmas/adicionar_pagina",
    'id' => 'formulario'
)); ?>
<?=$form->hidden('TurmaPagina.id');?>
<div class="row-fluid">
    <div class="span5">
        <label class="required" for="menu">Nome</label>
        <?=$form->input('menu',array(
            'label' => false,
            'id' => 'menu',
            'data-description' => 'notEmpty',
            'data-describedby' => 'menu',
            'div' => 'input-control text',
            'data-required' => 'true',
            'error' => false)); ?>
    </div>
    <div class="span6">
        <label class="required" for="titulo">Descrição</label>
        <?=$form->input('titulo',array(
            'label' => false,
            'id' => 'titulo',
            'data-description' => 'notEmpty',
            'data-describedby' => 'titulo',
            'div' => 'input-control text',
            'data-required' => 'true',
            'error' => false)); ?>
    </div>
</div>
<?=$form->end(array('label' => false, 'div' => false, 'style' => 'display:none')); ?>