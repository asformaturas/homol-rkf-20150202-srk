<style type="text/css">
.well { padding:6px; background:white }
.well .button, .well button { margin:0 }
.nav-list.scrollable { max-height:300px; overflow-x: auto; margin-bottom:5px }
.nav-list li a { position:relative; cursor:pointer; padding:3px 10px; font-size:12px }
.nav-list li.selecionado a { background:#333; color:white }
.nav-list li { margin-bottom: 5px }
.editar,.adicionar { display: none }
</style>
<script type="text/javascript">
$(document).ready(function() {
    
    $("[data-tipo]:not(:last-child)").on('click','.carregar',function(e) {
        e.preventDefault();
        var src = this;
        var target = null;
        var current = null;
        $("#dados > [data-tipo]").each(function() {
            if(current != null && target != null)
                $(this).find('.adicionar').fadeOut(500,function() {
                    $(this).prev().html('');
                });
            else if(current != null && target == null)
                target = this;
            else if($(this).data('tipo') == $(src).parents('[data-tipo]').data('tipo'))
                current = this;
        });
        var url = '/<?=$this->params['prefix']?>/faculdades/dados/' +
                $(target).data('tipo') + '/' + $(current).data('tipo').toLowerCase() +
                '_id/' + $(src).data('id');
        $(current).find('.nav-list.scrollable > li.selecionado').removeClass('selecionado');
        $(target).find('.editar').fadeOut(500,function() {
            $(current).find('.editar').fadeOut(500,function() {
                $.getJSON(url, function(response) {
                    var ul = $(target).find('.nav-list.scrollable');
                    ul.html('');
                    $.each(response[$(target).data('tipo')],function(i,data) {
                        $("<li>").
                            append($("<a>",{
                                text: data[$(target).data('tipo')]['nome'],
                                class: 'carregar',
                                'data-id': data[$(target).data('tipo')]['id']
                            })).
                            appendTo(ul);
                    });
                }).always(function() {
                    $.data(current,'id',$(src).data('id'));
                    $(src).parent().addClass('selecionado');
                    $(current).find('.editar').fadeIn(500,function() {
                        $(target).find('.adicionar').fadeIn(500);
                    });
                });
            });
        });
    });
    
    $("[data-tipo]:last-child").on('click','.carregar',function(e) {
        e.preventDefault();
        var src = this;
        var current = $(src).parents('[data-tipo]')[0];
        $.data(current,'id',$(src).data('id'));
        $(current).find('.nav-list.scrollable > li.selecionado').removeClass('selecionado');
        $(src).parent().addClass('selecionado');
        $(current).find('.editar').fadeIn(500);
    });
    
    $("#dados").on('click','.acao',function(e) {
        e.preventDefault();
        acao = $(this).data('acao');
        div = $(this).parents('[data-tipo]')[0];
        id = $(div).data('id') == undefined ? '' : $(div).data('id');
        params = '';
        $("#dados > [data-tipo]").each(function() {
            if($(this).index() < $(div).index())
                if($(this).data('id') != undefined)
                    params+= $(this).data('id') + '/';
            else
                return;
        });
        if(acao == 'criar') {
            acao = 'editar';
            id = '';
        }
        var url = '/<?=$this->params['prefix']?>/' + $(div).data('tipo').toLowerCase() +
            's/' + acao + '/' + params + '/' + id;
        bootbox.dialog('Carregando',[{
            label: 'Cancelar'
        }],{
            remote: url
        });
    });
});
</script>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Gerenciar Dados
        </h2>
    </div>
</div>
<?php $session->flash(); ?>
<div class="row-fluid" id="dados">
    <div class="span5" data-tipo="Universidade">
        <div class='well'>
            <ul class="nav nav-list">
                <li class='active'>
                    <a>Colégios</a>
                </li>
            </ul>
            <ul class="nav nav-list scrollable">
                <?php foreach($universidades as $universidade) : ?>
                <li>
                    <a class="carregar" data-id="<?=$universidade['Universidade']['id']?>">
                        <?="{$universidade['Universidade']['nome']}/{$universidade['Universidade']['sigla']}"?>
                    </a>
                </li>
                <?php endforeach; ?>
            </ul>
            <div class="row-fluid">
                <div class="span4">
                    <a class="input-block-level button mini default acao" data-acao="criar">
                        Criar
                    </a>
                </div>
                <div class="span8">
                    <div class="row-fluid editar">
                        <div class="span6">
                            <a class="input-block-level button mini bg-color-blue acao" data-acao="editar">
                                Editar
                            </a>
                        </div>
                        <div class="span6">
                            <a class="input-block-level button mini bg-color-red acao" data-acao="remover">
                                Remover
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="span4" data-tipo="Faculdade">
        <div class='well'>
            <ul class="nav nav-list">
                <li class='active'>
                    <a>Unidades</a>
                </li>
            </ul>
            <ul class="nav nav-list scrollable">
                
            </ul>
            <div class="row-fluid adicionar">
                <div class="span4">
                    <a class="input-block-level button mini default acao" data-acao="criar">
                        Criar
                    </a>
                </div>
                <div class="span8">
                    <div class="row-fluid editar">
                        <div class="span6">
                            <a class="input-block-level button mini bg-color-blue acao" data-acao="editar">
                                Editar
                            </a>
                        </div>
                        <div class="span6">
                            <a class="input-block-level button mini bg-color-red acao" data-acao="remover">
                                Remover
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="span3" data-tipo="Curso">
        <div class='well'>
            <ul class="nav nav-list">
                <li class='active'>
                    <a>Cursos</a>
                </li>
            </ul>
            <ul class="nav nav-list scrollable">
                
            </ul>
            <div class="row-fluid adicionar">
                <div class="span4">
                    <a class="input-block-level button mini default acao" data-acao="criar">
                        Criar
                    </a>
                </div>
                <div class="span8">
                    <div class="row-fluid editar">
                        <div class="span6">
                            <a class="input-block-level button mini bg-color-blue acao" data-acao="editar">
                                Editar
                            </a>
                        </div>
                        <div class="span6">
                            <a class="input-block-level button mini bg-color-red acao" data-acao="remover">
                                Remover
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>