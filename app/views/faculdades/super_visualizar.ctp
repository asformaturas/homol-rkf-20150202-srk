<span id="conteudo-titulo" class="box-com-titulo-header">Informações da Faculdade</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<div class="detalhes">
		<p class="grid_11 alpha omega">
			<label class="grid_11 alpha omega">Universidade</label>
			<span class="grid_5 alpha first"> <?php echo $faculdade['Universidade']['nome']?> </span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Faculdade</label>
			<label class="grid_6 alpha">Sigla</label>
			<span class="grid_5 alpha first"><?php echo $faculdade['Faculdade']['nome']?></span>
			<span class="grid_6 alpha"><?php echo $faculdade['Faculdade']['sigla']?></span>
		</p>
		<p class="grid_11 alpha omega">
			<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'action' => 'index') ,array('class' => 'cancel')); ?>
			<?php echo $html->link('Editar', array($this->params['prefix'] => true, 'controller' => 'Faculdades', 'action' => 'editar', $faculdade['Faculdade']['id']), array('class' => 'submit ')); ?>
			<?php echo $html->link('Adicionar Outra', array($this->params['prefix'] => true, 'controller' => 'Faculdades', 'action' => 'adicionar'), array('class' => 'submit ')); ?>
		</p>
	</div>
</div>