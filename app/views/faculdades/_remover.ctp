<?php if(count($faculdades) > 0) : ?>
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/max/form_validate.css">
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/chosen.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/chosen.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/jquery-validate.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#formulario").submit(function(e) {
            e.preventDefault();
        });
        $(".chosen").chosen({width:'100%'});
        var button = $("<button>",{
            type: 'button',
            class: 'button bg-color-blue',
            id:'salvar-formulario',
            text:'Remover'
        });
        $('.modal-footer').prepend(button);
        $("#salvar-formulario").click(function(e) {
            e.preventDefault();
            $('#formulario').trigger('submit');
        });
        $('#formulario').validate({
            sendForm : false,
            eachValidField : function() {
                $(this).removeClass('error').removeClass('form-error').addClass('success');
                var label = $('label[for="'+$(this).attr('id')+'"]');
                if(label.length > 0) {
                    if(label.children('span').length > 0)
                        label.children('span').fadeOut(500,function() { $(this).remove()});
                }
            },
            eachInvalidField : function() {
                $(this).removeClass('success').addClass('error');
            },
            description: {
                notEmpty : {
                    required : function() {
                        var label = $('label[for="'+$(this).attr('id')+'"]');
                        if(label.length > 0) {
                            if(label.children('span').length > 0)
                                label.children('span').html('').attr('class','fg-color-red');
                            else
                                label.append($('<span>',{class:'fg-color-red'}));
                            mensagem = $(this).data('error') ||
                                'Complete o Campo';
                            label.children('span').html(mensagem);
                        }
                        return '';
                    }
                }
            },
            valid: function() {
                var button = $('.modal-footer').find(':contains("Remove")');
                button.remove();
                var context = ko.contextFor($("#content-body")[0]);
                var dados = $("#formulario").serialize();
                var url = $("#formulario").attr('action');
                context.$data.showLoading(function() {
                    $.ajax({
                        url : url,
                        data : dados,
                        type : "POST",
                        dataType : "json",
                        complete : function() {
                            bootbox.hideAll();
                            context.$data.reload();
                        }
                    });
                });
            },
            invalid: function() {
                return false;
            }
        });
    });
</script>
<?=$form->create('Faculdade', array(
    'url' => "/{$this->params['prefix']}/faculdades/remover",
    'id' => 'formulario'
));
echo $form->hidden('Faculdade.id');
?>
<div class="row-fluid">
    <div class="span11">
        <h3 class="fg-color-red">
            Selecione uma Faculdade para substituir a removida
        </h3>
    </div>
</div>
<br />
<div class="row-fluid">
    <div class="span11">
        <?=$form->input('substituir', array(
            'options' => $faculdades,
            'type' => 'select',
            'empty' => 'Selecione uma Faculdade',
            'class' => 'chosen',
            'label' => array('text' => 'Faculdades','class' => 'required'),
            'id' => 'faculdades',
            'data-description' => 'notEmpty',
            'data-describedby' => 'faculdades',
            'div' => 'input-control text',
            'data-required' => 'true',
            'error' => false)); ?>
        <br />
        <br />
        <br />
        <br />
    </div>
</div>
<?=$form->end(array('label' => false, 'div' => false, 'style' => 'display:none')); ?>
<?php elseif($permitido) : ?>
<script type="text/javascript">
    $(document).ready(function(){
        $("#formulario").submit(function(e) {
            e.preventDefault();
        });
        var button = $("<button>",{
            type: 'button',
            class: 'button bg-color-blue',
            id:'salvar-formulario',
            text:'Remover'
        });
        $('.modal-footer').prepend(button);
        $("#salvar-formulario").click(function(e) {
            e.preventDefault();
            var button = $('.modal-footer').find(':contains("Remove")');
            button.remove();
            var context = ko.contextFor($("#content-body")[0]);
            var dados = $("#formulario").serialize();
            var url = $("#formulario").attr('action');
            context.$data.showLoading(function() {
                $.ajax({
                    url : url,
                    data : dados,
                    type : "POST",
                    dataType : "json",
                    complete : function() {
                        bootbox.hideAll();
                        context.$data.reload();
                    }
                });
            });
        });
    });
</script>
<?=$form->create('Faculdade', array(
    'url' => "/{$this->params['prefix']}/faculdades/remover",
    'id' => 'formulario'
));
echo $form->hidden('Faculdade.id');
?>
<h3 class="fg-color-red">
    N&atilde;o existe cursos vinculdados &agrave; essa faculdade
    <br />
    Portanto apenas clique em "Remover" para prosseguir
</h3>
<?=$form->end(array('label' => false, 'div' => false, 'style' => 'display:none')); ?>
<?php else : ?>
<h3 class="fg-color-red">Para Remover a Faculdade primeiro crie outra para substitu&iacute;-la</h3>
<?php endif; ?>
