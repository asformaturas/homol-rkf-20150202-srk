<?php
    $paginator->options(array(
        'url' => array($this->params['prefix'] => true)));
    $sortOptions = array('data-bind' => 'click: loadThis');
?>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<style type="text/css">
label { font-size: 13px; margin-bottom:5px; line-height: 16px }
.mini.max { font-size:12px; height:28px }
</style>
<script type="text/javascript">
$(document).ready(function() {
    $("#form-filtro").submit(function(e) {
        e.preventDefault();
        var context = ko.contextFor($(".metro-button.reload")[0]);
        context.$data.showLoading(function() {
            $("#form-filtro").find('[value="false"]').attr('disabled','disabled');
            var dados = $("#form-filtro").serialize();
            var url = $("#form-filtro").attr('action');
            $.ajax({
                url : url,
                data : dados,
                type : "POST",
                dataType : "json",
                complete : function() {
                    context.$data.reload();
                }
            });
        });
    })
})
</script>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Colégios
        </h2>
    </div>
</div>
<?php $session->flash(); ?>
<br />
<div class="row-fluid">
    <?=$form->create('Universidade',array(
        'url' => "/{$this->params['prefix']}/faculdades/relatorio",
        'id' => 'form-filtro')) ?>
    <div class="row-fluid">
        <div class="span3">
            <label>Colégio</label>
            <?=$form->input('Universidade.nome',
                array(
                    'label' => false,
                    'div' => 'input-control mini text',
                    'error' => false,
                    'type' => 'text',
                )); ?>
        </div>
        <div class="span2">
            <label>Sigla</label>
            <?=$form->input('Universidade.sigla',
                array(
                    'label' => false,
                    'div' => 'input-control mini text',
                    'error' => false,
                    'type' => 'text',
                )); ?>
        </div>
        <div class="span3">
            <label>Consultor</label>
            <?=$form->input('Usuario.nome',
                array(
                    'label' => false,
                    'div' => 'input-control mini text',
                    'error' => false,
                    'type' => 'text',
                )); ?>
        </div>
        <div class="span4">
            <label>&nbsp;</label>
            <button type='submit' class='mini max bg-color-red'>
                Filtrar
                <i class='icon-search-2'></i>
            </button>
        </div>
    </div>
    <?= $form->end(array('label' => false,
        'div' => false, 'class' => 'hide')); ?>
    <?php if (sizeof($faculdades) == 0) { ?>
    <h3 class='fg-color-red'>Nenhum Faculdade</h3>
    <?php } else { ?>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="40%"><?=$paginator->sort('Nome', 'Universidade.nome',$sortOptions); ?></th>
                <th scope="col" width="15%"><?=$paginator->sort('Sigla', 'Universidade.sigla',$sortOptions); ?></th>
                <th scope="col" width="30%">Consultores</th>
                <th scope="col" width="15%"></th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($faculdades as $faculdade): ?>
            <tr>
                <td><?=$faculdade['Universidade']['nome']?></td>
                <td><?=$faculdade['Universidade']['sigla']?></td>
                <td>
                    <?php if(count($faculdade['UniversidadeUsuario']) > 0) : ?>
                    <?php
                        $consultores = array();
                        foreach($faculdade['UniversidadeUsuario'] as $universidadeUsuario)
                            $consultores[] = $universidadeUsuario['nome'];
                        echo implode(", ", $consultores);
                    ?>
                    <?php else : ?>
                    Nenhum Consultor
                    <?php endif; ?>
                </td>
                <td dir="<?=$faculdade['Universidade']['id']?>"
                    style="text-align:center">
                    <a class="button mini default" data-bind="click: loadThis"
                        href="/<?=$this->params['prefix'] ?>/faculdades/relatorio_cursos/<?=$faculdade['Universidade']['id']?>">
                        Visualizar
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="2" class="paginacao">
                    <?=$paginator->numbers(array('separator' => ' ',
                        'data-bind' => 'click: loadThis')); ?>
                </td>
                <td colspan="2">
                    <?=$paginator->counter(array(
                        'format' => 'Total : %count% ' .  $this->name)); ?>
                </td>
            </tr>
        </tfoot>
    </table>
    <?php } ?>
</div>