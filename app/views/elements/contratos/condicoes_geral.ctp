<h4 style="text-align: center"><b>Condições Contratuais</b></h4>
<br />
<p style="text-align: justify">
Pelo presente instrumento particular, de um lado: 
<br /><br />
Adere ao Contrato de Prestação de Serviços, Representação Financeira e de Fotos firmado entre a Comissão de Formatura de seu curso e a empresa RK EVENTOS E FORMATURAS LTDA., inscrita no CNPJ/MF sob N.º09.551.738/0001-11, com sede na Rua Caramuru, 1159 - SAUDE, São Paulo/SP - CEP 04138-002, representada neste ato pelo sócio-diretor RAFAEL KEINER, portador do RG Nº 47.072.806-1, denominado CONTRATO COLETIVO e regido nos seguintes moldes:
<br /><br />
1. O formando acima qualificado reconhece a Comissão de Formatura de sua turma como legítima representante para tratar dos assuntos relacionados à sua Formatura, outorgando poderes para representá-lo em atos referentes à sua formatura, outorgando poderes para a Comissão celebrar aditivos contratuais que se fizerem necessários à realização dos eventos.
<br /><br />
2. As&nbsp;informações importantes a respeito do seu contrato estarão disponibilizadas no link: https://sistema.rkformaturas.com.br/usuarios/login. Caso precise do login e senha da sua turma, entre em contato com a sua comissão ou diretamente com a RK Formaturas.
<br /><br />
3. A comissão de formatura não poderá tratar assuntos de cunho financeiro, tais como forma de pagamento, renegociação de dívida ou cancelamento de contrato em nome do formando. 
<br /><br />
4. O formando acima qualificado autoriza a RK Formaturas a representá-lo na contratação e pagamento de fornecedores. O valor pago pelo CONTRATANTE permanecerá em posse da CONTRATADA de forma transitória, até o repasse aos fornecedores do pacote de formatura do quais a CONTRATADA é intermediária, ficando tão-somente com a comissão que lhe faz jus. 
<br /><br />
5. O(A) CONTRATANTE se obriga a pagar a CONTRATADA pelos eventos de formatura descritos no CONTRATO COLETIVO. 
<br /><br />
6. No momento da retirada dos convites o formando que optar por boletos bancários deverá quitar as parcelas em atraso e trocar os boletos ainda em aberto por cheques pré-datados. No mesmo momento, o formando que tiver optado por cheques pré-datados e ainda não tiver os entregue, deverá entregar os mesmos. 
<br /><br />
7. Os valores consignados neste contrato serão atualizados de 12 em 12 meses, a partir da assinatura do Contrato de Prestação de Serviços celebrado entre a Comissão de Formatura e a RK EVENTOS E FORMATURAS LTDA., adotando - se a variação do IGPM/FGV do período, conforme autoriza a Lei 9.069/95, ou por outro índice que venha a substituí-lo. 
<br /><br />
8. O atraso no pagamento de qualquer parcela acarretará multa de 5% (cinco por cento) acrescida de mora diária de 0,033% sobre o valor. 
<br /><br />
9. Os convites só serão entregues após feito o acerto financeiro, isto é, pendências devidamente quitadas, isto inclui os valores relativos ao IGPM, cheques trocados e entregues , conforme explicitado nos itens 6 e 7. 
<br /><br />
10. A Rescisão poderá ser promovida pelo CONTRATANTE, com aplicação de multa e ressarcimento nos termos da tabela abaixo: 
<br /><br />
(a) Da data de assinatura do contrato até 105 dias de antecedência ao término do ano letivo da turma: A Contratada reterá o valor proporcional correspondente aos valores já desembolsados com contratação de espaço de evento, prestadores de serviço, produtos e brindes até a data da comunicação da Rescisão, acrescido de multa de 30% sobre o valor do contrato.
<br /><br />
(b) De 104 a 90 dias de antecedência ao término do ano letivo da turma: A Contratada reterá o valor proporcional correspondente aos valores já desembolsados com contratação de espaço de evento, prestadores de serviço, produtos e brindes até a data da comunicação da Rescisão, acrescido de multa de 40% sobre o valor do contrato.
<br /><br />
(c) De 90 a 75 dias de antecedência ao término do ano letivo da turma: A Contratada reterá o valor proporcional correspondente aos valores já desembolsados com contratação espaço de evento, prestadores de serviço, produtos e brindes até a data da comunicação da Rescisão, acrescido de multa de 50% sobre o valor do contrato.
<br /><br />
(d) De 74 dias até a data do evento ao término do ano letivo da turma: A Contratada reterá o valor integral do contrato.
<br /><br />
(e) No caso de reprovação comprovada, e o formando não pretender realizar a festa, poderá solicitar o reembolso de 50% do valor total pago até 01 semana antes do evento.
<br /><br />
11. Constitui obrigação do(a) CONTRATANTE manter seu endereço e demais dados cadastrais atualizados, bem como a responsabilidade pela grafia correta de seu nome, nos textos originais que deverão ser entregues à CONTRATADA para a execução dos serviços gráficos, eximindo a CONTRATADA pela falta do nome e/ou grafia incorreta. 
<br /><br />
12. A empresa não se responsabiliza por objetos pessoais em todos os eventos organizados pela empresa. 
<br /><br />
13. Em caso de CASO FORTUITO OU de FORÇA MAIOR, o CONTRATANTE, autoriza o CONTRATADO a substituir produtos, serviços ou até mesmo o espaço, por outro(s) de igual ou melhor qualidade. 
<br /><br />
14. O (A) CONTRATANTE tem ciência da lei estadual número 14.592 de 19 de outubro de 2011, É proibida a venda, oferta, fornecimento, entrega e permissão do consumo de bebida alcóolica, ainda que gratuitamente, aos menores de 18 anos de idade. 
<br /><br />
14.1. Todo e qualquer menor que for convidado pelo CONTRATANTE, que flagrado ingerindo bebida alcoólica, ou tendo feito uso de bebida alcoólica no evento, é de TOTAL responsabilidade do CONTRATANTE, eximindo o CONTRATADO, de qualquer multa aplicada pela Lei acima descrita, e trazendo para si todo o ônus que poderá acarretar essa multa, como valores a pagar, honorários advocatícios, entre outros pelo descumprimento da lei.
<br /><br />
14.2. O CONTRATANTE que quiser levar sua garrafa de bebida alcoólica deverá preencher um TERMO DE RESPONSABILIDADE junto ao CONTRATADO, discriminando qual é o tipo de bebida, a marca e a quantidade, além do que estar se responsabilizando, pelo consumo da(s) mesma(s), seja para maiores ou menores de 18 anos, isentando o CONTRATADO de qualquer penalidade, referente à lei acima e assumindo as penalidades previstas da mesma.
<br /><br />
15. O número mínimo foi definido pela comissão de formatura. Caso esse número não seja atingido, a comissão de formatura irá discutir com a empresa formas de se readequar o orçamento.
<br /><br />
16. Fica eleito o Foro da Capital do Estado de São Paulo por mais privilegiado que outro seja, arcando a parte vencida com o ônus da sucumbência, inclusive honorários advocatícios de 20% do total da condenação.
<br /><br />
A RK Formaturas não cobra taxa de emissão de boletos. Para imprimi-los, acesse o espaço do formando com o seu login e senha e tenha acesso à todos os seus boletos na Área Financeira. 
<br /><br />
Caso opte por pagar em cheque, lembramos que é sua responsabilidade a entrega dos cheques para o representante da RK ou comissão de formatura no colégio ou diretamente em nossa sede. Sempre exija o seu recibo. 
<br /><br />
Lembramos que caso opte em pagar em cheque mas decida posteriormente pagar em boletos, não há problemas, basta acessar sua área financeira e imprimir os boletos. 
<br /><br />
Além disso, a RK disponibiliza um serviço adicional de envio dos boletos impressos. Caso queira recebê-los em casa, opte por boletos impressos. 
<br /><br />
Será cobrado o valor de R$ 45,00 referente ao envio dos mesmos. Lembramos que essa taxa não é obrigatória e você facilmente pode acessar todos os seus boletos online. Além disso, caso opte em receber os boletos na sua casa, você poderá também entrar no site e imprimir uma segunda via a qualquer momento.
</p>