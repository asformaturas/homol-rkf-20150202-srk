<?php if (sizeof($despesasExtras) == 0) { ?>
    <h2 class="fg-color-red">Nenhuma despesa de extras cadastrada</h2>
<?php } else { ?>
    <h3>Despesas Com Extras</h3>
    <br />
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col">Parcela</th>
                <th scope="col">Valor<br />Base</th>
                <th scope="col">Data de<br />Vencimento</th>
                <th scope="col">Data de<br />Pagamento</th>
                <th scope="col">Valor<br />Creditado</th>
                <th scope="col">Status<br />(Valor Base)</th>
                <th scope="col">Saldo da<br />Parcela</th>
                <?php /* if ($gerarBoleto) : */ ?>
                    <th scope="col">&nbsp;</th>
                <?php /* endif; */ ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($despesasExtras as $despesaExtra): ?>
                <?php
                $color = "";
                if ($despesaExtra['status'] == "aberta" && strtotime($despesaExtra['data_vencimento']) < strtotime('now'))
                    $color = "fg-color-red";
                elseif ($despesaExtra['status'] == "paga" && $despesaExtra['saldo_parcela'] <= 0)
                    $color = "fg-color-green";
                elseif ($despesaExtra['status'] == "paga" && $despesaExtra['saldo_parcela'] > 0)
                    $color = "fg-color-yellow";
                if ($despesaExtra['saldo_parcela'] == 0)
                    $saldoParcela = "-";
                elseif ($despesaExtra['saldo_parcela'] < 0)
                    $saldoParcela = "R$" . number_format($despesaExtra['saldo_parcela'] * -1, 2, ',', '.');
                else
                    $saldoParcela = "R$" . number_format($despesaExtra['saldo_parcela'], 2, ',', '.');
                ?>
                <tr>
                    <td><?= $despesaExtra['parcela'] . ' de ' . $despesaExtra['total_parcelas']; ?></td>
                    <td><?= "R$" . number_format($despesaExtra['valor'], 2, ',', '.') ?></td>
                    <td><?= date('d/m/Y', strtotime($despesaExtra['data_vencimento'])) ?></td>
                    <td>
                        <?php
                        if ($despesaExtra['status'] == 'paga' && !empty($despesaExtra['data_pagamento']))
                            echo date('d/m/Y', strtotime($despesaExtra['data_pagamento']));
                        else
                            echo '-';
                        ?>
                    </td>
                    <td><?= "R$" . number_format($despesaExtra['valor_pago'], 2, ',', '.') ?></td>
                    <?php //if($despesaExtra['conciliacao'] == 0) : ?>
                    <td class="<?= $color != "" ? "$color" : "" ?>"><?= $despesaExtra['status'] ?></td>
                    <?php //else : ?>
                    <!-- <td class='fg-color-orangeDark'>Aguardando Pagamento</td> -->
                    <?php //endif; ?>	
                    <td class="<?= $color != "" ? "$color" : "" ?>"><?= $saldoParcela ?></td>
                    <td>
                        <?php if ($despesaExtra['status'] == "aberta") : ?>
                            <a class="button mini default" href="/<?= $this->params['prefix'] ?>/area_financeira/gerar_boleto/<?= $despesaExtra['id'] ?>/<?= $despesaExtra['usuario_id'] ?>"
                               tabindex="-1" target="_blank">
                                Gerar Boleto
                                <i class="icon-new"></i>
                            </a>
                            <!-- <a href="<?= $this->params['prefix'] ?>/area_financeira/gerar_boleto/<?= $despesaExtra['id'] ?>"
                               class='button mini default' target='_blank'>
                                Gerar Boleto
                                <i class="icon-new"></i>
                            </a> -->
                        <?php endif; ?>				
                    </td>
                </tr>
    <?php endforeach; ?>
        </tbody>
    </table>
<?php } ?>
<?php if (sizeof($resumoExtras) > 0) { ?>
    <?php $compraId = 0; ?>
    <br />
    <h3>Resumo De Itens Comprados</h3>
    <br />
    <table class="table table-condensed table-striped">
        <?php foreach ($resumoExtras as $extra) : ?>
            <?php if ($extra['ViewRelatorioExtras']['campanhas_usuario_id'] != $compraId) : ?>
            <?php $compraId = $extra['ViewRelatorioExtras']['campanhas_usuario_id']; ?>
                <tr class="header">
                    <td colspan="5">
                        Compra Efetuada em <?= date('d/m/Y', strtotime($extra['ViewRelatorioExtras']['data_compra'])) ?> 
                        &agrave;s <?= date('H:i', strtotime($extra['ViewRelatorioExtras']['data_compra'])) ?>
                    </td>
                </tr>
        <?php endif; ?>
            <tr>
                <td><?= $extra['ViewRelatorioExtras']['evento'] ?></td>
                <td><?= $extra['ViewRelatorioExtras']['campanha'] ?></td>
                <td><?= $extra['ViewRelatorioExtras']['extra'] ?></td>
                <td><?= $extra['ViewRelatorioExtras']['quantidade_total'] ?></td>
                <td width="10%"><?= $extra['ViewRelatorioExtras']['status'] ?></td>
            </tr>
    <?php endforeach; ?>
    </table>
<?php } ?>
