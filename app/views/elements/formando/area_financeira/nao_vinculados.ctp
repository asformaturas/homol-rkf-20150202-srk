<?php if(sizeof($naoVinculados) == 0) { ?>
<h2 class="fg-color-red">Nenhum Pagamento N&atilde;o Vinculado Encontrado</h2>
<?php } else { ?>
<h3>Pagamentos N&atilde;o Vinculados</h3>
<br />
<table class="table table-condensed table-striped">
    <thead>
        <tr>
            <th scope="col">Tipo</th>
            <th scope="col">N&ordm; Documento</th>
            <th scope="col">Valor Creditado</th>
            <th scope="col">Data de Liquida&ccedil;&atilde;o</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($naoVinculados as $naoVinculado): ?>
            <tr>
                <td><?= str_replace("_", " ", $naoVinculado['tipo']); ?></td>
                <td>
                    <?= !empty($naoVinculado['codigo']) ? $naoVinculado['codigo'] : "N&atilde;o Dispon&iacute;vel" ?>
                </td>
                <td>R$<?= number_format($naoVinculado['valor_pago'], 2, ',', '.') ?></td>
                <td>
                    <?php
                    if (strtotime($naoVinculado['dt_liquidacao']) > strtotime("-10 year"))
                        echo date('d/m/Y', strtotime($naoVinculado['dt_liquidacao']));
                    else
                        echo 'N&atilde;o Dispon&iacute;vel';
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php } ?>