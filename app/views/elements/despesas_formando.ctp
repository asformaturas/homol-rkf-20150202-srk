<?php
echo $html->script('jquery.tools.min');
echo $html->css('tooltip');
echo $html->css('messages');
if ($config['gerar_boleto'])
    echo $form->create('GeraBoleto', array('url' => "/{$this->params['prefix']}/area_financeira/", 'target' => '_blank'));
?>
<style type="text/css">
    .parcelamento-igpm .opcoes { display:none }
    .pagamentos-igpm tbody tr:nth-child(odd) td { background-color:#F5FFFF }
</style>
<script type="text/javascript">
    jQuery(function($) {
        jQuery(".help").tooltip();
        $('.parcelamento-igpm .parcelar').click(function() {
            var that = this;
            if ($('.parcelamento-igpm .opcoes').is(':hidden')) {
                $('.parcelamento-igpm .opcoes').fadeIn(500, function() {
                    $(that).text('Cancelar');
                });
            } else {
                $('.parcelamento-igpm .opcoes').fadeOut(500, function() {
                    $(that).text('Parcelar Saldo');
                });
            }
        });
        $('.parcelamento-igpm .enviar-parcelamento').click(function() {
            var url = "/<?= $this->params["prefix"] ?>/area_financeira/";
            $.ajax({
                url: url + "parcelar_igpm/<?= $formando['id'] ?>",
                data: {parcelas: $('.parcelas-igpm').val()},
                type: "POST",
                dataType: "json",
                async: false,
                success: function(response) {
                    if ($('.pagamentos-igpm').is(':hidden'))
                        $('.pagamentos-igpm').fadeIn(500);
                    $.each(response.parcelas, function(i, parcela) {
                        $("<tr>", {class: "parcela-criada", css: {display: "none"}})
                                .append($("<td>", {text: "R$" + parcela.valor, css: {width: "10%"}}))
                                .append($("<td>", {text: parcela.linha_digitavel}))
                                .append($("<td>", {css: {width: "20%"}})
                                        .append($('<a>', {text: "Gerar Boleto", class: "submit", target: "_blank", href: url + "gerar_boleto_igpm/" + parcela.codigo}))
                                        ).appendTo($('.pagamentos-igpm tbody'));
                    });
                    $(".parcela-criada").fadeIn(500, function() {
                        $(this).removeClass('parcela-criada').addClass('parcela-aberta');
                    });
                    $('.pagamentos-igpm tbody').append(
                            $("<tr>")
                            .append($("<td>", {colspan: 3})
                                    .append($("<p>", {html: response.mensagem, css: {'text-align': 'justify', 'margin': '10px 0'}}))));
                    $('.parcelamento-igpm').remove();
                },
                error: function() {
                    alert('Erro ao criar parcelas. Por favor tente mais tarde.');
                }
            });
        });
        $(".cancelar-extra").click(function() {
            id = $(this).attr('dir');
            $(this).fadeOut(500, function() {
                var load = "<img src='<?= $this->webroot ?>img/load.gif' width='15'/>";
                $(this).parent().append(load);
                url = "/<?= $this->params["prefix"] ?>/area_financeira/cancelar_extra/" + id;
                $.ajax({
                    url: url,
                    type: "GET",
                    dataType: "json",
                });
                $('img').remove();
                $('#remover').remove();
                $(this).parent().append("<p>Extra cancelado</p>");
            });
        });
    });
</script>
<?php if ($formando['turma_id'] == 5560) : ?>
    <div class='message info' style="display:block; width:96%!important">
        <h3>Importante!</h3>
        <p>
            Entrega de convites em andamento.<br />
            Para verificar suas informa&ccedil;&otilde;es financeiras por favor entre em contato com o atendimento 
            da &Aacute;S Formaturas.<br />
            Tel (11) 5594-3043
            <br />
            Email <a href="mailto:atendimento@rkformaturas.com.br">atendimento@rkformaturas.com.br</a>
    </div>
<?php else: ?>
    <div style="overflow:hidden; width:100%" class="clearfix">
        <div class="legenda" style="width:50%; margin:10px 0 0 0px; border:1px solid #989999">
            <table>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td colspan="2"><h2>Informa&ccedil;&otilde;es da ades&atilde;o</h2></td>
                            </tr>
                            <tr height="5">
                                <td colspan="2"></td>
                            </tr>
                            <tr>
                                <td width="200">Nome</td>
                                <td><?=$formando['nome'] ?></td>
                            </tr>
                            <tr>
                                <td width="200">C&oacute;digo</td>
                                <td><?= $formando['codigo_formando'] ?></td>
                            </tr>
                            <tr>
                                <td width="200">Grupo</td>
                                <td><?= $formando['grupo'] ?></td>
                            </tr>
                            <tr>
                                <td width="200">Data contrato da Turma</td>
                                <td><?= date('d/m/Y', strtotime($turma['Turma']['data_assinatura_contrato'])); ?></td>
                            </tr>
                            <tr>
                                <td>Data Adesão</td>
                                <td><?= date('d/m/Y', strtotime($formando['data_adesao'])); ?></td>
                            </tr>
                            <?php if (!empty($formando['Parcelamento']['titulo'])) : ?>
                                <tr>
                                    <td>Plano de Ades&atilde;o</td>
                                    <td><?= $formando['Parcelamento']['titulo'] ?></td>
                                </tr>
                            <?php endif; ?>
                            <tr>
                                <td>Data IGPM</td>
                                <td><?= !empty($turma['Turma']['data_igpm']) ? date('d/m/Y', strtotime($turma['Turma']['data_igpm'])) : "IGPM ainda não aplicado"; ?></td>
                            </tr>
                            <tr>
                                <td>Mesas do contrato</td>
                                <td><?= $turma['Turma']['mesas_contrato']; ?></td>
                            </tr>
                            <tr>
                                <td>Convites do contrato</td>
                                <td><?= $turma['Turma']['convites_contrato']; ?></td>
                            </tr>
                            <tr>
                                <td>Número de Parcelas do Contrato</td>
                                <td><?=$formando['parcelas_adesao']; ?></td>
                            </tr>
                            <?php if ($cancelado) { ?>
                                <tr>
                                    <td><div style='background:red; font-weight:bold; color:white; text-align:center; padding:5px'>Contrato Cancelado</div></td>
                                    <td style='text-align:left!important; padding-left:10px'>
                                        <?php if (!empty($protocolo)) { ?>
                                            <?= $html->link('Mais Informações', array($this->params['prefix'] => true, 'controller' => 'solicitacoes', 'action' => 'visualizar', $protocolo), array('class' => 'submit button')); ?>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div class="legenda clearfix" style="width:40%; margin:10px 0 0 10px; border:1px solid #989999">
            <table>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td colspan="2"><h2>Resumo financeiro</h2></td>
                            </tr>
                            <tr height="5">
                                <td colspan="2"></td>
                            </tr>
                            <tr>
                                <td>Valor de Contrato</td>
                                <td>R$<?=number_format($totalAdesao, 2, ',', '.'); ?></td>
                            </tr>
                            <tr>
                                <td>Valor de IGPM</td>
                                <td>R$<?=number_format($totalIGPM, 2, ',', '.'); ?></td>
                            </tr>
                            <tr>
                                <td>Valor de Contrato c/ correção</td>
                                <td>R$<?= number_format($totalAdesao + $totalIGPM, 2, ',', '.'); ?></td>
                            </tr>
                            <tr>
                                <td width="200">Total Recebido</td>
                                <td id='total-pago'>R$<?= number_format($totalPago, 2, ',', '.'); ?></td>
                            </tr>
                            <tr>
                                <?php if($saldoAtual > 0) : ?>
                                <td style="color:green">Saldo Positivo</td>
                                <?php elseif($saldoAtual < 0) : ?>
                                <td style="color:red">Saldo Negativo</td>
                                <?php else : ?>
                                <td>Saldo</td>
                                <?php endif; ?>
                                <td>R$<?=number_format(abs($saldoAtual), 2, ',', '.'); ?></td>
                            </tr>
                            <?php if (!$cancelado && $formando['realizou_checkout'] == "0") : ?>
                                <tr>
                                    <td style="padding-top:10px; text-align:right" colspan="2">
                                        <?php
                                        if ($config['renegociar_contrato'])
                                            echo $html->link('Renegociar Parcelas Em Atraso', array($this->params['prefix'] => true, 'controller' => 'area_financeira', 'action' => 'renegociacao', $formando['id']), array('class' => 'button submit'));
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top:10px; text-align:right" colspan="2">
                                        <?php
                                        if ($config['cancelar_contrato'])
                                            echo $html->link('Cancelar Contrato', array($this->params['prefix'] => true, 'controller' => 'area_financeira', 'action' => 'cancelamento', $formando['id']), array('class' => 'button submit'));
                                        ?>
                                    </td>
                                </tr>
                            <?php endif; ?>
                            <?php if ($turma['Turma']['checkout'] == 'aberto' && !$cancelado && $formando['realizou_checkout'] == "0") : ?>
                                <tr>
                                    <td style="padding-top:10px; text-align:right" colspan="2">
                                        <?=
                                        $html->link('Checkout', array($this->params['prefix'] => true, 'controller' => 'checkout', 'action' => 'index'), array('class' => 'button submit'));
                                        ?>
                                    </td>
                                </tr>
                            <?php endif; ?>
                            <?php if ($this->params['prefix'] == 'atendimento' && !$cancelado && $formando['realizou_checkout'] == "1") : ?>
                                <tr>
                                    <td style="padding-top:10px; text-align:right" colspan="2">
                                        <?=
                                        $html->link('Comprar Itens Checkout', array($this->params['prefix'] => true, 'controller' => 'checkout', 'action' => 'comprar_itens', $formando['id']), array('class' => 'button submit'));
                                        ?>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="grid_full alpha omega first" id="conteudo-container">
        <?php $session->flash(); ?>
        <div>
            <h2><b>Despesas com Ades&atilde;o</b></h2>
        </div>
        <br>
        <?php
        if (sizeof($despesas_adesao) == 0) {
            echo '<p style="color:red">Nenhuma despesa com adesão encontrada.</p>';
        } else {
            ?>
            <div class="grid_full container-tabela">
                <table class="tabela-boletos">
                    <thead>
                        <tr>
                            <th scope="col">Parcela</th>
                            <th scope="col">Valor Base</th>
                            <th scope="col">Data de<br />Vencimento</th>
                            <th scope="col">Data de<br />Pagamento</th>
                            <th scope="col">Valor Creditado</th>
                            <th scope="col"><span class='item'>Valor IGPM<div class='help' title='<?= $textoIGPM ?>'></div></span></th>
                            <th scope="col">Status <br />(Valor Base)</th>
                            <th scope="col">Saldo da Parcela</th>
                            <th scope="col">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $isOdd = false; ?>
                        <?php $igpm = 0; ?>
                        <?php foreach ($despesas_adesao as $despesa_adesao): ?>
                            <?php
                            if ($despesa_adesao["Despesa"]["status"] == "aberta") {
                                $valorPago = 0;
                                $valorPagoParaExibicao = 0;
                            } else {
                                if (isset($despesa_adesao['DespesaPagamento'][0]['Pagamento'])) {
                                    if ($despesa_adesao['DespesaPagamento'][0]['Pagamento']['valor_pago'] < $despesa_adesao['DespesaPagamento'][0]['Pagamento']['valor_nominal'])
                                        $valorPago = $despesa_adesao['DespesaPagamento'][0]['Pagamento']['valor_pago'];
                                    else
                                        $valorPago = $despesa_adesao['DespesaPagamento'][0]['Pagamento']['valor_nominal'];
                                } else {
                                    $valorPago = 0;
                                }
                                if ($valorPago > $despesa_adesao['Despesa']['valor'])
                                    $valorPago = $despesa_adesao['Despesa']['valor'];
                                if ($despesa_adesao['Despesa']['status_igpm'] == 'pago') {
                                    if ($despesa_adesao['DespesaPagamento'][0]['Pagamento']['valor_pago'] >
                                            $despesa_adesao['Despesa']['valor'] + $despesa_adesao['Despesa']['correcao_igpm'])
                                        $valorPagoParaExibicao = $despesa_adesao['Despesa']['valor'] + $despesa_adesao['Despesa']['correcao_igpm'];
                                    else
                                        $valorPagoParaExibicao = $despesa_adesao['DespesaPagamento'][0]['Pagamento']['valor_pago'];
                                } else {
                                    $valorPagoParaExibicao = $valorPago;
                                }
                                if ($despesa_adesao["Despesa"]["status"] == "cancelada")
                                    $valorPago = 0;
                            }
                            ?>
                            <tr class="linha-despesa hover<?= $isOdd ? " odd" : "" ?>"  id = "linha-despesa-<?= $despesa_adesao['Despesa']['id']; ?>">
                                <td  colspan="1" width="10%"><?= $despesa_adesao['Despesa']['parcela'] . ' de ' . $despesa_adesao['Despesa']['total_parcelas']; ?></td>
                                <td  colspan="1" width="10%">
                                    <span<?= $despesa_adesao['Despesa']['status'] == 'aberta' ? " id='valor-despesa-{$despesa_adesao['Despesa']['id']}'" : "" ?>>
                                        <?=$despesa_adesao["Despesa"]["status"] == "paga" ? "-" : "R\${$despesa_adesao['Despesa']['valor']}"; ?>
                                    </span>
                                </td>
                                <td valign="center" colspan="1" width="10%"><span<?= $despesa_adesao['Despesa']['status'] == 'aberta' ? " id='data-despesa-{$despesa_adesao['Despesa']['id']}'" : "" ?>><?= date('d/m/Y', strtotime($despesa_adesao['Despesa']['data_vencimento'])); ?></span></td>
                                <td  colspan="1" width="10%">
                                    <?php
                                    if (($despesa_adesao['Despesa']['status'] == 'paga' || $despesa_adesao['Despesa']['status'] == 'renegociada') &&
                                            !empty($despesa_adesao['Despesa']['data_pagamento']))
                                        echo date('d/m/Y', strtotime($despesa_adesao['Despesa']['data_pagamento']));
                                    else
                                        echo '-';
                                    ?>
                                </td>
                                <td class='valor-pago' soma="<?= $valorPago ?>" width="10%"><?= $valorPagoParaExibicao > 0 ? "R$$valorPagoParaExibicao" : "-" ?></td>
                                <?php
                                $valor_igmp = !empty($despesa_adesao['Despesa']['correcao_igpm']) ? $despesa_adesao['Despesa']['correcao_igpm'] : 0;
                                $saldo_parcela = $valorPago - ($despesa_adesao['Despesa']['valor']);
                                $saldo_parcela-= ($despesa_adesao['Despesa']['status'] == 'aberta' ? $valor_igmp : 0);
                                $color = $saldo_parcela < 0 ? "red" : "green";
                                ?>
                                <td><?= $valor_igmp > 0 ? "R\${$valor_igmp}" : "Não Aplicado"; ?></td>
                                <td  colspan="1" width="10%">
                                    <?php
                                    if ($despesa_adesao['Despesa']['status'] == 'paga' && $saldo_parcela < 0)
                                        echo "<span style='color:orange;'>Paga Parcialmente</span>";
                                    elseif ($despesa_adesao['Despesa']['status'] == 'aberta' || $despesa_adesao['Despesa']['status'] == 'vencida')
                                        echo "<span style='color:red;'>{$despesa_adesao['Despesa']['status']}</span>";
                                    elseif ($despesa_adesao['Despesa']['status'] == 'paga')
                                        echo "<span style='color:green;'>{$despesa_adesao['Despesa']['status']}</span>";
                                    elseif ($despesa_adesao['Despesa']['status'] == 'renegociada')
                                        echo "<span style='color:purple;'>{$despesa_adesao['Despesa']['status']}</span>";
                                    else
                                        echo "<span style='color:orange;'>{$despesa_adesao['Despesa']['status']}</span>";
                                    ?>
                                </td>
                                <td width="10%" style="color:<?= $saldo_parcela < 0 ? "red" : "green" ?>" class="saldo-parcela" id='saldo-parcela-<?= $despesa_adesao['Despesa']['id'] ?>'>
                                    <?php $saldo_parcela = $saldo_parcela < 0 ? $saldo_parcela * -1 : $saldo_parcela; ?>
                                    <?= $saldo_parcela == 0 ? "-" : "R$" . number_format($saldo_parcela, 2) ?>
                                </td>
                                <td  colspan="1" width="20%">
                                    <?php
                                    if ($config['gerar_boleto']) {
                                        $boletoAtrasado = "Op&ccedil;&atilde;o para pagamento de boletos em atraso pela internet, no site do seu banco.<br />";
                                        $boletoAtrasado.= "A data de vencimento do boleto ser&aacute; hoje.<br /> O valor do boleto ser&aacute; soma do valor base + valor da multa por atraso.";
                                        if ($despesa_adesao['Despesa']['status'] == 'aberta')
                                            echo $html->link('Gerar Boleto', array($this->params['prefix'] => true, 'controller' => 'area_financeira', 'action' => 'gerar_boleto', $despesa_adesao['Despesa']['id']), array('class' => 'submit botao-gerar-boleto', 'target' => '_blank'));

                                        if ($despesa_adesao['Despesa']['status_igpm'] == 'nao_pago' && $despesa_adesao['Despesa']['status'] == 'paga' && false)
                                            echo $html->link('Boleto IGPM', array($this->params['prefix'] => true, 'controller' => 'area_financeira', 'action' => 'gerar_boleto_igpm', $despesa_adesao['Despesa']['id']), array('class' => 'submit botao-gerar-boleto', 'target' => '_blank'));

                                        if (strtotime('now') > strtotime($despesa_adesao['Despesa']['data_vencimento']) && $despesa_adesao['Despesa']['status'] == 'aberta')
                                            echo "&nbsp;&nbsp;<a href='/{$this->params['prefix']}/area_financeira/gerar_boleto_com_multa/{$despesa_adesao['Despesa']['id']}' class='submit botao-gerar-boleto item' target='_blank'>Boleto Atrasado<div class='help' title='$boletoAtrasado'></div></a>";
                                    }
                                    if ($despesa_adesao['Despesa']['status_igpm'] == 'nao_pago' && $despesa_adesao['Despesa']['status'] == 'paga')
                                        $igpm+= $despesa_adesao['Despesa']['correcao_igpm'];
                                    ?>
                                </td>
                            </tr>
                            <?php $isOdd = !($isOdd); ?>
                        <?php endforeach; ?>
                        <?php
                        if ($despesaIgpm) :
                            $saldoIgpm = $totalPagoIgpm - $despesaIgpm['Despesa']['valor'];
                            ?>
                            <tr class="linha-despesa hover<?= $isOdd ? " odd" : "" ?>">
                                <td  colspan="1" width="10%"><span class="item">IGPM<div class='help' title='<?= $textoParcelaIgpm ?>'></div></span></td>
                                <td  colspan="1" width="10%">
                                    <span>
            <?= "R\${$despesaIgpm['Despesa']['valor']}"; ?>
                                    </span>
                                </td>
                                <td valign="center" colspan="1" width="10%">-</td>
                                <td  colspan="1" width="10%">-</td>
                                <td class='valor-pago' soma="<?= $totalPagoIgpm ?>" width="10%"><?= $totalPagoIgpm > 0 ? "R$$totalPagoIgpm" : "-" ?></td>
                                <td></td>
                                <?php
                                $color = $saldoIgpm < 0 ? "red" : "green";
                                ?>
                                <td colspan="1" width="10%">
                                    <?php
                                    if ($despesaIgpm['Despesa']['status'] == 'paga' && $saldo_parcela < 0)
                                        echo "<span style='color:orange;'>Paga Parcialmente</span>";
                                    else if ($despesaIgpm['Despesa']['status'] == 'aberta' || $despesaIgpm['Despesa']['status'] == 'vencida')
                                        echo "<span style='color:red;'>{$despesaIgpm['Despesa']['status']}</span>";
                                    else if ($despesaIgpm['Despesa']['status'] == 'paga')
                                        echo "<span style='color:green;'>{$despesaIgpm['Despesa']['status']}</span>";
                                    else
                                        echo "<span style='color:orange;'>{$despesaIgpm['Despesa']['status']}</span>";
                                    ?>
                                </td>
                                <td width="20%" style="color:<?= $color ?>" class="saldo-despesa <?= $saldoIgpm < 0 ? 'negativo' : '' ?>">
                                    <?php $saldoIgpm = $saldoIgpm < 0 ? $saldoIgpm * -1 : $saldoIgpm; ?>
            <?= $saldoIgpm == 0 ? "-" : "R$" . number_format($saldoIgpm, 2, ',', '.') ?>
                                </td>
                                <td width="10%"></td>
                            </tr>
                            <tr height="30"><td></td></tr>
                            <tr>
                                <td colspan='4' style='vertical-align:top'>
            <?php if ($despesaIgpm['Despesa']['status'] != 'paga') { ?>
                                        <table>
                                            <tr>
                                                <td class='container-parcerias' colspan='3'>
                                                    <h3 class='fundo-vermelho'>Ajuste IGPM de Parcelas Pagas</h3>
                                                </td>
                                            </tr>
                                            <tr height="10"><td></td></tr>
                                            <tr>
                                                <td width="30%"><strong>Valor</strong></td>
                                                <td width="40%"></td>
                                                <td width="30%"><?= "R\${$despesaIgpm['Despesa']['valor']}" ?></td>
                                            </tr>
                                            <tr>
                                                <td><strong>Total Pago</strong></td>
                                                <td></td>
                                                <td><?= $totalPagoIgpm > 0 ? "R\${$totalPagoIgpm}" : "-" ?></td>
                                            </tr>
                                            <tr>
                                                <td><strong>Saldo</strong></td>
                                                <td></td>
                                                <td>R$<?= $saldoIgpm ?></td>
                                            </tr>
                <?php if (empty($parcelasIgpm)) : ?>
                                                <tr height="10"><td></td></tr>
                                                <tr class='parcelamento-igpm'>
                                                    <td>
                                                        <span class='submit button parcelar'>Parcelar Saldo</span>
                                                    </td>
                                                    <td class='opcoes'>
                                                        <select class='parcelas-igpm'>
                                                            <?php $max = $this->params['prefix'] == "atendimento" ? 10 : 3 ?>
                                                            <?php for ($a = 1; $a <= $max; $a++) : ?>
                                                                <option value='<?= $a ?>'><?= $a ?>x</option>
                    <?php endfor; ?>
                                                        </select>
                                                    </td>
                                                    <td class='opcoes'>
                                                        <span class='submit button enviar-parcelamento'>Confirmar</span>
                                                    </td>
                                                </tr>
                <?php endif; ?>
                                            <tr height="20"><td></td></tr>
                                        </table>
            <?php } ?>
                                </td>
                                <td colspan='5' style='vertical-align:top'>
                                    <table class='pagamentos-igpm' style="<?= empty($parcelasIgpm) ? "display:none;" : "" ?>">
                                        <tbody>
                                            <tr>
                                                <td class='container-parcerias' colspan='3'>
                                                    <h3 class='fundo-vermelho'>Parcelas IGPM</h3>
                                                </td>
                                            </tr>
                                            <tr height="10"><td></td></tr>
            <?php foreach ($parcelasIgpm as $parcela) : ?>
                                                <tr>
                                                    <td>
                                                        <span class="item">
                <?= "R\${$parcela['Pagamento']['valor_nominal']}" ?>
                                                            <div class='help' title='<?= $textoParcelaIgpm ?>'></div>
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        if ($parcela['Pagamento']['status'] == 'pago') {
                                                            if (date('Y-m-d H:i:s', strtotime($parcela['Pagamento']['dt_liquidacao'])) != $parcela['Pagamento']['dt_liquidacao'])
                                                                echo "Data de pagamento n&atilde;o dispon&iacute;vel";
                                                            else
                                                                echo "Parcela paga em " . date('d/m/Y', strtotime($parcela['Pagamento']['dt_liquidacao']));
                                                        } else {
                                                            echo $parcela['Pagamento']['linha_digitavel'];
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                <?php if ($parcela['Pagamento']['status'] == 'aberto') : ?>
                                                            <a target="_blank" href="<?= $this->webroot . $this->params['prefix'] ?>/area_financeira/gerar_boleto_igpm/<?= $parcela['Pagamento']['id'] ?>" class='submit'>
                                                                Gerar Boleto
                                                            </a>
                <?php endif; ?>
                                                    </td>
                                                </tr>
            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
        <?php endif; ?>
                    </tbody>
                </table>
            </div>
    <?php } ?>
        <div class="grid_full alpha omega first" id="div_despesas_extras">
            <h2><b>Despesas com Extras</b></h2>
            <br />
            <?php
            if (sizeof($despesas_extras) == 0) {
                echo '<p style="color:red">Nenhuma despesa com extras encontrada.</p>';
            } else {
                ?>
                <div class="grid_full container-tabela" style="margin:5px 0 10px 0">
                    <table class='tabela-boletos'>
                        <thead>
                            <tr>					
                                <th scope="col">Campanha</th>
                                <th scope="col">Valor</th>
                                <th scope="col">Parcela</th>
                                <th scope="col">Data de Vencimento</th>
                                <th scope="col">Data de Pagamento</th>
                                <th scope="col">Valor Creditado</th>
                                <th scope="col">Status (Valor base)</th>
                                <th scope="col">Saldo da Parcela</th>
                                <th scope="col">&nbsp;</th>
                                <th scope="col">&nbsp;</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr class="linha-despesa">
                                <td colspan="4">
                                </td>
                                <td colspan="2"><?= count($despesas_extras); ?> despesas com extras</td>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php $isOdd = false; ?>
                            <?php foreach ($despesas_extras as $despesa_extra): ?>
                                <?php
                                if ($despesa_extra["Despesa"]["status"] == "aberta") {
                                    $valorPago = 0;
                                } else {
                                    if (isset($despesa_extra['DespesaPagamento'][0]['Pagamento'])) {
                                        if ($despesa_extra['DespesaPagamento'][0]['Pagamento']['valor_pago'] < $despesa_extra['DespesaPagamento'][0]['Pagamento']['valor_nominal'])
                                            $valorPago = $despesa_extra['DespesaPagamento'][0]['Pagamento']['valor_pago'];
                                        else
                                            $valorPago = $despesa_extra['DespesaPagamento'][0]['Pagamento']['valor_nominal'];
                                    } else {
                                        $valorPago = 0;
                                    }
                                    $valorPago = $valorPago > $despesa_extra['Despesa']['valor'] ? $despesa_extra['Despesa']['valor'] : $valorPago;
                                }
                                $saldoParcela = $valorPago - $despesa_extra['Despesa']['valor'];
                                ?>
                                <tr class="<?= $isOdd ? "odd" : "" ?> linha-despesa hover">
                                    <td  colspan="1" width="20%"><?= $despesa_extra['CampanhasUsuario']['Campanha']['nome']; ?></td>
                                    <td  colspan="1" width="10%"><?= $despesa_extra['Despesa']['valor']; ?></td>
                                    <td  colspan="1" width="10%"><?= $despesa_extra['Despesa']['parcela'] . ' de ' . $despesa_extra['Despesa']['total_parcelas']; ?></td>
                                    <td  colspan="1" width="10%">
                                        <span<?= $despesa_extra['Despesa']['status'] == 'aberta' ? " id='data-despesa-{$despesa_extra['Despesa']['id']}'" : "" ?>>
            <?= date('d/m/Y', strtotime($despesa_extra['Despesa']['data_vencimento'])); ?>
                                        </span>
                                    </td>					
                                    <td  colspan="1" width="10%">
                                        <?php
                                        if ($despesa_extra['Despesa']['status'] == 'paga') {
                                            echo date('d/m/Y', strtotime($despesa_extra['Despesa']['data_pagamento']));
                                        } else {
                                            echo '-';
                                        }
                                        ?>
                                    </td>
                                    <td class='valor-pago' soma="<?= $valorPago ?>"><?= $valorPago > 0 ? "R$$valorPago" : "-" ?></td>				
                                    <td  colspan="1" width="15%">
                                        <?php
                                        if ($despesa_extra['Despesa']['status'] == 'paga' && $saldoParcela < 0)
                                            echo "<span style='color:orange;'>Paga Parcialmente</span>";
                                        else if ($despesa_extra['Despesa']['status'] == 'aberta' || $despesa_extra['Despesa']['status'] == 'vencida')
                                            echo "<span style='color:red;'>{$despesa_extra['Despesa']['status']}</span>";
                                        else if ($despesa_extra['Despesa']['status'] == 'paga')
                                            echo "<span style='color:green;'>{$despesa_extra['Despesa']['status']}</span>";
                                        else
                                            echo "<span style='color:orange;'>{$despesa_extra['Despesa']['status']}</span>";
                                        ?>
                                    </td>
                                    <td style='color:<?= $saldoParcela == 0 ? "inherit" : ($saldoParcela < 0 ? "red" : "green") ?>'>
                                        <?= $saldoParcela == 0 ? "-" : ($saldoParcela < 0 ? "R$" . number_format($saldoParcela * -1, 2, ',', '.') : "R$" . number_format($saldoParcela, 2, ',', '.')) ?>
                                    </td>
                                    <td>
                                        <?php
                                        if ($despesa_extra['Despesa']['status'] == 'aberta' && $config['gerar_boleto'])
                                            echo $html->link('Boleto', array($this->params['prefix'] => true, 'controller' => 'area_financeira', 'action' => 'gerar_boleto', $despesa_extra['Despesa']['id']), array('class' => 'submit botao-gerar-boleto', 'target' => '_blank', 'id' => 'remover'));
                                        ?>
                                    </td>
                                    <td>
                                        <?php if ($despesa_extra['Despesa']['status'] == 'aberta' && $this->params['prefix'] == 'atendimento') { ?>
                                            <span class='cancelar-extra submit' dir="<?= $despesa_extra['Despesa']['id'] ?>">Cancelar</span>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php $isOdd = !($isOdd); ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <?php
                // Final do if que verifica se há despesas com extras 
            }
            ?>
        </div>
        <div class="grid_full alpha omega first" id="div_pagamentos_efetuados">
            <h2><b>Pagamentos recebidos não vinculados.</b></h2>
            <?php
            if (count($boletos) == 0) {
                echo '<p style="color:red">Nenhum pagamento efetuado foi encontrado.</p>';
            } else {
                ?>
                <div class="grid_full container-tabela" style="margin:5px 0 10px 0">
                    <table border="0">
                        <thead>
                            <tr>
                                <th scope="col">Tipo</th>
                                <th scope="col">No do documento</th>
                                <?php if (in_array($this->params['prefix'], array('formando', 'comissao'))) : ?>
                                    <th scope="col">Valor Creditado</th>
                                <?php else : ?>
                                    <th scope="col">Valor Nominal</th>
                                    <th scope="col">Valor Recebido</th>
                                <?php endif; ?>
                                <th scope="col">Data de Vencimento</th>
                                <th scope="col">Data de Liquida&ccedil;&atilde;o</th>
                                <th scope="col">Status</th>
                                <th scope="col">&nbsp;</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <td colspan="4">
                                </td>
                                <td colspan="2"><?= count($boletos); ?> boletos encontrados.</td>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php $isOdd = false; ?>
                            <?php foreach ($boletos as $boleto): ?>
                                <?php $valorPago = $boleto['Pagamento']['valor_pago'] < $boleto['Pagamento']['valor_nominal'] ? $boleto['Pagamento']['valor_pago'] : $boleto['Pagamento']['valor_nominal']; ?>
                                <tr class="<?= $isOdd ? "odd" : "" ?> hover">
                                    <td  colspan="1" width="15%" style="text-transform:capitalize;"><?= str_replace("_", " ", $boleto['Pagamento']['tipo']); ?></td>
                                    <td  colspan="1" width="20%"><?= $boleto['Pagamento']['codigo']; ?></td>
                                    <?php if (in_array($this->params['prefix'], array('formando', 'comissao'))) : ?>
                                        <td  colspan="1" width="10%"><?= $boleto['Pagamento']['valor_nominal']; ?></td>
                                    <?php else : ?>
                                        <td  colspan="1" width="10%"><?= $boleto['Pagamento']['valor_nominal']; ?></td>
                                        <td  colspan="1" width="10%" class='valor-pago' soma="<?= $valorPago ?>"><?= $valorPago ?></td>
                                    <?php endif; ?>
                                    <td  colspan="1" width="15%"><?= date('d/m/Y', strtotime($boleto['Pagamento']['dt_vencimento'])); ?></td>
                                    <td  colspan="1" width="15%"><?= date('d/m/Y', strtotime($boleto['Pagamento']['dt_liquidacao'])); ?></td>				
                                    <td  colspan="1"><?= $boleto['Pagamento']['status']; ?></td>
                                    <td>
                                        <?php if (sizeof($despesas_adesao) > 0 && $config['vincular_pagamentos']) { ?>
                                            <span class='vincular-pagamento submit' id="<?= "pagamento-{$boleto["Pagamento"]["id"]}-usuario-{$formando['id']}" ?>">Vincular Pagamento</span>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php $isOdd = !($isOdd); ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>		
                </div>
            <?php } ?>
        </div>
        <!-- Exibir resumo de extras -->
        <br><br>
        <h2><b>Resumo de Extras</b></h2>
        <?php
        if (count($resumoExtras) == 0) {
            echo '<p style="color:red">Nenhum pagamento de extra efetuado foi encontrado.</p>';
        } else {
            ?>
            <div class="grid_full container-tabela" style="margin:5px 0 10px 0">
                <table>
                    <thead>
                        <tr>
                            <th scope="col">Evento</th>
                            <th scope="col">Extra</th>
                            <th scope="col">Qtde. Total</th>
                            <th scope="col">Qtde. Cancelados</th>
                            <th scope="col">Qtde. Retirados</th>
                            <th scope="col">A Retirar</th>
                            <th scope="col">Total Pago</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $isOdd = false;

                        foreach ($resumoExtras as $extraResumo) :
                            ?>
                            <tr class="linha-despesa <?= $isOdd ? "odd" : "" ?>">
                                <td  colspan="1" width="20%"><?= $extraResumo['ViewRelatorioExtras']['evento']; ?></td>
                                <td  colspan="1" width="20%"><?= $extraResumo['ViewRelatorioExtras']['extra']; ?></td>
                                <td  colspan="1" width="10%"><?= $extraResumo['ViewRelatorioExtras']['quantidade_total'] ?></td>
                                <td  colspan="1" width="10%"><?= $extraResumo['ViewRelatorioExtras']['quantidade_cancelada']; ?></td>
                                <td  colspan="1" width="10%"><?= $extraResumo['ViewRelatorioExtras']['quantidade_retirada']; ?></td>
                                <td  colspan="1" width="15%"><?= $extraResumo['ViewRelatorioExtras']['status'] == 'a retirar' ?
                                    $extraResumo['ViewRelatorioExtras']['quantidade_total'] - $extraResumo['ViewRelatorioExtras']['quantidade_retirada'] - $extraResumo['ViewRelatorioExtras']['quantidade_cancelada'] : 0;
                            ?>
                                </td>
                                <td  colspan="1" width="20%">R$<?= number_format($extraResumo['ViewRelatorioExtras']['valor_pago'], 2, ',', '.') ?></td>
                            </tr>
            <?php $isOdd = !($isOdd); ?>
        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
    <?php } ?>
    </div>
<?php endif; ?>