<script type='text/javascript'>
jQuery(function($) {
	exibeDestino($("#InformativoDestinatario").val());
	$("#InformativoDestinatario").change( function() {
		show = $(this).val();
		exibeDestino(show);
	});
	
	$('#enviar').click(function(event) {
		event.preventDefault();
		$(this).hide();
		desabilitaForm();
		var cancelar = "<input type='button' class='submit' id='cancelar' value='Cancelar' style='float:left' />";
		$(this).parent().append(cancelar);
		var revisao = $("#revisao");
		revisao.children("[dir='titulo']").after($("#InformativoTitulo").val());
		revisao.children("[dir='mensagem']").after($("#InformativoMensagem").val());
		revisao.fadeIn(500);
	});

	$("#cancelar").live('click', function() {
		$(this).remove();
		$("#enviar").show();
		habilitaForm();
		$("#revisao").fadeOut(500);
	});
	
	$("#confirmar").live('click', function() {
		habilitaForm();
	});
	
	function exibeDestino(destinatario) {
		if(destinatario != "") {
			$('.informativos:not(.' + destinatario +')').hide();
			$('.' + destinatario).show();
		}
	}
	
	function desabilitaForm() {
		$('#enviar').attr('disabled','disabled');
		$("#InformativoTitulo").attr('disabled','disabled');
		$("#InformativoMensagem").attr('disabled','disabled');
		$("#InformativoDestinatario").attr('disabled','disabled');
	}
	function habilitaForm() {
		$('#enviar').removeAttr('disabled');
		$("#InformativoTitulo").removeAttr('disabled');
		$("#InformativoMensagem").removeAttr('disabled');
		$("#InformativoDestinatario").removeAttr('disabled');
	}
});
</script>
<style type='text/css'>
#revisao { float:left; width:100%; margin-top:10px; padding:0; display:none; font-size:14px; line-height:20px }
</style>
<span id="conteudo-titulo" class="grid_full box-com-titulo-header">Informativos</span>
<div id="conteudo-container">
	<?php
		$session->flash();
		echo $form->create('Informativo', array('url' => "/{$this->params['prefix']}/informativos/enviar"));
	?>
	<p class="grid_full alpha omega" style='padding-top:20px'>
		<?=$html->link('Visualizar informativos enviados',array($this->params['prefix'] => true, 'controller' => 'informativos', 'action' => 'enviados'),array('class' => ''));?>
		<label class="grid_full alpha omega" style='margin-top:10px'>T&iacute;tulo</label>
		<?php echo $form->input('titulo', array('class' => 'grid_6 alpha omega first', 'label' => false, 'div' => false,'error' => array('wrap' => 'span', 'class' => 'grid_4'))); ?>
	</p>
	<p class="grid_full alpha omega">
		<label class="grid_full alpha omega">Mensagem</label>
		<?php echo $form->input('mensagem', array('type' => 'textarea','escape' => false,'class' => 'grid_6 alpha omega first', 'label' => false, 'div' => false,'error' => array('wrap' => 'span', 'class' => 'grid_4'))); ?>
	</p>
	<p class="grid_full alpha omega">
		<label class="grid_2 alpha omega destinatarios" style="margin-bottom:10px">Destinatários</label>
		<?php if(isset($destinatarios)) { ?>
		<?=$form->input('destinatario', array('options' => $destinatarios, 'type' => 'select', 'class' => 'grid_4 alpha omega', 'label' => false, 'div' => false)); ?>
		<?php } ?>
	</p>
	<?php if(isset($grupos)) { ?>
	<p class="grid_full alpha omega grupo informativos">
		<label class="grid_2 alpha omega">Grupo</label>
		<?=$form->input('grupo', array('options' => $grupos, 'type' => 'select', 'class' => 'grid_4 alpha omega', 'label' => false, 'div' => false)); ?>
	</p>
	<?php } ?>
	<?php if(isset($turmas)) { ?>
	<p class="grid_full alpha omega turma informativos" style='display:none'>
		<label class="grid_2 alpha omega">Turma</label>
		<?=$form->input('turma', array('options' => $turmas, 'type' => 'select', 'class' => 'grid_4 alpha omega', 'label' => false, 'div' => false)); ?>
	</p>
	<?php } ?>
	<p class="grid_full alpha omega formando informativos" style='display:none'>
		<label class="grid_2 alpha omega">C&oacute;digo do formando</label>
		<?php echo $form->input('codigo_formando', array('class' => 'grid_3 alpha omega', 'label' => false, 'div' => false)); ?>
	</p>
	<p>
		<input type='button' class='submit' id='enviar' value='Enviar' style='float:left' />
	</p>
	<div id='revisao'>
		<h2>Revise o informativo antes de enviar</h2>
		<br />
		<strong dir="titulo">T&iacute;tulo: </strong><br />
		<strong dir='mensagem'>Mensagem: </strong>
		<br />
		<br />
		<?=$form->end(array('id' => 'confirmar','label' => 'Confirmar', 'div' => false, 'class' => 'submit'));?>
	</div>
</div>