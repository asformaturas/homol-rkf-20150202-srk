<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<h2><?php echo $turmaMapa['Evento']['nome'] ?> - Setor <span class="fg-color-red"><?php echo $setor ?></span></h2>

<?php if ($formandos_sem_setor): ?>
	<br/>
	<button type="button" class="adicionar-formando default bg-color-red">
	    Adicionar Formando
	</button>
<?php endif ?>
<?php if (count($formandos) > 0) : ?>
    <br/>
    <h3>Formandos</h3>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th class="input-small" scope="col">Código Formando</th>
                <th scope="col">Nome</th>
                <th scope="col">
                	<?php echo $form->create('FormandoMapaHorario',array(
				        'url' => "/{$this->params['prefix']}/mapa_mesas/setorizacao_remover_todos/{$evento_id}/".urlencode($setor),
				        'id' => 'form-filtro'
				    )) ?>
                	<button type="submit" class="remover-todos default mini bg-color-red">
                        Remover Todos
                    </button>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($formandos as $formando) : ?>
            <tr>
                <td><?=$formando['FormandoMapaHorario']['codigo_formando']; ?></td>
                <td><?=$formando['VwCodigoFormando']['nome']; ?></td>
                <td>
                	<button type="button" class="remover-setor default mini bg-color-red" formando="<?= $formando['FormandoMapaHorario']['codigo_formando'] ?>">
                        Remover
                    </button>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>

<script type="text/javascript">
	$(document).ready(function(){
		$("#form-filtro").submit(function() {
	        $("#form-filtro").find('[value="0"]').attr('disabled','disabled');
	    });

	    $(".remover-setor").click(function(event){
            event.preventDefault();
            var url = '/<?=$this->params['prefix']?>/mapa_mesas/setorizacao_remover_formando/<?= $evento_id ?>/'+$(this).attr('formando');
            $.ajax({
                url : url,
                type : "POST",
                dataType : "json",
                complete : function(data) {
                    bootbox.hideAll();
                    bootbox.dialog('Carregando',[{
		                label: 'Fechar'
		            }],{
		                remote: '/<?=$this->params['prefix']?>/mapa_mesas/visualizar_setor/<?= $evento_id ?>/<?= urlencode($setor) ?>'
		            });
                }
            });
        });

        $(".adicionar-formando").click(function(event){
            event.preventDefault();
            bootbox.hideAll();
        	bootbox.dialog('Carregando',[{
                label: 'Salvar',
                class: 'max bg-color-green',
                callback: function(){
                    $('#conteudo').html('<div class="row-fluid content-loader" id="content-loading"><div class="cycle-loader"></div></div>');
                    $('#form-filtro').submit();
                    return false;
                }
            },{
                label: 'Fechar'
            }],{
                remote: '/<?=$this->params['prefix']?>/mapa_mesas/setorizacao_adicionar_formandos/<?= $evento_id ?>/<?= urlencode($setor) ?>'
            });
        });
	});
</script>