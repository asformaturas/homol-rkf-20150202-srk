<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<h2><?php echo $turmaMapa['Evento']['nome'] ?> - Setor <span class="fg-color-red"><?php echo $setor ?></span></h2>

<?php if (count($qtd_formandos) > 0) : ?>
    <?php $totalMesas = 0; ?>
    <br/>
    <h3>Formandos Que Não Marcaram Mesa</h3>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th class="input-small" scope="col">Código Formando</th>
                <th scope="col">Nome</th>
                <th scope="col">Data Adesão</th>
                <th scope="col">Data Liberação</th>
                <th scope="col">Qtd Mesas</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($qtd_formandos as $formando) : ?>
            <?php $totalMesas += $formando['ViewFormandos']['quantidade_mesas']; ?>
            <tr>
                <td><?=$formando['FormandoMapaHorario']['codigo_formando']; ?></td>
                <td><?=$formando['ViewFormandos']['nome']; ?></td>
                <td><?= date('d/m/Y H:i:s', strtotime($formando['ViewFormandos']['data_adesao'])); ?></td>
                <td><?= date('d/m/Y H:i:s', strtotime($formando['FormandoMapaHorario']['data_liberacao'])); ?></td>
                <td><?=$formando['ViewFormandos']['quantidade_mesas']; ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <thead>
            <tr>
                <td colspan="4"></td>
                <td style="font-weight:bold"><?= $totalMesas ?></td>
            </tr>
        </thead>
    </table>
<?php endif; ?>