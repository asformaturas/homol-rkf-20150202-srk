<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/bootstrap/datepicker.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/bootstrap-timepicker.min.js"></script>
<script type="text/javascript">
     $(document).ready(function(){ 
        
        $('#duracao').timepicker({
            minuteStep: 15,
            showMeridian: false,
            defaultTime: false
        });
        
        $('#hora').timepicker({
            minuteStep: 15,
            showMeridian: false,
            defaultTime: false
        });
        
        $(".datepicker").datepicker();

    //plugin de ativacao do plugin
    $('.selectpicker').selectpicker({width:'100%'});
});
</script>

<div class="row-fluid alert">
    <?php $session->flash(); ?>
</div>

<div class="row-fluid">
    <h2>
        Agendamento de Escolha de Mesas
    </h2>
</div>
<br />

<?php echo $form->create('TurmaMapa', array(
    'url' => "/{$this->params['prefix']}/mapa_mesas/salvar_agendamento",
    'id' => 'form'
)); ?>

<div class="row-fluid">

    <?=$form->hidden('Evento.id', array(
        'value' => $eventoId,
        'div'   => 'input-control text')); ?>

</div>
<div class="row-fluid">
    <div class="span2">
            <label>Data início</label>
            <?php echo $form->input('TurmaMapa.data_inicio', array( 
                'class' => 'input-control text datepicker', 
                'label' => false, 
                'div'   => false,
                'error' => false, 
                'id'    => 'datepicker',
                'div'   => 'input-control text'
            )); ?>
    </div>

    <div class="span2">
            <label>Data finalização</label>
            <?php echo $form->input('TurmaMapa.data_finalizacao', array(
                'class' => 'input-control text datepicker', 
                'label' => false, 
                'div'   => false,
                'error' => false,
                'div'   => 'input-control text'
            )); ?>
    </div>
</div>

<div class="row-fluid">

    <div class="span2">
        <div class="input-append bootstrap-timepicker">
            <label>Início diário</label>
            <?php echo $form->text('TurmaMapa.hora_inicio', array(
                'class' => 'input-control text', 
                'label' => false, 
                'div' => false, 
                'error' => false, 
                'id' => 'duracao',
                'div' => 'input-control text',
                'value' => '10:00',
                'required' => true
            )); ?>
        </div>
    </div>

     <div class="span2">
        <div class="input-append bootstrap-timepicker">
            <label>Finalização diária</label>
            <?php echo $form->text('TurmaMapa.hora_finalizacao', array(
                'class' => 'input-control text', 
                'label' => false, 
                'div' => false, 
                'error' => false, 
                'id' => 'duracao',
                'div' => 'input-control text',
                'value' => '18:00'
            )); ?>
        </div>
    </div>

    <div class="span2">
        <?=$form->input('TurmaMapa.intervalo', array(
            'options' => $intervalos,
            'type' => 'select',
            'class' => 'selectpicker',
            'label' => 'Liberação a cada',
            'div' => 'input-control text')); ?>
    </div>

</div>

<?php echo $form->end(array(
    'label' => 'Criar agendamento', 
    'div' => false, 
    'class' => 
    'button max bg-color-greenDark')
    );
?>
