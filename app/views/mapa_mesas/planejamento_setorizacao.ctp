<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<h2><?php echo $turmaMapa['Evento']['nome'] ?> - Setores</h2>

<?php if (count($setores) > 0) : ?>
    <?php 
        $totalMesas = 0;
        $totalFormandosNaoMarcaram = 0;
    ?>
    <br/>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th class="input-medium" scope="col">Nome</th>
                <th scope="col">Quantidade Formandos</th>
                <th scope="col">Quantidade Mesas</th>
                <th scope="col">Quantidade Formandos Não Marcaram Mesa</th>
                <th class="input-mini" scope="col"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($setores as $setor) : ?>
            <?php
                $totalMesas += $setor['qtd_mesas'];
                $totalFormandosNaoMarcaram += ($setor['qtd_formandos'] - $setor['qtd_formandos_marcados']);
            ?>
            <tr>
                <td><?=$setor['nome']; ?></td>
                <td><?=$setor['qtd_formandos']; ?></td>
                <td><?=$setor['qtd_mesas']; ?></td>
                <td>
                    <?= ($setor['qtd_formandos'] - $setor['qtd_formandos_marcados']); ?>
                    <button type="button" class="ver-formandos-nao-marcados default mini bg-color-orangeDark" style="float:right;" setor="<?= urlencode($setor['nome']) ?>">
                        Formandos
                    </button>
                </td>
                <td>
                    <button type="button" class="ver-setor default mini bg-color-blueDark" setor="<?= urlencode($setor['nome']) ?>">
                        Visualizar
                    </button>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <thead>
            <tr>
                <td colspan="2"></td>
                <td style="font-weight:bold"><?= $totalMesas ?></td>
                <td style="font-weight:bold"><?= $totalFormandosNaoMarcaram ?></td>
                <td></td>
            </tr>
        </thead>
    </table>
<?php else : ?>
    <h2 class="fg-color-red">Nenhum Setor Encontrado</h2>
<?php endif; ?>

<?php if (count($formandos_sem_setor) > 0) : ?>
    <br/>
    <h3>Formandos Sem Setor</h3>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th class="input-small" scope="col">Código Formando</th>
                <th scope="col">Nome</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($formandos_sem_setor as $formando) : ?>
            <tr>
                <td><?=$formando['FormandoMapaHorario']['codigo_formando']; ?></td>
                <td><?=$formando['VwCodigoFormando']['nome']; ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>

<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/alert.js?v=0.1"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/tooltip.js?v=0.1"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/modal.js?v=0.1"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/popover.js?v=0.1"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/tab.js?v=0.1"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/bootbox.js?v=0.2"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".ver-setor").click(function(event){
            event.preventDefault();
            bootbox.dialog('Carregando',[{
                label: 'Fechar'
            }],{
                remote: '/<?=$this->params['prefix']?>/mapa_mesas/visualizar_setor/<?= $evento_id ?>/'+$(this).attr('setor')
            });
        });

        $(".ver-formandos-nao-marcados").click(function(event){
            event.preventDefault();
            bootbox.dialog('Carregando',[{
                label: 'Fechar'
            }],{
                remote: '/<?=$this->params['prefix']?>/mapa_mesas/visualizar_formandos_nao_marcados/<?= $evento_id ?>/'+$(this).attr('setor')
            });
        });
    });
</script>