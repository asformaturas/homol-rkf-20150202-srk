<div class="span9 metro" style="position:relative" id="content-container">
    <div class="row-fluid content-loader hide" id="content-loading">
        <div class="cycle-loader"></div>
    </div>
    <div class="row-fluid content-loader hide" id="content-body"></div>
    <div class="row-fluid content-loader" id="content-home">
        <h2><?php echo $evento['Evento']['nome'] ?></h2>

        <?php if(!$agendamentos) : ?>
            <a href="<?php echo $this->webroot . $this->params['prefix'] ?>/mapa_mesas/agendamento/<?php echo $evento_id ?>" >
            <div class="tile icon bg-color-green"
                 data-bind="click: function(data, event) {
                 page('<?= $this->webroot . $this->params['prefix'] ?>/mapa_mesas/agendamento/<?php echo $evento_id ?>') }">
                <div class="tile-content">
                    <span class="img icon-calendar"></span>
                </div>
                <div class="brand"><span class="name">Agendamento Escolha</span></div>
            </div>
            </a>
        <?php else: ?>
            <a href="<?php echo $this->webroot . $this->params['prefix'] ?>/mapa_mesas/setorizacao/<?php echo $evento_id ?>" >
            <div class="tile icon bg-color-orange"
                 data-bind="click: function(data, event) {
                 page('<?= $this->webroot . $this->params['prefix'] ?>/mapa_mesas/setorizacao/<?php echo $evento_id ?>') }">
                <div class="tile-content">
                    <span class="img icon-globe"></span>
                </div>
                <div class="brand"><span class="name">Setorização</span></div>
            </div>
            </a>
            <a href="<?php echo $this->webroot . $this->params['prefix'] ?>/mapa_mesas/relatorio_agendados/<?php echo $evento_id ?>" >
            <div class="tile icon bg-color-brown"
                 data-bind="click: function(data, event) {
                 page('<?= $this->webroot . $this->params['prefix'] ?>/mapa_mesas/relatorio_agendados/<?php echo $evento_id ?>') }">
                <div class="tile-content">
                    <span class="img icon-key"></span>
                </div>
                <div class="brand"><span class="name">Relatório</span></div>
            </div>
            </a>
        <?php endif; ?>

        <a id="consulta_mapa" href="<?php echo $this->webroot . $this->params['prefix'] ?>/eventos/mapa/<?php echo $evento_id ?>" >
            <div class="tile icon bg-color-purple">
                <div class="tile-content">
                    <span class="img icon-map"></span>
                </div>
                <div class="brand"><span class="name">Consulta Mapa de Mesa</span></div>
            </div>
        </a>

        <!--<div class="tile icon bg-color-blue"
             data-bind="click: function(data, event) {
             page('<?= $this->webroot . $this->params['prefix'] ?>/checkout/listar') }">
            <div class="tile-content">
                <span class="img icon-drawer-2"></span>
            </div>
            <div class="brand"><span class="name">Relatório</span></div>
        </div> -->

    </div>
</div>

<script type="text/javascript">

$(document).ready(function(){
    $("#consulta_mapa").click(function(event){
        event.preventDefault();
        var url = $(this).attr('href');
        $.ajax ({
            //prefixo, controller, action e parametro
            url : url,
            data : {},
            type : "POST",
            complete: function(response) {
                $('#conteudo').html(response.responseText);
            }
        });
    });
});
</script>