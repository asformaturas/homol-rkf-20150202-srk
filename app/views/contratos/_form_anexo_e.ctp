<?php
//Faculdade
//Cursos
//Previsão Conclusão
?>

<p class="grid_11 alpha omega">
	<label class="grid_11 alpha">Faculdade</label>
	<?php echo $form->input('faculdade', array('class' => 'grid_11 alpha first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_11'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_11 alpha">Cursos</label>
	<?php echo $form->textarea('cursos', array('rows' => 10,'class' => 'grid_11 alpha first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_11'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_11 alpha">Previsão de Conclusão</label>
	<?php echo $form->input('previsao_conclusao', array('class' => 'grid_11 alpha first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_11'))); ?>
</p>
