<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Anexos da Turma
<?php if($this->params['prefix'] == 'planejamento'){ ?>
        <a class='button mini bg-color-gray pull-right' data-bind="click: function() { 
            page('<?="/{$this->params['prefix']}/contratos/inserir"?>') }">
            Adicionar Anexo
            <i class='icon-paper-2'></i>
        </a>
<?php } ?>
        </h2>
    </div>
</div>
<?php $session->flash(); ?>
<div class="row-fluid">
    <span class="label label-info pointer">
        <h2><?=$nomeContrato;?></h2>
    </span>
    <br />
    <br />
    <table class="table">
        <tbody>
        <?php foreach(array_keys($tipos_contratos) as $keytipo) : ?>
        <?php $tipo = $tipos_contratos[$keytipo]; ?>
            <tr class="header">
                <td colspan="5"><h4 class="fg-color-white"><?=$tipo['TiposContratoAnexos']['nome'];?></h4></td>
            </tr>
            <?php if(array_key_exists($tipo['TiposContratoAnexos']['nome_anexo'], $contratos)) : ?>
            <?php foreach($contratos[$tipo['TiposContratoAnexos']['nome_anexo']] as $contrato) { ?>
            <tr>
                <td>
                    <?=$contrato['Usuario']['nome']?> / <?=$contrato['Usuario']['grupo']?>
                </td>
                <td><?=$contrato['Contrato']['nome']?></td>
                <td><?=date('d/m/Y',strtotime($contrato['Contrato']['criado']))?></td>
                <td><?=date('H:i',strtotime($contrato['Contrato']['criado']))?></td>
                <td>
                    <?=$html->link('Baixar', array(
                        $this->params['prefix'] => true,
                        'controller' => 'Contratos',
                        'action' => 'baixar', $contrato['Contrato']['id']
                    ), array('class' => 'button mini')) ?>
                </td>
            </tr>
            <?php } ?>
            <?php else : ?>
            <tr>
                <td colspan="5"><i>Anexo não cadastrado.</i></td>
            </tr>
            <?php endif; ?>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>