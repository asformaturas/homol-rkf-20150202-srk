<?php
/* 
 * Dados recebidos do controller são:
 * $parametros['faculdade']
 * $parametros['curso']
 * $parametros['numero_aderentes']
 * $turma['data_conclusao']
 * $turma['valor_formando']
 * $turma['data_assinatura_contrato']
 * $turma['beneficios_comissao']
 * $turma['negociacoes_contratuais']
 * $turma['valor_total_contrato']
 */

App::import('Vendor','xtcpdf');  
$tcpdf = new XTCPDF();
$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans' 

$tcpdf->SetAuthor("RK Formaturas http://rkformaturas.com.br/"); 

$textfont = 'helvetica';
 
$tcpdf->SetAuthor("RK Formaturas");
 
$tcpdf->setPrintHeader(false);
$tcpdf->setPrintFooter(false);
$tcpdf->SetMargins(30, 40, 30);
$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'',9);
 
$tcpdf->AddPage();
 
// create some HTML content
$htmlcontent = 

"
<p><p><b>ANEXO E – EXCLUSIVIDADE FOTOGRÁFICA</b></p>

<p>Faculdade: <b>".$this->data['Contrato']['faculdade']."</b> </p>

<p>Curso: <b>".$this->data['Contrato']['cursos']."</b></p>

<p>Conclusão: <b>".$this->data['Contrato']['previsao_conclusao']."</b></p>

<p>1. As fotografias que comporão os álbuns serão coloridas, no tamanho 24 x 30 cm, com padrão de qualidade compatível com o mercado fotográfico, ilustrando todos os eventos extra oficiais solicitados pela Comissão de formatura, além da Colação de Grau e Baile de Gala. Para garantir a máxima qualidade, todas as fotos serão capturadas digitalmente e ampliadas em Mini Labs Digitais.</p>

<p>2. As fitas de vídeo serão editadas no sistema MDV (Mini DV) em uma ESTAÇÃO DIGITAL. Todas as imagens serão captadas através de câmeras digitais Mini DV. Quando o formando optar por DVD não há necessidade de transcodificação uma vez que a fonte e o destino são digitais. Quando o formando optar por VHS, as cópias serão transcodificadas para o sistema VHS (compatível com o vídeo doméstico) e personalizadas para cada formando. A captação em Mini DV garante a máxima qualidade de imagem e som, sendo a captura 100% digital.</p>

<p>3. Os profissionais destacados para os eventos Extra Oficiais estarão vestidos com calça social preta, sapato social preto, camisa social branca e para os eventos oficiais, estarão em traje social completo. Em todos os eventos os profissionais estarão ainda identificados de crachás ÁS EVENTOS.</p>

<p>4. Serão destacados para o baile de formatura no mínimo um fotógrafo para cada doze alunos e um cinegrafista para cada vinte e cinco alunos.</p>

<p>5. Os preços das fotografias, fitas de vídeo e DVD serão os mesmos adotados pela tabela da SEAFESP (Sindicato das Empresas de Formaturas do Estado de São Paulo) vigente à época, sobre o qual forneceremos desconto especial de 30% sobre o valor de tabela. Ressaltando que os alunos não serão obrigados a adquirir fotos e fitas, no entanto, caso optem por adquiri-las poderão descartar até 30% das fotos. Os álbuns serão entregues montados, sendo facultada ao formando a opção de adquiri-lo ou não<i>.</i></p>

<p>6. O prazo de entrega do material fotográfico dar-se-á no máximo de 120 dias úteis após a realização do Baile de Gala. A visita para apresentação do material será em local e horário a critério do formando através de agendamento prévio. A forma de pagamento que estará sujeito o formando respeitará todos os critérios ora citados, podendo a empresa criar meios complementares e facilitadores para aquisição dos produtos mencionados. Assim, o álbum poderá ser parcelado em até 12 vezes.</p>

<p>7. Na festa de formatura será permitido o uso de máquinas fotográficas <b>não profissionais </b>após a valsa de formatura. Na colação de grau não será permitido o uso de máquina fotográfica nem filmadora. O uso de máquinas fotográficas profissionais e filmadoras fica extremamente proibido em qualquer evento.</p>

<p>8. O formando não tem nenhuma obrigação na aquisição de fotografias e filmagem. No caso da compra das fotos, é permitido um descarte máximo de 30% de fotos do total do álbum de formatura.</p>

<p>9. A RK Formaturas é detentora da exclusividade vídeo-fotográfica dos eventos relativos às festividades de formatura, oferecendo descontos e eventos extras conforme consta no Anexo A e eventualmente no Anexo B, sempre proporcional ao número de formandos identificados e que estejam aderidos ao baile de formatura.</p>

<p>São Paulo,______de______________de 20______</p>

<p>__________________________ __________________________</p>

<p>CONTRATADO CONTRATANTE</p>

</p>

";

// output the HTML content
$tcpdf->writeHTML($htmlcontent, true, 0, true, 0, 'J');

echo $tcpdf->Output(date('Y-m-d H:i:s').' - Anexo E.pdf', 'D'); 
?>