<span id="conteudo-titulo" class="box-com-titulo-header">Contratos</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	
	<div class="detalhes">
	<?php //print_r($contratos['anexo_b_fechamento']); ?>
	<?php foreach(array_keys($tipos_contratos) as $keytipo):?>
		<?php $tipo = $tipos_contratos[$keytipo]; ?>
        <p class="grid_full alpha omega">
            <label class="grid_8 alpha contratos_tipo"><?php echo $tipo; ?></label>
			<?php 
			if(!empty($contratos[$keytipo])) {
				foreach($contratos[$keytipo] as $contrato):?>
				<span class="grid_full alpha first">
					<?php 
						echo $contrato['Contrato']['nome'] . ' - ' . 
						date_format( $datas->create_date_time_from_format('Y-m-d H:i:s', $contrato['Contrato']['criado']),'d/m/Y H:i:s') . ' - ' . $html->link('Visualizar',array($this->params['prefix'] => true, 'action' => 'visualizar', $contrato['Contrato']['id'])); ?>
				</span></p>
				<?php endforeach;
			} else {
				?>
				<span class="grid_8 alpha first"><i>Nenhum contrato encontrado</i></span></p>
				<?php 
			} ?>
        </p>
	<?php endforeach;?>
    </div>


</div>