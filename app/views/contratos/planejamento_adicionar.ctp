<span id="conteudo-titulo" class="box-com-titulo-header">Arquivos - Adicionar</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<?php echo $form->create('Contrato', array('url' => "/{$this->params['prefix']}/Contratos/adicionar", 'type' => 'file')); ?>
	<p class="grid_11 alpha omega">
		<label class="grid_11 alpha omega">Tipo de Contrato</label>
		<?php echo $form->select('tipo', $tipos_contratos, array('selected' => $this->data['Contrato']['tipo'] ), array('empty' => null, 'class' => 'grid_4 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_5'))); ?>
	</p>
	
	<p class="grid_11 alpha omega">
		<label class="grid_11 alpha omega">Arquivo</label>
		<?php echo $form->file('arquivo') ?>
	</p>
	
	<p class="grid_11 alpha omega">
		<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'controller' => 'contratos', 'action' => 'index') ,array('class' => 'cancel')); ?>
		<?php echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'submit'));?>
	</p>
</div>