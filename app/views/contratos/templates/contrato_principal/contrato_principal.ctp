<?php if($out == "html") : ?>
<?php include('salvar_contrato.ctp')?>
<?php endif; ?>
<?php 
setlocale( LC_ALL, 'pt_BR', 'pt_BR.iso-8859-1', 'pt_BR.utf-8', 'portuguese'); 
$mes = strftime("%B");
?>
<h4 style="text-align: center"><b>Condições Contratuais</b></h4>
<br />
<p style="text-align: justify">
Pelo presente instrumento particular, de um lado:
<br />
<br />
Adere ao Contrato de Prestação de Serviços, Representação Financeira e de Fotos firmado entre a Comissão de Formatura de seu curso e a empresa <b>RK EVENTOS E FORMATURAS LTDA.</b>, inscrita no CNPJ/MF sob N.º09.551.738/0001-11, com sede na Rua Caramuru, 1159 - SAUDE, São Paulo/SP - CEP 04138-002, representada neste ato pelo sócio-diretor RAFAEL KEINER, portador do RG Nº 47.072.806-1, denominado <b>CONTRATO COLETIVO</b> e regido nos seguintes moldes:
<br />
<br />
<b>1.</b> O formando acima qualificado reconhece a <b>Comissão de Formatura</b> de sua turma como legítima representante para tratar dos assuntos relacionados à sua Formatura, outorgando poderes para representá-lo em atos referentes à sua formatura, outorgando poderes para a Comissão celebrar aditivos contratuais que se fizerem necessários à realização dos eventos. 
<br />
<br />
<b>2.</b> As informações importantes a respeito do seu contrato estarão disponibilizadas no link: <b>http://sistema.rkformaturas.com.br/usuarios/login</b>. Caso precise do login e senha da sua turma, entre em contato com a sua comissão ou diretamente com a RK Formaturas. 
<br />
<br />
<b>3.</b> A comissão de formatura não poderá tratar assuntos de cunho financeiro, tais como forma de pagamento, renegociação de dívida ou cancelamento de contrato em nome do formando.
<br />
<br />
<b>4</b>. O formando acima qualificado autoriza a <b>RK Formaturas</b> a representá-lo na contratação e pagamento de fornecedores. O valor pago pelo <b>CONTRATANTE</b> permanecerá em posse da <b>CONTRATADA</b> de forma transitória, até o repasse aos fornecedores do pacote de formatura do quais a <b>CONTRATADA</b> é intermediária, ficando tão-somente com a comissão que lhe faz jus.
<br />
<br />
<b>5.</b> O(A) <b>CONTRATANTE</b> se obriga a pagar a <b>CONTRATADA</b> pelos eventos de formatura descritos no <b>CONTRATO COLETIVO</b>.
<br />
<br />
<b>6.</b> No momento da retirada dos convites o formando que optar por boletos bancários deverá quitar as parcelas em atraso e trocar os boletos ainda em aberto por cheques pré-datados. No mesmo momento, o formando que tiver optado por cheques pré-datados e ainda não tiver os entregue, deverá entregar os mesmos. 
<br />
<br />
<b>7.</b> Os valores consignados neste contrato serão atualizados de 12 em 12 meses, a partir da assinatura do Contrato de Prestação de Serviços celebrado entre a <b>Comissão de Formatura</b> e a <b>RK EVENTOS E FORMATURAS LTDA.</b>, adotando - se a variação do IGPM/FGV do período, conforme autoriza a Lei 9.069/95, ou por outro índice que venha a substituí-lo.
<br />
<br />
<b>8.</b> O atraso no pagamento de qualquer parcela acarretará multa de 5% (cinco por cento) acrescida de mora diária de 0,033% sobre o valor.
<br />
<br />
<b>9.</b> Os convites só serão entregues após feito o acerto financeiro, isto é, pendências devidamente quitadas, isto inclui os valores relativos ao IGPM, cheques trocados e entregues , conforme explicitado nos itens 6 e 7.
<br />
<br />
<b>10.</b> A Rescisão poderá ser promovida pelo <b>CONTRATANTE</b>, com aplicação de multa e ressarcimento nos termos da tabela abaixo:
<br />
<p style="margin-left: 30px">
<b>(a)</b> Do ato de fechamento a 120 dias de antecedência: A <b>CONTRATADA</b> reterá o valor proporcional correspondente aos valores já desembolsados com contratação de produtos e brindes até a data da comunicação da Rescisão, acrescido de multa de 30% sobre o valor do contrato.
<br />
<br />
<b>(b)</b> De 119 a 90 dias de antecedência: A <b>CONTRATADA</b> reterá o valor proporcional correspondente aos valores já desembolsados com contratação de produtos e brindes até a data da comunicação da Rescisão, acrescido de multa de 40% sobre o valor do contrato.
<br />
<br />
<b>(c)</b> De 89 a 75 dias de antecedência: A <b>CONTRATADA</b> reterá o valor proporcional correspondente aos valores já desembolsados com contratação de produtos e brindes até a data da comunicação da Rescisão, acrescido de multa de 50% sobre o valor do contrato.
<br />
<br />
<b>(d)</b> De 74 dias até a data do evento: A <b>CONTRATADA</b> reterá o valor integral do contrato.
<br />
<br />
<b>(e)</b> No caso de reprovação comprovada através de documento da instituição de ensino, e o formando não pretender realizar a festa, poderá solicitar o reembolso de 50% do valor total pago até 15 dias antes do evento. Após esse periodo a <b>CONTRATADA</b> reterá o valor integral do contrato.
</p>
<b>11.</b> Constitui obrigação do(a) <b>CONTRATANTE</b> manter seu endereço e demais dados cadastrais atualizados, bem como a responsabilidade pela grafia correta de seu nome, nos textos originais que deverão ser entregues à <b>CONTRATADA</b> para a execução dos serviços gráficos, eximindo a <b>CONTRATADA</b> pela falta do nome e/ou grafia incorreta.
<br />
<br />
<b>12.</b> A empresa não se responsabiliza por objetos pessoais em todos os eventos organizados pela empresa.
<br />
<br />
<b>13.</b> Em caso de CASO FORTUITO OU DE FORÇA MAIOR, o CONTRATANTE, autoriza o CONTRATADO a substituir produtos, serviços ou até mesmo o espaço, por outro(s) de igual ou melhor qualidade.
<br />
<br />
<b>14.</b> O (A) CONTRATANTE tem ciência da lei estadual número 14.592 de 19 de outubro de 2011, <b>É proibida a venda, oferta, fornecimento, entrega e permissão do consumo de bebida alcóolica, ainda que gratuitamente, aos menores de 18 anos de idade</b>.
<br />
<p style="margin-left: 30px">
<b>14.1.</b> Todo e qualquer menor que for convidado pelo CONTRATANTE, que flagrado ingerindo bebida alcoólica, ou tendo feito uso de bebida alcoólica no evento, é de TOTAL responsabilidade do CONTRATANTE, eximindo o CONTRATADO, de qualquer multa aplicada pela Lei acima descrita, e trazendo para si todo o ônus que poderá acarretar essa multa, como valores a pagar, honorários advocatícios, entre outros pelo descumprimento da lei.
</p>
<b>15.</b> Fica eleito o Foro da Capital do Estado de São Paulo por mais privilegiado que outro seja, arcando a parte vencida com o ônus da sucumbência, inclusive honorários advocatícios de 20% do total da condenação.
<br />
<br />
A RK Formaturas não cobra taxa de emissão de boletos. Para imprimi-los, acesse o espaço do formando com o seu login e senha e tenha acesso à todos os seus boletos na Área Financeira.
<br />
<br />
Caso opte por pagar em cheque, lembramos que é sua responsabilidade a entrega dos cheques para o representante da RK ou comissão de formatura no colégio ou diretamente em nossa sede. Sempre exija o seu recibo.
<br />
<br />
Lembramos que caso opte em pagar em cheque mas decida posteriormente pagar em boletos, não há problemas, basta acessar sua área financeira e imprimir os boletos.
<br />
<br />
Além disso, a RK disponibiliza um serviço adicional de envio dos boletos impressos. Caso queira recebê-los em casa, opte por boletos impressos.
<br />
<br />
Será cobrado o valor de R$ 45,00 referente ao envio dos mesmos. Lembramos que essa taxa não é obrigatória e você facilmente pode acessar todos os seus boletos online. Além disso, caso opte em receber os boletos na sua casa, você poderá também entrar no site e imprimir uma segunda via a qualquer momento.
</p>
<br />
<br />
<h5 style="color: black">São Paulo, <?=date('d')?> de <?=ucfirst($mes)?> de <?=date('Y')?></h5><br /><br />
<?php //foreach($turma['Turma']['comissao'] as $comissao) : ?>
<!-- <p style="line-height: 1.3em">
<b>Nome: </b><?=key($comissao)?>
<br />
<b>RG: </b><?=$comissao[key($comissao)]?>
<br />
<br />
<?=str_pad("",60,"_")?>
<br />
ASSINATURA
<br />
</p> -->
<?php //endforeach; ?>
