<?php if($out == "html") : ?>
<?php include('salvar_contrato.ctp')?>
<?php endif;?>
<h4>ANEXO B - BENEFICIOS COMISSÃO</h4>
<p style="font-size: 1.05em; line-height: 1.8em">
<?=str_pad("Faculdade:", 70, "&nbsp;")?><?=$turma['Turma']['nome']?>
<br />
<?=str_pad("Curso:", 108, "&nbsp;")?><?=$turma['Turma']['cursos']?>
<br />
<?=str_pad("Conclusão:", 71, "&nbsp;")?><?=$turma['Turma']['ano_formatura']?>.<?=$turma['Turma']['semestre_formatura']?>
</p>
<p>
Fica estabelecido os seguintes benefícios para a comissão: 
<br />
<br />
</p>
<?php foreach($turma['Turma']['beneficios_comissao'] as $indice => $beneficio) : ?>
<p><b><?="{$indice}) ". nl2br($beneficio)?></b></p>
<?php endforeach; ?>
<br />
<br />
<?php foreach($turma['Turma']['comissao'] as $comissao) : ?>
<p style="line-height: 1.3em">
<br />
<b>Nome: </b><?=key($comissao)?>
<br />
<b>RG: </b><?=$comissao[key($comissao)]?>
<br />
<br />
<?=str_pad("",60,"_")?>
<br />
ASSINATURA
<br />
</p>
<?php endforeach; ?>