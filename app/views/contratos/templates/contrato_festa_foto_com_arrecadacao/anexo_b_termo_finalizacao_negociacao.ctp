<?php if($out == "html") : ?>
<?php include('salvar_contrato.ctp')?>
<?php endif;?>
<h4>ANEXO B - TERMO DE FINALIZAÇÃO DE NEGOCIAÇÃO</h4>
<p style="font-size: 1.05em; line-height: 1.8em">
<?=str_pad("Faculdade:", 70, "&nbsp;")?><?=$turma['Turma']['nome']?>
<br />
<?=str_pad("Curso:", 108, "&nbsp;")?><?=$turma['Turma']['cursos']?>
<br />
<?=str_pad("Conclusão:", 71, "&nbsp;")?><?=$turma['Turma']['ano_formatura']?>.<?=$turma['Turma']['semestre_formatura']?>
</p>
<p>
Na qualidade de <b>Contratantes</b>: Os formandos do(s) curso(s) de <b><?=$turma['Turma']['cursos']?></b>,
da <b><?=$turma['Turma']['nome']?></b>, com previsão de formatura para o <b><?=($turma['Turma']['semestre_formatura'] == 1) ? 'primeiro' : 'segundo';?> semestre de <?=$turma['Turma']['ano_formatura']?></b>, neste
ato devidamente representados pela <b>Comissão de Formatura</b> definem:
<br/> 
<br/> 
Todas as negociações extracontratuais estabelecidas entre <b>A CONTRATADA</b> e
<b>A CONTRATANTE</b> deverão ser preenchidas nesse documento. Não serão consideradas
informações anotadas a caneta, ou seja, todas as informações deverão ser
preenchidas e impressas de forma uniforme. 
<br />
<br />
</p>
<?php foreach($turma['Turma']['beneficios_formandos'] as $indice => $beneficio) : ?>
<p><b><?="{$indice}) ". nl2br($beneficio)?></b></p>
<?php endforeach; ?>
<br />
<br />
<?php foreach($turma['Turma']['comissao'] as $comissao) : ?>
<p style="line-height: 1.3em">
<br />
<b>Nome: </b><?=key($comissao)?>
<br />
<b>RG: </b><?=$comissao[key($comissao)]?>
<br />
<br />
<?=str_pad("",60,"_")?>
<br />
ASSINATURA
<br />
</p>
<?php endforeach; ?>