<?php if($out == "html") : ?>
<?php include('salvar_contrato.ctp')?>
<?php endif; ?>
<h4>ANEXO G - VALORES ARRECADADOS E À ARRECADAR</h4>
<p style="font-size: 1.05em; line-height: 1.8em">
<?=str_pad("Faculdade:", 70, "&nbsp;")?><?=$turma['Turma']['nome']?>
<br />
<?=str_pad("Curso:", 108, "&nbsp;")?><?=$turma['Turma']['cursos']?>
<br />
<?=str_pad("Conclusão:", 71, "&nbsp;")?><?=$turma['Turma']['ano_formatura']?>.<?=$turma['Turma']['semestre_formatura']?>
</p>
<p style="text-align: justify">
Fica estipulado que a comissão de formatura realizará os pagamentos conforme cronograma abaixo:
<br/><br/><br/><br/>
VALORES ARRECADADOS PELA COMISSÃO: <b>R$ <?=number_format($turma['Turma']['valor_arrecadado_comissao'], 2, ',', '.')?></b>
<br/><br/>
VALORES ARRECADAR PELOS FORMANDOS: <b>R$ <?=number_format($turma['Turma']['valor_arrecadar_formandos'], 2, ',', '.')?></b>
<br/><br/><br/><br/>
E abaixo a tabela que contempla a proporcionalidade paga por cada formando, bem como saldo devedor a ser cobrado pela ÁS 
(caso exista valor a ser cobrado).<br/><br/>
</p>
<p></p>
<p></p>
<?php foreach($turma['Turma']['comissao'] as $comissao) : ?>
<p style="line-height: 1.3em">
<br />
<b>Nome: </b><?=key($comissao)?>
<br />
<b>RG: </b><?=$comissao[key($comissao)]?>
<br />
<br />
<?=str_pad("",60,"_")?>
<br />
ASSINATURA
<br />
</p>
<?php endforeach; ?>
