<?php if($out == "html") : ?>
<?php include('salvar_contrato.ctp')?>
<?php endif; ?>
<h4>ANEXO E – TERMO DE COMERCIALIZAÇÃO DE ALBUNS</h4>
<p style="font-size: 1.05em; line-height: 1.8em">
<?=str_pad("Faculdade:", 70, "&nbsp;")?><?=$turma['Turma']['nome']?>
<br />
<?=str_pad("Curso:", 108, "&nbsp;")?><?=$turma['Turma']['cursos']?>
<br />
<?=str_pad("Conclusão:", 71, "&nbsp;")?><?=$turma['Turma']['ano_formatura']?>.<?=$turma['Turma']['semestre_formatura']?>
</p>
Na qualidade de <b>Contratantes</b>: Os formandos do(s) curso(s) de <b><?=$turma['Turma']['cursos']?></b>,
da <b><?=$turma['Turma']['nome']?></b>, com previsão de formatura para o <b><?=($turma['Turma']['semestre_formatura'] == 1) ? 'primeiro' : 'segundo';?> 
    semestre de <?=$turma['Turma']['ano_formatura']?></b>, neste ato devidamente representados pela <b>Comissão de Formatura</b> definem:<br/><br/> 
<b>1.</b> As fotografias que comporão os álbuns serão coloridas, no tamanho 30 x 40 cm, com padrão de qualidade compatível
com o mercado fotográfico, ilustrando todos os eventos extraoficiais solicitados pela Comissão de formatura, além da 
Colação de Grau e Baile de Gala.<br /><br />
<b>2.</b> Os materiais de vídeo serão editados em uma ESTAÇÃO DIGITAL onde todas as imagens serão captadas através de 
câmeras digitais.<br /><br />
<b>3.</b> Os preços das fotografias terão como base o valor de R$ 26,50, sendo esse preço referência em Janeiro de 2015, sendo que será
corrigido pelo IGPM no momento da venda do álbum ao formando (a correção do IGPM será o índice acumulado entre 01/01/2015 e a 
data da venda do álbum). Serão dadas diversas opções de parcelamento do álbum, negociadas diretamente entre formando e vendedor, 
sendo que os preços das fotos serão ajustados para absorver o financiamento.<br/><br/>
<b>4.</b>Serão dadas diversas opções de parcelamento do álbum, negociadas diretamente entre formando e vendedor, 
sendo que os preços das fotos serão ajustados para absorver o financiamento. <br /><br />
<b>5.</b> O prazo de entrega do material fotográfico dar-se-á no máximo de 180 dias úteis após a realização do Baile de Gala.
A visita para apresentação do material será em local e horário a critério do formando através de agendamento prévio. 
A forma de pagamento que estará sujeito o formando respeitará todos os critérios ora citados, podendo a empresa criar meios 
complementares e facilitadores para aquisição dos produtos mencionados. Assim, o álbum poderá ser parcelado em até 12 vezes.<br /><br />
<b>6.</b> Na festa de formatura será permitido o uso de máquinas fotográficas não profissionais após a valsa de formatura. 
Na colação de grau não será permitido o uso de máquina fotográfica nem filmadora. O uso de máquinas fotográficas profissionais 
e filmadoras ficam extremamente proibidos em qualquer evento.<br /><br />
<b>7.</b> O formando não tem nenhuma obrigação na aquisição de fotografias e filmagem. No caso da compra das fotos, é 
permitido um descarte máximo de 30% de fotos do total do álbum de formatura.<br /><br />
<b>8.</b> Informações adicionais foram definidas em comum acordo entre a AS e a comissão de formatura: <br /><br />
<?=nl2br($turma['Turma']['informacoes_adicionais_anexo_e'])?>