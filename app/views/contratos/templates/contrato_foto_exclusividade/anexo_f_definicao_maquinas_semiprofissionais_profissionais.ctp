<?php if($out == "html") : ?>
<?php include('salvar_contrato.ctp')?>
<?php endif; ?>
<h4>ANEXO F – DEFINIÇÃO DE MÁQUINAS FOTOGRÁFICAS PROFISSIONAIS E SEMI-PROFISSIONAIS</h4>
<p style="font-size: 1.05em; line-height: 1.8em">
<?=str_pad("Faculdade:", 70, "&nbsp;")?><?=$turma['Turma']['nome']?>
<br />
<?=str_pad("Curso:", 108, "&nbsp;")?><?=$turma['Turma']['cursos']?>
<br />
<?=str_pad("Conclusão:", 71, "&nbsp;")?><?=$turma['Turma']['ano_formatura']?>.<?=$turma['Turma']['semestre_formatura']?>
</p>
Na qualidade de <b>Contratantes</b>: Os formandos do(s) curso(s) de <b><?=$turma['Turma']['cursos']?></b>,
da <b><?=$turma['Turma']['nome']?></b>, com previsão de formatura para o <b><?=($turma['Turma']['semestre_formatura'] == 1) ? 'primeiro' : 'segundo';?> 
semestre de <?=$turma['Turma']['ano_formatura']?></b>, neste ato devidamente representados pela <b>Comissão de Formatura</b> definem: <br/><br/>
A Exclusividade Fotográfica é o vínculo feito com a Comissão de Formatura em contrato que garante apenas a presença e atuação
profissional (fotográfica e filmagem) da SADER & AVILA.<br />
Isto é, É PROIBIDA A ENTRADA DE CÂMERAS PROFISSIONAIS E SEMIPROFISSIONAIS NO EVENTO, as quais flash e/ou lentes sejam intercambiáveis.<br /><br />
É permitida somente a entrada de câmeras compactas.