<?php if($out == "html") : ?>
<?php include('salvar_contrato.ctp')?>
<?php endif; ?>
<h4>Anexo A - PREVISÃO DE EVENTOS DE FORMATURA PARA COBERTURA DE FOTO E FILMAGEM</h4>
<p style="font-size: 1.05em; line-height: 1.8em">
<?=str_pad("Faculdade:", 70, "&nbsp;")?><?=$turma['Turma']['nome']?>
<br />
<?=str_pad("Curso:", 108, "&nbsp;")?><?=$turma['Turma']['cursos']?>
<br />
<?=str_pad("Conclusão:", 71, "&nbsp;")?><?=$turma['Turma']['ano_formatura']?>.<?=$turma['Turma']['semestre_formatura']?>
</p>
Na qualidade de Contratantes: Os formandos do(s) curso(s) de <b><?=$turma['Turma']['cursos']?></b>,
da <b><?=$turma['Turma']['nome']?></b>, com previsão de formatura para o <b><?=($turma['Turma']['semestre_formatura'] == 1) ? 'primeiro' : 'segundo';?> 
semestre de <?=$turma['Turma']['ano_formatura']?></b>, neste ato devidamente representados pela Comissão de Formatura definem 
a previsão dos seguintes eventos relacionados a formatura que serão registrados pela <b>CONTRATANTE</b>:<br /><br />
<?=$turma['Turma']['eventos_anexo_a']?>