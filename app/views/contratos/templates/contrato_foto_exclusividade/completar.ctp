<script type="text/javascript">
    $(document).ready(function() {
        var button = $("<button>",{
            type: 'button',
            class: 'button bg-color-blue',
            id:'editar-turma',
            text:'Editar Turma'
        });
        $('.modal-footer').prepend(button);
        $("#editar-turma").click(function(e) {
            e.preventDefault();
            var context = ko.contextFor($("#content-body")[0]);
            context.$data.page('/<?=$this->params['prefix']?>/turmas/editar_dados');
            bootbox.hideAll();
        });
    });
</script>
<div class='row-fluid'>
    <div class='alert alert-danger'>
        <h4>Complete os campos da turma</h4>
        <p>
            Para gerar o contrato voc&ecirc; deve completar os campos da turma descritos abaixo
            <br />
            <br />
            <strong><?=implode(',',$completar)?></strong>
        </p>
    </div>
</div>