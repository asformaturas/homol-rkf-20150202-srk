<?php if($out == "html") : ?>
<?php include('salvar_contrato.ctp')?>
<?php endif; ?>
<h4>ANEXO D - ATA DE FECHAMENTO</h4>
<p style="font-size: 1.05em; line-height: 1.8em">
<?=str_pad("Faculdade:", 70, "&nbsp;")?><?=$turma['Turma']['nome']?>
<br />
<?=str_pad("Curso:", 108, "&nbsp;")?><?=$turma['Turma']['cursos']?>
<br />
<?=str_pad("Conclusão:", 71, "&nbsp;")?><?=$turma['Turma']['ano_formatura']?>.<?=$turma['Turma']['semestre_formatura']?>
</p>
<p style="text-align: justify">
1. O FEE cobrado pela empresa será de <b><?=$turma['Turma']['fee']?></b> sobre todos os créditos que 
entrarem na conta;<br/><br/>
2. O custo de emissão do boleto está contemplado no FEE cobrado pela empresa, não havendo cobrança adicional;<br/><br/>
3. Não haverá cobrança por saques realizados;<br/><br/>
4. Com o fechamento da Exclusividade Fotográfica com a SADER & AVILA vinculado ao contrato da ZAPE, o FEE passará de 
<b><?=$turma['Turma']['fee']?></b>% para 1%;<br/><br/>
5. Com o fechamento do Contrato com a ÁS EVENTOS para a realização dos eventos de formatura, o FEE não será mais cobrado;<br/><br/>
6. A Comissão de Formatura irá fornecer os dados dos formandos já coletados anteriormente, e os mesmos serão cadastrados 
no sistema online por onde serão gerados os novos boletos aos formandos;<br/><br/>
7. Para a geração do relatório final com todos os pagamentos realizados pelos formandos, a Comissão de Formatura deverá 
fornecer todas as informações de pagamentos atualizadas;<br/><br/>
8. A Comissão será a responsável por comunicar os formandos como gerar os boletos e acessar o espaço online;<br/><br/>
9. O Código da turma é <?=$turma['Turma']['id']?>;<br/><br/>
</p>
<br />
<br />