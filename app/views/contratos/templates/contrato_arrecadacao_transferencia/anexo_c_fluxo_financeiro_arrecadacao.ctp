<?php if($out == "html") : ?>
<?php include('salvar_contrato.ctp')?>
<?php endif; ?>
<h4>ANEXO C – ATA DO FLUXO FINANCEIRO DA COBRANÇA</h4>
<p style="font-size: 1.05em; line-height: 1.8em">
<?=str_pad("Faculdade:", 70, "&nbsp;")?><?=$turma['Turma']['nome']?>
<br />
<?=str_pad("Curso:", 108, "&nbsp;")?><?=$turma['Turma']['cursos']?>
<br />
<?=str_pad("Conclusão:", 71, "&nbsp;")?><?=$turma['Turma']['ano_formatura']?>.<?=$turma['Turma']['semestre_formatura']?>
</p>
<p style="text-align: justify">
Ficam estipuladas neste anexo, as condições referentes aos valores arrecadados dos formandos.
</p>
<p style="text-align: justify">
Valor por Formando – <b>R$<?=number_format($turma['Turma']['valor_total_contrato'] / $turma['Turma']['expectativa_formandos'], 2, ',', '.')?></b><br /><br />
Quantidade Máxima de Parcelas – <b><?=$turma['Turma']['qtde_parcelas']?></b><br /><br />
Opções de Data de Vencimento – <b><?=$turma['Turma']['vencimentos_disponiveis']?></b><br /><br />
Aplica IGPM a Cada 12 Meses – <b><?=$turma['Turma']['aplica_igpm'] == 0 ? 'Não' : 'Sim'?></b><br /><br />
Aplica Multa/Mora Por Atraso – <b><?=$turma['Turma']['aplica_multa'] == 0 ? 'Não' : 'Sim'?></b><br /><br />
<?php if($turma['Turma']['aplica_multa'] == 1) : ?>
Percentual da Multa Por Atraso – <b><?=$turma['Turma']['multa_por_atraso']?></b><br /><br />
Valor da Mora Por Dia – <b><?=$turma['Turma']['juros_mora']?></b><br /><br />
<?php endif; ?>
Haverá transferência mensal para conta corrente da Comissão – <b><?=$turma['Turma']['transferencia_mensal'] == 0 ? 'Não' : 'Sim'?></b></b><br />
</p>
<br />
Os membros são:
<br />
<br />
<?php foreach($turma['Turma']['comissao'] as $comissao) : ?>
<p style="line-height: 1.3em">
<br />
<b>Nome: </b><?=key($comissao)?>
<br />
<b>RG: </b><?=$comissao[key($comissao)]?>
<br />
<br />
<?=str_pad("",60,"_")?>
<br />
ASSINATURA
<br />
</p>
<?php endforeach; ?>