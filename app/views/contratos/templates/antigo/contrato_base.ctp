<?php if($out == "html") : ?>
<?php include('salvar_contrato.ctp')?>
<?php endif; ?>
<p><b>Pelo presente Contrato, as partes:</b>
<br />
<br />
ÁS EVENTOS LTDA., sociedade brasileira por quotas de responsabilidade limitada, inscrita no CNPJ sob o 
n.º 06.163.144/0001-45, estabelecida no estado de São Paulo, onde é sediada à Rua dos Bogaris, 04 - Mirandópolis,
CEP: 04047-020, São Paulo – SP, neste ato legalmente representada por seu sócio, o Sr. Rachid Sader, 
brasileiro, solteiro , administrador de empresa, portador da cédula de identidade RG n.º 26.572.035-7 SSP/SP 
e inscrito no CPF/MF sob o n.º 289.174.098-06, residente e domiciliado à Rua Nhandu 308, Planalto Paulista 
na Cidade de São Paulo, Estado de São Paulo
<br />
<br />
e de outro lado, os formandos da: <b><?=$turma['Turma']['nome']?></b>
<br />
representando o(s) curso(s): <b><?=$turma['Turma']['cursos']?></b>
<br />
com previsão de formatura para: <b><?=$turma['Turma']['ano_formatura']?>.<?=$turma['Turma']['semestre_formatura']?></b>
<br />
representados pela comissão de formatura, neste ato denominada simplesmente “Contratantes”,
<br />
<br />
têm entre si justo e contratado o presente Contrato de Prestação e Representação de Serviços (doravante 
denominado "Contrato de Prestação de Serviço"), que será regido pelos termos e condições a seguir 
estipulados, em conformidade com os artigos 125 e seguintes do Código Civil Brasileiro:
<br />
<br />
I - ANEXOS
<br />
<br />
1. São parte integrante do Contrato os seguintes anexos:
<br />
<br />
1.1 Anexo A - Planilha de Preços e Serviços Aprovada referente a festa de formatura- Planilha aprovada em 
<b><?=date('d/m/Y',strtotime($turma['Turma']['data_assinatura_contrato']))?></b>, pela Contratante e Contratada, que contém 
os serviços e produtos definidos pelas partes, assim <b>como a explicitação da taxa administrativa praticada 
pela ÁS eventos e que devem ser mantidos até o fiel cumprimento de todos os serviços descritos no Anexo A.</b>
<br />
<br />
1.2 Anexo B – Termo de Finalização da negociação – Ata padrão, incluindo acertos verbais estabelecidos 
entre a empresa e a comissão de formatura.
<br />
<br />
1.3. Anexo C – Ata de Constituição da Contratante;
<br />
<br />
1.4. Anexo D - Modelo do Termo de Adesão e Compromisso - Contrato de adesão individual a ser firmado 
entre cada formando nos cursos acima qualificados, (doravante denominados coletivamente como 
"Formandos" ou individualmente como "Formando") e a Contratada, cujos termos ficarão vinculados ao 
presente Contrato tão logo cada Formando venha a aderir ao evento de formatura (doravante denominado o 
"Evento"), presente objeto deste Contrato e descrito no Anexo A; 
<br />
<br />
1.5. Anexo E - Contrato de Cobertura Fotográfica e de Filmagem;
<br />
<br />
1.6. Anexo F - Boletim Informativo, com as formas de pagamento disponíveis para os formandos.
<br />
<br />
II – OBJETO E CONSIDERAÇÕES INICIAIS
<br />
<br />
2.1. A Contratada compromete-se a representar a Contratante, e providenciar a contratação de todos os 
serviços e produtos descritos no Anexo A ao presente instrumento (doravante denominados respectivamente 
"Serviços" e "Produtos"), bem como realizar todo o serviço de cobrança e captação de recursos para a 
contratante,.
<br />
<br />
2.2. A Contratada declara ter estrutura adequada para atender ao objeto do presente Contrato e assume, neste 
ato, o compromisso de organizar e fazer realizar todas as festividades descritas no Anexo A dos alunos 
representados pelos Contratantes.
<br />
<br />
2.3 O valor pago individualmente pelos formandos permanecerá em posse da CONTRATADA de forma 
transitória, até o repasse aos fornecedores do pacote de formatura do quais a CONTRATADA é intermediária, 
ficando tão-somente com a comissão que lhe faz jus.
<br />
<br />
2.4. Fica estipulado as seguintes condições gerais do contrato:
<p style="text-indent: 43px">
2.4.1 Número de Formandos aderentes: <b><?=$turma['Turma']['expectativa_formandos']?></b>
<br />
2.4.2 Valor total do contrato: <b>R$<?=number_format($turma['Turma']['valor_total_contrato'], 2, ',', '.')?></b>
<br />
2.4.3 Valor por formando: <b>R$<?=number_format($turma['Turma']['valor_total_contrato']/$turma['Turma']['expectativa_formandos'], 2, ',', '.')?></b>
</p>
<br />
<b>
2.5. Fica desde já estabelecido que a comissão de formatura que assina esse Contrato não tem nenhum 
tipo de obrigação ou responsabilidade civil ou financeira referente a este. Assim sendo, toda e qualquer 
obrigação ou responsabilidade civil ou financeira é de inteira responsabilidade da CONTRATADA.
</b>
<br />
<br />
III - PREÇOS E REAJUSTES
<br />
<br />
3. A Contratada obriga-se a realizar os Serviços do Anexo A, representando a contratante na contratação de 
terceiros pelo preço estabelecido no item 2.4.2 – Valor total do contrato - rateado entre o número de formandos 
definidos no item 2.4.1 – Número de Formandos Aderentes - cabendo individualmente ao Formando a 
responsabilidade (conforme o Termo de Adesão e Compromisso) pelo pagamento da cota estipulada no item 
2.4.3 – Valor por formando - que poderão ser quitados conforme as opções de pagamento descritas no anexo F 
– Boletim Informativo.
<br />
<br />
3.1. Os valores das parcelas serão reajustados de 12 em 12 meses, a contar da data de 
assinatura do contrato <b><?=date('d/m/Y',strtotime($turma['Turma']['data_assinatura_contrato']))?></b>, 
conforme a variação do índice IGP-M, divulgado pela Fundação Getúlio Vargas. Caso esse índice seja extinto, 
o índice de referência para o cálculo de reajuste será substituído por outro índice que venha a ser publicado 
pelo Governo para substituí-lo qualitativamente, ou seja, para refletir a inflação do período acima descrito. 
Na ausência de um índice oficial publicado pelo Governo, as partes acordarão, de boa fé, acerca do índice de 
reajuste alternativo.
</p>
<p>
3.1.1. A Contratada garante, desde já, que se obriga à prestação de todos os serviços, nas mesmas condições e 
preços de todos os seus anexos, caso o número de Formandos Aderentes atinja o número mínimo estabelecido 
no item 2.4.1 – Numero de Formandos aderentes. Caso esse número não seja atingido, as partes se 
comprometem a repactuar este Contrato ao número de formandos participantes, no entanto sem onerar ou 
aumentar as cotas individuais de cada formando.
<br />
<br />
3.1.2. É de inteira responsabilidade de a Contratada assumir os reajustes de preços dos serviços e locações 
constantes na Planilha de Orçamento, não cabendo quaisquer responsabilidades à Contratante, onde entendido 
e acordado entre as partes, a Contratada terá direito de efetuar a venda dos convites e mesas excedentes, 
desde que exclusivamente para os formandos que aderiram ao pacote de formatura.
<br />
<br />
3.2. Entende-se por Formando aderente, o Formando que aderir ao Termo de Adesão e Compromisso e 
cumprir com as obrigações financeiras estabelecidas e não desistir da formatura .
<br />
<br />
3.3. Em caso de falecimento, reprovação, transferência de universidade, transferência de Estado, desistência 
do curso, ou enfermidade, o Formando (ou seu representante legal) receberá a quantia já paga corrigida pelo 
mesmo índice previsto no Contrato.
<br />
<br />
3.4. A desistência por motivo diverso dos descritos acima, poderá ser feita até 90 dias antes do termino do ano
letivo e implicará na perda de 20% do valor contratual, em favor da Contratada. Caso a prestação dos serviços
e/ou o fornecimento dos produtos já tenha sido parcialmente cumprido e verificando-se um dano real da
Contratada, mediante comprovação de planilha de custo da mesma, o montante devido pelo tipo de desistência
aqui descrita, não poderá ultrapassar 50% (cinqüenta por cento) do valor do contrato. 1. Após este período será
exigido o pagamento integral do Contrato.
<br />
<br />
3.5. Eventual atraso no pagamento das parcelas remanescentes acarretará multa de 
<?=$turma['Turma']['multa_por_atraso']?>% (dez por cento), 
mais juros de mora de 1% (um por cento) ao mês sobre o valor em atraso, calculados desde a data do 
vencimento até a data do efetivo pagamento.
<br />
<br />
3.5.1. A Contratada se incumbirá de disponibilizar os boletos online no espaço do formando, 
em <a href="http://www.asformaturas.com.br">http://www.asformaturas.com.br</a>, sempre com 
antecedência aos vencimentos de modo a proporcionar tempo hábil ao pagamento, sob pena de 
isenção da multa da cláusula.
<br />
<br />
IV - SERVIÇOS EXTRACONTRATUAIS
<br />
<br />
4. Os demais serviços e produtos não constantes do Anexo A que eventualmente a Contratante e a Contratada 
venham a acordar, tanto no preço quanto em sua prestação ou fornecimento, deverão ser inclusos em forma 
de Aditamento Contratual.
<br />
<br />
V - OBRIGAÇÕES E RESPONSABILIDADES DA CONTRATADA
<br />
<br />
5.1. Para o fiel cumprimento das obrigações contratuais que se originam do presente instrumento, cabe à 
Contratada:
<br />
<br />
a. Responsabilizar-se integralmente pela prestação dos serviços, execução de subcontratações, fornecimento 
de produtos e prestação de informações que se façam necessárias ou que sejam solicitadas pela Contratante;
<br />
<br />
b. Cumprir as posturas dos municípios, bem como as disposições legais, sejam elas estaduais ou federais, que 
interfiram na prestação dos serviços ou fornecimento dos produtos; Inclui taxa de ECAD, e projeto necessário 
para aprovação do CONTRU e outras taxas quando necessárias.
<br />
<br />
c. Manter durante toda a execução do Contrato em compatibilidade com as obrigações assumidas, todas as 
condições que culminaram em sua habilitação e qualificação para sua contratação.
<br />
<br />
d. Reparar, corrigir, remover, substituir, trocar, às suas expensas, a critério da Contratante no todo ou em 
parte, quaisquer itens constantes do Anexo A e Anexo E do presente Contrato, em que se verifiquem vícios, 
defeitos ou incorreções resultantes da execução ou de materiais empregados, por qualquer uma das partes, 
sem com isso repassar qualquer custo à Contratante, desde que por culpa exclusiva 
da Contratada, e ou terceiros contratados por esta, dentro de um prazo de 30 (trinta) dias úteis após a 
notificação por parte dos Contratantes à Contratada, salvo nos casos de produtos de uso inferior a 30 dias, 
que deverão ser trocados em tempo hábil para a utilização dos mesmos.
<br />
<br />
e. Fornecer para controle dos Contratantes listagens dos pagamentos efetuados pelos formandos, sempre que 
solicitados por escrito, sendo expressamente proibido aos Contratantes a sua divulgação pública.
<br />
<br />
f. Executar os serviços dentro dos melhores padrões de qualidade, utilizando-se do que for convencionado 
com os Contratantes, empregando material de primeira qualidade.
<br />
<br />
g. Operar com empregados, mesmo os terceirizados, devidamente treinados e preparados para a execução 
das atividades ora contratadas, em número suficiente à perfeita prestação dos serviços.
<br />
<br />
h. A Contratada se responsabilizará por todo dano causado em qualquer convidado nos eventos ora 
contratados que sejam oriundos da montagem, de seus funcionários ou prepostos (garçom, segurança, equipe 
de coordenação, recepcionistas,etc...)
<br />
<br />
i. A Contratada, de posse dos Termos de Adesão, mesmo que haja problemas quanto ao pagamento das 
parcelas, se compromete a cumprir o que ora fica convencionado neste instrumento, exceto nos casos de 
efetiva desistência, onde a Contratada se obriga à devolução de parte da quantia paga ao formando.
<br />
<br />
j. Caso o número de formandos que realizarem adesão for superior ao estabelecido no item 2.4.1 – Número de 
Formandos Aderentes, é obrigação da CONTRATADA reverter 50% de cada adesão adicional para um fundo 
a ser usado pela CONTRANTANTE. Esse fundo de caixa pode ser usado para novos investimentos nos 
eventos contratados, na inclusão de novos itens no evento, contratação de novos serviços ou até mesmo 
reembolso aos formandos.
<br />
<br />
VI - OBRIGAÇÕES DA CONTRATANTE
<br />
<br />
6.1. Para a prestação dos serviços e fornecimento dos produtos, a Contratante obriga-se a:
<br />
<br />
a. Fornecer o número mínimo de formandos pagantes estabelecido no item 2.4.1 – Número de Formandos 
Aderentes, possibilitando-se, assim, a quitação total do presente instrumento.
<br />
<br />
a.1. Caso o número de adesões de formandos pagantes, participantes dos eventos, previsto neste Contrato 
não seja alcançado até o fim do ano letivo, a Contratada, após solicitação por escrito dos Contratantes, poderá 
alterar os produtos/serviços ora contratados, de forma a compensar o débito eventualmente existente, uma 
vez que o custo individual a ser pago pelo formando, a pedido exclusivo dos Contratantes, foi calculado 
dividindo-se o custo total deste instrumento pelo número estabelecido no item 2.4.1 – Número de Formandos 
Aderentes. Esse valor pago individualmente não sofrerá em hipótese alguma nenhum tipo de reajuste.
<br />
<br />
a.2. A alteração de produtos / serviços será feita de comum acordo entre a Contratada e o Contratante.
<br />
<br />
a.3. Definida a alteração de produtos / serviços a Contratada sugerirá aos Contratantes a adequação do 
Contrato os quais terão um prazo de 15 (quinze) dias para sua alteração ou formulação de nova sugestão.
<br />
<br />
b. Divulgar o presente Contrato junto aos formandos a fim de se atingir o número mínimo de adesões.
<br />
<br />
c. Fornecer as informações necessárias à fiel execução do presente Contrato, em tempo hábil, determinado 
quando da solicitação das mesmas;
<br />
<br />
d. No que se refere aos convites (descritos no Anexo A), fornecer especificações relativas aos textos, fotolitos, 
desenhos, distintivos, brasões, medalhas, gravuras ou quaisquer outras especificações que tenha sido 
previamente tratadas, com a antecedência de 60 (sessenta) dias da data de entrega do serviço.
<br />
<br />
6.2. Uma vez confirmados as datas junto a Contratada e esta comprometendo-se com terceiros, os 
Contratantes não mais poderão alterá-las, salvo motivo de força maior.
<br />
<br />
VII - OBRIGAÇÕES RECÍPROCAS
<br />
<br />
7. Será de responsabilidade da Contratante e da Contratada, a formulação de listas como os nomes e dados 
relevantes dos Formandos, para que os mesmos façam as devidas alterações e correções em tempo hábil. A 
fiscalização dos convites já impressos também será de responsabilidade de ambas as partes.
<br />
<br />
VIII – DOS DIREITOS DOS CONTRATANTES
<br />
<br />
8.1. Solicitar relatórios à Contratada, a qualquer momento, que demonstrem a situação financeira do presente 
contrato, bem como, da situação individual do formando, sobre as quais deverá manter sigilo.
<br />
<br />
8.2. Realizar reuniões com o atendimento da Contratada, desde que marcada com antecedência mínima de 5 
(cinco) dias úteis, encaminhando obrigatoriamente o pedido por e-mail, contendo a pauta da reunião, com 
clara especificação dos assuntos a serem abordados, a fim de que a Contratada possa programar-se, visando 
a antecipação das possíveis soluções.
<br />
<br />
8.3. Verificar e fiscalizar todos e quaisquer produtos que estão orçados no seu orçamento, através de 
degustações, apresentações, visita a fornecedores terceirizados através de solicitação prévia para o 
atendimento da CONTRATADA.
<br />
<br />
IX – FORO
<br />
9. Fica eleito o Foro Central da Comarca da capital do Estado de São Paulo, para dirimir eventuais questões 
oriundas da celebração, interpretação e execução do presente Contrato, com renúncia a qualquer outro, por 
mais privilegiado que seja, arcando a parte vencida com todo o ônus de sucumbência, inclusive honorários 
advocatícios de 20% (vinte por cento) do total da condenação.
<br />
<br />
X - FOTOGRAFIA E FILMAGEM
<br />
<br />
10. A prestação dos serviços de cobertura fotográfica e de filmagem dos Eventos, bem como a venda das 
respectivas fotografias e fitas editadas deverá obedecer ao disposto no Anexo E.
<br />
<br />
XI – DOCUMENTOS
<br />
<br />
11.1. O presente Contrato, seus respectivos anexos e eventuais aditamentos são os únicos instrumentos legais e 
reguladores dos serviços, produtos e eventos ora contratados, substituindo quaisquer documentos anteriormente 
trocados entre Contratante e Contratada.
<br />
<br />
11.2. Todos os documentos e/ou cartas entre a Contratante e a Contratada serão trocados através de expediente 
protocolado ou e-mail. Os documentos a serem entregues à Contratante deverão ser recebidos por qualquer 
membro da comissão de formatura.
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
</p>
<p style="text-indent: 150px; font-weight: bold">
São Paulo, <?=$form->diasDaSemana[date('w',strtotime($turma['Turma']['data_assinatura_contrato']))]?>, 
<?=date('d',strtotime($turma['Turma']['data_assinatura_contrato']))?> de 
<?=$form->meses[(date('n',strtotime($turma['Turma']['data_assinatura_contrato']))-1)]?> de 
<?=date('Y',strtotime($turma['Turma']['data_assinatura_contrato']))?>
<br />
<br />
<br />
<br />
<br />
</p>
<p style="font-size:0.86em">
PELA CONTRADADA
<br />
<br />
<br />
<br />
<br />
<?=str_pad("", 50, "_")?>
<br />
<br />
RACHID SADER
<br />
R.G. 26.572.035-07
<br />
<br />
</p>
<p style="font-size:0.86em">
PELA CONTRATANTE
<br />
<br />
<br />
</p>
<?php foreach($turma['Turma']['comissao'] as $comissao) : ?>
<p style="line-height: 1.3em">
<b>Nome: </b><?=key($comissao)?>
<br />
<b>RG: </b><?=$comissao[key($comissao)]?>
<br />
<br />
<?=str_pad("",60,"_")?>
<br />
ASSINATURA
<br />
</p>
<?php endforeach; ?>