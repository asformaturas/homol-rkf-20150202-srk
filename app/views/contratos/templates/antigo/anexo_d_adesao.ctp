<?php if($out == "html") : ?>
<?php include('salvar_contrato.ctp')?>
<?php endif; ?>
<?php function campos($qtde) { return str_pad("", ($qtde*3)+1, "|__"); } ?>
<table style="border-collapse: collapse">
    <tr>
        <td style="width:78%; font-size: 0.74em; line-height: 0.6em">
            <p>Caso opte entregar o contrato preenchido ao 
            invés de adesão on-line seu primeiro acesso será:
            <br />
            <br />
            login = e-mail e senha = primeiro nome e código da turma.
            </p>
        </td>
        <td style="width:17%; text-align: center; border: solid 1px #333333; font-size:0.86em; line-height: 1.4em">
            PARA USO DA &Aacute;S
            <br />
            <b style="line-height: 1.8em; font-size:1.05em"><?=$turma['Turma']['id']?></b>
        </td>
        <td style="width:5%"></td>
    </tr>
</table>
<h4 style="line-height: 0.8em">ANEXO D - CONTRATO DE ADESÃO E COMPROMISSO</h4>
<p style="text-align: center; font-size:0.9em">PREENCHIMENTO LEGÍVEL (letra de forma)</p>
<p style="font-size: 0.85em; line-height: 0.8em">Pelo presente instrumento particular de prestação 
    de serviços e na melhor forma de direito, o(a) Sr(a):</p>
<p style="font-size: 0.85em; line-height: 1.40em">
Nome do aluno: <?=campos(38)?>
<br />
Data de Nascimento: |__|__|/__|__|/__|__|, RG: N.º <?=campos(10)?>, 
CPF N.º <?=campos(3)?>.<?=campos(3)?>.<?=campos(3)?>-<?=campos(2)?>,
<b>DADOS CADASTRAIS</b> <span style="font-size:0.87em">(os dados cadastrais devem ser 
preenchidos com atenção para o correto cadastramento e comunicação com os formandos)</span>
<br />
Rua / Av. <?=campos(40)?>,
<br />
n&ordm; <?=campos(6)?>&nbsp;&nbsp;compl.&nbsp;&nbsp;<?=campos(14)?>&nbsp;&nbsp;
Bairro&nbsp;&nbsp;<?=campos(16)?>
<br />
CEP <?=campos(5)?> - <?=campos(3)?>&nbsp;Munic&iacute;pio&nbsp;<?=campos(29)?>,
<br />
fone: (res.) <?=campos(2)?> <?=campos(8)?>
, fone: (com.) <?=campos(2)?> <?=campos(8)?>
, celular <?=campos(2)?> <?=campos(9)?>
, email <?=campos(41)?>
<br />
aluno&nbsp;&nbsp;&nbsp;da&nbsp;&nbsp;&nbsp;: <?=campos(39)?>,<br />
Curso&nbsp;&nbsp;<?=campos(32)?>, Sala / Turma <?=campos(4)?>,<br />
Per&iacute;odo: M (__) V (__) N (__), 
Numera&ccedil;&atilde;o para cal&ccedil;ado <?=campos(2)?>, Tamanho para camisetas 
<?=campos(1)?> e Altura <?=campos(3)?>
<br />
<span style="font-size:0.89em">(obs: este item é meramente informativo, 
não obrigando a empresa contratada ao fornecimento de nenhum brinde associado a estes dados)
</span>
<br />
<span style="font-size:1em; line-height: 1.2em">Nome para correspondência: Reescreva aqui, 
com o máximo de atenção e capricho, e, principalmente de forma legível, o nome que 
você quer que apareça em todos os processos referentes as festividades de sua formatura, 
tais como: informativos, correspondências, materiais gráficos (convites, crachás, etc.), 
cerimonial, clipes, entre outros. Favor incluir acentos ou outros caracteres:
</span>
<span style="font-size:1em; line-height: 2.2em"><?=str_pad("",90, "_")?></span>
<br />
<b>OPÇÕES DE PAGAMENTO</b> (os dados financeiros, bem como a opção de pagamento 
deverão ser preenchidas para validação) 
<br />
Valor R$ <?=campos(4)?>,<?=campos(2)?> Número de parcelas <?=campos(2)?> 
Data da Primeira parcela |__|__|/__|__|/__|__|
<br />
Opção de pagamento: (__) Cheques pré-datados (__) 
Boletos online, sem custo (__) Boletos entregue em casa, custo de R$45,00
</p>
<p style="font-size:0.75em; text-indent: 20px; text-align: justify">
<b>Adere ao Contrato de Prestação de Serviços, Representação Financeira e de Fotos 
firmado entre a Comissão de Formatura de seu curso e a empresa ÁS EVENTOS LTDA.</b>, 
inscrita no CNPJ/MF sob N.º06.163.144/0001-45, com sede na Rua dos Bogaris, 04 - Mirandópolis,
CEP: 04047-020, São Paulo – SP, representada neste ato pelo sócio-diretor 
RACHID SADER, portador do RG Nº 26.572.035-7, contrato esse assinado em 
        <?=date('d/m/Y',strtotime($turma['Turma']['data_assinatura_contrato']))?> que 
será denominado <b>CONTRATO COLETIVO</b> e regido nos seguintes moldes: 
<br />
1.&nbsp;&nbsp;&nbsp;&nbsp;O formando acima qualificado reconhece a Comissão de Formatura de sua turma como 
legítima representante para tratar dos assuntos relacionados à sua Formatura, 
outorgando poderes para representá-lo em atos referentes à sua formatura, ratificando 
os termos do CONTRATO COLETIVO, declarando ter plenos conhecimentos desse contrato e 
outorgando poderes para a Comissão celebrar aditivos contratuais que se fizerem 
necessários à realização dos eventos.
<br />
2.&nbsp;&nbsp;&nbsp;&nbsp;As informações importantes a respeito do seu contrato estarão disponibilizadas no link: 
<a href="http://www.formaturas.as/espaco_formando"><b>http://www.formaturas.as/espaco_formando</b></a>
<br />
3.&nbsp;&nbsp;&nbsp;&nbsp;A comissão de formatura não poderá tratar assuntos de cunho financeiro, tais como 
forma de pagamento, renegociação de dívida ou cancelamento de contrato em nome do formando.
<br />
4.&nbsp;&nbsp;&nbsp;&nbsp;O formando acima qualificado autoriza a ÁS eventos a representá-lo 
na contratação e pagamento de fornecedores conforme descrito no CONTRATO COLETIVO. O valor 
pago pelo CONTRATANTE permanecerá em posse da CONTRATADA de forma transitória, até o repasse aos 
fornecedores do pacote de formatura do quais a CONTRATADA é intermediária, ficando tão-somente 
com a comissão que lhe faz jus.
<br />
5.&nbsp;&nbsp;&nbsp;&nbsp;O(A) CONTRATANTE se obriga a pagar a CONTRATADA pelos eventos de 
formatura descritos no CONTRATO COLETIVO e resumidos no 
boletim informativo entregue para o formando junto com esse contrato.
<br />
6.&nbsp;&nbsp;&nbsp;&nbsp;<b>No momento da retirada dos convites o formando que optar por boletos 
bancários deverá quitar as parcelas em atraso e trocar os boletos ainda em aberto por cheques 
pré-datados. No mesmo momento, o formando que tiver optado por cheques pré-datados e ainda não tiver 
os entregue, deverá entregar os mesmos.</b>
<br />
7.&nbsp;&nbsp;&nbsp;&nbsp;Os valores consignados neste contrato serão atualizados de 12 em 12 meses, 
a partir da assinatura do Contrato de Prestação de Serviços celebrado entre a Comissão de Formatura 
e a ÁS EVENTOS LTDA., (<?=date('d/m/Y',strtotime($turma['Turma']['data_assinatura_contrato']))?>) 
adotando-se a variação do IGPM/FGV do período, conforme autoriza a Lei 9.069/95, ou por outro índice 
que venha a substituí-lo. A ÁS eventos não é autorizada a realizar qualquer outro tipo de reajuste de 
valores contratuais. 
<br />
8.&nbsp;&nbsp;&nbsp;&nbsp;Os convites só serão entregues após feito o acerto financeiro, isto é, 
pendências devidamente quitadas, isto inclui os valores relativos ao IGPM, 
cheques trocados e entregues, conforme explicitado nos itens 6 e 7. 
<br />
9.&nbsp;&nbsp;&nbsp;&nbsp;No caso de rescisão contratual, o aluno poderá rescindir até 90 dias antes 
do término do ano letivo de conclusão do curso, desde que pago a título de multa o valor dos itens ou 
brindes já entregues ou contratados para os formandos adicionado de 20% do valor total do contrato. 
Após este período será exigido o pagamento integral do Contrato, exceto devido a motivos justificados 
e comprovados, tais como reprovação ou transferência por motivos profissionais, nesse caso ficando o 
formando livre de multa.
<br />
10.&nbsp;&nbsp;&nbsp;&nbsp;Constitui obrigação do(a) CONTRATANTE manter seu endereço e demais dados 
cadastrais atualizados, bem como a responsabilidade pela grafia correta de seu nome, nos textos originais 
que deverão ser entregues à CONTRATADA para a execução dos serviços gráficos, eximindo a 
CONTRATADA pela falta do nome e/ou grafia incorreta conforme previsão no Contrato Principal.
<br />
11.&nbsp;&nbsp;&nbsp;&nbsp;Considerando que os convites de luxo serão montados com fotos da turma e/ou 
formando, a Comissão de Formatura avisará todos os formandos da data/local e horário marcado junto à 
CONTRATADA para serem fotografados, estando o formando ciente que sua ausência isentará a CONTRATADA 
de qualquer responsabilidade referente as fotografias no convite.
<br />
12.&nbsp;&nbsp;&nbsp;&nbsp;A empresa não se responsabiliza por objetos pessoais em todos os eventos 
organizados pela empresa.
<br />
13.&nbsp;&nbsp;&nbsp;&nbsp;Fica eleito o Foro da Capital do Estado de São Paulo por mais privilegiado 
que outro seja, arcando a parte vencida com o ônus da sucumbência, inclusive honorários advocatícios 
de 20% do total da condenação.
</p>
<table>
    <tr>
        <td height="25" colspan="5" width="95%"
            style="font-size:0.75em; text-align: right">São&nbsp;&nbsp;Paulo,
        _____de________________de <?=substr(date('Y'),0,2)?>____</td>
        <td width="5%"></td>
    </tr>
    <tr>
        <td width="35%" style="font-size:0.75em">E assim, por estarem justos e contratados.</td>
        <td width="5%"></td>
        <td width="25%" style="height:0.7em; border-bottom:solid 1px black"></td>
        <td width="5%"></td>
        <td width="25%" style="height:0.7em; border-bottom:solid 1px black"></td>
        <td width="5%"></td>
    </tr>
    <tr>
        <td width="35%"></td>
        <td width="5%"></td>
        <td width="25%" style="height:0.5em; font-size:0.75em; text-align: center">CONTRATADO</td>
        <td width="5%"></td>
        <td width="25%" style="height:0.5em; font-size:0.75em; text-align: center">CONTRATANTE</td>
        <td width="5%"></td>
    </tr>
</table>