<?php if($out == "html") : ?>
<?php include('salvar_contrato.ctp')?>
<?php endif; ?>
<h4>ANEXO E – ATA DE REPASSE DOS VALORES COBRADOS</h4>
<p style="font-size: 1.05em; line-height: 1.8em">
<?=str_pad("Faculdade:", 70, "&nbsp;")?><?=$turma['Turma']['nome']?>
<br />
<?=str_pad("Curso:", 108, "&nbsp;")?><?=$turma['Turma']['cursos']?>
<br />
<?=str_pad("Conclusão:", 71, "&nbsp;")?><?=$turma['Turma']['ano_formatura']?>.<?=$turma['Turma']['semestre_formatura']?>
</p>
<p style="text-align: justify">
Ficam definidas neste anexo pelas partes a forma como será gerida os valores cobrados.
</p>
<p style="text-align: justify">
Origem do Valor transferido – ZAPE GESTÃO DE VALORES<br /><br />
Periodicidade – <?=!empty($turma['Turma']['periodicidade_transferencia']) ? "<b>".$turma['Turma']['periodicidade_transferencia']."</b>" : '' ?> em <?=!empty($turma['Turma']['periodicidade_transferencia']) ? "<b>".$turma['Turma']['periodicidade_transferencia']."</b>" : '' ?> dias<br /><br />
Informações da Conta que receberá o Crédito:<br /><br />
</p>
<p style="text-align: justify;margin-left: 20px">
<b>a.</b> Nome ou Razão Social – <?=!empty($turma['Turma']['nome_destinatario']) ? "<b>".$turma['Turma']['nome_destinatario']."</b>" : '' ?><br /><br />
<b>b.</b> CPF ou CNPJ – <?=!empty($turma['Turma']['cpf_cnpj_destinatario']) ? "<b>".$turma['Turma']['cpf_cnpj_destinatario']."</b>" : '' ?><br /><br />
<b>c.</b> Banco – <?=!empty($turma['Turma']['banco_destinatario']) ? "<b>".$turma['Turma']['banco_destinatario']."</b>" : '' ?><br /><br />
<b>d.</b> Agencia – <?=!empty($turma['Turma']['agencia_destinatario']) ? "<b>".$turma['Turma']['agencia_destinatario']."</b>" : '' ?><br /><br />
<b>e.</b> Conta Corrente com Dígito – <?=!empty($turma['Turma']['conta_destinatario']) ? "<b>".$turma['Turma']['conta_destinatario']."</b>" : '' ?><br /><br />
</p>
<br />
Os membros são:
<br />
<br />
<?php foreach($turma['Turma']['comissao'] as $comissao) : ?>
<p style="line-height: 1.3em">
<br />
<b>Nome: </b><?=key($comissao)?>
<br />
<b>RG: </b><?=$comissao[key($comissao)]?>
<br />
<br />
<?=str_pad("",60,"_")?>
<br />
ASSINATURA
<br />
</p>
<?php endforeach; ?>