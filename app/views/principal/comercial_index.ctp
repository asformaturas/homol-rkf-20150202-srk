<div class="span12 metro" style="position:relative" id="content-container">
    <div class="row-fluid content-loader hide" id="content-loading">
        <div class="cycle-loader"></div>
    </div>
    <div class="row-fluid content-loader hide" id="content-body"></div>
    <div class="row-fluid content-loader" id="content-home">
        <?php if(!empty($turmaLogada['Turma']['id'])) { ?>
        <div class="tile icon bg-color-red"
             data-bind="click: function(data, event) {
             page('<?=$this->webroot . $this->params['prefix'] ?>/comissoes/listar') }">
            <div class="tile-content">
                <div class="img icon-light-bulb"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Comiss&atilde;o</span>
            </div>
        </div>
        <div class="tile icon bg-color-darken"
             data-bind="click: function(data, event) {
             page('<?=$this->webroot . $this->params['prefix'] ?>/formandos/listar') }">
            <div class="tile-content">
                <div class="img icon-users"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Formandos</span>
            </div>
        </div>
        <div class="tile icon bg-color-green"
             data-bind="click: function(data, event) {
             page('<?=$this->webroot . $this->params['prefix'] ?>/mensagens/cronograma') }">
            <div class="tile-content">
                <div class="img icon-chat"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Cronograma</span>
            </div>
        </div>
        <div class="tile double icon bg-color-gray"
             data-bind="click: function(data, event) {
             page('/mensagens/email') }">
            <div class="tile-content">
                <div class="img icon-email"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Central De Mensagens</span>
            </div>
        </div>
        <div class="tile icon bg-color-blueDark"
             data-bind="click: function(data, event) {
             page('<?=$this->webroot . $this->params['prefix'] ?>/registros_internos/listar') }">
            <div class="tile-content">
                <div class="img icon-database"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Registro Interno</span>
            </div>
        </div>
        <div class="tile icon bg-color-darken"
             data-bind="click: function(data, event) {
             page('<?=$this->webroot . $this->params['prefix'] ?>/relatorio_financeiro/exibir') }">
            <div class="tile-content">
                <div class="img icon-calculate"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Relat&oacute;rio Financeiro</span>
            </div>
        </div>
        <div class="tile icon app bg-color-greenDark"
            data-bind="click: function(data, event) {
                page('<?= $this->webroot . $this->params['prefix'] ?>/contratos/listar') }">
            <div class="tile-content">
                <span class="img icon-book-2"></span>
            </div>
            <div class="brand"><span class="name">Contratos</span></div>
        </div>
<!--         <div class="tile icon bg-color-purple"
             data-bind="click: function(data, event) {
             page('<?=$this->webroot . $this->params['prefix'] ?>/arquivos/listar') }">
            <div class="tile-content">
                <div class="img icon-file-excel"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Arquivos</span>
            </div>
        </div> -->
        <div class="tile icon bg-color-orange"
             data-bind="click: function(data, event) {
             page('<?= $this->webroot . $this->params['prefix'] ?>/turmas/editar_site') }">
            <div class="tile-content">
                <span class="img icon-network"></span>
            </div>
            <div class="brand"><span class="name">Site da Turma</span></div>
        </div>
        <?php if($usuario["Usuario"]["nivel"] != 'basico') : ?>
        <div class="tile icon bg-color-purple"
            data-bind="click: function(data, event) {
            page('<?=$this->webroot . $this->params['prefix'] ?>/catalogo/listar') }">
           <div class="tile-content">
               <span class="img icon-book"></span>
           </div>
           <div class="brand"><span class="name">Cat&aacute;logo</span></div>
        </div>
        <?php endif; ?>
        <?php } else { ?>
        <div class="tile icon app bg-color-blueDark"
             data-bind="click: function(data, event) {
             page('<?= $this->webroot . $this->params['prefix'] ?>/usuarios/alterar_senha') }">
            <div class="tile-content">
                <span class="img icon-key"></span>
            </div>
            <div class="brand"><span class="name">Alterar Senha</span></div>
        </div>
        <div class="tile icon bg-color-pink"
             data-bind="click: function(data, event) {
             page('/calendario/exibir') }">
            <div class="tile-content">
                <div class="img icon-calendar"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Agenda</span>
            </div>
        </div>
        <div class="tile icon bg-color-red"
             data-bind="click: function(data, event) {
             page('/turmas/listar') }">
            <div class="tile-content">
                <span class="img icon-network"></span>
            </div>
            <div class="brand"><span class="name">Turmas</span></div>
        </div>
        <div class="tile icon bg-color-orange"
             data-bind="click: function(data, event) {
             page('<?= $this->webroot . $this->params['prefix'] ?>/faculdades/relatorio') }">
            <div class="tile-content">
                <span class="img icon-list-2"></span>
            </div>
            <div class="brand"><span class="name">Colégios</span></div>
        </div>
        <div class="tile icon bg-color-greenDark"
             data-bind="click: function(data, event) {
             page('<?= $this->webroot . $this->params['prefix'] ?>/faculdades/gerenciar') }">
            <div class="tile-content">
                <span class="img icon-wrench"></span>
            </div>
            <div class="brand"><span class="name">Gerenciar Dados</span></div>
        </div>
        <div class="tile icon bg-color-darken"
             data-bind="click: function(data, event) {
             page('<?=$this->webroot . $this->params['prefix'] ?>/registros_contatos/festas') }">
            <div class="tile-content">
                <div class="img icon-play"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Festas</span>
            </div>
        </div>
        <div class="tile icon bg-color-blue"
             data-bind="click: function(data, event) {
             page('/mensagens/email') }">
            <div class="tile-content">
                <span class="img icon-mail-3"></span>
            </div>
            <div class="brand"><span class="name">Mensagens</span></div>
        </div>
        <a href="/comercial/catalogo" >
            <div class="tile icon bg-color-purple">
               <div class="tile-content">
                   <span class="img icon-book"></span>
               </div>
               <div class="brand"><span class="name">Cat&aacute;logo</span></div>
            </div>
        </a>
        <a href="/comercial/material_venda" >
            <div class="tile icon bg-color-blueDark">
               <div class="tile-content">
                   <span class="img icon-star-5"></span>
               </div>
               <div class="brand"><span class="name">Material Vendas</span></div>
            </div>
        </a>
        <div class="tile icon bg-color-orangeDark"
             data-bind="click: function(data, event) {
             page('/turmas/ficha_evento') }">
            <div class="tile-content">
                <div class="img icon-drawer-2"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Ficha Evento</span>
            </div>
        </div>
        <?php } ?>
        <div class="tile icon bg-color-pink"
             data-bind="click: function(data, event) {
             page('turmas/analise_temporada') }">
            <div class="tile-content">
                <span class="img icon-amazon-2"></span>
            </div>
            <div class="brand"><span class="name">Análise Temporada</span></div>
        </div>
    </div>
</div>
<script type="text/html" id="tmpNotas">
    <div class="opcoes div">
        <!-- ko if: $root.nota().indice > 0  -->
        <i class="icon-arrow-9 fg-color-brown" data-bind="click:notaAnterior"></i>
        <!-- /ko -->
        <!-- ko if: ($root.nota().indice+1) < $root.notas().length  -->
        <i class="icon-arrow-6 direita fg-color-brown" data-bind="click:proximaNota"></i>
        <!-- /ko -->
        <div class="titulo fg-color-grayDark">Anota&ccedil;&otilde;es</div>
    </div>
    <div class="conteudo div fg-color-grayDark">
        <!-- ko if: $root.nota() && !$root.nova() -->
        <span class="data-criacao detalhes" data-bind="text: formataData($root.nota().UsuarioNota.data_cadastro)"></span>
        <!-- /ko -->
        <!-- ko if: $root.salvando() && !$root.salvo() -->
        <span class="salvo detalhes">Salvando anota&ccedil;&atilde;o</span>
        <!-- /ko -->
        <!-- ko if: $root.salvo() && !$root.erro() -->
        <span class="salvo detalhes">Anota&ccedil;&atilde;o salva</span>
        <!-- /ko -->
        <!-- ko if: $root.salvo() && $root.erro() -->
        <span class="salvo detalhes fg-color-red">Erro</span>
        <!-- /ko -->
        <!-- ko if: $root.nota() || $root.nova() || $root.notas().length == 0 -->
        <textarea disabled="disabled" id="texto" class="nota bg-color-pastel fg-color-grayDark"
            data-bind="value:texto, valueUpdate: 'afterkeydown',
                attr:{placeholder:$root.notas().length == 0 && $root.carregada() ? 'Nenhuma anotacao foi criada por enquanto' : 'Digite'}"></textarea>
        <!-- /ko -->
        <!-- ko if: !$root.nota() && !$root.nova() && $root.notas().length > 0 -->
        <ul class='div' data-bind="foreach: $root.notas()">
            <li class="fg-color-grayDark" data-bind='text:$root.trechoNota($data.UsuarioNota.texto),
                click:function() { $root.exibirNota($index()); }'></li>
        </ul>
        <!-- /ko -->
    </div>
    <div class="opcoes div">
        <!-- ko if: $root.nota() -->
        <i class="icon-list-2 fg-color-brown" data-bind="click:function() { $root.exibirLista() }"></i>
        <!-- /ko -->
        <!-- ko if: $root.nota() && !$root.editando() -->
        <i class="icon-pen fg-color-brown" data-bind="click:function() { editando(true) } "></i>
        <i class="icon-cancel fg-color-brown" data-bind="click:function() { $root.removerNota() }"></i>
        <!-- /ko -->
        <!-- ko if: $root.editando() && !$root.nova() -->
        <i class="icon-thumbs-up direita fg-color-brown" data-bind="click:cancelarEdicao"></i>
        <!-- /ko -->
        <!-- ko if: $root.editando() && $root.nova() -->
        <i class="icon-blocked direita fg-color-brown" data-bind="click:cancelarEdicao"></i>
        <!-- /ko -->
        <!-- ko if: !$root.nova() && !$root.editando() -->
        <i class="icon-plus-alt direita fg-color-brown" data-bind="click:novaNota"></i>
        <!-- /ko -->
    </div>
</script>
