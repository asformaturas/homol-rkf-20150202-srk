<div class="span12 metro" style="position:relative" id="content-container">
    <div class="row-fluid content-loader hide" id="content-loading">
        <div class="cycle-loader"></div>
    </div>
    <div class="row-fluid content-loader hide" id="content-body"></div>
    <div class="row-fluid content-loader" id="content-home">
        <?php if($festas) : ?>
        <script type="text/javascript">
            $(document).ready(function(){
                var context = ko.contextFor($("#content-body")[0]);
                $(".confimar-presenca").click(function() {
                    var alert = $(this).parent().parent('.alert');
                    var id = alert.data('id');
                    var confirmacao = $(this).data('confirmacao');
                    $.ajax({
                        url : '/registros_contatos/confirmar_presenca',
                        data : {
                            data : {
                                id : id,
                                confirmacao : confirmacao
                            }
                        },
                        type : "POST",
                        dataType : "json",
                        success: function(response) {
                            context.$data.removerAviso(id,'convite-festa');
                            alert.fadeOut(500,function() {
                                $(this).remove();
                            });
                        },
                        complete : function() {
                            context.$data.avisoSelecionado(false);
                        }
                    });
                });
            });
        </script>
        <?php foreach($festas as $festa) : ?>
        <div class="alert alert-info" data-id="<?=$festa['RegistroContatoUsuario']['id'];?>">
            <h2 class="fg-color-white">
                <?=$usuario['Usuario']['nome']?>
                <small class="fg-color-white"><?=$festa['RegistrosContato']['assunto'];?></small>
            </h2>
            <div style="margin:10px 0">
                Voce foi convidado(a) para uma festa pelo(a) consultor(a) 
                <span class="strong"><?=$festa['Consultor']['nome'];?></span>
            </div>
            Data: <span class="strong"><?=date('d/m/Y',strtotime($festa['RegistrosContato']['data']))?></span>
            Local: <span class="strong"><?=$festa['RegistrosContato']['local'];?></span>
            <div style="margin-top:10px">
                <button type="button" data-confirmacao="1" style="border:none"
                    class="bg-color-greenDark confimar-presenca mini">
                    Confirmar Presença
                </button>
                <button type="button" data-confirmacao="0" style="border:none"
                    class="bg-color-red confimar-presenca mini">
                    N&atilde;o Estarei Presente
                </button>
            </div>
        </div>
        <?php endforeach; endif; ?>
        <div class="tile icon double app bg-color-blueDark"
             data-bind="click: function(data, event) {
             page('<?= $this->webroot . $this->params['prefix'] ?>/usuarios/editar_dados/redes-sociais') }">
            <div class="tile-content">
                <span class="img icon-facebook-2"></span>
            </div>
            <div class="brand">
                <span class="name">
                    Vincular Facebook
                    <span class="badge badge-success">
                        Novo!
                    </span>
                </span>
            </div>
        </div>
        <?php if($temMapa && false) : ?>
        <div class="tile icon bg-color-orangeDark"
            data-bind="click: function(data, event) {
            page('<?= $this->webroot . $this->params['prefix'] ?>/eventos/mapas') }">
            <div class="tile-content">
                <span class="img icon-map"></span>
            </div>
            <div class="brand"><span class="name">Mapa de Mesas</span></div>
        </div>
        <?php endif; ?>
        <div class="tile icon bg-color-green"
             data-bind="click: function(data, event) {
             page('<?=$this->webroot . $this->params['prefix'] ?>/mensagens/cronograma') }">
            <div class="tile-content">
                <div class="img icon-stats-up"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Cronograma</span>
            </div>
        </div>
        <div class="tile icon bg-color-gray"
             data-bind="click: function(data, event) {
             page('/mensagens/email') }">
            <div class="tile-content">
                <div class="img icon-email"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Central De Mensagens</span>
            </div>
        </div>
        <div class="tile icon bg-color-greenDark"
             data-bind="click: function(data, event) {
             page('/mensagens/informativos') }">
            <div class="tile-content">
                <div class="img icon-broadcast"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Informativos</span>
            </div>
        </div>
        <div class="tile icon bg-color-red"
             data-bind="click: function(data, event) {
             page('<?=$this->webroot . $this->params['prefix'] ?>/comissoes/listar') }">
            <div class="tile-content">
                <div class="img icon-light-bulb"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Comiss&atilde;o</span>
            </div>
        </div>
        <div class="tile icon bg-color-pink"
             data-bind="click: function(data, event) {
             page('/calendario/exibir') }">
            <div class="tile-content">
                <div class="img icon-light-bulb"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Agenda</span>
            </div>
        </div>
        <div class="tile icon bg-color-darken"
             data-bind="click: function(data, event) {
             page('<?=$this->webroot . $this->params['prefix'] ?>/formandos/listar') }">
            <div class="tile-content">
                <div class="img icon-users"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Formandos</span>
            </div>
        </div>
        <div class="tile icon bg-color-yellow"
             data-bind="click: function(data, event) {
             page('<?=$this->webroot . $this->params['prefix'] ?>/eventos/listar') }">
            <div class="tile-content">
                <div class="img icon-flight"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Eventos</span>
            </div>
        </div>
        <!-- <div class="tile icon bg-color-orangeDark"
             data-bind="click: function(data, event) {
             page('<?=$this->webroot . $this->params['prefix'] ?>/parcerias/listar') }">
            <div class="tile-content">
                <div class="img icon-diamonds"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">RK Vantagens</span>
            </div>
        </div> -->
        <div class="tile icon bg-color-pinkDark"
             data-bind="click: function(data, event) {
             page('<?=$this->webroot . $this->params['prefix'] ?>/formandos/fotos_telao_listar') }">
            <div class="tile-content">
                <div class="img icon-picture-2"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Fotos Telão</span>
            </div>
        </div>
        <div class="tile icon bg-color-blue"
             data-bind="click: function(data, event) {
             page('<?=$this->webroot . $this->params['prefix'] ?>/pendencias/') }">
            <div class="tile-content">
                <div class="img icon-warning"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Pend&ecirc;ncias</span>
            </div>
        </div>
        <?php if($usuario['FormandoProfile']['aderido'] == 0){ ?>
        <div class="tile icon bg-color-purple"
             data-bind="click: function(data, event) {
             page('<?=$this->webroot . $this->params['prefix'] ?>/formandos/aderir') }">
            <div class="tile-content">
                <div class="img icon-user-4"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Fazer Adesão</span>
            </div>
        </div>
        <?php } ?>
        <!--
        <div class="tile icon bg-color-orange"
             data-bind="click: function(data, event) {
             page('<?=$this->webroot . $this->params['prefix'] ?>/arquivos/listar') }">
            <div class="tile-content">
                <div class="img icon-file-excel"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Arquivos</span>
            </div>
        </div>
        -->
<!--         <div class="tile icon bg-color-gray"
             data-bind="click: function(data, event) {
             page('<?=$this->webroot . $this->params['prefix'] ?>/contratos/listar') }">
            <div class="tile-content">
                <div class="img icon-paper-2"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Contratos</span>
            </div>
        </div> -->
        <div class="tile icon bg-color-green"
            data-bind="click: function(data, event) {
            page('/enquetes/nao_respondidas') }">
            <div class="tile-content">
                <div class="img icon-newspaper"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Enquetes</span>
            </div>
        </div>
    </div>
</div>