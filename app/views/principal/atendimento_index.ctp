<?php $session->flash(); ?>

<div class="span12 metro" style="position:relative" id="content-container">
    <div class="row-fluid content-loader hide" id="content-loading">
        <div class="cycle-loader"></div>
    </div>
    <div class="row-fluid content-loader hide" id="content-body"></div>
    <div class="row-fluid content-loader" id="content-home">
        <?php if(!empty($turmaLogada)){ ?>
        <div class="tile icon bg-color-greenLight"
             data-bind="click: function(data, event) {
             page('<?= $this->webroot . $this->params['prefix'] ?>/relatorio_financeiro/exibir') }">
            <div class="tile-content">
                <span class="img icon-calculate"></span>
            </div>
            <div class="brand"><span class="name">Relatório Financeiro</span></div>
        </div>
        <div class="tile icon bg-color-darken"
             data-bind="click: function(data, event) {
             page('<?=$this->webroot . $this->params['prefix'] ?>/formandos/lista_excel') }">
            <div class="tile-content">
                <div class="img icon-users"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Formandos</span>
            </div>
        </div>
        <div class="tile icon bg-color-blueDark"
             data-bind="click: function(data, event) {
             page('<?=$this->webroot . $this->params['prefix'] ?>/campanhas/listar') }">
            <div class="tile-content">
                <div class="img icon-folder-3"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Campanhas</span>
            </div>
        </div>
        <a href="<?=$this->webroot . $this->params['prefix'] ?>/mapa_mesas" target="_blank" >
            <div class="tile icon bg-color-purple">
                <div class="tile-content">
                    <div class="img icon-map"></div>
                </div>
                <div class="brand">
                    <div class="metro-badge"></div>
                    <span class="name">Mapa de Mesa</span>
                </div>
            </div>
        </a>
        <div class="tile icon bg-color-black"
             data-bind="click: function(data, event) {
             page('<?= $this->webroot . $this->params['prefix'] ?>/checkout/total_checkout/<?=$turmaLogada['Turma']['id']?>') }">
            <div class="tile-content">
                <span class="img icon-bell-3"></span>
            </div>
            <div class="brand"><span class="name">Relatório de Entrega</span></div>
        </div>
        <?php }else{ ?>
        <div class="tile icon bg-color-blue"
             data-bind="click: function(data, event) {
             page('<?=$this->webroot . $this->params['prefix'] ?>/formandos/listar') }">
            <div class="tile-content">
                <div class="img icon-users"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Formandos</span>
            </div>
        </div>
        <div class="tile icon bg-color-red"
             data-bind="click: function(data, event) {
             page('/turmas/listar') }">
            <div class="tile-content">
                <span class="img icon-network"></span>
            </div>
            <div class="brand"><span class="name">Turmas Gerenciadas</span></div>
        </div>
        <div class="tile icon bg-color-pink"
             data-bind="click: function(data, event) {
             page('/calendario/exibir') }">
            <div class="tile-content">
                <div class="img icon-calendar"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Agenda</span>
            </div>
        </div>
        <div class="tile icon bg-color-orangeDark"
             data-bind="click: function(data, event) {
             page('<?=$this->webroot . $this->params['prefix'] ?>/usuarios/listar') }">
            <div class="tile-content">
                <div class="img icon-users"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Todos Formandos</span>
            </div>
        </div>
        <?php } ?>
        <div class="tile icon bg-color-greenDark"
             data-bind="click: function(data, event) {
             page('/mensagens/email') }">
            <div class="tile-content">
                <span class="img icon-mail-3"></span>
            </div>
            <div class="brand"><span class="name">Mensagens</span></div>
        </div>
        <div class="tile icon bg-color-brown"
             data-bind="click: function(data, event) {
             page('<?=$this->webroot . $this->params['prefix'] ?>/formandos/devolucao') }">
            <div class="tile-content">
                <span class="img icon-battery-2"></span>
            </div>
            <div class="brand"><span class="name">Devolução Cancelamento</span></div>
        </div>
        <div class="tile icon app bg-color-darken"
             data-bind="click: function(data, event) {
             page('<?= $this->webroot . $this->params['prefix'] ?>/formandos/boletos') }">
            <div class="tile-content">
                <span class="img icon-printer"></span>
            </div>
            <div class="brand"><span class="name">Gerar Boletos</span></div>
        </div>
        <div class="tile icon bg-color-orangeDark"
             data-bind="click: function(data, event) {
             page('/turmas/ficha_evento') }">
            <div class="tile-content">
                <div class="img icon-drawer-2"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Ficha Evento</span>
            </div>
        </div>
        <div class="tile icon bg-color-pink"
             data-bind="click: function(data, event) {
             page('turmas/analise_temporada') }">
            <div class="tile-content">
                <span class="img icon-amazon-2"></span>
            </div>
            <div class="brand"><span class="name">Análise Temporada</span></div>
        </div>
    </div>
</div>
