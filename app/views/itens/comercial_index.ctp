<?php $paginator->options(array('url' => array_merge(array($this->params['prefix'] => true), $this->passedArgs))); ?>
<span id="conteudo-titulo" class="box-com-titulo-header">Selecionar item</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<div class="container-tabela">
		<table>
			<thead>
				<tr>
					<th scope="col"><?php echo $paginator->sort('Nome', 'nome'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Grupo', 'grupo'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Descrição', 'descricao'); ?></th>
					<th scope="col"> &nbsp;</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="3"><?php echo $paginator->counter(array('format' => 'Itens %start% ao %end% - página %page% de %pages%')); ?>
						<span class="paginacao">
							<?php echo $paginator->numbers(array('separator' => ' ')); ?>
						</span>
					</td>
					<td colspan="1"><?php echo $paginator->counter(array('format' => 'Total : %count% ' .  $this->name)); ?></td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach($itens as $item): ?>
				<?php if($isOdd):?>
					<tr class="odd">
				<?php else:?>
					<tr>
				<?php endif;?>
					<td colspan="1"><?php echo $item['Item']['nome'];?></td>
					<td colspan="1"><?php echo $item['Item']['grupo'];?></td>
					<td colspan="1"><?php echo $item['Item']['descricao'];?></td>
					<td colspan="1"><?php echo $html->link('Entrar', array($this->params['prefix'] => true, 'controller' => 'itens', 'action' =>'visualizar', $item['Item']['id']), array('class' => 'submit button')); ?></td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>