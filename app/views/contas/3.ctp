<span id="conteudo-titulo" class="box-com-titulo-header">Contas Banc&aacute;rias</span>
<div id="conteudo-container">
    <?php $session->flash(); ?>
    <div class="tabela-adicionar-item">
        <div style="clear:both;"></div>
    </div>
    <?php if(count($turmas) > 0) : ?>
    <?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
    <div class="container-tabela">
        <table>
            <thead>
                <tr>
                    <th scope="col" width="5%"><?php echo $paginator->sort('ID', 'id'); ?></th>
                    <th scope="col" width="30%"><?php echo $paginator->sort('Nome', 'nome'); ?></th>
                    <th scope="col" width="20%"><?php echo $paginator->sort('Banco', 'nome_configuracao'); ?></th>
                    <th scope="col" width="10%"><?php echo $paginator->sort('Agencia', 'agencia'); ?></th>
                    <th scope="col" width="35%">Alterar</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="3"><?php echo $paginator->counter(array('format' => 'Itens %start% ao %end% - página %page% de %pages%')); ?>
                        <span class="paginacao">
                            <?php echo $paginator->numbers(array('separator' => ' ')); ?>
                        </span>
                    </td>
                    <td colspan="2"><?php echo $paginator->counter(array('format' => 'Total : %count% ' . $this->name)); ?></td>
                </tr>
            </tfoot>
            <tbody>
                <?php $isOdd = false; ?>
                <?php foreach($turmas as $turma): ?>
                    <?php if ($isOdd): ?>
                        <tr class="odd">
                        <?php else: ?>
                        <tr>
                        <?php endif; ?>
                        <td  colspan="1"><?php echo $turma['Turma']['id']; ?></td>
                        <td  colspan="1"><?php echo $turma['Turma']['nome']; ?></td>
                        <td  colspan="1"><?php echo $turma['Banco']['nome']; ?></td>
                        <td  colspan="1"><?php echo $turma['BancoConta']['agencia']; ?></td>
                        <td  colspan="1">
                            <?php echo $form->create('Turma', array('url' => "/{$this->params['prefix']}/contas/turmas_atualizar")); ?>
                            <?php echo $form->hidden('id',array('value' => $turma['Turma']['id'])); ?>
                            <?php echo $form->input('banco_conta_id',array(
                                'type' => 'select',
                                'options' => $contas,
                                'value' => $turma['BancoConta']['id'],
                                'label' => false,
                                'div' => false,
                                'error' => false
                            )); ?>
                            <?php echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'submit')); ?>
                        </td>
                    </tr>
                    <?php $isOdd = !($isOdd); ?>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <?php else : ?>
    <h2 style="color:red">Nenhuma conta cadastrada</h2>
    <?php endif; ?>
</div>