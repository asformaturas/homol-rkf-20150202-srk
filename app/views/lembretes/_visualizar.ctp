
<span id="conteudo-titulo" class="box-com-titulo-header">Lembrete</span>
<div id="conteudo-container">

	<?php $session->flash(); ?>
	<div class="detalhes">
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Título</label>
			<span class="grid_8 alpha"><?php echo $lembrete['Lembrete']['titulo']; ?></span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Texto</label>
			<span class="grid_8 alpha"><?php echo $lembrete['Lembrete']['texto']; ?></span>
		</p>
		
		<p class="grid_11 alpha omega">
			<?php echo $html->link('Voltar',array($this->params['prefix'] => true,'controller' => 'principal', 'action' => 'index') ,array('class' => 'cancel')); ?>
			<?php echo $html->link('Editar', array($this->params['prefix'] => true,'controller' => 'lembretes', 'action' => 'editar', $lembrete['Lembrete']['id']), array('class' => 'submit ')); ?>
			<?php echo $html->link('Adicionar Outro', array($this->params['prefix'] => true,'controller' => 'Lembretes', 'action' => 'adicionar'), array('class' => 'submit ')); ?>
		</p>
	</div>
</div>