<?php

/*
	@author Vin�cius Alves Oyama
	Helper utilizado para o numero de assuntos pendentes em cada item
*/

class SidebarItensHelper extends AppHelper {
	var $helpers = array('Html','Javascript');

    function exibir($tituloSidebar, $arrayItensSidebar) {

		if ($this->possuiItensParaExibir($arrayItensSidebar)) {
			return $this->gerarHTML($tituloSidebar, $arrayItensSidebar);
		 }
		return "";
    }
	
	private function possuiItensParaExibir($arrayItens) {
		return sizeof($arrayItens) > 0;
	}
	
	private function gerarHTML($tituloSidebar, $arrayItensSidebar) {
		//N�mero m�ximo de itens do cronograma que aparecem na sidebar recolhido
		$limite_sidebar_itens = 10;
		
		$HTMLsidebar = "";
		
		
		$HTMLsidebar .= 
		"<span id='meuteste1'>
			<span id='menu-lateral-titulo' class='box-com-titulo-header'> \n
			{$tituloSidebar} \n
		</span>
		</span> \n";
		
		$HTMLsidebar .= "<ul class='grid_full alpha omega'> \n";
		
		$HTMLsidebar_extendido = "";
		
		$HTMLsidebar_extendido .= 
		"<span id='menu-lateral-titulo' class='box-com-titulo-header'> \n
			{$tituloSidebar} \n
		</span> \n";
		
		$HTMLsidebar_extendido .= "<ul class='grid_full alpha omega'> \n";
		
		$adicionados = 0;
		
		foreach($arrayItensSidebar as $item) { 
			

			if($item['quantidadeDeAssuntosPentendentes'] > 0) {
				$HTMLsidebar .= $this->gerarHTMLdoItem($item);
				$HTMLsidebar_extendido .= $this->gerarHTMLdoItem($item);
				$adicionados++;
			} else {
				$HTMLsidebar_extendido .= $this->gerarHTMLdoItem($item);
			}
		}
		
		//Botao de ver mais
		//$HTMLsidebar .= "<li>". "<a href='javascript:teste()'>teste</a>" . "</li";
		if ($adicionados == 0)
			$HTMLsidebar .= "N&atilde;o existem itens pendentes.\n";
			
		$HTMLsidebar .= "</ul> \n";

		return $HTMLsidebar;
	}
	
	private function gerarHTMLdoItem($item) {
	
		$htmlItem = "<li> \n";
		
		$htmlItem .= $this->Html->link(
					$item['nome'] . "<span class='menu-lateral-numero'>" . $item['quantidadeDeAssuntosPentendentes'] . "</span>",
					$item['link'], array(), false, false);
		$htmlItem .= "</li> \n";
		
		return $htmlItem;
	}
}

?>
