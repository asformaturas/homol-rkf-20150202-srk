<?php

/**
 * Monta a tabela de agendamentos
 *
 * @author Alexandre
 */
class ListaAgendaHelper extends AppHelper {

    var $helpers = array('Html', 'Form', 'Text', 'Datas');

    function html($data = '', $detail_controller='agendas' ,$detail_action = 'evento') {

        $month_list = array('Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro');

        $output = '';
        $output .= '<table class="lista-agenda">';
        $output .= '<thead><th>&nbsp</th>';
        $output .= '<th>Horário</th>';
        $output .= '<th>Título</th>';
        $output .= '<th>Local</th>';
        $output .= '<th>Criador</th>';
        $output .= '<th>&nbsp;</th>';
        $output .= '<th>&nbsp</th></thead>';

        $hasElement = false;
        foreach ($data as $dayEvent) {
            $hasElement = true;
            $agenda = $dayEvent[0]['Agenda'];
            $date =  $this->Datas->create_date_time_from_format('Y-m-d H:i:s', $agenda['data']);
            $day = date_format($date, 'd');
            $month = date_format($date, 'm');
            $year = date_format($date, 'Y');
            
            $output .= '<tr class="line-date"><td>&nbsp</td>';
            $output .= '<td colspan="4">';
            $output .= $day . ' de ' . $month_list[intval($month) - 1] . ' de ' . $year;
            $output .= '</td><td>&nbsp</td></tr>';

            $timeOrganized = array();
            foreach ($dayEvent as $evento) {
                $agenda = $evento['Agenda'];
               	$time =  $this->Datas->create_date_time_from_format('Y-m-d H:i:s', $agenda['data']);
                $timeOrganized[date_format($time, 'Hi')][] = $evento;
            }
            ksort($timeOrganized);

            foreach ($timeOrganized as $timeEvents) {
                foreach ($timeEvents as $evento) {
                    $agenda = $evento['Agenda'];
                    $criador = $evento['Criador'];
                    $time =  $this->Datas->create_date_time_from_format('Y-m-d H:i:s', $agenda['data']);

                    $output .= '<tr><td>&nbsp</td>';
                    $output .= '<td class="cell-time">' . date_format($time, 'H:i') . '</td>';
                    $output .= '<td class="cell-title">' . $this->Html->link($agenda['titulo'], array($this->params['prefix'] => true, 'controller' => $detail_controller, 'action' => $detail_action, $agenda['id'])) . '</td>';
                    $output .= '<td class="cell-local">' . $agenda['local'] . '</td>';
                    $output .= '<td class="cell-creator">' . $criador['nome'] . '</td>';
                    $output .= '</td><td>&nbsp</td></tr>';
                }
            }
        }
        if (!$hasElement) {
            $output .= '<tr><td colspan="6">Não há agendamentos...</td></tr>';
        }
        $output .= '</table>';

        return $output;
    }

}

?>
