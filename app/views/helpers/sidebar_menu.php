<?php

/*
	@author Vinicius Alves Oyama
	Helper utilizado para mostrar o sidebar menu nas views
*/

class SidebarMenuHelper extends AppHelper {
	var $helpers = array('Html');

    function exibir($tituloSidebar, $arrayLinksSidebar, $nomeControllerAtual, $nomeActionAtual = '', $arraySublinksSidebar) {
		$HTMLsidebar = "";
		$HTMLsidebar .= 
		"<span id='menu-lateral-titulo' class='box-com-titulo-header'> \n
			{$tituloSidebar} \n
		</span> \n";
		
		$HTMLsidebar .= "<ul class='grid_full alpha omega'> \n";

		foreach($arrayLinksSidebar as $linkSidebarMenu) { 
			$HTMLsidebar .= "<li> \n";
			
			// Verificar se o item � o selecionado no momento
			if ( strtolower($linkSidebarMenu['controller']) == strtolower($nomeControllerAtual) && 
					( $nomeActionAtual == '' ||
					strtolower($linkSidebarMenu['grupo'] . '_' . $linkSidebarMenu['action']) == strtolower($nomeActionAtual) )) {
				// Esta selecionado
				$HTMLsidebar .= $this->Html->link(
					utf8_decode($linkSidebarMenu['texto']),
					array($linkSidebarMenu['grupo'] => true, 
						  'controller' => $linkSidebarMenu['controller'], 
						  'action' => $linkSidebarMenu['action']),
					array('class' => "menu-lateral-selecionado {$linkSidebarMenu['class']}")
				);
				$HTMLsidebar .= !empty($linkSidebarMenu['after']) ? $linkSidebarMenu['after'] : "";
			} else {
				// Nao esta selecionado
				$HTMLsidebar .= $this->Html->link(
					utf8_decode($linkSidebarMenu['texto']),
					array($linkSidebarMenu['grupo'] => true,
					 	  'controller' => $linkSidebarMenu['controller'], 
						  'action' => $linkSidebarMenu['action']),
					(!empty($linkSidebarMenu['class']) ? array('class' => $linkSidebarMenu['class']) : "")
				);
				$HTMLsidebar .= !empty($linkSidebarMenu['after']) ? $linkSidebarMenu['after'] : "";
			}
			
			
			// Verificar se possui sublinks
			if($arraySublinksSidebar) { 
				if(array_key_exists(utf8_decode($linkSidebarMenu['texto']), $arraySublinksSidebar)) {
					$HTMLsidebar .= "<ul class='sublinks'>";
					$sublinks = $arraySublinksSidebar[utf8_decode($linkSidebarMenu['texto'])];
					
					foreach ($sublinks as $sublink) {
						// Verificar se está selecionado
						$sublink_controller = strtolower($sublink['controller']);
						$sublink_action = strtolower($sublink['grupo'] . '_' . $sublink['action']);
						if( $sublink_controller	 == strtolower($nomeControllerAtual) && 
							$sublink_action == strtolower($nomeActionAtual)
							) {
							
							$options = array('class' => 'menu-lateral-selecionado');		
						} else {
							$options = array('class' => 'teste');
						}
						
							
						$HTMLsidebar .= "<li>";
						$HTMLsidebar .= $this->Html->link(
							$sublink['texto'],
							array($sublink['grupo'] => true, 
						  		'controller' => $sublink['controller'], 
						  		'action' => $sublink['action']),
								$options
						);
						$HTMLsidebar .= "</li>"; 	
					}
					$HTMLsidebar .= "</ul>";
					
				} 
			}

			$HTMLsidebar .= "</li>\n";
			
		}
		
		$HTMLsidebar .= "</ul> \n";
			
		return $HTMLsidebar;
    }
    
    function novo($arrayLinksSidebar, $nomeControllerAtual, $nomeActionAtual = '',
            $arraySublinksSidebar) {
    	$HTMLsidebar = "";
    	$HTMLsidebar .= "<ul class='dropdown-menu' role='menu'> \n";
    	 
    	foreach($arrayLinksSidebar as $linkSidebarMenu) {
            if($linkSidebarMenu['novoLayout']) {
                $link = $this->Html->link(
                    utf8_decode($linkSidebarMenu['texto']),
                    array(
                        $linkSidebarMenu['grupo'] => true,
                        'controller' => $linkSidebarMenu['controller'],
                        'action' => $linkSidebarMenu['action']
                    ),
                    array(
                        'escape' => false,
                        'class' => (!empty($linkSidebarMenu['class']) ? $linkSidebarMenu['class'] : ""),
                        'data-bind' => 'click: loadThis'
                    )
                );
            } else {
                $link = $this->Html->link(
                    utf8_decode($linkSidebarMenu['texto']),
                    array(
                        $linkSidebarMenu['grupo'] => true,
                        'controller' => $linkSidebarMenu['controller'],
                        'action' => $linkSidebarMenu['action']
                    ),
                    array(
                        'escape' => false,
                        'class' => (!empty($linkSidebarMenu['class']) ? $linkSidebarMenu['class'] : "")
                    )
                );
            }
            if (strtolower($linkSidebarMenu['controller']) == strtolower($nomeControllerAtual)) {
                if($arraySublinksSidebar && array_key_exists(utf8_decode($linkSidebarMenu['texto']), $arraySublinksSidebar))
                    $HTMLsidebar .= "<li class='active'> \n";
    		else
                    $HTMLsidebar .= "<li> \n";
    		$HTMLsidebar .= $link;
                $HTMLsidebar .= !empty($linkSidebarMenu['after']) ? $linkSidebarMenu['after'] : "";
            } else {
                if($arraySublinksSidebar &&
                        array_key_exists(utf8_decode($linkSidebarMenu['texto']), $arraySublinksSidebar))
                    $HTMLsidebar .= "<li class='dropdown-submenu'> \n";
    		else
                    $HTMLsidebar .= "<li> \n";
    		$HTMLsidebar .= $link;
                $HTMLsidebar .= !empty($linkSidebarMenu['after']) ? $linkSidebarMenu['after'] : "";
            }
            
            if($arraySublinksSidebar) {
    		if(array_key_exists(utf8_decode($linkSidebarMenu['texto']), $arraySublinksSidebar)) {
                    
                    $HTMLsidebar .= "<ul class='dropdown-menu'>";
                    $sublinks = $arraySublinksSidebar[utf8_decode($linkSidebarMenu['texto'])];
                    foreach ($sublinks as $sublink) {
                        $sublink_controller = strtolower($sublink['controller']);
                        $sublink_action = strtolower($sublink['grupo'] . '_' . $sublink['action']);
                        if($sublink_controller	 == strtolower($nomeControllerAtual) &&
                                $sublink_action == strtolower($nomeActionAtual))
                            $HTMLsidebar .= "<li class='active'>";
                        else
                            $HTMLsidebar .= "<li>";
                        if($sublink['novoLayout']) {
                            $link = $this->Html->link(
                                utf8_decode($sublink['texto']),
                                array(
                                    $sublink['grupo'] => true,
                                    'controller' => $sublink['controller'],
                                    'action' => $sublink['action']
                                ),
                                array(
                                    'data-bind' => 'click: loadThis'
                                )
                            );
                        } else {
                            $link = $this->Html->link(
                            utf8_decode($sublink['texto']),
                                array(
                                    $sublink['grupo'] => true,
                                    'controller' => $sublink['controller'],
                                    'action' => $sublink['action']
                                )
                            );
                        }
                        $HTMLsidebar .= $link;
                        $HTMLsidebar .= "</li>";
                    }
                    $HTMLsidebar .= "</ul>";
                }
            }
            $HTMLsidebar .= "</li>\n";
        }
    		
    	$HTMLsidebar .= "</ul> \n";
    		
    	return $HTMLsidebar;
    }
}

?>
