<?php

/*
  @author Vinícius Alves Oyama
  Helper utilizado para mostrar o topbar menu nas views
 */

class TopbarMenuHelper extends AppHelper {

    var $helpers = array('Html');

    function exibir($arrayLinksTopbar, $nomeControllerAtual) {
        $HTMLtopbar = "";

        $HTMLtopbar .= "<ul> \n";


        foreach ($arrayLinksTopbar as $linkTopbarMenu) {
            $HTMLtopbar .= "<li> \n";
            if (utf8_decode($linkTopbarMenu['texto']) == 'home') {
                if (strtolower($linkTopbarMenu['controller']) == strtolower($nomeControllerAtual))
                    $menuSelecionado = 'class="menu-horizontal-selecionado"';
                else
                    $menuSelecionado = '';
                $HTMLtopbar .= '<a href="' . $this->webroot . $linkTopbarMenu['grupo'] . '/' .
                        $linkTopbarMenu['controller'] . '/' .
                        $linkTopbarMenu['action'] . '" id="home" ' . $menuSelecionado . '>' .
                        '<img src="' . $this->webroot . 'img/home.png"></a>';
            } else if (strtolower($linkTopbarMenu['controller']) == strtolower($nomeControllerAtual))
                $HTMLtopbar .= $this->Html->link(utf8_decode($linkTopbarMenu['texto']), array($linkTopbarMenu['grupo'] => true,
                    'controller' => $linkTopbarMenu['controller'],
                    'action' => $linkTopbarMenu['action']), array('class' => 'menu-horizontal-selecionado'));
            else
                $HTMLtopbar .= $this->Html->link(utf8_decode($linkTopbarMenu['texto']), array($linkTopbarMenu['grupo'] => true,
                    'controller' => $linkTopbarMenu['controller'],
                    'action' => $linkTopbarMenu['action']));
            $HTMLtopbar .= "</li>\n";
        }

        $HTMLtopbar .= "</ul> \n";
        return $HTMLtopbar;
    }

    function novo($arrayLinksTopbar) {
        $HTMLtopbar = "";

        $HTMLtopbar .= "<ul class='nav'> \n";


        foreach ($arrayLinksTopbar as $linkTopbarMenu) {
            $HTMLtopbar .= "<li> \n";
                if($linkTopbarMenu['novoLayout']) {
                    if (utf8_decode($linkTopbarMenu['texto']) == 'Home') {
                        $HTMLtopbar .= '<a href="#" data-bind="click: function() { hideHomeButton() }">Home</a>';
                    } else {
                        $a = $this->Html->url(array($linkTopbarMenu['grupo'] => true,
                            'controller' => $linkTopbarMenu['controller'],
                            'action' => $linkTopbarMenu['action']),true);
                        $HTMLtopbar .= $this->Html->link(utf8_decode($linkTopbarMenu['texto']),
                            array(
                                $linkTopbarMenu['grupo'] => true,
                                'controller' => $linkTopbarMenu['controller'],
                                'action' => $linkTopbarMenu['action']
                            ),
                            array('data-bind' => 'click: loadThis'));
                    }
                } else {
                    $HTMLtopbar .= $this->Html->link(utf8_decode($linkTopbarMenu['texto']), array($linkTopbarMenu['grupo'] => true,
                        'controller' => $linkTopbarMenu['controller'],
                        'action' => $linkTopbarMenu['action']));
                }
            $HTMLtopbar .= "</li>\n";
        }

        $HTMLtopbar .= "</ul> \n";
        return $HTMLtopbar;
    }

}

?>
