<span id="conteudo-titulo" class="box-com-titulo-header"><?php echo $item['Item']['nome'] . ' - Novo assunto';?></span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<?php echo $form->create('Assunto', array('url' => "/{$this->params['prefix']}/assuntos/adicionar/".$item['Item']['id'])); ?>
		
		<?php include('_form.ctp'); ?>
		
	<p class="grid_11 alpha omega">
		<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'controller' => 'itens', 'action' => 'visualizar', $item['Item']['id']) ,array('class' => 'cancel')); ?>
		<?php echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'submit'));?>
	</p>
</div>