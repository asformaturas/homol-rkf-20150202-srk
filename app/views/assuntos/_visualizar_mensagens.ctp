<?php $paginator->options(array('url' => array_merge(array($this->params['prefix'] => true), $this->passedArgs))); ?>
<span id="conteudo-titulo" class="box-com-titulo-header">
	<?php echo "".$assunto['Item']['nome'] . ' -> ' . $assunto['Assunto']['nome'];?>
</span>
<div id="conteudo-container">
	
	<?php $session->flash(); ?>
	<div class="tabela-adicionar-item">
		<?php echo $form->create('Mensagem', array('url' => "/{$this->params['prefix']}/assuntos/procurar", 'class' => 'procurar-form-inline')) ?>
		<?php echo $form->input('chave', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
		<?php echo $form->hidden('assunto_id', array('value' => $assunto['Assunto']['id']));?>
		<?php echo $form->end(array('label' => ' ', 'div' => false, 'class' => 'submit-busca')) ?>		
		<?php
			if($assunto['Assunto']['resolvido']) {
				if( in_array($this->params['prefix'], array('planejamento','comercial'))) {
					echo $html->link('Reabrir assunto', array($this->params['prefix'] => true, 'controller' => 'assuntos', 'action' => 'reabrir', $assunto['Assunto']['id']), null, 'Deseja reabrir este assunto?');	
				} 
			} else {
				echo $html->link('Nova mensagem', array($this->params['prefix'] => true, 'controller' => 'mensagens', 'action' => 'adicionar', $assunto['Assunto']['id']));
			} 
		?>
		<div style="clear:both;"></div>
	</div>
	<div class="forum-container">	
			<?php $isOdd = false; ?>
			<?php foreach($mensagens as $mensagem): ?>
				<div class="grid_full first forum-mensagem alpha">
					<p class="grid_full alpha forum-mensagem-data">
						<?php echo date('d/m/Y',strtotime($mensagem['Mensagem']['data']));?>
						<span>Mensagem id: <?php echo $mensagem['Mensagem']['id'];?></span>
					</p>
					<div class="grid_3 forum-mensagem-dados">
						<p class="forum-mensagem-dados-nome-usuario"><?php if(isset($mensagem['Usuario'])){echo $mensagem['Usuario']['nome'];} else {echo '-';}?></p>
						<p><b>Pend&ecirc;ncia: </b><?php echo $mensagem['Assunto']['pendencia'];?></p>
						<p><b>Resolvido: </b><?php echo ($mensagem['Assunto']['resolvido'] == 1 ? "Sim" : "Não");?></p>
					</div>
					<div class="grid_12 forum-mensagem-texto mensagem">
						<p class="grid_full alpha omega"><?php echo html_entity_decode($mensagem['Mensagem']['texto']);?></p>
							<?php if (!empty($mensagem['Arquivo']) && count($mensagem['Arquivo']) > 0): ?>
								<div class="grid_full first alpha omega visualizar-arquivos-anexados">
									<label class="grid_5 alpha">Arquivos Anexados</label>
									<ul>
										<?php foreach ($mensagem['Arquivo'] as $arquivo): ?>

						 				<?php
						 					$tamanho = $arquivo['tamanho'];
											if ($tamanho == 1024) {
												$tamanho = round(($tamanho/1024)) . " Kilobyte";
											}	else if ($tamanho > 1024 && $tamanho < 1024*1024) {
												$tamanho = round(($tamanho/1024)) . " Kilobytes";
											} else if ($tamanho == 1024*1024) {
													$tamanho = round((($tamanho/1024)/1024)) . " Megabyte";
											} else if ($tamanho > 1024*1024) {
												$tamanho = round((($tamanho/1024)/1024), 1) . " Megabytes";
											} else {
												$tamanho = $tamanho . " Bytes";
											}
											?>

											<li>
												<?php echo $html->link($arquivo['nome']." (".$tamanho.")", 
																	array($this->params['prefix'] => true, 
																				'controller' => 'mensagens', 
																				'action' => 'baixar_anexo', 
																				$arquivo['id'])); ?>
											</li>
										<?php endforeach; ?>
									</ul>
								</div>
							<?php endif ?>
					</div>				
				</div>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>

		<p class="grid_full"><?php echo $paginator->counter(array('format' => 'Mensagens %start% à %end% - página %page% de %pages%')); ?>
			<span class="paginacao">
				<?php echo $paginator->numbers(array('separator' => ' ')); ?>
			</span>
		</p>
		<p class="grid_full"><?php echo $paginator->counter(array('format' => 'Total : %count% mensagens')); ?></p>
		<p class="grid_16"><?php echo $html->link('Voltar', array($this->params['prefix'] => true, 'controller' => 'itens', 'action' => 'visualizar', $assunto['Item']['id']),array('class' => 'cancel')); ?> </p>
	</div>
</div>