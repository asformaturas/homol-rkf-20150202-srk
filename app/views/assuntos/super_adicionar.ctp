<span id="conteudo-titulo" class="box-com-titulo-header">Assunto - Adicionar</span>
<div id="conteudo-container">
	<?php echo $form->create('Assunto', array('url' => "/{$this->params['prefix']}/assuntos/adicionar/".$item['Item']['id'])); ?>
		<?php include	('_form.ctp'); ?>
	
	<p class="grid_11 alpha omega">
		<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'action' => 'index') ,array('class' => 'cancel')); ?>
		<?php echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'submit'));?>
	</p>
</div>