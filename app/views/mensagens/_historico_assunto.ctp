<div class="row-fluid">
    <?php if(count($mensagens) > 0) : ?>
    <?php foreach($mensagens as $mensagem) : ?>
    <h3>
        <?php if(!empty($mensagem['Usuario']['diretorio_foto_perfil'])) : ?>
        <img class="pull-left" src='<?="{$this->webroot}{$mensagem['Usuario']['diretorio_foto_perfil']}"?>' width="50" />
        <?php else : ?>
        <img class="pull-left" src='<?="/img/"?>uknown_user.gif' width="50" />
        <?php endif; ?>
        <i class="fg-color-red" style="margin-left:10px">
            <?=$mensagem['Usuario']['nome']?>
            <em class="fg-color-gray" style="font-size: 14px"> - (<?=$mensagem['Usuario']['grupo']?>)</em>
        </i>
        <small class="pull-right fg-color-red">
            <?=date('d/m/Y', strtotime($mensagem['Mensagem']['data']));?>
        </small>
    </h3>
    <br />
    <div style="margin-left:60px">
        <?=html_entity_decode($mensagem['Mensagem']['texto'])?>
    </div>
    <hr style="border-style: solid" />
    <br />
    <?php endforeach; ?>
    <?php else : ?>
    <h2 class="fg-color-red">Nenhuma mensagem encontrada</h2>
    <?php endif; ?>
</div>