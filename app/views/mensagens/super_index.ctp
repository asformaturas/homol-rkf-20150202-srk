<span id="conteudo-titulo" class="box-com-titulo-header">Mensagens</span>
<div id="conteudo-container">
	<?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
	<?php $session->flash(); ?>
	<div class="tabela-adicionar-item">
		<?php echo $form->create('Mensagens', array('url' => "/{$this->params['prefix']}/mensagens/procurar", 'class' => 'procurar-form-inline')) ?>
		<?php echo $form->input('chave', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
		<?php echo $form->end(array('label' => ' ', 'div' => false, 'class' => 'submit-busca')) ?>
		<div style="clear:both;"></div>
	</div>
	<div class="container-tabela">
		<table>
			<thead>
				<tr>
					<th scope="col"><?php echo $paginator->sort('Id', 'id'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Área', 'area'); ?></th>
					<th scope="col"><?php echo 'Caráter'; ?></th>
					<th scope="col"><?php echo 'Resolvido'; ?></th>
					<th scope="col"><?php echo 'Tópico'; ?></th>
					<th scope="col"><?php echo $paginator->sort('Assunto', 'nome'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Descrição', 'descricao'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Lido?', 'lido'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Data', 'data'); ?></th>
					<th scope="col"> &nbsp;</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="7"><?php echo $paginator->counter(array('format' => 'Itens %start% ao %end% - página %page% de %pages%')); ?>
						<span class="paginacao">
							<?php echo $paginator->numbers(array('separator' => ' ')); ?>
						</span>
					</td>
					<td colspan="1"><?php echo $paginator->counter(array('format' => 'Total : %count% ' .  $this->name)); ?></td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach($mensagens as $mensagem): ?>
				<?php if($isOdd):?>
					<tr class="odd">
				<?php else:?>
					<tr>
				<?php endif;?>
					<td colspan="1"><?php echo $mensagem['Mensagem']['id'];?></td>
					<td colspan="1"><?php echo $mensagem['Assunto']['Item']['grupo'];?></td>
					<td colspan="1"><?php echo $mensagem['Assunto']['pendencia'];?></td>
					<td colspan="1"><?php echo $mensagem['Assunto']['resolvido'];?></td>
					<td colspan="1"><?php echo $mensagem['Assunto']['Item']['nome'];?></td>
					<td colspan="1"><?php echo $mensagem['Assunto']['nome'];?></td>
					<td colspan="1"><?php echo $text->truncate($mensagem['Mensagem']['texto'], 30, array('ending' => '...', 'exact' => true, 'html' => false));?></td>
					<td colspan="1"><?php echo $mensagem['Mensagem']['lido'];?></td>
					<td colspan="1"><?php echo $mensagem['Mensagem']['data'];?></td>
					<td colspan="1"><?php echo $html->link('Visualizar', array($this->params['prefix'] => true, 'controller' => 'mensagens', 'action' =>'visualizar', $mensagem['Mensagem']['id']), array('class' => 'submit button')); ?></td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
		</table>
	</div>
</div>