<?=$html->css('uniform.default')?>
<?=$html->css('redactor')?>
<?=$html->css('mensagens')?>
<?=$html->script('jquery.uniform')?>
<?=$html->script('knockout-2.2.0')?>
<?=$html->script('redactor.min')?>
<?=$html->script('jquery.ba-replacetext')?>
<?=$html->script('mensagens.ko')?>
<script type="text/html" id="tmpMensagens">
	<div class="accordion-group" data-bind="attr: { id: 'group' + $root.tipo() + mensagem.Mensagem.id }">
		<div class="accordion-heading hm">
			<div class="c check">
				<input type="checkbox" class="uniform checkable" data-bind="checked: mensagem.selecionada, click: $root.verificarSelecao" />
			</div>
			<div class="c favorite" data-bind="css: { off : mensagem.MensagemUsuario.favorita() == 0 },
				click: function() { $parent.toggleFavorita($data) }"></div>
			<div class="c pendencia">
				<i rel="tooltip" data-placement="top" data-bind="css: {
						'iconic-user': mensagem.Mensagem.Assunto.pendencia == 'Comissao' && mensagem.Mensagem.Assunto.resolvido == 0,
						'iconic-info': mensagem.Mensagem.Assunto.pendencia == 'Informativo',
						'iconic-check green': mensagem.Mensagem.Assunto.resolvido == 1 && mensagem.Mensagem.Assunto.pendencia != 'Informativo',
						'as': mensagem.Mensagem.Assunto.pendencia == 'As' },
						attr: {
							title: mensagem.Mensagem.Assunto.resolvido == 1 && mensagem.Mensagem.Assunto.pendencia != 'Informativo' ?
								'Resolvido' : mensagem.Mensagem.Assunto.pendencia }"></i>
			</div>
			<div class="c title" data-bind="css: { 'nao-lida' : mensagem.MensagemUsuario.lida() == 0 }">
				<div class="t nome" data-bind="html: mensagem.usuario(),
					attr: { 'data-parent' : '#accordionpane'+$root.tipo(), href : '#collapse' + $root.tipo() + mensagem.Mensagem.id },
					click: function() { $parent.getMessage(mensagem,false) },
					style: { 'text-decoration': mensagem.MensagemUsuario.descartada() == 1 ? 'line-through' : 'none' }"
					data-toggle="collapse"></div>
				<div class="t assunto" data-bind="html: mensagem.assunto() + ' - ' + mensagem.item(),
					attr: { 'data-parent' : '#accordionpane'+$root.tipo(), href : '#collapse' + $root.tipo() + mensagem.Mensagem.id },
					click: function() { $parent.getMessage(mensagem) },
					style: { 'text-decoration': mensagem.MensagemUsuario.descartada() == 1 ? 'line-through' : 'none' }"
					data-toggle="collapse"></div>
				<div class="t data" data-bind="html: mensagem.Mensagem.data.split(' ')[0].split('-').reverse().join('/'),
					attr: { 'data-parent' : '#accordionpane'+$root.tipo(), href : '#collapse' + $root.tipo() + mensagem.Mensagem.id },
					click: function() { console.log($root.tipo()); $parent.getMessage(mensagem) },
					style: { 'text-decoration': mensagem.MensagemUsuario.descartada() == 1 ? 'line-through' : 'none' }"
					data-toggle="collapse"></div>
			</div>
		</div>
		<div data-bind="attr: { id: 'collapse' + $root.tipo() + mensagem.Mensagem.id }" class='accordion-body collapse'>
			<div class='accordion-inner' data-bind="template: { name: 'tmpMensagemLeitura',
				data: $data, if: mensagem.carregada(), beforeRemove: function() { console.log('removeu') } }">
			</div>
		</div>
	</div>
</script>
<script type="text/html" id="tmpMensagemLeitura">
	<div class="texto">
		<div class="i">
			<button type="button" class="btn-action" data-bind="click: $root.responder">
				<i class="icon icon-edit"></i>
				Responder
			</button>
		</div>
		<div class="i info" data-bind="text:'Enviada &agrave;s ' + mensagem.Mensagem.data.substring(11,16)">
		</div>
		<div class="i corpo" data-bind="html: mensagem.texto()">
		</div>
		<!-- ko if: mensagem.Mensagem.Arquivos.length < 1 -->
		<div class="i anexos text-info" data-bind="text:'Nenhum Arquivo Anexado'">
		</div>
		<!-- /ko -->
		<!-- ko ifnot: mensagem.Mensagem.Arquivos.length < 1 -->
		<div class="i anexos" data-bind="foreach: mensagem.Mensagem.Arquivos">
			<a class='btn-action' data-bind="attr: { href: '/formando/arquivos/baixar/'+id,target: '_blank' },
				html: '<i class=\'icon icon-download\'></i>' + nome">
			</a>
		</div>
		<!-- /ko -->
	</div>
</script>
<script type="text/html" id="tmpMensagemResposta">
	<div class="row-fluid">
		<div class="span1">
			<label class="help-inline">Para:</label>
		</div>
		<div class="span3">
			<input type="text" data-provide="typehead" class="input-small"
				placeholder="Buscar" data-bind="attr: { id: 'busca-' + $data.data.id }" />
		</div>
		<div class="span8 destinos" data-bind="foreach: $data.data.resposta.destinos()">
			<button class="btn btn-mini para" data-bind="html: nome,
				attr: { dir: id },click: $parent.data.resposta.removerDestino">
			</button>
		</div>
	</div>
	<div class="br"></div>
	<div class="row-fluid">
		<div class="span2">
			<label class="help-inline" for="assunto">Assunto:</label>
		</div>
		<div class="span10">
			<input class="input-large" name="data[Mensagem][assunto]" type="text"
				data-bind="value: $data.data.resposta.assunto, valueUpdate: 'afterkeydown',
					attr: { id: 'assunto-' + $data.data.id }" />
		</div>
	</div>
	<div class="row-fluid">
		<textarea name="data[Mensagem][texto]" data-bind="attr: { id: 'texto-' + $data.data.id },
			html: '<br />-----------------------------------<br />' + $data.data.resposta.texto">
		</textarea>
	</div>
	<div class="br"></div>
	<div class="br"></div>
	<div class="row-fluid">
		<div class="span5">
			<button type="button" data-bind="click: function() { $data.app.enviarResposta($data.data) }"
				class="btn btn-primary span12"><i class='icon icon-envelope icon-white'></i>Enviar</button>
		</div>
		<div class="span5 offset2">
			<button type="button" data-bind="click: function() { $data.app.cancelarResposta(); }"
				class="btn btn-danger span12"><i class='icon icon-remove icon-white'></i>Cancelar</button>
		</div>
	</div>
</script>
<script type="text/html" id="tmpPagination">
	<div class="desc" data-bind="text:$root.primeiroItemPaginaAtual() + ' - ' + $root.ultimoItemPaginaAtual() + ' de ' + $root.mensagens().length">
	</div>
	<!-- ko if: $root.mensagens().length > $root.itensPorPagina() -->
	<ul>
		<li data-bind="css: { disabled: 0 == $root.paginaAtual() }">
			<a href="#" data-bind="click: function() { $root.irParaPagina('prev') }">
				<i class="iconic-arrow-left"></i>
			</a>
		</li>
		<li data-bind="css: { disabled: $root.maximoPaginas() == $root.paginaAtual() }">
			<a href="" data-bind="click: function() { $root.irParaPagina('next') }">
				<i class="iconic-arrow-right"></i>
			</a>
		</li>
	</ul>
	<!-- /ko -->
</script>
<div class="row-fluid">
	<div class="titulo-header span12">Mensagens</div>
</div>
<div class="br"></div>
<div class="tabbable tabbable-bordered">
	<ul class="nav nav-tabs" id="nav">
		<?php foreach($tipos as $tipo) : ?>
		<li class="<?=$tipo['active'] ? "active" : ""?>">
			<a href="<?="#{$tipo['id']}"?>" data-toggle="tab" data-bind="click: function() { tipo('<?=$tipo['id']?>') }">
				<?php if(isset($tipo['icon'])) echo "<i class='icon {$tipo['icon']}'></i>"?>
				<?=$tipo['titulo']?>
			</a>
		</li>
		<?php if($tipo['active']) : ?>
		<script type="text/javascript"> var tipo = '<?=$tipo['id']?>'; </script>
		<?php endif; ?>
		<?php endforeach; ?>
	</ul>
	<div class="tab-content tab-mensagens" id="tab">
		<?php foreach($tipos as $tipo) : ?>
		<div class="tab-pane <?=$tipo['active'] ? "active" : ""?>" id="<?=$tipo['id']?>">
			<div class="accordion" id="accordion<?=$tipo['id']?>">
				<div class="tm">
					<div class="row-fluid">
						<div class="span3">
							<input type="text" class="input-medium" placeholder="Buscar"
								data-bind="value:textoBusca, valueUpdate: 'afterkeydown'" />
						</div>
						<div class="span9 pull-right">
							<div class="btn-group">
								<button class="dropdown-toggle btn-action" data-toggle="dropdown"
										data-bind="html: $root.itensPorPagina() + ' Mensagens <i class=iconic-fork></i>'">
								</button>
								<ul class="dropdown-menu dropdown-min" data-bind="foreach: optionsQuantidadeItens">
									<li>
										<a href="#" data-bind="click: function() { $root.selecionarQuantidade($data) }, text: $data + ' Mensagens'"></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="c check">
						<input type="checkbox" class="uniform checkable all" data-bind="checked: todasSelecionadas, click: selecionarTodas" />
					</div>
					<div class="c actions" data-bind="if: selecionadas() > 0">
						<button class="btn-action responder" data-bind="style: { display: selecionadas() == 1 ? 'inline-block' : 'none' },
							click: responderSelecionada">
							<i class="icon icon-arrow-right"></i>Responder
						</button>
						<button class="btn-action encaminhar" data-bind="style: { display: selecionadas() == 1 ? 'none' : 'none' }">
							<i class="icon icon-share"></i>Encaminhar
						</button>
						<div class="btn-group">
							<button class="dropdown-toggle btn-action" data-toggle="dropdown">
								Marcar Como
								<i class="iconic-link"></i>
							</button>
							<ul class="dropdown-menu dropdown-min">
								<!-- ko if: qtdePorStatus('lida') < selecionadas() -->
								<li>
									<a href="#" data-bind="click: function() { alterarStatus('lida',1) },
										text: 'Lida'"></a>
								</li>
								<!-- /ko -->
								<!-- ko if: qtdePorStatus('lida') > 0 -->
								<li>
									<a href="#" data-bind="click: function() { alterarStatus('lida',0) },
										text: 'N&atilde;o Lida'"></a>
								</li>
								<!-- /ko -->
								<!-- ko if: qtdePorStatus('favorita') < selecionadas() -->
								<li>
									<a href="#" data-bind="click: function() { alterarStatus('favorita',1) },
										text: 'Com estrela'"></a>
								</li>
								<!-- /ko -->
								<!-- ko if: qtdePorStatus('favorita') > 0 -->
								<li>
									<a href="#" data-bind="click: function() { alterarStatus('favorita',0) },
										text: 'Sem Estrela'"></a>
								</li>
								<!-- /ko -->
							</ul>
						</div>
						<button class="btn-action apagar"
							data-bind="style: { display: tipo() != 'descartadas' ? 'inline-block' : 'none' },
								click: function() { alterarStatus('descartada',1) }">
							<i class="icon icon-trash"></i>
							Lixeira
						</button>
						<button class="btn-action apagar"
							data-bind="style: { display: tipo() == 'descartadas' ? 'inline-block' : 'none' },
								click: function() { alterarStatus('descartada',0) }">
							<i class="iconic-undo"></i>
							Restaurar
						</button>
					</div>
					<!-- ko if: itensPaginaAtual().length > 0 -->
					<div class="pagination pagination-right paginacao" data-bind="template: { name: 'tmpPagination' }">
					</div>
					<!-- /ko -->
					<div class="c subactions">
						<div class="encaminhar">
							<input placeholder="Para:" type="text" data-provide="typeahead" class="select-encaminhar input-medium" />
						</div>
					</div>
				</div>
				<!-- ko if: itensPaginaAtual().length > 0 -->
				<div class="panel" id="accordionpane<?=$tipo['id']?>"
					data-bind="template: { name: 'tmpMensagens', foreach: itensPaginaAtual,
						afterRender: function() { if(iniciado) carregarUniform(); $('[rel=tooltip]').tooltip() }, if: tipo() == '<?=$tipo['id']?>' }">
				</div>
				<!-- /ko -->
				<!-- ko if: itensPaginaAtual().length < 1 -->
				<div class="panel" id="accordionpane<?=$tipo['id']?>" data-bind="if: iniciado">
					<div class="alert alert-error"><h4>Nenhuma Mensagem Encontrada</h4></div>
				</div>
				<!-- /ko -->
			</div>
			<!-- ko if: itensPaginaAtual().length > 0 -->
			<div class="pagination pagination-right paginacao">
				<i class="text-info" data-bind="text: mensagens().length + ' mensagens encontradas'"></i>
			</div>
			<!-- /ko -->
		</div>
		<?php endforeach; ?>
	</div>
</div>
<div class="br"></div>