

<span id="conteudo-titulo" class="box-com-titulo-header">Cadastro de Membro da Comissão de Formatura</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<?php echo $form->create('Usuario', array('url' => "/comitiva/adicionar")); ?>
	<?php include('_form.ctp'); ?>
	
	<p class="grid_16 alpha omega">
		<?php echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'submit'));?>
	</p>
</div>