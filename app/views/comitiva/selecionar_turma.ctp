<style type="text/css">
input[type="text"] {
    font-size:26px!important;
    font-weight:bold!important;
    padding:5px 0!important;
    text-align:center!important;
    height:initial!important;
    max-height:auto!important;
    border-width: 2px;
    box-sizing: border-box;
}
button { margin-left:-2px }
</style>
<div class="row-fluid">
    <div class="row-fluid">
        <div class="span4 offset4" style="text-align: center">
            <?=$form->create('Turma',array('url' => "confirmar_turma")); ?>
            <p style="font-size:18px">
                <span class="label label-info" style="vertical-align:top">!</span>
                Digite o codigo da turma
            </p>
            <?=$form->input('Turma.id', array(
                'type' => 'text',
                'class' => 'input-medium',
                'label' => false,
                'div' => false,
                'error' => false)); ?>
            <br />
            <button type="submit" class="big bg-color-red">
                Iniciar Cadastro
                <i class="icon-diamonds"></i>
            </button>
            <?=$form->end(array('label' => false, 'div' => false, 'class' => 'hide'));?>
        </div>
    </div>
    <div class="row-fluid">
        <?php $session->flash(); ?>
    </div>
</div>