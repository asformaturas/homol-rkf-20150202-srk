<style type="text/css">
.dl-horizontal dt { text-align:left }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        $("#continuar").click(function(e) {
            e.preventDefault();
            $("#form").submit();
        })
    });
</script>
<h2>Dados Da Turma</h2>
<br />
<div class="alert alert-info">
    Para prosseguir com o cadastro confirme os dados da Turma
</div>
<br />
<dl class="dl-horizontal">
    <dt>Nome da Turma</dt>
    <dd><?=$turma['Turma']['nome']?></dd>
    <dt>Data de Conclusão</dt>
    <dd><?= $turma['Turma']['ano_formatura'] ?> <?= $turma['Turma']['semestre_formatura'] ?>&ordm; Semestre</dd>
    <br />
    <dt>Cursos Participantes</dt>
    <?php
        $indiceDeCursos = 1;
        foreach ($info['universidades'] as $universidade) {
            foreach ($universidade['faculdades'] as $faculdade) {
                foreach ($faculdade['cursos'] as $curso) { ?>
        <dd><?= $indiceDeCursos . " - " . $universidade['nome'] . " - " . $faculdade['nome'] . " - " . $curso; ?></dd>
    <?php } } $indiceDeCursos++; } ?>
</dl>
<br />
<?=$form->create('Turma',array('url' => "cadastrar",'id' => 'form')); ?>
<?=$form->hidden('Turma.id'); ?>
<a class="button bg-color-red" href="/cadastro">
    Dados Incorretos, Alterar Turma
</a>
<a class="button bg-color-green" id="continuar">
    Dados Corretos, Prosseguir Com o Cadastro
</a>
<?=$form->end(array('label' => false, 'div' => false, 'class' => 'hide'));?>