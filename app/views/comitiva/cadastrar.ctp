<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/chosen.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/chosen.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/jquery-validate.js"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/jquery.meio.mask.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".chosen").chosen({width:'100%'});
        $('input[alt]').setMask();
        function verificarEmail(){
            var input = $('#input-email');
            var span = input.parent().prev().children('em');
            var url = "/formando/formandos/verificaEmail/";
            if($('#input-email').val() != ''){
                $.ajax({
                    url: url,
                    data: { 
                        data: { 
                           email: $('#input-email').val()
                        }
                    },
                    type: "POST",
                    dataType: "json",
                    success: function(data) {
                        if(data.mensagem == 1)
                            span.addClass('fg-color-green').text('Email disponível.');
                        else
                            span.addClass('fg-color-red').text('Email não disponível');
                    },
                    error: function(data) {
                        span.addClass('fg-color-red').text('Erro ao verificar.');
                    }
                });
            };
        };
        var timer;
        
        $("#input-email").keyup(function() {
            var input = $('#input-email');
            if(input.parent().prev().children('em').length < 1)
                input.parent().prev().append('<em></em>');
            var span = input.parent().prev().children('em');
            span.attr('class', 'pull-right').text('Verificando...');
            if(timer != null)
                clearTimeout(timer);
            timer = setTimeout(function(){verificarEmail()}, 700);
        });
    });
</script>
<style type="text/css">
    h3 { text-align: center; color: white }
</style>
<div class="row-fluid">
	<?php $session->flash(); ?>
	<?php echo $form->create('Usuario', array('url' => '/comitiva/cadastrar', 'id' => 'formulario')); ?>
	<?php echo $form->hidden('Turma.id', array('value' => $turma['Turma']['id'])); ?>
        <h2>Cadastro Comissão</h2><br/>
        <div class="row-fluid">
            <div class="span6 alert alert-info">
		<h3><?php echo $turma['Turma']['nome']?></h3>
            </div>
	</div>
	<hr/>
        <br/>
	<div class="row-fluid">
            <div class="span3">
		<label>Nome</label>
		<?php echo $form->input('nome', array('label' => false, 'div' => 'input-control', 'error' => false)); ?>
            </div>
            <div class="span3">
                <label>RG</label>
                <?php echo $form->input('FormandoProfile.rg', array('label' => false, 'div' => 'input-control', 'error' => false)); ?>
            </div>
	</div>
            
        <div class="row-fluid">
            <div class="span6">
                <label>Curso</label>
                <?php echo $form->select('FormandoProfile.curso_turma_id', $cursoTurma, null,  array('class' => 'chosen', 'empty'=>false, 'label' => false, 'div' => 'input-control', 'error' => false)); ?>
            </div>
        </div>
	
	<div class="row-fluid">
            <div class="span6">
		<label for="input-email">Email</label>
		<?php echo $form->input('Usuario.email', array('label' => false, 'div' => 'input-control', 'error' => false, 'class' => 'verificaEmail', 'id' => 'input-email',)); ?>
            </div>
        </div>
    
	<div class="row-fluid">
            <div class="span3">
		<label>Senha</label>
		<?php echo $form->input('Usuario.senha', array('type' => 'password', 'value' => '', 'label' => false, 'div' => 'input-control', 'error' => false)); ?>
            </div>
            <div class="span3">
                <label>Confirmar Senha</label>
                <?php echo $form->input('Usuario.confirmar', array('type' => 'password', 'value' => '', 'label' => false, 'div' => 'input-control', 'error' => false)); ?>
            </div>
	</div>
                
	<div class="row-fluid">
            <div class="span2">
		<label>Telefone Residencial</label>
		<?php echo $form->input('FormandoProfile.tel_residencial', array('label' => false, 'div' => 'input-control', 'error' => false, 'alt' => 'phone')); ?>
            </div>
            <div class="span2">
		<label>Telefone Comercial</label>
		<?php echo $form->input('FormandoProfile.tel_comercial', array('label' => false, 'div' => 'input-control', 'error' => false, 'alt' => 'phone')); ?>
            </div>
            <div class="span2">
                <label>Telefone Celular</label>
                <?php echo $form->input('FormandoProfile.tel_celular', array('label' => false, 'div' => 'input-control', 'error' => false, 'alt' => 'celphone')); ?>
            </div>
	</div>
    
	<div class="row-fluid">
            <div class="span6 bg">
		<label>Cargo</label>
			<?php echo $form->input('FormandoProfile.cargo_comissao', array('label' => false, 'div' => 'input-control', 'error' => false)); ?>
            </div>
	</div>
        <div class="row-fluid">
                <?php echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'button bg-color-greenDark submit'));?>
	</div>
</div>