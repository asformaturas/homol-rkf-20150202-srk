<script type="text/javascript">
jQuery(function($) {
	jQuery(".help").tooltip();
});
</script>
<p class="grid_5 omega alpha first">
	<br />
	<span class="submit button botao-proximo">Próximo</span>
</p>
<p class="grid_16 omega alpha first">
	<?php if($this->layout != "impressao" && $this->params['prefix'] != "atendimento") : ?>
	<br />
	Caro formando, finalizando o checkout você não poderá mais gerar boletos pelo site.
	<br />
	Se você encontrou divergência em alguma informação fornecida durante o checkout,
	<br />
	por favor descreva o caso no campo abaixo para facilitar a entrega dos seus convites para a formaturas.
	<br />
	<br />
	<?=$form->textarea('FormandoProfile.obs_checkout',array('class' => 'input-checkout')); ?>
	<br />
	<br />
	<span class="submit button" id='finalizar'>Finalizar</span>
	<?php elseif(!empty($formando['obs_checkout'])) : ?>
	<br />
	<strong>Observações:</strong> <?=$formando['obs_checkout']?>
	<?php endif; ?>
</p>
<div class="grid_full alpha omega first" id="conteudo-container">
	<div>
		<h2><b>Despesas com Ades&atilde;o</b></h2>
	</div>
	<br>
	<?php
	if( sizeof($despesas_adesao) == 0 ) {
		echo '<p style="color:red">Nenhuma despesa com adesão encontrada.</p>';
	} else {
	?>
	<div class="grid_full container-tabela">
		<table class="tabela-boletos">
			<thead>
				<tr>
					<th scope="col">Parcela</th>
					<th scope="col">Valor Base</th>
					<th scope="col">Data de<br />Vencimento</th>
					<th scope="col">Data de<br />Pagamento</th>
					<th scope="col">Valor Creditado</th>
					<th scope="col">Valor Pago</th>
					<!--<th scope="col">Multa</th>-->
					<th scope="col"><span class='item'>Valor IGPM<div class='help' title='<?=$textoIGPM?>'></div></span></th>
					<th scope="col">Status <br />(Valor Base)</th>
					<th scope="col">Saldo da Parcela</th>
				</tr>
			</thead>
			<tfoot>
				<tr class="linha-despesa">
					<td colspan="5"></td>
					<td colspan="2"><?=count($despesas_adesao);?> despesas com ades&atilde;o</td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach ($despesas_adesao as $despesa_adesao): ?>
				<?php
					if($despesa_adesao["Despesa"]["status"] == "aberta") {
						$valorPago = 0;
						$valorPagoReal = 0;
					} else {
						if(isset($despesa_adesao['DespesaPagamento'][0]['Pagamento'])) {
							$valorPagoReal = $despesa_adesao['DespesaPagamento'][0]['Pagamento']['valor_pago'];
							if($despesa_adesao['DespesaPagamento'][0]['Pagamento']['valor_pago'] < $despesa_adesao['DespesaPagamento'][0]['Pagamento']['valor_nominal'])
								$valorPago = $despesa_adesao['DespesaPagamento'][0]['Pagamento']['valor_pago'];
							else
								$valorPago = $despesa_adesao['DespesaPagamento'][0]['Pagamento']['valor_nominal'];
						} else {
							$valorPago = 0;
							$valorPagoReal = 0;
						}
						$valorPago = $valorPago > $despesa_adesao['Despesa']['valor'] ? $despesa_adesao['Despesa']['valor'] : $valorPago;
					}
					$multa = $valorPagoReal - $valorPago;
				?>
				<tr class="linha-despesa hover<?=$isOdd ? " odd" : ""?>"  id = "linha-despesa-<?=$despesa_adesao['Despesa']['id']; ?>">
					<td  colspan="1" width="10%"><?=$despesa_adesao['Despesa']['parcela'] . ' de ' . $despesa_adesao['Despesa']['total_parcelas']; ?></td>
					<td  colspan="1" width="10%">
						<span<?=$despesa_adesao['Despesa']['status'] == 'aberta' ? " id='valor-despesa-{$despesa_adesao['Despesa']['id']}'" : ""?>>
						<?=$despesa_adesao["Despesa"]["status"] == "paga" ? "-" : "R\${$despesa_adesao['Despesa']['valor']}"; ?>
						</span></td>
					<td valign="center" colspan="1" width="10%"><span<?=$despesa_adesao['Despesa']['status'] == 'aberta' ? " id='data-despesa-{$despesa_adesao['Despesa']['id']}'" : ""?>><?=date('d/m/Y', strtotime($despesa_adesao['Despesa']['data_vencimento'])); ?></span></td>
					<td  colspan="1" width="10%">
						<?php
							if($despesa_adesao['Despesa']['status'] == 'paga' && isset($despesa_adesao['DespesaPagamento'][0]['Pagamento']))
								echo date('d/m/Y', strtotime($despesa_adesao['Despesa']['data_pagamento']));	
							else
								echo '-';
						?>
					</td>
					<td class='valor-pago' width="10%"><?=$valorPago > 0 ? "R$$valorPago" : "-"?></td>
					<td width="10%"><?=$valorPagoReal > 0 ? "R$$valorPagoReal" : "-"?></td>
					<!--<td width="10%"><?=$multa > 0 ? "R$$multa" : "-"?></td>asdasdasdasdasd-->
					<?php
					$valo_igmp = !empty($despesa_adesao['Despesa']['correcao_igpm']) ? $despesa_adesao['Despesa']['correcao_igpm'] : 0; //alterar com a entrada de IGPM
					$saldo_parcela = $valorPago - ($despesa_adesao['Despesa']['valor']+$valo_igmp);
					$color = $saldo_parcela < 0 ? "red" : "green";
					?>
					<td><?=!empty($despesa_adesao['Despesa']['correcao_igpm']) ? "R\${$despesa_adesao['Despesa']['correcao_igpm']}" : "Não Aplicado"; ?></td>
					<td  colspan="1" width="10%">
						<?php
							if($despesa_adesao['Despesa']['status'] == 'paga' && $saldo_parcela < 0)
								echo "<span style='color:orange;'>Paga Parcialmente</span>";
							else if($despesa_adesao['Despesa']['status'] == 'aberta' || $despesa_adesao['Despesa']['status'] == 'vencida')
								echo "<span style='color:red;'>{$despesa_adesao['Despesa']['status']}</span>";
							else if ($despesa_adesao['Despesa']['status'] == 'paga' )
								echo "<span style='color:green;'>{$despesa_adesao['Despesa']['status']}</span>";
							else 
								echo "<span style='color:orange;'>{$despesa_adesao['Despesa']['status']}</span>";
						?>
					</td>
					<td width="20%" style="color:<?=$color?>" class="saldo-despesa <?=$saldo_parcela < 0 ? 'negativo' : ''?>">
					<?php $saldo_parcela = $saldo_parcela < 0 ? $saldo_parcela*-1 : $saldo_parcela;?>
					<?=$saldo_parcela == 0 ? "-" : "R$" . number_format($saldo_parcela,2,',','.')?>
					</td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
	<?php } ?>
	<div class="grid_full alpha omega first" id="div_despesas_extras">
	<h2><b>Despesas com Extras</b></h2>
	<?php
	if( sizeof($despesas_extras) == 0 ) {
		echo '<p style="color:red">Nenhuma despesa com extras encontrada.</p>';
	} else {
	?>
	<div class="grid_full container-tabela" style="margin:5px 0 10px 0">
		<table class='tabela-boletos'>
			<thead>
				<tr>					
					<th scope="col">Campanha</th>
					<th scope="col">Valor</th>
					<th scope="col">Parcela</th>
					<th scope="col">Data de Vencimento</th>
					<th scope="col">Data de Pagamento</th>
					<th scope="col">Valor Creditado</th>
					<th scope="col">Status (Valor base)</th>
					<th scope="col">Saldo da Parcela</th>
				</tr>
			</thead>
			<tfoot>
				<tr class="linha-despesa">
					<td colspan="4">
					</td>
					<td colspan="2"><?=count($despesas_extras);?> despesas com extras</td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach ($despesas_extras as $despesa_extra): ?>
			<?php
					if($despesa_extra["Despesa"]["status"] == "aberta") {
						$valorPago = 0;
					} else {
						if(isset($despesa_extra['DespesaPagamento'][0]['Pagamento'])) {
							if($despesa_extra['DespesaPagamento'][0]['Pagamento']['valor_pago'] < $despesa_extra['DespesaPagamento'][0]['Pagamento']['valor_nominal'])
								$valorPago = $despesa_extra['DespesaPagamento'][0]['Pagamento']['valor_pago'];
							else
								$valorPago = $despesa_extra['DespesaPagamento'][0]['Pagamento']['valor_nominal'];
						} else {
							$valorPago = 0;
						}
						$valorPago = $valorPago > $despesa_extra['Despesa']['valor'] ? $despesa_extra['Despesa']['valor'] : $valorPago;
					}
					$saldoParcela = $valorPago - $despesa_extra['Despesa']['valor'];
				?>
				<tr class="<?=$isOdd ? "odd" : ""?> linha-despesa hover">
					<td  colspan="1" width="20%"><?=$despesa_extra['CampanhasUsuario']['Campanha']['nome']; ?></td>
					<td  colspan="1" width="10%" class='valor-campanha'>R$<?=number_format($despesa_extra['Despesa']['valor'],2,',','.'); ?></td>
					<td  colspan="1" width="10%"><?=$despesa_extra['Despesa']['parcela'] . ' de ' . $despesa_extra['Despesa']['total_parcelas']; ?></td>
					<td  colspan="1" width="10%"><?=date('d/m/Y', strtotime($despesa_extra['Despesa']['data_vencimento'])); ?></td>					
					<td  colspan="1" width="10%">
						<?php
							if($despesa_extra['Despesa']['status'] == 'paga') {
								echo date('d/m/Y', strtotime($despesa_extra['Despesa']['data_pagamento']));	
							} else {
								echo '-';
							}
							
						?>
					</td>
					<td class='valor-pago'><?=$valorPago > 0 ? "R$$valorPago" : "-"?></td>				
					<td  colspan="1" width="15%">
						<?php
							if($despesa_extra['Despesa']['status'] == 'paga' && $saldoParcela < 0)
								echo "<span style='color:orange;'>Paga Parcialmente</span>";
							else if($despesa_extra['Despesa']['status'] == 'aberta' || $despesa_extra['Despesa']['status'] == 'vencida')
								echo "<span style='color:red;'>{$despesa_extra['Despesa']['status']}</span>";
							else if ($despesa_extra['Despesa']['status'] == 'paga' )
								echo "<span style='color:green;'>{$despesa_extra['Despesa']['status']}</span>";
							else 
								echo "<span style='color:orange;'>{$despesa_extra['Despesa']['status']}</span>";
						?>
					</td>
					<td style='color:<?=$saldoParcela == 0 ? "inherit" : ($saldoParcela < 0 ? "red" : "green")?>' class="saldo-campanha <?=$saldoParcela < 0 ? 'negativo' : ''?>">
						<?=$saldoParcela == 0 ? "-" : ($saldoParcela < 0 ? "R$" . number_format($saldoParcela*-1,2,',','.') : "R$" . number_format($saldoParcela,2,',','.'))?>
					</td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
	<?php
	// Final do if que verifica se há despesas com extras 
	}
	?>
	</div>
	<!-- Exibir resumo de extras -->
	<h2><b>Resumo de Extras</b></h2>
	<?php
	if( count($resumoExtras) == 0 ) {
		echo '<p style="color:red">Nenhum pagamento de extra efetuado foi encontrado.</p>';
	} else {
	?>
	<div class="grid_full container-tabela" style="margin:5px 0 10px 0">
		<table>
			<thead>
				<tr>
					<th scope="col">Evento</th>
					<th scope="col">Extra</th>
					<th scope="col">Total Em Aberto</th>
					<th scope="col">Total Pago</th>
					<th scope="col">Total Cancelado</th>
					<th scope="col">Total Retirado</th>
					<th scope="col">A Retirar</th>
					<th scope="col"></th>
				</tr>
			</thead>
			<tbody>
			<?php $isOdd = false; 
			
				foreach($resumoExtras as $extraResumo) :
			?>
				<?php if($isOdd):?>
					<tr class="odd linha-despesa">
				<?php else:?>
					<tr class="linha-despesa">
				<?php endif;?>
					<td  colspan="1" width="20%"><?=$extraResumo['evento_nome']; ?></td>
					<td  colspan="1" width="20%"><?=$extraResumo['extra_nome']; ?></td>
					<td  colspan="1" width="15%"><?=$extraResumo['total_em_aberto']; ?></td>
					<td  colspan="1" width="10%"><?=$extraResumo['total_pago']; ?></td>
					<td  colspan="1" width="10%"><?=$extraResumo['total_cancelado']; ?></td>
					<td  colspan="1" width="15%"><?=$extraResumo['total_retirado']; ?></td>
					<td  colspan="1" width="15%"><?=($extraResumo['total_em_aberto'] + $extraResumo['total_pago']) - $extraResumo['total_retirado']; ?></td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach;?>
			</tbody>
		</table>
	</div>
	<?php } ?>
</div>