<script type="text/html" id="tmpPagamentosFinal">
    <!-- ko if: valor() > 0 -->
    <tr>
        <td data-bind="text:$root.formatarValor(valor())"></td>
        <td data-bind="text:$data.tipo"></td>
        <td data-bind="text: $data.tipo() != 'dinheiro' ? data_vencimento : 'Indisponível'"></td>
    </tr>
    <!-- /ko -->
</script>
<script type="text/html" id="tmpItensFinal">
    <!-- ko if: quantidade > 0 -->
    <tr>
        <td data-bind="text:label"></td>
        <td data-bind="text:$root.formatarValor(valor)"></td>
        <td data-bind="text:quantidade"></td>
        <td data-bind="text:$root.formatarValor(valor*quantidade)"></td>
    </tr>
    <!-- /ko -->
</script>
<div class="row-fluid" data-bind="visible: saldo() < 0">
    <h3 class="fg-color-red" style="margin-bottom: 10px">Pagamentos</h3>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Valor</th>
                <th>Forma</th>
                <th>Vencimento</th>
                <th>Obs</th>
            </tr>
        </thead>
        <tbody data-bind="template: {
            name: 'tmpPagamentosFinal', foreach: pagamentos() }"></tbody>
    </table>
    <br />
</div>
<div class="row-fluid" data-bind="visible: itens().length > 0">
    <h3 class="fg-color-red" style="margin-bottom: 10px">Itens Comprados Durante Checkout</h3>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Item</th>
                <th>Valor</th>
                <th>Quantidade</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody data-bind="template: {
            name: 'tmpItensFinal', foreach: itens() }"></tbody>
    </table>
    <br />
</div>
<div class="row-fluid" id="itens-formatura">
    <h3 class="fg-color-red" style="margin-bottom: 10px">Itens de campanhas</h3>
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th width="50%">Descri&ccedil;&atilde;o</th>
                <th width="50%">Itens</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
    <br />
</div>
<div class="row-fluid">
    <button type="button"
        class="input-block-level bg-color-orange"
        data-bind="click:finalizarCheckout">
        Confirmar Dados
    </button>
</div>