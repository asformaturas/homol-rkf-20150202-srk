<?php if(count($despesasAdesao) > 0) : ?>
<h2 class="fg-color-red">Parcelas de ades&atilde;o</h2>
<br />
<table class="table table-condensed table-striped">
    <thead>
        <tr>
            <th width="10%">Parcela</th>
            <th width="12%">Valor</th>
            <th width="15%">Vencimento</th>
            <th width="15%">Data<br />Pagamento</th>
            <th width="12%">Valor<br />Creditado</th>
            <th width="12%">IGPM</th>
            <th width="12%">Status</th>
            <th width="12%">Saldo</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($despesasAdesao as $despesa): ?>
        <tr data-despesa="<?=$despesa['id']?>">
            <td><?= $despesa['parcela'] . ' de ' . $despesa['total_parcelas']; ?></td>
            <td>R$<?=number_format($despesa['valor'], 2, ',', '.') ?></td>
            <?php if (in_array($despesa['status'],array('atrasada','aberta'))) : ?>
            <td data-vencimento="<?=date('d/m/Y', strtotime($despesa['data_vencimento']))?>">
            <?php else : ?>
            <td>
            <?php endif; ?>
                <span class="pointer">
                    <?=date('d/m/Y', strtotime($despesa['data_vencimento']))?>
                </span>
            </td>
            <td>
                <?php
                if(!empty($despesa['data_pagamento']))
                    echo date('d/m/Y', strtotime($despesa['data_pagamento']));
                else
                    echo '-';
                ?>
            </td>
            <td><?=$despesa['valor_pago'] > 0 ? "R$" . number_format($despesa['valor_pago'], 2, ',', '.') : "-" ?></td>
            <td>
                <?=$despesa['correcao_igpm'] > 0 ? "R$" . number_format($despesa['correcao_igpm'], 2, ',', '.') : "-" ?>
                <?php if ($despesa['status_igpm'] == 'recriado') : ?>
                <i class="icon-help" rel="tooltip"
                    title="Esta Parcela de IGPM foi embutida em uma parcela única"></i>
                <?php endif; ?>
            </td>
            <td><?=$despesa['status']?></td>
            <?php if($despesa['saldo_parcela'] == 0) : ?>
            <td>-</td>
            <?php elseif($despesa['saldo_parcela'] > 0) : ?>
            <td class="fg-color-orange">
                R$<?= number_format(abs($despesa['saldo_parcela']), 2, ',', '.'); ?>
            </td>
            <?php else : ?>
            <td class="fg-color-green">
                R$<?= number_format(abs($despesa['saldo_parcela']), 2, ',', '.'); ?>
            </td>
            <?php endif; ?>
        </tr>
        <?php endforeach; ?>
        <?php if($igpm) : ?>
        <tr>
            <td>IGPM</td>
            <td>
                R$<?=number_format($igpm['valor'], 2, ',', '.') ?>
            </td>
            <td>-</td>
            <td>-</td>
            <td>
                R$<?=number_format($igpm['valor_pago'], 2, ',', '.') ?>
            </td>
            <td>-</td>
            <td>-</td>
            <?php if($igpm['saldo_parcela'] == 0) : ?>
            <td>-</td>
            <?php elseif($igpm['saldo_parcela'] > 0) : ?>
            <td class="fg-color-orange">
                R$<?= number_format($igpm['saldo_parcela'], 2, ',', '.'); ?>
            </td>
            <?php else : ?>
            <td class="fg-color-green">
                R$<?= number_format(abs($igpm['saldo_parcela']), 2, ',', '.'); ?>
            </td>
            <?php endif; ?>
        </tr>
        <?php endif; ?>
    </tbody>
</table>
<?php else : ?>
<h2 class="fg-color-red">Nenhuma parcela de ades&atilde;o encontrada</h2>
<?php endif; ?>