<div class="row-fluid">
    <div class="span6">
        <?=$form->input('FormandoProfile.interesse_fotos',
            array(
                'label' => 'Interesse no Álbum',
                'div' => 'input-control required',
                'error' => false,
                'empty' => 'Selecione',
                'class' => 'selectpicker',
                'id' => 'interesse-album',
                'type' => 'select',
                'options' => $interesseAlbum,
                'data-width' => '100%',
                'data-container' => 'body',
                'title' => 'Selecione'
            )); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span6">
        <?=$form->input('Usuario.nome',
            array(
                'label' => 'Nome',
                'div' => 'input-control text',
                'error' => false,
                'type' => 'text',
            )); ?>
    </div>
    <div class="span6">
        <?=$form->input('Usuario.email',
            array(
                'label' => 'Email',
                'div' => 'input-control text',
                'error' => false,
                'type' => 'text',
            )); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span3">
        <?=$form->input('FormandoProfile.tel_residencial',
            array(
                'label' => 'Tel Res',
                'div' => 'input-control text',
                'error' => false,
                'type' => 'text',
            )); ?>
    </div>
    <div class="span3">
        <?=$form->input('FormandoProfile.tel_celular',
            array(
                'label' => 'Celular',
                'div' => 'input-control text',
                'error' => false,
                'type' => 'text',
            )); ?>
    </div>
    <div class="span3">
        <?=$form->input('FormandoProfile.tel_comercial',
            array(
                'label' => 'Tel Com',
                'div' => 'input-control text',
                'error' => false,
                'type' => 'text',
            )); ?>
    </div>
    <div class="span3">
        <?=$form->input('FormandoProfile.nextel',
            array(
                'label' => 'Nextel',
                'div' => 'input-control text',
                'error' => false,
                'type' => 'text',
            )); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span6">
        <?=$form->input('FormandoProfile.nome_mae',
            array(
                'label' => 'Mãe',
                'div' => 'input-control text',
                'error' => false,
                'type' => 'text',
            )); ?>
    </div>
    <div class="span6">
        <?=$form->input('FormandoProfile.nome_pai',
            array(
                'label' => 'Pai',
                'div' => 'input-control text',
                'error' => false,
                'type' => 'text',
            )); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span3">
        <?=$form->input('FormandoProfile.tel_mae',
            array(
                'label' => 'Tel Mãe',
                'div' => 'input-control text',
                'error' => false,
                'type' => 'text',
            )); ?>
    </div>
    <div class="span3">
        <?=$form->input('FormandoProfile.tel_cel_mae',
            array(
                'label' => 'Cel Mãe',
                'div' => 'input-control text',
                'error' => false,
                'type' => 'text',
            )); ?>
    </div>
    <div class="span3">
        <?=$form->input('FormandoProfile.tel_pai',
            array(
                'label' => 'Tel Pai',
                'div' => 'input-control text',
                'error' => false,
                'type' => 'text',
            )); ?>
    </div>
    <div class="span3">
        <?=$form->input('FormandoProfile.tel_cel_pai',
            array(
                'label' => 'Cel Pai',
                'div' => 'input-control text',
                'error' => false,
                'type' => 'text',
            )); ?>
    </div>
</div>