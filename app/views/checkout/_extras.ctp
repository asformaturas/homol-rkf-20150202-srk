<div class='adicionar-itens'>
<?php if(!empty($itensExtras)) { ?>
<script type="text/javascript">
	$(function() {
		$('.item-select').change(function() {
			var id = $(this).attr('id').substr(12);
			checkout.extras.comprar($(this).val(),id);
		});
	});
</script>
	<table style='width:100%'>
		<thead>
			<tr>
				<th>Item</th>
				<th>Valor</th>
				<th>Qtde. Máxima de Itens</th>
				<th>Selecione</th>
				<th>Valor Total</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach($itensExtras as $itemExtra) : ?>
			<tr>
				<td><?=$itemExtra['titulo']?></td>
				<td>R$<?=number_format($itemExtra['valor'],2,',','.');?></td>
				<td><?=$itemExtra['itens_por_pessoa'];?></td>
				<td>
					<select name="data[itemExtra]" class="item-select" id="item-select-<?=$itemExtra['id']?>">
						<?php for($a=0;$a<=$itemExtra['itens_por_pessoa'];$a++) : ?>
						<option value="<?=$a?>"><?=$a?></option>
						<?php endfor; ?>
					</select>
				</td>
				<td class='saldo-item-<?=$itemExtra['id']?>'>-</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
<?php } else { ?>
	<h2 class='titulo'>Essa turma não tem itens extras disponíveis para venda</h2>
<?php } ?>
	<p>
		<br /><br />
		<input type="submit" class="submit botao-anterior" value="Anterior">
		&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="submit" class="submit botao-proximo" value="Próximo"></td>
	</p>
</div>
