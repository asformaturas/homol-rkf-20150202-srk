<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<style type="text/css">
#tabela-pagamentos tbody tr td { cursor: pointer }
#tabela-pagamentos tbody tr td i,
#tabela-pagamentos thead tr th i { position:relative; top:2px; cursor: pointer }
#tabela-pagamentos tbody tr.selecionado td { background: green; color:white;
    border-bottom: solid 1px white }
</style>
<div class="row-fluid">
    <h2>
        <a class="metro-button reload" data-bind="click: reload"></a>
        Lista de Pagamentos
    </h2>
</div>
<?php
$session->flash();
$paginator->options(array(
    'url' => array($this->params['prefix'] => true)));
$sortOptions = array('data-bind' => 'click: loadThis');
?>
<div class="row-fluid">
    <table class="table table-condensed table-striped" id="tabela-pagamentos">
        <thead>
            <tr>
                <th width="22%">
                    <?=$paginator->sort('Atendente', 'Atendente.id',$sortOptions); ?>
                </th>
                <th width="13%">
                    <?=$paginator->sort('Cod Form', 'codigo_formando',$sortOptions); ?>
                </th>
                <th width="13%">
                    <?=$paginator->sort('Tipo', 'CheckoutFormandoPagamento.tipo',$sortOptions); ?>
                </th>
                <th width="10%">
                    <?=$paginator->sort('Valor', 'CheckoutFormandoPagamento.valor',$sortOptions); ?>
                </th>
                <th width="15%">
                    <?=$paginator->sort('Vencimento', 'CheckoutFormandoPagamento.data_vencimento',$sortOptions); ?>
                </th>
                <th width="15%">
                    <?=$paginator->sort('Cadastro', 'CheckoutFormandoPagamento.data_cadastro',$sortOptions); ?>
                </th>
                <th width="12%">
                    <?=$paginator->sort('Recibo', 'CheckoutRecibo.id',$sortOptions); ?>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php if(count($pagamentos) > 0) : ?>
            <?php foreach($pagamentos as $pagamento) : ?>
            <tr>
                <td><?=$pagamento['Atendente']['nome']?></td>
                <td><?=$pagamento['Formando']['codigo_formando']?></td>
                <td>
                    <?=ucfirst($pagamento['CheckoutFormandoPagamento']['tipo']);?>
                </td>
                <td>R$<?=number_format($pagamento['CheckoutFormandoPagamento']['valor'], 2, ',', '.') ?></td>
                <td><?=date('d/m/Y',strtotime($pagamento['CheckoutFormandoPagamento']['data_vencimento']))?></td>
                <td>
                    <?=date('d/m/Y',strtotime($pagamento['CheckoutFormandoPagamento']['data_cadastro']))?>
                     - 
                    <?=date('H:i',strtotime($pagamento['CheckoutFormandoPagamento']['data_cadastro']))?>
                </td>
                <td>
                    <?php if(!empty($pagamento['CheckoutRecibo']['id'])) : ?>
                    <a class="button mini default" data-bind="click: loadThis"
                        href="/financeiro/checkout/recibo/<?=$pagamento['CheckoutRecibo']['id']?>">
                        Ver
                    </a>
                    <?php else : ?>
                    N&atildeo Gerado
                    <?php endif; ?>
                </td>
            </tr>
            <?php endforeach; ?>
            <?php else : ?>
            <tr>
                <td colspan="5">
                    <h2 class="fg-color-red">
                        Nenhum pagamento encontrado
                    </h2>
                </td>
            </tr>
            <?php endif; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="3" class="paginacao">
                    <?=$paginator->numbers(array('separator' => ' ',
                        'data-bind' => 'click: loadThis')); ?>
                </td>
                <td colspan="2">
                    <span class="label label-info">
                    <?=$paginator->counter(array(
                        'format' => '%count% pagamentos')); ?>
                    </span>
                </td>
            </tr>
        </tfoot>
    </table>
</div>