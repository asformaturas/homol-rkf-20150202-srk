<?php

$listaDeFormandosParaExcel = array();
foreach ($relatorio[2] as $r)
        $listaDeFormandosParaExcel[] = array(
            "Código Formando" => $r['codigo_formando'],
            "nome" => $r['nome'],
            "Mesas" => $r['total_mesas'],
            "Convites" => $r['total_convites'],
            "Meias" => $r['total_meias'],
            "Atendente" => $r['atendente'],
            "Data" => $r['data'],
        );
$excel->generate($listaDeFormandosParaExcel, 'lista_checkout_formandos_turma_' . $turma);
?>