<script type="text/javascript">
 	jQuery(function($) {
 		$("#FormandoProfileCpf").setMask("999.999.999-99");
 		$("#FormandoProfileTelResidencial").setMask("phone");
 		$("#FormandoProfileTelComercial").setMask("phone");
 		//$("#FormandoProfileTelCelular").setMask("(99) 99999-9999");
 		$("#FormandoProfileTelPai").setMask("phone");
 		//$("#FormandoProfileTelCelPai").setMask("(99) 99999-9999");
 		$("#FormandoProfileTelMae").setMask("phone");
 		//$("#FormandoProfileTelCelMae").setMask("(99) 99999-9999");
 		$("#FormandoProfileEndCep").setMask("99999-999");
 		//$("#FormandoProfileData-nascimento").setMask("99-99-9999");
 	});
</script>
<div class="grid_full alpha omega">

<p class="grid_6 alpha omega first ch-field ch-notice ch-important">
	<label class="grid_6 alpha omega">Nome</label>
	<?php echo $form->input('Usuario.nome', array('class' => 'grid_16 first alpha omega input-checkout','value' => $formando['nome'], 'label' => false, 'div' => false)); ?>
</p>

<p class="grid_6 ch-field ch-notice ch-important">
	<label class="grid_5 alpha">Email</label>
	<?php echo $form->input('Usuario.email', array('class' => 'grid_16 alpha  input-checkout','value' => $formando['email'], 'label' => false, 'div' => false)); ?>
</p>

<p class="grid_3 omega alpha first ch-field ch-notice ch-important">
	<label class="grid_10 alpha omega">Tel Res.</label>
	<?php echo $form->input('FormandoProfile.tel_residencial', array('class' => 'grid_13 alpha first omega input-checkout','value' => $formando['tel_residencial'], 'label' => false, 'div' => false)); ?>
</p>

<p class="grid_3 omega ch-field ch-notice ch-important">
	<label class="grid_7 alpha">Celular</label>
	<?php echo $form->input('FormandoProfile.tel_celular', array('class' => 'grid_13 alpha omega input-checkout','value' => $formando['tel_celular'], 'label' => false, 'div' => false)); ?>
</p>

<p class="grid_3 omega ch-field ch-notice ch-important">
	<label class="grid_10 alpha">Tel Com.</label>
	<?php echo $form->input('FormandoProfile.tel_comercial', array('class' => 'grid_13 alpha omega input-checkout','value' => $formando['tel_comercial'], 'label' => false, 'div' => false)); ?>
</p>

<p class="grid_3 omega ch-field ch-notice ch-important">
	<label class="grid_10 alpha">Nextel.</label>
	<?php echo $form->input('FormandoProfile.nextel', array('class' => 'grid_13 alpha omega input-checkout','value' => $formando['nextel'], 'label' => false, 'div' => false)); ?>
</p>

<p class="grid_6 alpha omega first ch-field ch-notice ch-important">
	<label class="grid_5 alpha omega">Nome Mãe</label>
	<?php echo $form->input('FormandoProfile.nome_mae', array('class' => 'grid_16 first alpha omega input-checkout','value' => $formando['nome_mae'], 'label' => false, 'div' => false)); ?>
</p>

<p class="grid_6 omega ch-field ch-notice ch-important">
	<label class="grid_5 alpha">Nome Pai</label>
	<?php echo $form->input('FormandoProfile.nome_pai', array('class' => 'grid_16 first alpha omega input-checkout','value' => $formando['nome_pai'], 'label' => false, 'div' => false)); ?>
</p>

<p class="grid_3 omega alpha first ch-field ch-notice ch-important">
	<label class="grid_11 alpha omega">Telefone Mãe</label>
	<?php echo $form->input('FormandoProfile.tel_mae', array('class' => 'grid_13 alpha first omega input-checkout','value' => $formando['tel_mae'], 'label' => false, 'div' => false)); ?>
</p>

<p class="grid_3 omega ch-field ch-notice ch-important">
	<label class="grid_11 alpha omega">Celular Mãe</label>
	<?php echo $form->input('FormandoProfile.tel_cel_mae', array('class' => 'grid_13 alpha first omega input-checkout','value' => $formando['tel_cel_mae'], 'label' => false, 'div' => false)); ?>
</p>

<p class="grid_3 omega ch-field ch-notice ch-important">
	<label class="grid_11 alpha">Telefone Pai</label>
	<?php echo $form->input('FormandoProfile.tel_pai', array('class' => 'grid_13 alpha omega input-checkout','value' => $formando['tel_pai'], 'label' => false, 'div' => false)); ?>
</p>

<p class="grid_3 omega ch-field ch-notice ch-important">
	<label class="grid_11 alpha">Celular Pai</label>
	<?php echo $form->input('FormandoProfile.tel_cel_pai', array('class' => 'grid_13 alpha omega input-checkout','value' => $formando['tel_cel_pai'], 'label' => false, 'div' => false)); ?>
</p>

<p class="grid_6 omega alpha first ch-field ch-important<?=empty($formando['rg']) ? " ch-notice ch-info" : ""?>">
	<label class="grid_full alpha">Interesse no Álbum</label>
	<?php echo $form->input('FormandoProfile.interesse_fotos',
			array('class' => 'grid_15 alpha omega input-checkout','type' => 'select','options' => $interesse_album,
			'label' => false, 'div' => false)); ?>
</p>

<p class="grid_3 omega ch-field<?=empty($formando['rg']) ? " ch-notice ch-info" : ""?>">
	<label class="grid_3 alpha">RG</label>
	<?php echo $form->input('FormandoProfile.rg', array('class' => 'grid_15 alpha omega input-checkout','value' => $formando['rg'], 'label' => false, 'div' => false)); ?>
</p>

<p class="grid_3 ch-field<?=empty($formando['cpf']) ? " ch-notice ch-info" : ""?>">
	<label class="grid_3 alpha">CPF</label>
	<?php echo $form->input('FormandoProfile.cpf', array('class' => 'grid_15 alpha omega input-checkout','value' => $formando['cpf'], 'label' => false, 'div' => false)); ?>
</p>

<p class="grid_3 omega alpha first ch-field<?=empty($formando['data_nascimento']) ? " ch-notice ch-info" : ""?>">
	<label class="grid_10 alpha omega">Data Nasc.</label>
	<?php echo $form->input('FormandoProfile.data-nascimento', array('class' => 'grid_10 alpha first omega input-checkout','value' => date('d-m-Y',strtotime($formando['data_nascimento'])), 'label' => false, 'div' => false)); ?>
</p>

<p class="grid_3 omega ch-field<?=empty($formando['end_cep']) ? " ch-important" : ""?>">
	<label class="grid_10 alpha">Cep</label>
	<?php echo $form->input('FormandoProfile.end_cep', array('class' => 'grid_10 alpha omega input-checkout','value' => $formando['end_cep'], 'label' => false, 'div' => false)); ?>
</p>

<p class="grid_6 omega ch-field">
	<label class="grid_10 alpha">Endereço</label>
	<?php echo $form->input('FormandoProfile.end_rua', array('class' => 'grid_14 alpha input-checkout','value' => $formando['end_rua'], 'label' => false, 'div' => false)); ?>
</p>

<p class="grid_3 omega alpha ch-field first">
	<label class="grid_10 alpha omega">Bairro</label>
	<?php echo $form->input('FormandoProfile.end_bairro', array('class' => 'grid_12 alpha omega first input-checkout','value' => $formando['end_bairro'], 'label' => false, 'div' => false)); ?>
</p>

<p class="grid_3 omega ch-field">
	<label class="grid_10 alpha">Número</label>
	<?php echo $form->input('FormandoProfile.end_numero', array('class' => 'grid_10 alpha omega input-checkout','value' => $formando['end_numero'], 'label' => false, 'div' => false)); ?>
</p>

<p class="grid_3 omega ch-field">
	<label class="grid_3 alpha">Cidade</label>
	<?php echo $form->input('FormandoProfile.end_cidade', array('class' => 'grid_15 alpha omega input-checkout','value' => $formando['end_cidade'], 'label' => false, 'div' => false)); ?>
</p>

<p class="grid_3 omega ch-field">
	<label class="grid_11 alpha">Estado</label>
	<?php $uf_brasil = array(
		'AC' => 'Acre',
		'AL' => 'Alagoas',
		'AP' => 'Amapá',
		'AM' => 'Amazonas',
		'BA' => 'Bahia',
		'CE' => 'Ceará',
		'DF' => 'Distrito Federal',
		'ES' => 'Espírito Santo',
		'GO' => 'Goiás',
		'MA' => 'Maranhão',
		'MT' => 'Mato Grosso',
		'MS' => 'Mato Grosso do Sul',
		'MG' => 'Minas Gerais',
		'PA' => 'Pará',
		'PB' => 'Paraíba',
		'PR' => 'Paraná',
		'PE' => 'Pernambuco',
		'PI' => 'Piauí',
		'RJ' => 'Rio de Janeiro',
		'RN' => 'Rio Grande do Norte',
		'RS' => 'Rio Grande do Sul',
		'RO' => 'Rondonia',
		'RR' => 'Roraima',
		'SC' => 'Santa Catarina',
		'SP' => 'São Paulo',
		'SE' => 'Sergipe',
		'TO' => 'Tocantins');
	echo $form->select('FormandoProfile.end_uf', $uf_brasil, 'SP', array('empty'=>false, 'class' => 'grid_10 alpha omega input-checkout', 'label' => false, 'div' => false)); ?>
</p>
<?php if($this->layout != "impressao") : ?>
<p class="grid_5 omega alpha first">
	<span class="submit button botao-proximo">Próximo</span>
</p>
<?php endif; ?>
</div>