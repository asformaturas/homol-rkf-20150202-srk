<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<style type="text/css">
#tabela-pagamentos tbody tr td { cursor: pointer }
#tabela-pagamentos tbody tr td i,
#tabela-pagamentos thead tr th i { position:relative; top:2px; cursor: pointer }
#tabela-pagamentos tbody tr.selecionado td { background: green; color:white;
    border-bottom: solid 1px white }
</style>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.selectpicker').selectpicker();
    var context = ko.contextFor($("#content-body")[0]);
    $("#form-filtro").submit(function(e) {
        e.preventDefault();
        context.$data.showLoading(function() {
            var dados = $("#form-filtro").serialize();
            var url = $("#form-filtro").attr('action');
            $.ajax({
                url : url,
                data : dados,
                type : "POST",
                dataType : "json",
                complete : function() {
                    context.$data.reload();
                }
            });
        });
    });
    var atendenteSelecionada = null;
    $("#tabela-pagamentos tbody").on('click','td',function() {
        var tr = $(this).parent();
        var atendente = tr.data('atendente');
        if(atendente != atendenteSelecionada) {
            $("i.icon-checkbox")
                .removeClass('icon-checkbox')
                .addClass('icon-checkbox-unchecked');
            $("tr.selecionado").removeClass('selecionado');
            atendenteSelecionada = atendente;
        }
        tr.toggleClass('selecionado');
        tr.find("td:first").children("i")
                .toggleClass('icon-checkbox-unchecked')
                .toggleClass('icon-checkbox');
        if($("tr.selecionado").length > 0)
            $("#pagamentos-selecionados").fadeIn(500,function() {
                exibePagamentosRecibo();
            });
        else
            $("#pagamentos-selecionados").fadeOut(500);
    });
    $(".selecionar-pagamentos").click(function() {
        if($("tr.selecionado").length > 0) {
            $("tr.selecionado").removeClass("selecionado");
            $('i.icon-checkbox').removeClass("icon-checkbox")
                    .addClass('icon-checkbox-unchecked');
            $("#pagamentos-selecionados").fadeOut(500,function() {
                exibePagamentosRecibo();
            });
        } else {
            $("tr[data-id]:not(.selecionado)").addClass('selecionado');
            $('i.icon-checkbox-unchecked')
                    .removeClass("icon-checkbox-unchecked")
                    .addClass('icon-checkbox');
            $("#pagamentos-selecionados").fadeIn(500,function() {
                exibePagamentosRecibo();
            });
        }
    });
    $("#enviar-pagamentos").click(function(e) {
        e.preventDefault();
        var mensagem = "O recibo não poderá ser substituido Após o envio.<br /><br />" +
                "Você confirma o recibo de " + $("#quantidade-pagamentos").text() +
                " pagamento(s) somando o valor de " +
                $("#soma-pagamentos").text() + " reais?";
        bootbox.confirm(mensagem,function(response) {
            if(response) {
                context.$data.showLoading(function() {
                    var dados = {
                        CheckoutRecibo : {
                            atendente_id : atendenteSelecionada
                        },
                        CheckoutReciboPagamento : []
                    };
                    $("tr.selecionado").each(function() {
                        dados.CheckoutReciboPagamento.push({
                            checkout_pagamento_id : $(this).data('id')
                        });
                    });
                    $.ajax({
                        url : '/<?=$this->params['prefix']?>/checkout/gerar_recibo',
                        data : {
                            data : dados
                        },
                        type : "POST",
                        dataType : "json",
                        complete : function() {
                            context.$data.reload();
                        }
                    });
                });
            }
        });
    });
    function exibePagamentosRecibo() {
        $("#quantidade-pagamentos").text($("tr.selecionado").length);
        var valor = 0.0;
        $("tr.selecionado").each(function() {
            valor+= parseFloat($(this).data('valor'));
        });
        $("#soma-pagamentos").text("R$"+number_format(valor.toFixed(2),2,',','.'));
    }
});
</script>
<div class="row-fluid">
    <h2>
        <a class="metro-button reload" data-bind="click: reload"></a>
        Lista de Pagamentos
    </h2>
</div>
<?php
$session->flash();
$paginator->options(array(
    'url' => array($this->params['prefix'] => true)));
$sortOptions = array('data-bind' => 'click: loadThis');
?>
<?=$form->create('Protocolo',array(
    'url' => $this->here,
    'id' => 'form-filtro')) ?>
<div class="row-fluid">
    <div class="span4">
        <?=$form->input('usuario_criador',array(
            'options' => $atendentes,
            'type' => 'select',
            'id' => 'atendente',
            'class' => 'selectpicker',
            'data-width' => '100%',
            'data-container' => 'body',
            'data-live-search' => 'true',
            'label' => 'Filtro',
            'div' => 'input-control text')); ?>
    </div>
    <div class="span2">
        <div class="input-control text">
            <label>&nbsp;</label>
            <button type='submit' class='bg-color-red input-block-level'>
                Filtrar
                <i class='icon-search-2'></i>
            </button>
        </div>
    </div>
    <div class="span5 pull-right hide" id="pagamentos-selecionados">
        <label class="pull-right">
            <b id="quantidade-pagamentos">1</b> pagamento(s) = <b id="soma-pagamentos">R$00,00</b>
        </label>
        <button type='submit' class='bg-color-blueDark input-block-level'
            id="enviar-pagamentos">
            Gerar Recibo dos Pagamentos
        </button>
    </div>
</div>
<?= $form->end(array('label' => false,
    'div' => false, 'class' => 'hide')); ?>
<div class="row-fluid">
    <?php if(!$atendente) : ?>
    <h2 class="fg-color-red">
        Selecione um(a) atendente
    </h2>
    <?php else : ?>
    <table class="table table-condensed table-striped" id="tabela-pagamentos">
        <thead>
            <tr>
                <th width="5%"></th>
                <th width="25%">
                    <?=$paginator->sort('Atendente', 'Protoclo.usuario_criador',$sortOptions); ?>
                </th>
                <th width="15%">
                    <?=$paginator->sort('Cod Form', 'Protocolo.usuario_id',$sortOptions); ?>
                </th>
                <th width="10%">
                    <?=$paginator->sort('Tipo', 'CheckoutFormandoPagamento.tipo',$sortOptions); ?>
                </th>
                <th width="15%">
                    <?=$paginator->sort('Valor', 'CheckoutFormandoPagamento.valor',$sortOptions); ?>
                </th>
                <th width="15%">
                    <?=$paginator->sort('Data Vencimento', 'CheckoutFormandoPagamento.data_vencimento',$sortOptions); ?>
                </th>
                <th width="15%">
                    <?=$paginator->sort('Data Cadastro', 'CheckoutFormandoPagamento.data_cadastro',$sortOptions); ?>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php if(count($pagamentos) > 0) : ?>
            <?php foreach($pagamentos as $pagamento) : ?>
            <tr data-id="<?=$pagamento['CheckoutFormandoPagamento']['id']?>"
                data-atendente="<?=$pagamento['Usuario']['id']?>"
                data-valor="<?=$pagamento['CheckoutFormandoPagamento']['valor']?>">
                <td style="text-align:center">
                    <i class="icon-checkbox-unchecked"></i>
                </td>
                <td><?=$pagamento['Usuario']['nome']?></td>
                <td><?=$pagamento['Protocolo']['Formando']['codigo_formando']?></td>
                <td>
                    <?=ucfirst($pagamento['CheckoutFormandoPagamento']['tipo']);?>
                </td>
                <td>R$<?=number_format($pagamento['CheckoutFormandoPagamento']['valor'], 2, ',', '.') ?></td>
                <td><?=date('d/m/Y',strtotime($pagamento['CheckoutFormandoPagamento']['data_vencimento']))?></td>
                <td>
                    <?=date('d/m/Y',strtotime($pagamento['CheckoutFormandoPagamento']['data_cadastro']))?>
                    &nbsp;&nbsp;
                    <?=date('H:i',strtotime($pagamento['CheckoutFormandoPagamento']['data_cadastro']))?>
                </td>
            </tr>
            <?php endforeach; ?>
            <?php else : ?>
            <tr>
                <td colspan="5">
                    <h2 class="fg-color-red">
                        Nenhum pagamento encontrado
                    </h2>
                </td>
            </tr>
            <?php endif; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="3" class="paginacao">
                    <?=$paginator->numbers(array('separator' => ' ',
                        'data-bind' => 'click: loadThis')); ?>
                </td>
                <td colspan="2">
                    <span class="label label-info">
                    <?=$paginator->counter(array(
                        'format' => '%count% pagamentos')); ?>
                    </span>
                </td>
            </tr>
        </tfoot>
    </table>
    <?php endif; ?>
</div>