<?php

$pagamentos = array();
foreach($recibo['CheckoutReciboPagamento'] as $pagamento)
    $pagamentos[] = array(
        'Formando' => $pagamento['CheckoutFormandoPagamento']['Protocolo']['Formando']['codigo_formando'],
        'Financeiro' => $recibo['Financeiro']['nome'],
        'Atendente' => $recibo['Atendente']['nome'],
        'Valor' => number_format($pagamento['CheckoutFormandoPagamento']['valor'], 2, ',', ''),
        'Tipo Access' => 'Cheque Contrato',
        'Tipo' => ucfirst($pagamento['CheckoutFormandoPagamento']['tipo']),
        'Data de Vencimento' => date('d/m/Y',strtotime($pagamento['CheckoutFormandoPagamento']['data_vencimento']))
    );
$excel->generate($pagamentos, 'recibo_' . $recibo['CheckoutRecibo']['id']);
?>