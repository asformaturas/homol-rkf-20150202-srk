<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.selectpicker').selectpicker();
    var context = ko.contextFor($("#content-body")[0]);
    $("#form-filtro").submit(function(e) {
        e.preventDefault();
        context.$data.showLoading(function() {
            var dados = $("#form-filtro").serialize();
            var url = $("#form-filtro").attr('action');
            $.ajax({
                url : url,
                data : dados,
                type : "POST",
                dataType : "json",
                complete : function() {
                    context.$data.reload();
                }
            });
        });
    });
});
</script>
<div class="row-fluid">
    <h2>
        <a class="metro-button reload" data-bind="click: reload"></a>
        Lista de Recibos
    </h2>
</div>
<?php
$session->flash();
$paginator->options(array(
    'url' => array($this->params['prefix'] => true)));
$sortOptions = array('data-bind' => 'click: loadThis');
?>
<?=$form->create('CheckoutRecibo',array(
    'url' => "/{$this->params['prefix']}/checkout/recibos",
    'id' => 'form-filtro')) ?>
<div class="row-fluid">
    <div class="span4">
        <?=$form->input('atendente_id',array(
            'options' => $atendentes,
            'type' => 'select',
            'empty' => 'Todos Atendentes',
            'class' => 'selectpicker',
            'data-width' => '100%',
            'id' => 'atendente',
            'data-container' => 'body',
            'label' => 'Atendentes',
            'div' => 'input-control text')); ?>
    </div>
    <div class="span2">
        <div class="input-control text">
            <label>&nbsp;</label>
            <button type='submit' class='bg-color-red input-block-level'>
                Filtrar
                <i class='icon-search-2'></i>
            </button>
        </div>
    </div>
</div>
<?= $form->end(array('label' => false,
    'div' => false, 'class' => 'hide')); ?>
<?php if(count($recibos) > 0) : ?>
<div class="row-fluid">
    <table class="table table-condensed table-striped" id="tabela-pagamentos">
        <thead>
            <tr>
                <th width="25%">
                    <?=$paginator->sort('Atendente', 'CheckoutRecibo.atendente_id',$sortOptions); ?>
                </th>
                <th width="15%">
                    <?=$paginator->sort('Valor', 'valor',$sortOptions); ?>
                </th>
                <th width="15%">
                    <?=$paginator->sort('Data', 'CheckoutRecibo.data_cadastro',$sortOptions); ?>
                </th>
                <th width="10%">
                    <?=$paginator->sort('Hora', 'CheckoutRecibo.data_cadastro',$sortOptions); ?>
                </th>
                <th width="35%"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($recibos as $recibo) : ?>
            <tr>
                <td><?=$recibo['Atendente']['nome']?></td>
                <td>R$<?=number_format($recibo[0]['valor'], 2, ',', '.') ?></td>
                <td>
                    <?=date('d/m/Y',strtotime($recibo['CheckoutRecibo']['data_cadastro']))?>
                </td>
                <td>
                    <?=date('H:i',strtotime($recibo['CheckoutRecibo']['data_cadastro']))?>
                </td>
                <td>
                    <a class="button mini default" data-bind="click: loadThis"
                        href="/<?=$this->params['prefix']?>/checkout/recibo/<?=$recibo['CheckoutRecibo']['id']?>">
                        Visualizar
                    </a>
                    <a class="button mini bg-color-blue" target="_blank"
                        href="/<?=$this->params['prefix']?>/checkout/recibo/<?=$recibo['CheckoutRecibo']['id']?>/imprimir">
                        Imprimir
                        <i class="icon-printer"></i>
                    </a>
                    <a class="button mini bg-color-orange" target="_blank"
                        href="/<?=$this->params['prefix']?>/checkout/recibo/<?=$recibo['CheckoutRecibo']['id']?>/excel">
                        Gerar Excel
                        <i class="icon-file-excel"></i>
                    </a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4" class="paginacao">
                    <?=$paginator->numbers(array('separator' => ' ',
                        'data-bind' => 'click: loadThis')); ?>
                </td>
                <td colspan="1">
                    <span class="label label-info">
                    <?=$paginator->counter(array(
                        'format' => '%count% recibos')); ?>
                    </span>
                </td>
            </tr>
        </tfoot>
    </table>
</div>
<?php else : ?>
<h2 class="fg-color-red">Nenhum recibo encontrado</h2>
<?php endif; ?>
