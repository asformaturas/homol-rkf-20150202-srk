<script type="text/javascript">
$(function() {
	$("#forma-pagamento").click( function() {
		checkout.pagamentos.iniciar();
	});
	$("#forma-pagamento-padrao").click( function() {
		if($('input[name="pagamento-padrao-data"]').val() == "") {
			alert('preencha a data base do cheque');
			$('input[name="pagamento-padrao-data"]').focus();
			return false;
		}
		var d = $('input[name="pagamento-padrao-data"]').val().split("/");
		if(d.length == 3) {
			d[1]--;
			var dataBase = new Date(d[2], d[1], d[0], 0, 0, 0, 0);
		} else {
			alert('data digitada é inválida');
			$('input[name="pagamento-padrao-data"]').focus();
			return false;
		}
		saldoExtras = checkout.extras.obterSaldoTotal() + checkout.campanhas.saldo;
		saldoDespesas = checkout.adesao.saldo;
		if($('select[name="pagamento-padrao-parcelas"]').val() != undefined)
			parcelas = $('select[name="pagamento-padrao-parcelas"]').val();
		else
			parcelas = 0;
		parcela = saldoDespesas / parcelas;
		parcelaComExtras = 0;
		itens = ['adesao'];
		if(checkout.campanhas.ativo)
			itensComDespesas = ['adesao','campanhas'];
		else
			itensComDespesas = ['adesao'];
		$.each(checkout.extras.itens,function(i,item) {
			if(!isNaN(item.saldo) && item.saldo < 0)
				itensComDespesas.push(item.titulo);
		});
		if($(".selecionavel.join-adesao-extra").hasClass("selecionado")) {
			parcelaComExtras = saldoExtras+parcela;
		} else if(saldoExtras < 0) {
			parcelasExtras = 0;
			date = new Date();
			mes = parseInt(date.getMonth())+1;
			var dataVencimento = date.getDate() + "/" +
				(mes < 10 ? "0" + mes : mes) + "/" +
				date.getFullYear();
			checkout.pagamentos.adicionar({
				valor:(saldoExtras < 0 ? saldoExtras*-1:saldoExtras),
				data_vencimento:dataVencimento,
				forma_pagamento:"cheque",
				obs:"",
				id:parcelas,
				itens:['campanhas','extras']
			});
		}
		for(a=0;a<parcelas;a++) {
			dataBase = new Date(d[2],parseInt(d[1])+a, d[0], 0, 0, 0, 0);
			mes = dataBase.getMonth()+1;
			var dataVencimento = dataBase.getDate() + "/" + (mes < 10 ? "0" + mes : mes) + "/" + dataBase.getFullYear();
			if(a == 0 && parcelaComExtras != 0)
				checkout.pagamentos.adicionar({valor:(parcelaComExtras < 0 ? parcelaComExtras*-1:parcelaComExtras),data_vencimento:dataVencimento,forma_pagamento:"cheque",obs:"",id:a,itens:itensComDespesas});
			else
				checkout.pagamentos.adicionar({valor:(parcela < 0 ? parcela*-1:parcela),data_vencimento:dataVencimento,forma_pagamento:"cheque",obs:"",id:a,itens:itens});
		}
		checkout.pagamentos.atualizarSoma();
		checkout.total.atualizar();
		$("tr.forma-pagamento-padrao").fadeOut(500,function() {
			$("#tabs").tabs("select",'tabs-revisao-checkout');
			window.top.location.href = "#conteudo";
			$("tr.forma-pagamento-edicao").fadeIn(500);
		});
	});
	$(".adicionar.pagamentos").live('click',function() {
		var id = $("#box-pagamentos table tbody tr").length > checkout.pagamentos.parcelas.length ? $("#box-pagamentos table tbody tr").length : checkout.pagamentos.parcelas.length;
		checkout.pagamentos.adicionar({valor:0,data_vencimento:"",forma_pagamento:"",obs:"",id:id,itens:[]});
	});
	$('.remover.pagamentos').live('click',function() {
		var tr = $(this).parent().parent();
		tr.fadeOut(500, function() {
			tr.remove();
			checkout.pagamentos.atualizarSoma();
			checkout.pagamentos.atualizarSaldoRestante();
		});
	});
	$(".selecionavel.parcela").live('click', function() {
		var id = $(this).parent().parent().attr('id').substring(8);
		var dir = $(this).attr('dir');
		var obj;
		$.each(checkout.pagamentos.parcelas,function(i,parcela) {
			if(id == parcela.id) {
				obj = parcela;
				return true;
			}
		});							
		if($(this).hasClass('selecionado')) {
			$(this).removeClass('selecionado');
			obj.itens.splice([$.inArray(dir,obj.itens)],1);
		} else {
			obj.itens.push(dir);
			$(this).addClass('selecionado');
		}
	});
	$('.parcela-valor').live('keyup',function() {
		checkout.pagamentos.atualizarSoma();
	});
	$('.parcela-valor').live('blur',function() {
		checkout.pagamentos.atualizarSaldoRestante();
	});
	$('#finalizar-pagamentos').live('click',function() {
		checkout.pagamentos.finalizar();
	});
	$(".selecionavel.join-adesao-extra").click(function() {
		if(checkout.extras.obterSaldoTotal() < 0) {
			if($(this).hasClass('selecionado')) {
				$(this).removeClass('selecionado');
				$(this).html('Não');
			} else {
				$(this).addClass('selecionado');
				$(this).html('Sim');
			}
		}
	});
});
</script>
<div class='adicionar-itens'>
	<table style='width:50%'>
		<thead>
			<tr>
				<th>Item</th>
				<th>Valor</th>
			</tr>
		</thead>
		<tfoot>
			<tr class='forma-pagamento-edicao' style='display:none'>
				<td style='padding-top:5px'><span class='submit button' id='forma-pagamento'>Selecionar Forma de Pagamento</span></td>
				<td>
					<input type="submit" class="submit botao-anterior" value="Anterior">
					<input type="submit" class="submit botao-proximo" value="Próximo">
				</td>
			</tr>
			<tr class='forma-pagamento-padrao'>
				<td class='head' colspan='2'>Forma de Pagamento</td>
			</tr>
			<?php if($qtdeParcelas > 0) : ?>
			<tr class='forma-pagamento-padrao'>
				<td>Unir 1&ordf; parcela de adesão com extras?</td>
				<td><span class="selecionavel join-adesao-extra">Não</span></td>
			</tr>
			<tr class='forma-pagamento-padrao'>
				<td>N&ordm; de Parcelas</td>
				<td>
					<select name='pagamento-padrao-parcelas'>
						<?php for($a = 1; $a <= $qtdeParcelas; $a++) : ?>
						<option value="<?=$a?>"><?=$a?></option>
						<?php endfor; ?>
					</select>
				</td>
			</tr>
			<?php else : ?>
			<span class="selecionavel join-adesao-extra" style="display:none"></span>
			<?php endif; ?>
			<tr class='forma-pagamento-padrao'>
				<td>Data base do cheque:</td>
				<td><input type="text" name='pagamento-padrao-data' /></td>
			</tr>
			<tr class='forma-pagamento-padrao'>
				<td><span class='submit button' id='forma-pagamento-padrao'>Confirmar</span></td>
			</tr>
		</tfoot>
		<tbody>
			<tr class='pendencias' dir='adesao'>
				<td>Adesão</td>
				<td class='saldo-adesao'></td>
			</tr>
			<tr class='pendencias' dir="campanhas">
				<td>Campanhas</td>
				<td class='saldo-campanhas'></td>
			</tr>
			<?php if($itensExtras) { ?>
			<?php foreach($itensExtras as $itemExtra) { ?>
			<tr class='pendencias' dir="item-<?=$itemExtra['id']?>">
				<td><?=$itemExtra['titulo']?></td>
				<td class='saldo-item-<?=$itemExtra['id']?>'>-</td>
			</tr>
			<?php } ?>
			<?php } ?>
			<tr>
				<td class='saldo-total-label'></td>
				<td class='saldo-total'></td>
			</tr>
		</tbody>
	</table>
</div>