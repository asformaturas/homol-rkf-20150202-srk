<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Relat&oacute;rio Checkout
        </h2>
    </div>
</div>
<div class="row-fluid">	
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col">C&oacute;digo</th>
                <th scope="col">Nome</th>
                <th scope="col">Campanha</th>
                <th scope="col">Item</th>
                <th scope="col">Qtde</th>
                <th scope="col">Valor Total</th>
            </tr>
        </thead>
        <tbody>
            <?php if(count($lista) > 0) : ?>
            <?php foreach($lista as $item) : ?>
            <tr>
                <td><?=$item['codigo_formando']?></td>
                <td><?=$item['nome']?></td>
                <td><?=$item['campanha']?></td>
                <td><?=$item['item']?></td>
                <td><?=$item['quantidade']?></td>
                <td>R$<?=number_format($item['valor'], 2, ',', '.') ?></td>
            </tr>
            <?php endforeach; ?>
            <?php else : ?>
            <tr>
                <td colspan='3'>
                    <h2 style="color:red">Nenhuma Venda Encontrada</h2>
                </td>
            </tr>
            <?php endif; ?>
        </tbody>
    </table>
</div>