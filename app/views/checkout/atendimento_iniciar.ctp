<?php
echo $html->script('jquery.tools.min');
echo $html->script('checkout');
echo $html->css('tooltip');
echo $html->css('checkout');
echo $html->css('messages');
?>
<?=debug(json_encode($formando))?>
<script type="text/javascript">

	$(document).ready(function() {
		
		$( "#tabs" ).tabs();
		
		$(".botao-proximo").click(function() {
			indiceTabSelecionada = ($(".ui-state-active").get(0).id).replace("tab-","");
			indiceDaProximaTab = parseInt(indiceTabSelecionada) + 1;
			nomeDaProximaTab = $("#tab-" + indiceDaProximaTab).children().get(0).rel;
			$("#tabs").tabs("select",nomeDaProximaTab);
			window.top.location.href = "#conteudo";
			return false;
		});
		
		$(".botao-anterior").click(function() {
			indiceTabSelecionada = ($(".ui-state-active").get(0).id).replace("tab-","");
			indiceDaProximaTab = parseInt(indiceTabSelecionada) - 1;
			nomeDaProximaTab = $("#tab-" + indiceDaProximaTab).children().get(0).rel;
			$("#tabs").tabs("select",nomeDaProximaTab);
			window.top.location.href = "#conteudo";
			return false;
		});

		$("#confirmar-pagamentos").live('click',function() {
			$('#finalizar-checkout').fadeIn(500);
		});
		
		var adesao = parseFloat(<?=number_format($saldoAdesao,2,'.','')?>);
		var campanhas = parseFloat(<?=number_format($saldoExtras,2,'.','')?>);
		var itens = <?=json_encode($itensExtras ? $itensExtras : array())?>;
		checkout = obterCheckout();
		checkout.init({adesao:adesao,campanhas:campanhas,extras:itens});
		$("#finalizar-checkout").live('click', function() {
			var Usuario = {};
			var FormandoProfile = {};
			$('.input-checkout').each(function() {
				var campo = $(this).attr('name').replace(/data\[/g,"");
				campo = campo.replace(/\]\[/g,".");
				campo = campo.replace(/\]/g,"");
				campo = campo.replace(/-/g,"_");
				valor = $(this).val().replace(/\'/g,"\\'");
				eval(campo + " = '" + valor + "'");
			});
			var dados = {};
			dados.pagamentos = JSON.parse(JSON.stringify(checkout.pagamentos, null, 2));
			dados.formando = JSON.parse(JSON.stringify(<?=json_encode($formando)?>));
			dados.Usuario = Usuario;
			dados.FormandoProfile = FormandoProfile;
			dados.itens = JSON.parse(JSON.stringify(checkout.extras.itens, null, 2));
			dados.adesao = JSON.parse(JSON.stringify(checkout.adesao, null, 2));
			dados.campanhas = JSON.parse(JSON.stringify(checkout.campanhas, null, 2));
			$.colorbox({
				width : 700,
				height: 600,
				html : '<p id="response" style="font-size:14px">Aguarde</p>',
				escKey : true,
	 			overlayClose : false,
	 			close : false,
				onLoad : function() { $('#cboxClose').remove(); },
				onComplete : function() {
					jQuery.ajax({
				 		url : "/<?=$this->params["prefix"]?>/checkout/finalizar",
				 		data : dados,
				 		type : "POST",
				 		dataType : "json",
				 		success : function(response) {
					 		var html = "";
				 	 		if(response.error) {
					 	 		html = response.message;
				 	 		} else {
					 	 		if(response.alertas != undefined) {
					 	 			html = "Checkout foi finalizado com pendências<br /><br />Pendências<br />";
					 	 			$.each(response.alertas, function(tipo,mensagem) {
						 	 			html+= mensagem + "<br />";
					 	 			});
					 	 		} else {
					 	 			html = "Checkout foi finalizado com sucesso<br /><br />";
					 	 		}
					 	 		html+= "Protocolo: " + response.protocolo;
					 	 		html+= '<br /><br /><a href="/<?=$this->params['prefix']?>/solicitacoes/visualizar/' + response.protocolo + '" class="submit button">Protocolo de Retirada</a>';
					 	 		$('#response').html(html);
				 	 		}
				 	 		html+= '<br /><br /><a href="/<?=$this->params['prefix']?>/" class="submit button">Home</a>';
				 	 		$('#response').html(html);
				 		},
				 		error : function() {
				 	 		alert("Ocorreu um erro com os nossos servidores. Por favor tente mais tarde.");
				 		}
				 	});
				}
			});
		});
	});

	function selecionarTab(tab) {
		$("#tabs").tabs( "select" , tab );
		window.top.location.href = "#conteudo";
		return false;
	}
	
</script>
<style type="text/css">
p { font-size:12px }
#conteudo-container { margin:10px 0 0 0 !important }
h2.titulo { background-color:#C7C5C6; color:black; padding:5px }
#cboxLoadedContent { background-color:white!important; border:solid 1px black; border-radius:4px; padding:20px }
.ui-tabs .ui-tabs-panel { padding:10px 0 0 0!important }
#conteudo-container { margin:10px 0 0 0 !important }
h2.titulo { background-color:#C7C5C6; color:black; padding:5px }
.adicionar-itens { font-size:12px }
.adicionar-itens table { width:95%; margin-top:20px }
.adicionar-itens table thead tr th, td.head { background:white; border:none; border-bottom:solid 1px #333; padding-bottom:5px!important;
	font-size:15px!important; text-align:left!important; font-weight:bold!important }
.adicionar-itens table tr { background:white; border:none }
.adicionar-itens table tr td { padding:5px; text-align:left }
.upper { text-transform:capitalize; }
.resumo-checkout p { font-size:14px; line-height:30px; }
.resumo-checkout span.pendencia { color:red }
.resumo-checkout span.pendencia.green { color:green }
.pendencias-finais tr td { border-bottom: solid 1px black }
.selecionavel {
	float:left;
	margin-left:5px;
	border: 1px solid #DCDCDC;
	border-color: rgba(0, 0, 0, .1);
	background:#F2F2F2;
	color: #444 !important;
	display: inline-block;
	font-size:11px;
	font-weight: bold;
	padding:5px;
	text-align: center;
	-moz-user-select: none;
	-webkit-user-select: none;
	text-align:center;
	-moz-border-radius:2px;
	-webkit-border-radius:2px;
	-khtml-border-radius:2px;
	border-radius:2px;
}
.selecionavel:first-child { margin-left:0 }
.selecionavel:hover { cursor:pointer }
.selecionavel.selecionado { color:white; background:gainsboro; }
.validar { font-weight:bold; color:orange; }
.validar:hover { cursor:pointer }
</style>
<span id="conteudo-titulo" class="box-com-titulo-header">Checkout de Formando</span>
<input type='hidden' id='convites-contrato' value="<?=$turma['Turma']['convites_contrato'];?>" />
<input type='hidden' id='mesas-contrato' value="<?=$turma['Turma']['mesas_contrato'];?>" />
<div id="conteudo-container">
	<?=$this->element('checkout/formando_info');?>
	<div id="tabs" style='border:none!important' class="grid_full alpha omega">
		<ul>
			<li id="tab-0"><a href="#tabs-dados-formando" rel="tabs-dados-formando">Dados Cadastrais</a></li>
			<li class="seta"></li>
			<li id="tab-1"><a href="#tabs-dados-financeiros" rel="tabs-dados-financeiros">Dados Financeiros</a></li>
			<li class="seta"></li>
			<li id="tab-2"><a href="#tabs-campanhas" rel="tabs-campanhas">Extras</a></li>
			<li class="seta"></li>
			<li id="tab-3"><a href="#tabs-extras" rel="tabs-extras">Adicionar Itens</a></li>
			<li class="seta"></li>
			<li id="tab-4"><a href="#tabs-pagamentos" rel="tabs-pagamentos">Forma de Pagamento</a></li>
			<li class="seta"></li>
			<li id="tab-5"><a href="#tabs-revisao-checkout" rel="tabs-revisao-checkout">Finalizar</a></li>
		</ul>
		<div id="tabs-dados-formando" class="grid_full alpha omega">
			<?php include('_form_formando.ctp') ?>
		</div>
		<div id="tabs-dados-financeiros" class="grid_full alpha omega">
			<?=$this->element('checkout/formando_despesas'); ?>
		</div>
		<div id="tabs-campanhas" class="grid_full alpha omega">
			<?php include("_campanhas.ctp")?>
		</div>
		<div id="tabs-extras" class="grid_full alpha omega">
			<?php include("_extras.ctp")?>
		</div>
		<div id="tabs-pagamentos" class="grid_full alpha omega">
			<?php include("_pagamentos.ctp")?>
		</div>
		<div id="tabs-revisao-checkout" class="grid_full alpha omega">
			<?php include('_finalizar.ctp')?>
		</div>
	</div>
	<div style="clear:both;"></div>
</div>
