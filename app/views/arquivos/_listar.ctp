<script type="text/javascript">
$(document).ready(function() {
    $('.visualizar').click(function() {
        bootbox.dialog('Carregando',[{
            label: 'Fechar'
        }],{
            remote: '<?="/{$this->params['prefix']}/arquivos/exibir/"?>'+
                    $(this).attr('dir')
        });
    });
});
</script>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Arquivos da Turma
            <a class="button mini bg-color-purple pull-right"
                data-bind="click: function() {
                page('<?= "/{$this->params['prefix']}/arquivos/inserir"?>') }">
                Inserir Arquivo
                <i class='icon-file-excel'></i>
            </a>
        </h2>
    </div>
</div>
<?php $session->flash(); ?>
<div class="row-fluid">
<?php if (sizeof($arquivos) > 0) : ?>
<?php
    $paginator->options(array(
        'url' => array($this->params['prefix'] => true)));
    $sortOptions = array('data-bind' => 'click: loadThis');
?>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col"><?=$paginator->sort('ID', 'Arquivo.id',$sortOptions); ?></th>
                <th scope="col"><?=$paginator->sort('Arquivo', 'Arquivo.nome',$sortOptions); ?></th>
                <th scope="col"><?=$paginator->sort('Usuário', 'Usuario.nome',$sortOptions); ?></th>
                <th scope="col"><?=$paginator->sort('Tipo', 'Arquivo.tipo',$sortOptions); ?></th>
                <th scope="col"><?=$paginator->sort('Tamanho', 'Arquivo.tamanho',$sortOptions); ?></th>
                <th scope="col"><?=$paginator->sort('Data Criação', 'Arquivo.criado',$sortOptions); ?></th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($arquivos as $arquivo) : ?>
            <tr>
                <td width="10%"><?=$arquivo['Arquivo']['id']; ?></td>
                <td width="30%"><?=$arquivo['Arquivo']['nome']; ?></td>
                <td width="20%"><?=$arquivo['Usuario']['nome']; ?></td>					
                <td width="5%"><?=$arquivo['Arquivo']['tipo']; ?></td>
                <td width="10%"><?=round($arquivo['Arquivo']['tamanho']/1000); ?> Kb</td>
                <td width="10%"><?=$arquivo['Arquivo']['criado']; ?></td>
                <td width="15%">
                    <button class="mini visualizar" type="button" dir="<?=$arquivo['Arquivo']['id']?>">
                        Visualizar
                    </button>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4">
                    <?=$paginator->numbers(array('separator' => ' ','data-bind' => 'click: loadThis')); ?>
                </td>
                <td colspan="2">
                    <?=$paginator->counter(array('format' => 'Total : %count% ' .  $this->name)); ?>
                </td>
            </tr>
        </tfoot>
    </table>
<?php else : ?>
    <h2 class="fg-color-red">Nenhum Arquivo Encontrado Para Esta Turma</h2>
<?php endif; ?>
</div>