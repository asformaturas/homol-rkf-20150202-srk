<div class="row-fluid">
    <h2>
        <a class="metro-button reload" data-bind="click: reload"></a>
        Configura&ccedil;&atilde;o do Mapa
        <span class="label label-inverse" id="carregando">Carregando</span>
        <button type="button" class="pull-right mini default" dir="<?=$mapa['Evento']['id']?>"
            id="enviar-configuracao">
            Enviar
        </button>
        <button type="button" class="pull-right mini bg-color-blueDark relatorio" dir="<?=$mapa['Evento']['id']?>" style="margin-right: 10px">
            Relatorio
        </button>
        <button type="button" class="pull-right mini bg-color-grayDark lista" dir="<?=$mapa['Evento']['id']?>" style="margin-right: 10px">
            Lista
        </button>
        <button type="button" class="pull-right mini bg-color-orangeDark reservar" dir="<?=$mapa['Evento']['id']?>" style="margin-right: 10px">
            Reservar
        </button>
    </h2>
</div>
<?php $session->flash(); ?>
<?php if($mapa) : ?>
<style type="text/css">
#numero { margin-bottom: 0; }
#container-mapa {
    -webkit-user-select:none;
    -moz-user-select:-moz-none; -moz-user-select:none;
    -o-user-select:none; -khtml-user-select:none;
    -ms-user-select:none; user-select:none;
}
#imagem-mapa { opacity: 0; position: absolute }
#mapa {
    position:relative;
    margin-top: 20px;
    background-repeat:no-repeat;
    background-size: 100% auto;
    cursor:pointer;
}
#lixeira {
    position: absolute;
    top:0;
    right:0;
    width:30px;
    height:30px;
    color:#333333;
    transition: color .2s;
    -webkit-transition: color .2s;
}
#lixeira:hover { color:red }
#lixeira span { width:30px; height:30px; font-size: 30px;
    line-height: 30px }
.local { position: absolute; height:17px; width:17px;
    box-sizing: border-box; display:inline-block; background: green;
    background: rgba(20,102,20,.7); border:none; border-radius: 50%;
    -webkit-user-select:none;
    -moz-user-select:-moz-none; -moz-user-select:none;
    -o-user-select:none; -khtml-user-select:none;
    -ms-user-select:none; user-select:none;
}
.local.editando { background: red; background: rgba(102,21,21,.7) }
.local.escolhido { background: yellow; background: rgba(186,191,23,.7) }
.local.selecionado { background: black }
.local:hover .coluna { display:block!important; }
.local:hover { background:black }
.local .coluna { width:100%; height:100%; display:none; padding:0; margin:0;
    background: transparent; color:white; font-size:10px; line-height: 19px;
    text-align: center; text-indent:1px; -webkit-user-select:none;
    -moz-user-select:-moz-none; -moz-user-select:none;
    -o-user-select:none; -khtml-user-select:none;
    -ms-user-select:none; user-select:none; }
.alinhamento button { float:left; min-width:initial!important;
    border:none; }
.alinhamento button i { margin:0!important; line-height: 13px!important;
    font-size:16px!important; }
</style>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/jquery.meio.mask.js"></script>
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        
        var mapa = <?=json_encode($mapa);?>;
        
        $('.reservar').click(function(){
            dir = $(this).attr('dir');
            var context = ko.contextFor($(".metro-button.reload")[0]);
            context.$data.showLoading(function() {
                $.ajax({
                    complete: function() {
                        context.$data.page('planejamento/eventos/mapa_reservar/' + dir);
                    }
                });
            });
        });
        
        $('.lista').click(function(){
            dir = $(this).attr('dir');
            var context = ko.contextFor($(".metro-button.reload")[0]);
            context.$data.showLoading(function() {
                $.ajax({
                    complete: function() {
                        context.$data.page('planejamento/eventos/mapa_lista/' + dir);
                    }
                });
            });
        });
        
        $('.relatorio').click(function(){
            dir = $(this).attr('dir');
            var context = ko.contextFor($(".metro-button.reload")[0]);
            context.$data.showLoading(function() {
                $.ajax({
                    complete: function() {
                        context.$data.page('planejamento/eventos/mapa_relatorio/' + dir);
                    }
                });
            });
        });
        
        $("#imagem-mapa").load(function() {
            
            var context = ko.contextFor($("#content-body")[0]);
            var largura = $(this).width();
            var altura = $(this).height();
            var diferenca = mapa.EventoMapa.largura != largura ? largura / mapa.EventoMapa.largura : 0;
            console.log(largura);
            console.log(mapa.EventoMapa.largura);
            console.log(diferenca);
            var letras = <?=json_encode($letras)?>;
            
            $('#mapa').on('click', '.escolhido', function(){
                var id = $(this).attr('data-id');
                bootbox.alert("Tem certeza que deseja desalocar o formando?");
                var desalocar = $("<button>",{
                    type: 'button',
                    class: 'button bg-color-greenDark',
                    id: 'desalocar',
                    text:'Desalocar'
                });
                $('.modal-footer').prepend(desalocar);
                $('#desalocar').click(function() {
                    var context = ko.contextFor($(".metro-button.reload")[0]);
                    var url = '<?="/eventos/mapa_desalocar_formando/"?>'+id;
                    bootbox.hideAll();
                    context.$data.showLoading(function() {
                        $.ajax({
                            url : url,
                            complete : function() {
                                context.$data.reload();
                            }
                        });
                    });
                });
            });
            
            $('input[alt]').setMask();
            
            $("#mapa")
                    .css('background-image',"url("+$(this).attr('src')+")")
                    .width(largura)
                    .height(altura);
            
            $("#mapa").on('click','.local:not(.selecionado)',function(e) {
                e.stopPropagation();
                var local = $(this);
                var coluna = local.attr('data-coluna');
                selecionarColuna(coluna);
                selecionarLocal(local);
            });
            
            $("#mapa").dblclick(function(e) {
                if($("#colunas option").length < 1)
                    criarColuna();
                var coluna = $("#colunas").val();
                if($(".local[data-coluna='"+coluna+"']").length > 0)
                    var numero = parseInt($(".local[data-coluna='"+coluna+"']:last").data('numero'))+1;
                else
                    var numero = 1;
                var posX = $(this).offset().left,
                    posY = $(this).offset().top;
                var left = e.pageX - posX - 8,
                    top = e.pageY - posY - 8;
                var local = criarLocal(left,top,numero);
                selecionarLocal(local);
            });
            
            $("#mapa").on('dblclick','.local',function(e) {
                e.stopPropagation();
            });
            
            $("#container-mapa").keydown(function(e) {
                if(e.altKey || e.ctrlKey)
                    return;
                if(e.keyCode >= 37 && e.keyCode <= 40 && !e.shiftKey &&
                        $(".local.selecionado").length > 0) {
                    e.preventDefault();
                    var local = $(".local.selecionado");
                    if(!local.hasClass('novo'))
                        local.addClass('alterado');
                    exibirBotaoEnvio();
                    var left = parseFloat(local.css('left'));
                    var top = parseFloat(local.css('top'));
                    if(e.keyCode == 37)
                        local.css('left',(left-1)+'px');
                    else if(e.keyCode == 38)
                        local.css('top',(top-1)+'px');
                    else if(e.keyCode == 39)
                        local.css('left',(left+1)+'px');
                    else
                        local.css('top',(top+1)+'px');
                } else if(e.keyCode == 9) {
                    if($(".local.selecionado").length < 1) {
                        var selecionar = $(".local:first");
                    } else {
                        var local = $(".local.selecionado");
                        var coluna = local.attr('data-coluna');
                        e.preventDefault();
                        var atual = local.attr('data-numero');
                        var selecionar = false;
                        if(e.shiftKey) {
                            atual--;
                            var ultimo = 1;
                            for(var a = atual; a >= ultimo; a--) {
                                if($(".local[data-coluna="+coluna+"][data-numero="+a+"]").length > 0) {
                                    selecionar = $(".local[data-coluna="+coluna+"][data-numero="+a+"]");
                                    break;
                                }
                            }
                        } else {
                            atual++;
                            var ultimo = $(".local[data-coluna="+coluna+"]:last").attr('data-numero');
                            if($(".local[data-coluna="+coluna+"]").length > ultimo)
                                ultimo = $(".local[data-coluna="+coluna+"]").length;
                            for(var a = atual; a <= ultimo; a++) {
                                if($(".local[data-coluna="+coluna+"][data-numero="+a+"]").length > 0) {
                                    selecionar = $(".local[data-coluna="+coluna+"][data-numero="+a+"]");
                                    break;
                                }
                            }
                        }
                    }
                    if(selecionar) {
                        selecionarColuna(selecionar.attr('data-coluna'));
                        selecionarLocal(selecionar);
                    }
                } else if(e.shiftKey && e.keyCode == 13) {
                    e.preventDefault();
                    if($(".local").length > 0)
                        copiarColuna();
                } else if(e.shiftKey && e.keyCode == 32) {
                    e.preventDefault();
                    if($(".local").length > 0) {
                        var local = $(".local:last");
                        var top = parseFloat(local.css('top'))+parseFloat(local.width())+10;
                        var left = parseFloat(local.css('left'));
                        var numero = parseInt(local.data('numero'))+1;
                    } else {
                        if($("#colunas option").length < 1)
                            criarColuna();
                        var posX = $(this).offset().left,
                            posY = $(this).offset().top,
                            numero = 1;
                        var left = e.pageX - posX - 8,
                            top = e.pageY - posY - 8;
                    }
                    local = criarLocal(left,top,numero);
                    selecionarLocal(local);
                } else if(e.shiftKey && e.keyCode == 8 && $(".local.selecionado").length > 0) {
                    e.preventDefault();
                    removerLocal($(".local.selecionado"));
                }
            });
            
            $("[data-tamanho='maior']").click(function() {
                var t = $(".local:first").width();
                $(".local").width(t+1).height(t+1);
                $(".local:not(.novo)").addClass('alterado');
                exibirBotaoEnvio();
            });
            
            $("[data-tamanho='menor']").click(function() {
                var t = $(".local:first").width();
                $(".local").width(t-1).height(t-1);
                $(".local:not(.novo)").addClass('alterado');
                exibirBotaoEnvio();
            });
            
            $("#colunas").change(function() {
                var coluna = $(this).val();
                var local = $(".local[data-coluna='"+coluna+"']:first");
                selecionarColuna(coluna);
                selecionarLocal(local);
                exibirOpcoesColunas();
            });
            
            $("#numero").keyup(function() {
                var numero = $(this).val();
                var coluna = $("#colunas").val();
                $(".local.selecionado")
                        .attr('data-numero',numero)
                        .children('.coluna')
                        .text(coluna+numero);
                $(".local.selecionado:not(.novo)").addClass('alterado');
            });
            
            var locais = {
                inserir : [],
                alterar : [],
                remover : []
            };
            
            $("#novo-local").click(function() {
                if($(".local").length > 0) {
                    var local = $(".local:last");
                    var top = parseFloat(local.css('top'))+parseFloat(local.width())+10;
                    var left = parseFloat(local.css('left'));
                    var numero = parseInt(local.data('numero'))+1;
                } else {
                    if($("#colunas option").length < 1)
                        criarColuna();
                    var posX = $(this).offset().left,
                        posY = $(this).offset().top,
                        numero = 1;
                    var left = 15,
                        top = 15;
                }
                local = criarLocal(left,top,numero);
                selecionarLocal(local);
            });
            
            $("#copiar-coluna").click(function() {
                copiarColuna();
            });
            
            $(".vertical").click(function() {
                $("#colunas option").each(function() {
                    var coluna = $(this).val();
                    var left = 0.0;
                    var quantidade = $(".local[data-coluna='"+coluna+"']").length;
                    $(".local[data-coluna='"+coluna+"']").each(function() {
                        left+=parseFloat($(this).css('left'));
                    });
                    $(".local[data-coluna='"+coluna+"']").css('left',parseFloat(left/quantidade).toFixed(2)+'px');
                    $(".local[data-coluna='"+coluna+"']:not(.novo)").addClass('alterado');
                });
                $("#enviar-configuracao").fadeIn(500);
            });
            
            $(".horizontal").click(function() {
                var linhas = {};
                $(".local").each(function() {
                    if(linhas[$(this).attr('data-numero')] == undefined)
                        linhas[$(this).attr('data-numero')] = { q : 0, t : 0.0 };
                    linhas[$(this).attr('data-numero')]['q']++;
                    linhas[$(this).attr('data-numero')]['t']+=parseFloat($(this).css('top'));
                });
                $.each(linhas,function(i,linha) {
                    $(".local[data-numero='"+i+"']").css('top',parseFloat(linha['t']/linha['q']).toFixed(2)+'px');
                    $(".local[data-numero='"+i+"']:not(.novo)").addClass('alterado')
                });
                $("#enviar-configuracao").fadeIn(500);
            });
            
            $("[data-direcao='mais']").click(function() {
                if($(".local.selecionado").length == 1) {
                    var direcao = $(this).parents("[data-alinhamento]").data('alinhamento');
                    var local = $(".local.selecionado");
                    if(direcao == 'horizontal') {
                        var coluna = local.attr('data-coluna');
                        $(".local[data-coluna='"+coluna+"']").each(function() {
                            var left =parseFloat($(this).css('left'))+1;
                            $(this).css('left',left+'px');
                            if(!$(this).hasClass('novo'))
                                $(this).addClass('alterado')
                        });
                    } else {
                        var linha = local.attr('data-numero');
                        $(".local[data-numero='"+linha+"']").each(function() {
                            var top =parseFloat($(this).css('top'))-1;
                            $(this).css('top',top+'px');
                            if(!$(this).hasClass('novo'))
                                $(this).addClass('alterado')
                        });
                    }
                }
                $("#enviar-configuracao").fadeIn(500);
            });
            
            $("[data-direcao='menos']").click(function() {
                if($(".local.selecionado").length == 1) {
                    var direcao = $(this).parents("[data-alinhamento]").data('alinhamento');
                    var local = $(".local.selecionado");
                    if(direcao == 'horizontal') {
                        var coluna = local.attr('data-coluna');
                        $(".local[data-coluna='"+coluna+"']").each(function() {
                            var left =parseFloat($(this).css('left'))-1;
                            $(this).css('left',left+'px');
                            if(!$(this).hasClass('novo'))
                                $(this).addClass('alterado')
                        });
                    } else {
                        var linha = local.attr('data-numero');
                        $(".local[data-numero='"+linha+"']").each(function() {
                            var top =parseFloat($(this).css('top'))+1;
                            $(this).css('top',top+'px');
                            if(!$(this).hasClass('novo'))
                                $(this).addClass('alterado')
                        });
                    }
                }
                $("#enviar-configuracao").fadeIn(500);
            });
            
            $("#reiniciar-configuracao").click(function() {
                dir = $(this).attr('dir');
                bootbox.confirm("Todos os locais criados serão apagados. Confirma o reinicio da configuração?",function(result) {
                    if(result) {
                        context.$data.showLoading(function() {
                            var url = "/<?=$this->params['prefix']?>/eventos/mapa_reiniciar/" + dir;
                            $.getJSON(url).always(function() {
                                context.$data.page("/<?=$this->params['prefix']?>/eventos/mapa/" + dir);
                            });
                        });
                    }
                });
            });
            
            $("#enviar-configuracao").click(function() {
                dir = $(this).attr('dir');
                var colunas = [];
                $("#colunas > option").each(function() {
                    colunas.push($(this).val());
                });
                $(".local.novo").each(function() {
                    var local = {
                        top : parseFloat($(this).css('top')).toFixed(2),
                        left : parseFloat($(this).css('left')).toFixed(2),
                        width : $(this).width(),
                        coluna : $(this).attr('data-coluna'),
                        numero : $(this).attr('data-numero')
                    };
                    locais.inserir.push(local);
                });
                $(".local.alterado").each(function() {
                    var local = $(this).data();
                    locais.alterar.push({
                        EventoMapaLocal : {
                            top : parseFloat($(this).css('top')).toFixed(2),
                            left : parseFloat($(this).css('left')).toFixed(2),
                            width : $(this).width(),
                            evento_mapa_coluna_id : local.evento_mapa_coluna_id,
                            id : local.id,
                            numero : local.numero
                        }
                    });
                });
                var data = {
                    colunas : colunas,
                    locais : locais,
                    mapa : {
                        largura : largura,
                        altura : altura
                    }
                };
                context.$data.showLoading(function() {
                    $.ajax({
                        url : '<?=$this->here?>/' + dir,
                        data : {
                            data : data
                        },
                        type : "POST",
                        dataType : "json",
                        complete : function(response) {
                            context.$data.reload();
                        }
                    });
                });
            });
            
            function exibirBotaoEnvio() {
                if($(".local.novo").length > 0 || $(".local.alterado").length || locais.remover > 0)
                    $("#enviar-configuracao").fadeIn(500);
                else
                    $("#enviar-configuracao").fadeOut(500);
            }
            
            function exibirOpcoesColunas() {
                var colunas = $("#colunas");
                if(colunas.find('option').length > 0) {
                    var coluna = colunas.val();
                    if($(".local[data-coluna='"+coluna+"']").length > 0) {
                        if($(".colunas:disabled").length > 0)
                            $(".colunas").addClass('default').removeAttr('disabled');
                    } else if($(".local[data-coluna='"+coluna+"']").length == 0) {
                        $(".colunas").removeClass('default').attr('disabled','disabled');
                    }
                } else if(!$("#copiar-coluna").is(":disabled")) {
                    $("#copiar-coluna").removeClass('default').attr('disabled','disabled');
                }
            }
            
            function criarColuna() {
                var colunas = $("#colunas");
                var coluna = letras[$("#colunas option").length];
                var option = $("<option/>",{
                    value : coluna,
                    'data-nome' : coluna,
                    text : "Coluna " + coluna
                });
                colunas.append(option).val(coluna);
                $("#numero").fadeOut(500);
                selecionarColuna(coluna);
                exibirOpcoesColunas();
            }
            
            function copiarColuna() {
                var colunaCopiada = $("#colunas").val();
                criarColuna();
                var novaColuna = $("#colunas").val();
                $(".local[data-coluna='"+colunaCopiada+"']").each(function() {
                    var left = parseFloat($(this).css('left'))+$(this).width()+10;
                    var top = parseFloat($(this).css('top'));
                    var numero = $(this).attr('data-numero');
                    criarLocal(left,top,numero);
                });
                var local = $(".local[data-coluna='"+novaColuna+"']:first");
                selecionarLocal(local);
            }
            
            function criarLocal(left,top,numero) {
                var coluna = $("#colunas").val();
                var width = $(".local:first").width() || 17;
                var local = $("<div/>",{
                    class:'local novo editando',
                    'data-coluna' : coluna,
                    'data-numero' : numero,
                    style:'top:'+top+'px;left:'+left+'px;width:'+
                            width+'px;height:'+width+'px'
                }).appendTo($("#mapa"));
                var tooltip = "<div class=' coluna'>" + coluna +
                        numero + "</div>";
                local.append(tooltip);
                $("#numero").val(numero).fadeIn(500);
                criarDragLocal(local);
                exibirOpcoesColunas();
                exibirBotaoEnvio();
                return local;
            }
            
            function removerLocal(local) {
                if(local.attr('data-id') != undefined)
                    locais.remover.push(local.attr('data-id'));
                local.remove();
                $("#numero").val('').fadeOut(500,function() {
                    exibirOpcoesColunas();
                    exibirBotaoEnvio();
                });
            }
            
            function criarDragLocal(local) {
                local.draggable({
                    containment: "parent",
                    start: function(e,u) {
                        var coluna = u.helper.attr('data-coluna');
                        selecionarColuna(coluna);
                        selecionarLocal(u.helper);
                        $("#lixeira").css('color','#333333').fadeIn(200);
                    },
                    stop: function(e,u) {
                        var right = largura-u.position['left']-u.helper.width();
                        var coluna = u.helper.attr('data-coluna');
                        if(right < 27 && u.position['top'] < 27) {
                            removerLocal(u.helper);
                        } else if(!u.helper.hasClass('novo')) {
                            u.helper.addClass('alterado');
                        }
                        $("#lixeira").fadeOut(200,function() {
                            exibirBotaoEnvio();
                        });
                    },
                    drag: function(e,u) {
                        var right = largura-u.position['left']-u.helper.width();
                        if(right < 27 && u.position['top'] < 27)
                            $("#lixeira").css('color','red');
                        else
                            $("#lixeira").css('color','#333333');
                    }
                });
            }
            
            function selecionarLocal(local) {
                if(!local.hasClass('selecionado')) {
                    var numero = local.attr('data-numero');
                    $(".local.selecionado").removeClass('selecionado')
                            .children('.coluna').hide();
                    local.addClass('selecionado');
                    local.children(".coluna").show();
                    $(".local[data-numero='"+numero+"']").addClass('editando');
                    $("#numero").val(numero).fadeIn(500);
                    $("#container-mapa").focus();
                }
            }
            
            function selecionarColuna(coluna) {
                $(".local.selecionado").removeClass('selecionado');
                $(".local.editando").removeClass('editando');
                $(".local > .coluna:visible").hide();
                $(".local[data-coluna='"+coluna+"']").addClass('editando');
                $('#colunas').val(coluna);
                $("#numero").fadeOut(500);
            }
            
            setTimeout(function() {
                $("#carregando").fadeOut(500,function() {
                    $("#mapa").fadeIn(500,function() {
                        context.$data.loaded(true);
                    });
                });
            },1000);
            
            $.each(<?=json_encode($colunas)?>,function(i,coluna) {
                var option = $("<option/>",{
                    value : coluna.EventoMapaColuna.nome,
                    'data-nome' : coluna.EventoMapaColuna.nome,
                    text : "Coluna " + coluna.EventoMapaColuna.nome
                });
                $("#colunas").append(option);
                $.each(coluna.EventoMapaLocal,function(i,local) {
                    var width = parseFloat(local.width).toFixed(2);
                    if(diferenca != 0) {
                        width*= diferenca;
                        local.left*= diferenca;
                        local.top*= diferenca;
                    }
                    var div = $("<div>",{
                        class:'local',
                        style:'top:'+local.top+'px;left:'+
                                local.left+'px; width:'+
                                width+'px; height:'+width+'px',
                        'data-id' : local.id,
                        'data-numero' : local.numero,
                        'data-top' : local.top,
                        'data-left' : local.numero,
                        'data-width' : local.width,
                        'data-coluna' : coluna.EventoMapaColuna.nome,
                        'data-evento_mapa_coluna_id' : local.evento_mapa_coluna_id,
                    });
                    if(local.usuario_id != null) {
                        div.addClass('escolhido');
                        div.attr('title',local.Formando.codigo_formando + " - " +
                                local.Formando.nome);
                        div.tooltip();
                    }
                    div.appendTo($("#mapa"));
                    var tooltip = "<div class='coluna'>" + coluna.EventoMapaColuna.nome +
                            local.numero + "</div>";
                    div.append(tooltip);
                    criarDragLocal(div);
                    //div.draggable("disable");
                });
            });
            
            if($("#colunas option").length > 0)
                $('#colunas')
                    .val($(".local[data-coluna]:last").attr('data-coluna'))
                    .trigger('change');
        });
    });
</script>
<img src="/<?=$mapa['EventoMapa']['arquivo']?>" id="imagem-mapa" />
<div class="row-fluid" id="container-mapa" tabindex="0">
    <?php if($colunas) : ?>
    <div class="row-fluid">
        <div class="span4">
            <button type="button" class="bg-color-red input-block-level" dir="<?=$mapa['Evento']['id']?>"
                id="reiniciar-configuracao">
                Remover Mapa
            </button>
        </div>
    </div>
    <?php endif; ?>
    <div class="row-fluid">
        <div class="span2">
            <button type="button" class="default input-block-level colunas"
                id="novo-local">
                Novo
            </button>
            <?=$form->input(false,array(
                'type' => 'select',
                'title' => 'Nenhuma',
                'class' => 'hide',
                'disabled' => 'disabled',
                'id' => 'colunas',
                'data-width' => '100%',
                'label' => false,
                'div' => 'input-control text'
            )); ?>
        </div>
        <div class="span2">
            <button type="button" class="input-block-level colunas"
                id="copiar-coluna" disabled="disabled">
                Copiar
            </button>
        </div>
        <div class="span1">
            <?=$form->input(false,array(
                'label' => false,
                'div' => 'input-control',
                'class' => 'hide',
                'error' => false,
                'id' => 'numero',
                'alt' => '99',
                'maxlength' => 2,
                'type' => 'text'
            )); ?>
        </div>
        <div class='span5'>
            <div class="btn-group alinhamento pull-right" data-alinhamento='horizontal'>
                <button type="button" class='bg-color-blueDark' data-direcao='menos'>
                    <i class='icon-arrow-9'></i>
                </button>
                <button type="button" class='vertical'>
                    Coluna
                </button>
                <button type="button" class='bg-color-blueDark' data-direcao='mais'>
                    <i class='icon-arrow-6'></i>
                </button>
            </div>
            <div class="btn-group alinhamento pull-right"
                data-alinhamento='vertical' style="margin-right:10px">
                <button type="button" class='bg-color-blueDark mais' data-direcao='mais'>
                    <i class='icon-arrow-7'></i>
                </button>
                <button type="button" class='horizontal'>
                    Linha
                </button>
                <button type="button" class='bg-color-blueDark' data-direcao='menos'>
                    <i class='icon-arrow-8'></i>
                </button>
            </div>
        </div>
        <div class='span2'>
            <div class="btn-group alinhamento pull-right">
                <button type="button" class='bg-color-greenDark' data-tamanho="menor">
                    <i class='icon-minus-3'></i>
                </button>
                <button type="button" class='bg-color-greenDark' data-tamanho="maior">
                    <i class='icon-plus-3'></i>
                </button>
            </div>
        </div>
    </div>
    <div id="mapa" class="hide">
        <div id="lixeira" class="hide">
            <span class="icon-trash-stroke"></span>
        </div>
    </div>
</div>
<br />
<div class="row-fluid">
    <h3>Desalocar</h3>
    <strong class="fg-color-red">Para desalocar um usuário, basta clicar no círculo correspondente</strong>
</div>
<br />
<div class="row-fluid">
    <h3>Bot&otilde;es de ajuda</h3>
    <strong>Novo:</strong> Adiciona um local
    <br />
    <strong>Copiar:</strong> Copia a coluna ativa
    <br />
    <i class='icon-arrow-7'></i>: Move a linha ativa para cima
    <br />
    <i class='icon-arrow-8'></i>: Move a linha ativa para baixo
    <br />
    <strong>Linha:</strong> Organiza horizontalmente a linha ativa
    <br />
    <i class='icon-arrow-9'></i>: Move a coluna ativa para a esquerda
    <br />
    <i class='icon-arrow-6'></i>: Move a coluna ativa para a direita
    <br />
    <strong>Coluna:</strong> Organiza verticalmente a coluna ativa
</div>
<br />
<div class="row-fluid">
    <h3>Atalhos do teclado</h3>
    <strong>Duplo clique:</strong> Adiciona um local na posi&ccedil;&atilde;o clicada
    <br />
    <strong>Shift + Barra de espa&ccedil;o:</strong> Adiciona um local
    <br />
    <strong>Shift + Enter:</strong> Copia a coluna ativa
    <br />
    <strong>Shift + Delete:</strong> Remove um local
    <br />
    <strong>Teclas de dire&ccedil;&atilde;o:</strong> Move o local selecionado
    <br />
    <strong>Tab:</strong> Seleciona o p&oacute;ximo local
    <br />
    <strong>Shift + Tab:</strong> Seleciona o local anterior
</div>
<br />
<?php else : ?>
<h2 class="fg-color-red">Mapa n&atilde;o encontrado</h2>
<?php endif; ?>
