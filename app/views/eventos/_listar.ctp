<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/metroui/listview.css">
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/bootstrap/bootbox.js"></script>
<style type="text/css">
	iframe{
		border:dashed 3px #000000;
		-moz-border-radius-topleft: 6px;
		-moz-border-radius-topright:5px;
		-moz-border-radius-bottomleft:5px;
		-moz-border-radius-bottomright:5px;
		-webkit-border-top-left-radius:6px;
		-webkit-border-top-right-radius:5px;
		-webkit-border-bottom-left-radius:5px;
		-webkit-border-bottom-right-radius:5px;
		border-top-left-radius:6px;
		border-top-right-radius:5px;
		border-bottom-left-radius:5px;
		border-bottom-right-radius:5px;
	}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		$(".popover-evento").click(function() {
			bootbox.alert($("#"+$(this).attr('dir')).html());
			<?php if($this->params['prefix'] == 'planejamento'){ ?>
				id = $(this).attr('dir').substring(7);
				var alterar = $("<button>",{
					type: 'submit',
					class: 'button bg-color-orange',
					id:'alterar-evento',
					text:'Alterar'
				});
				$('.modal-footer').prepend(alterar);
				var extras = $("<button>",{
					type: 'submit',
					class: 'button bg-color-greenDark',
					id:'extras',
					text:'Extras'
				});
				$('.modal-footer').prepend(extras);
				$('#alterar-evento').click(function() {
					bootbox.hideAll();
					var context = ko.contextFor($(".metro-button.reload")[0]);
					var url = '<?="/{$this->params['prefix']}/eventos/alterar/"?>'+id;
					context.$data.showLoading(function() {
						$.ajax({
							url: url,
							type: "GET",
							complete : function() {
								context.$data.page('<?="/{$this->params['prefix']}/eventos/alterar/"?>'+id);
							}
						});
					});
				});
				$('#extras').click(function() {
					bootbox.hideAll();
					var context = ko.contextFor($(".metro-button.reload")[0]);
					var url = '<?="/{$this->params['prefix']}/extras/listar/"?>'+id;
					context.$data.showLoading(function() {
						$.ajax({
							url: url,
							type: "GET",
							complete : function() {
								context.$data.page('<?="/{$this->params['prefix']}/extras/listar/"?>'+id);
							}
						});
					});
				});
				<?php } ?>
			});
});
</script>
<?php $session->flash(); ?>
<div class="row-fluid">
	<div class="span12">
		<h2>
			<a class="metro-button reload" data-bind="click: function() { reload() }"></a>
			Eventos Turma <?=$turma['Turma']['id']?>
			<?php if($this->params['prefix'] == 'planejamento') { ?>
			<a class="button mini bg-color-red pull-right"
			data-bind="click: function() {
			page('<?= "/{$this->params['prefix']}/eventos/inserir"?>') }">
			Inserir Evento
			<i class='icon-location'></i>
		</a>
		<?php } ?>
	</h2>
</div>
</div>
<div class="row-fluid">  
	<?php if(!empty($eventos)){ ?>
	<div class="span12">
		<ul class="listview fluid">
			<?php foreach($eventos as $evento): ?>
				<li class="bg-color-red fg-color-white popover-evento" title="<?=$evento['TiposEvento']['nome']?>"
					dir="evento-<?=$evento['Evento']['id']?>">
					<div class="icon">
						<img src="http://macmagazine.com.br/wp-content/uploads/2012/12/13-googlemaps_icon-256x256.png" width="64">
					</div>
					<div class="data">
						<h4 class="fg-color-white"><?=$evento['Evento']['nome'];?></label>
							<p><em><?=$evento['TiposEvento']['nome']?></em></p>
							<p>
								<?=date('d/m/Y',strtotime($evento['Evento']['data']));?>
							</p>
						</div>
					</li>
					<div class="hide" id="evento-<?=$evento['Evento']['id']?>">
						<div class="row-fluid" dir="<?=$evento['Evento']['id']?>">
							<div class="span6">
								<span>Evento</span>
								<label class="strong item"><?=$evento['Evento']['nome'];?></label>
							</div>
							<div class="span6">
								<span>Tipo de Evento</span>
								<label class="strong item"><?=$evento['TiposEvento']['nome'];?></label>
							</div>
						</div>
						<div class="row-fluid">
							<div class="span6">
								<span>Local</span>
								<label class="strong item"><?=$evento['Evento']['local'];?></label>
							</div>
							<div class="span6">
								<span>Endere&ccedil;o</span>
								<label class="strong item"><?=$evento['Evento']['local_endereco'];?></label>
							</div>
						</div>
						<div class="row-fluid">
							<div class="span6">
								<span>Convites Formandos</span>
								<label class="strong item"><?=$evento['Evento']['convites_formandos']; ?></label>
							</div>
							<div class="span6">
								<span>Convites Extras</span>
								<label class="strong item"><?=$evento['Evento']['convites_extras']; ?></label>
							</div>
						</div>
						<div class="row-fluid">
							<div class="span6">
								<span>Início do Evento</span>
								<label class="strong item"><?=date('d/m/Y - H:i',strtotime($evento['Evento']['data']));?></label>
							</div>
							<div class="span6">
								<span>Fim do Evento</span>
								<label class="strong item"><?=date('d/m/Y - H:i',strtotime($evento['Evento']['data_fim']));?></label>
							</div>
						</div>
						<?php if(!in_array($this->params['prefix'], array('formando', 'comissao'))) : ?>
							<div class="row-fluid">
								<div class="span6">
									<span>Banda</span>
									<label class="strong item"><?=$evento['Evento']['banda'];?></label>
								</div>
								<div class="span6">
									<span>Atra&ccedil;&atilde;o</span>
									<label class="strong item"><?=$evento['Evento']['atracoes'];?></label>
								</div>
							</div>
							<div class="row-fluid">
								<div class="span6">
									<span>Buffet</span>
									<label class="strong item"><?=$evento['Evento']['buffet'];?></label>
								</div>
								<div class="span6">
									<span>Bar</span>
									<label class="strong item"><?=$evento['Evento']['bar'];?></label>
								</div>
							</div>
							<div class="row-fluid">
								<div class="span6">
									<span>Brindes</span>
									<label class="strong item"><?=$evento['Evento']['brindes'];?></label>
								</div>
								<div class="span6">
									<span>Outras Inform&ccedil;&otilde;es</span>
									<label class="strong item"><?=$evento['Evento']['outras_informacoes'];?></label>
								</div>
							</div>
						<?php endif; ?>
						<br/>
						<div class="row-fluid" style="min-height: 400px">
							<div class="span12" align='center'>
								<label class="strong">Mapa</label>
								<br />
								<?php if(!empty($evento['Evento']['local_mapa'])) : ?>
									<?php echo $evento['Evento']['local_mapa'];?>
								<?php else : ?>
									<label class="strong">Mapa do local n&atilde;o dispon&iacute;vel</label>
								<?php endif; ?>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
	<?php }else{ ?>
	<h2 class="fg-color-red">Não há eventos cadastrados para esta turma.</h2>
	<?php } ?>