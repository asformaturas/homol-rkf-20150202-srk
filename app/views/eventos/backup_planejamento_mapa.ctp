<?php $session->flash(); ?>
<?php if($evento) : ?>
<script type="text/javascript" src="<?= $this->webroot ?>js/mapas-de-mesas/jcanvas.min.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>js/mapas-de-mesas/main.js"></script>
<script type="text/javascript">

    $(document).ready(function() {

        palco = new MapaDeMesa.PalcoMapaMesa("meucanvas");
        fabrica = new MapaDeMesa.MesaFactory(palco);
        

        var urlConf = $("#formulario").attr('action') + "_selecionar_locais/";
        
        $('.fileupload').bind('loaded',function(e) {

            var src = e.imagem.replace(/^data:image\/(gif|png|jpe?g);base64,/, "");
            if(src) {
                $("#src").val(src);
                $("#configurar-mapa").removeClass('hide');

            } else {
                $("#src").val('');
                $("#configurar-mapa").addClass('hide');
            }
            return;
        });
        
        



        $("input.criar").click(function() {
            
            linha = $(".container-linhas .linha").val();
            coluna = $(".container-linhas .coluna").val();
            
            grupo = fabrica.criarGrupoMesa (  linha, coluna );
            palco.adicionarObjeto(grupo);

            $(".container-linhas").fadeTo("fast",0);
            $(".container-toolbar").fadeTo( "fast", 1 ); 

             habilitarRotacionar();

            });
        

        /**
         * Salvar o mapa 
         * 
         */

        $("input#salvarComposicao").click(function() {
            
            var layout = new MapaDeMesa.GerenciadorLayout(palco);
            var json = layout.serializarLayout();    
            console.log(json);

            $('#mapalayout').val(json);

             $.ajax ({
                        //prefixo, controller, action e parametro
                        url :    'planejamento/eventos/salvar_layout/',
                        data : {
                            MapaLayout : json,
                            TurmaMapaID : $("#turmamapaID").val()
                        },
                        type : "POST",
                        complete: function(response) { 
                                $('#conteudo').html( response.responseText);
                        }

                    });

        });
        

        /**
         * Clique do botao rotacionar
         * Voltar
         */

        $(".rotacionar #avancar").click(function() {
            
            rotacaoAtual = palco.grupoSelecionado.rotacao;   
            palco.grupoSelecionado.rotacionar(rotacaoAtual+15).desenhar();

        });
        /**
         * Clique do botao rotacionar
         * Avancar
         */

        $(".rotacionar #voltar").click(function() {

            rotacaoAtual = palco.grupoSelecionado.rotacao;
            palco.grupoSelecionado.rotacionar(rotacaoAtual-15).desenhar();

        });


        /**
         * Container Toolbar, ao clicar a div aonde tem os elementos de linha
         * e coluna aparecem
         */
        $("input.criargrupo").click(function() {


            $(".container-linhas").removeClass('hide');
            $(".container-linhas").fadeTo("fast",1);
            desabilitarRotacionar ();

        });

        //$(".container-toolbar").fadeTo( "fast", 1 ); 
        desabilitarRotacionar ();

        function habilitarRotacionar (){
            $(".rotacionar").fadeTo( "fast", 1 ); 
            $(".rotacionar button").prop( "disabled", false );  
        }

        function desabilitarRotacionar () {
            $(".rotacionar").fadeTo( "fast", 0.5 ); 
            $(".rotacionar button").prop( "disabled", true );     
        }


        /**
         * Cancelar Grupo
         * Ao clicar, some o container, e zera os inputs
         * Efeito de over e out, habilitando e desabilitando o rotacionar
         */        

        $("input.cancelargrupo").click(function() {
           
            $(".container-linhas").addClass('hide');
            $(".container-linhas .linha").val('');
            $(".container-linhas .coluna").val('');

            $(".rotacionar").fadeTo( "fast", 1 );
            $(".container-toolbar").fadeTo( "fast", 1 );
            $(".rotacionar").prop( "disabled", false );

        });

        $("#configurar-mapa").click(function() {
            var context = ko.contextFor($(".metro-button.reload")[0]);
            dir = $(this).attr('dir');
            context.$data.showLoading(function() {

                var url = '/<?=$this->params['prefix']?>/eventos/mapa_selecionar_locais/' + dir;
                if($("#src").val() == '') {
                    context.$data.page(url);
                } else {
                    var dados = {
                        src : $("#src").val()
                    };

                  $.ajax ({
                        //prefixo, controller, action e parametro
                        url :    'planejamento/eventos/salvar_layout/',
                        
                        data : {
                            MapaLayout : $("#mapalayout").val(),
                            TurmaMapaID : $("#turmamapaID").val()
                        },

                        type : "POST",

                        complete: function(response) { 
                                $('#conteudo').html( response.responseText);
                        }

                    });

                }
            });
        });
    });
</script>
<div class="row-fluid">
    <h2>
        <a class="metro-button reload" data-bind="click: reload"></a>
        Mapa de Mesa <img src=<?php echo $webroot; ?>mapas/<?php echo $mapa_src;  ?> height="1" width="1" />
    </h2>
</div>
<h2 class="fg-color-red">
    <?="{$evento['TiposEvento']['nome']} - {$evento['Evento']['nome']}"?>
</h2>
<br />
<?=$form->hidden(false,array('id' => 'src'));?>
<div class="row-fluid">
    

    <div class="span9">
    <canvas width="900" id="meucanvas" height="400" style="border:1px solid #666;">
    </canvas>
    </div>

    <div class="span3">

        <div class="container-form-canvas">
   
            <!-- formulário do componente -->
            <form id="componente" action=""/>
                <input type="hidden" id="mapalayout" name="mapalayout" value="" />
                <input type="hidden" id="turmamapaID" name ="turmamapaID" value="<?php echo $turmaID; ?>" />
                
                <!-- Componente de linhas e colunas -->
                <div class="container-linhas hide"> 
                    <label>Linha X Coluna</label>
                    <p>
                        <input type="text" class="linha" maxlength="2" style="width:25px;"/>
                        <input type="text" class="coluna"  maxlength="2" style="width:25px;"/>
                    </p>
                    <input type="submit" id="criarObjetosHandler" class="criar" value="Criar">
                    <input class="cancelargrupo" id="cancelarObjetosHandler" type="submit" value="Cancelar">
                </div>
                
                <!-- Componente de rotação -->
                <div id="rotacionarHandler">
                <label>Rotacionar</label>
                <div class="toolbar-group fg-black">
                    <button id="voltar"><i class="icon-undo"></i></button>
                    <button id="avancar"><i class="icon-redo"></i></button>
                </div>
                </div>

                <!-- Container Toolbar // Criar e Salvar -->
                <div class="container-toolbar">
                    <input id="criarPainelHandler" type="submit" value="Criar Grupo">
                    <input id="salvarHandler" type="submit" value="Salvar composição">
                </div> 
            

            </form> 
        </div>
    </div>
</div>
<?php else : ?>
<h2 class="fg-color-red">Nenhuma festa encontrada para esta turma</h2>
<?php endif; ?>

