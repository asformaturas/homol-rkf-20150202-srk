<span id="conteudo-titulo" class="box-com-titulo-header">Evento</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<div class="detalhes">
		<?php include('_visualizar.ctp');?>
		<p class="grid_6 alpha omega">
			<?php echo $html->link('Editar',array($this->params['prefix'] => true, 'controller' => 'eventos' , 'action' => 'editar', $evento['Evento']['id']) ,array('class' => 'submit')); ?>
			<?php echo $html->link('Excluir',array($this->params['prefix'] => true, 'controller' => 'eventos' , 'action' => 'excluir', $evento['Evento']['id']) ,array('class' => 'submit')); ?>
			<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'action' => 'index') ,array('class' => 'cancel')); ?>
		</p>
		
	</div>
</div>
