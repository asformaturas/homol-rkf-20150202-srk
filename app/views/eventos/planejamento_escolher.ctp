<span id="conteudo-titulo" class="box-com-titulo-header">Adicionar Evento</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<?php echo $form->create('EscolherTipo', array('url' => "/{$this->params['prefix']}/eventos/escolher")); ?>
	
	<p class="grid_11 alpha omega">
		<label class="grid_11 alpha">Tipo de Evento</label>
		<?php echo $form->select('tipos_evento_id', $tiposeventos, array('selected' => $this->data['Evento']['tipo_evento_id'] ), array('empty' => null, 'class' => 'grid_4 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_5'))); ?>
	</p>


	<p class="grid_11 alpha omega">
		<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'action' => 'index') ,array('class' => 'cancel')); ?>
		<?php echo $form->end(array('label' => 'Continuar', 'div' => false, 'class' => 'submit'));?>
	</p>
</div>