<span id="conteudo-titulo" class="box-com-titulo-header">Adicionar Evento</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<?php echo $form->create('Evento', array('url' => "/{$this->params['prefix']}/eventos/adicionar/{$tipoEvento['TiposEvento']['id']}")); ?>
	
	<p class="grid_11 alpha omega">
		Tipo de Evento: <b><?php echo($tipoEvento['TiposEvento']['nome']);?></b>
	</p>
	
	<?php include('_form.ctp');?>
	
	<p class="grid_11 alpha omega">
		<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'action' => 'index') ,array('class' => 'cancel')); ?>
		<?php echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'submit'));?>
	</p>
</div>