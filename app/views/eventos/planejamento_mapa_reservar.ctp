<style type="text/css">
#mapa {
    position:relative;
    margin-top: 20px;
    background-repeat:no-repeat;
    background-size: 100% auto;
    cursor:pointer;
}
.local { position: absolute; box-sizing: border-box; background:green;
    width:17px; height: 17px; display:inline-block;
    border:none; border-radius: 50%; }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        
        $("#imagem-mapa").load(function() {
            
            var context = ko.contextFor($("#content-body")[0]);
            var largura = $(this).width();
            var altura = $(this).height();
            
            $("#mapa")
                    .css('background-image',"url("+$(this).attr('src')+")")
                    .width(largura)
                    .height(altura);
            
            $("#mapa").on('click','.local:not(.escolhido)',function(e) {
                e.stopPropagation();
                var local = $(this);
                var reservado = $(this).attr('data-reservar') || $(this).data('reservado');
                reservado = reservado == 1 ? 0 : 1;
                
                if(reservado == 1)
                    $(this).addClass('bg-color-yellow');
                else
                    $(this).removeClass('bg-color-yellow');
                
                if(reservado == $(this).data('reservado'))
                    $(this).removeAttr('data-reservar');
                else
                    $(this).attr('data-reservar',reservado);
                
                if($('[data-reservar]').length > 0)
                    $("#enviar-reservas").fadeIn(500);
                else
                    $("#enviar-reservas").fadeOut(500);
            });
            
            $("#enviar-reservas").click(function() {
                var data = {
                    reservas : []
                };
                $('[data-reservar]').each(function() {
                    data.reservas.push({
                        EventoMapaLocal : {
                            id : $(this).data('id'),
                            reservado : $(this).attr('data-reservar')
                        }
                    });
                });
                context.$data.showLoading(function() {
                    $.ajax({
                        url : '<?=$this->here?>',
                        data : {
                            data : data
                        },
                        type : "POST",
                        dataType : "json",
                        complete : function(response) {
                            context.$data.reload();
                        }
                    });
                });
            });
            
            setTimeout(function() {
                $("#carregando").fadeOut(500,function() {
                    $("#mapa").fadeIn(500,function() {
                        context.$data.loaded(true);
                    });
                });
            },1000);
            
            $.each(<?=json_encode($colunas)?>,function(i,coluna) {
                $.each(coluna.EventoMapaLocal,function(i,local) {
                    var width = parseFloat(local.width).toFixed(2);
                    var div = $("<div>",{
                        class:'local',
                        style:'top:'+local.top+'px;left:'+
                                local.left+'px; width:'+
                                width+'px; height:'+width+'px',
                        'data-id' : local.id,
                        'data-reservado' : local.reservado
                    });
                    if(local.usuario_id != null) {
                        div.addClass('escolhido').addClass('bg-color-darken');
                        div.attr('title',local.Formando.codigo_formando + " - " +
                                local.Formando.nome);
                        div.tooltip();
                    } else if(local.reservado == 1) {
                        div.addClass('reservado').addClass('bg-color-yellow');
                    }
                    div.appendTo($("#mapa"));
                });
            });
        });
    });
</script>
<div class="row-fluid">
    <h2>
        <a class="metro-button reload" data-bind="click: reload"></a>
        Reservar lugares
        <span class="label label-inverse" id="carregando">Carregando</span>
        <button type="button" class="pull-right mini default hide"
            id="enviar-reservas">
            Enviar
        </button>
    </h2>
</div>
<img src="/<?=$mapa['EventoMapa']['arquivo']?>" id="imagem-mapa" class="hide" />
<?php $session->flash(); ?>
<div class="row-fluid">
    <div id="mapa" class="hide"></div>
</div>