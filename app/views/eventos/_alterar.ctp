<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/metroui/listview.css">
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/bootstrap/bootbox.js"></script>
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/bootstrap/bootstrap-timepicker.min.css">
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/bootstrap/datepicker.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/datepicker.js"></script>
<style type="text/css">
    .chzn-single{
        width: 206px!important;
        height: 28px
    }
    .chzn-drop{
        width: 206px!important;
    }
    #dsada{
        width: 150px!important
    }
</style>
<script type="text/javascript">
$(document).ready(function() {

    $('#duracao').timepicker({
        minuteStep: 15,
        showMeridian: false
    });
    
    $('#valsa').timepicker({
        minuteStep: 15,
        showMeridian: false
    });
    
    $('#hora').timepicker({
        minuteStep: 15,
        showMeridian: false
    });
    
    $('#data-hora').datepicker()
    
    $("#seletor").selectpicker({width:'68%'});
    
    $("#beca").selectpicker({width:'100%'});  
    
    $("#colacao").hide();
        
    $("#seletor").change(function(){
        selecionaTipo();
    });
    
    $("#alerta").click(function(){
        if($(this).is(":checked")){
            $(this).next().attr('value', 1);
        }else{
            $(this).next().attr('value', 0);
        }
    });
    
    function selecionaTipo(){
        if($('#seletor option').filter(':selected').text() == "Festa de Formatura"){
            $("#festa").fadeIn("slow");
            $("#colacao").fadeOut("slow");
        }
        if($('#seletor option').filter(':selected').text() == "Colação de Grau"){
            $("#colacao").fadeIn("slow");
            $("#festa").fadeOut("slow");
        }
        if(($('#seletor option').filter(':selected').text() != "Colação de Grau") && 
            ($('#seletor option').filter(':selected').text() != "Festa de Formatura")){
                $("#colacao").fadeOut("slow");
                $("#festa").fadeOut("slow");
        }
        if($('#seletor option').filter(':selected').text() == 'Outros'){
            $('.outroevento').fadeIn('slow');
        }else{
            $('#TiposEventoNome').attr('value', '');
            $('.outroevento').fadeOut('slow');
        }
    }

    selecionaTipo();
    
    $("#form").submit(function(e) {
            e.preventDefault();
            var context = ko.contextFor($(".metro-button.reload")[0]);
            context.$data.showLoading(function() {
            var dados = $("#form").serialize();
            var url = '<?="/{$this->params['prefix']}/eventos/alterar/"?>' + $('#EventoId').val();
            $.ajax({
                url: url,
                data: dados,
                type: "POST",
                dataType: "json",
                complete: function() {
                    context.$data.page('/<?=$this->params['prefix']?>/eventos/listar');
                }
            });
            });
    });
});
</script>
<div class="row-fluid">
	<div class="span12">
		<h2>
			<a class="metro-button reload" data-bind="click: function() { reload() }"></a>
			Alterar Evento
		</h2>
	</div>
</div>
<?php $session->flash(); ?>
<?php echo $form->create('Evento', array('url' => "/{$this->params['prefix']}/eventos/alterar/", 'id' => 'form')); ?>
<?=$form->hidden('Evento.id'); ?>
<div class="row-fluid">	
    <div class="row-fluid">	
            <div class="span4">
                <?=$form->input('tipos_evento_id',array(
                'label' => 'Tipo de Evento',
                'type' => 'select',
                'class' => 'selectpicker',
                'options' => $tiposeventos,
                'selected' => $tipoescolhido['TiposEvento']['id'],
                'div' => 'input-control text',
                'error' => false,
                'id' => 'seletor')); ?>
            </div>
            <div class="span4 hide outroevento">
                <label>Digite</label>
                <?php echo $form->input('TiposEvento.nome', array('class' => 'input-control text', 'label' => false, 'div' => false, 'error' => false)); ?>
            </div>
            <div class="span5">
                <label>&nbsp</label>
                <label class="input-control checkbox">
                    <input type="checkbox" id="alerta" <?=($checked == 1) ? "checked" : ""?>>
                    <input type="hidden" name="data[Evento][alerta_telao]" value="<?=($checked == 1) ? 1 : 0?>"/>
                    <span class="helper">&nbsp;Ativar alerta para envio de fotos do telão?</span>
                </label>
                <label for="aceitar-contrato">&nbsp;</label>
            </div>
    </div>
    <div class="row-fluid">
        <div class="span4">
                <label>Nome</label>
                <?php echo $form->input("Evento.nome", 
                                  array('value' => $evento['Evento']['nome'], 'class' => 'input-control text', 'label' => false, 'div' => false, 'error' => false));?>
        </div>
        <div class="span4">
                <label>Local</label>
                <?php echo $form->input("Evento.local", 
                                  array('value' => $evento['Evento']['local'], 'class' => 'input-control text', 'label' => false, 'div' => false, 'error' => false));?>
        </div>
        <div class="span4">
                <label>Endereço do Local</label>
                <?php echo $form->input("Evento.local_endereco", 
                                  array('value' => $evento['Evento']['local_endereco'], 'class' => 'input-control text', 'label' => false, 'div' => false, 'error' => false));?>
        </div>
    </div>
    <div class="row-fluid">
            <div class="span12">
                <label>Mapa do Local</label>
                <?php echo $form->input("Evento.local_mapa", 
                                  array('value' => $evento['Evento']['local_mapa'], 'class' => 'input-control text', 'label' => false, 'div' => false, 'error' => false, 'style' => 'width: 785px'));?>
            </div>
    </div>
    <div class="row-fluid">
        <div class="span4">
                <div class="input-append bootstrap-timepicker">
                    <label>Horário de Inicio</label>
                    <?php echo $form->text("Evento.hora", 
                                    array('value' => $evento['Evento']['hora'], 'class' => 'input-control text', 
                                          'label' => false, 'div' => false, 'error' => false, 'id' => 'hora'));?>
                    <span class="add-on"><i class="icon-clock-2"></i></span>
                </div>
        </div>
        <div class="span4">
                <div class="input-append bootstrap-timepicker">
                    <label>Horário de Fim</label>
                    <?php echo $evento['Evento']['hora']; ?>
                    <?php echo $form->text("Evento.hora_fim", 
                                    array('value' => $evento['Evento']['hora_fim'], 'class' => 'input-control text', 
                                          'label' => false, 'div' => false, 'error' => false, 'id' => 'duracao'));?>
                    <span class="add-on input"><i class="icon-clock-2"></i></span>
                </div>
        </div>
        <div class="span4">
                  <label>Data</label>
                <?php echo $form->input("Evento.data-hora", 
                                  array('class' => 'input-control text', 
                                      'label' => false, 'div' => false, 'error' => false, 'id' => 'data-hora'));?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span3">
                <label>Convites Formandos</label>
                <?php echo $form->input("Evento.convites_formandos", 
                                  array('value' => $evento['Evento']['convites_formandos'], 'class' => 'input-medium text', 'label' => false, 'div' => false, 'error' => false, 'style' => 'width: 134px'));?>
        </div>
        <div class="span3">
                <label>Convites Extras</label>
                <?php echo $form->input("Evento.convites_extras", 
                                  array('value' => $evento['Evento']['convites_extras'], 'class' => 'input-medium text', 'label' => false, 'div' => false, 'error' => false, 'style' => 'width: 134px'));?>
        </div>
        <div class="span3">
                <label>Traje</label>
                <?php echo $form->input("Evento.traje", 
                                  array('value' => $evento['Evento']['traje'], 'class' => 'input-medium text', 'label' => false, 'div' => false, 'error' => false, 'style' => 'width: 134px'));?>
        </div>
        <div class="span3">
                <label>Banda</label>
                <?php echo $form->input("Evento.banda", 
                                  array('value' => $evento['Evento']['banda'], 'class' => 'input-medium text', 'label' => false, 'div' => false, 'error' => false, 'style' => 'width: 134px'));?>
        </div>
    </div>
    <div class="row-fluid" id="festa">
        <div class="span4">
                <div class="input-append bootstrap-timepicker">
                    <label>Horário da Valsa</label>
                    <?php echo $form->input("Evento.horario_valsa_aux", 
                                  array('value' => $evento['Evento']['horario_valsa'], 'class' => 'input-control text', 
                                        'label' => false, 'div' => false, 'error' => false, 'id' => 'valsa'));?>
                    <span class="add-on"><i class="icon-clock-2"></i></span>
                </div>
        </div>
    </div>
    <div class="row-fluid" id="colacao">
            <div class="span3">
                <?=$form->input('tipo_beca',array(
                'label' => 'Beca',
                'type' => 'select',
                'class' => 'selectpicker',
                'options' => array('americana','tradicional'),
                'div' => 'input-control text',
                'error' => false,
                'id' => 'beca')); ?>
            </div>
        
        <div class="span4">
                <label>Cor da Faixa</label>
                <?php echo $form->input("Evento.cor_da_faixa", 
                                  array('value' => $evento['Evento']['cor_da_faixa'], 'class' => 'input-control text', 'label' => false, 'div' => false, 'error' => false));?>
        </div>
        
        <div class="span4">
                <label>Capelo</label>
                <?php echo $form->input("Evento.capelo", 
                                  array('value' => $evento['Evento']['capelo'], 'class' => 'input-control text', 'label' => false, 'div' => false, 'error' => false));?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span6">
                <label>Atrações</label>
                <?php echo $form->textarea("Evento.atracoes", 
                                  array('value' => $evento['Evento']['atracoes'], 'class' => 'input-control text', 'label' => false, 'div' => false, 'error' => false, 'style' => 'width: 350px; height: 124px'));?>
        </div>

        <div class="span6">
                <label>Buffet</label>
                <?php echo $form->textarea("Evento.buffet", 
                                  array('value' => $evento['Evento']['buffet'], 'class' => 'input-control text', 'label' => false, 'div' => false, 'error' => false, 'style' => 'width: 350px; height: 124px'));?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span6">
                <label>Bar</label>
                <?php echo $form->textarea("Evento.bar", 
                                  array('value' => $evento['Evento']['bar'], 'class' => 'input-control text', 'label' => false, 'div' => false, 'error' => false, 'style' => 'width: 350px; height: 124px'));?>
        </div>

        <div class="span6">
                <label>Brindes</label>
                <?php echo $form->textarea("Evento.brindes", 
                                  array('value' => $evento['Evento']['brindes'], 'class' => 'input-control text', 'label' => false, 'div' => false, 'error' => false, 'style' => 'width: 350px; height: 124px'));?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <label>Outras Informações</label>
            <?php echo $form->input("Evento.outras_informacoes", 
                                  array('value' => $evento['Evento']['outras_informacoes'], 'class' => 'input-control text', 'label' => false, 'div' => false, 'error' => false, 'style' => 'width: 865px'));?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <label>Informações Fotógrafos</label>
            <?php echo $form->input("Evento.informacoes_fotografos",
                                  array('value' => $evento['Evento']['informacoes_fotografos'], 'class' => 'input-control text', 'label' => false, 'div' => false, 'error' => false, 'style' => 'width: 865px'));?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <?php echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'button default bg-color-green', 'style' => 'margin-right: 60px'));?>
        </div>
    </div>
</div>