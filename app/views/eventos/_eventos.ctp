<?php setlocale (LC_ALL, 'pt_BR'); ?>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.selectpicker').selectpicker();
        $('.mapa').click(function() {
        var map = $('mapfull').removeClass('hide').show();
            bootbox.dialog('Carregando',[{
                label: 'Fechar'
            }],{
                remote: '<?="/{$this->params['prefix']}/eventos/mapa_eventos/"?>'+
                        $(this).attr('dir')
            });
        });
        $("#form-filtro").submit(function(e){
            e.preventDefault();
            var context = ko.contextFor($(".metro-button.reload")[0]);
            context.$data.showLoading(function() {
                $("#form-filtro").find('[value="false"]').attr('disabled','disabled');
                var dados = $("#form-filtro").serialize();
                var url = $("#form-filtro").attr('action');
                $.ajax({
                    url : url,
                    data : dados,
                    type : "POST",
                    dataType : "json",
                    complete : function() {
                        context.$data.reload();
                    }
                });
            });
        });
    });
</script>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Eventos do mês de <?php echo strftime("%B");?>
        </h2>
    </div>
</div>
<?php
$paginator->options(array(
    'url' => array($this->params['prefix'] => true)));
$sortOptions = array('data-bind' => 'click: loadThis');
$buttonAfter = '<button class="helper" onclick="return false" ' .
    'tabindex="-1" type="button"></button>';
?>
<div class="row-fluid">
    <?=$form->create('Evento',array(
        'url' => "/{$this->params['prefix']}/eventos/eventos",
        'id' => 'form-filtro')) ?>
    <div class="row-fluid">
        <div class="span3">
            <?=$form->input('mes',array(
                'options' => $meses,
                'type' => 'select',
                'class' => 'selectpicker',
                'data-width' => '100%',
                'label' => 'Escolha o mês:',
                'div' => 'input-control text')); ?>
        </div>

        <div class="span9">
            <label>&nbsp;</label>
            <button type='submit' class='button max bg-color-red'>
                Filtrar
                <i class='icon-search-2'></i>
            </button>
        </div>
    </div>
    <div class="row-fluid">
        <?php if (sizeof($eventos > 0)) : ?>
            <table class="table table-condensed table-striped">
                <thead>
                    <tr>
                        <th scope="col" width="5%"><?=$paginator->sort('Turma', 'Turma.id',$sortOptions); ?></th>
                        <th scope="col" width="20%"><?=$paginator->sort('Nome', 'Turma.nome',$sortOptions); ?></th>
                        <th scope="col" width="20%"><?=$paginator->sort('Tipo Evento', 'TiposEvento.nome',$sortOptions); ?></th>
                        <th scope="col" width="20%"><?=$paginator->sort('Data', 'Evento.data',$sortOptions); ?></th>
                        <th scope="col" width="15%"><?=$paginator->sort('Local', 'Evento.local',$sortOptions); ?></th>
                        <th scope="col" width="20%">&nbsp</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($eventos as $ev) : ?>
                    <tr>
                        <td><?=$ev['Turma']['id']; ?></td>
                        <td><?=$ev['Turma']['nome']; ?></td>
                        <td><?=$ev['TiposEvento']['nome']; ?></td>
                        <td><?=date('d/m/Y &\a\g\r\a\v\e;\s H:i:s',strtotime($ev['Evento']['data']))?></td>
                        <td><?=$ev['Evento']['local']; ?></td>
                        <td style="text-align: center">
                            <button type="button" class="mini bg-color-yellow mapa" dir="<?=$ev['Evento']['id']?>">
                                    Exibir Mapa<i class="icon-map"></i>
                            </button>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="5" class="paginacao">
                            <?=$paginator->numbers(array('separator' => ' ','data-bind' => 'click: loadThis')); ?>
                        </td>
                        <td colspan="5">
                            <?=$paginator->counter(array('format' => 'Total : %count% ' .  'Eventos')); ?>
                        </td>
                    </tr>
                </tfoot>
            </table>
        <?php else : ?>
            <h2 class="fg-color-red">Nenhum Evento Encontrado</h2>
        <?php endif; ?>
    </div>
</div>
