<script type="text/javascript">
$(document).ready(function() {
    $("#form-filtro").submit(function(e) {
        e.preventDefault();
        var context = ko.contextFor($(".metro-button.reload")[0]);
        context.$data.showLoading(function() {
            $("#form-filtro").find('[value="false"]').attr('disabled','disabled');
            var dados = $("#form-filtro").serialize();
            var url = $("#form-filtro").attr('action');
            $.ajax({
                url : url,
                data : dados,
                type : "POST",
                dataType : "json",
                complete : function() {
                    context.$data.reload();
                }
            });
        });
    })
})
</script>
<h2>
    <a class="metro-button reload" data-bind="click: reload"></a>
    Parceiros
    <a href="/marketing/parceiros/editar" data-bind="click: loadThis"
        class="button mini default pull-right">
        Adicionar
    </a>
</h2>
<?=$form->create('Parceiro',array(
    'url' => $this->here,
    'id' => 'form-filtro')) ?>
<div class="row-fluid">
    <div class="span3">
        <label>Filtrar</label>
        <div class="input-control text">
            <input type="text" value="<?=isset($filtro) ? $filtro : ""?>" name="data[filtro]" />
        </div>
    </div>
    <div class="span3">
        <label>&nbsp;</label>
        <button type='submit' class='mini max bg-color-red'>
            Filtrar
            <i class='icon-search-2'></i>
        </button>
    </div>
</div>
<?= $form->end(array('label' => false,
    'div' => false, 'class' => 'hide')); ?>
<div class="row-fluid">
    <?php if (sizeof($parceiros) > 0) : ?>
    <?php $sortOptions = array('data-bind' => 'click: loadThis'); ?>
    <?php $paginator->options(array('url' => array(
        $this->params['prefix'] => true
            ))); ?>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th>
                    <?=$paginator->sort('Parceiro', 'Parceiro.id',$sortOptions); ?>
                </th>
                <th>
                    <?=$paginator->sort('Ativo', 'Parceiro.id',$sortOptions); ?>
                </th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($parceiros as $parceiro) : ?>
            <tr>
                <td><?=$parceiro['Parceiro']['nome']?></td>
                <td><?=$parceiro['Parceiro']['ativo'] == 1 ? "Sim" : "Não"?></td>
                <td>
                    <button class="button mini default" type="button"
                        data-bind="click: loadThis"
                        href="/<?=$this->params['prefix']?>/parceiros/editar/<?=$parceiro['Parceiro']['id']; ?>">
                        Editar
                    </button>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4" class="paginacao">
                    <?=$paginator->numbers(array('separator' => ' ',
                        'data-bind' => 'click: loadThis')); ?>
                </td>
            </tr>
        </tfoot>
    </table>
    <?php else : ?>
    <?php endif; ?>
</div>