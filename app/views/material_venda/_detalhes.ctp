<?php if($catalogo) : ?>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/owl/owl.carousel.css">
<script type="text/javascript" src="<?=$this->webroot ?>metro/owl/owl.carousel.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var unica = $('#carousel-unica');
        unica.owlCarousel({
            loop:true,
            margin:0,
            nav : true,
            navText: ["<span class='faicon faicon-chevron-circle-left'></span>",
                "<span class='faicon faicon-chevron-circle-right'></span>"],
            items : 1
        });
        var lista = $('#carousel-lista');
        lista.owlCarousel({
            loop:true,
            margin:10,
            nav : true,
            navText: ["<span class='faicon faicon-angle-left text-danger'></span>",
                "<span class='faicon faicon-angle-right text-danger'></span>"],
            items : 4
        });
        $('#carousel-lista').on('click','.owl-item',function(e) {
            unica.trigger('to.owl.carousel', [$(e.currentTarget).data("owlItem").index,300]);
        });
    });
</script>
<style type="text/css">
.carousel-wrap { border:solid 1px #ddd }
#carousel-lista { padding:7px 40px; border-top:solid 1px #ddd }
.owl-carousel .owl-controls .owl-nav div { position: absolute; top:0;
    height:100%; background:transparent; text-align: center; width:30px; }
#carousel-unica .owl-controls .owl-nav div { background:rgba(255,255,255,.4); width:7%; }
.owl-carousel .owl-controls .owl-nav .owl-prev { left:0; }
.owl-carousel .owl-controls .owl-nav .owl-next { right:0; }
.owl-carousel .owl-controls .owl-nav .faicon { position:relative; top:50%;
    margin-top:-1em; font-size:3em; }
#carousel-unica .owl-controls .owl-nav .faicon { font-size:2.5em; }
#carousel-lista img { border:solid 1px #eee }
.anexo { font-size:.8em; padding:4px 0; line-height: 1em;
    text-align: center }
.anexo:hover { text-decoration: none }
.anexo .nome { font-size:1em }
.anexo img { margin:2px 0 }
.anexo .faicon { font-size: .7em }
#menu-topo ul li.active a { background-color: #15008F!important }
</style>
<div class="linha-titulo">
    <div class="text-danger text-uppercase titulo">
        <?=$catalogo["MaterialVenda"]["nome"]?>
        <span class="pull-right" style="font-size: 1.1em; line-height: 1.1em">
            <i class="faicon faicon-refresh"></i>
            <?=date("d/m",strtotime($catalogo['MaterialVenda']['data_ultima_alteracao']))?>
        </span>
    </div>
</div>
<div class="row">
    <div class="col-lg-5">
        <?php if(!empty($catalogo["MaterialVenda"]["informacoes"])) : ?>
        <h3 style="margin-top:0" class="text-danger">Informa&ccedil;&otilde;es</h3>
        <br />
        <?=$catalogo["MaterialVenda"]["informacoes"]?>
        <br />
        <br />
        <?php endif; ?>
    </div>
    <div class="col-lg-7">
        <div class="carousel-wrap">
            <?php if(count($catalogo["MaterialVendaFoto"]) > 0) : ?>
            <div class="owl-carousel" id="carousel-unica">
                <?php foreach($catalogo["MaterialVendaFoto"] as $foto) : ?>
                <div class="item">
                    <img src="/fotos/fit/<?=base64_encode("upload/material_venda_fotos/{$foto["id"]}/default.{$foto["ext"]}")?>/550/300" />
                </div>
                <?php endforeach; ?>
            </div>
            <div class="owl-carousel" id="carousel-lista">
                <?php foreach($catalogo["MaterialVendaFoto"] as $foto) : ?>
                <div class="item">
                    <img src="/fotos/crop/<?=base64_encode("upload/material_venda_fotos/{$foto["id"]}/default.{$foto["ext"]}")?>/140/100/middle" />
                </div>
                <?php endforeach; ?>
            </div>
            <?php endif; ?>
        </div>
        <?php if(count($catalogo["MaterialVendaAnexo"]) > 0) : ?>
        <h3 class="text-danger">Anexos</h3>
        <div class="row">
        <?php foreach($catalogo["MaterialVendaAnexo"] as $anexo) : ?>
            <div class="col-lg-2">
                <a class="thumbnail anexo text-muted"
                    href="/<?=$this->params['prefix']?>/material_venda/baixar_anexo/<?=$anexo['id']?>">
                    <span class="nome"><?=$anexo["titulo"]?></span>
                    <img src="/catalogos/icons/<?=$form->tipoArquivoPorMime($anexo["tipo"], explode(".", $anexo["titulo"]))?>.png" />
                    <span class="faicon faicon-long-arrow-down"></span> Baixar
                </a>
            </div>
        <?php endforeach; ?>
        </div>
        <?php endif; ?>
    </div>
</div>
<?php else : ?>
<h2 class="text-danger">Catalogo não encontrado.</h2>
<?php endif; ?>
