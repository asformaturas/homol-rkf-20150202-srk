<h2>
    <a class="metro-button reload" data-bind="click: reload"></a>
    Materiais
    <a href="/<?=$this->params['prefix']?>/material_venda/editar_evento" data-bind="click: loadThis"
        class="button mini default pull-right">
        Novo Material
    </a>
</h2>
<?php $session->flash(); ?>
<div class="row-fluid">
    <?php if (sizeof($eventos) > 0) : ?>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th>Materiais de Venda</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($eventos as $evento) : ?>
            <?php if(substr($evento['TiposEvento']['nome'], 0, 8) == 'Material') : ?>
            <tr>
                <td><?=$evento['TiposEvento']['nome']?></td>
                <td>
                    <button class="button mini default" type="button"
                        data-bind="click: loadThis"
                        href="/<?=$this->params['prefix']?>/material_venda/listar/<?=$evento['TiposEvento']['id']; ?>">
                        Entrar
                    </button>
                    <button class="button mini bg-color-green" type="button"
                        data-bind="click: loadThis"
                        href="/<?=$this->params['prefix']?>/material_venda/editar_evento/<?=$evento['TiposEvento']['id']; ?>">
                        <i class="icon-pen-alt-fill"></i>
                        Editar
                    </button>
                    <button class="button mini bg-color-<?=$evento["TiposEvento"]["ativo"] == 1 ? "red" : "blue"?>"
                        type="button" data-bind="click: loadThis"
                        href="/<?=$this->params['prefix']?>/material_venda/status_evento/<?=$evento['TiposEvento']['id']; ?>">
                        <?=$evento["TiposEvento"]["ativo"] == 1 ? "Desativar" : "Ativar"?>
                    </button>
                </td>
            </tr>
            <?php endif; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php else : ?>
    <h2 class="fg-color-red">Nenhum material cadastrado.</h2>
    <?php endif; ?>
</div>