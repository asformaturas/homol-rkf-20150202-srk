<script type="text/javascript">
$(document).ready(function() {
    $("#form-filtro").submit(function(e) {
        e.preventDefault();
        var context = ko.contextFor($(".metro-button.reload")[0]);
        context.$data.showLoading(function() {
            $("#form-filtro").find('[value="false"]').attr('disabled','disabled');
            var dados = $("#form-filtro").serialize();
            var url = $("#form-filtro").attr('action');
            $.ajax({
                url : url,
                data : dados,
                type : "POST",
                dataType : "json",
                complete : function() {
                    context.$data.reload();
                }
            });
        });
    })
});
</script>
<h2>
    <a class="metro-button reload" data-bind="click: reload"></a>
    Material de Vendas Online
    <a href="/<?=$this->params['prefix']?>/material_venda/editar" data-bind="click: loadThis"
        class="button mini default pull-right">
        Adicionar
    </a>
</h2>
<?=$form->create('MaterialVenda',array(
    'url' => $this->here,
    'id' => 'form-filtro')) ?>
<div class="row-fluid">
    <div class="span3">
        <label>Filtrar</label>
        <div class="input-control text">
            <input type="text" value="<?=isset($filtro) ? $filtro : ""?>" name="data[filtro]" />
        </div>
    </div>
    <div class="span3">
        <label>&nbsp;</label>
        <button type='submit' class='mini max bg-color-red'>
            Filtrar
            <i class='icon-search-2'></i>
        </button>
    </div>
</div>
<?= $form->end(array('label' => false,
    'div' => false, 'class' => 'hide')); ?>
<div class="row-fluid">
    <?php if (sizeof($catalogos) > 0) : ?>
    <?php $sortOptions = array('data-bind' => 'click: loadThis'); ?>
    <?php $paginator->options(array('url' => array(
        $this->params['prefix'] => true
            ))); ?> 
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th>
                    <?=$paginator->sort('Nome', 'MaterialVenda.nome',$sortOptions); ?>
                </th>
                <th>
                    <?=$paginator->sort('Tipo', 'TiposEvento.nome',$sortOptions); ?>
                </th>
                <th>
                    <?=$paginator->sort('Item', 'MaterialVendaItem.nome',$sortOptions); ?>
                </th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($catalogos as $catalogo) : ?>
            <tr>
                <td><?=$catalogo['MaterialVenda']['nome']?></td>
                <td><?=$catalogo['TiposEvento']['nome']?></td>
                <td><?=$catalogo['MaterialVendaItem']['nome']?></td>
                <td>
                    <button class="button mini default" type="button"
                        data-bind="click: loadThis"
                        href="/<?=$this->params['prefix']?>/material_venda/editar/<?=$catalogo['MaterialVenda']['id']; ?>">
                        Editar
                    </button>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4" class="paginacao">
                    <?=$paginator->numbers(array('separator' => ' ',
                        'data-bind' => 'click: loadThis')); ?>
                </td>
            </tr>
        </tfoot>
    </table>
    <?php else : ?>
    <h2 class="fg-color-red">Nenhum material de venda cadastrado.</h2>
    <?php endif; ?>
</div>