<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var context = ko.contextFor($("#content-body")[0]),
            itens = [];
        $("#lista-itens").sortable({
            containment : 'parent',
            items : ' > tr',
            update : function() {
                var alterado = false;
                itens = []
                $("#lista-itens > tr").each(function() {
                    if($(this).index() != parseInt($(this).data('ordem')))
                        alterado = true;
                    itens.push({
                        id : $(this).data('id'),
                        ordem : $(this).index()+1
                    });
                });
                if(alterado && !$("#confirmar-alteracoes").is(":visible"))
                    $("#confirmar-alteracoes").fadeIn(500);
                else if(!alterado && $("#confirmar-alteracoes").is(":visible"))
                    $("#confirmar-alteracoes").fadeOut(500);
            }
        });
        $("#confirmar-alteracoes").click(function() {
            context.$data.showLoading(function() {
                $.ajax({
                    type : 'POST',
                    url : "/<?=$this->params['prefix']?>/material_venda/ordem_itens/",
                    dataType : 'json',
                    data : {
                        data : {
                            itens : itens
                        }
                    },
                    error : function() {
                        alert("Erro ao enviar dados");
                    },
                    complete : function() {
                        context.$data.reload();
                    }
                });
            });
        });
    });
</script>
<h2>
    <a class="metro-button back" data-bind="click: loadThis"
        href="/<?=$this->params['prefix']?>/material_venda/listar/"></a>
    <a class="metro-button reload" data-bind="click: reload"></a>
    <?=$evento["TiposEvento"]['nome']?> - Itens
    <a href="/<?=$this->params['prefix']?>/material_venda/editar_item/<?=$evento["TiposEvento"]['id']?>" data-bind="click: loadThis"
        class="button mini default pull-right">
        Novo Item
    </a>
    <a class="button mini bg-color-blueDark hide pull-right"
        id="confirmar-alteracoes" style="margin-right: 5px">
        Confirmar Alteração
    </a>
</h2>
<?php $session->flash(); ?>
<div class="row-fluid">
    <?php if (sizeof($itens) > 0) : ?>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th>Item</th>
                <th></th>
            </tr>
        </thead>
        <tbody id="lista-itens">
            <?php foreach($itens as $item) : ?>
            <tr data-id="<?=$item['MaterialVendaItem']['id']; ?>"
                data-ordem="<?=$item['MaterialVendaItem']['ordem']; ?>">
                <td><?=$item['MaterialVendaItem']['nome']?></td>
                <td>
                    <button class="button mini default" type="button"
                        data-bind="click: loadThis"
                        href="/<?=$this->params['prefix']?>/material_venda/listar/<?=$evento["TiposEvento"]['id']?>/<?=$item['MaterialVendaItem']['id']; ?>">
                        Entrar
                    </button>
                    <button class="button mini bg-color-green" type="button"
                        data-bind="click: loadThis"
                        href="/<?=$this->params['prefix']?>/material_venda/editar_item/<?=$evento["TiposEvento"]['id']?>/<?=$item['MaterialVendaItem']['id']; ?>">
                        <i class="icon-pen-alt-fill"></i>
                        Editar
                    </button>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php else : ?>
    <h2 class="fg-color-red">Nenhum item para material de venda cadastrado.</h2>
    <?php endif; ?>
</div>