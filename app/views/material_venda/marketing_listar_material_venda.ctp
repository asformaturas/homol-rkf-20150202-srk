<h2>
    <a class="metro-button back" data-bind="click: loadThis"
        href="/<?=$this->params['prefix']?>/material_venda/listar/<?=$evento["TiposEvento"]['id']?>"></a>
    <a class="metro-button reload" data-bind="click: reload"></a>
    <?=$evento["TiposEvento"]['nome']?> - <?=$item['MaterialVendaItem']['nome']; ?>
    <a href="/<?=$this->params['prefix']?>/material_venda/editar/<?=$evento["TiposEvento"]['id']?>/<?=$item['MaterialVendaItem']['id']; ?>" data-bind="click: loadThis"
        class="button mini default pull-right">
        Novo Material de Venda
    </a>
</h2>
<div class="row-fluid">
    <?php if (sizeof($catalogos) > 0) : ?>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th>Evento</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($catalogos as $catalogo) : ?>
            <tr>
                <td><?=$catalogo['MaterialVenda']['nome']?></td>
                <td>
                    <button class="button mini default bg-color-green" type="button"
                        data-bind="click: loadThis"
                        href="/<?=$this->params['prefix']?>/material_venda/editar/<?=$evento["TiposEvento"]['id']?>/<?=$item['MaterialVendaItem']['id']; ?>/<?=$catalogo['MaterialVenda']['id']; ?>">
                        <i class="icon-pen-alt-fill"></i>
                        Editar
                    </button>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php else : ?>
    <h2 class="fg-color-red">Nenhum material de venda cadastrado.</h2>
    <?php endif; ?>
</div>