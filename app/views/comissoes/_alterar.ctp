<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<style type="text/css">
    .modal-body{
        overflow: hidden;
    }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        $(".selectpicker").selectpicker({width:'100%'});
        var button = $("<button>",{
            type: 'button',
            class: 'button bg-color-greenDark',
            id: 'alterar-comissao',
            dir: '<?=$usuario['FormandoProfile']['usuario_id']?>',
            text:'Alterar'
            });
        $('.modal-footer').prepend(button);
        $('.modal-footer').on('click', '#alterar-comissao',function(){
            if($('#FormandoProfileValorAdesaoComissao').val() == "" || $('#FormandoProfileMaxParcelasComissao').val() == ""){
                bootbox.alert("Os campos valores de adesão e/ou parcelas estão vazios.");
            }else{
                dir = $(this).attr('dir');
                var context = ko.contextFor($(".metro-button.reload")[0]);
                var dados = $("#form").serialize();
                var url = '<?="/{$this->params['prefix']}/comissoes/editar/"?>'+dir;
                bootbox.hideAll();
                context.$data.showLoading(function() {
                    $.ajax({
                        url: url,
                        data: dados,
                        type: "POST",
                        dataType: "json",
                        complete: function() {
                            context.$data.reload();
                        }
                    });
                });
            }
        });
    });

</script>
<div class="row-fluid">
    <?php if($usuario) { ?>    
    <h2 class="fg-color-red"><?=$usuario['Usuario']['nome']?></h2>
    <br />
    <div class="row-fluid">
        <div class="span6">
            <h5>Email</h5>
            <h4><em>
                <?=$usuario['Usuario']['email']?>
            </em></h4>
        </div>
        <div class="span6">
            <h5>RG</h5>
            <h4><em>
                <?=$usuario['FormandoProfile']['rg']?>
            </em></h4>
        </div>
    </div>
    <br />
    <div class="row-fluid">
        <div class="span6">
            <h5>Tel Res</h5>
            <h4><em>
                <?=$usuario['FormandoProfile']['tel_residencial']?>
            </em></h4>
        </div>
        <div class="span6">
            <h5>Tel Com</h5>
            <h4><em>
                <?=$usuario['FormandoProfile']['tel_comercial']?>
            </em></h4>
        </div>
    </div>
    <br />
    <div class="row-fluid">
        <div class="span6">
            <h5>Cargo</h5>
            <h4><em>
                <?=$usuario['FormandoProfile']['cargo_comissao']?>
            </em></h4>
        </div>
    </div>
    <br/>
    <?php
        $session->flash();
        echo $form->create('Comissao', array('url' => "/{$this->params['prefix']}/comissoes/alterar", "id" => "form"));
        echo $form->hidden('FormandoProfile.usuario_id',  array('hiddenField' => true, 'value' => $usuario['FormandoProfile']['usuario_id']));
    ?>
    <div class="row-fluid">
        <div class='span6'>
            <?=$form->input('FormandoProfile.parcelamento_id',
                array(
                    'label' => 'Pacote', 
                    'type' => 'select',
                    'options' => $parcelamentos,
                    'empty' => 'Selecione',
                    'class' => 'selectpicker', 
                    'data-placeholder' => 'Selecione', 
                    'div' => 'input-control', 
                    'error' => false, 
                    'title' => 'Selecione'
                )
            ); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span3">
            <label>Valor da Ades&atilde;o</label>
            <?=$form->input('FormandoProfile.valor_adesao_comissao',
                array(
                    'label' => false, 
                    'id' => 'FormandoProfileValorAdesaoComissao',
                    'class' => 'real-msk',
                    'div' => 'input-control text',
                    'error' => false
                )
            ); ?>
        </div>
        <div class="span3">
            <label>M&aacute;ximo de Parcelas</label>
            <?=$form->input('FormandoProfile.max_parcelas_comissao',
                array(
                    'label' => false, 
                    'id' => 'FormandoProfileMaxParcelasComissao',
                    'div' => 'input-control text',
                    'error' => false
                )
            ); ?>
        </div>
    </div>
    <?php }else{ ?>
    <h2 class="fg-color-red">Usuário Não Encontrado</h2>
    <?php } ?>
</div>