<?php
/* SVN FILE: $Id$ */

/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) :  Rapid Development Framework (http://www.cakephp.org)
 * Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright     Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 * @link          http://www.cakefoundation.org/projects/info/cakephp CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.view.templates.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
            <?php echo $html->charset(); ?>
          
            <title>
                <?php echo $title_for_layout; ?>
            </title>
            
            <link rel="icon" href="<?php echo $this->webroot; ?>img/favicon2.ico" type="image/x-icon"> 
			<link rel="shortcut icon" href="<?php echo $this->webroot; ?>img/favicon2.ico" type="image/x-icon"> 
           
            <?php
				echo $html->css('bootstrap');
				echo $html->css('bootstrap-responsive.min');
				echo $html->css('default');
				echo $html->css('calendario');
				echo $scripts_for_layout;
				echo $html->script('jquery-1.8.0.min');
				echo $html->script('bootstrap/bootstrap-transition.js');
				echo $html->script('bootstrap/bootstrap-alert.js');
				echo $html->script('bootstrap/bootstrap-modal.js');
				echo $html->script('bootstrap/bootstrap-dropdown.js');
				echo $html->script('bootstrap/bootstrap-scrollspy.js');
				echo $html->script('bootstrap/bootstrap-tab.js');
				echo $html->script('bootstrap/bootstrap-tooltip.js');
				echo $html->script('bootstrap/bootstrap-popover.js');
				echo $html->script('bootstrap/bootstrap-button.js');
				echo $html->script('bootstrap/bootstrap-collapse.js');
				echo $html->script('bootstrap/bootstrap-carousel.js');
				echo $html->script('bootstrap/bootstrap-typeahead.js');
				echo $html->script('bootstrap/bootstrap-affix.js');
				echo $html->script('bootstrap/bootstrap-application.js');
				echo $html->script('bootbox');
				echo $html->script('functions');
			?>
            <!--[if IE 6]><?php echo $html->css('grid_ie6'); ?><![endif]--> 
            <!--[if IE 7]><?php echo $html->css('grid_ie7'); ?><![endif]-->
            <script type="text/javascript">
			jQuery(function($) {
				//initRelogio();
				//initCalendar();
			});
			</script>
    </head>
    <body>
        <div id='container' class='container'>
			<div class='br'></div>
			<div class='br'></div>
			<!-- header start -->
			<header class="row-fluid">
				<div class="row-fluid cabecalho">
					<div class="span3" id="logo">
						<a href="
							<?php 
		                		if(isset($this->params['prefix'])) {
		                			echo $html->url(array($this->params['prefix'] => true, 'controller' => 'principal', 'action' => 'index'));
		                		} else {
		                			echo $html->url(array('controller' => 'principal', 'action' => 'index'));
		                		}
		                	?>
						">
							<?php echo $html->image('logo.jpg'); ?>
						</a>
					</div>
					<div class="span9">
	                </div>
				</div>
				<div class="navbar red">
	                <div class="navbar-inner">
	                    <div class='container'>
	                    	<div class='nav-collapse collapse'>
	                        <?php
	                        if(isset($configuracaoTopbarMenu))
	                            echo $topbarMenu->novo($configuracaoTopbarMenu['links'], $this->params['controller']);
	                        ?>
	                        </div>
						</div>
					</div>
				</div>
			</header>
			<!-- header end -->
			<!-- #menu-lateral start -->
			<div class='row-fluid'>
				<div class='menu-lateral'>
					<?php
					if(isset($configuracaoSidebarMenu)) {
						if (isset($turmaLogada['Turma']))
							echo $sidebarMenu->novo($turmaLogada['Turma']['nome'] , $configuracaoSidebarMenu['links'], $this->params['controller'], $this->params['action'], $configuracaoSidebarMenu['sublinks']);
						else
							echo $sidebarMenu->novo($configuracaoSidebarMenu['titulo'], $configuracaoSidebarMenu['links'], $this->params['controller'], $this->params['action'],  $configuracaoSidebarMenu['sublinks']);
					}
					if(isset($itensPendentesSidebar)) {
						//if(isset($turmaLogada['Turma']['nome'])) echo $sidebarItens->exibir("Itens - " . $turmaLogada['Turma']['nome'] , $itensPendentesSidebar);
						//else echo $sidebarItens->exibir("Itens" , $itensPendentesSidebar);
					}
					?>
				</div>
				<!-- #menu-lateral end -->
				<!-- #conteudo start -->
				<div class='conteudo'>
					<?php echo $content_for_layout; ?>
				</div>
			</div>
        </div>
    </body>
</html>
