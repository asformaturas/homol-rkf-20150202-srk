<script type="text/javascript">
$(document).ready(function() {
	var message = '<?php if(is_array($content_for_layout)) : foreach($content_for_layout as $message) : echo "$message<br />"; endforeach; ?>
	<?php  else : echo $content_for_layout; endif; ?>';
	bootbox.classes('warning bg-color-red');
	bootbox.alert(message);
	bootbox.classes('');
})
</script>