<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
    <?=$html->charset(); ?>
    <title><?=$title_for_layout; ?></title>
    <link rel="icon" href="<?php echo $this->webroot; ?>img/favicon2.ico" type="image/x-icon"> 
    <link rel="shortcut icon" href="<?php echo $this->webroot; ?>img/favicon2.ico" type="image/x-icon"> 
    <link rel="stylesheet" type="text/css" href="<?=$this->webroot ?>metro/css/min/bootmetro/default.css?v=0.2">
    <link rel="stylesheet" type="text/css" href="<?=$this->webroot ?>metro/css/min/bootmetro/icons.css?v=0.2">
    <link rel="stylesheet" type="text/css" href="<?=$this->webroot ?>metro/css/min/bootmetro/responsive.css?v=0.2">
    <link rel="stylesheet" type="text/css" href="<?=$this->webroot ?>metro/css/min/metroui/core.css?v=0.2">
    <link rel="stylesheet" type="text/css" href="<?=$this->webroot ?>metro/css/min/metroui/fonts.css?v=0.2">
    <link rel="stylesheet" type="text/css" href="<?=$this->webroot ?>metro/css/min/metroui/typography.css?v=0.2">
    <link rel="stylesheet" type="text/css" href="<?=$this->webroot ?>metro/css/min/metroui/color.css?v=0.2">
    <link rel="stylesheet" type="text/css" href="<?=$this->webroot ?>metro/css/min/metroui/tile.css?v=0.2">
    <link rel="stylesheet" type="text/css" href="<?=$this->webroot ?>metro/css/min/metroui/form.css?v=1.2">
    <link rel="stylesheet" type="text/css" href="<?=$this->webroot ?>metro/css/min/metroui/button.css?v=0.2">
    <link rel="stylesheet" type="text/css" href="<?=$this->webroot ?>metro/css/min/metroui/table.css">
    <link rel="stylesheet" type="text/css" href="<?=$this->webroot ?>metro/css/min/default.css?v=0.7">
    <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="<?=$this->webroot ?>metro/css/min/bootmetro/icons-ie7.css?v=0.2">
    <![endif]-->
    <script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/modernizr-2.6.2.js?v=0.1"></script>
    <script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/jquery-1.9.1.js?v=0.2"></script>
    <script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/jquery.mask.min.js?v=0.2"></script>
    <script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/knockout/knockout-2.2.1.js?v=0.1"></script>
    <script>window.jQuery || document.write("<script src='<?=$this->webroot ?>metro/js/min/jquery-1.9.1.js?v=0.1'>\x3C/script>")</script>
    <script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/jquery.easing.1.3.js?v=0.1"></script>
    <script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/metroui/input-control.js?v=0.1"></script>
    <script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/dropdown.js?v=0.1"></script>
    <script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/transition.js?v=0.1"></script>
    <script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/alert.js?v=0.1"></script>
    <script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/tooltip.js?v=0.1"></script>
    <script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/modal.js?v=0.1"></script>
    <script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/popover.js?v=0.1"></script>
    <script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/tab.js?v=0.1"></script>
    <script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/bootbox.js?v=0.2"></script>
    <script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/utils.js?v=0.1"></script>
    <script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/knockout/content.js?v=0.3"></script>
    <script type="text/javascript" src="<?=$this->webroot ?>metro/js/enquetes.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.real-msk').mask('#.##0,00', {
                reverse: true, 
                clearIfNotMatch: true
            });
            var enquete = '';
            <?php if($turmaLogada['Turma']['status'] == 'fechada') : ?>
            enquete = <?=!empty($enquete) && !in_array($turmaLogada['Turma']['id'], array('6028','6984','6631')) ? $enquete : json_encode(array())?>;
            <?php endif; ?>
            if(enquete['EnquetePergunta']) {
                exibirEnquete(enquete);
                if(enquete['Enquete']['obrigatoria'] == 1){
                    $('.depois').remove();
                    $('.rejeitar').remove(); 
                }
            }
            <?php if($turmaLogada && $eFuncionario) : ?>
            $('body').tooltip({ selector: '[rel=tooltip]'});
            $('#info-turma-logada').click(function() {
                bootbox.dialog('Carregando',[{
                    label: 'Fechar'
                }],{
                    remote: '/<?=$this->params['prefix']?>/turmas/info/'
                });
            });
            <?php endif; ?>
        })
    </script>
</head>
<body>
    <div id="fb-root"></div>
    <?php if(isset($redirecionar)) { ?>
    <input type="hidden" id="redirecionar-para-pagina" value="<?=$redirecionar?>" />
    <?php } ?>
    <div id="menu-topo" class="navbar navbar-fixed-top">
        <div class="navbar-inner bg-color-orange">
            <div class="container todo">
                <a class="brand">
                    <img src="<?=$this->webroot ?>metro/img/logo_rk_branco_min3.png" />
                </a>
                <div class="nav-collapse collapse">
                    <?php
                    if(isset($configuracaoTopbarMenu))
                        echo $topbarMenu->novo($configuracaoTopbarMenu['links']);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container todo">
        <header id="nav-bar" class="container" style="width:100%">
            <div class="row-fluid">
                <div class="span12">
                    <div id="header-container">
                        <a id="homeButton" class="win-backbutton home" href="#"
                           data-bind="click: function() { hideHomeButton() }"></a>
                        <h5>RK Formaturas</h5>
                        <div class="dropdown pull-left">
                            <a class="header-dropdown dropdown-toggle accent-color"
                               data-role="dropdown" data-toggle="dropdown" href="#">Iniciar<b class="caret"></b>
                            </a>
                            <?php if(isset($configuracaoSidebarMenu)) { ?>
                            <?=$sidebarMenu->novo($configuracaoSidebarMenu['links'],
                                    $this->params['controller'],
                                    $this->params['action'],
                                    $configuracaoSidebarMenu['sublinks']);?>
                            <?php } ?>
                        </div>
                        <div class="dropdown pull-left vazio" id="avisos" data-bind="css: { vazio: $root.avisos().length == 0 }">
                            <span class="dropdown-toggle" data-toggle="dropdown" href="#">
                                Avisos
                                <i class="icon-bell"></i>
                                <b class="contador bg-color-red" data-bind="text: $root.avisos().length"></b>
                            </span>
                            <!-- ko if: $root.avisos().length > 0 -->
                            <ul class="dropdown-menu" data-bind="foreach: $root.avisos">
                                <li>
                                    <a href="#" data-bind="html:$data.label, click:$root.selecionarAviso"></a>
                                </li>
                            </ul>
                            <!-- /ko -->
                        </div>
                    </div>
                    <div id="top-info" class="pull-right">
                        <a href="<?=$this->webroot . $this->params['prefix'] ?>/usuarios/logout"
                           class="pull-right" rel="tooltip" title="Sair">
                            <b class="icon-exit" style="font-size:28px"></b>
                        </a>
                        <!--
                        <a id="settings" class="pull-right">
                            <b class="icon-settings"></b>
                        </a>
                        -->
                        <!--<hr class="separator pull-right"/>-->
                        <?php
                        $fotoPerfil = "img/uknown_user.gif";
                        if (isset($usuario['Usuario']['diretorio_foto_perfil']))
                            if (!empty($usuario['Usuario']['diretorio_foto_perfil']))
                                if (file_exists(APP . "webroot/{$usuario['Usuario']['diretorio_foto_perfil']}"))
                                    $fotoPerfil = $usuario['Usuario']['diretorio_foto_perfil'];
                        ?>
                        <?php if($usuario['Usuario']['grupo'] != 'formando' && $usuario['Usuario']['grupo'] != 'comissao'){ ?>
                        <a id="logged-user" data-bind="click:
                           function() { page('/usuarios/editar_dados_funcionario/') }"
                            class="pull-right perfil pointer" rel="tooltip" title="Editar Dados">
                            <img src="<?="{$this->webroot}{$fotoPerfil}" ?>" />
                        </a>
                        <?php }else{ ?>
                        <a id="logged-user" data-bind="click:
                           function() { page('<?=$this->webroot .
                                $this->params['prefix'] ?>/usuarios/editar_dados') }"
                            class="pull-right perfil pointer" rel="tooltip" title="Editar Dados">
                            <img src="<?="{$this->webroot}{$fotoPerfil}" ?>" />
                        </a>
                        <?php } ?>
                        <?php if($turmaLogada) : ?>
                        <div class="pull-left user-info">
                            <p>
                                <span class="label bg-color-orange pointer" id='info-turma-logada'
                                    rel="tooltip" title='Informações da Turma'>
                                    <?=$turmaLogada['Turma']['nome']?> <?=$turmaLogada['Turma']['id']?>
                                </span>
                                <?php if($eFuncionario) : ?>
                                <?=$html->link('Sair da Turma',
                                    array('controller' => 'turmas',
                                        'action' => 'sair'),
                                        array('class' => 'fg-color-orange'));
                                ?>
                                <?php endif; ?>
                            </p>
                        </div>
                        <?php endif; ?>
                        <?php if($usuario['Usuario']['grupo'] != 'formando' && $usuario['Usuario']['grupo'] != 'comissao'){ ?>
                        <div class="pull-left">
                            <a data-bind="click:
                               function() { page('/usuarios/editar_dados_funcionario/') }"
                               class="pull-right pointer" rel="tooltip" title="Editar Dados">
                                <div class='fg-color-orange' style='max-width: 400px;font-size: 26px'><?=$usuario['Usuario']['nome'] ?></div>
                            </a>
                            <h4 class="dados-usuario grupo"><?=$usuario['Usuario']['grupo']?></h4>
                        <?php }else{ ?>
                            <div class="pull-left">
                            <a data-bind="click:
                               function() { page('<?=$this->webroot .
                                   $this->params['prefix'] ?>/usuarios/editar_dados') }"
                               class="pull-right pointer" rel="tooltip" title="Editar Dados">
                                <div class='fg-color-orange'  style='max-width: 400px;font-size: 26px'><?=$usuario['Usuario']['nome'] ?></div>
                            </a>
                            <h4 class="dados-usuario grupo"><?=$usuario['Usuario']['grupo']?></h4>
                        <?php } ?>
                        <?php
                            if(isset($usuario) && $exibirSwitchComissaoFormando) {
                                if($usuario['Usuario']['grupo'] == 'comissao')
                                    $label = 'Entrar Como Formando';
                                elseif($usuario['Usuario']['grupo'] == 'formando')
                                    $label = 'Entrar Como Comissão';
                                if(isset($label))
                                    echo $html->link($label,
                                        array($this->params['prefix'] => true,
                                        'controller' => 'usuarios',
                                        'action' => 'mudar_grupo'),
                                         array('class' => 'fg-color-red'));
                            }
                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="row-fluid" id="conteudo">
            <?=$content_for_layout; ?>
        </div>
    </div>
    <footer id="footer-body" class="bg-color-orange">
        <div>
            <p>formaturas <?=date("Y") ?> - Todos os direitos reservados &reg;</p>
        </div>
    </footer>
    <?php if($ambiente == 'producao') : ?>
    <script type="text/javascript" src="<?= $this->webroot ?>metro/js/facebook.js"></script>
    <?php endif; ?>
</body>
</html>
