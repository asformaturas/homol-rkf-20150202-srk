<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
    <meta name="google-site-verification" content="Ip4-BMIP8v6t5ky279zPi7nhLEHl3vxifr2ccDarP30" />
    <?= $html->charset(); ?>
    <title><?= $title_for_layout; ?></title>
    <link rel="icon" href="<?php echo $this->webroot; ?>img/favicon2.ico" type="image/x-icon"> 
    <link rel="shortcut icon" href="<?php echo $this->webroot; ?>img/favicon2.ico" type="image/x-icon"> 
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootmetro/default.css">
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootmetro/icons.css">
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootmetro/responsive.css">
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/fonts.css">
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/typography.css">
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/color.css">
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/form.css">
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/button.css">
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/default.css">
    <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootmetro/icons-ie7.css">
    <![endif]-->
    <script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/modernizr-2.6.2.js"></script>
    <script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/knockout/knockout-2.2.1.js"></script>
    <script>window.jQuery || document.write("<script src='<?= $this->webroot ?>metro/js/min/jquery-1.9.1.js'>\x3C/script>")</script>
    <script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/transition.js?v=0.1"></script>
    <script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/alert.js?v=0.1"></script>
    <script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/tooltip.js?v=0.1"></script>
    <script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/modal.js?v=0.1"></script>
    <script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/popover.js?v=0.1"></script>
    <script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/tab.js?v=0.1"></script>
    <script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/bootbox.js?v=0.2"></script>
    <script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/metroui/input-control.js"></script>
    <script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/knockout/content_login.js"></script>
</head>
<body>
    <div id="fb-root"></div>
    <div id="menu-topo" class="navbar navbar-fixed-top">
        <div class="navbar-inner bg-color-orange">
            <div class="container todo">
                <a class="brand">
                    <div class='logo-min-branco'></div>
                </a>
            </div>
        </div>
    </div>
    <div class="container todo">
        <div class="row-fluid" id="conteudo">
            <div style="min-height:120px; display:block"></div>
            <div class="row-fluid" style="text-align:center">
                <div>
					<?php $session->flash(); ?>
                    <?php if($turmaLogada) : ?>
                    <h2 class="fg-color-orange">Seja bem-vindo ao espaço da Turma <?=$turmaLogada['Turma']['nome'];?>!!</h2>
                    <?php else : ?>
                    <h2 class="fg-color-orange"><strong>Seja bem-vindo ao novo sistema online da RK Formaturas!!</strong></h2>
                    <?php endif; ?>
                    <br />
                    <br />
                    <div style="width:31.623931623931625%;margin:auto">
						<?php $session->flash(); ?>
                        <?=$content_for_layout; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer id="footer-body" class="bg-color-orange">
        <div>
            <p>formaturas <?=date("Y") ?> - Todos os direitos reservados &reg;</p>
        </div>
    </footer>
    <script type="text/javascript" src="<?= $this->webroot ?>metro/js/facebook.js"></script>
</body>
</html>