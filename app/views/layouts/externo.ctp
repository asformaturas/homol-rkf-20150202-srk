<?php
/* SVN FILE: $Id$ */

/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) :  Rapid Development Framework (http://www.cakephp.org)
 * Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright     Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 * @link          http://www.cakefoundation.org/projects/info/cakephp CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.view.templates.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
	<?php echo $html->charset(); ?>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	
    <link rel="icon" href="<?php echo $this->webroot; ?>img/favicon2.ico" type="image/x-icon"> 
	<link rel="shortcut icon" href="<?php echo $this->webroot; ?>img/favicon2.ico" type="image/x-icon"> 
           
	<?php
		//echo $html->meta('icon');

		echo $html->css('estrutura');
                echo $html->css('forms');
                echo $html->css('grid');
                echo $html->css('reset');
                echo $html->css('tabelas');
                echo $html->css('texto');
                echo $html->css('jquery-ui');
				echo $html->css('colorbox');

		echo $scripts_for_layout;
		echo $html->script('jquery-1.6.2.min');
        echo $html->script('jquery-ui-1.8.14.min');
        echo $html->script('jquery-ui-datepicker-pt-BR');
        echo $html->script('jquery.meio.mask.js');
        echo $html->script('jquery.imgareaselect.min');
		echo $html->script('jquery.colorbox');
		echo $html->script('analytics');
		
	?>
	<!--[if IE 6]><?php echo $html->css('grid_ie6'); ?><![endif]--> 
	<!--[if IE 7]><?php echo $html->css('grid_ie7'); ?><![endif]--> 
</head>
<body>
	<div class="container_16">
		
		<!-- header start -->
		<div id="header" class="grid_full alpha omega">
			<div class="grid_4 alpha" id="logo">
				<a href="
                	<?php 
                		echo $html->url(array('controller' => 'principal', 'action' => 'index'));
                	
                	?> 
                	">
                	<?php echo $html->image('logo.jpg'); ?>
                 </a>
			</div>
			<div class="grid_12 omega">
				<div  id="usuario-logado">
					<p id="usuario-logado-nome">Cadastro
					</p>
				</div> 
			</div>
			<div class="grid_full first" id="menu-horizontal">
				<div id="menu-horizontal-links">
				</div>
			</div>
		</div>
		<!-- header end -->

		<!-- #conteudo start -->
			<div id="conteudo"  class="box-com-titulo grid_12 prefix_2">
				<?php echo $content_for_layout;?>
			</div>
		<!-- #conteudo end -->

		<!-- #footer start -->
		<div id="footer"  class="grid_full">
			<?=$this->element('default/footer'); ?><a href="https://twitter.com/asformaturas" target='_blank' id="twitter"><img src="<?php echo $this->webroot; ?>img/twitter.jpg"/ alt="twitter"/></a>
		</div>
		<!-- #footer end -->
	</div>
</body>
</html>
