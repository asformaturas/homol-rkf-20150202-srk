<?php
/* SVN FILE: $Id$ */

/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) :  Rapid Development Framework (http://www.cakephp.org)
 * Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright     Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 * @link          http://www.cakefoundation.org/projects/info/cakephp CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.view.templates.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
	<?php echo $html->charset(); ?>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	
	
    <link rel="icon" href="<?php echo $this->webroot; ?>img/favicon2.ico" type="image/x-icon"> 
	<link rel="shortcut icon" href="<?php echo $this->webroot; ?>img/favicon2.ico" type="image/x-icon"> 
           
	<?php
		//echo $html->meta('icon');

		echo $html->css('estrutura');
                echo $html->css('forms');
                echo $html->css('grid');
                echo $html->css('reset');
                echo $html->css('tabelas');
                echo $html->css('texto');

		echo $scripts_for_layout;
		echo $html->script('jquery-1.6.2.min');
		echo $html->script('analytics');
		
	?>
	<!--[if IE 6]><?php echo $html->css('grid_ie6'); ?><![endif]--> 
	<!--[if IE 7]><?php echo $html->css('grid_ie7'); ?><![endif]--> 
</head>
<script type="text/javascript">
	$(document).ready( function() {
		if($('#UsuarioEmail').val() == undefined)
			$("#UsuarioLoginAntigo").focus();
		else
			$('#UsuarioEmail').focus();
	});
</script>
<body>
	<div class="container_16">
		
		<!-- header start -->
		<div id="header" class="grid_full alpha omega">
			<div class="grid_4 alpha" id="logo"><img src="<?php echo $this->webroot; ?>img/logo.jpg"/></div>
			<div class="grid_12 omega">
				<div  id="usuario-logado">
					<p id="usuario-logado-nome">Login
					</p>
				</div> 
			</div>
			<div class="grid_full first" id="menu-horizontal">
				<div id="menu-horizontal-links">
				</div>
			</div>
		</div>
		<!-- header end -->

		<!-- #conteudo start -->
		<div class="grid_16" style="display:block; ">
		<div class="grid_16" style="display:block;  margin: auto">
			<div class="grid_12 alpha omega first " style="text-align:justify; padding:0px; margin:0px">
					<h1> Seja bem-vindo ao novo sistema online da RK Formaturas!!</h1>
					<span id="login-bemvindo">
						<ul>
							<li>Para fazer o login, utilize seu e-mail cadastrado no sistema e sua senha antiga. </li>
							<li>Se você não lembra sua senha pode utilizar o link 'recuperar' senha, e as instruções para gerar uma nova senha serão enviadas para o seu e-mail.</li>
							<li>Caso você não lembre ou não tenha cadastrado um e-mail, utilize a opção <b> <?php echo $html->link('Login com dados antigos', 'login_antigo'); ?> </b> para entrar utilizando o seu login e senha do sistema anterior. ( o login é seu primeiro nome e a senha padrão é seu cpf)</li>
							<li>Atualize seus dados cadastrais para sempre estar com os dados corretos cadastrados no sistema!</li>
						</ul>
					</span>
					
			
			</div>
			<div id="conteudo"  class="box-com-titulo grid_4 alpha omega" style="float: right; border-left: solid 1px #e0e0e0 ; margin:0px; padding-left: 15px">
				<?php echo $content_for_layout;?>
			</div>
		</div>
		</div>
		<!-- #conteudo end -->

		<!-- #footer start -->
		<div id="footer"  class="grid_full">
			<?=$this->element('default/footer'); ?>
		</div>
		<!-- #footer end -->
	</div>
</body>
</html>
