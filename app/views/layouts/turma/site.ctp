<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
    <head>
        <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
        <meta name="google-site-verification" content="Ip4-BMIP8v6t5ky279zPi7nhLEHl3vxifr2ccDarP30" />
        <?= $html->charset(); ?>
        <title><?= $title_for_layout; ?></title>
        <link rel="icon" href="<?php echo $this->webroot; ?>img/favicon2.ico" type="image/x-icon"> 
        <link rel="shortcut icon" href="<?php echo $this->webroot; ?>img/favicon2.ico" type="image/x-icon">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootmetro/default.css">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootmetro/icons.css">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootmetro/responsive.css">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/fonts.css">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/typography.css">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/button.css">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/color.css">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/site_turma.css">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/unslider.css">
        <!--[if IE 7]>
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootmetro/icons-ie7.css">
        <![endif]-->
        <script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/modernizr-2.6.2.js"></script>
        <script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/jquery-1.9.1.js"></script>
        <script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/transition.js?v=0.1"></script>
        <script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/dropdown.js?v=0.1"></script>
        <script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/alert.js?v=0.1"></script>
        <script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/modal.js?v=0.1"></script>
        <script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/bootbox.js?v=0.2"></script>
        <script src="<?= $this->webroot ?>metro/js/unslider.min.js"></script>
        <style type="text/css">
            #container:before { position:absolute; z-index:-1; width:1680px;
                                min-height:100%; height:100%; top:0px; left:-350px; content:' ';
                                display:block; overflow: visible;
                                background:
                                    url('../../img/turmas/bolhas.png') left -40px no-repeat scroll,
                                    url('../../img/turmas/bolhas.png') 1012px 15px no-repeat scroll,
                                    url('../../img/turmas/bolhas.png') 1052px 885px no-repeat scroll,
                                    url('../../img/turmas/bolhas.png') 2px 685px no-repeat scroll,
                                    url('<?= $logoBg ?>') 220px 50px / 12% no-repeat scroll,
                                    url('<?= $logoBg ?>') 1350px 25px / 10% no-repeat scroll,
                                    url('<?= $logoBg ?>') 1260px 275px no-repeat scroll,
                                    url('<?= $logoBg ?>') 183px 390px / 22% no-repeat scroll,
                                    url('<?= $logoBg ?>') 1300px 755px no-repeat scroll,
                                    url('<?= $logoBg ?>') 183px 715px / 21% no-repeat scroll;
            }
        </style>
    </head>
    <body>
        <div class="container" id="container">
            <div class="body">
                <div class="cabecalho">
                    <a href="http://formaturas.as" target="_blank" class="logo"></a>
                    <div class="menu">
                        <ul>
                            <?php foreach($menu as $titulo => $link) : ?>
                            <li>
                                <a href="<?=$link?>">
                                    <?=$titulo?>
                                </a>
                            </li>
                            <?php endforeach; ?>
                            <?php if (!empty($submenu)) : ?>
                                <li class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        Mais
                                    </a>
                                    <ul class="dropdown-menu">
                                        <?php foreach ($submenu as $titulo => $link) : ?>
                                            <li>
                                                <a href="<?=$link?>">
                                                    <?=$titulo?>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </li>
                            <?php endif; ?>
                            <li class="adesao right">
                                <a id="login" href="#" class="acao bg-color-blueDark">
                                    Login
                                </a>
                            </li>
                            <?php if($adesaoLiberada) : ?>
                            <li class="adesao">
                                <a href="<?php echo $this->webroot ?>/formandos/confirmar_turma/<?php echo $turma_id ?>" class="acao bg-color-red">
                                    Fa&ccedil;a Sua Ades&atilde;o
                                </a>
                            </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
                <div class="banner">
                    <ul>
                        <?php foreach ($fotosCabecalho as $fotoCabecalho) : ?>
                            <li style='background-image: url(<?= "{$this->webroot}{$fotoCabecalho['TurmaSiteFoto']['arquivo']}" ?>)'></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <?= $content_for_layout; ?>
                <br />
                <br />
                <br />
                <div class="row-fluid">
                    <!--
                    <div class="span5">
                        <div class="video"></div>
                    </div>
                    <div class="span4">
                        <div class="declaracao"></div>
                    </div>
                    -->
                    <div class="span3 pull-right">
                        <div class="asinho"></div>
                    </div>
                </div>
                <br />
            </div>
        </div>
        <div id="fb-root"></div>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.banner').unslider({
                    arrows: false,
                    fluid: true,
                    keys: false,
                    dots: true,
                    delay: 5000
                });
            });
        </script>
        <script>
            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if(d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=222741717865891";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
    </body>
</html>