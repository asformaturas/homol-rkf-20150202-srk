<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
    <head>
        <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
        <meta name="google-site-verification" content="Ip4-BMIP8v6t5ky279zPi7nhLEHl3vxifr2ccDarP30" />
        <?= $html->charset(); ?>
        <title><?= $title_for_layout; ?></title>
        <link rel="icon" href="<?php echo $this->webroot; ?>img/favicon2.ico" type="image/x-icon"> 
        <link rel="shortcut icon" href="<?php echo $this->webroot; ?>img/favicon2.ico" type="image/x-icon">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootmetro/default.css">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootmetro/icons.css">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootmetro/responsive.css">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/fonts.css">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/typography.css">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/button.css">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/color.css">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/font_jacques.css">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/site_turma.css">
        <!--
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/color.css">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/form.css">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/button.css">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/default.css">
        -->
        <!--[if IE 7]>
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootmetro/icons-ie7.css">
        <![endif]-->
        <script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/modernizr-2.6.2.js"></script>
        <script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/jquery-1.9.1.js"></script>
        <script src="<?= $this->webroot ?>metro/js/max/jquery.pagescroller.lite.js"></script>
        <script src="<?= $this->webroot ?>metro/js/holder.js"></script>
    </head>
    <body>
        <div class="container" id="page-wrap">
            <header id="main-header" class="container">
                <div id="nav">
                    <div id="logo"></div>
                    <ul id="navegacao">
                        <li>
                            <a href="#bem-vindo" class="scroll">Bem-Vindo!</a>
                        </li>
                        <?php foreach($paginas as $pagina) : ?>
                        <li>
                            <a href="#<?=strtolower($pagina['TurmaPagina']['menu'])?>"
                               class="scroll"><?=$pagina['TurmaPagina']['menu']?></a>
                        </li>
                        <?php endforeach; ?>
                        <li>
                            <a href="#novidade" class="scroll">Novidades</a>
                        </li>
                        <li>
                            <a href="#fotos" class="scroll">Fotos</a>
                        </li>
                        <li>
                            <a href="#contato" class="scroll">Contato</a>
                        </li>
                        <button type="button" id="login" class="button bg-color-blue pull-right" style="margin-top:25px">
                            Login
                        </button>
                    </ul>
                </div>
            </header>
            <div class="secao bem-vindo" id="bem-vindo">
                <div class="row-fluid">
                    <div class="span6">
                        <div class="fake-foto media">
                            <?php if(!empty($turmaLogada['Turma']['logo_site'])) : ?>
                            <img src="<?="{$this->webroot}{$turmaLogada['Turma']['logo_site']}"?>" />
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="span5 offset1 boas-vindas">
                        <h2>
                            Sejam bem vindos Formandos <?=$turmaLogada['Turma']['nome']?>!
                        </h2>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        </p>
                        <br />
                        <button type="button" class="input-block-level bg-color-blue">
                            Entrar no Sistema
                        </button>
                    </div>
                </div>
            </div>
            <?php foreach($paginas as $pagina) : ?>
            <div class="secao <?=$bg[array_rand($bg)]?>" id="<?=strtolower($pagina['TurmaPagina']['menu'])?>">
                <div class="row-fluid">
                    <h1 class="titulo">
                        <?=$pagina['TurmaPagina']['titulo']?>
                    </h1>
                    <div class="fake-foto enorme">
                        <?php foreach($pagina['TurmaPaginaFoto'] as $paginaFoto) : ?>
                        <img src="<?="{$this->webroot}{$paginaFoto['arquivo']}"?>" />
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
            <div class="secao novidade" id="novidade">
                <div class="row-fluid">
                    <h1 class="titulo">
                        Confira as Novidades
                    </h1>
                    <div class="row-fluid novidades">
                        <div class="span6 item">
                            <div class="img">
                                <img src="holder.js/120x180/social/text:Foto">
                            </div>
                            <h3>Teste</h3>
                        </div>
                        <div class="span6 item">
                            <div class="img">
                                <img src="holder.js/120x180/social/text:Foto">
                            </div>
                            <h3>Teste Com Novidade Grande, Muito Grande</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temp</p>
                        </div>
                    </div>
                    <div class="row-fluid novidades">
                        <div class="span6 item">
                            <div class="img">
                                <img src="holder.js/120x180/social/text:Foto">
                            </div>
                        </div>
                        <div class="span6 item">
                            <div class="img">
                                <img src="http://localhost/bootas/gara.jpg">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="secao pastel" id="fotos">
                <div class="row-fluid">
                    <h1 class="titulo">
                        Fotos
                    </h1>
                    <div class="row-fluid">
                        <div class="span3">
                            <div class="fake-foto pequena">
                                
                            </div>
                        </div>
                        <div class="span3">
                            <div class="fake-foto pequena">
                                
                            </div>
                        </div>
                        <div class="span3">
                            <div class="fake-foto pequena">
                                
                            </div>
                        </div>
                        <div class="span3">
                            <div class="fake-foto pequena">
                                
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row-fluid">
                        <div class="span3">
                            <div class="fake-foto pequena">
                                
                            </div>
                        </div>
                        <div class="span3">
                            <div class="fake-foto pequena">
                                
                            </div>
                        </div>
                        <div class="span3">
                            <div class="fake-foto pequena">
                                
                            </div>
                        </div>
                        <div class="span3">
                            <div class="fake-foto pequena">
                                
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row-fluid">
                        <div class="span3">
                            <div class="fake-foto pequena">
                                
                            </div>
                        </div>
                        <div class="span3">
                            <div class="fake-foto pequena">
                                
                            </div>
                        </div>
                        <div class="span3">
                            <div class="fake-foto pequena">
                                
                            </div>
                        </div>
                        <div class="span3">
                            <div class="fake-foto pequena">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="secao contato" id="contato">
                <div class="row-fluid">
                    <h1 class="titulo branco">
                        Contato
                    </h1>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function() {
                
                $("#login").click(function(e) {
                    e.preventDefault();
                    alert('Vai fazer login!');
                });
                
                $('#page-wrap').pageScroller({
                    sectionClass: 'secao',
                    navigation: "#navegacao",
                    scrollOffset: -70
                });

            });
        </script>
    </body>
</html>