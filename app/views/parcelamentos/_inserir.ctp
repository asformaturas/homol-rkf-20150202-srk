<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/fileupload.js"></script>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootstrap/fileupload.css">
<style type="text/css">
    .button.botao{
        margin-top: 0;
        min-height: 30px;
        height: 30px;
        line-height: 10px
    }
    .fileupload .uneditable-input{
        vertical-align: top;
    }
</style>
<script type='text/javascript'>
    $(document).ready(function() {
        $('.fileupload').bind('loaded', function(e) {
            var position = e.src.indexOf("base64,");
            if (position > 0) {
                $("#binario").val(e.src.substring(position + 7));
                $("#tamanho").val(e.file.size);
                if(e.file.type == "")
                    $("#extensao").val(e.file.name.split('.').pop());
                else
                    $("#extensao").val(e.file.type);
                $("#arquivo").val(e.file.name);
            } else {
                $("#binario").val("");
            }
            return;
        });
        $("#parcelamento").submit(function(e) {
            e.preventDefault();
            if ($("#nome").val() == "" || $("#binario").val() == "") {
                bootbox.alert("Você deve selecionar um tipo de parcelamento e um nome.");
            }else if ($("#mesas").val() == "" || $("#convites").val() == ""){
                bootbox.alert("Você deve preencher as mesas e convites da turma.");
            } else {
                var context = ko.contextFor($(".metro-button.back")[0]);
                context.$data.showLoading(function() {
                    var dados = {data: {
                            Parcelamaker: {
                                src: $("#binario").val(),
                                tamanho: $("#tamanho").val(),
                                extensao: $("#extensao").val(),
                                arquivo: $("#arquivo").val(),
                                titulo: $("#nome").val(),
                            },
                            mesas: $("#mesas").val(),
                            convites: $("#convites").val()
                        }};
                    $.ajax({
                        url: $("#parcelamento").attr("action"),
                        dataType: "json",
                        type: "POST",
                        data: dados,
                        complete: function() {
                            context.$data.page('/<?=$this->params['prefix']?>/parcelamentos/listar');
                        }
                    });
                });
            }

        });
    });
</script>
<div class="row-fluid">
    <h2>
        <a class="metro-button back" data-bind="click: function() { 
            page('<?="/{$this->params['prefix']}/parcelamentos/listar"?>') }">
        </a>
        Adicionar Parcelamento
    </h2>
</div>
<?php $session->flash(); ?>
<?php echo $form->create('Parcelamaker', array('url' => "/{$this->params['prefix']}/parcelamentos/inserir", 'type' => 'file', 'id' => 'parcelamento')); ?>
<?php echo $form->hidden('binario', array('id' => 'binario')); ?>
<?php echo $form->hidden('tamanho', array('id' => 'tamanho')); ?>
<?php echo $form->hidden('extensao', array('id' => 'extensao')); ?>
<?php echo $form->hidden('arquivo', array('id' => 'arquivo')); ?>
<div class="row-fluid">
    <div class="span2">
        <label>Mesas Contrato</label>
        <?php echo $form->input('mesas_contrato', array('label' => false, 'div' => 'input-control', 'error' => false, "onkeyup"=>"this.value=this.value.replace(/[^\d]/,'')", 'id' => 'mesas')); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span2">
        <label>Convites Contrato</label>
        <?php echo $form->input('convites_contrato', array('label' => false, 'div' => 'input-control', 'error' => false, 'id' => 'convites', "onkeyup"=>"this.value=this.value.replace(/[^\d]/,'')")); ?>
    </div>
</div>
<div id="row-fluid">
    <label>Nome</label>
    <?= $form->input('titulo', array('value' => 'Padrão', 'class' => 'input-control text', 'label' => false, 'div' => false, 'error' => false, 'id' => 'nome')); ?>
    <div class="fileupload fileupload-new" data-provides="fileupload"
         data-reader="true">
        <div class="input-append">
            <div class="uneditable-input span3"><i class="icon-file fileupload-exists"></i> 
                <span class="fileupload-preview"></span>
            </div>
            <span class="button btn-file botao bg-color-orange fileupload-new">
                <span>Selecione o Parcelamento</span>
                <input type="file"/>
            </span>
            <span class="button btn-file botao bg-color-orange fileupload-exists">
                <span>Alterar</span>
                <input type="file"/>
            </span>
            <a href="#" class="button fileupload-exists botao bg-color-red" data-dismiss="fileupload">Remover</a>
        </div>
    </div>
</div>
<?php echo $form->end(array('label' => 'Salvar', 'div' => false,
    'class' => 'button bg-color-orange'));
?>