<?php
$largura = 10.0/(sizeof($dados)+1);
?>
<?=isset($parcelamaker) ? "<h1>{$parcelamaker['Parcelamento']['titulo']}</h1>" : ""?>
<div class="container-tabela">
	<table>
		<thead>
			<tr style="font-size: x-small">
				<th scope="col" width="<?php echo $largura;?>%">Parcelas</th>
				<?php
					
					foreach(array_keys($dados) as $mes) :					
					?>
						<th scope="col" width="<?php echo $largura;?>%"><?php echo $mes; ?></th>
				<?php
					endforeach;
				?>					
			</tr>
		</thead>
		<tbody>
		<?php 
		$isOdd = false; 
		?>
		
		
		<?php foreach ($listaParcelas as $numparcela): ?>
			<?php if($isOdd):?><tr class="odd"style="font-size: x-small" ><?php else:?><tr style="font-size: x-small"><?php endif;?>
				<td colspan="1"><?php echo $numparcela; ?></td>
				<?php foreach(array_keys($dados) as $data):?>
					<td colspan="1">
						<?php 
							$out = '-';
							if(isset($dados[$data][$numparcela])) $out = $dados[$data][$numparcela];
							echo $out; 
						?>
					</td>
				<?php endforeach;?>
			</tr>
			<?php if($isOdd){ $isOdd = false; } else {$isOdd = true; } ?>
		<?php endforeach; ?>
		</tbody>
	</table>
</div>
