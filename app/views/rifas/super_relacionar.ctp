<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('.selectpicker').selectpicker({width:'100%'});
    $("#form").submit(function(e) {
        e.preventDefault();
        var context = ko.contextFor($(".metro-button.reload")[0]);
        var dados = $("#form").serialize();
        var url = $("#form").attr('action');
        bootbox.hideAll();
        context.$data.showLoading(function() {
            $.ajax({
                url: url,
                data: dados,
                type: "POST",
                dataType: "json",
                complete: function() {
                    context.$data.reload();
                }
            });
        });
    });
});
</script>
<div class="row-fluid">
	<?php $session->flash(); ?>
	<?php echo $form->create('RifaTurma', array('url' => "/{$this->params['prefix']}/rifas/relacionar/", 'id' => 'form')); ?>
    <div class="row-fluid">
        <div class="span4">
            <?=$form->input('RifaTurma.turma_id', array(
                'options' => $turmas,
                'type' => 'select',
                'class' => 'selectpicker',
                'label' => 'Turmas',
                'div' => 'input-control text')); ?>
        </div>
        <div class="span4">
            <?=$form->input('RifaTurma.rifa_id', array(
                'options' => $rifas,
                'type' => 'select',
                'class' => 'selectpicker',
                'label' => 'Rifas',
                'div' => 'input-control text')); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4">
            <?=$form->input('RifaTurma.cupons', array(
                'options' => $cupons,
                'type' => 'select',
                'class' => 'selectpicker',
                'label' => 'Cupons',
                'div' => 'input-control text')); ?>
        </div>
        <div class="span4">
            <?=$form->input('RifaTurma.ativo', array(
                'options' => $status,
                'type' => 'select',
                'class' => 'selectpicker',
                'label' => 'Status',
                'div' => 'input-control text')); ?>
        </div>
    </div>
    <br />
    <div class="row-fluid">
        <div class="span2">
            <button type='submit' class='button max bg-color-greenDark'>
                Salvar
                <i class='icon-ok'></i>
            </button>
        </div>
    </div>
</div>