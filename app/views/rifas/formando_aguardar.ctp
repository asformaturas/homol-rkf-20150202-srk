<div class="row-fluid">
    <div class="span12">
        <h2 class="fg-color-red">
            <a class="metro-button reload fg-color-red border-color-red" data-bind="click: function() { reload() }"></a>
            <?= $rifa['Rifa']['nome']; ?>
        </h2>
    </div>
</div>
<?php $session->flash(); ?>
<?php if($cupomApp['CupomApp']['status'] == 'erro') : ?>
<div class="alert alert-error">
    <?=$cupomApp['CupomApp']['mensagem']?>
</div>
<?php else : ?>
<div class="alert alert-error">
    <h4>Os cupons estão sendo processados</h4>
    <br />
    Clique no botão abaixo atualizar
</div>
<?php endif; ?>
<a href="<?=$this->params['prefix'] ?>/rifas/cupons"
   class='button default' data-bind='click:loadThis'>
    Atualizar
    <i class="icon-reload"></i>
</a>