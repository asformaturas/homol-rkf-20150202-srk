<span id="conteudo-titulo" class="box-com-titulo-header">Rifas</span>
<div id="conteudo-container">
	<?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
	<?php $session->flash(); ?>
	<div class="tabela-adicionar-item">
		<?php echo $html->link('Vincular Rifa com Turma',array($this->params['prefix'] => true, 'action' => 'vincular_adicionar')); ?>
		<div style="clear:both;"></div>
	</div>
	<div class="container-tabela">
		<table>
			<thead>
				<tr>
					<th scope="col"><?php echo $paginator->sort('Id', 'id'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Cód. Turma', 'turma_id'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Cód. Rifa', 'rifa_id'); ?></th>				
					<th scope="col"><?php echo $paginator->sort('Cupons', 'cupons'); ?></th>
                    <th scope="col"><?php echo $paginator->sort('Ativa', 'ativo'); ?></th>
					<th scope="col"> &nbsp;</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="5"><?php echo $paginator->counter(array('format' => 'Registros %start% a %end% - página %page% de %pages%')); ?>
						<span class="paginacao">
							<?php echo $paginator->numbers(array('separator' => ' ')); ?>
						</span>
					</td>
					<td colspan="1"><?php echo $paginator->counter(array('format' => 'Total : %count% ' .  $this->name)); ?></td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach($rifasturmas as $rifaturma): ?>
				<?php if($isOdd):?>
					<tr class="odd">
				<?php else:?>
					<tr>
				<?php endif;?>
					<td colspan="1" width="5%"><?php echo $rifaturma['RifaTurma']['id'];?></td>
					<td colspan="1" width="25%"><?php echo "<b>".$rifaturma['RifaTurma']['turma_id']."</b> - <small>".$rifaturma['Turma']['nome']."</small>";?></td>
					<td colspan="1" width="25%"><?php echo "<b>".$rifaturma['RifaTurma']['rifa_id']."</b> - <small>".$rifaturma['Rifa']['nome']."</small>";?></td>
					<td colspan="1" width="5%"><?php echo $rifaturma['RifaTurma']['cupons'];?></td>
                    <td colspan="1" width="10%"><?php echo ($rifaturma['RifaTurma']['ativo'] == '1') ? 'Sim' : 'Não';?></td>
					<td colspan="1" width="30%">
						<?php //echo $html->link('Ver', array($this->params['prefix'] => true, 'controller' => 'rifas', 'action' =>'vincular_visualizar', $rifaturma['RifaTurma']['id']), array('class' => 'submit button')); ?>
						<?php //echo $html->link('Editar', array($this->params['prefix'] => true, 'controller' => 'rifas', 'action' =>'vincular_editar', $rifaturma['RifaTurma']['id']), array('class' => 'submit button')); ?>
						<?php 
						if($rifaturma['RifaTurma']['ativo'] == '1')
							echo $html->link('Desativar', array($this->params['prefix'] => true, 'controller' => 'rifas', 'action' =>'vincular_desativar', $rifaturma['RifaTurma']['id']), array('class' => 'submit button'));
						else	
						 	echo $html->link('Ativar', array($this->params['prefix'] => true, 'controller' => 'rifas', 'action' =>'vincular_ativar', $rifaturma['RifaTurma']['id']), array('class' => 'submit button'));
						
						?>
                    </td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>