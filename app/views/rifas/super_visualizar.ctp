<span id="conteudo-titulo" class="box-com-titulo-header">Informações da Rifa</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<div class="detalhes">
		<p class="grid_11 alpha omega">
			<label class="grid_11 alpha omega">Rifa</label>
			<span class="grid_11 alpha first"> <?php echo $rifa['Rifa']['nome'];?> </span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_11 alpha omega">Data do Sorteio</label>
			<span class="grid_11 alpha first"> <?php echo date('d/m/Y',strtotime($rifa['Rifa']['data-sorteio']));?> </span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_11 alpha omega">Prêmios</label>
			<span class="grid_11 alpha first"> 
				<?php
					echo "1&deg; Pr&ecirc;mio: ".$rifa['Rifa']['premio_1']."<br />";
					if ($rifa['Rifa']['premio_2'] != "") {
						echo "2&deg; Pr&ecirc;mio: ".$rifa['Rifa']['premio_2']."<br />";
					}
					if ($rifa['Rifa']['premio_3'] != "") {
						echo "3&deg; Pr&ecirc;mio: ".$rifa['Rifa']['premio_3'];
					}
				?>
			</span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_11 alpha omega">Número Sorteado</label>
			<span class="grid_11 alpha first"> <?php echo $rifa['Rifa']['numero_sorteado']; ?> </span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_11 alpha omega">Ativa</label>
			<span class="grid_11 alpha first"> <?php echo ($rifa['Rifa']['ativa'] == 1) ? 'Sim' : 'Não';?> </span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_11 alpha omega">Data de Criação</label>
			<span class="grid_11 alpha first"> <?php echo date('d/m/Y',strtotime($rifa['Rifa']['data_criacao']));?> </span>
		</p>
	</div>
</div>

