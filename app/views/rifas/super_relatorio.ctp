<script type="text/javascript">
    $(document).ready(function() {
        $("#gerar-excel").click(function() {
            if($("#rifa").val() == "")
                alert("Selecione uma rifa");
            else
                window.open('/<?=$this->params['prefix']?>/rifas/relatorio_excel/'+$("#rifa").val(),'_blank');
        });
    });
</script>
<span id="conteudo-titulo" class="box-com-titulo-header">Rifas</span>
<div id="conteudo-container">
    <?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
    <div>
        <?=$form->input('rifas',
            array(
                'type' => 'select',
                'empty' => 'Selecione',
                'id' => 'rifa',
                'options' => $rifas,
                'class' => 'grid6 alpha omega',
                'label' => false,
                'div' => false
            )); ?>
        <span class="button submit" id="gerar-excel">Gerar Excel</span>
        <div style="clear:both;"></div>
    </div>
    <div class="container-tabela">
        <table>
            <thead>
                <tr>
                    <th scope="col"><?=$paginator->sort('Cod Formando', 'codigo_formando'); ?></th>
                    <th scope="col"><?=$paginator->sort('Rifa', 'Rifa.id'); ?></th>				
                    <th scope="col"><?=$paginator->sort('No', 'numero'); ?></th>
                    <th scope="col"><?=$paginator->sort('Data Gerado', 'data_cadastro'); ?></th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="3"><?=$paginator->counter(array('format' => 'Cupons %start% à %end% - página %page% de %pages%')); ?>
                        <span class="paginacao">
                            <?=$paginator->numbers(array('separator' => ' ')); ?>
                        </span>
                    </td>
                    <td colspan="1"><?=$paginator->counter(array('format' => 'Total : %count% ' . $this->name)); ?></td>
                </tr>
            </tfoot>
            <tbody>
                <?php foreach ($cupons as $cupom): ?>
                <tr>
                    <td colspan="1" width="25%"><?=$cupom['Usuario']['codigo_formando']; ?></td>
                    <td colspan="1" width="25%"><?=$cupom['Rifa']['nome']; ?></td>
                    <td colspan="1" width="25%"><?=str_pad($cupom['Cupom']['numero'] % 100000, 5, '0',STR_PAD_LEFT); ?></td>
                    <td colspan="1" width="25%">
                        <?php if($form->validarData($cupom['Cupom']['data_cadastro'],'Y-m-d H:i:s')) : ?>
                        <?=date('d/m/Y', strtotime($cupom['Cupom']['data_cadastro'])); ?>
                        <?php else : ?>
                        Indefinido
                        <?php endif; ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>