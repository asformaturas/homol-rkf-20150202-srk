<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/bootstrap/datepicker.css">
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/bootstrap/datepicker.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $("#data_sorteio").datepicker();
    
    $("#form").submit(function(e) {
        e.preventDefault();
        var context = ko.contextFor($(".metro-button.reload")[0]);
        var dados = $("#form").serialize();
        var url = $("#form").attr('action');
        bootbox.hideAll();
        context.$data.showLoading(function() {
            $.ajax({
                url: url,
                data: dados,
                type: "POST",
                dataType: "json",
                complete: function() {
                    context.$data.reload();
                }
            });
        });
    });
});
</script>
<div class="row-fluid">
	<?php $session->flash(); ?>
	<?php echo $form->create('Rifa', array('url' => "/{$this->params['prefix']}/rifas/inserir/", 'id' => 'form')); ?>
    <div class="row-fluid">
        <div class="span4">
                <?php echo $form->input('nome', array('label' => 'Nome', 'div' => 'input-control text', 'error' => false)); ?>
        </div>
        <div class="span4">
                <?php echo $form->input('data', array('label' => 'Data Sorteio', 'div' => 'input-control text', 'error' => false, 'id' => 'data_sorteio')); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span8">
                <?php echo $form->input('premio_1', array('label' => 'Prêmio 1', 'div' => 'input-control text', 'error' => false)); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span8">
                <?php echo $form->input('premio_2', array('label' => 'Prêmio 2', 'div' => 'input-control text', 'error' => false)); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span8">
                <?php echo $form->input('premio_3', array('label' => 'Prêmio 3', 'div' => 'input-control text', 'error' => false)); ?>
        </div>
    </div>
    <br />
    <div class="row-fluid">
        <div class="span2">
            <button type='submit' class='button max bg-color-greenDark'>
                Salvar
                <i class='icon-ok'></i>
            </button>
        </div>
    </div>
</div>