<span id="conteudo-titulo" class="box-com-titulo-header">Rifas</span>
<div id="conteudo-container">
	<?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
	<?php $session->flash(); ?>
	<div class="tabela-adicionar-item">
		<?php echo $html->link('Nova Rifa',array($this->params['prefix'] => true, 'action' => 'adicionar')); ?>
		<div style="clear:both;"></div>
	</div>
	<div class="container-tabela">
		<table>
			<thead>
				<tr>
					<th scope="col"><?php echo $paginator->sort('Id', 'id'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Nome', 'nome'); ?></th>				
					<th scope="col">Prêmios</th>
                    <th scope="col"><?php echo $paginator->sort('Data Sorteio', 'data_sorteio'); ?></th>
                    <th scope="col"><?php echo $paginator->sort('Ativa', 'ativa'); ?></th>
					<th scope="col"> &nbsp;</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="5"><?php echo $paginator->counter(array('format' => 'Rifas %start% à %end% - página %page% de %pages%')); ?>
						<span class="paginacao">
							<?php echo $paginator->numbers(array('separator' => ' ')); ?>
						</span>
					</td>
					<td colspan="1"><?php echo $paginator->counter(array('format' => 'Total : %count% ' .  $this->name)); ?></td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach($rifas as $rifa): ?>
				<?php if($isOdd):?>
					<tr class="odd">
				<?php else:?>
					<tr>
				<?php endif;?>
					<td colspan="1" width="5%"><?php echo $rifa['Rifa']['id'];?></td>
					<td colspan="1" width="20%"><?php echo $rifa['Rifa']['nome'];?></td>
					<td colspan="1" width="20%">
						<?php 
							echo substr($rifa['Rifa']['premio_1'].'<br />',0,50);
							echo substr($rifa['Rifa']['premio_2'].'<br />',0,50);
							echo substr($rifa['Rifa']['premio_3'],0,50);
						?>
					</td>
                    <td colspan="1" width="15%"><?php echo date('d/m/Y',strtotime($rifa['Rifa']['data_sorteio'])); ?></td>
                    <td colspan="1" width="10%"><?php echo ($rifa['Rifa']['ativa'] == '1') ? 'Sim' : 'Não';?></td>
					<td colspan="1" width="30%">
						<?php echo $html->link('Ver', array($this->params['prefix'] => true, 'controller' => 'rifas', 'action' =>'visualizar', $rifa['Rifa']['id']), array('class' => 'submit button')); ?>
						<?php //echo $html->link('Editar', array($this->params['prefix'] => true, 'controller' => 'rifas', 'action' =>'editar', $rifa['Rifa']['id']), array('class' => 'submit button')); ?>
						<?php 
						if ($rifa['Rifa']['ativa'] == '1')
							echo $html->link('Desativar', array($this->params['prefix'] => true, 'controller' => 'rifas', 'action' =>'desativar', $rifa['Rifa']['id']), array('class' => 'submit button')); 
						else
							echo $html->link('Ativar', array($this->params['prefix'] => true, 'controller' => 'rifas', 'action' =>'ativar', $rifa['Rifa']['id']), array('class' => 'submit button')); 
						
						?>
						<?php //echo $html->link('Buscar Cupom', array($this->params['prefix'] => true, 'controller' => 'rifas', 'action' =>'buscar_cupom', $rifa['Rifa']['id']), array('class' => 'submit button')); ?>
                    </td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>