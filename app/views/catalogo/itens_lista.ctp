<link rel="stylesheet" type="text/css" href="<?=$this->webroot ?>metro/css/min/bootmetro/icons.css?v=0.2">
<div class="row">
    <div class="col-lg-10">
        <?php foreach($catalogos as $i => $catalogo) : ?>
            <div data-catalogo-id="<?=$catalogo['Catalogo']['id']?>" class="panel panel-default lista-item">
                <div class="panel-body">
                    <a class="lista-titulo text-danger text-uppercase"
                    href="/<?=$this->params['prefix']?>/catalogo/detalhes/<?=$catalogo['Catalogo']['id']?>">
                    <?=$catalogo['Catalogo']['nome']?>
                    <span class="faicon faicon-angle-right faicon-lg pull-right"></span>
                </a>
                <span class="faicon faicon-refresh pull-right" style="margin-right: 10%">
                    <?=date("d/m",strtotime($catalogo['Catalogo']['data_ultima_alteracao']))?>
                </span> 
                <?php if($grupo_usuario == 'marketing'): ?>
                    <span class="text-danger">
                        <i class="icon-remove desativar" data-id="<?=$catalogo['Catalogo']['id']?>"></i>
                    </span>
                <?php endif;?>
            </div>
        </div>
    <?php endforeach; ?>
</div>
</div>
<script>
    $(".desativar").click(function(){
        var catalogoId = $(this).data("id");
        if(confirm("Você deseja realmente desativar este item?")){
            $.ajax({
                url: "/<?=$this->params['prefix']?>/catalogo/detalhes/"+catalogoId+"/1?>",
                type: "POST",
                success: function() {
                    $("[data-catalogo-id='"+catalogoId+"']").fadeOut("slow");
                }
            });
        }
    });
</script>