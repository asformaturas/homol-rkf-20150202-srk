<script type="text/javascript">
	jQuery(function($) {
 		$(".hover").click(function() {
			$(this).colorbox({
 	 			width:"930",
 	 			height:"900",
 	 			escKey : false,
 	 			href: "/<?=$this->params['prefix']?>/vincular_pagamentos/listar_despesas_formando/" + $(this).find('.codigo-formando').html(),
 	 			overlayClose : false,
 	 			close : false,
 	 			top : 0,
 	 			onCleanup : function() { window.location.reload(); }
 	 	 	});
		});
 	});
</script>
<style type="text/css">
#cboxLoadedContent { background-color:white!important; border:solid 1px black; border-radius:4px; padding:20px }
</style>
<span id="conteudo-titulo" class="box-com-titulo-header">Turmas gerenciadas</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	
	<div class="tabela-adicionar-item">
		<?=$form->create(false, array('url' => "/{$this->params['prefix']}/vincular_pagamentos", 'class' => 'procurar-form-inline')); ?>
		<span>Busca: <?=$form->input('filtro-id', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false , 'value' => isset($valor) ? $valor : "")); ?> </span>
		<?=$form->input('filtro-tipo', array('options' => $arrayOptions, 'type' => 'select', 'class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
		&nbsp;&nbsp;&nbsp;
		<span>Qtde Por Página:
		<?=$form->input('qtde-por-pagina', array('options' => array("20" => "20" , "50" => "50" , "100" => "100"), 'type' => 'select', 'class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
		</span>
		&nbsp;
		<?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
		<?=$form->end(array('label' => ' ', 'div' => false, 'class' => 'submit-busca')); ?>
		<div style="clear:both;"></div>
	</div>
    <div class="container-tabela">
    	<table>
            <thead>
                <tr>
                    <th scope="col"><?=$paginator->sort('Código do Formando', 'codigo_formando'); ?></th>
                    <th scope="col"><?=$paginator->sort('Nome', 'nome'); ?></th>
                    <th scope="col"><?=$paginator->sort('E-mail', 'email'); ?></th>
                    <th scope="col"><?=$paginator->sort('Turma', 'Turma.nome'); ?></th>
                    <th scope="col"><?=$paginator->sort('Curso', 'curso_nome'); ?></th>
                    <th scope="col">Não Vinculado</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="6"><?=$paginator->counter(array('format' => 'Itens %start% ao %end% - página %page% de %pages%')); ?>
                        <span class="paginacao">
                            <?=$paginator->numbers(array('separator' => ' ')); ?>
                        </span>
                    </td>
                    <td colspan="2"><?=$paginator->counter(array('format' => 'Total : %count% ')); ?></td>
                </tr>
            </tfoot>
            <tbody>
                <?php $isOdd = false; ?>
                <?php foreach ($formandos as $formando): ?>
                	<tr class="hover<?=$isOdd ? " odd" : ""?>">
                        <td  colspan="1" width="10%" class='codigo-formando'><?=isset($formando['ViewFormandos']['codigo_formando']) ? $formando['ViewFormandos']['codigo_formando'] : $formando[0]['codigo_formando']; ?></td>
                        <td  colspan="1"><?=$formando['ViewFormandos']['nome']; ?></td>
                        <td  colspan="1"><?=$formando['ViewFormandos']['email']; ?></td>
                        <td  colspan="1"><?=$formando['Turma']['nome']; ?></td>
                        <td  colspan="1"><?=$formando['ViewFormandos']['curso_nome']; ?></td>
                        <td  colspan="1"><?=$formando[0]['nao_vinculado']; ?></td>
                    </tr>
                    <?php $isOdd = !($isOdd); ?>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>