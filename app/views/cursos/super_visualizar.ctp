<span id="conteudo-titulo" class="box-com-titulo-header">Informações do Curso</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<div class="detalhes">
		<p class="grid_11 alpha omega">
			<label class="grid_11 alpha omega">Faculdade</label>
			<span class="grid_11 alpha first"> <?php echo $curso['Faculdade']['nome']?> </span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Curso</label>
			<label class="grid_6 alpha">Sigla</label>
			<span class="grid_5 alpha first"><?php echo $curso['Curso']['nome']?></span>
			<span class="grid_5 alpha"><?php echo $curso['Curso']['sigla']?></span>
		</p>
		<p class="grid_11 alpha omega">
			<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'action' => 'index') ,array('class' => 'cancel')); ?>
			<?php echo $html->link('Editar', array($this->params['prefix'] => true, 'controller' => 'Cursos', 'action' => 'editar', $curso['Curso']['id']), array('class' => 'submit ')); ?>
			<?php echo $html->link('Adicionar Outro', array($this->params['prefix'] => true, 'controller' => 'Cursos', 'action' => 'adicionar'), array('class' => 'submit ')); ?>
		</p>
	</div>
</div>