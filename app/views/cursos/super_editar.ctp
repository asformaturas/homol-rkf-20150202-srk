<span id="conteudo-titulo" class="box-com-titulo-header">Cursos - Editar</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<?php echo $form->create('Curso', array('url' => "/{$this->params['prefix']}/cursos/editar")); ?>
	<?php include('_form.ctp'); ?>
	
	<p class="grid_11 alpha omega">
		<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'action' => 'index') ,array('class' => 'cancel')); ?>
		<?php echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'submit'));?>
	</p>
</div>