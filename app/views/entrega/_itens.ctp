<script type="text/javascript">
	jQuery(function($) {
 		$(".dados-financeiros").click(function(e) {
 	 		e.preventDefault();
 	 		var id = $(this).parent().parent().find('.usuario-id').val();
			$(this).colorbox({
 	 			width:jQuery('body').width()*0.8,
 	 			height:"900",
 	 			escKey : false,
 	 			href: "/<?=$this->params['prefix']?>/vincular_pagamentos/listar_despesas_formando/" + id,
 	 			overlayClose : false,
 	 			close : false,
 	 			top : 0
 	 	 	});
		});
 		$("#link-gerar-excel").click(function() {
			$("#form-filtro").attr('target', '_blank');
			$("#form-filtro").attr('action', $("#form-filtro").attr('action').replace('index', 'gerar_excel'));
			$("#form-filtro").submit();
			$("#form-filtro").attr('action', $("#form-filtro").attr('action').replace('gerar_excel', 'index'));
			$("#form-filtro").attr('target', '');
			return false;
		});
 	});
</script>
<style type="text/css">
#cboxLoadedContent { background-color:white!important; border:solid 1px black; border-radius:4px; padding:20px }
</style>
<span id="conteudo-titulo" class="box-com-titulo-header">Turmas gerenciadas</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	
	<div class="tabela-adicionar-item">
		<?=$form->create(false, array('url' => "/{$this->params['prefix']}/entrega/itens", 'class' => 'procurar-form-inline', 'id' => 'form-filtro')); ?>
		<span>Busca: <?=$form->input('campo', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false , 'value' => isset($valor) ? $valor : "")); ?> </span>
		<?=$form->input('filtro', array('options' => array('codigo_formando' => 'Código do Formando', 'turma_id' => 'Turma'), 'type' => 'select', 'class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
		&nbsp;
		<?php if(!$resumo) $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
		<?=$form->end(array('label' => ' ', 'div' => false, 'class' => 'submit-busca')); ?>
		<?php echo $html->link('Gerar excel',array($this->params['prefix'] => true, 'action' => 'index'), array('id' => 'link-gerar-excel')); ?>
		<div style="clear:both;"></div>
	</div>
</div>