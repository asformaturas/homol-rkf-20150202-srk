<?php 
	$listaDeFormandosParaExcel = array();
	foreach ($formandos as $formando) {
		$saldoAberto = $formando['ViewRelatorioFinanceiro']['valor_total'] - $formando['ViewRelatorioFinanceiro']['total_pago'];
		$listaDeFormandosParaExcel[] = array(
				"Codigo do formando" => "{$formando['ViewRelatorioFinanceiro']['codigo_formando']} ",
				"Nome" =>  $formando['ViewRelatorioFinanceiro']['nome'],
				"Status" =>  $formando['ViewRelatorioFinanceiro']['status'],
				"Total de Parcelas" => $formando['ViewRelatorioFinanceiro']['total_parcelas'],
				"Valor da Parcela" => number_format($formando['ViewRelatorioFinanceiro']['valor_parcela'], 2, ',' , '.'),
				"Valor Total" => number_format($formando['ViewRelatorioFinanceiro']['valor_total'], 2, ',' , '.'),
				"Data 1a Parcela" => $formando['ViewRelatorioFinanceiro']["data_primeira_parcela"],
				"Parcelas Pagas" => $formando['ViewRelatorioFinanceiro']['parcelas_pagas'],
				"Total Pago" => number_format($formando['ViewRelatorioFinanceiro']['total_pago'], 2, ',' , '.'),
				"Parcelas Atrasadas" => $formando['ViewRelatorioFinanceiro']['parcelas_atrasadas'],
				"Saldo Em Aberto" => number_format($saldoAberto, 2, ',' , '.')
			);
	}
	$excel->generate($listaDeFormandosParaExcel, date("Y_m_d_") . 'relatorio_financeiro_formandos_turma_'.$turmaLogada['Turma']['id']); 
?>