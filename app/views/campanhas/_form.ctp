<script type="text/javascript">
    $(function() {
        $( "#datepicker_inicial_aux" ).datepicker( $.datepicker.regional[ "pt-BR" ] );
        $( "#datepicker_final_aux" ).datepicker( $.datepicker.regional[ "pt-BR" ] );
    });
    
    //$('#conteudo-wrap').delegate('.remover-inline', 'click', function() {
    //    $(this).parent().remove();
    //});
    
    // Quando clicar em remover, insere uma acao em um campo escondido para que seja deletado pelo controller
    //$('li a.remover-inline').click(function(){
    $('#conteudo-wrap').delegate('.remover-inline', 'click', function() {
        removerInline($(this));
    });
    
    $('#conteudo-wrap').delegate('#adicionar-inline-extra', 'click', function() {
        $('#adicionar-inline-extra').html('Salvar').attr('id', 'salvar-inline-extra');
        $('#adicionar-extra-select').attr('disabled',true);
        $('#select-extra').show();
    });
    
    $('#conteudo-wrap').delegate('#salvar-inline-extra', 'click', function() {
        if($('#adicionar-extra-select').val() != null && $('#adicionar-extra-select').val() != "") {
            $('#salvar-inline-extra').html('Adicionar Item').attr('id', 'adicionar-inline-extra');
            $('#adicionar-extra-select').attr('disabled', true);
            $('#CampanhaEvento option:first').attr('selected', 'selected');
            $('#select-extra').hide();



            var extra_id = $('#adicionar-extra-select').val();
            $("#list option[value='2']").text()
            var extra_nome = $('#adicionar-extra-select option[value="' + extra_id + '"]').text();
            var evento_id = $('#CampanhaEvento').val();
            var evento_nome = 'Nome do Evento';
            var quantidade_minima = $('#extraQuantidadeMinima').val();
            var quantidade_maxima = $('#extraQuantidadeMaxima').val();
            var quantidade_disponivel = $('#extraQuantidadeDisponivel').val();
            var valor = $('#extraValor').val();
            var campanha_extra_id = null;
			
            //alert(extra_id + "|" + extra_nome  + "|" + quantidade_minima  + "|" + quantidade_maxima + "|" + valor);
			

            oldValue = $('#adicionar_extras_array').val();
            if(oldValue == ''){
                $('#adicionar_extras_array').val(
                '{extra_id:' + extra_id + 
                    ', evento_id:' + evento_id +
                    ', quantidade_minima:' + quantidade_minima +
                    ', quantidade_maxima:' + quantidade_maxima +
                    ', quantidade_disponivel:' + quantidade_disponivel +
                    ', valor:' + valor +
                    ', campanha_extra_id: ' + campanha_extra_id +
                    '}');
            } else {
                $('#adicionar_extras_array').val(oldValue + 
                    '{extra_id:' + extra_id + 
                    ', evento_id:' + evento_id +
                    ', quantidade_minima:' + quantidade_minima +
                    ', quantidade_maxima:' + quantidade_maxima +
                    ', quantidade_disponivel:' + quantidade_disponivel +
                    ', valor:' + valor +
                    ', campanha_extra_id:' + campanha_extra_id +
                    '}');
            }
			

            $('ul#adicionar-extra-inline').append('<li><a class="remover-inline"></a>' + "\n" +
                extra_nome + ', (' + evento_nome + ') ,' +
                ' Mínimo: ' + quantidade_minima + ', Máximo: ' + quantidade_maxima +
                ', Disponível: ' + quantidade_disponivel + ', Valor: R$' + valor +
                '<input name=data[Campanha][extra][] type="hidden" value="(' + extra_id + ')"' +
                '</input</li>');
        } else {
            //alert('Erro');
        }
    });
    
    $('#conteudo-wrap').delegate('#CampanhaEvento', 'change', function() {
        if($('#CampanhaEvento').val() != null && $('#CampanhaEvento').val() != "") {
            $('#adicionar-extra-select').html('');
            $('#adicionar-extra-select').attr('disabled',true);
            $.get("<?php echo $html->url(array($this->params['prefix'] => true, 'controller' => 'campanhas', 'action' => 'extra')); ?>/" + $('#CampanhaEvento').val(), function(data) {
                jQuery.each(jQuery.parseJSON(data), function() {
                    $('#adicionar-extra-select').html($('#adicionar-extra-select').html() + "<option value=" + this.Extra.id + ">"+ this.Extra.nome + "</option>");
                });
            });
            $('#adicionar-extra-select').attr('disabled',false);
        }
        else {
            $('#adicionar-extra-select').html('');
            $('#adicionar-extra-select').attr('disabled',true);
        }
    });
    
    function removerInline(objeto){
        if(confirm("Remover o item extra da campanha?")) {
            var campanha_extra_id = $(objeto).parent().find('span.campanha_extra_id').html();
            
            if($('#remover_extras_array').val() == '')
                $('#remover_extras_array').val('{campanha_extra_id:' + campanha_extra_id +'}');
            else {
                var oldValue = $('#remover_extras_array').val();
                $('#remover_extras_array').val(oldValue + ',{campanha_extra_id:' + campanha_extra_id + '}');
            }
            $(objeto).parent().remove();
        }
	
    }
</script>

<?php echo $form->input('id', array('hiddenField' => true)); ?>

<p class="grid_11 alpha omega">
    <label class="grid_5 alpha">Nome</label>
    <?php echo $form->input('nome', array('class' => 'grid_8 alpha first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
</p>

<p class="grid_11 alpha omega">
    <label class="grid_5 alpha">Tipo</label>
    <?php echo $form->select('tipo', array('final' => 'Final','comum' => 'Comum'), 'comum', array('class' => 'grid_8 alpha first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
</p>

<p class="grid_11 alpha omega">
    <label class="grid_5 alpha">Descrição</label>
    <?php echo $form->input('descricao', array('class' => 'grid_8 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
</p>

<?php echo $form->input('adicionar_extras_array', array('style' => 'display:none;', 'label' => false, 'div' => false, 'id' => 'adicionar_extras_array')); ?>
<?php echo $form->input('remover_extras_array', array('style' => 'display:none;', 'label' => false, 'div' => false, 'id' => 'remover_extras_array')); ?>

<div class="grid_11 alpha omega crud-inline">
    <label class="grid_0 alpha omega">Itens da campanha</label>
    <ul id="adicionar-extra-inline">
        <?php
        if (@$this->data['CampanhasExtra'] != '') {
            foreach ($this->data['CampanhasExtra'] as $extra):
                ?>
                <li>
                    <a class="remover-inline"></a>
                    <?php
                    echo $extra['Extra']['Evento']['nome'] . ' - ' . $extra['Extra']['nome'] .
                    	' - min: ' . $extra['quantidade_minima'] . ' - máx: ' . $extra['quantidade_maxima'] .
                    	' - disponível: ' . $extra['quantidade_disponivel'] . ' - valor: ' . $extra['valor']
                    ?>
                    <span class="campanha_extra_id" style="display:none"><?php echo $extra['id']; ?></span>
                    <input name="data[Campanha][extra][]" type="hidden" value="<?php echo $extra['Extra']['id']; ?>"></input>
                </li>
            <?php endforeach;
        } ?>
    </ul>
    <div class="adicionar-inline-form">
        <div id="select-extra" style="display:none">
            <br>
            <label>Selecione o Evento</label>
            <p class="grid_11 alpha omega">
                <?php echo $form->select('evento', $eventos, array('class' => 'grid_8 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))) ?>
            </p>
            <p class="grid_11 alpha omega">
                <label>Selecione o Extra</label>
                <select type="select" id="adicionar-extra-select"></select>
            </p>

            <!-- Quantidade Mínima -->
            <p class="grid_11 alpha omega">
                <label class="grid_5 alpha">Quantidade Mínima</label>
                <?php echo $form->input('extra.quantidade_minima', array('class' => 'grid_8 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
            </p>

            <!-- Quantidade Máxima -->
            <p class="grid_11 alpha omega">
                <label class="grid_5 alpha">Quantidade Máxima</label>
                <?php echo $form->input('extra.quantidade_maxima', array('class' => 'grid_8 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
            </p>
            
            <p class="grid_11 alpha omega">
                <label class="grid_5 alpha">Quantidade Dispon&iacute;vel</label>
                <?php echo $form->input('extra.quantidade_disponivel', array('class' => 'grid_8 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
            </p>

            <!-- Valor -->
            <p class="grid_11 alpha omega">
                <label class="grid_5 alpha">Valor (R$)</label>
                <?php echo $form->input('extra.valor', array('class' => 'grid_8 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
            </p>

        </div>
    </div>
    <p class="grid_11 alpha omega">
        <a class="adicionar-inline" id="adicionar-inline-extra">Adicionar Item</a>
    </p>
</div>

<p class="grid_11 alpha omega">
    <label class="grid_5 alpha">Máximo de parcelas</label>
    <?php echo $form->input('max_parcelas', array('class' => 'grid_8 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
</p>

<p class="grid_11 alpha omega">
    <label class="grid_0 alpha omega">Data Início</label>
    <?php echo $form->input('data_inicio_aux', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'), 'id' => 'datepicker_inicial_aux')); ?>
</p>

<p class="grid_11 alpha omega">
    <label class="grid_0 alpha omega">Data Fim</label>
    <?php echo $form->input('data_fim_aux', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'), 'id' => 'datepicker_final_aux')); ?>
</p>