<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootstrap/select.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<style type="text/css">
    .bootstrap-select > .btn{
        width: 100px!important;
    }
    .quantidade-comprada{
        margin-top: 5px;
        width: 100px!important
    }
    .parcelamento{
        width: 100px!important
    }
    .dropdown-menu.inner{
        max-height: 160px!important
    }
</style>
<script type="text/javascript">
    $(function() {
        $('.selectpicker').selectpicker({width:'100%'});
        $(".quantidade-comprada").change(function() {
            quantidadeEscolhida = this.value;
            idDaRelacaoCampanhaExtra = this.id.replace("campanhaExtra", "");
            valorUnitario = stringToFloat(stringMoneyToNumber($("#valor-unitario-" + idDaRelacaoCampanhaExtra).html()), 2);
            valorTotal = (parseFloat(valorUnitario) * parseFloat(quantidadeEscolhida)).toFixed(2);
            valorTotal = number_format(valorTotal, 2, ',', '.');
            $("#valor-compra-" + idDaRelacaoCampanhaExtra).html(valorTotal);
            valorTotalDeTodasAsCompras = 0.0;

            $(".valor-compra").each(function() {
                valorTotalDaCompra = stringToFloat(stringMoneyToNumber($(this).html()), 2);
                valorTotalDeTodasAsCompras += valorTotalDaCompra;
            });

            $("#valor-total").html(number_format(valorTotalDeTodasAsCompras, 2, ',', '.'));

            numeroDeParcelas = 1.0;

            $("#numero-de-parcelas").each(function() {
                numeroDeParcelas = parseFloat(this.value);
            });

            valorParcelado = (valorTotalDeTodasAsCompras / numeroDeParcelas).toFixed(2);
            $("#valor-parcelado").html(number_format(valorParcelado, 2, ',', '.'));
            return false;
        });

        $("#numero-de-parcelas").change(function() {
            valorTotalDeTodasAsCompras = $("#valor-total").html().replace(".", "").replace(",", ".");
            numeroDeParcelas = this.value;
            valorParcelado = (valorTotalDeTodasAsCompras / numeroDeParcelas).toFixed(2);
            $("#valor-parcelado").html(number_format(valorParcelado, 2, ',', '.'));
            return false;
        });

        $("#comprar").click(function(e) {
            e.stopPropagation();
            e.preventDefault();
            $(this).attr('disabled', 'disabled');
            $("#conteudo-load").stop().fadeTo(200, 0, function() {
                data = $("#form-adesao-campanha").serialize();
                var url = "/<?= $this->params["prefix"] ?>/campanhas/inserir";
                $("#conteudo-load").css({'opacity': 1, 'display': 'none'});
                $.ajax({
                    url: url,
                    data: data,
                    type: "POST",
                    dataType: "json",
                    success: function(response) {
                        carregarPagina("<?= $this->here ?>");
                    },
                    error: function(response) {
                        carregarPagina("<?= $this->here ?>");
                    }
                });
            });
        });
    });
    function enviarCompra(callback) {
        data = $("#form-adesao-campanha").serialize();
        var url = "/<?= $this->params["prefix"] ?>/campanhas/inserir";
        $.ajax({
            url: url,
            data: data,
            type: "POST",
            dataType: "json",
            complete: function() {
                callback();
            }
        });
    }
</script>
<div class="row-fluid">
    
    <div class="span12">
        <h2>
            <a class="metro-button back" data-bind="click: function() { 
               page('<?= "/{$this->params['prefix']}/campanhas/listar" ?>') }"></a>
               <?= $campanha ? "{$campanha['Campanha']['nome']}" : "Campanhas" ?>
        </h2>
    </div>
</div>
<br />
<?php $session->flash(); ?>
<div class="row-fluid">
    <div class="span12">
        <h3><?= $campanha['Campanha']['descricao'] ?><small> - <?= $campanha['Campanha']['status']; ?></small></h3>
    </div>
</div>
<br />
<div class="row-fluid">
    <div class="span4">
        <span>Data In&iacute;cio</span>
        <label class="strong"><?= date('d/m/Y', strtotime($campanha['Campanha']['data_inicio'])); ?></label>
    </div>
    <div class="span4">
        <span>Data Fim</span>
        <label class="strong"><?= date('d/m/Y', strtotime($campanha['Campanha']['data_fim'])); ?></label>
    </div>
    <div class="span4">
        <span>Max Parcelas</span>
        <label class="strong"><?= $campanha['Campanha']['max_parcelas']; ?></label>
    </div>
</div>
<br />
<?php if($campanha['CampanhasExtra'][0]['ativo'] != 0){ ?>
<div class="row-fluid">
    <div class="span12">
        <?= $form->create(false, array('url' => "/{$this->params['prefix']}/campanhas/comprar", "id" => "form-adesao-campanha")); ?>
        <input type="hidden" name="data[Campanha][id]" value="<?= $campanha['Campanha']['id'] ?>" id="CampanhaId"></input>
        <div class="row-fluid">
            <div class="span12">
                <h3>Extras Dispon&iacute;veis Para Compra</h3>
            </div>
        </div>
        <table class="table table-condensed table-striped">
            <thead>
                <tr>
                <tr>
                    <th scope="col">Evento</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Valor Unit&aacute;rio</th>
                    <th scope="col">Qtde. para compra.</th>
                    <th scope="col">Valor da Compra</th>
                </tr>
            </thead>
            <tbody>
                <?php $valorTotal = 0; ?>
                <?php foreach ($campanha['CampanhasExtra'] as $extra): ?>
                <?php if($extra['ativo'] == 1 && strpos($extra['Extra']['nome'], 'Comiss') == false &&
                         strpos($extra['Extra']['nome'], 'Benef') == false) : ?>
                    <tr>
                        <td colspan="1"><?= $extra['Extra']['Evento']['nome']; ?></td>
                        <td colspan="1"><?= $extra['Extra']['nome']; ?></td>		
                        <td colspan="1">
                            R$<span id="valor-unitario-<?= $extra['id']; ?>">
                                <?= number_format($extra['valor'], 2, ',', '.'); ?></span>
                        </td>
                        <td colspan="1" style="text-align: center">
                            <?php
                            if ($campanha['Campanha']['status'] == 'Aberta')
                                echo $form->select('campanhaExtra' . $extra['id'], $optionsDeCompraDeExtras[$extra['id']], '0', array('id' => 'campanhaExtra' . $extra['id'], 'empty' => false,
                                    'class' => 'quantidade-comprada selectpicker', 'label' => false, 'div' => false, 'style' => 'margin-top: 5px'));
                            else
                                echo '0';
                            ?>
                        </td>
                        <td colspan="1">R$<span class="valor-compra" id="valor-compra-<?= $extra['id']; ?>">0,00</span></td>
                    </tr>
                    <?php endif; ?>
                    <?php $valorTotal += $extra['valor']; ?>
                <?php endforeach; ?>
            </tbody>
        </table>
        <br />
        <br />
        <?php if ($campanha['Campanha']['status'] == 'Aberta') : ?>
            <div class="row-fluid">
                <div class="span3" style="margin-top: 10px">
                    <label>
                        <strong>Parcelamento</strong>
                    </label>
                </div>
                <div class="span2">
                    <?php
                        $optionsDeParcelas = array();
                        for ($i = 1; $i <= $campanha['Campanha']['max_parcelas']; $i++)
                            $optionsDeParcelas[$i] = $i;
                        echo $form->select('numero-de-parcelas', $optionsDeParcelas, 0, array('label' => false, 'div' => false, 'class' => 'selectpicker parcelamento'));
                    ?>

                </div>
                <div class="span2 fg-color-greenDark pull-left" style="margin-top: 10px">
                    <label><strong>X  R$ <span id="valor-parcelado">0,00</span></strong></label>
                </div>
                <div class="span5" style="margin-top: 10px">
                    <label><strong class="pull-right">Valor total: R$ <span id="valor-total">0,00</span></strong></label>
                </div>
            </div>
            <br />
            <br />
            <div class="row-fluid">
                <div class="span12">
                    <button type="button" class="button bg-color-red fg-color-white pull-right"
                            data-bind="click: function() {
                            showLoading(function() { enviarCompra(function() { reload() }) }) }">
                        Comprar
                        <i class="icon-thumbs-up"></i>
                    </button>
                </div>
            </div>
        <?php endif; ?>
<?= $form->end(array('label' => false, 'div' => false, 'class' => 'hide')); ?>
    </div>
</div>
<?php }else{ ?>
    <h2 class="fg-color-red">Não há extras ativos para esta campanha.</h2>
<?php } ?>