<span id="conteudo-titulo" class="box-com-titulo-header">Campanhas</span>
<div id="conteudo-container">
	<?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
	<?php $session->flash(); ?>
	<div class="tabela-adicionar-item">
		<?php echo $html->link('Nova Campanha',array($this->params['prefix'] => true, 'action' => 'adicionar')); ?>
		<div style="clear:both;"></div>
	</div>
	<div class="container-tabela">
		<table>
			<thead>
				<tr>
					<th scope="col"><?php echo $paginator->sort('Id', 'id'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Nome', 'nome'); ?></th>				
					<th scope="col"><?php echo $paginator->sort('Descrição', 'descricao'); ?></th>
                    <th scope="col"><?php echo $paginator->sort('Máx Parcelas', 'max_parcelas'); ?></th>
                    <th scope="col"><?php echo $paginator->sort('Data Início', 'data_inicio'); ?></th>
                    <th scope="col"><?php echo $paginator->sort('Data Fim', 'data_fim'); ?></th>
                    <th scope="col"><?php echo $paginator->sort('Tipo', 'tipo'); ?></th>
					<th scope="col"> &nbsp;</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="7"><?php echo $paginator->counter(array('format' => 'Campanha %start% ao %end% - página %page% de %pages%')); ?>
						<span class="paginacao">
							<?php echo $paginator->numbers(array('separator' => ' ')); ?>
						</span>
					</td>
					<td colspan="1"><?php echo $paginator->counter(array('format' => 'Total : %count% ' .  $this->name)); ?></td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach($campanhas as $campanha): ?>
				<?php if($isOdd):?>
					<tr class="odd">
				<?php else:?>
					<tr>
				<?php endif;?>
					<td colspan="1" width="5%"><?php echo $campanha['Campanha']['id'];?></td>
					<td colspan="1" width="10%"><?php echo $campanha['Campanha']['nome'];?></td>
					<td colspan="1" width="20%"><?php echo $campanha['Campanha']['descricao']?></td>
                    <td colspan="1" width="10%"><?php echo $campanha['Campanha']['max_parcelas']?></td>
                    <td colspan="1" width="10%"><?php echo date('d/m/Y',strtotime($campanha['Campanha']['data_inicio'])); ?></td>
                    <td colspan="1" width="5%"><?php echo date('d/m/Y',strtotime($campanha['Campanha']['data_fim']));?></td>
                    <td colspan="1" width="10%"><?php echo $campanha['Campanha']['tipo'];?></td>
					<td colspan="1" width="30%">
					<?php echo $html->link('Visualizar', array($this->params['prefix'] => true, 'controller' => 'campanhas', 'action' =>'visualizar', $campanha['Campanha']['id']), array('class' => 'submit button')); ?>
					<?php echo $html->link('Editar', array($this->params['prefix'] => true, 'controller' => 'campanhas', 'action' =>'editar', $campanha['Campanha']['id']), array('class' => 'submit button')); ?>
                                        <?php echo $html->link('Excluir', array($this->params['prefix'] => true, 'controller' => 'campanhas', 'action' =>'excluir', $campanha['Campanha']['id']), array('class' => 'submit button')); ?></td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>