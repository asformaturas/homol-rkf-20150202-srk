<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/metroui/table.css">
<div class="row-fluid">
   <h2>
       <a class="metro-button back" data-bind="click: function() { 
         page('<?= $this->webroot . $this->params['prefix'] ?>/campanhas/') }">
       </a>
       <?php echo $data['Campanha']['nome'];?>
   </h2>
</div>
	<?php $session->flash(); ?>
<div class="row-fluid">
        <?php include('_visualizar.ctp');?>

    <span class="span8" style="padding-top: 20px;">
            <table class="table-bordered">
                    <thead>
                            <tr class="bg-color-lighten" style="height: 40px; color: black!important">
                                    <th scope="col">Id</th>
                                    <th scope="col">Evento</th>
                                    <th scope="col">Nome</th>
                                    <th scope="col">Qtde. Min.</th>
                                    <th scope="col">Qtde. Máx.</th>
                                    <th scope="col">Valor</th>
                            </tr>
                    </thead>
                    <tbody>
                            <tr>
                    <?php foreach($data['CampanhasExtra'] as $extra): ?>
                                    <td colspan="1"><?php echo $extra['Extra']['id'];?></td>
                                    <td colspan="1"><?php echo $extra['Extra']['Evento']['nome'];?></td>
                                    <td colspan="1"><?php echo $extra['Extra']['nome'];?></td>
                                    <td colspan="1"><?php echo $extra['quantidade_minima'];?></td>
                                    <td colspan="1"><?php echo $extra['quantidade_maxima']?></td>
                                    <td colspan="1"><?php echo $extra['valor'];?></td>
                            </tr>
                    <?php endforeach; ?>
                    </tbody>
            </table>
    </span>
</div>