<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/metroui/table.css">

<span class="span8" style="padding-top: 10px;">
	<strong>Nome</strong><br/>
	<?php echo $data['Campanha']['nome']; ?>
</span>


<span class="span8" style="padding-top: 10px;">
	<strong>Descrição</strong><br/>
	<?php echo $data['Campanha']['descricao']; ?>
</span>


<?php
// Não mostrar 'tipo de campanha' para o formando
if($this->params['prefix'] != 'formando') {
	?>
<span class="span8" style="padding-top: 10px;">
	<strong>Tipo</strong><br/>
	<?php echo $data['Campanha']['tipo']; ?>
</span>
<?php
}
?>

<span class="span4" style="padding-top: 10px;">
	<strong class="pull-left">Data Início</strong><br/>
        <span class="pull-left"><?php echo date('d/m/Y',strtotime($data['Campanha']['data_inicio'])); ?></span>
</span>
<br/>
<span class="span4" style="padding-top: 10px;">
	<strong class="pull-right">Data Fim</strong><br/>
	<span class="pull-right"><?php echo date('d/m/Y',strtotime($data['Campanha']['data_fim'])); ?></span>	
</span>
<br/>
<span class="span4" style="padding-top: 10px;">
	<strong class="pull-left">Máx. Parcelas</strong><br/>
        <span class="pull-left"><?php echo $data['Campanha']['max_parcelas']; ?></span>
</span>
<br/>
<span class="span4" style="padding-top: 10px;">
	<strong class="pull-right">Status</strong><br/>
	<span class="pull-right"><?php echo $data['Campanha']['status']; ?></span>
</span>
