<script type="text/javascript">
	$(function() {
		$(".quantidade-comprada").change(function() {
			quantidadeEscolhida = this.value;
			idDaRelacaoCampanhaExtra = this.id.replace("campanhaExtra", "");
			valorUnitario = $("#valor-unitario-"+idDaRelacaoCampanhaExtra).html().replace(",",".");
			valorTotal = (parseFloat(valorUnitario)*parseFloat(quantidadeEscolhida)).toFixed(2);
			$("#valor-compra-"+idDaRelacaoCampanhaExtra).html((valorTotal + "").replace(".",","));
			valorTotalDeTodasAsCompras = 0.0;
			
			$(".valor-compra").each(function() {
				valorTotalDaCompra = parseFloat($(this).html().replace(",","."));
				valorTotalDeTodasAsCompras += valorTotalDaCompra;
			});
			
			$("#valor-total").html((valorTotalDeTodasAsCompras.toFixed(2) + "").replace(".",","));
			
			numeroDeParcelas = 1.0;
			
			$("#numero-de-parcelas").each(function() { 
				numeroDeParcelas = parseFloat(this.value);
			});
			
			valorParcelado =  (valorTotalDeTodasAsCompras/numeroDeParcelas).toFixed(2);
			$("#valor-parcelado").html((valorParcelado + "").replace(".",","));
			return false;
		});

		$("#numero-de-parcelas").change(function() {
			valorTotalDeTodasAsCompras = parseFloat($("#valor-total").html().replace(",","."));			
			numeroDeParcelas = this.value;		
			valorParcelado =  (valorTotalDeTodasAsCompras/numeroDeParcelas).toFixed(2);
			$("#valor-parcelado").html((valorParcelado + "").replace(".",","));
			return false;
		});
	});
</script>

<span id="conteudo-titulo" class="box-com-titulo-header">Campanha Final - Checkout</span>
<div id="conteudo-container">
	<?php $session->flash();?>
	<div class="detalhes">
		<?php include('_visualizar.ctp');?>
		<?php echo $form->create(false, array('url' => "/{$this->params['prefix']}/campanhas/checkout_comprar/{$idDoUsuario}/{$idDoFormandoProfile}", "class" => "form-adesao-campanha")); ?>
		<input type="hidden" name="data[Campanha][id]" value="<?php echo $data['Campanha']['id']?>" id="CampanhaId"></input>
		
		<p class="grid_11 alpha omega titulo first">Extras dispon&iacute;veis
		</p>
        <div class="conteudo-container first">
			<table>
					<thead>
							<tr>
								<th scope="col">Evento</th>
								<th scope="col">Nome</th>
								<th scope="col">Valor Unit&aacute;rio</th>
								<th scope="col">Qtde. para compra.</th>
								<th scope="col">Valor da Compra</th>
							</tr>
					</thead>
					<tbody>
					<?php $isOdd = false; ?>
					<?php 
						$valorTotal = 0;
						foreach($data['CampanhasExtra'] as $extra): ?>
							<?php if($isOdd):?>
									<tr class="odd">
							<?php else:?>
									<tr>
							<?php endif;?>
									<td colspan="1"><?php echo $extra['Extra']['Evento']['nome'];?></td>
									<td colspan="1"><?php echo $extra['Extra']['nome'];?></td>		
									<td colspan="1">R$ <span id="valor-unitario-<?php echo $extra['id'];?>"> <?php echo number_format($extra['valor'], 2, ',', ' ');?></span></td>
									<td colspan="1">
										<?php 
										if ($data['Campanha']['status'] == 'Aberta')
											echo $form->select('campanhaExtra'.$extra['id'], $optionsDeCompraDeExtras[$extra['id']], '0', array('id' => 'campanhaExtra'.$extra['id'], 'empty'=>false, 'class' => 'quantidade-comprada','label' => false, 'div' => false));
										else
											echo '0';
											
										?>
									</td>
									<td colspan="1">R$ <span class="valor-compra" id="valor-compra-<?php echo $extra['id'];?>">0,00</span></td>
									</tr>
							<?php $isOdd = !($isOdd); ?>
					<?php 
							$valorTotal += $extra['valor'];
						endforeach; ?>
					</tbody>
					<?php if ($data['Campanha']['status'] == 'Aberta') { ?>
						<tfoot>
							<tr>
								<td colspan="1">
								</td>
								<td colspan="1" style="text-align:right;">Valor parcelado:</td>
								<td colspan="1" style="text-align:center;">
									<?php 
										
										$optionsDeParcelas;
										for ($i = 1; $i <= $data['Campanha']['max_parcelas']; $i++)
											$optionsDeParcelas[$i] = $i;
											
										
										echo $form->select('numero-de-parcelas', $optionsDeParcelas, '1', array('empty'=>false,'label' => false, 'div' => false)).' X ';
										
									?> 
									R$ <span id="valor-parcelado">0,00</span>
								</td>
								<td colspan="1" style="text-align:right;">Valor total:</td>
								<td colspan="1" style="text-align:center;">R$ <span id="valor-total">0,00</span></td>
							</tr>
							<tr>
								<td colspan="5"><p class="grid_16"><input type="submit" class="submit" value="Comprar"><p></td>
							</tr>
						</tfoot>
					<?php } ?>
			</table>
		</div>
		</form>
		<?php 
			$numeroDeAdesoes = 1;
			
			foreach($adesoesDoUsuario as $adesaoDoUsuario) {
			
		?>
			<p class="grid_11 alpha omega titulo first" style="margin: 30px 0 0 0;" >Extras comprados - Ades&atilde;o <?php echo $numeroDeAdesoes++;?></p>
			
			<div class="conteudo-container first">
				<table>
						<thead>
								<tr>
									<th scope="col">Evento</th>
									<th scope="col">Nome</th>
									<th scope="col">Qtde. Comprada.</th>
									<th scope="col">Valor Unit&aacute;rio</th>
									<th scope="col">Valor Pago</th>
								</tr>
						</thead>
						<tbody>
						<?php $isOdd = false; ?>
						<?php 
							$valorTotal = 0;
							foreach($adesaoDoUsuario['CampanhasUsuariosCampanhasExtras'] as $extraDaAdesao): 
						?>
								<?php if($isOdd):?>
										<tr class="odd">
								<?php else:?>
										<tr>
								<?php endif;?>
										<td colspan="1"><?php 
										echo $campanhasExtras[$extraDaAdesao['campanhas_extra_id']]['Extra']['Evento']['nome'];?></td>
										<td colspan="1"><?php echo $campanhasExtras[$extraDaAdesao['campanhas_extra_id']]['Extra']['nome'];?></td>
										<td colspan="1"><?php echo $extraDaAdesao['quantidade'];?></td>
										<td colspan="1">R$ <?php echo number_format($campanhasExtras[$extraDaAdesao['campanhas_extra_id']]['valor'], 2, ',', ' ');?></td>
										<td colspan="1">R$ <?php echo number_format($extraDaAdesao['quantidade']*$campanhasExtras[$extraDaAdesao['campanhas_extra_id']]['valor'], 2, ',', ' ');?></td>
								</tr>
								<?php $isOdd = !($isOdd); ?>
						<?php 
							$valorTotal += $extraDaAdesao['quantidade']*$campanhasExtras[$extraDaAdesao['campanhas_extra_id']]['valor'];
							endforeach; 
						?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="3"></td>
								<td colspan="1" style="text-align:right;">Valor total:</td>
								<td colspan="1" style="text-align:center;">R$ <?php echo number_format($valorTotal, 2, ',', ' ');;?></td>
							</tr>
						</tfoot>
				</table>
			</div>
		<?php 
			}
		?>
        <p class="grid_16 alpha omega">
			<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'controller' => 'formandos', 'action' => 'checkout', $idDoFormandoProfile) ,array('class' => 'cancel')); ?>
		</p>
	</div>
</div>