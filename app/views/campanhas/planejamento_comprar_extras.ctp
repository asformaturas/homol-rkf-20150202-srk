<style type="text/css">
.remover-extra { margin-left:10px; }
.remover-extra:hover { color:black!important; }
</style>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/jquery.meio.mask.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.selectpicker').selectpicker();
        $('#data_a_vista').setMask();
        $("#dados-financeiros").click(function(e) {
            e.preventDefault();
            $(".modal-body").html('<h2>Carregando</h2>');
            $(".modal-body").load($(this).attr('href'));
            //$(".modal-body").load('<?=$this->here?>');
        });
        var valorTotal = 0;
        $("#extra").change(function(){
            var itemSelecionado = $('#extra option').filter(':selected').text();
            var itens = ["", "Convite Infantil", "Convite Luxo", "Convite Extra", "Combo" ,"Mesa Extra", "Brinde", "Outros"];
            if($.inArray(itemSelecionado, itens)){
                if(itemSelecionado.substr(0, 13) == "Convite Extra"){
                    $('#extraQuantidadeConvites').attr('disabled','disabled');
                    $('#extraQuantidadeMesas').attr('disabled','disabled');
                    $('#extraQuantidadeMesas').val(0);
                    $('#extraQuantidadeConvites').val(1);
                }
                if(itemSelecionado.substr(0, 16) == "Convite Infantil"){
                    $('#extraQuantidadeConvites').attr('disabled','disabled');
                    $('#extraQuantidadeMesas').attr('disabled','disabled');
                    $('#extraQuantidadeMesas').val(0);
                    $('#extraQuantidadeConvites').val(0);
                }
                if(itemSelecionado.substr(0, 12) == "Convite Luxo"){
                    $('#extraQuantidadeConvites').attr('disabled','disabled');
                    $('#extraQuantidadeMesas').attr('disabled','disabled');
                    $('#extraQuantidadeMesas').val(0);
                    $('#extraQuantidadeConvites').val(0);
                }
                if(itemSelecionado.substr(0, 6) == "Brinde"){
                    $('#extraQuantidadeConvites').attr('disabled','disabled');
                    $('#extraQuantidadeMesas').attr('disabled','disabled');
                    $('#extraQuantidadeMesas').val(0);
                    $('#extraQuantidadeConvites').val(0);
                }
                if(itemSelecionado.substr(0, 6) == "Outros"){
                    $('#extraQuantidadeConvites').attr('disabled','disabled');
                    $('#extraQuantidadeMesas').attr('disabled','disabled');
                    $('#extraQuantidadeMesas').val(0);
                    $('#extraQuantidadeConvites').val(0);
                }
                if(itemSelecionado.substr(0, 10) == "Mesa Extra"){
                    $('#extraQuantidadeConvites').attr('disabled','disabled');
                    $('#extraQuantidadeMesas').attr('disabled','disabled');
                    $('#extraQuantidadeMesas').val(1);
                    $('#extraQuantidadeConvites').val(0);
                }
                if(itemSelecionado.substr(0, 5) == "Combo"){
                    $('#extraQuantidadeConvites').attr('disabled','disabled');
                    $('#extraQuantidadeMesas').attr('disabled','disabled');
                    $('#extraQuantidadeMesas').val(1);
                    $('#extraQuantidadeConvites').val(10);
                }
            }
        });
        $("#extra").change(function() {
            if($(this).val() != '') {
                $("[data-extra='detalhes']").fadeIn(500);
            } else {
                $("[data-extra='detalhes']").fadeOut(500);
            }
        });
        $("#extra-confirmar").click(function() {
            var completar = false;
            var dados = {};
            $("[data-extra='inserir']").find("input:visible").each(function() {
                if($(this).val() == "") {
                    completar = true;
                    $(this).focus();
                    return false;
                } else
                    dados[$(this).data('tipo')] = $(this).val();
            });
            if(completar) {
                alert('complete todos os campos');
            } else if(parseInt(dados.quantidade) < 1) {
                alert('A quantidade deve ser maior que zero');
                $("[data-tipo='quantidade']").focus();
            } else {
                var extra = $("#extra option[value='"+$("#extra").val()+"']");
                dados.extra_id = $("#extra").val();
                var span = $("<span/>",{
                    text : dados.quantidade + " " +
                            extra.text() + " - Valor Unitário: R$" + dados.valor,
                    class : 'label label-info'
                });
                span.append("<b class='remover-extra pointer fg-color-red'>x</b>");
                var div = $("<div/>",{
                    class : 'row-fluid extra-comprado'
                }).append(span);
                div.data(dados);
                valorTotal+= stringMoneyToNumber(dados.valor)*dados.quantidade;
                $("[data-extra='detalhes']").fadeOut(500,function() {
                    $("#extra").val('');
                    $('#extra').selectpicker('refresh');
                    $("input[data-tipo]").val('');
                    $("#extras-comprados").append(div);
                    if(valorTotal > 0) {
                        $("[data-parcelamento='inserir']").fadeIn(500);
                        verificarFormaDePagamento();
                    } else {
                        $("[data-parcelamento],[data-parcelas]").fadeOut(500);
                        $("#compra-enviar").fadeIn(500);
                    }
                });
            }
        });
        $("#extras-comprados").on('click','.remover-extra',function() {
            $(this).parent().parent().fadeOut(500,function() {
                valorTotal-= stringToFloat($(this).data('valor'),2)*parseInt($(this).data('quantidade'));
                $(this).remove();
                if($("#extras-comprados > .extra-comprado").length < 1) {
                    $("#compra-enviar,[data-parcelamento],[data-parcelas]").fadeOut(500);
                } else if(valorTotal == 0) {
                    $("[data-parcelamento],[data-parcelas]").fadeOut(500);
                    $("#compra-enviar").fadeIn(500);
                } else {
                    verificarFormaDePagamento();
                }
            });
        });
        $("#forma-pagamento").change(function() {
            verificarFormaDePagamento();
        });
        $("#criar-parcelas").click(function() {
            var parcelas = parseInt($("#parcelas").val());
            if(isNaN(parcelas) || $("#parcelas").val() == "")
                parcelas = 0;
            if(parcelas > 1) {
                var valorParcela = parseFloat(parseFloat(valorTotal/parcelas).toFixed(2));
                var valorParcelas = 0;
                $('[data-parcelas="inserir"]').html('');
                for(var a = 1; a <= parcelas; a++) {
                    if(a == parcelas)
                        valorParcela = valorTotal - valorParcelas;
                    valorParcelas+= valorParcela;
                    var div = $('[data-parcelas="modelo"]').clone();
                    div.attr('data-parcelas','nova');
                    div.data('parcelas','nova');
                    div.find('.valor').val(parseFloat((valorParcela+"")).toFixed(2).replace(".",','));
                    div.find('.forma-pagamento').val('dinheiro').selectpicker();
                    div.find('.data-vencimento').setMask();
                    $('[data-parcelas="inserir"]').append(div);
                    div.fadeIn(500);
                }
                $('[data-parcelas="inserir"]').fadeIn(500);
            } else {
                alert('O número de parcelas deve ser maior que 1');
            }
        });
        $('[data-parcelas="inserir"]').on('change','.forma-pagamento',function() {
            if($(this).val() == "dinheiro") {
                $(this).parents('[data-parcelas="nova"]').find('.data-vencimento').fadeOut(500);
            } else {
                $(this).parents('[data-parcelas="nova"]')
                        .find('.data-vencimento').fadeIn(500,function() {
                        $(this).focus();
                });
            }
        });
        $("#compra-enviar").click(function() {
            var despesas = [];
            var formaPagamento = $("#forma-pagamento").val();
            if(valorTotal > 0) {
                if(formaPagamento == "") {
                    alert('Selecione uma forma de pagamento');
                    return false;
                } else if(formaPagamento == 'cheque' || formaPagamento == 'boleto') {
                    if($("#data_a_vista").val() == "") {
                        alert('Digite a data do ' + formaPagamento);
                        $("#data_a_vista").focus();
                        return false;
                    } else {
                        despesas.push({
                            forma_pagamento : formaPagamento,
                            valor : valorTotal,
                            data_vencimento : $("#data_a_vista").val()
                        });
                    }
                } else if(formaPagamento == 'parcelar') {
                    if($('[data-parcelas="nova"]').length > 1) {
                        var somaParcelas = 0.0;
                        var erro = false;
                        $('[data-parcelas="nova"]').each(function() {
                            var valor = stringToFloat(stringMoneyToNumber($(this).find('.valor').val()),2);
                            if(isNaN(valor))
                                valor = 0;
                            if(valor > 0) {
                                var forma = $(this).find('.forma-pagamento').val();
                                var data = $(this).find('.data-vencimento').val();
                                if(forma != "dinheiro" && data == "") {
                                    alert('Digite a data do ' + forma);
                                    $(this).find('.data-vencimento').focus();
                                    erro = true;
                                    return false;
                                } else {
                                    somaParcelas+= valor;
                                    despesas.push({
                                        forma_pagamento : forma,
                                        valor : valor,
                                        data_vencimento : data
                                    });
                                }
                            } else {
                                alert("Digite um valor válido para a parcela");
                                $(this).find('.valor').focus();
                                return false;
                            }
                        });
                        if(!erro) {
                            var diferenca = stringToFloat(Math.abs(somaParcelas-valorTotal),2)+"";
                            if(somaParcelas.toFixed(2) == valorTotal.toFixed(2)) {
                                
                            } else if(somaParcelas.toFixed(2) > valorTotal.toFixed(2)) {
                                alert("A soma das parcelas excede em " + diferenca.replace(".",',') +
                                        " reais o valor total");
                                return false;
                            } else if(somaParcelas.toFixed(2) < valorTotal.toFixed(2)) {
                                alert("O valor total excede em " + diferenca.replace(".",',') +
                                        " reais a soma das parcelas");
                                return false;
                            }
                        } else {
                            return false;
                        }
                    } else {
                        alert("Crie as parcelas antes de enviar a compra");
                        $('#parcelas').focus();
                        return false;
                    }
                } else {
                    despesas.push({
                        forma_pagamento : formaPagamento,
                        valor : valorTotal
                    });
                }
            } else {
                despesas.push({
                    forma_pagamento : 'dinheiro',
                    valor : 0
                });
            }
            var compras = [];
            $('.extra-comprado').each(function() {
                var data = $(this).data();
                data.valor = stringToFloat(stringMoneyToNumber(data.valor+""),2);
                compras.push(data);
            });
            var dados = {
                forma_pagamento : formaPagamento,
                despesas : despesas,
                compras : compras
            };
            $(".modal-body").html('<h2>Enviando</h2>');
            $.ajax({
                type : 'POST',
                dataType : 'json',
                url : '<?=$this->here?>',
                data : {
                    data : dados
                },
                error : function() {
                    $(".modal-body").html('<h2 class="fg-color-red">Erro ao enviar os dados</h2>');
                },
                complete : function() {
                    $(".modal-body").load('<?=$this->here?>');
                }
            });
        });
        
        function verificarFormaDePagamento() {
            var formaDePagamento = $("#forma-pagamento");
            if(formaDePagamento.val() == "") {
                $("[data-parcelas],[data-parcelamento='data_a_vista'],#compra-enviar").fadeOut(500);
            } else {
                $("#compra-enviar").fadeIn(500);
                if(formaDePagamento.val() == 'dinheiro') {
                    $("[data-parcelamento='parcelas'],[data-parcelas],[data-parcelamento='data_a_vista']").fadeOut(500);
                } else if(formaDePagamento.val() == 'parcelar') {
                    if($("[data-parcelamento='data_a_vista']").is(':visible'))
                        $("[data-parcelamento='data_a_vista']").fadeOut(500,function() {
                            $("[data-parcelamento='parcelas']").fadeIn(500,function() {
                                $("#parcelas").focus();
                            });
                        });
                    else
                        $("[data-parcelamento='parcelas']").fadeIn(500,function() {
                            $("#parcelas").focus();
                        });
                } else {
                    $("[data-parcelas]").fadeOut(500);
                    if($("[data-parcelamento='parcelas']").is(':visible'))
                        $("[data-parcelamento='parcelas']").fadeOut(500,function() {
                            $("[data-parcelamento='data_a_vista']").fadeIn(500,function() {
                                $("#data_a_vista").focus();
                            });
                        });
                    else
                        $("[data-parcelamento='data_a_vista']").fadeIn(500,function() {
                            $("#data_a_vista").focus();
                        });
                }
            }
        }
    });
</script>
<?php $session->flash(); ?>
<div class="row-fluid">
    <h4 class="fg-color-red"><b>ATENÇÃO:</b> Utilizar somente vírgula no campo valor unitário!</h4>
</div>
<br />
<div class="row-fluid" data-extra="inserir">
    <?php
        $beneficios = array();
        foreach($extras as $i => $extra){
            foreach($extra as $ex => $e)
                if(strpos($e, 'Comiss') !== false ||
                   strpos($e, 'Benef') !== false)
                    $beneficios[$ex] = $e;
        }
    ?>
    <div class="span3">
        <?=$form->input(false,array(
            'options' => $beneficios,
            'type' => 'select',
            'empty' => 'Selecione',
            'id' => 'extra',
            'class' => 'selectpicker',
            'data-width' => '100%',
            'data-container' => 'body',
            'label' => 'Extra',
            'div' => 'input-control text')); ?>
    </div>
    <div class="span2 hide" data-extra="detalhes">
        <?=$form->input(false,array(
            'label' => 'Qtd',
            'div' => 'input-control text',
            'data-tipo' => 'quantidade',
            'error' => false,
            'type' => 'text'
        )); ?>
    </div>
    <div class="span2 hide" data-extra="detalhes">
        <?=$form->input(false,array(
            'label' => 'Qtd Convites',
            'div' => 'input-control text',
            'data-tipo' => 'quantidade-convites',
            'id' => 'extraQuantidadeConvites',
            'error' => false,
            'type' => 'text'
        )); ?>
    </div>
    <div class="span2 hide" data-extra="detalhes">
        <?=$form->input(false,array(
            'label' => 'Qtd Mesas',
            'div' => 'input-control text',
            'data-tipo' => 'quantidade-mesas',
            'id' => 'extraQuantidadeMesas',
            'error' => false,
            'type' => 'text'
        )); ?>
    </div>
    <div class="span2 hide" data-extra="detalhes">
        <?=$form->input(false,array(
            'label' => 'Valor Unitário',
            'div' => 'input-control text',
            'data-tipo' => 'valor',
            'error' => false,
            'type' => 'text'
        )); ?>
    </div>
    <div class="span2 hide" data-extra="detalhes">
        <label>&nbsp;</label>
        <button type="button" class="default"
            id="extra-confirmar">
            <i class="icon-thumbs-up"></i>
        </button>
    </div>
</div>
<div class="row-fluid" id="extras-comprados"></div>
<br />
<div class="row-fluid hide" data-parcelamento="inserir">
    <div class="span6">
        <?=$form->input(false,array(
            'options' => $formaParcelamento,
            'type' => 'select',
            'empty' => 'Selecione a forma de pagamento',
            'id' => 'forma-pagamento',
            'class' => 'selectpicker',
            'data-width' => '100%',
            'data-container' => 'body',
            'label' => 'Forma de pagamento',
            'div' => 'input-control text')); ?>
    </div>
    <div class="span3 hide" data-parcelamento="data_a_vista">
        <?=$form->input(false,array(
            'label' => 'Data',
            'div' => 'input-control text',
            'id' => 'data_a_vista',
            'alt' => '99/99/9999',
            'error' => false,
            'type' => 'text'
        )); ?>
    </div>
    <div class="span2 hide" data-parcelamento="parcelas">
        <?=$form->input(false,array(
            'label' => 'Parcelas',
            'id' => 'parcelas',
            'div' => 'input-control text',
            'error' => false,
            'type' => 'text'
        )); ?>
    </div>
    <div class="span2 hide" data-parcelamento="parcelas">
        <label>&nbsp;</label>
        <button type="button" id="criar-parcelas"
            class="bg-color-purple input-block-level">
            Parcelar
        </button>
    </div>
</div>
<div class="row-fluid hide" data-parcelas="inserir">
    
</div>
<div class="row-fluid hide" data-parcelas="modelo">
    <div class="span2">
        <?=$form->input(false,array(
            'label' => false,
            'div' => 'input-control text',
            'placeholder' => 'Valor',
            'class' => 'valor',
            'error' => false,
            'type' => 'text'
        )); ?>
    </div>
    <div class="span4">
        <?=$form->input(false,array(
            'options' => $formaPagamento,
            'type' => 'select',
            'data-width' => '100%',
            'data-container' => 'body',
            'class' => 'forma-pagamento',
            'label' => false,
            'div' => 'input-control text')); ?>
    </div>
    <div class="span4">
        <?=$form->input(false,array(
            'label' => false,
            'placeholder' => 'Data',
            'div' => 'input-control text',
            'alt' => '99/99/9999',
            'class' => 'data-vencimento hide',
            'type' => 'text'
        )); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span3">
        <button type="button" id="compra-enviar"
            class="bg-color-blueDark input-block-level hide">
            Enviar Compra
        </button>
    </siv>
</div>