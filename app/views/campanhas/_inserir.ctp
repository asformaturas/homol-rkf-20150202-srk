<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/chosen.css">
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/bootstrap/datepicker.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/datepicker.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/chosen.js"></script>
<style type="text/css">
    .chzn-single{
        width: 412px!important;
        height: 32px!important
    }
    .chzn-drop{
        width: 412px!important;
    }
    .remover-inline{
        text-decoration: none!important;
        color: orange
    }
    .remover-inline :hover{
        color: red
    }
    #adicionar-extra-inline{
        list-style-type: none;
        margin: 0px
    }
    .datepicker{
        top: -217px!important;
    }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        var context = ko.contextFor($("#content-body")[0]);
        
        $("#datepicker_inicial_aux").datepicker();
        $("#datepicker_final_aux").datepicker();      
        
        $(".chosen").chosen({width:'100%'});
        
        $('.remover').on('click','.remover-inline', function() {
            removerInline($(this));
        });

        $('#adicionar-inline-extra').on('click',function() {
            $('.adicionar-inline-form').fadeIn(500);
            $('#adicionar-inline-extra').html('Salvar Item').attr('id', 'salvar-inline-extra');
            $('#select-extra').fadeIn();
            context.$data.loaded(true);
        });
        
        $('#cancelar-extra').on('click',function(){
            $('.adicionar-inline-form').fadeOut(500);
            $('#cancelar-extra').hide();
            $('#salvar-inline-extra').html('Adicionar Item').attr('id', 'adicionar-inline-extra');
        });

        $('.extras').on('click','#salvar-inline-extra', function() {
            $('#cancelar-extra').fadeIn();
            if ($('#adicionar-extra-select').val() != null && $('#adicionar-extra-select').val() != "") {
                $('#salvar-inline-extra').html('Adicionar Item').attr('id', 'adicionar-inline-extra');
                $('#adicionar-extra-select').attr('disabled', true);
                $('#select-extra').fadeOut();
                
                var extra_id = $('#adicionar-extra-select').val();
                $("#list option[value='2']").text()
                var extra_nome = $('#adicionar-extra-select option[value="' + extra_id + '"]').text();
                var evento_id = $('#CampanhaEvento').val();
                var evento_nome = $('#CampanhaEvento option[value="'+ evento_id +'"]').text();
                var quantidade_minima = $('#extraQuantidadeMinima').val();
                var quantidade_maxima = $('#extraQuantidadeMaxima').val();
                var quantidade_disponivel = $('#extraQuantidadeDisponivel').val();
                var valor = $('#extraValor').val();
                var campanha_extra_id = null;
                
                oldValue = $('#adicionar_extras_array').val();
                if (oldValue == '') {
                    $('#adicionar_extras_array').val(
                            '{extra_id:' + extra_id +
                            ', evento_id:' +
                            ', quantidade_minima:' + quantidade_minima +
                            ', quantidade_maxima:' + quantidade_maxima +
                            ', quantidade_disponivel:' + quantidade_disponivel +
                            ', valor:' + valor +
                            ', campanha_extra_id: ' + campanha_extra_id +
                            '}');
                } else {
                    $('#adicionar_extras_array').val(oldValue +
                            '{extra_id:' + extra_id +
                            ', evento_id:' +
                            ', quantidade_minima:' + quantidade_minima +
                            ', quantidade_maxima:' + quantidade_maxima +
                            ', quantidade_disponivel:' + quantidade_disponivel +
                            ', valor:' + valor +
                            ', campanha_extra_id:' + campanha_extra_id +
                            '}');
                }
                
                $('ul#adicionar-extra-inline').append('<li><a class="remover-inline"></a>' + "\n" +
                evento_nome + ' - ' + extra_nome + ' - ' +
                ' Mínimo: ' + quantidade_minima + '- Máximo: ' + quantidade_maxima +
                '- Disponível: ' + quantidade_disponivel + '- Valor: R$' + valor +
                '<input name=data[Campanha][extra][] type="hidden" value="(' + extra_id + ')"' +
                '</input>' + '<a class="remover-inline"><i class="icon-remove"></i></a>' + '</li>');
                
            }
            $('#adicionar-extra-select').html('');
            $('#adicionar-extra-select').removeAttr('disabled').trigger("liszt:updated");
            $('#CampanhaEvento').val('Selecione o Evento').trigger("liszt:updated");
            $('#nenhum').remove();
        });
        $('#CampanhaEvento').change(function() {
            $('#adicionar-extra-select').html('');
            if ($('#CampanhaEvento').val() != null && $('#CampanhaEvento').val() != ""){
                $.get("<?php echo $html->url(array($this->params['prefix'] => true, 'controller' => 'campanhas', 'action' => 'extra')); ?>/" + $('#CampanhaEvento').val(), function(data) {
                    jQuery.each(jQuery.parseJSON(data), function() {
                        $('#adicionar-extra-select').append("<option value=" + this.Extra.id + ">" + this.Extra.nome + "</option>");
                    });
                    if(jQuery.parseJSON(data).length == 0)
                        $('#adicionar-extra-select').append("<option value='0' id='nenhum'>Nenhum Extra Pra Esse Evento</option>");
                    $('#adicionar-extra-select').removeAttr('disabled').trigger("liszt:updated");
                });
            }
        });

        function removerInline(objeto) {
            if (confirm("Remover o item extra da campanha?")) {
                var campanha_extra_id = $(objeto).parent().find('span.campanha_extra_id').html();

                if ($('#remover_extras_array').val() == '')
                    $('#remover_extras_array').val('{campanha_extra_id:' + campanha_extra_id + '}');
                else {
                    var oldValue = $('#remover_extras_array').val();
                    $('#remover_extras_array').val(oldValue + ',{campanha_extra_id:' + campanha_extra_id + '}');
                }
                $(objeto).parent().remove();
            }

        }
        
        $("#form").submit(function(e) {
            e.preventDefault();
            var context = ko.contextFor($(".metro-button.back")[0]);
            var dados = $("#form").serialize();
            var url = '<?="/{$this->params['prefix']}/campanhas/inserir/"?>';
            context.$data.showLoading(function() {
                $.ajax({
                    url: url,
                    data: dados,
                    type: "POST",
                    dataType: "json",
                    complete: function() {
                        context.$data.page('/<?=$this->params['prefix']?>/campanhas/listar');
                    }
                });
            });
        });
    });
</script>
<div class="row-fluid">
    <h2>
        <a class="metro-button back" data-bind="click: function() { 
            page('<?="/{$this->params['prefix']}/campanhas/listar"?>') }">
        </a>
        Inserir Campanha
    </h2>
</div>
<div class="row-fluid">
        <?php $session->flash(); ?>
        <?php echo $form->create('Campanha', array('url' => "/{$this->params['prefix']}/campanhas/alterar/{$this->data['Campanha']['id']}", 'id' => 'form')); ?>

        <?php echo $form->input('Campanha.id', array('hiddenField' => true)); ?>
    <div class="row-fluid">
        <div class="span6">
            <label>Nome</label>
            <?php echo $form->input('nome', array('label' => false, 'div' => 'input-control text', 'error' => false)); ?>
        </div>
        <div class="span6 pull-right">
            <label>Tipo</label>
            <?=$form->input('tipo',array(
                'label' => false,
                'type' => 'select',
                'class' => 'chosen',
                'options' => array('comum' => 'Comum'),
                'div' => 'input-control text',
                'error' => false)); ?>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <label>Descrição</label>
            <?php echo $form->input('descricao', array('label' => false, 'div' => 'input-control text', 'error' => false)); ?>
        </div>
    </div>
    
    <div class="row-fluid">
        <div class="span4">
            <label>Máximo de parcelas</label>
            <?php echo $form->input('max_parcelas', array('label' => false, 'div' => 'input-control text', 'error' => false)); ?>
        </div>

        <div class="span4">
            <label>Data Início</label>
            <?php echo $form->input('data_inicio_aux', array('label' => false, 'div' => 'input-control text', 'error' => false, 'id' => 'datepicker_inicial_aux')); ?>
        </div>

        <div class="span4 pull-right">
            <label>Data Fim</label>
            <?php echo $form->input('data_fim_aux', array('label' => false, 'div' => 'input-control text', 'error' => false, 'id' => 'datepicker_final_aux'));?>
        </div>
    </div>

        <?php echo $form->input('adicionar_extras_array', array('style' => 'display:none;', 'label' => false, 'div' => false, 'id' => 'adicionar_extras_array')); ?>
        <?php echo $form->input('remover_extras_array', array('style' => 'display:none;', 'label' => false, 'div' => false, 'id' => 'remover_extras_array')); ?>

    <div class="row-fluid">
        <div class="span12 remover">
            <label>Itens da campanha</label>
            <ul id="adicionar-extra-inline">
                <?php
                if (@$this->data['CampanhasExtra'] != '') {
                    foreach ($this->data['CampanhasExtra'] as $extra):
                        ?>
                        <li>
                            <?php
                            echo $extra['Extra']['Evento']['nome'] . ' - ' . $extra['Extra']['nome'] .
                            ' - Mínimo: ' . $extra['quantidade_minima'] . ' - Máximo: ' . $extra['quantidade_maxima'] .
                            ' - Disponível: ' . $extra['quantidade_disponivel'] . ' - Valor: R$' . $extra['valor']
                            ?>
                            <span class="campanha_extra_id" style="display:none"><?php echo $extra['id']; ?></span>
                            <input name="data[Campanha][extra][]" type="hidden" value="<?php echo $extra['Extra']['id']; ?>"/>
                            <a class="remover-inline"><i class="icon-remove"></i></a>
                        </li>
                    <?php endforeach;
                }
                ?>
            </ul>
        </div>
    </div>
    <br/>
    <div class="row-fluid extras">
        <div class="span12 adicionar-inline-form" style="display: none">
            <div id="select-extra">
                <div class="row-fluid">
                    <div class="span6">
                        <?=$form->input('evento',array(
                            'label' => 'Selecione o Evento',
                            'type' => 'select',
                            'class' => 'chosen',
                            'options' => $eventos,
                            'empty' => 'Selecione o Evento',
                            'div' => 'input-control',
                            'error' => false,
                            'style' => 'width: 412px')); ?>
                    </div>
                    <div class="span6">
                        <label>Selecione o Extra</label>
                            <div class="input-control">
                                <select type="select" id="adicionar-extra-select" class="chosen" data-placeholder="Selecione o Evento"></select>
                            </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <!-- Quantidade Mínima -->
                    <div class="span6">
                        <label>Quantidade Mínima</label>
                        <?php echo $form->input('extra.quantidade_minima', array('label' => false, 'div' => 'input-control text', 'error' => false)); ?>
                    </div>

                    <!-- Quantidade Máxima -->
                    <div class="span6 pull-right">
                        <label>Quantidade Máxima</label>
                        <?php echo $form->input('extra.quantidade_maxima', array('label' => false, 'div' => 'input-control text', 'error' => false)); ?>
                    </div>
                </div>

                <div class="row-fluid">
                    <div class="span6">
                        <label>Quantidade Dispon&iacute;vel</label>
                        <?php echo $form->input('extra.quantidade_disponivel', array('label' => false, 'div' => 'input-control text', 'error' => false)); ?>
                    </div>

                    <!-- Valor -->
                    <div class="span6 pull-right">
                        <label>Valor (R$)</label>
                        <?php echo $form->input('extra.valor', array('label' => false, 'div' => 'input-control text', 'error' => false)); ?>
                    </div>
                </div>
                <a class="button bg-color-red" id="cancelar-extra" style="display:none">Cancelar</a>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <a class="button bg-color-blueDark adicionar-inline" id="adicionar-inline-extra">Adicionar Item</a>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <?php echo $form->end(array('label' => 'Salvar', 'div' => false, 
                       'class' => 'button bg-color-blueDark pull-right'));?>
        </div>
    </div>
</div>