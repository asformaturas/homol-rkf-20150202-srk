<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/tooltip.js"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/jquery.meio.mask.js"></script>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<script type="text/javascript">
    $(document).ready(function() {
        $.Input();
        $('input[alt]').setMask();
        $("#form-filtro").submit(function(e){
            e.preventDefault();
            var context = ko.contextFor($(".metro-button.reload")[0]);
            context.$data.showLoading(function() {
                $("#form-filtro").find('[value="false"]').attr('disabled','disabled');
                var dados = $("#form-filtro").serialize();
                var url = $("#form-filtro").attr('action');
                $.ajax({
                    url : url,
                    data : dados,
                    type : "POST",
                    dataType : "json",
                    complete : function() {
                        context.$data.reload();
                    }
                });
            });
        });
        $('.alterar').click(function() {
            bootbox.dialog('Carregando',[{
                    label: 'Fechar'
                }],{
                    remote: '<?="/{$this->params['prefix']}/igpm/alterar/"?>'+
                            $(this).attr('dir')
            });
        });
        $('.inserir').click(function() {
            bootbox.dialog('Carregando',[{
                    label: 'Fechar'
                }],{
                    remote: '<?="/{$this->params['prefix']}/igpm/inserir/"?>'
            });
        });
    });
</script>
<?php $session->flash(); ?>
<div class="row-fluid">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Lista de IGPMs
        </h2>
</div>
<?php 
$paginator->options(array(
    'url' => array($this->params['prefix'] => true)));
$sortOptions = array('data-bind' => 'click: loadThis');
?>
<div class="row-fluid">
    <?=$form->create('Igpm',array(
        'url' => "/{$this->params['prefix']}/igpm/listar/",
        'id' => 'form-filtro')) ?>
    <div class="row-fluid">
        <div class="span3">
            <?=$form->input('ano',
                array('label' => 'Ano', 'div' => 'input-control mini text',
                    'error' => false,'type' => 'text')); ?>
        </div>
        <div class="span3">
            <label>&nbsp;</label>
            <button type='submit' class='mini max bg-color-red'>
                Filtrar
                <i class='icon-search-2'></i>
            </button>
        </div>
        <div class="span6">
            <label>&nbsp;</label>
            <button type="button" class="button mini pull-right bg-color-greenDark inserir">
                Inserir IGPM
                <i class="icon-upload"></i>
            </button>
        </div>
    </div>
<?php if (sizeof($igpms > 0)) : ?>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="40%"><?=$paginator->sort('Mês/Ano', 'Igpm.ano',$sortOptions); ?></th>
                <th scope="col" width="40%"><?=$paginator->sort('Valor', 'Igpm.valor',$sortOptions); ?></th>
                <th scope="col" width="20%"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($igpms as $igpm) : ?>
            <tr>
                <td width="25%"><?php echo $igpm['Igpm']['mes'] . '/' . $igpm['Igpm']['ano']; ?></td>
                <td width="25%"><?=$igpm['Igpm']['valor']; ?></td>
                <td width="25%" style="text-align:center">
                    <button type="button" class="default mini bg-color-blueDark alterar" dir="<?=$igpm['Igpm']['id']?>">
                            Alterar
                    </button>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="2" class="paginacao">
                    <?=$paginator->numbers(array('separator' => ' ','data-bind' => 'click: loadThis')); ?>
                </td>
                <td colspan="3">
                    <?=$paginator->counter(array('format' => 'Total : %count% ' .  'IGPMs')); ?>
                </td>
            </tr>
        </tfoot>
    </table>
    <?= $form->end(array('label' => false,
        'div' => false, 'class' => 'hide')); ?>
<?php else : ?>
    <h2 class="fg-color-red">Nenhum IGPM Encontrado.</h2>
<?php endif; ?>
</div>