<script type="text/javascript">
    $(function() {
        $( "#datepicker" ).datetimepicker( $.datepicker.regional[ "pt-BR" ] );
    });
</script>

<p class="grid_11 alpha omega">
	<label class="grid_0 alpha">Valor</label>
	<?php echo $form->input(
		'valor', 
		array(	'class' => 'grid_2 first alpha omega',
				'label' => false,
				'div' => false,
				'error' => array ( 'wrap' => 'span', 'class' => 'grid_8')
		)
	); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_0 alpha">Mês</label>
	<?php echo $form->input(
		'mes', 
		array(	'class' => 'grid_2 first alpha omega',
				'label' => false,
				'div' => false,
				'error' => array ( 'wrap' => 'span', 'class' => 'grid_8')
		)
	); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_0 alpha">Ano</label>
	<?php echo $form->input(
		'ano', 
		array(	'class' => 'grid_2 first alpha omega',
				'label' => false,
				'div' => false,
				'error' => array ( 'wrap' => 'span', 'class' => 'grid_8')
		)
	); ?>
</p>


