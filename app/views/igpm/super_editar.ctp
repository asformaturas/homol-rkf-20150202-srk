<span id="conteudo-titulo" class="box-com-titulo-header">IGP-M - Editar</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<?php echo $form->create('Igpm', array('url' => "/{$this->params['prefix']}/igpm/editar")); ?>
		<?php include	('_form.ctp'); ?>
		<?php echo $form->input('id',  array('hiddenField' => true)); ?>	

	<p class="grid_11 alpha omega">
		<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'action' => 'index') ,array('class' => 'cancel')); ?>
		<?php echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'submit'));?>
	</p>
</div>
