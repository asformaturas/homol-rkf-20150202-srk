<span id="conteudo-titulo" class="box-com-titulo-header">Adicionar Extra</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<?php echo $form->create('Extra', array('url' => "/{$this->params['prefix']}/extras/adicionar/" . $evento)); ?>
	
	<?php include('_form.ctp');?>
	
	<p class="grid_11 alpha omega">
		<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'action' => 'index') ,array('class' => 'cancel')); ?>
		<?php echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'submit'));?>
	</p>
</div>