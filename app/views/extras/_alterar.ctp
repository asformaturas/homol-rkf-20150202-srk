<script type="text/javascript">
        $("#form").submit(function(e) {
                e.preventDefault();
                var context = ko.contextFor($(".metro-button.reload")[0]);
                var dados = $("#form").serialize();
                var url = $("#form").attr('action');
                bootbox.hideAll();
                context.$data.showLoading(function() {
                $.ajax({
                    url: url,
                    data: dados,
                    type: "POST",
                    dataType: "json",
                    complete: function() {
                        context.$data.reload();
                    }
                });
                });
        });
</script>
<div class="row-fluid" dir="<?=$extra['Extra']['id']?>">
	<?php $session->flash(); ?>
	<?php echo $form->create('Extra', array('url' => "/{$this->params['prefix']}/extras/alterar/" . $this->data['Extra']['id'], 'id' => 'form')); ?>
	
	<?php echo $form->input('id', array('hiddenField' => true)); ?>
        <?php echo $form->hidden('evento_id', array()); ?>
    <div class="row-fluid">
        <div class="span8">
                <label>Nome</label>
                <?php echo $form->input('nome', array('label' => false, 'div' => 'input-control', 'error' => false)); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span8">
                <label>Descrição</label>
                <?php echo $form->textarea('descricao', array('label' => false, 'div' => 'input-control', 'error' => false, 'style' => 'width:479px; height: 91px')); ?>
        </div>
    </div>
    <br/>
    <div class="row-fluid">
	<div class="span8">
		<?php echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'button bg-color-greenDark'));?>
	</div>
    </div>
</div>