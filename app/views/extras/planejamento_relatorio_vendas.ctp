<span id="conteudo-titulo" class="box-com-titulo-header">Relatorio de Vendas Por Formando</span>
<div id="conteudo-container">
    <br />
    <div class="container-tabela">
        <table>
            <thead>
                <tr>
                    <th scope="col">C&oacute;digo</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Campanha</th>
                    <th scope="col">Item</th>
                    <th scope="col">Qtde</th>
                    <th scope="col">Valor Total</th>
                </tr>
            </thead>
            <tbody>
                <?php if(count($formandos) > 0) : ?>
                <?php foreach($formandos as $formando) : ?>
                <?php if($formando['ViewFormandos']['convites_contrato'] > 0 && !empty($formando['Protocolo']['id'])) { ?>
                <tr>
                    <td><?=$formando['ViewFormandos']['codigo_formando']?></td>
                    <td><?=$formando['ViewFormandos']['nome']?></td>
                    <td>Itens de Contrato</td>
                    <td>Convite</td>
                    <td><?=$formando['ViewFormandos']['convites_contrato']?></td>
                    <td>-</td>
                </tr>
                <?php } ?>
                <?php if($formando['ViewFormandos']['mesas_contrato'] > 0 && !empty($formando['Protocolo']['id'])) { ?>
                <tr>
                    <td><?=$formando['ViewFormandos']['codigo_formando']?></td>
                    <td><?=$formando['ViewFormandos']['nome']?></td>
                    <td>Itens de Contrato</td>
                    <td>Mesa</td>
                    <td><?=$formando['ViewFormandos']['mesas_contrato']?></td>
                    <td>-</td>
                </tr>
                <?php } ?>
                <?php if(count($formando['extras']) > 0) { foreach($formando['extras'] as $extra) { ?>
                <tr>
                    <td><?=$formando['ViewFormandos']['codigo_formando']?></td>
                    <td><?=$formando['ViewFormandos']['nome']?></td>
                    <td><?=$extra['ViewRelatorioExtras']['campanha']?></td>
                    <td><?=$extra['ViewRelatorioExtras']['extra']?></td>
                    <td><?=$extra['ViewRelatorioExtras']['quantidade_total']?></td>
                    <td><?= "R$" . number_format($extra['ViewRelatorioExtras']['valor_unitario']*$extra['ViewRelatorioExtras']['quantidade_total'], 2, ',', '.') ?></td>
                </tr>
                <?php } } ?>
                <?php if(isset($formando['checkout'])) { foreach($formando['checkout'] as $checkout) { ?>
                <tr>
                    <td><?=$formando['ViewFormandos']['codigo_formando']?></td>
                    <td><?=$formando['ViewFormandos']['nome']?></td>
                    <td>Checkout</td>
                    <td><?=$checkout['CheckoutItem']['titulo']?></td>
                    <td><?=$checkout['CheckoutUsuarioItem']['quantidade']?></td>
                    <td><?= "R$" . number_format($checkout['CheckoutUsuarioItem']['saldo']*-1, 2, ',', '.') ?></td>
                </tr>
                <?php } } ?>
                <?php endforeach; ?>
                <?php else : ?>
                <tr>
                    <td colspan='3'>
                        <h2 style="color:red">Nenhuma Venda Encontrada</h2>
                    </td>
                </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>