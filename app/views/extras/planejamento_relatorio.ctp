<?php
echo $html->script('jquery.tools.min');
echo $html->css('tooltip');
?>
<style type="text/css">
.center { text-align:center!important; }
.extras tr td:not(:last-child) { border-collapse:collapse!important; border-right:solid 1px #575756!important }
.extras tr.border-top td { padding-top:20px; border-collapse:collapse!important; border-top:solid 1px #575756!important }
</style>
<script type="text/javascript">
jQuery(function($) {
	jQuery(".help").tooltip();
});
</script>
<label id="conteudo-titulo" class="box-com-titulo-header">Relat&oacute;rio financeiro</label>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
	<div style="overflow:hidden; width:100%">
	<?php if($resumo) : ?>
	<div class="legenda" style="width:80%">
		<table class='extras'>
			<tr>
				<td><h2>Resumo</h2></td>
				<td class="center" width="12%"><h3>Inativo</h3></td>
				<td class="center" width="12%"><h3>Inadimplente</h3></td>
				<td class="center" width="12%"><h3>Pago Parcialemente</h3></td>
				<td class="center" width="12%"><h3>A Retirar</h3></td>
				<td class="center" width="12%"><h3>Total</h3></td>
			</tr>
			<?php foreach($resumo as $campanha) : ?>
			<tr class="border-top">
				<td><h3><?=$campanha['titulo']?></h3></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td colspan='1'></td>
			</tr>
			<tr height="10">
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td colspan='1'></td>
			</tr>
			<?php foreach($campanha['itens'] as $index => $item) : ?>
			<tr>
				<td><?=$extras[$index]?></td>
				<td class="center"><?=$item['inativo']?></td>
				<td class="center"><?=$item['inadimplente']?></td>
				<td class="center"><?=$item['pago parcialmente']?></td>
				<td class="center"><?=$item['a retirar']?></td>
				<td class="center"><?=$item['total']?></td>
			</tr>
			<?php endforeach; ?>
			<?php endforeach; ?>
		</table>
	</div>
	<?php endif; ?>
	</div>
	<div class="row-fluid">
            <a class='button mini bg-color-green pull-right' href='<?="/{$this->params['prefix']}/formandos/gerar_excel"?>'
                target='_blank'>
                Excel
                <i class='icon-file-excel'></i>
            </a>
	</div>
	<div class="container-tabela">
		<table>
			<thead>
				<tr>
					<th scope="col"><?=$paginator->sort('Cód.', 'codigo_formando'); ?></th>
					<th scope="col"><?=$paginator->sort('Formando', 'formando'); ?></th>
					<th scope="col"><?=$paginator->sort('Extra', 'extra'); ?></th>
					<th scope="col"><?=$paginator->sort('Qtd.', 'quantidade'); ?></th>
					<th scope="col"><?=$paginator->sort('Campanha', 'campanha'); ?></th>
					<th scope="col"><?=$paginator->sort('Evento', 'evento'); ?></th>
					<th scope="col"><?=$paginator->sort('Valor Campanha', 'valor_campanha'); ?></th>
					<th scope="col"><?=$paginator->sort('Valor Pago', 'valor_pago'); ?></th>
					<th scope="col"><?=$paginator->sort('Status', 'status'); ?></th>
				</tr>
			</thead>
			<tbody>
			<?php if(count($formandos) > 0) :?>
			<?php foreach($formandos as $formando): ?>
				<?php
					if($formando["ViewRelatorioExtras"]["status"] == "pago parcialmente")
						$color = "#FBFCC2";
					elseif($formando["ViewRelatorioExtras"]["status"] == "a retirar")
						$color = "#C2FCE8";
					elseif($formando["ViewRelatorioExtras"]["status"] == "inadimplente")
						$color = "#FCD2D2";
					elseif($formando["ViewRelatorioExtras"]["status"] == "inativo")
						$color = "#E7E7E7";
				?>
				<?="<tr style='background-color:$color'>"?>
					<td  collabel="1" width="5%">
						<span class='item'>
							<?=$formando['ViewRelatorioExtras']['codigo_formando']; ?>
							<div class='help' title='<?="Extra comprado em " . date('d/m/Y',strtotime($formando['ViewRelatorioExtras']['data_compra']))?>'></div>
						</span>
					</td>
					<td  collabel="1" width="15%"><?=$formando['ViewRelatorioExtras']['formando']; ?></td>
					<td  collabel="1"><?=$formando["ViewRelatorioExtras"]["extra"] ?></td>
					<td  collabel="1"><?=$formando['ViewRelatorioExtras']['quantidade_total']; ?></td>
					<td  collabel="1"><?=$formando['ViewRelatorioExtras']['campanha'] ?></td>
					<td  collabel="1"><?=$formando['ViewRelatorioExtras']["evento"]; ?></td>
					<td  collabel="1">R$<?=number_format($formando['ViewRelatorioExtras']['valor_campanha'], 2, ',' , '.'); ?></td>
					<td  collabel="1">R$<?=number_format($formando['ViewRelatorioExtras']['valor_pago'], 2, ',' , '.'); ?></td>
					<td  collabel="1"><?=$formando["ViewRelatorioExtras"]["status"] ?></td>
				</tr>
			<?php endforeach; ?>
			<?php  else : ?>
				<td colspan="10"><h2>Nenhum extra foi vendido nesta turma</h2></td>
			<?php endif; ?>
			</tbody>
		</table>
	</div>
</div>