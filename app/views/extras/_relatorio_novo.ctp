<?php
    echo $html->script('jquery.tools.min');
    echo $html->css('tooltip');

    $paginator->options(array(
        'url' => array($this->params['prefix'] => true)));
    $sortOptions = array('data-bind' => 'click: loadThis');
?>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/tooltip.js"></script>
<script type="text/javascript">
$('body').tooltip({ selector: '[rel=tooltip]'});
</script>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Relat&oacute;rio de Extras
            <a class='button mini bg-color-green pull-right' href='<?="/{$this->params['prefix']}/extras/gerar_excel"?>'
            target='_blank'>
            Excel
            <i class='icon-file-excel'></i>
        </a>
        </h2>
    </div>
</div>
	<?php if($resumo) : ?>
<div class="row-fluid">
        <table class="table table-striped table-condensed table-bordered" style="border-left: 1px solid #ccc;">
            <thead>
                <tr>
                        <th>Resumo</th>
                        <th>Inativo</th>
                        <th>Cancelado</th>
                        <th>Inadimplente</th>
                        <th>Pago Parcialemente</th>
                        <th>A Retirar</th>
                        <th>Total</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($resumo as $campanha) : ?>
                <?php foreach($campanha['itens'] as $index => $item) : ?>
                <tr>
                    <td><?=$extras[$index]?> - <?=$campanha['titulo'];?> </td>
                        <td><?=$item['inativo']?></td>
                        <td><?=$item['cancelado']?></td>
                        <td><?=$item['inadimplente']?></td>
                        <td><?=$item['pago parcialmente']?></td>
                        <td><?=$item['a retirar']?></td>
                        <td><?=$item['total']?></td>
                </tr>
            </tbody>
                <?php endforeach; ?>
                <?php endforeach; ?>
        </table>
</div>
<?php endif; ?>
<br/>

<div class="row-fluid">
        <table class="table table-condensed table-bordered">
                <thead>
                        <tr>
                                <th scope="col"><?=$paginator->sort('Cód.', 'codigo_formando', $sortOptions); ?></th>
                                <th scope="col"><?=$paginator->sort('Formando', 'formando', $sortOptions); ?></th>
                                <th scope="col"><?=$paginator->sort('Extra', 'extra', $sortOptions); ?></th>
                                <th scope="col"><?=$paginator->sort('Qtd.', 'quantidade', $sortOptions); ?></th>
                                <th scope="col"><?=$paginator->sort('Campanha', 'campanha', $sortOptions); ?></th>
                                <th scope="col"><?=$paginator->sort('Evento', 'evento', $sortOptions); ?></th>
                                <th scope="col"><?=$paginator->sort('Valor Campanha', 'valor_campanha', $sortOptions); ?></th>
                                <th scope="col"><?=$paginator->sort('Valor Pago', 'valor_pago', $sortOptions); ?></th>
                                <th scope="col"><?=$paginator->sort('Status', 'status', $sortOptions); ?></th>
                        </tr>
                </thead>
                <tbody>
                <?php if(count($formandos) > 0) : ?>
                <?php foreach($formandos as $formando) : ?>
                        <?php
                                if($formando["ViewRelatorioExtras"]["status"] == "pago parcialmente")
                                        $class = "bg-color-yellow";
                                elseif($formando["ViewRelatorioExtras"]["status"] == "a retirar")
                                        $class = "bg-color-greenDark";
                                elseif($formando["ViewRelatorioExtras"]["status"] == "inadimplente")
                                        $class = "bg-color-orangeDark";
                                elseif($formando["ViewRelatorioExtras"]["status"] == "inativo")
                                        $class = "bg-color-gray";
                                elseif($formando["ViewRelatorioExtras"]["status"] == "cancelado")
                                        $class = "bg-color-red";
                        ?>
                        <tr class="<?=$class?>" style="opacity:.8" rel="tooltip" title='<?="Extra comprado em " . date('d/m/Y',strtotime($formando['ViewRelatorioExtras']['data_compra']))?>'>
                                <td  collabel="1" width="5%">
                                <?=$formando['ViewRelatorioExtras']['codigo_formando']; ?>
                                </td>
                                <td  collabel="1" width="15%"><?=$formando['ViewRelatorioExtras']['formando']; ?></td>
                                <td  collabel="1"><?=$formando["ViewRelatorioExtras"]["extra"] ?></td>
                                <td  collabel="1"><?=$formando['ViewRelatorioExtras']['quantidade_total']; ?></td>
                                <td  collabel="1"><?=$formando['ViewRelatorioExtras']['campanha'] ?></td>
                                <td  collabel="1"><?=$formando['ViewRelatorioExtras']["evento"]; ?></td>
                                <td  collabel="1">R$<?=number_format($formando['ViewRelatorioExtras']['valor_campanha'], 2, ',' , '.'); ?></td>
                                <td  collabel="1">R$<?=number_format($formando['ViewRelatorioExtras']['valor_pago'], 2, ',' , '.'); ?></td>
                                <td  collabel="1">
                                <?=$formando["ViewRelatorioExtras"]["status"] ?>
                                </td>
                        </tr>
                <?php endforeach; ?>
                <?php  else : ?>
                        <td colspan="10"><h2>Nenhum extra vendido para esta turma.</h2></td>
                <?php endif; ?>
                </tbody>
        </table>
</div>