<span id="conteudo-titulo" class="box-com-titulo-header"><?php echo 'Editar item de cronograma';?></span>
<div id="conteudo-container">
<?php $session->flash(); ?>
	<?php echo $form->create('Cronograma', array('url' => "/{$this->params['prefix']}/cronogramas/editar/{$this->data['Cronograma']['id']}")); ?>
		
		<?php include('_form_editar.ctp'); ?>
		
	<p class="grid_11 alpha omega">
		<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'controller' => 'cronogramas', 'action' => 'index') ,array('class' => 'cancel')); ?>
		<?php echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'submit'));?>
	</p>
</div>