<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Item</label>
	<span class="grid_5 alpha first"><?php echo $cronograma['Item']['nome']; ?></span>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Descrição do Item</label>
	<span class="grid_5 alpha first"><?php echo $cronograma['Item']['descricao']; ?></span>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Data Limite</label>
	<span class="grid_5 alpha first"><?php echo date('d/m/Y',strtotime($cronograma['Cronograma']['data_limite'])); ?></span>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Pendências RK</label>
	<label class="grid_5 alpha">Pendências Comissão</label>
	<span class="grid_8 alpha first"><?php echo $pendencias['as']; ?></span>
	<span class="grid_8 alpha"><?php echo $pendencias['comissao']; ?></span>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Status</label>
	<span class="grid_8 alpha first"><?php echo $cronograma['Cronograma']['status']; ?></span>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Conclusão</label>
	<span class="grid_8 alpha first"><?php echo $cronograma['Cronograma']['conclusao']; ?></span>
</p>