<?php if (count($pagamentosProcessados['boleto']['novo']) > 0) { ?>
<div class="container-tabela-financeiro">
	<h2 style="margin-bottom: 5px; border-bottom: 1px solid #000;">Boletos novos importados</h2>
		<table class="tabela-financeiro tabela-boletos" >
			<thead>
				<tr>
					<th scope="col">Cod. Formando</th>
					<th scope="col">N&uacute;mero do documento</th>
					<th scope="col">Valor Pago</th>
					<th scope="col">Desp. Paga</th>
					<th scope="col">Data de liquidacao</th>
					<th scope="col">Status</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="4">
					</td>
					<td colspan="2"><?php echo count($pagamentosProcessados['boleto']['novo']);?> boleto(s) novos encontrados.</td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach ($pagamentosProcessados['boleto']['novo'] as $boleto): ?>
				<?php if($isOdd):?>
					<tr class="odd">
				<?php else:?>
					<tr>
				<?php endif;?>
					<td  colspan="1" width="20%"><?php echo $boleto['codigoDoFormando']; ?></td>
					<td  colspan="1" width="10%"><?php echo $boleto['nossoNumero']; ?></td>
					<td  colspan="1" width="15%"><?php echo $boleto['valorPagoTotal']; ?></td>
					<td  colspan="1" width="15%"><?php echo (isset($boleto['despesa']['id'])) ? $boleto['despesa']['id'] : '-'; ?></td>
					<td  colspan="1" width="10%"><?php echo ($boleto['dataDeLiquidacao'] == "") ? '-' : preg_replace('#(\d{2})/(\d{2})/(\d{4})#', '$3-$2-$1', $boleto['dataDeLiquidacao']); ?></td>					
					<td  colspan="1" width="30%"><?php echo $boleto['texto_status']; ?></td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
<?php  } ?>

<?php if (count($pagamentosProcessados['boleto']['antigo']) > 0) { ?>
<div class="container-tabela-financeiro">
	<h2 style="margin-bottom: 5px; border-bottom: 1px solid #000;">Boletos antigos importados</h2>
		<table class="tabela-financeiro tabela-boletos" >
			<thead>
				<tr>
					<th scope="col">Cod. Formando</th>
					<th scope="col">N&uacute;mero do documento</th>
					<th scope="col">Valor Pago</th>
					<th scope="col">Desp. Paga</th>
					<th scope="col">Data de liquidacao</th>
					<th scope="col">Status</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="3">
					</td>
					<td colspan="2"><?php echo count($pagamentosProcessados['boleto']['antigo']);?> boleto(s) antigos encontrados.</td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach ($pagamentosProcessados['boleto']['antigo'] as $boleto): ?>
				<?php if($isOdd):?>
					<tr class="odd">
				<?php else:?>
					<tr>
				<?php endif;?>
					<td  colspan="1" width="20%"><?php echo $boleto['codigoDoFormando']; ?></td>
					<td  colspan="1" width="10%"><?php echo $boleto['nossoNumero']; ?></td>
					<td  colspan="1" width="15%"><?php echo $boleto['valorPagoTotal']; ?></td>
					<td  colspan="1" width="15%"><?php echo (isset($boleto['despesa']['id'])) ? $boleto['despesa']['id'] : '-'; ?></td>
					<td  colspan="1" width="10%"><?php echo ($boleto['dataDeLiquidacao'] == "") ? '-' : date('d/m/Y', strtotime($boleto['dataDeLiquidacao'])); ?></td>					
					<td  colspan="1" width="30%"><?php echo $boleto['texto_status']; ?></td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
<?php  } ?>

<?php if (count($pagamentosProcessados['boleto']['igpm']) > 0) { ?>
<div class="container-tabela-financeiro">
	<h2 style="margin-bottom: 5px; border-bottom: 1px solid #000;">Boletos de IGPM importados</h2>
		<table class="tabela-financeiro tabela-boletos" >
			<thead>
				<tr>
					<th scope="col">Cod. Formando</th>
					<th scope="col">N&uacute;mero do documento</th>
					<th scope="col">Valor Pago</th>
					<th scope="col">Desp. Paga</th>
					<th scope="col">Data de liquidacao</th>
					<th scope="col">Status</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="3">
					</td>
					<td colspan="2"><?php echo count($pagamentosProcessados['boleto']['igpm']);?> boleto(s) de igpm encontrados.</td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach ($pagamentosProcessados['boleto']['igpm'] as $boleto): ?>
				<?php if($isOdd):?>
					<tr class="odd">
				<?php else:?>
					<tr>
				<?php endif;?>
					<td  colspan="1" width="20%"><?php echo $boleto['codigoDoFormando']; ?></td>
					<td  colspan="1" width="10%"><?php echo $boleto['nossoNumero']; ?></td>
					<td  colspan="1" width="15%"><?php echo $boleto['valorPagoTotal']; ?></td>
					<td  colspan="1" width="15%"><?php echo (isset($boleto['despesa']['id'])) ? $boleto['despesa']['id'] : '-'; ?></td>
					<td  colspan="1" width="10%"><?php echo ($boleto['dataDeLiquidacao'] == "") ? '-' : date('d/m/Y', strtotime($boleto['dataDeLiquidacao'])); ?></td>					
					<td  colspan="1" width="30%"><?php echo $boleto['texto_status']; ?></td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
<?php  } ?>

<?php if (count($pagamentosProcessados['cheque']) > 0) { ?>
<div class="container-tabela-financeiro">
	<h2 style="margin-bottom: 5px; border-bottom: 1px solid #000;">Cheques importados</h2>
		<table class="tabela-financeiro tabela-boletos" >
			<thead>
				<tr>
					<th scope="col">Cod. Formando</th>
					<th scope="col">Valor Pago</th>
					<th scope="col">Desp. Paga</th>
					<th scope="col">Data de pagamento</th>
					<th scope="col">Data de liquidacao</th>
					<th scope="col">Status</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="3">
					</td>
					<td colspan="2"><?php echo count($pagamentosProcessados['cheque']);?> cheques(s) encontrados.</td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach ($pagamentosProcessados['cheque'] as $cheque): ?>
				<?php if($isOdd):?>
					<tr class="odd">
				<?php else:?>
					<tr>
				<?php endif;?>
					<td  colspan="1" width="20%"><?php echo $cheque['codigoDoFormando']; ?></td>
					<td  colspan="1" width="15%"><?php echo $cheque['valorPagoTotal']; ?></td>
					<td  colspan="1" width="15%"><?php echo (isset($cheque['despesa']['id'])) ? $cheque['despesa']['id'] : '-'; ?></td>
					<td  colspan="1" width="10%"><?php echo date('d/m/Y', strtotime($cheque['dataPagamento'])); ?></td>			
					<td  colspan="1" width="10%"><?php echo ($cheque['dataDeLiquidacao'] == "") ? '-' : date('d/m/Y', strtotime($cheque['dataDeLiquidacao'])); ?></td>					
					<td  colspan="1" width="30%"><?php echo $cheque['texto_status']; ?></td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
<?php  } ?>