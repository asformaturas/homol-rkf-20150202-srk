<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/bootstrap/fileupload.css">
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/bootmetro/progressbar.css">
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/metroui/table.css">
<style type="text/css">
.button-upload { height:auto!important; min-height:auto!important; vertical-align:middle; }
.close.fileupload-exists { opacity:1 }
.progress { position:relative; float:left; width:60%; height:26px;
    display:inline-block; margin:0 5px 0 0 }
.progress .descricao { position:absolute; top:10px; left:5px; font-size:11px;
    font-weight:bold; line-height:8px }
</style>
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/bootstrap/fileupload.js"></script>
<script type="text/javascript">
var arquivo = {};
$(document).ready(function() {
    
    var timer;
    
    $("#file-pagamento").change(function(e) {
        if(e.target.files[0] != undefined) {
            arquivo.extensao = e.target.files[0].name.split('.').pop();
            arquivo.tipo = e.target.files[0].type;
            arquivo.tamanho = e.target.files[0].size;
            var reader = new FileReader();
            reader.readAsDataURL(e.target.files[0]);
            reader.onload = function(f) {
                var posIni = f.target.result.indexOf('base64,')+7;
                arquivo.data = f.target.result.substring(posIni,
                    f.target.result.length);
            }
        } else {
            arquivo = {};
        }
    });
    
    $(".agendamento").click(function(e) {
        e.preventDefault();
        var id = $(this).parents('.app-upload').attr('id').substring(7);
        var context = ko.contextFor($(".metro-button.reload")[0]);
        if($(this).hasClass('enviar')) {
            context.$data.showLoading(function() {
                var url = "/<?=$this->params["prefix"]?>/pagamentos/inserir_agendamento/"+id;
                $.ajax({
                    url : url,
                    complete : function() {
                        context.$data.reload();
                    }
                });
            });
        } else {
            bootbox.confirm('Tem ceteza que deseja cancelar o agendamento?',function(response) {
                if(response) {
                    context.$data.showLoading(function() {
                        var url = "/<?=$this->params["prefix"]?>/pagamentos/cancelar_agendamento/"+id;
                        $.ajax({
                            url : url,
                            complete : function() {
                                context.$data.reload();
                            }
                        });
                    });
                }
            })
        }
    });
    
    $(".apagar").click(function(e) {
        e.preventDefault();
        var id = $(this).parents('.app-upload').attr('id').substring(7);
        var context = ko.contextFor($(".metro-button.reload")[0]);
        bootbox.confirm('Tem ceteza que deseja apagar o registro?',function(response) {
            if(response) {
                context.$data.showLoading(function() {
                    var url = "/<?=$this->params["prefix"]?>/pagamentos/apagar_upload/"+id;
                    $.ajax({
                        url : url,
                        complete : function() {
                            context.$data.reload();
                        }
                    });
                });
            }
        });
    });
    
    $("#form-upload").submit(function(e) { e.preventDefault(); });
    
    $("#enviar-planilha").click(function(e) {
        e.preventDefault();
        var context = ko.contextFor($(".metro-button.reload")[0]);
        context.$data.showLoading(function() {
            var url = "/<?=$this->params["prefix"]?>/pagamentos/upload_pagamentos";
            $.ajax({
                url : url,
                data : { data: arquivo },
                type : "POST",
                dataType : "json",
                complete : function() {
                    context.$data.reload();
                }
            });
        });
    });

    
    $('.iniciar-upload').click(function(e) {
        e.preventDefault();
        if($(".app-upload.iniciado:not(.finalizado)").length < 1)
            iniciarUpload($(this).parent().parent());
    });
    
    $(".app-upload.iniciado:not(.finalizado)").each(function() {
        //iniciarUpload($(this));
    });
    
    function iniciarUpload(app) {
        $('.iniciar-upload').addClass('disabled');
        app.addClass('iniciado');
        var processo = app.children('.processo');
        var buttonImportar = processo.children('.acao');
        var descricao = $('<div>',{class:'descricao fg-color-red'});
        var bar = $('<div>',{class:'bar'});
        var progress = $("<div>",{
            class:'progress hide'
        })
            .append(descricao)
            .append(bar)
            .appendTo(processo);
        $("#form-upload").fadeOut(500,function() {
            buttonImportar.fadeOut(500,function() {
                progress.fadeIn(500,function() {
                    descricao.html('Iniciando Importa&ccedil;&atilde;o');
                    clearTimeout(timer);
                    verificarUpload(app[0]);
                });
            });
        })
    }
    
    function verificarUpload(app) {
        var id = $(app).attr('id').substring(7);
        var obj = $(app).children('.processo')[0];
        var progressbar = $(obj).children('.progress');
        var descricao = progressbar.children('.descricao');
        var bar = progressbar.children('.bar');
        var url = "/<?=$this->params["prefix"]?>/pagamentos/verificar_upload/"+id;
        $.getJSON(url, function(data) {
            if(data.erro == 1) {
                if(data.mensagem != undefined)
                    descricao.html(data.mensagem);
                else
                    descricao.html('Erro ao Verificar Upload');
                $(app).addClass('finalizado');
            } else {
                var upload = data.arquivo.UploadPagamento;
                var registrosProcessados = upload.registros_processados;
                var total = upload.total_registros;
                var porcentagem = (registrosProcessados*100)/total;
                var texto = registrosProcessados + " de " + total +
                        " pagamentos importados";
                descricao.text(texto);
                bar.css('width',parseFloat(porcentagem.toFixed(2))+'%');
                if($(app).children('.data-inicio').html().trim() == "" && upload.data_inicio != null)
                    $(app).children('.data-inicio').html(upload.data_inicio);
                if(data.arquivo.UploadPagamento.data_fim != null && !$(app).hasClass('finalizado')) {
                    $(app).addClass('finalizado');
                    $(app).children('.data-fim').html(upload.data_fim);
                    var log = '<a href="/<?=$this->params["prefix"]?>/pagamentos/view/'+
                            id+'" class="button mini bg-color-blueDark fg-color-white" '+
                            'target="_blank">Ver Log <i class="icon-bars"></i></a>';
                    progressbar.fadeOut(500,function() {
                        $(obj).prepend(log);
                    });
                }
            }
        }).fail(function() {
            $(app).addClass('finalizado');
        }).always(function() {
            if($(".app-upload.iniciado:not(.finalizado)").length > 0) {
                timer = setTimeout(function() { verificarUpload(app) },1000);
            } else {
                $("#form-upload").fadeIn(500);
                $('.iniciar-upload').removeClass('disabled');
            }
        });
    }
});
</script>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Importar Pagamentos
        </h2>
    </div>
</div>
<br />
<?php $session->flash(); ?>
<div class="row-fluid">
    <div class="span12">
        <?=$form->create('UploadPagamento',
            array('url' => "/{$this->params['prefix']}/pagamentos/upload_pagamentos",
                'type' => 'file', 'id' => 'form-upload')); ?>
        <div class="fileupload fileupload-new" data-provides="fileupload"
            style="display:inline-block">
            <span class="button btn-file fg-color-black button-upload">
                <span class="fileupload-new">Selecionar Arquivo</span>
                <span class="fileupload-exists">Alterar</span>
                <input type="file" id="file-pagamento" />
            </span>
            &nbsp;
            <span class="fileupload-preview"></span>
            <a href="#" class="close fileupload-exists help-inline"
               data-dismiss="fileupload" style="float: none">×</a>
            &nbsp;
        </div>
        <button type="button" class="button bg-color-red"
            id="enviar-planilha">
            <i class="icon-cloud-upload"></i>
            Enviar
        </button>
        <?= $form->end(array('label' => false,
            'div' => false, 'class' => 'hide')); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span12">
        <table class="table table-condensed table-striped">
            <thead>
                <tr>
                    <th>Usu&aacute;rio</th>
                    <th>Data Cadastro</th>
                    <th>Data In&iacute;cio</th>
                    <th>Data Fim</th>
                    <th width="50%"></th>
                </tr>
            </thead>
            <tbody>
                <?php if(count($arquivos) > 0) : foreach($arquivos as $arquivo) : ?>
                <?php
                    if(!empty($arquivo['UploadPagamento']['data_fim']))
                        $appClass = 'finalizado';
                    elseif(!empty($arquivo['UploadPagamento']['data_inicio']))
                        $appClass = 'iniciado';
                    else
                        $appClass = '';
                ?>
                <tr class="app-upload <?=$appClass?>" id='upload-<?=$arquivo['UploadPagamento']['id']?>'>
                    <td><?=$arquivo['Usuario']['nome']; ?></td>
                    <td><?=date('d/m/Y à\s H:i',
                            strtotime($arquivo['UploadPagamento']['data_cadastro'])); ?></td>
                    <td class="data-inicio">
                        <?php
                            if(!empty($arquivo['UploadPagamento']['data_inicio']))
                                echo date('d/m/Y à\s H:i',
                                    strtotime($arquivo['UploadPagamento']['data_inicio']));
                        ?>
                    </td>
                    <td class="data-fim">
                        <?php if(!empty($arquivo['UploadPagamento']['data_fim'])) : ?>
                        <?=date('d/m/Y à\s H:i',
                            strtotime($arquivo['UploadPagamento']['data_fim'])); ?>
                        <?php endif; ?>
                    </td>
                    <td class="processo">
                        <?php if(empty($arquivo['UploadPagamento']['data_inicio'])) : ?>
                        <a class="button mini bg-color-red apagar acao">
                            Apagar
                            <i class="icon-cancel-3"></i>
                        </a>
                        <?php if($arquivo['UploadPagamento']['agendado'] == 0) : ?>
                        <a class="button mini bg-color-darken acao agendamento enviar">
                            Agendar Importa&ccedil;&atilde;o
                            <i class="icon-fire"></i>
                        </a>
                        <?php else : ?>
                        <a class="button mini bg-color-orange acao agendamento cancelar">
                            Cancelar Agendamento
                            <i class="icon-minus-2"></i>
                        </a>
                        <?php endif; ?>
                        <?php elseif(empty($arquivo['UploadPagamento']['data_fim'])) : ?>
                        <span class="label label-info">Processando Pagamentos</span>
                        <?php endif; ?>
                        <?php if(!empty($arquivo['UploadPagamento']['arquivo_html'])) : ?>
                        <a href="/financeiro/pagamentos/view/<?=$arquivo['UploadPagamento']['id']?>"
                           class="button mini bg-color-blueDark" target="_blank">
                            Ver Log
                            <i class="icon-bars"></i>
                        </a>
                        <?php endif; ?>
                        <a href="/financeiro/pagamentos/excel/<?=$arquivo['UploadPagamento']['id']?>"
                           class="button mini bg-color-greenDark" target="_blank">
                            Baixar Arquivo
                            <i class="icon-new"></i>
                        </a>
                    </td>
                </tr>
                <?php endforeach; else : ?>
                <tr>
                    <td colspan="5">
                        <h3 class="fg-color-red">
                            Nenhum Arquivo Importado
                        </h3>
                    </td>
                </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>