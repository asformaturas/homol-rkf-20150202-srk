<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/form_validate.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/datetimepicker.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/locales/bootstrap-datetimepicker.pt-BR.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/jquery-validate.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<style type="text/css">
.form_datetime:read-only { background: white!important }
.vincular-turmas { display: none }
</style>
<script type="text/javascript">
    $(document).ready(function(){
        $("#formulario").submit(function(e) {
            e.preventDefault();
        });
        $('.selectpicker').selectpicker();
        $(".vincular-departamentos").change(function(e) {
            e.preventDefault();
            if($.inArray('funcionarios',$(this).val()) >= 0)
                $(this).val('funcionarios').selectpicker('refresh');
        });
        $("#vincular-turmas").click(function() {
            $(this).fadeOut(500,function() {
                $(".vincular-turmas").fadeIn(500);
            });
        });
        $("#cancelar-turmas").click(function() {
            $(".vincular-turmas").fadeOut(500,function() {
                $("#vincular-turmas").fadeIn(500,function() {
                    $("div.vincular-turmas").find('select[multiple]').each(function() {
                        $(this).val('').selectpicker('refresh');
                    });
                });
            });
        });
        $(".form_datetime").datetimepicker({
            format: 'dd/mm/yyyy hh:ii',
            autoclose : true,
            language : 'pt-BR'
        });
        $("#select-tipo").change(function() {
            if($(this).val() == 'evento_as') {
                $("<input>",{
                    type:'hidden',
                    name:'data[Atividade][grupos]',
                    class: 'evento-as',
                    value:'funcionarios'
                }).appendTo($("#formulario"));
                $("[data-tipo='convite']").hide();
            } else {
                $(".evento-as").remove();
                $("[data-tipo='convite']").show();
                if($(this).val().indexOf('reuniao') >= 0 && $("[data-campo-extra='Pessoas Esperadas']").length == 0) {
                    $(this).parent().parent().append(criarInput('Pessoas Esperadas'));
                } else {
                    $("[data-campo-extra='Pessoas Esperadas']").remove();
                }
                $('#formulario').validate('validate');
            }
        });
        $("#local").change(function() {
            if($(this).val() == "outros")
                $("#local-manual").fadeIn(500);
            else
                $("#local-manual").fadeOut(500);
        });
        
        var turmasInseridas = [];
        
        function obterIdTurma(id) {
            var turmaId = null;
            $.each(turmasInseridas,function(i,turma) {
                if(turma.turma_id == id)
                    turmaId = turma.id;
            });
            return turmaId;
        }
        
        function criarInput(nome) {
            return $("<div>",{
                class : 'input-control text required',
                'data-campo-extra' : nome,
            }).append($("<label>",{
                for : nome,
                text : nome,
            })).append($("<input>",{
                type : 'text',
                'id' : nome,
                'class' : 'input-block-level',
                'data-required' : true
            }));
        }
        
        <?php if($atividade) : ?>
        var departamentosInseridos = ('<?=$atividade['Atividade']['grupos']?>').split(',');
        $("#vincular-departamentos").selectpicker('val',departamentosInseridos);
        turmasInseridas = <?=json_encode($atividade['AtividadeTurma'])?>;
        var t = [];
        var g = [];
        $.each(turmasInseridas,function(i,turma) {
            t.push(turma.turma_id);
            g = turma.grupos.split(',');
        });
        if(t.length > 0) {
            $("#select-turmas").selectpicker('val',t);
            if($.inArray('comissao',g) >= 0) {
                $("#vincular-comissao").selectpicker('val',1);
                g.splice($.inArray('comissao',g),1);
            }
            $("#vincular-turmas-departamentos").selectpicker('val',g);
            $("#vincular-turmas").trigger('click');
        }
        <?php endif; ?>
        $('#formulario').validate({
            valid: function() {
                $("#enviar-evento").hide();
                var turmas = [];
                if($("#select-tipo").val() != 'evento_as') {
                    
                    var departamentos = $("#vincular-departamentos").val() != null ?
                        $("#vincular-departamentos").val() : [];
                    
                    $("<input>",{
                        type:'hidden',
                        name:'data[Atividade][grupos]',
                        value:departamentos.join(',')
                    }).appendTo($("#formulario"));
                    
                    if($("#select-turmas").val() != null) {
                        var departamentosTurmas = $("#vincular-turmas-departamentos").val() != null ?
                            $("#vincular-turmas-departamentos").val() : [];
                            
                        if($("#vincular-comissao").val() == 1)
                            departamentosTurmas.push('comissao');
                        
                        turmas = $("#select-turmas").val();
                        $.each(turmas,function(i,turma) {
                            var id = obterIdTurma(turma);
                            if(id != null)
                                $("<input>",{
                                    type:'hidden',
                                    name:'data[AtividadeTurma]['+i+'][id]',
                                    value:id
                                }).appendTo($("#formulario"));
                            $("<input>",{
                                type:'hidden',
                                name:'data[AtividadeTurma]['+i+'][turma_id]',
                                value:turma
                            }).appendTo($("#formulario"));
                            $("<input>",{
                                type:'hidden',
                                name:'data[AtividadeTurma]['+i+'][grupos]',
                                value:departamentosTurmas.join(',')
                            }).appendTo($("#formulario"));
                        });
                    }
                }
                var removerTurmas = [];
                $.each(turmasInseridas,function(i,turma) {
                    if(!($.inArray(turma.turma_id,turmas) >= 0))
                        removerTurmas.push(turma.id);
                });
                $.each(removerTurmas,function(i,turma) {
                    $("<input>",{
                        type:'hidden',
                        name:'data[remover_turmas]['+i+']',
                        value:turma
                    }).appendTo($("#formulario"));
                });
                if($("#local").val() == "outros") {
                    $("#local").find("option:first").val($("#local-manual").val());
                    $("#local").val($("#local-manual").val());
                }
                $("#local-manual").attr('disabled','disabled');
                var context = ko.contextFor($("#content-body")[0]);
                var dados = $("#formulario").serialize();
                var url = $("#formulario").attr('action');
                context.$data.showLoading(function() {
                    $.ajax({
                        url : url,
                        data : dados,
                        type : "POST",
                        dataType : "json",
                        complete : function() {
                            bootbox.hideAll();
                            context.$data.reload();
                        }
                    });
                });
            },
            conditional : {
                local : function() {
                    if($("#local").val() == "outros")
                        return $("#local-manual").val() != "";
                    else
                        return $("#local").val() != "";
                }
            }
        });
    });
</script>
<?php
$session->flash();
echo $form->create('Atividade', array(
    'url' => "/calendario/editar",
    'id' => 'formulario'
));
echo $form->hidden('Atividade.id');
?>
<div class='row-fluid'>
    <?=$form->input('nome', array(
        'label' => 'Titulo',
        'data-required' => 'true',
        'class' => 'input-block-level',
        'div' => 'input-control text required'));
    ?>
</div>
<div class='row-fluid'>
    <div class='span6'>
        <?=$form->input('tipo', array(
            'options' => $tipo,
            'type' => 'select',
            'id' => 'select-tipo',
            'title' => 'Selecione um tipo',
            'data-width' => '100%',
            'data-size' => '5',
            'class' => 'selectpicker',
            'label' => 'Tipo',
            'data-required' => 'true',
            'div' => 'input-control text required')); ?>
    </div>
    <div class='span3'>
        <?=$form->input('data_inicio', array(
            'label' => 'Data Inicio',
            'readonly' => 'readonly',
            'type' => 'text',
            'data-required' => 'true',
            'class' => 'form_datetime input-block-level',
            'error' => false,
            'div' => 'input-control text required'));
        ?>
    </div>
    <div class='span3'>
        <?=$form->input('data_fim', array(
            'label' => 'Data Fim',
            'readonly' => 'readonly',
            'type' => 'text',
            'class' => 'form_datetime input-block-level',
            'error' => false,
            'div' => 'input-control text'));
        ?>
    </div>
</div>
<div class='row-fluid'>
    <div class="span6">
        <?=$form->input('local', array(
            'label' => 'Local',
            'options' => $local,
            'type' => 'select',
            'id' => 'local',
            'data-conditional' => 'local',
            'title' => 'Selecione um local',
            'data-width' => '100%',
            'data-size' => '5',
            'class' => 'selectpicker',
            'div' => 'input-control text'));
        ?>
    </div>
    <div class="span6">
        <?=$form->input('local-manual', array(
            'label' => '&nbsp;',
            'id' => 'local-manual',
            'data-conditional' => 'local',
            'class' => 'input-block-level hide',
            'div' => 'input-control text'));
        ?>
    </div>
</div>
<div class='row-fluid'>
    <div class="span4">
        <?=$form->input('numero_pessoas', array(
            'label' => 'N&ordm; Pessoas',
            'data-required' => 'true',
            'type' => 'text',
            'div' => 'input-control text'));
        ?>
    </div>
</div>
<div class='row-fluid'>
    <?=$form->input('descricao', array(
        'label' => 'Descricao',
        'data-required' => 'true',
        'class' => 'input-block-level',
        'rows' => 4,
        'div' => 'input-control text required'));
    ?>
</div>
<br />
<?php if($podeConvidar) : ?>
<div class="row-fluid" data-tipo='convite'>
    <div class="row-fluid">
        <?=$form->input('grupos', array(
            'options' => $grupos['funcionarios'],
            'type' => 'select',
            'id' => 'vincular-departamentos',
            'multiple' => 'multiple',
            'title' => 'Departamentos',
            'data-live-search' => 'true',
            'data-width' => '100%',
            'data-size' => '5',
            'class' => 'selectpicker vincular-departamentos',
            'label' => 'Vincular Departamentos',
            'div' => 'input-control text')); ?>
    </div>
    <div class="row-fluid">
        <button class="mini default" type="button"
            id="vincular-turmas">
            Vincular Turmas
        </button>
        <button class="mini bg-color-red vincular-turmas"
            type="button" id="cancelar-turmas">
            Remover Turmas
        </button>
    </div>
    <div class="row-fluid vincular-turmas">
        <?=$form->input(false, array(
            'options' => $turmas,
            'type' => 'select',
            'id' => 'select-turmas',
            'multiple' => 'multiple',
            'title' => 'Selecione as Turmas',
            'data-width' => '100%',
            'data-size' => '5',
            'data-selected-text-format' => 'count>2',
            'data-count-selected-text' => '{0} turmas selecionadas',
            'data-live-search' => 'true',
            'class' => 'selectpicker',
            'label' => false,
            'div' => 'input-control text')); ?>
        <?=$form->input(false, array(
            'options' => array('Não','Sim'),
            'value' => 0,
            'type' => 'select',
            'id' => 'vincular-comissao',
            'title' => 'Comissão',
            'class' => 'selectpicker',
            'label' => 'Liberar para comiss&atilde;o',
            'div' => 'input-control text')); ?>
        <?=$form->input(false, array(
            'options' => $grupos['funcionarios'],
            'type' => 'select',
            'id' => 'vincular-turmas-departamentos',
            'multiple' => 'multiple',
            'title' => 'Departamentos',
            'data-live-search' => 'true',
            'data-width' => '100%',
            'data-size' => '5',
            'class' => 'selectpicker vincular-departamentos',
            'label' => 'Departamentos',
            'div' => 'input-control text')); ?>
    </div>
</div>
<?php endif; ?>
<br />
<div class='row-fluid'>
    <button type="submit" class="bg-color-blueDark" id='enviar-evento'>
        Enviar
    </button>
</div>
<?=$form->end(array('label' => false, 'div' => false, 'style' => 'display:none')); ?>