<h3>
    Atividades para o dia <?=date('d/m/Y',$dia)?>
    <a class="button mini default pull-right"
        href="/calendario/imprimir_salas/<?=$dia?>"
        target="_blank">
        Imprimir
        <i class="icon-printer"></i>
    </a>
</h3>
<br />
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.selectpicker').selectpicker();
        $(".select-local").change(function() {
            var button = $(this).parent().parent().next().children('button');
            if($(this).val() != 0 && $(this).data('value') != $(this).val())
                button.fadeIn(500);
            else
                button.fadeOut(500);
        });
        $("#datepicker").datepicker({
            update: function(date) {
                this.$el.val(date).change();
                var d = date.split("/");
                var dia = parseInt((new Date(d[2], d[1] - 1, d[0])).getTime()/1000);
                $("#custom-content-reveal").trigger({
                    type: 'alocar-data',
                    dia: dia
                });
            }
        });
        $(".enviar-local").click(function() {
            var button = this;
            var atividade = $(button).data('id');
            var select = $(button).parent().prev().find('select');
            var dados = {
                data : {
                    atividade : atividade,
                    local : select.val()
                }
            };
            $(button).removeClass('bg-color-red')
                    .addClass('bg-color-green')
                    .text('Enviando');
            $.ajax({
                url : "/calendario/alocar_salas/",
                data : dados,
                type : "POST",
                dataType : "json",
                success : function(response) {
                    if(response.error == 1)
                        $(button)
                            .text('Erro ao enviar')
                            .removeClass('bg-color-green')
                            .addClass('bg-color-red');
                    else {
                        select.data('value',select.val());
                        $(button).fadeOut(500,function() {
                            $(this)
                                .text('Alocar Sala')
                                .removeClass('bg-color-green')
                                .addClass('bg-color-red');
                        });
                    }
                },
                error : function() {
                    $(button)
                        .text('Erro ao enviar')
                        .removeClass('bg-color-green')
                        .addClass('bg-color-red');
                }
            });
        });
    });
</script>
<div class="row-fluid">
    <div class="span4">
        <?=$form->input(false, array(
            'id' => 'datepicker',
            'type' => 'text',
            'label' => 'Alterar Data',
            'value' => date('d/m/Y',$dia),
            'div' => 'input-control text')); ?>
    </div>
</div>
<?php if(count($atividades) > 0) : ?>
<?php foreach($atividades as $atividade) : ?>
<div class="row-fluid">
    <label>
        <span class="strong fg-color-greenDark">
            <?=$atividade['Atividade']['nome']?>
            <?=$atividade['Usuario']['nome']?> 
            (<?=$atividade['Usuario']['grupo']?>)
        </span>
        <br />
        <span class="label label-error">
            Marcado: <?=date('d/m/Y &\a\g\r\a\v\e;\s H:i:s', strtotime($atividade['Atividade']['data_cadastro']));?>
        </span>
        <span class="label bg-color-blueDark">
            Local: <?=$atividade['Atividade']['local']?>
        </span>
        <span class="label bg-color-blueDark">
            Local: <?=$atividade['Atividade']['local']?>
        </span>
        <span class="label label-important">
            In&iacute;cio: <?=date('H:i',strtotime($atividade['Atividade']['data_inicio']))?> / 
            Fim: <?=!empty($atividade['Atividade']['data_fim']) ?
                date('H:i',strtotime($atividade['Atividade']['data_fim'])) : "n&atilde;o informado"?>
        </span>
        <span class="label label-warning">
            N&ordm; Pessoas <?=$atividade['Atividade']['numero_pessoas'] > 0 ?
                $atividade['Atividade']['numero_pessoas'] : "n&atilde;o informado"?>
        </span>
        <?php if(count($atividade['AtividadeTurma']) > 0) : ?>
        <?php foreach($atividade['AtividadeTurma'] as $turma) : ?>
        <?php endforeach; ?>
        <span class="label bg-color-blue">
            Turma <?=$turma['turma_id']?> <?=$turma['grupos']?>
        </span>
        <?php else : ?>
        <span class="label bg-color-blue">
            Nenhuma turma
        </span>
        <?php endif; ?>
    </label>
</div>
<div class="row-fluid">
    <div class="span4">
        <?=$form->input(false, array(
            'options' => $locais,
            'type' => 'select',
            'title' => 'Local',
            'value' => $atividade['Atividade']['local_id'],
            'data-value' => $atividade['Atividade']['local_id'],
            'class' => 'selectpicker select-local',
            'label' => false,
            'div' => 'input-control text')); ?>
    </div>
    <div class="span4">
        <button type="button" class="bg-color-red hide enviar-local"
            data-id="<?=$atividade['Atividade']['id']?>">
            Alocar Sala
        </button>
    </div>
</div>
<br />
<br />
<?php endforeach; ?>
<?php else : ?>
<h3 class="fg-color-red">Nenhuma atividade para esse dia</h3>
<?php endif; ?>