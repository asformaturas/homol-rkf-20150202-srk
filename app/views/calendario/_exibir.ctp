<style type="text/css">
    #conteudo { height: 1200px!important }
</style>
<link href='<?= $this->webroot ?>metro/css/fullcalendar/fullcalendar.css' rel='stylesheet' />
<link href='<?= $this->webroot ?>metro/css/fullcalendar/custom.css?v=0.1' rel='stylesheet' />
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/bootstrap/datepicker.css">
<link href='<?= $this->webroot ?>metro/css/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/bootstrap/datepicker.js"></script>
<script src='<?= $this->webroot ?>metro/js/fullcalendar/loader.js'></script>
<script src='<?= $this->webroot ?>metro/js/fullcalendar/app.js'></script>
<?=$form->hidden('usuario',array('value' => $usuario['Usuario']['id'],'id' => 'usuario_id'))?>
<?php $session->flash(); ?>
<?php if(!empty($podeFiltrar)) : ?>
<div class="row-fluid calendario-legenda">
    <?php if(!empty($podeAlocar)) : ?>
    <div class="span2 bg-color-purple alocar-salas">
        Administrar
    </div>
    <div class="span2 reunioes" data-tipo="reuniao">
        Reuni&otilde;es
    </div>
    <?php else : ?>
    <div class="span2 offset2 reunioes" data-tipo="reuniao">
        Reuni&otilde;es
    </div>
    <?php endif; ?>
    <div class="span2 eventos-as" data-tipo="interno">
        Ev Internos
    </div>
    <div class="span2 planejamento" data-tipo="evento">
        Planejamento
    </div>
    <div class="span2 outros" data-tipo="outros">
        Outros
    </div>
    <div class="span2 meus strong" data-tipo="meus">
        Meu
    </div>
</div>
<br />
<?php endif; ?>
<div class='calendar-container' id='calendar-container'>
    <div class='carregando-eventos'>
        <span>Carregando</span>
    </div>
    <div class='calendar-header'>
        <div class='calendar-nav'>
            <nav class='month'>
                <span id="custom-prev" class="custom-prev"></span>
                <span id="custom-next" class="custom-next"></span>
            </nav>
            <h2><b id='current-month'></b><small id='current-year'></small></h2>
        </div>
        <div class='calendar-options'>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            <?php if(!empty($podeFiltrar)) : ?>
            <button class="mini" type='button' id='view-nova'>
                Novo
            </button>
            <?php endif; ?>
            <button class="mini" type='button' id='view-day'>
                Dia
            </button>
            <button class="mini" type='button' id='view-week'>
                Semana
            </button>
            <button class="mini" type='button' id='view-month'>
                M&ecirc;s
            </button>
            <button class="mini" type='button' id='view-go'>
                Ir Para
            </button>
        </div>
    </div>
    <div class='calendar-content' id='calendar-content'>
        <div id='calendar'></div>
        <div id="custom-content-reveal" class="custom-content-reveal">
            <h4 id="titulo-evento"></h4>
            <span class='custom-content-close' id='fechar-evento'></span>
            <div class='row-fluid'>
                <div class="span10 offset1" id="conteudo-evento"></div>
            </div>
        </div>
    </div>
</div>