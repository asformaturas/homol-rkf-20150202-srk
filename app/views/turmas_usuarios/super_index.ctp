<span id="conteudo-titulo" class="box-com-titulo-header">Alocação de Usuários</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
	<div class="tabela-adicionar-item">
		<?php echo $form->create('Turmas', array('url' => "/{$this->params['prefix']}/turmas_usuarios/procurar", 'class' => 'procurar-form-inline')) ?>
		<?php echo $form->input('chave', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
		<?php echo $form->input('status', array('options' => $statuses, 'type' => 'select', 'class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
		<?php echo $form->end(array('label' => ' ', 'div' => false, 'class' => 'submit-busca')) ?>
		<div style="clear:both;"></div>
	</div>
	<div class="container-tabela">
		<table>
			<thead>
				<tr>
					<th scope="col"><?php echo $paginator->sort('Id', 'id'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Nome', 'nome'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Status', 'status'); ?></th>
					<th scope="col">Usuario Comercial</th>
					<th scope="col">Usuario Planejamento</th>
					<th scope="col"> &nbsp;</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="3"><?php echo $paginator->counter(array('format' => 'Itens %start% ao %end% - página %page% de %pages%')); ?>
						<span class="paginacao">
							<?php echo $paginator->numbers(array('separator' => ' ')); ?>
						</span>
					</td>
					<td colspan="1"><?php echo $paginator->counter(array('format' => 'Total : %count% ' .  $this->name)); ?></td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach ($turmas as $turma): ?>
				
				<?php 
				// Coloca nas variaveis os usuarios de comercial e planejamento alocados àquela turma
				$usuario_comercial = array('email' => '');
				$usuario_planejamento = array('email' => '');
				if(!empty($turma['Usuario']))
					foreach ($turma['Usuario'] as $usuario) {
						// ve qual é o usuario do planejamento e o usuario 
						if ($usuario['grupo'] == 'comercial') {
							$usuario_comercial = $usuario;
						} else if($usuario['grupo'] == 'planejamento') {
							$usuario_planejamento = $usuario;
						}
					}
				?>
				
				<?php if($isOdd):?>
					<tr class="odd">
				<?php else:?>
					<tr>
				<?php endif;?>
					<td  colspan="1" width="10%"><?php echo $turma['Turma']['id']; ?></td>
					<td  colspan="1" width="10%"><?php echo $turma['Turma']['nome']; ?></td>
					<td  colspan="1" width="20%"><?php echo $turma['Turma']['status']; ?></td>
					<td  colspan="1" width="15%"><?php echo $usuario_comercial['email']; ?></td>
					<td  colspan="1" width="15%"><?php echo $usuario_planejamento['email']; ?></td>
					<td  colspan="1" width="30%">
						<?php echo $html->link('Alocar Usuario', array($this->params['prefix'] => true, 'controller' => 'turmas_usuarios', 'action' =>'alocar_usuario', $turma['Turma']['id']), array('class' => 'submit button')); ?>
						<?php echo $html->link('Deletar Usuario', array($this->params['prefix'] => true, 'controller' => 'turmas_usuarios', 'action' => 'deletar', $turma['Turma']['id']), 
							array('class' => 'submit button', 'onclick' => "javacript: if(confirm('Deseja excluir o item {$turma['Turma']['id']}?')) return true; else return false;")); ?>
					</td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>