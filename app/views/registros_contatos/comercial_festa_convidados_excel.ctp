<?php

$listaDeFormandosParaExcel = array();

foreach ($festa['RegistroContatoUsuario'] as $usuario)
    $listaDeFormandosParaExcel[] = array(
        "Turma ID" => $usuario['Usuario']['turma_id'],
        "Turma" => $usuario['Usuario']['turma_nome'],
        "Nome" => $usuario['Usuario']['nome'],
        "RG" => $usuario['Usuario']['rg'],
        "Data Convite" => date('d/m/Y',strtotime($usuario['data_cadastro'])),
        "Consultor" => $usuario['Consultor']['nome'],
        "Confirmou" => $usuario['confirmou_presenca'] == 1 ? 'Sim' : 'Nao',
        "Compareceu" => $usuario['compareceu'] == 1 ? 'Sim' : 'Nao'
    );
$excel->generate($listaDeFormandosParaExcel, "{$festa['RegistrosContato']['id']}_lista_convidados_festa_".date('Y_m_d'));
?>