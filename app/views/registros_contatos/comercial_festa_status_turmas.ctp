<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload"
                data-bind="click: reload"></a>
            <a class="metro-button back" data-bind="click: loadThis"
                href='<?="/{$this->params['prefix']}/registros_contatos/festas/"?>'></a>
            Turmas da Festa - <?=$festa['RegistrosContato']['assunto']?>
        </h2>
    </div>
</div>
<div class="row-fluid">
    <?php if (sizeof($turmas) == 0) { ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#table-status').tooltip({selector: '[rel=tooltip]'});
    });
</script>
    <h3 class='fg-color-red'>Nenhum Registro de Festa</h3>
    <?php } else { ?>
    <table class="table table-condensed table-striped" id="table-status">
        <thead>
            <tr>
                <th scope="col" width="6%">ID</th>
                <th scope="col" width="25%">Nome</th>
                <th scope="col" width="25%">Consultores</th>
                <th scope="col" width="8%">Convidados</th>
                <th scope="col" width="8%">Confirm</th>
                <th scope="col" width="8%">Comparec</th>
                <th scope="col" width="20%">Status</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($turmas as $turma): ?>
            <tr>
                <td><?=$turma['Turma']['id']?></td>
                <td><?=$turma['Turma']['nome']?></td>
                <td>
                    <div rel="tooltip" data-html="true" data-placement="left"
                        title="<?=$turma[0]['consultores']?>">
                        <?=substr($turma[0]['consultores'],0,30)?>...
                    </div>
                </td>
                <td><?=$turma[0]['convidados']?></td>
                <td><?=$turma[0]['confirmaram']?></td>
                <td><?=$turma[0]['compareceram']?></td>
                <td>
                    <?php if($turma[0]['fechamento_antes_festa'] == 1) : ?>
                    Fechada Antes da Festa
                    <?php else : ?>
                    <?=ucfirst($turma['Turma']['status']); ?>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <?php } ?>
</div>