<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/chosen.css">
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/bootstrap/datepicker.css">
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/max/form_validate.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/chosen.js"></script>
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/bootstrap/datepicker.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/jquery-validate.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".chosen").chosen({width:'100%'});
        $('.datepicker').datepicker();
        $("#formulario-registro").submit(function(e) {
            e.preventDefault();
        });
        $('#formulario-registro').validate({
            sendForm : true,
            eachValidField : function() {
                $(this).removeClass('error').removeClass('form-error').addClass('success');
                var label = $('label[for="'+$(this).attr('id')+'"]');
                if(label.length > 0) {
                    if(label.children('span').length > 0)
                        label.children('span').fadeOut(500,function() { $(this).remove()});
                }
            },
            eachInvalidField : function() {
                $(this).removeClass('success').addClass('error');
            },
            description: {
                notEmpty : {
                    required : function() {
                        var label = $('label[for="'+$(this).attr('id')+'"]');
                        if(label.length > 0) {
                            if(label.children('span').length > 0)
                                label.children('span').html('').attr('class','fg-color-red');
                            else
                                label.append($('<span>',{class:'fg-color-red'}));
                            mensagem = $(this).data('error') ||
                                'Complete o campo';
                            label.children('span').html(mensagem);
                        }
                        return '';
                    }
                }
            },
            valid: function() {
                var button = $('.modal-footer').find(':contains("Salva")');
                button.remove();
                var context = ko.contextFor($("#content-body")[0]);
                var participantesInserir = $('#participantes-comissao').chosen().val();
                var participantesRemover = [];
                $('#participantes-comissao option').each(function() {
                    if($(this).attr('selected') != undefined) {
                        if($.inArray($(this).val(),participantesInserir) < 0)
                            participantesRemover.push($(this).val());
                        else
                            participantesInserir.splice($.inArray($(this).val(),participantesInserir),1);
                    }
                });
                $('#participantes-comissao').remove();
                for(var i in participantesInserir) {
                    $("<input>",{
                        type:'hidden',
                        name:'data[RegistroContatoUsuario]['+i+'][usuario_id]',
                        value:participantesInserir[i]
                    }).appendTo($("#formulario-registro"));
                    $("<input>",{
                        type:'hidden',
                        name:'data[RegistroContatoUsuario]['+i+'][consultor_id]',
                        value:<?=$usuario['Usuario']['id']?>
                    }).appendTo($("#formulario-registro"));
                    $("<input>",{
                        type:'hidden',
                        name:'data[RegistroContatoUsuario]['+i+'][data_cadastro]',
                        value:'<?=date('Y-m-d H:i:s')?>'
                    }).appendTo($("#formulario-registro"));
                }
                for(var i in participantesRemover)
                    $("<input>",{
                        type:'hidden',
                        name:'data[participantesRemovidos][]',
                        value:participantesRemover[i]
                    }).appendTo($("#formulario-registro"));
                var dados = $("#formulario-registro").serialize();
                var url = $("#formulario-registro").attr('action');
                bootbox.hideAll();
                context.$data.showLoading(function() {
                    $.ajax({
                        url : url,
                        data : dados,
                        type : "POST",
                        dataType : "json",
                        complete : function() {
                            context.$data.reload();
                        }
                    });
                });
            },
            invalid: function() {
                return false;
            }
        });
    })
</script>
<?=$form->create('RegistrosContato', array(
    'url' => "/{$this->params['prefix']}/registros_contatos/editar",
    'id' => 'formulario-registro'
)); ?>
<?php
if($contatoId)
    echo $form->hidden('RegistrosContato.id');
?>
<div class="row-fluid">
    <div class="span11">
        <label class="required" for="assunto">Assunto</label>
        <?=$form->input('assunto',array(
            'label' => false,
            'id' => 'assunto',
            'data-description' => 'notEmpty',
            'data-describedby' => 'assunto',
            'div' => 'input-control text',
            'data-required' => 'true',
            'error' => false)); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span3">
        <label class="required">Data</label>
        <?=$form->input('data-hora',array(
            'label' => false,
            'class' => 'datepicker',
            'div' => 'input-control text',
            'data-required' => 'true',
            'error' => false)); ?>
    </div>
    <div class="span4">
        <?=$form->input('tipo', array(
            'options' => $tipo_select,
            'type' => 'select',
            'class' => 'chosen',
            'label' => 'Tipo',
            'div' => false)); ?>
    </div>
    <div class="span4">
        <label>Duração (Min)</label>
        <?=$form->input('duracao',array(
            'label' => false,
            'div' => 'input-control text',
            'error' => false)); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span11">
        <?=$form->input('membros_comissao', array(
            'options' => $comissao,
            'selected' => $selecionados,
            'multiple' => 'multiple',
            'id' => 'participantes-comissao',
            'data-placeholder' => 'Selecione',
            'type' => 'select',
            'class' => 'chosen',
            'label' => 'Participantes da Comissão',
            'div' => false)); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span5">
        <label>Participantes da AS</label>
        <?=$form->input('funcionarios_as',array(
            'label' => false,
            'rows' => '2',
            'div' => 'input-control textarea',
            'error' => false)); ?>
    </div>
    <div class="span6">
        <label>Detalhes</label>
        <?=$form->input('detalhes',array(
            'label' => false,
            'rows' => '2',
            'div' => 'input-control textarea',
            'error' => false)); ?>
    </div>
</div>
<?=$form->end(array('label' => false, 'div' => false, 'style' => 'display:none')); ?>