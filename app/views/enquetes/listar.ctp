<div class="row-fluid">
    <h2>
        <a class="metro-button reload" data-bind="click: reload"></a>
        Enquetes
        <a href="/enquetes/editar" class="button mini default pull-right"
            data-bind="click: loadThis">Nova Enquete</a>
    </h2>
</div>
<?php $session->flash(); ?>
<div class="row-fluid">
    <?php if (sizeof($enquetes) > 0) : ?>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="50%">Descri&ccedil;&atilde;o</th>
                <th scope="col" width="10%">In&iacute;cio</th>
                <th scope="col" width="20%">Turmas</th>
                <th scope="col" width="5%">Ativa</th>
                <th scope="col" width="5%">Perguntas</th>
                <th scope="col" width="10%"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($enquetes as $enquete) : ?>
            <tr>
                <td><?=$enquete['Enquete']['texto'] ?></td>
                <td><?=date('d/m/Y',strtotime($enquete['Enquete']['data_inicio'])); ?></td>
                <td>
                    <?php if($enquete['Enquete']['turmas'] == 'selecionadas' && count($enquete['EnqueteTurma']) > 0 ) : ?>
                    <?php $turmas = array(); foreach($enquete['EnqueteTurma'] as $turma) : $turmas[] = $turma['turma_id']; endforeach;?>
                    <?=implode(", ",$turmas)?>
                    <?php else : ?>
                    Todas
                    <?php endif; ?>
                </td>
                <td><?=$enquete['Enquete']['ativa'] == 1 ? "Sim" : "Não"?></td>
                <td><?=count($enquete['EnquetePergunta'])?></td>
                <td style="text-align:center">
                    <a href="/enquetes/editar/<?=$enquete['Enquete']['id']?>"
                       class="button mini default"
                        data-bind="click: loadThis">
                        Editar
                    </a>
                    <a href="/enquetes/respostas/<?=$enquete['Enquete']['id']?>"
                       class="button mini bg-color-blue"
                        data-bind="click: loadThis">
                        Respostas
                    </a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php endif; ?>
</div>