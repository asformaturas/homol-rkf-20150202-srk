<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootstrap/select.css">
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/bootstrap/datepicker.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/datepicker.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/jquery-ui-1.10.3.custom.min.js"></script>
<style type="text/css">
textarea {
    resize: none;
    min-height: initial!important;
}
.duvida:after {
    font-family: 'IcoMoon';
    -webkit-font-smoothing: antialiased;
    margin-left:4px;
    width:12px;
    height:12px;
    font-weight: normal;
    content: "\e035";
    speak: none;
    font-size:12px;
    line-height: 12px;
    vertical-align:0%;
    color:#666666;
}
.image-button { width:100%; padding-right:0; text-align: left; }
.image-button input {
    height:auto;
    min-height: initial;
    width:90%;
    margin:0;
    padding:0;
    line-height: 1em;
    font-size: 12px;
    font-weight: 200;
    background: transparent;
    border: none;
}
.image-button .action { position: absolute; top:0; right:0; min-width:35px;
    width: 35px; height: 32px; text-align: center }
.image-button .action i { margin-left: 0!important; position:relative; top:12px }
.image-button .action:hover { background: #333333!important }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        
        var context = ko.contextFor($("#content-body")[0]);
        var turmasCadastradas = <?=isset($turmasCadastradas) ? json_encode($turmasCadastradas) : json_encode(array())?>;
        $(".datepicker").datepicker();  
        $('.selectpicker').selectpicker({width:'100%'});
        $('#content-body').tooltip({ selector: '[rel=tooltip]'});
        
        $("#turmas").change(function(e) {
            if($(this).val() == "selecionadas")
                $("#selecionar-turmas").fadeIn(500,function() {
                    context.$data.loaded(true);
                });
            else
                $("#selecionar-turmas").fadeOut(500,function() {
                    context.$data.loaded(true);
                });
        });
        
        if($("#EnqueteGrupos").val() != "")
            $("#grupos").selectpicker('val',$("#EnqueteGrupos").val().split(','));
        
        $("#turmas-selecionadas").selectpicker('val',Object.keys(turmasCadastradas));
        $("#turmas").trigger('change');
        
        $("#criar-pergunta").click(function(e) {
            $("#criar-pergunta,#perguntas").fadeOut(500,function() {
                $("#nova-pergunta").fadeIn(500,function() {
                    context.$data.loaded(true);
                });
            });
        });
        
        $("#confirmar-nova-pergunta").click(function() {
            if($("#pergunta").val() == "") {
                alert('Digite a pergunta');
            } else {
                adicionarPergunta($("#pergunta").val());
                $("#cancelar-nova-pergunta").trigger('click');
            }
        });
        
        $("#cancelar-nova-pergunta").click(function() {
            $("#nova-pergunta").fadeOut(500,function() {
                $("#pergunta").val('');
                $("#criar-pergunta,#perguntas").fadeIn(500,function() {
                    context.$data.loaded(true);
                });
            });
        });
        
        $("#perguntas").on('click','.editar-pergunta',function() {
            var pergunta = $(this).parents('.pergunta');
            exibirAlternativas(pergunta);
        });
        
        $("#enviar-enquete").click(function() {
            if($("#data_inicio").val() == "") {
                alert('Selecione a data de início');
            } else if(!$("#grupos").val()) {
                alert('Selecione algum grupo');
            } else if($('#turmas').val() == "selecionadas" && !$("#turmas-selecionadas").val()) {
                alert('Selecione as turmas');
            } else if($(".pergunta").length == 0) {
                alert('A enquete precisa ter no mínimo uma pergunta');
            } else {
                var valido = true,
                    input = $("<div>");
                $(".pergunta").each(function(p,pergunta) {
                    if($(pergunta).find('.input-pergunta').val() == "") {
                        alert('Complete o texto de todas as perguntas');
                        valido = false;
                        return false;
                    }
                    if($(pergunta).find('.alternativa').length < 2) {
                        alert('As perguntas precisam ter no mínimo 2 alternativas');
                        valido = false;
                        return false;
                    }
                    $("<input>",{
                        type : 'hidden',
                        name : 'data[EnquetePergunta]['+p+'][texto]',
                        value : $(pergunta).find('.input-pergunta').val()
                    }).appendTo(input);
                    $("<input>",{
                        type : 'hidden',
                        name : 'data[EnquetePergunta]['+p+'][quantidade_escolhas]',
                        value : $(pergunta).find('.quantidade-escolhas').val()
                    }).appendTo(input);
                    $("<input>",{
                        type : 'hidden',
                        name : 'data[EnquetePergunta]['+p+'][ordem]',
                        value : (p+1)
                    }).appendTo(input);
                    if($(pergunta).data('id'))
                        $("<input>",{
                            type : 'hidden',
                            name : 'data[EnquetePergunta]['+p+'][id]',
                            value : $(pergunta).data('id')
                        }).appendTo(input);
                    $(pergunta).find('.alternativa').each(function(a,alternativa) {
                        $("<input>",{
                            type : 'hidden',
                            name : 'data[EnquetePergunta]['+p+'][EnqueteAlternativa]['+a+'][texto]',
                            value : $(alternativa).find('.input-alternativa').val()
                        }).appendTo(input);
                        $("<input>",{
                            type : 'hidden',
                            name : 'data[EnquetePergunta]['+p+'][EnqueteAlternativa]['+a+'][ordem]',
                            value : (a+1)
                        }).appendTo(input);
                        if($(alternativa).data('id'))
                            $("<input>",{
                                type : 'hidden',
                                name : 'data[EnquetePergunta]['+p+'][EnqueteAlternativa]['+a+'][id]',
                                value : $(alternativa).data('id')
                            }).appendTo(input);
                        if($(alternativa).attr('data-ativa'))
                            $("<input>",{
                                type : 'hidden',
                                name : 'data[EnquetePergunta]['+p+'][EnqueteAlternativa]['+a+'][ativa]',
                                value : $(alternativa).attr('data-ativa')
                            }).appendTo(input);
                        if($(alternativa).attr('data-ir-para'))
                            $("<input>",{
                                type : 'hidden',
                                name : 'data[EnquetePergunta]['+p+'][EnqueteAlternativa]['+a+'][ir_para]',
                                value : $(alternativa).attr('data-ir-para')
                            }).appendTo(input);
                        else
                            $("<input>",{
                                type : 'hidden',
                                name : 'data[EnquetePergunta]['+p+'][EnqueteAlternativa]['+a+'][ir_para]',
                                value : null
                            }).appendTo(input);
                    });
                });
                if(valido) {
                    var turmas = $('#turmas').val() == "selecionadas" ? $("#turmas-selecionadas").val() : [],
                        listaTurmas = Object.keys(turmasCadastradas);
                    $.each(turmas,function(i,turma) {
                        if(!turmasCadastradas[turma])
                            $("<input>",{
                                type : 'hidden',
                                name : 'data[EnqueteTurma][][turma_id]',
                                value : turma
                            }).appendTo(input);
                    });
                    $("<input>",{
                        type : 'hidden',
                        name : 'data[Enquete][grupos]',
                        value : $("#grupos").val().join(',')
                    }).appendTo(input);
                    $("#grupos").remove();
                    $.each(turmasCadastradas,function(turma,i) {
                        if($.inArray(turma,turmas) < 0)
                            $("<input>",{
                                type : 'hidden',
                                name : 'data[remover_turmas][]',
                                value : i
                            }).appendTo(input);
                    });
                    $("#turmas-selecionadas").remove();
                    $("#perguntas").remove();
                    input.appendTo($("#form"));
                    var dados = $("#form").serialize(),
                        url = $("#form").attr('action');
                    $.ajax({
                        url : url,
                        data : dados,
                        type : "POST",
                        dataType : "json",
                        complete : function() {
                            context.$data.page('/enquetes/listar');
                        }
                    });
                }
            }
        });
        
        $("#perguntas").sortable({
            containment : 'parent',
            connectWith : '.pergunta'
        });
        
        window.adicionarPergunta = function(pergunta) {
            var div = $("<div>",{
                class : 'row-fluid pergunta'
            });
            var button = $("<a>",{
                href : 'javascript:void(0)',
                class : 'button image-button'
            }).append($("<input>",{
                type : 'text',
                class : 'input-pergunta',
                value : pergunta
            }));
            button.append($("<span>",{
                class : 'action editar-pergunta bg-color-blue',
                html : "<i class='icon-pencil'></i>"
            }));
            var alternativas = $("<div>",{
                class : 'row-fluid hide alternativas'
            });
            div.append(button);
            div.append(alternativas);
            $("#perguntas").append(div);
        };
        
        function exibirAlternativas(pergunta) {
            var div = $("#alternativas").clone();
            div.removeAttr('id');
            div.find('.texto-pergunta').prepend(pergunta.find('.input-pergunta').val());
            div.find('.alternativas').html(pergunta.find('.alternativas').html());
            if(pergunta.data('id'))
                div.find('.texto-pergunta').attr('data-id',pergunta.data('id'));
            if(div.find('.alternativa').length > 1)
                div.find('.criar-pontuacao').hide();
            else
                div.find('.criar-pontuacao').show();
            div.find('.alternativa').each(function() {
                if($(this).attr('data-ir-para')) {
                    var o = $(".pergunta[data-id="+$(this).attr('data-ir-para')+"]").index();
                    $(this).find('.ir-para-pergunta').text("Ir para " + (o+1));
                }
            });
            boot = bootbox.dialog(div.html(),[{
                label: 'Confirmar',
                'class': 'bg-color-green',
                callback : function() {
                    if($(".quantidade-escolhas:visible").val() <= 0) {
                        alert('Digite uma quantidade de escolhas válida');
                        $(".quantidade-escolhas:visible").focus();
                        return false;
                    }
                    var vazio = false;
                    $(".alternativas:visible").find(".input-alternativa").each(function() {
                        if($(this).val() == "") {
                            vazio = true;
                            return false;
                        } else {
                            $(this).attr('value',$(this).val());
                        }
                    });
                    if(vazio) {
                        alert('Complete o texto das alternativas');
                        return false;
                    } else {
                        pergunta.find('.alternativas').html($(".modal-body").find('.alternativas').html());
                        bootbox.hideAll();
                    }
                }
            },{
                label: 'Cancelar',
                'class': 'bg-color-red',
                callback : function() {
                    bootbox.hideAll();
                }
            }]);
            boot.on('shown',function() {
                $(".alternativas:visible").sortable({
                    containment : 'parent',
                    connectWith : '.alternativa'
                });
                $(".criar-alternativa").click(function() {
                    adicionarAlternativa("");
                });
                $('.alternativas').on('click','.remover-alternativa',function() {
                    removerAlternativa($(this).parents('.alternativa'));
                });
                $(".status-alternativa").click(function() {
                    statusAlternativa($(this).parents('.alternativa'));
                });
                $(".ir-para-pergunta").click(function() {
                    alternativaIrParaPergunta($(this).parents('.alternativa'));
                });
                $(".quantidade-escolhas").change(function() {
                    $(this).attr('value',$(this).val());
                });
                $(".criar-pontuacao").click(function() {
                    var div = $(this).parents('.cabecalho-alternativas');
                    div.find('.tipo-alternativas').fadeOut(500,function() {
                        div.next('.div-pontuacao').fadeIn(500);
                    });
                });
                $(".cancelar-pontuacao").click(function() {
                    var div = $(this).parents('.div-pontuacao');
                    div.fadeOut(500,function() {
                        div.prev('.cabecalho-alternativas').find('.tipo-alternativas').fadeIn(500);
                    });
                });
                $(".confirmar-pontuacao").click(function() {
                    var div = $(this).parents('.div-pontuacao');
                    var de = div.find('.pontuacao-de'),
                        ate = div.find('.pontuacao-ate');
                    if(de.val() == "" || ate.val() == "" || isNaN(de.val()) || isNaN(ate.val())) {
                        alert('Valores inválidos');
                    } else if((parseInt(ate.val())-parseInt(de.val())) > 10) {
                        alert('A diferença entre a maior pontuação e a menor não deve ser maior que 9');
                    } else {
                        for(var i = parseInt(ate.val()); i >= parseInt(de.val()); i--)
                            adicionarAlternativa(i);
                        $(".criar-pontuacao").fadeOut(500,function() {
                            $(".cancelar-pontuacao").trigger('click');
                        });
                    }
                });
            });
        }
        
        function adicionarAlternativa(alternativa) {
            var div = $("<div>",{
                class : 'row-fluid alternativa'
            });
            var texto = $("<a>",{
                href : 'javascript:void(0)',
                class : 'button image-button'
            }).append($("<input>",{
                type : 'text',
                class : 'input-alternativa',
                placeholder : "Digite aqui",
                value : alternativa
            }));
            var remover = $("<button>",{
                type : 'button',
                class : 'button bg-color-darken remover-alternativa input-block-level',
                text : 'Remover'
            });
            div.append($("<div>",{
                class : 'span10'
            }).append(texto));
            div.append($("<div>",{
                class : 'span2'
            }).append(remover));
            $(".alternativas:visible").append(div);
        }
        
        function removerAlternativa(alternativa) {
            alternativa.fadeOut(500,function() {
                $(this).remove();
            });
        }
        
        function statusAlternativa(alternativa) {
            var button = alternativa.find('.status-alternativa');
            if(alternativa.attr('data-ativa') == 1) {
                button.addClass('bg-color-darken').removeClass('bg-color-greenDark').text("Inativa");
                alternativa.attr('data-ativa',0);
            } else {
                button.addClass('bg-color-greenDark').removeClass('bg-color-darken').text("Ativa");
                alternativa.attr('data-ativa',1);
            }
        }
        
        function alternativaIrParaPergunta(alternativa) {
            var perguntaId = alternativa.parents('.modal-body')
                    .find('.cabecalho-alternativas').find('.texto-pergunta').attr('data-id'),
                pergunta = $(".pergunta[data-id="+perguntaId+"]");
            var perguntas = $(".pergunta[data-id!="+perguntaId+"]");
            var novaDiv = $("<div>",{
                class : 'span2 hide'
            });
            var select = $("<select>");
            select.append($("<option>",{
                value : '',
                text : "Selecione",
                selected : "selected"
            }));
            perguntas.each(function(i,p) {
                if(i >= pergunta.index())
                    select.append($("<option>",{
                        value : $(this).attr('data-id'),
                        text : ($(this).index()+1) + " " + $(this).find(".input-pergunta").val()
                    }));
            });
            select.append($("<option>",{
                value : 'cancelar',
                text : 'Cancelar'
            })).appendTo(novaDiv);
            var div = alternativa.find('.ir-para-pergunta').parent();
            div.fadeOut(500,function() {
                alternativa.append(novaDiv);
                select.selectpicker({width:'100%',container : 'body'});
                novaDiv.fadeIn(500,function() {
                    select.change(function() {
                        if($(this).val() == "") {
                            alternativa.removeAttr('data-ir-para');
                        } else if($(this).val() == "cancelar") {
                            alternativa.removeAttr('data-ir-para');
                            novaDiv.fadeOut(500,function() {
                                novaDiv.remove();
                                div.fadeIn(500);
                            });
                        } else {
                            alternativa.attr('data-ir-para',$(this).val());
                            var o = $(".pergunta[data-id="+$(this).val()+"]").index();
                            div.find('.ir-para-pergunta').text("Ir para " + (o+1));
                            novaDiv.fadeOut(500,function() {
                                novaDiv.remove();
                                div.fadeIn(500);
                            });
                        }
                    });
                });
            });
        }
    });
</script>
<div class="row-fluid">
    <h2>
        <a class="metro-button reload" data-bind="click: reload"></a>
        <?=!$enqueteId ? "Nova" : "Editar" ?> Enquete
        <a href="/enquetes/listar" class="button mini default pull-right"
            data-bind="click: loadThis">Voltar</a>
    </h2>
</div>
<?php $session->flash(); ?>
<?=$form->create('Enquete',
    array(
        'url' => "/enquetes/editar/",
        'id' => 'form'
    )); ?>
<?=$enqueteId ? $form->hidden('Enquete.id') : ""; ?>
<?=$form->hidden('Enquete.usuario_id'); ?>
<?=$form->hidden('Enquete.grupos'); ?>
<div class="row-fluid">
    <?=$form->input('Enquete.texto',array(
        'type' => 'textarea',
        'label' => 'Texto Abertura',
        'rows' => 6,
        'div' => 'input-control textarea')); ?>
</div>
<div class="row-fluid">
    <?=$form->input('Enquete.texto_pergunta',array(
        'type' => 'textarea',
        'label' => array(
            'rel' => 'tooltip',
            'text' => 'Texto Perguntas',
            'data-placement' => 'right',
            'class' => 'duvida',
            'style' => 'display:inline-block',
            'title' => "Não é obrigatório"
        ),
        'rows' => 3,
        'div' => 'input-control textarea')); ?>
</div>
<div class="row-fluid">
    <div class="span4">
        <?=$form->input('Enquete.periodo',array(
            'options' => $periodo,
            'type' => 'select',
            'class' => 'selectpicker',
            'data-width' => '100%',
            'label' => 'Quantas vezes',
            'div' => 'input-control text')); ?>
    </div>
    <div class="span4">
        <?=$form->input('Enquete.grupo',array(
            'options' => $grupos,
            'type' => 'select',
            'multiple' => 'multiple',
            'id' => 'grupos',
            'empty' => "Selecione",
            'class' => 'selectpicker',
            'data-width' => '100%',
            'label' => 'Grupos',
            'div' => 'input-control text')); ?>
    </div>
    <div class="span4">
        <?=$form->input('Enquete.turmas',array(
            'options' => $turmas,
            'type' => 'select',
            'id' => 'turmas',
            'class' => 'selectpicker',
            'data-width' => '100%',
            'label' => 'Turmas',
            'div' => 'input-control text')); ?>
    </div>
</div>
<div class="row-fluid hide" id="selecionar-turmas">
    <?=$form->input('turmas_id',array(
        'options' => $listaTurmas,
        'empty' => "Selecione",
        'type' => 'select',
        'multiple' => 'multiple',
        'id' => 'turmas-selecionadas',
        'class' => 'selectpicker',
        'data-width' => '100%',
        'data-size' => '5',
        'data-selected-text-format' => 'count>2',
        'data-count-selected-text' => '{0} turmas selecionadas',
        'data-live-search' => 'true',
        'label' => 'Selecionar as turmas',
        'div' => 'input-control text')); ?>
</div>
<div class="row-fluid">
    <div class='span4'>
        <?=$form->input('Enquete.data_inicio', array(
            'label' => 'Início',
            'readonly' => 'readonly',
            'type' => 'text',
            'data-required' => 'true',
            'class' => 'datepicker input-block-level',
            'id' => 'data_inicio',
            'error' => false,
            'div' => 'input-control text required'));
        ?>
    </div>
    <div class='span4'>
        <?=$form->input('Enquete.data_fim', array(
            'label' => array(
                'rel' => 'tooltip',
                'text' => 'Fim',
                'data-placement' => 'left',
                'class' => 'duvida',
                'title' => "Não é obrigatório"
            ),
            'readonly' => 'readonly',
            'type' => 'text',
            'class' => 'datepicker input-block-level',
            'error' => false,
            'div' => 'input-control text'));
        ?>
    </div>
    <div class="span4">
        <?=$form->input('Enquete.obrigatoria',array(
            'options' => $obrigatoria,
            'type' => 'select',
            'id' => 'obrigatoria',
            'class' => 'selectpicker',
            'data-width' => '100%',
            'label' => 'Obrigatória?',
            'div' => 'input-control text')); ?>
    </div>
    <?php if($enqueteId) : ?>
    <div class="span4">
        <?=$form->input('Enquete.ativa',array(
            'options' => $ativa,
            'type' => 'select',
            'class' => 'selectpicker',
            'data-width' => '100%',
            'label' => 'Ativa',
            'div' => 'input-control text')); ?>
    </div>
    <?php endif; ?>
</div>
<h2>
    Perguntas
    <button class="button mini bg-color-blueDark pull-right"
        id="criar-pergunta" type="button">
        Nova Pergunta
    </button>
</h2>
<br />
<div class="row-fluid hide" id="nova-pergunta">
    <div class="span8">
        <?=$form->input('nova_pergunta',array(
            'label' => 'Pergunta',
            'type' => 'textarea',
            'rows' => 3,
            'id' => "pergunta",
            'div' => 'input-control text')); ?>
    </div>
    <div class="span2">
        <label>&nbsp;</label>
        <button type="button" id="confirmar-nova-pergunta"
            class="button input-block-level bg-color-greenDark">
            OK
        </button>
    </div>
    <div class="span2">
        <label>&nbsp;</label>
        <button type="button" id="cancelar-nova-pergunta"
            class="button input-block-level bg-color-red">
            Cancelar
        </button>
    </div>
</div>
<div class="row-fluid">
    <div class="row-fluid" id="perguntas">
        <?php foreach($this->data['EnquetePergunta'] as $pergunta) : ?>
        <div class="row-fluid pergunta" data-id="<?=$pergunta['id']?>">
            <a class="button image-button" href="javascript:void(0)">
                <input type="text" class="input-pergunta" value="<?=$pergunta['texto']?>" />
                <span class="action editar-pergunta bg-color-blue">
                    <i class='icon-pencil'></i>
                </span>
            </a>
            <div class="row-fluid hide alternativas">
                <div class="row-fluid">
                    <br />
                    <div class='input-control text'>
                        Quantidade de escolhas 
                        <input type="number" size="5" value="<?=$pergunta['quantidade_escolhas']?>"
                            class="quantidade-escolhas"
                            style="width:50px; margin-left: 10px; vertical-align: baseline" />
                    </div>
                </div>
                <?php foreach($pergunta['EnqueteAlternativa'] as $alternativa) : ?>
                <div class="row-fluid alternativa" data-ativa="<?=$alternativa['ativa']?>"
                    <?=!empty($alternativa['ir_para']) ? "data-ir-para='{$alternativa['ir_para']}' " : ""?>
                    data-id="<?=$alternativa['id']?>">
                    <div class="span10">
                        <a class="button image-button" href="javascript:void(0)">
                            <input type="text" class="input-alternativa" value="<?=$alternativa['texto']?>" />
                        </a>
                    </div>
                    <!--
                    <div class="span2">
                        <button class="bg-color-<?=$alternativa['ativa'] == 1 ? "greenDark" : "darken"?> 
                            status-alternativa input-block-level">
                            <?=$alternativa['ativa'] == 1 ? "Ativa" : "Inativa"?>
                        </button>
                    </div>
                    -->
                    <div class="span2 <?=$alternativa['ativa'] == 1 ? "" : "hide"?>">
                        <button class="bg-color-orange ir-para-pergunta input-block-level">
                            Ir para...
                        </button>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>
<br />
<div class="row-fluid">
    <div class="span8">
        <button type="button" class="button bg-color-greenDark input-block-level"
            id="enviar-enquete">
            Enviar
        </button>
    </div>
</div>
<div class="hide" id="alternativas">
    <div class="row-fluid cabecalho-alternativas">
        <div class="span10">
            <h2 class="texto-pergunta"></h2>
        </div>
        <div class="span2 tipo-alternativas">
            <button class="button mini bg-color-blueDark criar-alternativa pull-right"
                type="button">
                Nova Alternativa
            </button>
            <br />
            <button class="button mini bg-color-purple criar-pontuacao pull-right"
                type="button">
                Pontua&ccedil;&atilde;o
            </button>
        </div>
    </div>
    <div class="row-fluid hide div-pontuacao">
        <div class="span2">
            <label>&nbsp;</label>
            <div class='input-control text'>
                <input type="number" class="input-block-level pontuacao-de" size="1"
                    placeholder="De" />
            </div>
        </div>
        <div class="span2">
            <label>&nbsp;</label>
            <div class='input-control text'>
                <input type="number" class="input-block-level pontuacao-ate" size="1"
                    placeholder="Até" />
            </div>
        </div>
        <div class="span2">
            <label>&nbsp;</label>
            <button type="button"
                class="button input-block-level bg-color-greenDark confirmar-pontuacao">
                OK
            </button>
        </div>
        <div class="span2">
            <label>&nbsp;</label>
            <button type="button"
                class="button input-block-level bg-color-red cancelar-pontuacao">
                Cancelar
            </button>
        </div>
    </div>
    <div class="row-fluid">
        <br />
        <div class="row-fluid alternativas"></div>
    </div>
</div>
<?=$form->end(array('label' => false, 'div' => false, 'class' => 'hide')); ?>