<div class="row-fluid">
    <h2>
        <a class="metro-button reload" data-bind="click: reload"></a>
        Enquetes Não Respondidas
    </h2>
</div>
<?php if (sizeof($enquetes) > 0) : ?>
<script type="text/javascript">
    $(document).ready(function() {
        var enquetes = <?=json_encode($enquetes); ?>;
        $(".enquete").click(function(e) {
            e.preventDefault();
            var id = $(this).data('id'), enquete = null;
            $.each(enquetes,function(i,e) {
                if(id == e['Enquete']['id']) {
                    enquete = e;
                    return false;
                }
            });
            if(enquete)
                exibirEnquete(enquete);
        });
    });
</script>
<div class="row-fluid">
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="70%">Descri&ccedil;&atilde;o</th>
                <th scope="col" width="15%">Perguntas</th>
                <th scope="col" width="15%"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($enquetes as $enquete) : ?>
            <tr>
                <td><?=$enquete['Enquete']['texto'] ?></td>
                <td><?=count($enquete['EnquetePergunta'])?></td>
                <td style="text-align:center">
                    <a href="javascript:"
                        class="button mini default enquete"
                        data-id="<?=$enquete['Enquete']['id']?>">
                        Responder
                    </a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php else : ?>
<h2 class="fg-color-blue">Nenhuma enquete encontrada</h2>
<?php endif; ?>