<script type="text/javascript">
    $(document).ready(function() {
        //$.Input();
        $("body").on('fbinit',function() {
            $("#facebook-login").fadeIn(500,function(e) {
                $("#facebook-login").click(function(e) {
                    e.preventDefault();
                    $(this).attr('disabled','disabled');
                    loginFacebook();
                });
            });
        });
        
        $("body").on('conta-logada',function() {
            loginFacebook();
        });
        
        function loginFacebook() {
            if($("#facebook-login").attr('disabled') == "disabled") {
                FB.api('/me', function(response) {
                    if(response.id) {
                        $.ajax({
                            type : 'POST',
                            url : '/usuarios/verificar_facebook',
                            data : {
                                data : {
                                    facebook : response
                                }
                            },
                            success : function(r) {
                                bootbox.dialog(r);
                            },
                            error : function() {

                            }
                        });
                    } else {
                        FB.login(function(r) {
                            $("#facebook-login").removeAttr('disabled');
                        },{
                            scope: 'email,user_likes'
                        });
                    }
                });
            }
        }
    });
</script>
<div class="row-fluid">
    <div class="bg-color-orange alert strong">RK Formaturas</div>
</div>
<?php
$session->flash();
$session->flash('auth');
?>
<?= $form->create('Usuario', array('action' => 'login')); ?>
<div class="row-fluid">
    <label>Email</label>
    <?=$form->input('email', array('label' => false, 'div' => 'input-control text', 'tabindex' => '1',
        'after' => '<button class="helper" onclick="return false" type="button"></button>'));?>
    <label>Senha</label>
    <small><?= $html->link('Esqueceu sua senha?', 'recuperar_senha', array('class' => 'btn btn-link fg-color-red', 'style' => 'font-size:11px', 'tabindex' => '4',));
    ?></small>
    <?= $form->input('senha', array('label' => false, 'div' => 'input-control password', 'type' => 'password', 'tabindex' => '1',
        'after' => '<button class="helper" onclick="return false" type="button"></button>'));
    ?>
</div>
<div class="row-fluid">
    <div class="span6">
        <button type="button" class="button bg-color-blue fg-color-white hide"
            id="facebook-login">
            <i class="icon-facebook"></i>
            Login com Facebook
        </button>
    </div>
    <div class="span6">
        <button class="default pull-right bg-color-orange" tabindex="3" type="submit">Login</button>
    </div>
</div>
<?= $form->end(array('label' => false, 'div' => false, 'class' => 'hide')); ?>