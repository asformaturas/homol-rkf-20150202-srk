<?= $form->create('Usuario', array('action' => 'login')); ?>
<div class="row-fluid">
    <div class="span4">
        <label>Email</label>
        <?=$form->input('email',
                array(
                    'label' => false,
                    'div' => 'input-control text',
                    'class' => 'input-block-level'
                ));?>
    </div>
    <div class="span4">
        <label>Senha</label>
        <?=$form->input('senha',
                array(
                    'label' => false,
                    'div' => 'input-control text',
                    'type' => 'password',
                    'class' => 'input-block-level'
                ));?>
    </div>
</div>
<div class="row-fluid">
    <div class="span4">
        <button class="bg-color-red" type="submit">Login</button>
    </div>
    <div class="span4">
        <?=$html->link('Esqueceu sua senha?', 'recuperar_senha',
                array(
                    'class' => 'btn btn-link pull-right',
                    'style' => 'font-size:11px; padding:0'
                ));?>
    </div>
</div>
<?=$form->end(array('label' => false, 'div' => false, 'class' => 'hide')); ?>