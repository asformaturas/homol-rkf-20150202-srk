<span id="conteudo-titulo" class="box-com-titulo-header">Autenticação por CPF</span>
<div id="conteudo-container" style="margin-left:0; margin-top: 0;">
	<?php $session->flash(); ?>

	<?php echo $form->create('Usuario', array('action' => 'autenticacao_por_cpf')); ?>
	<br /><p style="text-align: center" >Insira seu CPF para autenticarmos sua conta.</p>

	<p class="grid_full alpha omega">
    	<label class="grid_16 alpha omega">CPF</label>
			<?php echo $form->input('cpf', array('class' => 'grid_full alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_full'))); ?>
	<p class="grid_full alpha omega">

	<?php echo $form->input('Usuario.chave_de_recuperacao', array('value'=>$chave, 'type'=>'hidden' ) ); ?>

	<p class="grid_full alpha omega">
		<?php echo $form->end(array('label' => 'Autenticar', 'div' => false, 'class' => 'submit'));?>
	</p>
</div>
