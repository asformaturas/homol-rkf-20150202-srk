<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('.selectpicker').selectpicker({width:'100%'});
    
    $("#form").submit(function(e) {
        e.preventDefault();
        var context = ko.contextFor($(".metro-button.reload")[0]);
        var dados = $("#form").serialize();
        var url = $("#form").attr('action');
        bootbox.hideAll();
        context.$data.showLoading(function() {
            $.ajax({
                url: url,
                data: dados,
                type: "POST",
                dataType: "json",
                complete: function() {
                    context.$data.reload();
                }
            });
        });
    });
});
</script>
<?=$form->create('Usuario', array(
    'url' => "/{$this->params['prefix']}/usuarios/alterar/{$usuario['Usuario']['id']}",
    'id' => 'form')); ?>
<?=$form->hidden('Usuario.id'); ?>
<div class="row-fluid">
    <div class="row-fluid">
        <div class="span6">
            <?=$form->input('nome',
                array('label' => 'Nome', 'div' => 'input-control mini text',
                    'error' => false,'type' => 'text')); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span6">
            <?=$form->input('email',
                array('label' => 'Email', 'div' => 'input-control mini text',
                    'error' => false,'type' => 'text')); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span3">
            <?=$form->input('grupo', 
                array('options' => $grupos, 'type' => 'select', 
                    'class' => 'selectpicker', 'label' => 'Grupo', 'div' => false)); ?>
        </div>
        <div class="span3">
            <?=$form->input('nivel', 
                array('options' => $niveis, 'type' => 'select', 
                    'class' => 'selectpicker', 'label' => 'Nivel', 'div' => false)); ?>
        </div>
    </div>
    <br />
    <div class="row-fluid">
        <div class="span2">
            <button type='submit' class='button max bg-color-greenDark'>
                Salvar
                <i class='icon-ok'></i>
            </button>
        </div>
    </div>
</div>

