<script type="text/javascript">
	$(document).ready(function() {
		$.Input();
	});
</script>
<div class="row-fluid">
	<div class="alert alert-error strong">Efetuar login com dados antigos</div>
</div>
<?php 
	$session->flash();
?>
<?=$form->create('Usuario', array('action' => 'login_antigo')); ?>
<div class="row-fluid">
	<label>Login no sistema antigo</label>
	<?=$form->input('login_antigo', array('label' => false, 'div' => 'input-control text',
			'after' => '<button class="helper" onclick="return false" tabindex="-1" type="button"></button>')); ?>
	<label>Senha no sistema antigo</label>
	<?=$form->input('senha_antiga', array('label' => false, 'div' => 'input-control password','type' => 'password',
			'after' => '<button class="helper" onclick="return false" tabindex="-1" type="button"></button>')); ?>
</div>
<div class="row-fluid">
	<div class="span6">
            <a href="/usuarios/login" style="text-decoration: none; color: white">
                <button class="default pull-left bg-color-red" type="button">
                    Voltar
                </button>
            </a>
	</div>
	<div class="span6">
		<button class="default pull-right bg-color-red" type="submit">Login</button>
	</div>
</div>
<?=$form->end(array('label' => false, 'div' => false, 'class' => 'hide'));?>

