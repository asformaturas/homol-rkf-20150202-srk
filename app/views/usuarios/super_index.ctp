
<span id="conteudo-titulo" class="box-com-titulo-header">Usuários</span>
<div id="conteudo-container">
    <?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
    <?php $session->flash(); ?>
    <div class="tabela-adicionar-item">
        <?=$form->create('Usuarios', array('url' => "/{$this->params['prefix']}/usuarios/procurar", 'class' => 'procurar-form-inline')) ?>
        <?=$form->input('chave', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
        <?=$form->input('grupo', array('options' => $grupos, 'type' => 'select', 'class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
        <?=$form->input('nivel', array('options' => $niveis, 'type' => 'select', 'class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
        <?=$form->end(array('label' => ' ', 'div' => false, 'class' => 'submit-busca')) ?>
        <?=$html->link('Adicionar', array($this->params['prefix'] => true, 'action' => 'adicionar')); ?>
        <div style="clear:both;"></div>
    </div>
    <div class="container-tabela">
        <table>
            <thead>
                <tr>
                    <th scope="col"><?=$paginator->sort('Nome', 'nome'); ?></th>
                    <th scope="col"><?=$paginator->sort('Email', 'email'); ?></th>
                    <th scope="col"><?=$paginator->sort('Grupo', 'grupo'); ?></th>
                    <th scope="col"><?=$paginator->sort('Nível', 'nivel'); ?></th>
                    <th scope="col"> &nbsp;</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="2"><?=$paginator->counter(array('format' => 'Itens %start% ao %end% - página %page% de %pages%')); ?>
                        <span class="paginacao">
                            <?=$paginator->numbers(array('separator' => ' ')); ?>				
                        </span>
                    </td>
                    <td colspan="3"><?=$paginator->counter(array('format' => 'Total : %count% ' . $this->name)); ?></td>
                </tr>
            </tfoot>
            <tbody>
                <?php $isOdd = false; ?>
                <?php foreach ($usuarios as $usuario): ?>
                    <?php if ($isOdd): ?><tr class="odd"><?php else: ?><tr><?php endif; ?>
                        <td colspan="1" width="20%"><?=$html->link($usuario['Usuario']['nome'], array('controller' => 'Usuarios', 'action' => 'visualizar', $usuario['Usuario']['id'])); ?></td>
                        <td colspan="1" width="20%"><?=$usuario['Usuario']['email']; ?></td>
                        <td colspan="1" width="10%"><?=$usuario['Usuario']['grupo']; ?></td>
                        <td colspan="1" width="10%"><?=$usuario['Usuario']['nivel']; ?></td>
                        <td  colspan="1" width="40%">
                            <?=$html->link('Visualizar', array($this->params['prefix'] => true, 'controller' => 'Usuarios', 'action' => 'visualizar', $usuario['Usuario']['id']), array('class' => 'submit button')); ?>
                            <?=$html->link('Editar', array($this->params['prefix'] => true, 'controller' => 'Usuarios', 'action' => 'editar', $usuario['Usuario']['id']), array('class' => 'submit button')) ?>
                            <?=$html->link('Deletar', array($this->params['prefix'] => true, 'controller' => 'Usuarios', 'action' => 'deletar', $usuario['Usuario']['id']), array('class' => 'submit button', 'onclick' => "javacript: if(confirm('Deseja excluir o usuário {$usuario['Usuario']['nome']}?')) return true; else return false;"));?>
                            <?php if($usuarioLogado['Usuario']['nivel'] == 'administrador') : ?>
                            <?=$html->link('Entrar', array($this->params['prefix'] => true, 'controller' => 'Usuarios', 'action' => 'entrar', $usuario['Usuario']['id']), array('class' => 'submit button')); ?>
                            <?php endif; ?>
                            <?=$html->link('Troca de Senha',
                                    array(
                                        $this->params['prefix'] => true,
                                        'controller' => 'Usuarios',
                                        'action' => 'exigir_troca_senha', $usuario['Usuario']['id']
                                    ),
                                    array(
                                        'class' => 'submit button',
                                        'onclick' => "javacript: if(confirm('Confirma a troca de senha pelo usuário {$usuario['Usuario']['nome']}?')) return true; else return false;"));?>
                        </td>
                    </tr>
                    <?php $isOdd = !$isOdd; ?>
<?php endforeach; ?>
        </table>
    </div>
</div>