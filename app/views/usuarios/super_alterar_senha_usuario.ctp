<?php $buttonAfter = '<button class="helper" onclick="return false" '.
        'tabindex="-1" type="button"></button>'; ?>
<script type="text/javascript">
    $(document).ready(function() {
        $.Input();
        $("#form-senha").submit(function(e) {
            e.preventDefault();
            if($("#senha").val() != "" && $("#confirmar").val() != "") {
                if($("#senha").val() == $("#confirmar").val()) {
                    var context = ko.contextFor($(".metro-button.reload")[0]);
                    var dados = $("#form-senha").serialize();
                    var url = $("#form-senha").attr('action');
                    bootbox.hideAll();
                    context.$data.showLoading(function() {
                        $.ajax({
                            url : url,
                            data : dados,
                            type : "POST",
                            dataType : "json",
                            complete : function() {
                                context.$data.reload();
                            }
                        });
                    });
                } else {
                    bootbox.alert("As senhas n&atilde;o coincidem");
                }
            } else {
                bootbox.alert("Preencha os campos \"Senha\" e \"Confirmar Senha\"");
            }
        })
    });
</script>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Alterar Senha
        </h2>
    </div>
</div>
<?php $session->flash();?>
<?=$form->create('Usuario', array(
    'url' => "/{$this->params['prefix']}/usuarios/alterar_senha_usuario/",
    'id' => 'form-senha')); ?>
<?=$form->hidden('Usuario.id'); ?>
<?=$form->hidden('exibirFlash'); ?>
<div class='row-fluid'>
    <div class='span6'>
        <label>Senha</label>
        <?=$form->input('senha',
            array(
                'label' => false,
                'div' => 'input-control password',
                'type' => 'password',
                'id' => 'senha',
                'error' => false,
                'after' => $buttonAfter));
        ?>
    </div>
    <div class='span6'>
        <label>Confirmar Senha</label>
        <?=$form->input('confirmar',
            array(
                'label' => false,
                'div' => 'input-control password',
                'type' => 'password',
                'id' => 'confirmar',
                'error' => false,
                'after' => $buttonAfter));
        ?>
    </div>
    <button type='submit' class="default bg-color-red big" id='atualizar-senha'>
        <i class="icon-checkmark"></i>
        Atualizar
    </button>
</div>
<?=$form->end(array('label' => false, 'div' => false, 'class' => 'hide', 'style' => 'display: none')); ?>