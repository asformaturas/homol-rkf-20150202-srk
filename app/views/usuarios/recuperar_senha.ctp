<script type="text/javascript">
    $(document).ready(function() {
        $.Input();
        $('#form').submit(function(){
            var dados = $('#form').serialize();
            var url = '/usuarios/recuperar_senha';
            $.ajax({
                url: url,
                data: dados,
                type: "POST",
                dataType: "json",
                success: function() {
                }
            });
        });
    });
</script>
<div class="row-fluid">
	<div class="alert alert-error strong">Recuperar Senha</div>
</div>
<?=$form->create('Usuario', array('id' => 'form')); ?>
<div class="row-fluid">
    <div id="erro"><?php $session->flash(); ?></div>
    <label>E-mail</label>
    <?=$form->input('email', array('label' => false, 'div' => 'input-control text',
                    'after' => '<button class="helper" onclick="return false" tabindex="-1" type="button"></button>', 'id' => 'email')); ?>
    <?=$form->end(array('label' => 'Enviar', 'div' => false, 'class' => 'button bg-color-greenDark'));?>
</div>


