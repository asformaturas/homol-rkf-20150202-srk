<div class="row-fluid">
    <div class="span9">
        <div class="alert alert-info">
            <h4>Importante</h4>
            O envio de emails pelo sistema é importante para mantê-lo atualizado sobre sua festa de formatura
        </div>
        <h2 class="fg-color-red">Confirmar envio de emails pelo sistema</h2>
        <br />
        <a class="button bg-color-green" href="/usuarios/confirmar_envio_email/<?=$usuarioId?>/1">
            Quero continuar recebendo emails da AS
        </a>
        <a class="button bg-color-red" href="/usuarios/confirmar_envio_email/<?=$usuarioId?>/0">
            Não quero mais receber emails da AS
        </a>
    </div>
</div>