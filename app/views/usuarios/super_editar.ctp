<span id="conteudo-titulo" class="box-com-titulo-header">Usuários - Editar</span>
<div id="conteudo-container">
    <?php $session->flash(); ?>
    <?php echo $form->create('Usuario', array(
        'url' => "/{$this->params['prefix']}/usuarios/editar",
        'id' => 'formulario-usuario'
    )); ?>
    <?php include('_form.ctp'); ?>

    <p class="grid_7 alpha omega">
        <?php echo $html->link('Voltar', array($this->params['prefix'] => true, 'action' => 'index'), array('class' => 'cancel')); ?>
        <?php echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'submit')); ?>
    </p>
</div>