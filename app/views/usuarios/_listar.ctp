<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/tooltip.js"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/jquery.meio.mask.js"></script>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<style type="text/css">
    .ramal { width:20px; height: 5px!important; margin-top: 10px!important }
    .input[type=text] { min-width: 50px }
</style>
<script type="text/javascript">
    
    $(document).ready(function() {
        $.Input();
        $('input[alt]').setMask();
        $("#form-filtro").submit(function(e){
            e.preventDefault();
            context.$data.showLoading(function() {
                $("#form-filtro").find('[value="false"]').attr('disabled','disabled');
                var dados = $("#form-filtro").serialize();
                var url = $("#form-filtro").attr('action');
                $.ajax({
                    url : url,
                    data : dados,
                    type : "POST",
                    dataType : "json",
                    complete : function() {
                        context.$data.reload();
                    }
                });
            });
        });
        $(".confirmar-presenca").on('click', function() {
            div = $(this).next();
            $(this).fadeOut(500,function() {
                div.fadeIn(500);
            });
        });
        $(".cancelar-confirmacao").click(function() {
            button = $(this).parent().prev();
            $(this).parent().fadeOut(500,function() {
                button.fadeIn(500);
            });
        });
        $(".enviar-confirmacao").click(function(e) {
            e.preventDefault();
            var td = $(this).parents('td');
            var div = $(this).parent();
            var id = td.data('id');
            var ramal = $(this).prev().val();
            if(ramal != ''){
                $.ajax({
                    url : '/rh/usuarios/alterar_ramal/',
                    data : {
                        data : {
                            id : id,
                            ramal : ramal
                        }
                    },
                    type : "POST",
                    dataType : "json",
                    success: function(response) {
                        div.fadeOut(500,function() {
                            if(response.error == 0){
                                div.prev().text(ramal).fadeIn(500).removeClass().addClass("default mini bg-color-greenDark confirmar-presenca");
                            }else{
                                bootbox.alert("Ramal já está sendo utilizado por outro usuário.");
                                $(".confirmar-presenca").fadeIn(500);
                        }
                        });
                    },
                    error : function() {
                        div.fadeOut(500,function() {
                            td.html('Erro');
                            div.remove();
                        });
                    }
                });
            }else{
                bootbox.alert('Preencha o campo.');
            }
        });
        
    });
</script>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Lista de Funcionários
        </h2>
    </div>
</div>
<?php 
$paginator->options(array(
    'url' => array($this->params['prefix'] => true)));
$sortOptions = array('data-bind' => 'click: loadThis');
$buttonAfter = '<button class="helper" onclick="return false" ' .
    'tabindex="-1" type="button"></button>';
?>
<div class="row-fluid">
    <?=$form->create('Usuario',array(
        'url' => "/usuarios/listar/",
        'id' => 'form-filtro')) ?>
    <div class="row-fluid">
        <div class="span3">
            <label>Nome</label>
            <?=$form->input('nome',
                array('label' => false, 'div' => 'input-control mini text',
                    'error' => false,'type' => 'text', 'after' => $buttonAfter)); ?>
        </div>

        <div class="span9">
            <label>&nbsp;</label>
            <button type='submit' class='mini max bg-color-red'>
                Filtrar
                <i class='icon-search-2'></i>
            </button>
        </div>
        
    </div>
<?php if (sizeof($funcionarios > 0)) : ?>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="5%"><?=$paginator->sort('ID', 'Usuario.id',$sortOptions); ?></th>
                <th scope="col" width="25%"><?=$paginator->sort('Nome', 'Usuario.nome',$sortOptions); ?></th>
                <th scope="col" width="20%"><?=$paginator->sort('Email', 'Usuario.email',$sortOptions); ?></th>
                <th scope="col" width="12%"><?=$paginator->sort('Setor', 'Usuario.grupo',$sortOptions); ?></th>
                <th scope="col" width="12%"><?=$paginator->sort('Telefone', 'FormandoFormandoProfile.tel_residencial',$sortOptions); ?></th>
                <th scope="col" width="12%"><?=$paginator->sort('Celular', 'FormandoFormandoProfile.tel_celular',$sortOptions); ?></th>
                <th scope="col" width="14%"><?=$paginator->sort('Ramal', 'Usuario.ramal',$sortOptions); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($funcionarios as $funcionario) : ?>
            <tr>
                <td><?=$funcionario['Usuario']['id']; ?></td>
                <td><?=$funcionario['Usuario']['nome']; ?></td>
                <td><?=$funcionario['Usuario']['email']; ?></td>
                <td><?=ucfirst($funcionario['Usuario']['grupo']); ?></td>
                <td style="text-align:center"><?=$funcionario['FormandoProfile']['tel_residencial']; ?></td>
                <td style="text-align:center"><?=$funcionario['FormandoProfile']['tel_celular']; ?></td>
                <?php if($usuario['Usuario']['grupo'] != 'rh'){ ?>
                <td style="text-align:center"><?=$funcionario['Usuario']['ramal']; ?></td>
                <?php }else{ ?>
                <td style="text-align:center"
                    data-id="<?=$funcionario['Usuario']['id']?>">
                    <?php if(empty($funcionario['Usuario']['ramal'])){ ?>
                    <button type="button" class="default mini bg-color-blueDark confirmar-presenca">
                            Cadastrar
                    </button>
                    <?php }else{ ?>
                    <button type="button" class="default mini bg-color-greenDark confirmar-presenca">
                            <?=$funcionario['Usuario']['ramal'];?>
                    </button>
                    <?php } ?>
                    <div class="hide">
                        <input type="text" alt="9999" class="input-control mini ramal"/>
                        <button type="button" class="mini bg-color-greenDark enviar-confirmacao">
                            <i class="icon-thumbs-up"></i>
                        </button>
                        <button type="button" class="mini bg-color-red cancelar-confirmacao">
                            <i class="icon-cancel-3"></i>
                        </button>
                    </div>
                </td>
                <?php } ?>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="3" class="paginacao">
                    <?=$paginator->numbers(array('separator' => ' ','data-bind' => 'click: loadThis')); ?>
                </td>
                <td colspan="3">
                    <?=$paginator->counter(array('format' => 'Total : %count% ' .  'Funcionários')); ?>
                </td>
            </tr>
        </tfoot>
    </table>
    <?= $form->end(array('label' => false,
        'div' => false, 'class' => 'hide')); ?>
<?php else : ?>
    <h2 class="fg-color-red">Nenhum Funcionário Encontrado</h2>
<?php endif; ?>
</div>