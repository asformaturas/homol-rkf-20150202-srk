<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<style type="text/css">
table tbody tr td { border-bottom: solid 1px white!important }
</style>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.selectpicker').selectpicker();
    $.Input();
    $('#content-body').tooltip({ selector: '[rel=tooltip]'});
    $("#form-filtro").submit(function(e) {
        e.preventDefault();
        var context = ko.contextFor($(".metro-button.reload")[0]);
        context.$data.showLoading(function() {
            $("#form-filtro").find('[value="false"]').attr('disabled','disabled');
            var dados = $("#form-filtro").serialize();
            var url = $("#form-filtro").attr('action');
            $.ajax({
                url : url,
                data : dados,
                type : "POST",
                dataType : "json",
                complete : function() {
                    context.$data.reload();
                }
            });
        });
    });
    $(".dados-financeiros").click(function(e) {
        e.preventDefault();
        bootbox.dialog('Carregando',[{
            label: 'Fechar'
        }],{
            remote: $(this).attr('href')
        });
    });
    
    $('.senha').click(function() {
        bootbox.dialog('Carregando',[{
            label: 'Fechar'
        }],{
            remote: '<?="/{$this->params['prefix']}/usuarios/alterar_senha_usuario/"?>'+
                    $(this).attr('dir')
        });
    });
})
</script>
<div class="row-fluid">
    <h2>
        <a class="metro-button reload" data-bind="click: reload"></a>
        Formandos
    </h2>
</div>
<?php
$session->flash();
$paginator->options(array(
    'url' => array($this->params['prefix'] => true)));
$sortOptions = array('data-bind' => 'click: loadThis');
$buttonAfter = '<button class="helper" onclick="return false" ' .
    'tabindex="-1" type="button"></button>';
?>
<div class="row-fluid">
    <?=$form->create('ViewFormandos',array(
        'url' => "/{$this->params['prefix']}/usuarios/listar",
        'id' => 'form-filtro')) ?>
    <div class="row-fluid">
        <div class="span2">
            <?=$form->input('codigo_formando',
                array(
                    'label' => 'Cod',
                    'div' => 'input-control text',
                    'error' => false,
                    'type' => 'text',
                    'after' => $buttonAfter
                )); ?>
        </div>
        <div class="span3">
            <?=$form->input('nome',
                array(
                    'label' => 'Nome',
                    'div' => 'input-control text',
                    'error' => false,
                    'type' => 'text',
                    'after' => $buttonAfter
                )); ?>
        </div>
        <div class="span2">
            <?=$form->input('turma_id',
                array(
                    'label' => 'Turma',
                    'div' => 'input-control text',
                    'error' => false,
                    'type' => 'text',
                    'after' => $buttonAfter
                )); ?>
        </div>
        <div class="span3">
            <div class="input-control text">
                <label>&nbsp;</label>
                <button type='submit' class='mini max bg-color-red'>
                    Filtrar
                    <i class='icon-search-2'></i>
                </button>
            </div>
        </div>
    </div>
    <?= $form->end(array('label' => false,
        'div' => false, 'class' => 'hide')); ?>
<?php if (sizeof($formandos) > 0) : ?>
    <div class="row-fluid">
        <div class="span2 bg-color-greenLight">
            Ativo
        </div>
        <div class="span2 bg-color-red">
            Cancelado
        </div>
    </div>
    <br />
    <div class="row-fluid">
        <table class="table table-condensed">
            <thead>
                <tr>
                    <th scope="col" width="5%">
                        <?=$paginator->sort('Cod', 'codigo_formando',$sortOptions); ?>
                    </th>
                    <th scope="col" width="40%">
                        <?=$paginator->sort('Nome', 'nome',$sortOptions); ?>
                    </th>
                    <th scope="col" width="5%">
                        <?=$paginator->sort('Grupo', 'grupo',$sortOptions); ?>
                    </th>
                    <th scope="col" width="20%">
                        <?=$paginator->sort('Email', 'email',$sortOptions); ?>
                    </th>
                    <th scope="col" width="5%">
                        <?=$paginator->sort('Data Adesao', 'data_adesao',$sortOptions); ?>
                    </th>
                    <th scope="col" width="5%">
                        <?=$paginator->sort('Fez Check', 'fez_checkout',$sortOptions); ?>
                    </th>
                    <th scope="col" width="20%"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($formandos as $formando) : ?>
                <?php
                    if($formando['ViewFormandos']['situacao'] == 'cancelado')
                        $class = 'bg-color-red';
                    else
                        $class = 'bg-color-greenLight';
                ?>
                <tr class="<?=$class?>">
                    <td><?=$formando['ViewFormandos']['codigo_formando']; ?></td>
                    <td><?=$formando['ViewFormandos']['nome']; ?></td>
                    <td><?=substr($formando['ViewFormandos']['grupo'],0,6) ?></td>
                    <td><?=$formando['ViewFormandos']['email']; ?></td>
                    <td>
                        <?php if(!empty($formando['ViewFormandos']['data_adesao'])) : ?>
                        <?=date('d/m/Y',strtotime($formando['ViewFormandos']['data_adesao'])); ?>
                        <?php else : ?>
                        -
                        <?php endif; ?>
                    </td>
                    <td><?=$formando[0]['fez_checkout'] == 0 ? "Não" : "Sim"; ?></td>
                    <td>
                        <?=$html->link('Cadastro',
                            array(
                                $this->params['prefix'] => true,
                                'controller' => 'usuarios',
                                'action' =>'editar_dados',
                                'pessoais',
                                $formando['ViewFormandos']['id']
                            ), array(
                                'class' => 'button mini default bg-color-red',
                                'data-bind' => 'click: loadThis'
                            )); ?>
                        <?=$html->link('Financeiro',
                            array(
                                $this->params['prefix'] => true,
                                'controller' => 'area_financeira',
                                'action' =>'dados',
                                $formando['ViewFormandos']['id']
                            ), array(
                                'class' => 'button mini bg-color-blueDark dados-financeiros'
                            )); ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="4" class="paginacao">
                        <?=$paginator->numbers(array('separator' => ' ',
                            'data-bind' => 'click: loadThis')); ?>
                    </td>
                    <td colspan="3">
                        <span class="label label-info">
                        <?=$paginator->counter(array(
                            'format' => 'Total : %count% ' .  $this->name)); ?>
                        </span>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
<?php else : ?>
    <h2 class="fg-color-red">Nenhum Formando Encontrado</h2>
<?php endif; ?>
</div>
