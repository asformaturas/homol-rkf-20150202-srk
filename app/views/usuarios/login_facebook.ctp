<?php if($usuario) { ?>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/transition.js"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/alert.js"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/tooltip.js"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/modal.js"></script>
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/bootstrap/bootbox.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    bootbox.dialog($("#confirmacao").html(),[{
        'label' : 'Sim, entrar no sistema',
        'class' : 'btn-success',
        'callback' : function() {
            var url = "/usuarios/login_facebook/";
            $.ajax({
                url: url,
                data: { data: { usuario: <?=$usuario['Usuario']['id']?> }},
                type: "POST",
                dataType: "json",
                async: true,
                success: function(response) {
                    if(response.error == 0 && response.url != undefined) {
                        document.location.href = response.url;
                    } else {
                        bootbox.alert('Erro ao conectar conta do Facebook',function() {
                            bootbox.hideAll();
                        });
                    }
                },
                error: function() {
                    bootbox.alert('Erro ao conectar conta do Facebook',function() {
                        bootbox.hideAll();
                    });
                }
            });
            return false;
        }
    }, {
        'label' : 'N&atilde;o, voltar ao login',
        'class' : 'btn-danger',
        'callback' : function() { }
    }]);
})
</script>
<style type="text/css">
.img-center { position:relative; left:50%; margin-left:-70px }
</style>
<div id="confirmacao" class="hide">
    <h2 style="text-align:center">Este é você?</h2>
    <br />
    <img src="https://graph.facebook.com/<?=$facebookProfile['id']?>/picture?width=140&height=140" class="img-circle img-center" />
</div>
<?php } ?>
<?php include('login.ctp'); ?>