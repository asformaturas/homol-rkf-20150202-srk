<div class="grid_16">
<?php 
	$session->flash();
	echo $form->create('Usuario', array('url' => "/{$this->params['prefix']}/usuarios/mudar_senha", 'style' => 'display:block;')); 
?>

<p class="grid_5 alpha omega first">
	<label class="grid_full alpha omega">Senha atual</label>
	<?php echo $form->password('senha_atual', array('class' => 'grid_full alpha omega first', 'label' => false, 'div' => false, 'default' => '')); ?>
</p>	
<p class="grid_5 alpha omega first">
	<label class="grid_full alpha omega">Senha nova</label>
	<?php echo $form->password('senha_nova', array('class' => 'grid_full alpha omega first', 'label' => false, 'div' => false)); ?>
</p>

<p class="grid_5 alpha omega first">
	<label class="grid_full alpha omega">Confirmar senha nova</label>
	<?php echo $form->password('senha_nova_confirmar', array('class' => 'grid_full alpha omega first', 'label' => false, 'div' => false)); ?>
</p>

<p class="grid_5 alpha omega first">
	<?php echo $form->end(array('label' => 'Mudar', 'div' => false, 'class' => 'submit'));?>
</p>
</div>