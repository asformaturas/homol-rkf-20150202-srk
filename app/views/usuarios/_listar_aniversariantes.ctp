<?php setlocale (LC_ALL, 'pt_BR'); ?>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Lista de Aniversariantes do Mês de <?php echo strftime("%B");?>
        </h2>
    </div>
</div>
<div class="row-fluid">
    <div class="span8">
    <?php if (sizeof($usuarios > 0)) : ?>
        <table class="table table-condensed table-striped">
            <thead>
                <tr>
                    <th scope="col">Nome</th>
                    <th scope="col">Email</th>
                    <th scope="col">Data</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($usuarios as $usuario) : ?>
                <tr>
                    <td width="45%"><?=$usuario['Usuario']['nome']; ?></td>
                    <td width="45%"><?=$usuario['Usuario']['email']; ?></td>
                    <td width="10%"><?=date('d/m',strtotime($usuario['f']['data_nascimento'])); ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else : ?>
        <h2 class="fg-color-red">Nenhum Aniversariante para esse mês.</h2>
    <?php endif; ?>
    </div>
</div>