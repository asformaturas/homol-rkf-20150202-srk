<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('.selectpicker').selectpicker({width:'100%'});
    $("#form").submit(function(e) {
        e.preventDefault();
        var context = ko.contextFor($(".metro-button.reload")[0]);
        var dados = $("#form").serialize();
        var url = $("#form").attr('action');
        bootbox.hideAll();
        context.$data.showLoading(function() {
            $.ajax({
                url: url,
                data: dados,
                type: "POST",
                dataType: "json",
                complete: function() {
                    context.$data.reload();
                }
            });
        });
    });
});
</script>
<div class="row-fluid">
	<?php $session->flash(); ?>
	<?php echo $form->create('Usuario', array('url' => "/{$this->params['prefix']}/usuarios/inserir/", 'id' => 'form')); ?>
    <div class="row-fluid">
        <div class="span5">
                <?php echo $form->input('Usuario.nome', array('label' => 'Nome', 'div' => 'input-control text', 'error' => false)); ?>
        </div>
        <div class="span5">
                <?php echo $form->input('Usuario.email', array('label' => 'Email', 'div' => 'input-control text', 'error' => false)); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span5">
            <?=$form->input('Usuario.nivel', array(
                'options' => $niveis,
                'type' => 'select',
                'empty' => 'Selecione o Nível',
                'class' => 'selectpicker select-nivel',
                'label' => 'Nível',
                'div' => 'input-control text')); ?>
        </div>
        <div class="span5">
            <?=$form->input('Usuario.grupo', array(
                'options' => $grupos,
                'type' => 'select',
                'empty' => 'Selecione o Grupo',
                'class' => 'selectpicker select-grupo',
                'label' => 'Grupo',
                'div' => 'input-control text')); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span5">
                <?php echo $form->input('Usuario.senha', array('label' => 'Senha', 'type' => 'password', 'value' => '', 'div' => 'input-control text', 'error' => false, 'id' => 'senha')); ?>
        </div>
        <div class="span5">
                <?php echo $form->input('Usuario.confirmar', array('label' => 'Confirmar Senha', 'type' => 'password', 'div' => 'input-control text', 'error' => false, 'id' => 'confirmar-senha')); ?>
        </div>
    </div>
    <br />
    <div class="row-fluid">
        <div class="span12">
            <button type='submit' class='button max bg-color-greenDark'>
                Salvar
                <i class='icon-ok'></i>
            </button>
        </div>
    </div>
</div>