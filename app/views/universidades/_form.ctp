<?php echo $form->input('id',  array('hiddenField' => true)); ?>

<p class="grid_full alpha omega">
	<label class="grid_11 alpha omega">Nome</label>
	<?php echo $form->input('nome', array('class' => 'grid_6 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_11 alpha omega">Sigla</label>
	<?php echo $form->input('sigla', array('class' => 'grid_4 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
