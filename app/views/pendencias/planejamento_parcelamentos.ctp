<style type="text/css">
.chzn-container { line-height:16px }
.chzn-container-single .chzn-single div b { top:2px!important; }
.chzn-container-single .chzn-single span { position:relative; top:2px }
label { font-size: 13px; margin-bottom:5px; line-height: 16px }
.mini.max { font-size:12px; height:28px }
</style>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/chosen.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/chosen.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $(".chosen").chosen({width:'100%'});
    $("#form-filtro").submit(function(e) {
        e.preventDefault();
        var context = ko.contextFor($(".metro-button.reload")[0]);
        context.$data.showLoading(function() {
            $("#form-filtro").find('[value="false"]').attr('disabled','disabled');
            var dados = $("#form-filtro").serialize();
            var url = $("#form-filtro").attr('action');
            $.ajax({
                url : url,
                data : dados,
                type : "POST",
                dataType : "json",
                complete : function() {
                    context.$data.reload();
                }
            });
        });
    });
});
</script>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Turmas
        </h2>
    </div>
</div>
<?php
$session->flash();
if(sizeof($turmas) > 0) :
    $paginator->options(array(
        'url' => array($this->params['prefix'] => true)));
    $sortOptions = array('data-bind' => 'click: loadThis'); ?>
<div class="row-fluid">
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="5%">
                    <?=$paginator->sort('ID', 'Turma.id',$sortOptions); ?>
                </th>
                <th scope="col" width="30%">
                    <?=$paginator->sort('Nome', 'Turma.nome',$sortOptions); ?>
                </th>
                <th scope="col" width="12%">
                    <?=$paginator->sort('Assinatura', 'Turma.data_assinatura_contrato',$sortOptions); ?>
                </th>
                <th scope="col" width="12%">
                    <?=$paginator->sort('Ult Aplicacao', 'data_ultima_aplicacao',$sortOptions); ?>
                </th>
                <th scope="col" width="12%">
                    <?=$paginator->sort('Prox Aplicacao', 'data_proxima_aplicacao',$sortOptions); ?>
                </th>
                <th scope="col" width="12%">
                    <?=$paginator->sort('Ult P Maker', 'data_ultimo_parcelamento',$sortOptions); ?>
                </th>
                <th scope="col" width="5%">
                    <?=$paginator->sort('Aplicacoes', 'aplicacoes',$sortOptions); ?>
                </th>
                <th scope="col" width="12%"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($turmas as $turma) : ?>
            <tr>
                <td><?=$turma['Turma']['id']; ?></td>
                <td><?=$turma['Turma']['nome']; ?></td>
                <td><?=date('d/m/Y',strtotime($turma['Turma']['data_assinatura_contrato'])); ?></td>
                <td><?=!empty($turma[0]['data_ultima_aplicacao']) ? date('d/m/Y',strtotime($turma[0]['data_ultima_aplicacao'])) : ''; ?></td>
                <td><?=!empty($turma[0]['data_proxima_aplicacao']) ? date('d/m/Y',strtotime($turma[0]['data_proxima_aplicacao'])) : ''; ?></td>
                <td><?=!empty($turma[0]['data_ultimo_parcelamento']) ? date('d/m/Y',strtotime($turma[0]['data_ultimo_parcelamento'])) : ''; ?></td>
                <td><?=$turma[0]['aplicacoes']; ?></td>
                <td>
                    <a class="button mini bg-color-greenDark" data-bind="click: loadThis"
                        href="/<?=$this->params['prefix']?>/turmas/formandos_igpm/<?=$turma['Turma']['id']; ?>">
                        Formandos
                    </a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4" class="paginacao">
                    <?=$paginator->numbers(array('separator' => ' ',
                        'data-bind' => 'click: loadThis')); ?>
                </td>
                <td colspan="2">
                    <span class="label label-info">
                    <?=$paginator->counter(array(
                        'format' => 'Total : %count% Turmas')); ?>
                    </span>
                </td>
            </tr>
        </tfoot>
    </table>
</div>
<?php else : ?>
<h2 class="fg-color-red">Nenhuma Turma Encontrada</h2>
<?php endif; ?>