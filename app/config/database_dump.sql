-- MySQL dump 10.13  Distrib 5.5.14, for osx10.6 (i386)
--
-- Host: mysql.brunobueno.com    Database: teste_as
-- ------------------------------------------------------
-- Server version	5.1.56-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- ============================ --
-- DUMP REALIZADO EM 15/08/2011 --
-- ============================ --
-- _____øøøøø_______øøøøø______ --
-- ___øøøøøøøøø___øøøøøøøøø____ --
-- __øøøøøøøøøøø_øøøøøøøøøøø___ --
-- __øøøøøøøøøøøøøøøøøøøøøøø___ --
-- __øøøøøøøøøøøøøøøøøøøøøøø___ --
-- ___øøøøøøøøøøøøøøøøøøøøø____ --
-- ____øøøøøøøøøøøøøøøøøøø_____ --
-- ______øøøøøøøøøøøøøøø_______ --
-- ________øøøøøøøøøøø_________ -- 
-- ___________øøøøø____________ -- 
-- _____________ø______________ --

--
-- Table structure for table `agendas`
--

DROP TABLE IF EXISTS `agendas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agendas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usuarios_id` int(10) unsigned NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `descricao` text NOT NULL,
  `local` varchar(200) NOT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Agendas_has_usuarios` (`usuarios_id`),
  CONSTRAINT `fk_Agendas_has_usuarios` FOREIGN KEY (`usuarios_id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COMMENT='Tabela persiste eventos criados por determinado usuario';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agendas`
--

LOCK TABLES `agendas` WRITE;
/*!40000 ALTER TABLE `agendas` DISABLE KEYS */;
INSERT INTO `agendas` VALUES (22,10,'TESTE1','testsets','tsetse','2011-08-10 08:32:00'),(23,10,'Encontro marcado','Festa na as e tal','sala as','2011-08-20 05:36:00'),(24,10,'evento compartilhado','teste','a','2011-08-18 05:36:00'),(25,20,'Reuniao com o Jorge','descricao','sala 5','2011-08-10 06:30:00'),(26,20,'evento 2','descricao 2','as','2011-08-19 18:00:00');
/*!40000 ALTER TABLE `agendas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agendas_usuarios`
--

DROP TABLE IF EXISTS `agendas_usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agendas_usuarios` (
  `agenda_id` int(10) unsigned NOT NULL,
  `usuario_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`agenda_id`,`usuario_id`),
  UNIQUE KEY `unique` (`agenda_id`,`usuario_id`) USING BTREE,
  KEY `fk_Agendas_has_Usuarios` (`usuario_id`),
  KEY `fk_Usuarios_has_Agendas` (`agenda_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agendas_usuarios`
--

LOCK TABLES `agendas_usuarios` WRITE;
/*!40000 ALTER TABLE `agendas_usuarios` DISABLE KEYS */;
INSERT INTO `agendas_usuarios` VALUES (22,9),(22,10),(22,14),(22,15),(24,16),(26,16);
/*!40000 ALTER TABLE `agendas_usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `arquivos`
--

DROP TABLE IF EXISTS `arquivos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `arquivos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `tipo` varchar(32) NOT NULL,
  `categoria` varchar(255) DEFAULT NULL,
  `tamanho` int(9) unsigned NOT NULL,
  `usuario_id` int(10) unsigned DEFAULT NULL,
  `turma_id` int(10) unsigned NOT NULL,
  `formando_id` int(10) unsigned DEFAULT '0',
  `deletado` tinyint(1) DEFAULT NULL,
  `deletado_por` int(10) unsigned DEFAULT NULL,
  `diretorio` varchar(255) NOT NULL,
  `nome_secreto` varchar(32) NOT NULL,
  `criado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado` datetime DEFAULT NULL,
  `descricao` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_Arquivos_Usuarios1` (`usuario_id`),
  KEY `fk_Arquivos_Turmas1` (`turma_id`),
  KEY `fk_Arquivos_Formandos1` (`formando_id`),
  CONSTRAINT `fk_Arquivos_Formandos1` FOREIGN KEY (`formando_id`) REFERENCES `formandos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Arquivos_Turmas1` FOREIGN KEY (`turma_id`) REFERENCES `turmas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Arquivos_Usuarios1` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Um arquivo deve pertencer a um usuário ou a um formando. Ess';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `arquivos`
--

LOCK TABLES `arquivos` WRITE;
/*!40000 ALTER TABLE `arquivos` DISABLE KEYS */;
/*!40000 ALTER TABLE `arquivos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assuntos`
--

DROP TABLE IF EXISTS `assuntos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assuntos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL,
  `conclusao` text,
  `pendencia` enum('As','Comissao','Informativo') NOT NULL,
  `resolvido` tinyint(1) NOT NULL DEFAULT '0',
  `nome` varchar(45) DEFAULT NULL,
  `turma_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_assuntos_itens1` (`item_id`),
  KEY `fk_assuntos_turma` (`turma_id`),
  CONSTRAINT `fk_assuntos_itens1` FOREIGN KEY (`item_id`) REFERENCES `itens` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_assuntos_turma` FOREIGN KEY (`turma_id`) REFERENCES `turmas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `arquivos_mensagens`;
CREATE TABLE `arquivos_mensagens` (
  `arquivo_id` int(10) unsigned NOT NULL,
  `mensagem_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`arquivo_id`,`mensagem_id`),
  KEY `fk_Arquivos_has_Mensagens_Arquivos1` (`arquivo_id`),
  KEY `fk_Arquivos_has_Mensagens_Mensagens1` (`mensagem_id`),
  CONSTRAINT `fk_Arquivos_has_Mensagens_Arquivos1` FOREIGN KEY (`arquivo_id`) REFERENCES `arquivos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Arquivos_has_Mensagens_Mensagens1` FOREIGN KEY (`mensagem_id`) REFERENCES `mensagens` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `assuntos`
--

LOCK TABLES `assuntos` WRITE;
/*!40000 ALTER TABLE `assuntos` DISABLE KEYS */;
INSERT INTO `assuntos` VALUES (1,1,NULL,'As',0,'Jo acertou dessa vez',12),(2,1,NULL,'Comissao',0,'assunto 2',12),(3,1,NULL,'Comissao',0,'festa na sexta',12),(4,2,NULL,'As',0,'assunto do item 2',12),(5,1,NULL,'As',0,'Mensagens tem que ser links',12);
/*!40000 ALTER TABLE `assuntos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campanhas`
--

DROP TABLE IF EXISTS `campanhas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campanhas` (
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campanhas`
--

LOCK TABLES `campanhas` WRITE;
/*!40000 ALTER TABLE `campanhas` DISABLE KEYS */;
/*!40000 ALTER TABLE `campanhas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cursos`
--

DROP TABLE IF EXISTS `cursos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cursos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `faculdade_id` int(10) unsigned NOT NULL,
  `nome` varchar(70) NOT NULL COMMENT 'Deve ser único dentro da faculdade. Essa verificação deve ser implementada na aplicação.',
  `sigla` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_Cursos_Faculdades1` (`faculdade_id`),
  CONSTRAINT `fk_Cursos_Faculdades1` FOREIGN KEY (`faculdade_id`) REFERENCES `faculdades` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cursos`
--

LOCK TABLES `cursos` WRITE;
/*!40000 ALTER TABLE `cursos` DISABLE KEYS */;
INSERT INTO `cursos` VALUES (1,2,'Engenharia de Computacao','PCS'),(2,2,'Engenharia Naval','PNV'),(3,2,'Engenharia de Telecomunicacoes','PTC'),(4,2,'Engenharia de Minas e Energia','PMR'),(5,3,'Historia','HST'),(6,4,'Administracao','ADM'),(7,4,'Economia','ECO'),(8,5,'Medicina','MED'),(9,5,'Farmacia','FMA');
/*!40000 ALTER TABLE `cursos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cursos_formandos`
--

DROP TABLE IF EXISTS `cursos_formandos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cursos_formandos` (
  `formando_id` int(10) unsigned NOT NULL,
  `curso_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`formando_id`,`curso_id`),
  UNIQUE KEY `unique` (`curso_id`,`formando_id`) USING BTREE,
  KEY `fk_Formandos_has_Cursos_Cursos1` (`curso_id`),
  KEY `fk_Formandos_has_Cursos_Formandos1` (`formando_id`),
  CONSTRAINT `fk_Formandos_has_Cursos_Cursos1` FOREIGN KEY (`curso_id`) REFERENCES `cursos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Formandos_has_Cursos_Formandos1` FOREIGN KEY (`formando_id`) REFERENCES `formandos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cursos_formandos`
--

LOCK TABLES `cursos_formandos` WRITE;
/*!40000 ALTER TABLE `cursos_formandos` DISABLE KEYS */;
/*!40000 ALTER TABLE `cursos_formandos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cursos_turmas`
--

DROP TABLE IF EXISTS `cursos_turmas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cursos_turmas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `curso_id` int(10) unsigned NOT NULL,
  `turma_id` int(10) unsigned NOT NULL,
  `turno` enum('matutino','vespertino','noturno','integral','outros') DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `curso_id` (`curso_id`,`turma_id`,`turno`),
  KEY `fk_Cursos_has_Turmas_Turmas1` (`turma_id`),
  KEY `fk_Cursos_has_Turmas_Cursos1` (`curso_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Tabela indica quais cursos pertencem a uma turma de formatur';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cursos_turmas`
--

LOCK TABLES `cursos_turmas` WRITE;
/*!40000 ALTER TABLE `cursos_turmas` DISABLE KEYS */;
INSERT INTO `cursos_turmas` VALUES (3,2,15,'integral'),(2,3,15,'matutino'),(1,3,15,'noturno');
/*!40000 ALTER TABLE `cursos_turmas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eventos`
--

DROP TABLE IF EXISTS `eventos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eventos` (
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eventos`
--

LOCK TABLES `eventos` WRITE;
/*!40000 ALTER TABLE `eventos` DISABLE KEYS */;
/*!40000 ALTER TABLE `eventos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faculdades`
--

DROP TABLE IF EXISTS `faculdades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faculdades` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `universidade_id` int(10) unsigned NOT NULL,
  `nome` varchar(100) NOT NULL COMMENT 'Deve ser único dentro da universidade. Essa verificação deve ser implementada na aplicação.',
  `sigla` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_Faculdades_Universidades1` (`universidade_id`),
  CONSTRAINT `fk_Faculdades_Universidades1` FOREIGN KEY (`universidade_id`) REFERENCES `universidades` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faculdades`
--

LOCK TABLES `faculdades` WRITE;
/*!40000 ALTER TABLE `faculdades` DISABLE KEYS */;
INSERT INTO `faculdades` VALUES (2,4,'Escola Politecnica','POLI'),(3,4,'Faculdade de Filosofia, Letras e Ciencias Humanas','FFLCH'),(4,4,'Faculdade de Economia e Administracao','FEA'),(5,10,'Faculdade Oswaldo Cruz','FOC');
/*!40000 ALTER TABLE `faculdades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `formandos`
--

DROP TABLE IF EXISTS `formandos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formandos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `rg` varchar(9) DEFAULT NULL,
  `data_nascimento` date DEFAULT NULL,
  `sexo` enum('M','F') DEFAULT NULL,
  `end_rua` varchar(200) DEFAULT NULL,
  `end_numero` varchar(10) DEFAULT NULL,
  `end_complemento` varchar(45) DEFAULT NULL,
  `end_bairro` varchar(90) DEFAULT NULL,
  `end_cidade` varchar(100) DEFAULT NULL,
  `end_uf` varchar(2) DEFAULT NULL,
  `tel_residencial` varchar(11) DEFAULT NULL,
  `tel_comercial` varchar(11) DEFAULT NULL,
  `tel_celular` varchar(11) DEFAULT NULL,
  `operadora_celular` varchar(30) DEFAULT NULL,
  `nome_mae` varchar(100) DEFAULT NULL,
  `tel_mae` varchar(11) DEFAULT NULL,
  `nome_pai` varchar(100) DEFAULT NULL,
  `tel_pai` varchar(100) DEFAULT NULL,
  `nextel` varchar(20) DEFAULT NULL,
  `tam_camiseta` enum('PP','P','M','G','GG') DEFAULT NULL,
  `numero_havaiana` int(2) DEFAULT NULL,
  `cpf` varchar(11) DEFAULT NULL,
  `data_entrada_comissao` date DEFAULT NULL,
  `data_saida_comissao` date DEFAULT NULL,
  `cargo_comissao` varchar(255) DEFAULT NULL,
  `anotacoes_comissao` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `cpf` (`cpf`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `formandos`
--

LOCK TABLES `formandos` WRITE;
/*!40000 ALTER TABLE `formandos` DISABLE KEYS */;
INSERT INTO `formandos` VALUES (3,'Cleys','cley@cley.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'32222','2313131','4948559',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Subordinado','Trabalha pouco');
/*!40000 ALTER TABLE `formandos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `formandos_turmas`
--

DROP TABLE IF EXISTS `formandos_turmas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formandos_turmas` (
  `turma_id` int(10) unsigned NOT NULL,
  `formando_id` int(10) unsigned NOT NULL,
  `data_cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data_adesao` timestamp NULL DEFAULT NULL,
  `valor_adesao` decimal(11,2) DEFAULT NULL,
  `data_primeira_parcela` date DEFAULT NULL,
  `parcelas` int(3) DEFAULT NULL,
  `forma_pagamento` varchar(50) DEFAULT NULL,
  `cancelado` tinyint(1) NOT NULL DEFAULT '0',
  `data_cancelamento` datetime DEFAULT NULL,
  `observacao` text,
  `interesse_fotos` tinyint(1) DEFAULT NULL,
  `album_produzido` date DEFAULT NULL,
  `album_oferecido` date DEFAULT NULL,
  `album_data_venda` date DEFAULT NULL,
  `album_valor_venda` decimal(7,2) DEFAULT NULL,
  `data_conferido_as` datetime DEFAULT NULL,
  `origem` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`turma_id`,`formando_id`),
  UNIQUE KEY `unique` (`formando_id`,`turma_id`),
  KEY `fk_Turmas_has_Formandos_Formandos1` (`formando_id`),
  KEY `fk_Turmas_has_Formandos_Turmas1` (`turma_id`),
  CONSTRAINT `fk_Turmas_has_Formandos_Formandos1` FOREIGN KEY (`formando_id`) REFERENCES `formandos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Turmas_has_Formandos_Turmas1` FOREIGN KEY (`turma_id`) REFERENCES `turmas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `formandos_turmas`
--

LOCK TABLES `formandos_turmas` WRITE;
/*!40000 ALTER TABLE `formandos_turmas` DISABLE KEYS */;
/*!40000 ALTER TABLE `formandos_turmas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itens`
--

DROP TABLE IF EXISTS `itens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `grupo` enum('comercial','planejamento','atendimento') NOT NULL,
  `nome` varchar(45) NOT NULL,
  `descricao` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itens`
--

LOCK TABLES `itens` WRITE;
/*!40000 ALTER TABLE `itens` DISABLE KEYS */;
INSERT INTO `itens` VALUES (1,'comercial','item teste',NULL),(2,'planejamento','item 2','descriÃ§Ã£o item 2'),(3,'atendimento','Local de festa','');
/*!40000 ALTER TABLE `itens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lembretes`
--

DROP TABLE IF EXISTS `lembretes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lembretes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) DEFAULT NULL,
  `texto` varchar(200) DEFAULT NULL,
  `data_criacao` varchar(45) DEFAULT NULL,
  `data_modificacao` varchar(45) DEFAULT NULL,
  `usuario_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_Lembretes_Usuarios1` (`usuario_id`),
  CONSTRAINT `fk_Lembretes_Usuarios1` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lembretes`
--

LOCK TABLES `lembretes` WRITE;
/*!40000 ALTER TABLE `lembretes` DISABLE KEYS */;
INSERT INTO `lembretes` VALUES (1,'local','lorem ipsum',NULL,NULL,10),(2,'asdasd','caroxis',NULL,NULL,10),(3,'birulao','coringao',NULL,NULL,9),(4,'lembrete','lorem',NULL,NULL,16),(6,'lembrete 1','texto 1',NULL,NULL,10),(7,'lembrete','a aslkjdsa lkdsajlkjdlk jfldk Ã§jasl jdslf djlfkjdslo jsdÃ§flajds flÃ§sdj flÃ§kad fjdkls falÃ§asdÃ§lk asÃ§dlkf jadÃ§lf jdsÃ§lfradsÃ§Ã§sfkl asl fÃ§alsasdlÃ§ajasdl\r\n',NULL,NULL,10),(8,'jlkjljlÃ§jl','lhlkhlkhlÃ§',NULL,NULL,10);
/*!40000 ALTER TABLE `lembretes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mensagens`
--

DROP TABLE IF EXISTS `mensagens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mensagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `assunto_id` int(10) unsigned NOT NULL,
  `turma_id` int(10) unsigned NOT NULL,
  `usuario_id` int(10) unsigned DEFAULT NULL,
  `formando_id` int(10) unsigned DEFAULT NULL,
  `texto` text,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `data` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_mensagens_assuntos1` (`assunto_id`),
  KEY `fk_mensagens_turmas1` (`turma_id`),
  KEY `fk_mensagens_formandos` (`formando_id`),
  KEY `fk_mensagens_usuarios` (`usuario_id`),
  CONSTRAINT `fk_mensagens_formandos` FOREIGN KEY (`formando_id`) REFERENCES `formandos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_mensagens_usuarios` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_mensagens_assuntos1` FOREIGN KEY (`assunto_id`) REFERENCES `assuntos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_mensagens_turmas1` FOREIGN KEY (`turma_id`) REFERENCES `turmas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mensagens`
--

LOCK TABLES `mensagens` WRITE;
/*!40000 ALTER TABLE `mensagens` DISABLE KEYS */;
INSERT INTO `mensagens` VALUES (1,1,12,16,NULL,'texto da mensagem\r\n',0,'2011-08-09'),(2,1,12,16,NULL,'resposta 1\r\n',0,NULL),(3,1,12,16,NULL,'resposta 3\r\n',0,NULL),(4,1,12,16,NULL,'mensagem 4\r\n',0,NULL),(5,1,12,16,NULL,'responder a pendnecia',0,'2011-08-09'),(6,2,12,16,NULL,'Primeira mensagem do teste',0,'2011-08-09'),(7,4,12,16,NULL,'texto um do item 2\r\n',0,'2011-08-09');
/*!40000 ALTER TABLE `mensagens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parcelamentos`
--

DROP TABLE IF EXISTS `parcelamentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parcelamentos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `turma_id` int(10) unsigned NOT NULL,
  `data` date NOT NULL,
  `parcelas` int(3) unsigned NOT NULL,
  `valor` float(7,2) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_parcelamentos_turmas1` (`turma_id`),
  CONSTRAINT `fk_parcelamentos_turmas1` FOREIGN KEY (`turma_id`) REFERENCES `turmas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Esta tabela contém informações sobre as possibilidades de pa';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parcelamentos`
--

LOCK TABLES `parcelamentos` WRITE;
/*!40000 ALTER TABLE `parcelamentos` DISABLE KEYS */;
/*!40000 ALTER TABLE `parcelamentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registros_contatos`
--

DROP TABLE IF EXISTS `registros_contatos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registros_contatos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `turma_id` int(10) unsigned NOT NULL,
  `usuario_id` int(10) unsigned NOT NULL,
  `data` datetime NOT NULL,
  `tipo` enum('telefone','email','reunião','msn','outros') NOT NULL,
  `duracao` int(11) DEFAULT NULL,
  `membros_comissao` varchar(400) NOT NULL,
  `funcionarios_as` varchar(200) NOT NULL,
  `assunto` varchar(300) NOT NULL,
  `detalhes` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_registros_contatos_turmas1` (`turma_id`),
  KEY `fk_registros_contatos_usuarios1` (`usuario_id`),
  CONSTRAINT `fk_registros_contatos_turmas1` FOREIGN KEY (`turma_id`) REFERENCES `turmas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_registros_contatos_usuarios1` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registros_contatos`
--

LOCK TABLES `registros_contatos` WRITE;
/*!40000 ALTER TABLE `registros_contatos` DISABLE KEYS */;
/*!40000 ALTER TABLE `registros_contatos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_eventos`
--

DROP TABLE IF EXISTS `tipo_eventos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_eventos` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `pessoas_esperadas` varchar(45) DEFAULT NULL,
  `pessoas_presentes` varchar(45) DEFAULT NULL,
  `eventos_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tipo_evento_eventos1` (`eventos_id`),
  CONSTRAINT `fk_tipo_evento_eventos1` FOREIGN KEY (`eventos_id`) REFERENCES `eventos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_eventos`
--

LOCK TABLES `tipo_eventos` WRITE;
/*!40000 ALTER TABLE `tipo_eventos` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_eventos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `turmas`
--

DROP TABLE IF EXISTS `turmas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `turmas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL COMMENT 'Nome identificador para a turma.',
  `ano_formatura` varchar(45) DEFAULT NULL,
  `semestre_formatura` enum('1','2') DEFAULT NULL,
  `expectativa_formandos` varchar(45) DEFAULT NULL COMMENT 'número esperado de formandos',
  `expectativa_fechamento` enum('alta','media','baixa') NOT NULL,
  `memorando` text,
  `fundo_caixa_usado` decimal(9,2) DEFAULT NULL,
  `igpm` varchar(255) DEFAULT NULL,
  `status` enum('aberta','fechada','descartada','concluida') NOT NULL,
  `data_assinatura_contrato` date DEFAULT NULL,
  `como_chegou` enum('internet','indicacao','consultor','outros') NOT NULL,
  `como_chegou_detalhes` text,
  `data_abertura_turma` date NOT NULL,
  `pretensao` enum('festa','colacao','ambos') NOT NULL,
  `analise_as` text,
  `beneficios_comissao` varchar(255) DEFAULT NULL,
  `concorrentes` char(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `turmas`
--

LOCK TABLES `turmas` WRITE;
/*!40000 ALTER TABLE `turmas` DISABLE KEYS */;
INSERT INTO `turmas` VALUES (12,'Turma do Abacate','2002','2','222222','media','23',22.00,'123123','aberta','2011-07-13','indicacao','123','2011-07-19','colacao','123','123',NULL),(15,'Turma do Amendoim','2008','1','2','alta','test',21.00,'12','aberta','2011-08-08','internet','','2011-08-08','festa','aa','sd',NULL),(16,'Turma do Barulho','','1','','alta','',NULL,'','aberta','2011-08-08','internet','','2011-08-08','festa','','',NULL);
/*!40000 ALTER TABLE `turmas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `turmas_usuarios`
--

DROP TABLE IF EXISTS `turmas_usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `turmas_usuarios` (
  `usuario_id` int(10) unsigned NOT NULL,
  `turma_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`usuario_id`,`turma_id`),
  KEY `fk_Usuarios_has_Turmas_Turmas1` (`turma_id`),
  KEY `fk_Usuarios_has_Turmas_Usuarios1` (`usuario_id`),
  CONSTRAINT `fk_Usuarios_has_Turmas_Turmas1` FOREIGN KEY (`turma_id`) REFERENCES `turmas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Usuarios_has_Turmas_Usuarios1` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `turmas_usuarios`
--

LOCK TABLES `turmas_usuarios` WRITE;
/*!40000 ALTER TABLE `turmas_usuarios` DISABLE KEYS */;
INSERT INTO `turmas_usuarios` VALUES (16,15);
/*!40000 ALTER TABLE `turmas_usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `universidades`
--

DROP TABLE IF EXISTS `universidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `universidades` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `sigla` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `nome_UNIQUE` (`nome`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `universidades`
--

LOCK TABLES `universidades` WRITE;
/*!40000 ALTER TABLE `universidades` DISABLE KEYS */;
INSERT INTO `universidades` VALUES (4,'Universidade de SÃ£o Paulo','USP'),(5,'Universidade de Campinas','UNICAMP'),(7,'Universidade Paulista','UNIP'),(8,'Universidade Estadual de Sao Paulo','UNESP'),(9,'Universidade Estadual de Santa Catarina','UNESC'),(10,'Sem Universidade',''),(11,'Universidade Internacional Anhembi Morumbi','UAM');
/*!40000 ALTER TABLE `universidades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `grupo` enum('comercial','planejamento','atendimento','super') NOT NULL,
  `nivel` enum('basico','administrador','gerencial') NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `login` varchar(45) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `data_criacao` varchar(45) DEFAULT NULL,
  `data_ultima_alteracao` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (9,'','super','gerencial','rafael2@rafael.com','alface','621e328101f600d26969c29aa275fa7488f9d8d6',NULL,NULL),(10,'','super','administrador','admin@admin.com','admin','5dc7dfe3eb04ede750ee3044c36314905f223bce',NULL,NULL),(14,'','planejamento','administrador','vinicius.oyama@gmail.com','vinicius','18e0e26748baf764f3dbab1096a8379dfaa34368',NULL,NULL),(15,'','comercial','basico','rafael@rafael.com','asdsa','18e0e26748baf764f3dbab1096a8379dfaa34368',NULL,NULL),(16,'','comercial','basico','abaco@abaco.com','abaco','6893a9443fba2c2cd4dd62bdaeb627042933461c',NULL,NULL),(17,'','super','gerencial','bruno@papa.com','bpapa','741471b6eba1e8939a9c7a0e9341fe1bd2c1d3f4',NULL,NULL),(18,'','super','gerencial','adminadmin@adm.com','admin','5dc7dfe3eb04ede750ee3044c36314905f223bce',NULL,NULL),(20,'','comercial','basico',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-08-15  2:52:55