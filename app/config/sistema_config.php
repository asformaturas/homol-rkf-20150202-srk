<?php

//Configuração de Ambiente
Configure::write('env','producao');
Configure::write('debug', 0);

//Diretorios principal para salvamento de arquivos
Configure::write('FileDirectoryPathUploadPagamentos', ROOT . "/pagamentos/");
Configure::write('FileDirectoryPath', ROOT . "/asfiles/");
Configure::write('FileSystemDirectoryPath', ROOT . "/asfiles/");

// Hotsites e WP WP Gerenciador
Configure::write('host_site','rkformaturas.com.br');
Configure::write('url_site','http://sistema.rkformaturas.com.br/');
Configure::write('WpGerenciador.wpDomain',      'hotsites.br'); // SEM http e SEM / no final

//Tema WP padrão aser utilizado ao criar o Hotsite
Configure::write('WpGerenciador.wpTheme',       'as-formaturas-hotsite');
Configure::write('WpGerenciador.webServiceKey', '21def5e6453b17bdec1a86494b17847d');

define('AS_FORMATURAS_SITE', 'siaf.localhost');

if(isset($_SERVER['HTTP_REFERER']) && preg_match('/'.AS_FORMATURAS_SITE.'/', $_SERVER['HTTP_REFERER'])){
	Configure::write('Security.level', 'low');
}