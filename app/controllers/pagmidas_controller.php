<?php

App::import('Vendor','nusoap');

class PagMidasController extends AppController {
	
	var $name = 'PagMidas';
	var $uses = array('ViewFormandos','PagmidasConfig','PagmidasBandeiras','PagmidasTransacoes','PagmidasCartoes','PagmidasItens','PagmidasResponses','PagmidasStatusCodigos');
	private $config = array();
	private $estagio = 'producao';
	var $paginate = array(
		'limit' => 25,
		'order' => array(
				'PagmidasTransacoes.id' => 'desc'
		)
	);
	
	public function beforeFilter() {
	    parent::beforeFilter();
	    $config = $this->PagmidasConfig->find('first',array('conditions' => array('ativo' => 1,'tipo' => $this->estagio)));
	    if(!empty($config))
	    	$this->config = $config['PagmidasConfig'];
	    else
	    	$this->Session->setFlash('O sistema de pagamentos não foi configurado. Por favor contate o administrador.', 'flash_erro');
	}
	
	function vendedor_index() {
		$this->super_index();
	}
	
	function super_index() {
		if(!empty($this->data)) {
			$usuario = $this->Session->read('Usuario');
			$this->PagmidasTransacoes->begin();
			$this->PagmidasCartoes->begin();
			$this->PagmidasItens->begin();
			$this->data['PagmidasTransacoes']['usuario_id'] = $usuario['Usuario']['id'];
			if(!empty($this->data['PagmidasTransacoes']['usuario_id_formando']))
				$formando = $this->ViewFormandos->find('first',array('conditions' => array('id' => $this->data['PagmidasTransacoes']['usuario_id_formando'])));
			$transacao = $this->PagmidasTransacoes->save($this->data);
			$errors = array();
			if(empty($transacao)) {
				foreach($this->PagmidasTransacoes->invalidFields() as $error)
					$errors[] = $error;
			} else {
				$formando = $this->ViewFormandos->find('first',array('conditions' => array('id' => $this->data['PagmidasTransacoes']['usuario_id_formando'])));
				$this->data['PagmidasItens']['transacao_id'] = $this->PagmidasTransacoes->id;
				$item = $this->PagmidasItens->save($this->data);
				if(empty($item))
					foreach($this->PagmidasItens->invalidFields() as $error)
					$errors[] = $error;
				$this->data['PagmidasCartoes']['transacao_id'] = $this->PagmidasTransacoes->id;
				$this->data['PagmidasCartoes']['valor'] = $this->data['PagmidasTransacoes']['valor'];
				$cartoes = $this->PagmidasCartoes->save($this->data);
				if(empty($cartoes))
					foreach($this->PagmidasCartoes->invalidFields() as $error)
					$errors[] = $error;
			}
			if(!empty($errors)) {
				$this->PagmidasTransacoes->rollback();
				$this->PagmidasCartoes->rollback();
				$this->PagmidasItens->rollback();
				if(isset($formando))
					$this->set('formando',$formando);
				$this->Session->setFlash($errors, 'flash_erro');
			} else {
				$this->data = array();
				$this->PagmidasTransacoes->commit();
				$this->PagmidasCartoes->commit();
				$this->PagmidasItens->commit();
				$this->PagmidasBandeiras->id = $this->PagmidasCartoes->field('bandeira_id');
				$client = new nusoap_client($this->config['wsdl'], false,'','','','');
				if($client->getError()) {
					$this->Session->setFlash($client->getError(), 'flash_erro');
				} else {
					$client->setNsPrefix($this->config['ns_prefix']);
					$valor = (int)str_replace('.','',$this->PagmidasTransacoes->field('valor'));
					$params = array(
						'transacao' => array(
							'codigoEstabelecimento' => (int)$this->config['codigo_estabelecimento'],
							'codigoFormaPagamento' => (int)$this->PagmidasBandeiras->field('codigo'),
							'codigoSeguranca' => (int)$this->PagmidasCartoes->field('codigo_seguranca'),
							'dadosUsuarioTransacao' => array(
							),
							'dataValidadeCartao' => $this->PagmidasCartoes->field('data_validade'),
							'idioma' => 1,
							'itensDoPedido' => array(
									'codigoCategoria' => 0,
									'codigoProduto' => 0,
									'nomeCategoria' => 0,
									'nomeProduto' => $this->PagmidasItens->field('nome'),
									'valorUnitarioProduto' => $valor,
									'quantidadeProduto' => 1
							),
							'nomeTitularCartaoCredito' => $this->PagmidasCartoes->field('nome'),
							'numeroCartaoCredito' => $this->PagmidasCartoes->field('numero_cartao'),
							'numeroTransacao' => (int)$this->PagmidasTransacoes->id,
							'valor' => $valor,
							'parcelas' => $this->PagmidasCartoes->field('parcelas')
						),
						'usuario' => $this->config['usuario'],
						'senha' => $this->config['senha']
					);
					$client->setAsDebug(false);
					$result = $client->call('pagamentoTransacaoCompleta', $params,$this->config['header'],'');
					if($client->fault) {
						$insert = array('error' => 1, 'error_content' => "{$result['faultcode']} - {$result['faultstring']}");
						$msg = "O pagamento foi salvo, porém ocorreu um erro ao enviar para o provedor.<br />";
						$msg.= "Procure esse pagamento na lista abaixo e clique em reenviar ou crie um novo pagamento.<br />";
						$msg.= "Se o erro persistir contate o administrador.";
						$this->Session->setFlash($msg, 'flash_erro');
					} else {
						if($client->getError()) {
							$msg = "O pagamento foi salvo, porém ocorreu um erro ao enviar para o provedor.<br />";
							$msg.= "Procure esse pagamento na lista abaixo e clique em reenviar ou crie um novo pagamento.<br />";
							$msg.= "Se o erro persistir contate o administrador.";
							$insert = array('error' => 1, 'content' => $client->getError());
							$this->Session->setFlash($msg, 'flash_erro');
						} else {
							$insert = array(
									'autorizacao' => $result['autorizacao'],
									'codigo_transacao_operadora' => $result['codigoTransacaoOperadora'],
									'data_aprovacao_operadora' => implode('-',array_reverse(explode("/",$result['dataAprovacaoOperadora']))),
									'mensagem_venda' => $result['mensagemVenda'],
									'numero_comprovante_venda' => $result['numeroComprovanteVenda'],
									'status_transacao' => $result['statusTransacao'],
									'taxa_embarque' => $result['taxaEmbarque'],
									'url_pagamento' => $result['urlPagamento'],
							);
							$status = $this->PagmidasStatusCodigos->find('first',
									array('conditions' => array('codigo'=> $result['statusTransacao'])));
							if(!empty($status)) {
								if($status['PagmidasStatusCodigos']['erro'] == 0) {
									$flash = array(
										'content' => array(
											'Transa&ccedil;&atilde;o efetuada com sucesso!',
											"Status: {$status['PagmidasStatusCodigos']['mensagem']}"
										),
										'type' => 'flash_sucesso'
									);
									if(isset($result['autorizacao']))
										$flash['content'][] = "C&oacute;digo de autoriza&ccedil;&atilde;o: {$result['autorizacao']}";
								} else {
									$flash = array(
										'content' => array(
											'Transa&ccedil;&atilde;o não finalizada',
											"Status: {$status['PagmidasStatusCodigos']['mensagem']}"
										),
										'type' => 'flash_erro'
									);
								}
							} else {
								$flash = array('content' => 'Erro ao resgatar status da transa&ccedil;&atilde;o','type' => 'flash_erro');
							}
							$flash['content'][] = "Clique <a href='/{$this->params['prefix']}/pagmidas/transacao/{$this->PagmidasTransacoes->id}'>aqui</a> para mais informações.";
							$this->Session->setFlash($flash['content'],$flash['type']);
							$this->redirect("/{$this->params['prefix']}/pagmidas/transacoes");
						}
					}
					$insert['data_cadastro'] = date('y-m-d H:i:s');
					$insert['transacao_id'] = $this->PagmidasTransacoes->id;
					$this->PagmidasResponses->create();
					$this->PagmidasResponses->save($insert);
				}
			}
		}
		$bandeiras = $this->PagmidasBandeiras->find('all',array('conditions' => array('ativo' => 1)));
		$this->set('bandeiras',$bandeiras);
	}
	
	function listar_formandos() {
		if($this->Session->read('Usuario')) {
			$this->layout = false;
			$this->autoRender = false;
			Configure::write(array('debug' => 0));
			$query = $_GET['data'];
			$this->loadModel('ViewFormandos');
			if(is_numeric($query))
				$formandos = $this->ViewFormandos->find('all',array('conditions' => array("codigo_formando like('%$query%')"),'limit' => 20));
			else
				$formandos = $this->ViewFormandos->find('all',array('conditions' => array("upper(nome) like('".strtoupper($query) . "%')"),'limit' => 20));
			$json = array();
			if(count($formandos) > 0) {
				foreach($formandos as $formando)
					array_push($json, $formando['ViewFormandos']);
			}
			echo json_encode($json);
		}
	}
	
	function vendedor_transacoes() {
		$this->super_transacoes();
	}
	
	function super_transacoes() {
		$this->PagmidasTransacoes->unBindModel(array('belongsTo' => array('PagmidasItens')) , false);
		if($this->data) {
			$data = $this->data;
			foreach($this->data as $c => $v)
				$this->Session->write("PagmidasTransacoes.$c",$v);
		} elseif($this->Session->read("PagmidasTransacoes")) {
			$data = $this->Session->read("PagmidasTransacoes");
		}
		if(isset($data)) {
			$options = array('conditions' => array());
			if($data['tipo'] == "formando") {
				$options['conditions'][] = "upper(PagmidasCartoes.nome_cartao) like('%" .
						strtoupper($data['formando']) . "%')";
			} elseif($data['tipo'] == "periodo") {
				$periodoInicial = implode("-",array_reverse(explode("/",$data['periodo-inicial'])));
				$periodoFinal = implode("-",array_reverse(explode("/",$data['periodo-final'])));
				$options['conditions'][] = "PagmidasTransacoes.data_cadastro >= '$periodoInicial' and " .
				"PagmidasTransacoes.data_cadastro <= '$periodoFinal'";
			} elseif($data['tipo'] == 'bandeira') {
				$options['conditions'][] = "PagmidasCartoes.bandeira_id = {$data['bandeira']}";
			}
			$this->paginate['PagmidasTransacoes'] = $options;
			$this->data = $data;
		}
		$transacoes = $this->paginate('PagmidasTransacoes');
		$filtro = array('formando' => 'Formando','periodo' => 'Período','bandeira' => 'Bandeira');
		$this->set('filtro',$filtro);
		$this->set('transacoes',$transacoes);
		$this->set('codigos',$this->obterCodigos());
		$this->set('bandeiras',$this->obterBandeiras());
	}
	
	private function obterBandeiras() {
		$bandeiras = array();
		foreach($this->PagmidasBandeiras->find('all') as $bandeira)
			$bandeiras[$bandeira['PagmidasBandeiras']['id']] = $bandeira['PagmidasBandeiras'];
		return $bandeiras;
	}
	
	private function obterCodigos() {
		$codigos = array();
		foreach($this->PagmidasStatusCodigos->find('all') as $codigo)
			$codigos[$codigo['PagmidasStatusCodigos']['codigo']] = $codigo['PagmidasStatusCodigos'];
		return $codigos;
	}
	
	function vendedor_transacao($acao,$transacaoId = false) {
		$this->super_transacao($acao,$transacaoId);
	}
	
	function super_transacao($acao,$transacaoId = false) {
		if(!$transacaoId) {
			$transacaoId = $acao;
		} else {
			if($acao == "verificar") {
				$transacao = $this->PagmidasTransacoes->find('first',array('conditions' => array('PagmidasTransacoes.id' => $transacaoId)));
				if(!empty($transacao)) {
					$client = new nusoap_client($this->config['wsdl'], false,'','','','');
					if($client->getError()) {
						$this->Session->setFlash($client->getError(), 'flash_erro');
					} else {
						$client->setNsPrefix($this->config['ns_prefix']);
						$params = array(
							'consultaTransacaoWS' => array(
								'codigoEstabelecimento' => (int)$this->config['codigo_estabelecimento'],
								'numeroTransacao' => $transacao['PagmidasTransacoes']['id']
							),
							'usuario' => $this->config['usuario'],
							'senha' => $this->config['senha']
						);
						$client->setAsDebug(false);
						$result = $client->call('consultaTransacaoEspecifica', $params,$this->config['header'],'');
						if($client->fault) {
							$this->Session->setFlash($result['faultstring'], 'flash_erro');
						} elseif($client->getError()) {
							$this->Session->setFlash($client->getError(), 'flash_erro');
						} else {
							$insert = array(
								'autorizacao' => $result['autorizacao'],
								'codigo_transacao_operadora' => $result['codigoTransacaoOperadora'],
								'data_aprovacao_operadora' => implode('-',array_reverse(explode("/",$result['dataAprovacaoOperadora']))),
								'mensagem_venda' => $result['mensagemVenda'],
								'numero_comprovante_venda' => $result['numeroComprovanteVenda'],
								'status_transacao' => $result['statusTransacao'],
								'taxa_embarque' => $result['taxaEmbarque'],
								'url_pagamento' => $result['urlPagamento'],
								'transacao_id' => $transacao['PagmidasTransacoes']['id'],
								'data_cadastro' => date('y-m-d H:i:s')
							);
							$this->PagmidasResponses->create();
							$this->PagmidasResponses->save($insert);
							$status = $this->PagmidasStatusCodigos->find('first',
									array('conditions' => array('codigo'=> $result['statusTransacao'])));
							if(!empty($status)) {
								if($status['PagmidasStatusCodigos']['erro'] == 0)
									$flash = array('type' => 'flash_sucesso', 'content' => "Status: {$status['PagmidasStatusCodigos']['mensagem']}");
								else
									$flash = array('type' => 'flash_erro', 'content' => "Status: {$status['PagmidasStatusCodigos']['mensagem']}");
							} else {
								$flash = array('content' => 'Erro ao resgatar status da transa&ccedil;&atilde;o','type' => 'flash_erro');
							}
							$this->Session->setFlash($flash['content'],$flash['type']);
							$this->redirect("/{$this->params['prefix']}/pagmidas/transacao/{$transacao['PagmidasTransacoes']['id']}");
						}
					}
				}
			}
		}
		$transacao = $this->PagmidasTransacoes->find('first',array('conditions' => array('PagmidasTransacoes.id' => $transacaoId)));
		$this->set('transacao',$transacao);
		$this->set('codigos',$this->obterCodigos());
		$this->set('bandeiras',$this->obterBandeiras());
	}

}