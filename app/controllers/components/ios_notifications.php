<?php
require_once 'ApnsPHP/Autoload.php';
class IosNotificationsComponent {

    var $Push;
    
    function connect() {
        if(Configure::read('env') == 'producao')
            $env = ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION;
        else
            $env = ApnsPHP_Abstract::ENVIRONMENT_SANDBOX;
        $this->Push = new ApnsPHP_Push(
            $env,
            APP.'controllers/components/ApnsPHP/server_certificates_bundle.pem'
        );
        $this->Push->setRootCertificationAuthority(APP.'controllers/components/ApnsPHP/entrust_root_certification_authority.pem');
        $this->Push->connect();
    }

    function send($token,$texto,$id,$params) {
        
        $message = new ApnsPHP_Message($token);
        $message->setCustomIdentifier($id);
        $message->setBadge(3);
        $message->setText($texto);
        $message->setSound();
        
        foreach($params as $p => $v)
            $message->setCustomProperty($p,$v);
        
        $message->setExpiry(30);
        $this->Push->add($message);
        $this->Push->send();
    }
    
    function disconnect() {
        $this->Push->disconnect();
    }

}
