<?php

class BoletoContaComponent extends Object {

    var $includePath = "vendors/PHPBoleto/include/";
    var $uses = array('BancoConta','Despesa','DespesaPagamento','Pagamento',
        'DespesaPagamento','FormandoProfile');
    var $components = array('Session');
    var $dadosboleto = array();
    var $despesaId = null;
    var $usuario = null;
    var $turma = null;
    var $bancoConta = null;
    var $dataVencimento = null;
    var $multa = 0;
    
    function __construct() {
        $this->dadosboleto["endereco1"] = "";
        $this->dadosboleto["endereco2"] = "";
        $this->dadosboleto["demonstrativo1"] = "BOLETO REFERENTE A EVENTOS DE FORMATURA.";
        $this->dadosboleto["demonstrativo3"] = "";
        $this->dadosboleto["instrucoes1"] = "BOLETO REFERENTE A EVENTOS DE FORMATURA.";
        $this->dadosboleto["instrucoes3"] = "";
        $this->dadosboleto["instrucoes4"] = "";
        $this->dadosboleto["valor_unitario"] = "";
        $this->dadosboleto["nosso_numero"] = "";
        if ($this->uses !== false)
            foreach($this->uses as $modelClass)
                $this->$modelClass = ClassRegistry::init($modelClass);
    }
    
    function iniciar($despesaId = false,$usuario,$turma) {
        if($despesaId)
            $this->despesaId = $despesaId;
        $this->usuario = $usuario;
        $this->turma = $turma;
        $this->bancoConta = $this->BancoConta->read(null, $this->turma['Turma']['banco_conta_id']);
        if($this->bancoConta) {
            $this->_configurarConta();
            $this->DespesaPagamento->recursive = 1;
            $ultimaDespesaPagamentoCriada = $this->DespesaPagamento->find('first', array(
                'order' => array('DespesaPagamento.id DESC'),
                'conditions' => array(
                    'Pagamento.tipo' => 'boleto',
                    'DespesaPagamento.despesa_id' => $this->despesaId
                )
            ));
            if($ultimaDespesaPagamentoCriada == false)
                $this->_configurar_primeira_via();
            else
                $this->_configurar_segunda_via($ultimaDespesaPagamentoCriada['DespesaPagamento']['pagamento_id']);
            $this->_configurarCodigoDeBarras();
        } else {
            $this->Session->setFlash('Erro ao criar boleto', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/");
        }
    }
    
    function obterCodigoDeBarras() {
        return $this->dadosboleto["linha_digitavel"];
    }
    
    private function _configurarConta() {
        $this->bancoConta['BancoConta']['cedente'] = utf8_decode($this->bancoConta['BancoConta']['cedente']);
        $this->dadosboleto = array_merge($this->dadosboleto,$this->bancoConta['BancoConta']);
        $this->dadosboleto['nome_configuracao'] = $this->bancoConta['Banco']['nome_configuracao'];
        $this->dadosboleto["ponto_venda"] = $this->bancoConta['BancoConta']['agencia'];
        $this->dadosboleto["codigo_banco"] = $this->bancoConta['Banco']['codigo'];
        $this->dadosboleto["carteira_descricao"] = strtoupper($this->bancoConta['BancoConta']['carteira_descricao']);
        $this->dadosboleto["identificacao"] = $this->bancoConta['BancoConta']['cedente'];
        eval("\$this->dadosboleto['demonstrativo2'] = utf8_decode(\"{$this->bancoConta['BancoConta']['instrucoes_1']}\");");
        eval("\$this->dadosboleto['instrucoes2'] = utf8_decode(\"{$this->bancoConta['BancoConta']['instrucoes_2']}\");");
        $cpfCnpj = strlen($this->bancoConta['BancoConta']['cpf_cnpj']) == 11 ? 'CPF' : 'CNPJ';
        $this->dadosboleto["cpf_cnpj"] = $cpfCnpj . ": " . $this->formatCPFCNPJ($this->bancoConta['BancoConta']['cpf_cnpj']);
        $this->dadosboleto["endereco"] = $this->bancoConta['BancoConta']['endereco'];
        $this->dadosboleto["cidade_uf"] = "{$this->bancoConta['BancoConta']['cidade']}/{$this->bancoConta['BancoConta']['uf']}";
    }

    function gerarBoleto($webroot) {
        $dadosboleto = $this->dadosboleto;
        include(APP . $this->includePath . "layout_{$this->dadosboleto['nome_configuracao']}.php");
    }
    
    private function _configurar_primeira_via() {
        $ultimoBoletoGerado = $this->_obterUltimoNumeroBoleto();
        $numeroDoProximoBoleto = (int) ($ultimoBoletoGerado[0]['ordemDoUltimoBoleto']) + 1;
        $this->FormandoProfile->recursive = -1;
        $formandoProfile = $this->FormandoProfile->find('first', array(
            'conditions' => array('usuario_id' => $this->usuario['Usuario']['id'])
        ));
        $despesa = $this->Despesa->read(null,$this->despesaId);
        $this->dadosboleto['codigoFormando'] = $formandoProfile['FormandoProfile']['codigo_formando'];
        $this->dadosboleto['turmaId'] = $this->turma['Turma']['id'];
        $this->dadosboleto['usuarioId'] = $formandoProfile['FormandoProfile']['usuario_id'];
        $this->dadosboleto['usuarioCPF'] = $formandoProfile['FormandoProfile']['cpf'];
        $this->dadosboleto['ordemBoleto'] = $numeroDoProximoBoleto;
        $this->dadosboleto['demonstrativo2'] = "AP&Oacute;S O VENCIMENTO APLICAR MULTA DE {$this->turma['Turma']['multa_por_atraso']} SOBRE O VALOR. INCLUIR JUROS/MORA de {$this->turma['Turma']['juros_mora']} AO DIA.";
        $this->dadosboleto['instrucoes2'] = "AP&Oacute;S O VENCIMENTO APLICAR MULTA DE {$this->turma['Turma']['multa_por_atraso']} SOBRE O VALOR. INCLUIR JUROS/MORA de {$this->turma['Turma']['juros_mora']} AO DIA.";;
        if(empty($this->dataVencimento))
            $this->dadosboleto["data_vencimento"] = date('d/m/Y', strtotime($despesa['Despesa']['data_vencimento']));
        else
            $this->dadosboleto["data_vencimento"] = $this->dataVencimento;
        $this->dadosboleto["valor_boleto"] = number_format($despesa['Despesa']['valor'] + $despesa['Despesa']['correcao_igpm'] + $this->multa, 2, ',', '.');
        $this->dadosboleto["sacado"] = $this->usuario['Usuario']['nome'];
        $pagamento = array(
            'usuario_id' => $this->usuario['Usuario']['id'],
            'valor_nominal' => $despesa['Despesa']['valor'] + $despesa['Despesa']['correcao_igpm'],
            'codigo' => $this->gerarNumeroDocumento(),
            'dt_cadastro' => date('Y-m-d H:i:s'),
            'dt_vencimento' => $despesa['Despesa']['data_vencimento'],
            'tipo' => 'boleto',
            'status' => 'aberto',
        );

        $despesa_pagamento = array(
            'despesa_id' => $despesa['Despesa']['id'],
            'valor' => $despesa['Despesa']['valor'],
            'multa' => $despesa['Despesa']['multa'],
            'correcao_igpm' => $despesa['Despesa']['correcao_igpm'] + 0
        );

        $dados['Pagamento'] = $pagamento;
        $dados['DespesaPagamento'] = array($despesa_pagamento);

        $this->Pagamento->bindModel(array('hasMany' => array('DespesaPagamento' =>
                array('foreignKey' => 'pagamento_id'))));

        $this->Despesa->bindModel(array('hasMany' => array('DespesaPagamento' =>
                array('foreignKey' => 'despesa_id'))));
        if ($numeroDoProximoBoleto > 999) {
            $this->Session->setFlash('Erro ao criar gerar boleto', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/");
        } elseif(!$this->Pagamento->saveAll($dados)) {
            $this->Session->setFlash('Erro ao criar gerar boleto', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/");
        }
    }
    
    private function _configurar_segunda_via($pagamentoId) {
        $this->_atualizar_pagamento($pagamentoId);
        $pagamento = $this->Pagamento->read(null,$pagamentoId);
        $this->FormandoProfile->recursive = -1;
        $formandoProfile = $this->FormandoProfile->find('first', array(
            'conditions' => array('usuario_id' => $this->usuario['Usuario']['id'])
        ));
        $this->dadosboleto['codigoFormando'] = $formandoProfile['FormandoProfile']['codigo_formando'];
        $this->dadosboleto['turmaId'] = $this->turma['Turma']['id'];
        $this->dadosboleto['usuarioId'] = $formandoProfile['FormandoProfile']['usuario_id'];
        $this->dadosboleto['usuarioCPF'] = $formandoProfile['FormandoProfile']['cpf'];
        $this->dadosboleto['ordemBoleto'] = substr($pagamento['Pagamento']['codigo'], -3);
        $this->dadosboleto['demonstrativo2'] = "AP&Oacute;S O VENCIMENTO APLICAR MULTA DE {$this->turma['Turma']['multa_por_atraso']} SOBRE O VALOR. INCLUIR JUROS/MORA de {$this->turma['Turma']['juros_mora']} AO DIA.";
        $this->dadosboleto['instrucoes2'] = "AP&Oacute;S O VENCIMENTO APLICAR MULTA DE {$this->turma['Turma']['multa_por_atraso']} SOBRE O VALOR. INCLUIR JUROS/MORA de {$this->turma['Turma']['juros_mora']} AO DIA.";;
        if(empty($this->dataVencimento))
            $this->dadosboleto["data_vencimento"] = date('d/m/Y', strtotime($pagamento['Pagamento']['dt_vencimento']));
        else
            $this->dadosboleto["data_vencimento"] = $this->dataVencimento;
        $this->dadosboleto["valor_boleto"] = number_format($pagamento['Pagamento']['valor_nominal'] + $this->multa, 2, ',', '.');
        $this->dadosboleto["sacado"] = $this->usuario['Usuario']['nome'];
    }
    
    private function _atualizar_pagamento($pagamentoId) {
        $this->Pagamento->recursive = 2;
        $this->Pagamento->bindModel(array('hasMany' => array('DespesaPagamento' =>
                array('foreignKey' => 'pagamento_id'))));
        $this->Despesa->bindModel(array('hasMany' => array('DespesaPagamento' =>
                array('foreignKey' => 'despesa_id'))));
        $pagamento = $this->Pagamento->read(null,$pagamentoId);
        if (empty($pagamento)) {
            $this->Session->setFlash('Ocorreu um erro ao gerar o boleto', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/area_financeira");
        }
        if(count($pagamento['DespesaPagamento']) > 0) {
            $despesa = $pagamento['DespesaPagamento'][0]['Despesa'];
            if ($despesa['valor'] + $despesa['correcao_igpm'] + 0 != $pagamento['Pagamento']['valor_nominal'] + 0) {
                $this->Pagamento->updateAll(
                    array('Pagamento.valor_nominal' => $despesa['valor'] + $despesa['correcao_igpm']),
                    array('Pagamento.id' => $pagamento['Pagamento']['id'])
                );
                $this->DespesaPagamento->updateAll(
                    array(
                        'DespesaPagamento.correcao_igpm' => $despesa['correcao_igpm'],
                        'DespesaPagamento.valor' => $despesa['valor']
                    ),
                    array('DespesaPagamento.id' => $pagamento['DespesaPagamento'][0]['id'])
                );
            }
        }
    }
    
    private function _obterUltimoNumeroBoleto($codigo = false) {
        if (!$codigo) {
            $numero = $this->Pagamento->find('first', array(
                'conditions' => array(
                    'usuario_id' => $this->usuario['Usuario']['id'],
                    "tipo like('boleto%')"),
                'fields' => array('count(0) as ordemDoUltimoBoleto')
            ));
            $codigo = str_pad((int) $numero[0]['ordemDoUltimoBoleto'] + 1, 3, '0', STR_PAD_LEFT);
        } else {
            $numero[] = array('ordemDoUltimoBoleto' => $codigo);
            $codigo = str_pad($codigo, 3, '0', STR_PAD_LEFT);
        }

        $verificacao = $this->Pagamento->find('first', array(
            'conditions' => array(
                'usuario_id' => $this->usuario['Usuario']['id'],
                "tipo like('boleto%')",
                'IFNULL(RIGHT(codigo,3),0)' => $codigo)
        ));

        if ($verificacao)
            $numero = $this->_obterUltimoNumeroBoleto($numero[0]['ordemDoUltimoBoleto'] + 1);

        return $numero;
    }

    private function _configurarCodigoDeBarras() {
        require_once(APP . $this->includePath . "funcoes_{$this->dadosboleto['nome_configuracao']}.php");
        if ($this->dadosboleto['nome_configuracao'] == 'santander_banespa') {
            $nummoeda = "9";
            $fixo = "9";
            $ios = "0";
            $this->dadosboleto['codigo_banco_com_dv'] = geraCodigoBanco($this->dadosboleto['codigo_banco']);
            $fator_vencimento = fator_vencimento($this->dadosboleto["data_vencimento"]);
            $valor = formata_numero(str_replace('.', '', $this->dadosboleto["valor_boleto"]), 10, 0, "valor");
            $codigocliente = formata_numero($this->dadosboleto["codigo_cliente"], 7, 0);
            if(empty($this->dadosboleto["nosso_numero"]))
                $this->dadosboleto["nosso_numero"] = $this->gerarNumeroDocumento();
            $this->dadosboleto["numero_documento"] = $this->dadosboleto["nosso_numero"];
            $this->dadosboleto["data_documento"] = date("d/m/Y");
            $this->dadosboleto["data_processamento"] = date("d/m/Y");
            $barra = "{$this->dadosboleto['codigo_banco']}{$nummoeda}{$fator_vencimento}{$valor}{$fixo}" .
                    "{$codigocliente}{$this->dadosboleto["nosso_numero"]}{$ios}" .
                    "{$this->dadosboleto["carteira"]}";
            $dv = digitoVerificador_barra($barra);
            $linha = substr($barra, 0, 4) . $dv . substr($barra, 4);
            $this->dadosboleto["codigo_barras"] = $linha;
            $this->dadosboleto["linha_digitavel"] = monta_linha_digitavel($linha);
        } elseif($this->dadosboleto['nome_configuracao'] == 'itau') {
            $nummoeda = "9";
            $this->dadosboleto['codigo_banco_com_dv'] = geraCodigoBanco($this->dadosboleto['codigo_banco']);
            $fator_vencimento = fator_vencimento($this->dadosboleto["data_vencimento"]);
            $valor = formata_numero(str_replace('.', '', $this->dadosboleto["valor_boleto"]), 10, 0, "valor");
            $codigocliente = formata_numero($this->dadosboleto["codigo_cliente"], 7, 0);
            $conta = formata_numero($this->dadosboleto["conta"],5,0);
            $conta_dv = formata_numero($this->dadosboleto["conta_dv"],1,0);
            $agencia = formata_numero($this->dadosboleto["agencia"],4,0);
            if(empty($this->dadosboleto["nosso_numero"]))
                $this->dadosboleto["nosso_numero"] = $this->gerarNumeroDocumento();
            $this->dadosboleto["numero_documento"] = $this->dadosboleto["nosso_numero"];
            $nnum = formata_numero($this->dadosboleto["nosso_numero"],8,0);
            $mod1 = modulo_10($agencia.$conta.$this->dadosboleto["carteira"].$nnum);
            $mod2 = modulo_10($agencia.$conta);
            $this->dadosboleto["data_documento"] = date("d/m/Y");
            $this->dadosboleto["data_processamento"] = date("d/m/Y");
            $barra = "{$this->dadosboleto['codigo_banco']}{$nummoeda}{$fator_vencimento}{$valor}".
                "{$this->dadosboleto["carteira"]}{$nnum}".modulo_10($agencia.$conta.$this->dadosboleto["carteira"].$nnum).
                $agencia.$conta.modulo_10($agencia.$conta).'000';
            $dv = digitoVerificador_barra($barra);
            $linha = substr($barra, 0, 4) . $dv . substr($barra, 4);
            $linha = substr($barra,0,4).$dv.substr($barra,4,43);
            $nossonumero = $this->dadosboleto["carteira"].'/'.$nnum.'-'.modulo_10($agencia.$conta.$this->dadosboleto["carteira"].$nnum);
            //$this->dadosboleto["nosso_numero"] = $nossonumero;
            $this->dadosboleto["agencia_codigo"] = "{$agencia} / {$conta}-".modulo_10($agencia.$conta);
            $this->dadosboleto["codigo_barras"] = $linha;
            $this->dadosboleto["linha_digitavel"] = monta_linha_digitavel($linha);
        } elseif($this->dadosboleto['nome_configuracao'] == 'bradesco') {
            $nummoeda = "9";
            $this->dadosboleto['codigo_banco_com_dv'] = geraCodigoBanco($this->dadosboleto['codigo_banco']);
            $fator_vencimento = fator_vencimento($this->dadosboleto["data_vencimento"]);
            $valor = formata_numero(str_replace('.', '', $this->dadosboleto["valor_boleto"]), 10, 0, "valor");
            $agencia = formata_numero($this->dadosboleto["agencia"],4,0);
            $conta = formata_numero($this->dadosboleto["conta"],7,0);
            $conta_dv = formata_numero($this->dadosboleto["conta_dv"],1,0);
            if(empty($this->dadosboleto["nosso_numero"]))
                $this->dadosboleto["nosso_numero"] = $this->gerarNumeroDocumento();
            $this->dadosboleto["numero_documento"] = $this->dadosboleto["nosso_numero"];
            $nnum = formata_numero($this->dadosboleto["carteira"],2,0).formata_numero($this->dadosboleto["nosso_numero"],11,0);
            $codigocliente = formata_numero($this->dadosboleto["codigo_cliente"], 7, 0);
            $agencia_dv = formata_numero($this->dadosboleto["agencia_dv"],1,0);
            $this->dadosboleto["data_documento"] = date("d/m/Y");
            $this->dadosboleto["data_processamento"] = date("d/m/Y");
            $dv = digitoVerificador_barra("{$this->dadosboleto['codigo_banco']}{$nummoeda}{$fator_vencimento}{$valor}{$agencia}{$nnum}{$conta}".'0', 9, 0);
            
            $linha = "{$this->dadosboleto['codigo_banco']}{$nummoeda}{$dv}{$fator_vencimento}{$valor}{$agencia}{$nnum}{$conta}"."0";
            $this->dadosboleto["agencia_codigo"] = "{$agencia}-{$agencia_dv} / {$conta}-{$conta_dv}";
            $this->dadosboleto["codigo_barras"] = $linha;
            $this->dadosboleto["linha_digitavel"] = monta_linha_digitavel($linha);
        } elseif($this->dadosboleto['nome_configuracao'] == 'hsbc') {
            $nummoeda = "9";
            $this->dadosboleto['codigo_banco_com_dv'] = geraCodigoBanco($this->dadosboleto['codigo_banco']);
            $fator_vencimento = fator_vencimento($this->dadosboleto["data_vencimento"]);
            $juliano = dataJuliano($this->dadosboleto["data_vencimento"]);
            $valor = formata_numero(str_replace('.', '', $this->dadosboleto["valor_boleto"]), 10, 0, "valor");
            $conta = 4453123;
            $conta_dv = formata_numero($this->dadosboleto["conta_dv"],1,0);
            if(empty($this->dadosboleto["nosso_numero"]))
                $this->dadosboleto["nosso_numero"] = $this->gerarNumeroDocumento();
            if(substr($this->dadosboleto["nosso_numero"], 0, 1) == 0)
                $this->dadosboleto["nosso_numero"] = 999999 . substr($this->dadosboleto["nosso_numero"], 1);
            else
                $this->dadosboleto["nosso_numero"] = 99999 . $this->dadosboleto["nosso_numero"];
            $this->dadosboleto["numero_documento"] = $this->dadosboleto["nosso_numero"];
            $nnum = formata_numero($this->dadosboleto["carteira"],2,0).formata_numero($this->dadosboleto["nosso_numero"],11,0);
            $codigocliente = 8351202;
            $agencia_dv = formata_numero($this->dadosboleto["agencia_dv"],1,0);
            $this->dadosboleto["data_documento"] = date("d/m/Y");
            $this->dadosboleto["data_processamento"] = date("d/m/Y");
            $primeiroDigito = digitoVerificador_nossonumero($this->dadosboleto["nosso_numero"]); 
            $dataArrumada = str_split(str_replace("/", "", $this->dadosboleto["data_vencimento"]), 2);
            unset($dataArrumada[2]);
            $soma = (int) $this->dadosboleto["nosso_numero"] . $primeiroDigito . 4 + (int) $conta + (int) implode($dataArrumada);
            $terceiroDigito = modulo_11($soma);
            $this->dadosboleto["nosso_numero"] = $this->dadosboleto["nosso_numero"] . $primeiroDigito . 4 . $terceiroDigito; 
            //$dv = digitoVerificador_barra("{$this->dadosboleto['codigo_banco']}{$nummoeda}{$fator_vencimento}{$valor}{$agencia}{$nnum}{$conta}".'0', 9, 0);
            $criaDac = "{$this->dadosboleto['codigo_banco']}{$nummoeda}{$fator_vencimento}{$valor}{$conta}{$this->dadosboleto["numero_documento"]}{$juliano}"."2";
            $dac = modulo_11($criaDac);
            if($dac == 0 || $dac == 10)
                $dac = 1;
            //$linha = "{$this->dadosboleto['codigo_banco']}{$nummoeda}{$conta}{$dv}{$conta}{$this->dadosboleto["nosso_numero"]}{$dv}{$this->dadosboleto["nosso_numero"]}{$vencjuliano}"."0"."{$dv}{$dac}{$fator_vencimento}{$valor}";
            $linha = "{$this->dadosboleto['codigo_banco']}{$nummoeda}{$dac}{$fator_vencimento}{$valor}{$conta}{$this->dadosboleto["numero_documento"]}{$juliano}"."2";
            //personalização do hsbc entre aspas - agência retirada "{$agencia}-{$agencia_dv} /"
            $this->dadosboleto["agencia_codigo"] = "{$conta}-{$conta_dv}";
            $this->dadosboleto["codigo_barras"] = $linha;
            $this->dadosboleto["linha_digitavel"] = monta_linha_digitavel($linha);
        }
    }

    function gerarNumeroDocumento() {
        if ($this->dadosboleto['nome_configuracao'] == 'santander_banespa'){
            return '0' . str_pad($this->dadosboleto['turmaId'], 5, 0, STR_PAD_LEFT) .
                str_pad($this->dadosboleto['codigoFormando'], 3, '0', STR_PAD_LEFT) .
                str_pad($this->dadosboleto['ordemBoleto'], 4, '0', STR_PAD_LEFT);
        } else {
            return str_pad($this->dadosboleto['usuarioId'], 5, 0, STR_PAD_LEFT) .
                str_pad($this->dadosboleto['ordemBoleto'], 3, 0, STR_PAD_LEFT);
        }
    }
    
    function formatCPFCNPJ($string) {
        $output = preg_replace("[' '-./ t]", '', $string);
        $size = (strlen($output) - 2);
        if ($size != 9 && $size != 12)
            return false;
        $mask = ($size == 9) ? '###.###.###-##' : '##.###.###/####-##';
        $index = -1;
        for ($i = 0; $i < strlen($mask); $i++):
            if ($mask[$i] == '#')
                $mask[$i] = $output[++$index];
        endfor;
        return $mask;
    }


}

?>