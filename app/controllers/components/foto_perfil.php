<?php

class FotoPerfilComponent extends Object {
    
    var $tamanhos = array(80,160);
    
    var $uses = array('Usuario');
    
    var $uploadDir = false;
    
    var $SimpleImage = false;
	
    function __construct() {
        $this->webDir = APP."webroot/";
        $this->uploadDir = "upload/foto_perfil";
        if($this->uses !== false)
            foreach($this->uses as $modelClass)
                $this->$modelClass = ClassRegistry::init($modelClass);
    }
    
    private function getSimpleImage() {
        if(!$this->SimpleImage) {
            App::import('Component', 'SimpleImage');
            $this->SimpleImage = new SimpleImageComponent();
        }
        return $this->SimpleImage;
    }
    
    function criarPastas() {
        $tamanhos = array();
        foreach($this->tamanhos as $tamanho) {
            $dir = "{$this->webDir}{$this->uploadDir}_{$tamanho}";
            if(!is_dir($dir)) {
                if(mkdir($dir))
                    $tamanhos[] = $tamanho;
            } else {
                $tamanhos[] = $tamanho;
            }
        }
        $this->tamanhos = $tamanhos;
    }
    
    function obterArquivoPorTamanho($arquivo,$tamanho) {
        $path = "{$this->webDir}{$this->uploadDir}_{$tamanho}/{$arquivo}";
        return file_exists($path) ? "{$this->uploadDir}_{$tamanho}/{$arquivo}" : false;
    }
    
    function obterArquivos($diretorioFotoPerfil) {
        $arquivos = array();
        if(file_exists("{$this->webDir}{$diretorioFotoPerfil}")) {
            list($arquivo,$extensao) = $this->obterInfoDoArquivo("{$this->webDir}{$diretorioFotoPerfil}");
            foreach($this->tamanhos as $tamanho) {
                $f = $this->obterArquivoPorTamanho("{$arquivo}{$extensao}", $tamanho);
                if($f) $arquivos[$tamanho] = $f;
            }
            $max = max($arquivos);
            $arquivos[$max+1] = $diretorioFotoPerfil;
        }
        return $arquivos;
    }
    
    function criarArquivos($src,$arquivo,$extensao) {
        $this->criarPastas();
        $simpleImage = $this->getSimpleImage();
        $return = false;
        try {
            if(!file_exists("{$this->webDir}{$this->uploadDir}/{$arquivo}{$extensao}")) {
                $simpleImage->load($src);
                $simpleImage->save("{$this->webDir}{$this->uploadDir}/{$arquivo}{$extensao}",100);
                $return = true;
            } else {
                $return = true;
            }
            foreach($this->tamanhos as $tamanho) {
                if(!$this->obterArquivoPorTamanho("{$arquivo}{$extensao}", $tamanho)) {
                    $simpleImage->load($src);
                    $simpleImage->fit_to_width($tamanho);
                    $simpleImage->save("{$this->webDir}{$this->uploadDir}_{$tamanho}/{$arquivo}{$extensao}",100);
                }
            }
        } catch (Exception $ex) {
            
        }
        return $return;
    }
    
    function removerDados($usuario) {
        //list($arquivo,$ext) = $this->obterDadosDoArquivo(APP.$usuario['Usuario']['diretorio_foto_perfil']);
        //$this->removerArquivosAdicionais($arquivo,$ext);
        //if(file_exists("{$arquivo}{$ext}")) unlink("{$arquivo}{$ext}");
        $this->Usuario->id = $usuario['Usuario']['id'];
        $this->Usuario->saveField('diretorio_foto_perfil',null);
    }
    
    function removerArquivosAdicionais($arquivo,$ext) {
        foreach($this->tamanhos as $tamanho)
            if(file_exists("{$arquivo}_{$tamanho}px{$ext}")) unlink("{$arquivo}_{$tamanho}px{$ext}");
        return;
    }
    
    function obterInfoDoArquivo($arquivo) {
        $d = explode(".",$arquivo);
        if(count($d) > 1) {
            $ext = "." . array_pop($d);
            $fl = implode(".",$d);
        } else {
            $ext = "";
            $fl = $arquivo;
        }
        $p = explode("/",$fl);
        if(count($p) > 1)
            $file = array_pop($p);
        else
            $file = $fl;
        return array($file,$ext);
    }
    
    function obterExtensaoDaImagem($file) {
        $simpleImage = $this->getSimpleImage();
        $ext = false;
        try {
            $simpleImage->load($file);
            $info = $simpleImage->get_original_info();
            $ext = isset($info['format']) ? ".".$info['format'] : false;
        } catch (Exception $ex) {
        }
        return $ext;
    }
}