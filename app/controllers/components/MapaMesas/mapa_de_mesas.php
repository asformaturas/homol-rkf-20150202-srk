<?php
class MapaDeMesasComponent extends Object {

    const MESA_TIPO_CONTRATO = 1;
    const MESA_TIPO_CAMPANHA = 2;
    const MESA_TIPO_CHECKOUT = 3;

    public function checarPermissaoParaEscolhaMesa($usuarioId, $eventoId) {

    }

    public function ajustarMapaParaEvento($turmaId, $eventoId, $mapaId) {

    }

     /**
     * Retorna numero de mesas que determinado usuário adiquiriu
     * @param  integer $usuarioId Id do usuário a ser consultado
     * @param  integer $eventoId  Id do evento
     * @param  integer $mesaTipo  Tipo de mesa que deve ser pesquisada
     * @return  array Tuplas com numero de mesas adquiridas em: 
     *                       Contrato; Campanha; Checkout;
     */
    public function retornaMesasCompradasPorUsuario($usuarioId, $eventoId, $mesaTipo = 0) {
        $retorno = array();

        if(!$mesaTipo || $mesaTipo==self::MESA_TIPO_CONTRATO){
            $this->TurmaMapa = ClassRegistry::init('TurmaMapa');
            $campos = array(
                'fields'     => 'Turma.mesas_contrato',
                'conditions' => array('TurmaMapa.evento_id' => $eventoId)
            );
            $this->TurmaMapa->bindTurma();
            $turma = $this->TurmaMapa->find('first',$campos);

            $retorno['mesasContrato'] = $turma['Turma']['mesas_contrato'];
        }

        if(!$mesaTipo || $mesaTipo==self::MESA_TIPO_CAMPANHA){
            $this->ViewRelatorioExtras = ClassRegistry::init('ViewRelatorioExtras');
            
            $conditions = array(
                'ViewRelatorioExtras.usuario_id' => $usuarioId,
                'ViewRelatorioExtras.cancelada'  => 0,
                'ViewRelatorioExtras.evento_id'  => $eventoId
            );

            $extras = $this->ViewRelatorioExtras->find(
                'all', 
                array('conditions' => $conditions)
            );

            $mesasCampanhas = 0;
            foreach($extras as $extra){
                $quantidadeMesas     = $extra['ViewRelatorioExtras']['quantidade_mesas'];
                $quantidadeComprada  = $extra['ViewRelatorioExtras']['quantidade_total'];
                $mesasCampanhas     += $quantidadeMesas * $quantidadeComprada;
            }
            $retorno['mesasCampanhas'] = $mesasCampanhas;
        }

        if(!$mesaTipo || $mesaTipo==self::MESA_TIPO_CHECKOUT){
            $this->CheckoutUsuarioItem = ClassRegistry::init('CheckoutUsuarioItem');

            $conditions = array(
                    'CheckoutUsuarioItem.usuario_id' => $usuarioId,
                    'CheckoutItem.evento_id'         => $eventoId
                    );

            $extrasCheckout = $this->CheckoutUsuarioItem->find(
                'all', 
                array('conditions' => $conditions)
            );

            $mesasCheckout = 0;
            #TODO
            #Varificar possibilidade de usar "sum" ao invés de "all"
            #bucando traser apenas a soma das mesas
            foreach($extrasCheckout as $extraCheckout){
                if($extraCheckout['CheckoutUsuarioItem']['retirou'] == 1)
                    $mesasCheckout += $extraCheckout['CheckoutUsuarioItem']['quantidade'] * $extraCheckout['CheckoutItem']['quantidade_mesas'];
            }
            $retorno['mesasCheckout'] = $mesasCheckout;
        }
        return $retorno ;
    }

    public function salvarLayoutMesas($turma_id, $evento_id, $mapa_id ) {

    }

    public function salvarEscolhaDeMesas($usuarioId, $turmaMapaId, $eventoId, $mesasEscolhidas) {
        $this->FormandoMapaMesa = ClassRegistry::init('FormandoMapaMesa');
        $this->TurmaMapa = ClassRegistry::init('TurmaMapa');
        $this->ViewFormandos = ClassRegistry::init('ViewFormandos');

        $usuario = $this->ViewFormandos->findById($usuarioId);
        $codigo_formando = $usuario['ViewFormandos']['codigo_formando'];
        
        $nomes = array();
        foreach($mesasEscolhidas as $mesa){
            $nomes[] = $mesa->nome;
        }
        // Se retornar mesa significa que já estão selecionadas
        $mesas = $this->FormandoMapaMesa->getMesaPorNomeTurmaMapa($nomes, $turmaMapaId, $codigo_formando);
        if($mesas){
            return false;
        }

        $quantidadeMesas = $this->retornaMesasCompradasPorUsuario($usuarioId, $eventoId);
        $quantidadeMesasTotal = array_sum($quantidadeMesas);

        if(count($mesasEscolhidas) > $quantidadeMesasTotal){
            return false;
        }

        // Desativa antigas escolhas de  mesas do evento específico
        $this->FormandoMapaMesa->updateAll(
            array('status' => 0),
            array(
                'codigo_formando' => $codigo_formando,
                'turma_mapa_id'   => $turmaMapaId
            )
        );

        // Persiste novas informações na base de dados
        $criacao = date('Y-m-d H:i:s');
        foreach($mesasEscolhidas as $mesa) {
            $this->FormandoMapaMesa->create();
            $this->FormandoMapaMesa->set('turma_mapa_id'  , $turmaMapaId);
            $this->FormandoMapaMesa->set('codigo_formando', $codigo_formando);
            $this->FormandoMapaMesa->set('mesa'           , $mesa->nome);
            $this->FormandoMapaMesa->set('criacao'        , $criacao);
            $this->FormandoMapaMesa->set('referencia'     , $mesa->letra.$mesa->numero);
            $this->FormandoMapaMesa->save();
        }
    }

    public function carregarMesasSelecionadas($turma_mapa_id){
        $this->FormandoMapaMesa = ClassRegistry::init('FormandoMapaMesa');

        $this->FormandoMapaMesa->bindUsuario();
        $mesasSelecionadas = $this->FormandoMapaMesa->find('all', array(
            'conditions' => array(
                'FormandoMapaMesa.status' => 1,
                'FormandoMapaMesa.turma_mapa_id' => $turma_mapa_id
            ),
            'fields' => array(
                'FormandoMapaMesa.mesa',
                'ViewFormandos.nome',
                'FormandoMapaMesa.codigo_formando',
            ),
        ));

        $mesas = array();
        foreach($mesasSelecionadas as $mesa){
            $mesas[] = array(
                'nome' => $mesa['FormandoMapaMesa']['mesa'],
                'texto' => $mesa['ViewFormandos']['nome'],
                'idFormando' => $mesa['FormandoMapaMesa']['codigo_formando'],
            );
        }
        $mesas = json_encode($mesas);

        return $mesas;
    }

    public function gerarAgendamentos($evento_id,$data_inicio,$data_finalizacao,
        $horas_dia) {

        // Retornar id de turma para seguintes consultas
        $this->TurmaMapa = ClassRegistry::init('TurmaMapa');
        $args = array(
                'fields'     => 'TurmaMapa.turma_id',
                'conditions' => array('TurmaMapa.evento_id' => $eventoId),
                'recursive' => -1
            );
        $turma = $this->TurmaMapa->find('first',$campos);

        var_dump($turma);die;
    }

    public function retornarIntervalos() {
        return array( 10 => '10 minutos', 20 => '20 minutos',
            30 => '30 minutos', 45 => '45 minutos', 60 => '60 minutos');
    }

    public function retornarEventosPorTurma($turmaId) {
        $this->FormandoMapaMesa = ClassRegistry::init('TurmaMapa');
        $this->TurmaMapa = new TurmaMapa();
        $this->TurmaMapa->bindEvento();
        $args = array(
            'fields'     => array('Evento.id,Evento.nome'),
            'conditions' => array(
                'TurmaMapa.turma_id'=>$turmaId , 
                'TurmaMapa.status'=>1
            )
        );
        $data = $this->TurmaMapa->find('all', $args);

        $eventos = array();
        foreach ($data as $evento)
            $eventos[$evento['Evento']['id']] = $evento['Evento']['nome'];

        return $eventos;
    }

    #TODO - Assim que possível passar isso para uma Model (Discutir a melhor forma)
    public function retornarFormandosPorTurma($turmaId) {
        $args = array(
                'fields' => array('FormandoProfile.usuario_id, Usuario.nome',
                   'FormandoProfile.data_adesao',
                   'FormandoProfile.realizou_checkout',
                   'Formandos.codigo_formando',
                   'FormandoMesas.id',
                   'FormandoMesas.referencia',
                   'Usuario.grupo'),
                'recursive'  => 1,
                'conditions' => array('FormandoProfile.data_adesao IS NOT NULL'),
                'order'      => array('FormandoProfile.data_adesao'),
                'joins'      => array(
                    array(
                        'table' => 'turmas_usuarios',
                        'alias' => 'TurmaUsuarios',
                        'type'  => 'INNER',
                        'conditions' => array(
                            'TurmaUsuarios.turma_id' => $turmaId,
                            'TurmaUsuarios.usuario_id = FormandoProfile.usuario_id'
                            )
                    ),
                    array(
                        'table' => 'vw_formandos',
                        'fields'=> array('codigo_formando'),
                        'alias' => 'Formandos',
                        'type'  => 'INNER',
                        'conditions' => array(
                            'Formandos.id = FormandoProfile.usuario_id'
                        )
                    ),
                    array(
                        'table' => 'formando_mapa_mesas',
                        'fields'=> array('codigo_formando'),
                        'alias' => 'FormandoMesas',
                        'type'  => 'LEFT',
                        'conditions' => array(
                            'FormandoMesas.codigo_formando 
                            = Formandos.codigo_formando',
                            'FormandoMesas.status' => 1
                        )
                    )
                )
        );
        
        $this->FormandoProfile = ClassRegistry::init('FormandoProfile');
        $formandosProfiles = $this->FormandoProfile->find('all',$args);

        $formandos = array();
        foreach ($formandosProfiles as $key => $formando) {
            $usuario_id = $formando['FormandoProfile']['usuario_id'];
            $formandos[$usuario_id]['Usuario'] = $formando['Usuario'];
            $formandos[$usuario_id]['Formandos'] = $formando['Formandos'];
            $formandos[$usuario_id]['FormandoProfile'] = $formando['FormandoProfile'];
            $formandos[$usuario_id]['FormandoMesas'][] = $formando['FormandoMesas'];
        }
        return $formandos;
    }

    public function salvarAgendamento(MapaDeMesasAgendamento $agendamneto) {

    }

    public function salvarAgendamento($args) {
        $this->MapaMesasAgendamento = ClassRegistry::init(
            'MapaMesasAgendamento');

        $turmaMapaId = $args['turmaMapaId'];
        $dataInicio = $args['dataInicio'];
        $dataFinalizacao = $args['dataFinalizacao'];
        $horaInicio = $args['horaInicio'];
        $horaFinalizacao = $args['horaFinalizacao'];
        $dataAgendamento = (new DateTime())->format('Y-m-d H:i:s');

        // Cacela outros demais agendamnetos para o mapa que possa existir
        $this->MapaMesasAgendamento->cancelarAgendamento($turmaMapaId);

        // Registrar Agendamento
        $this->MapaMesasAgendamento->create();
        $this->MapaMesasAgendamento->set('turma_mapa_id', 
            $args['turmaMapaId']);
        $this->MapaMesasAgendamento->set('data_inicio', 
            $args['dataInicio']);
        $this->MapaMesasAgendamento->set('data_finalizacao', 
            $args['dataFinalizacao']);
        $this->MapaMesasAgendamento->set('hora_inicio',
            $args['horaInicio']);
        $this->MapaMesasAgendamento->set('hora_finalizacao', 
            $args['horaFinalizacao']);
        $this->MapaMesasAgendamento->set('data_agendamento', 
            $dataAgendamento);
        $this->MapaMesasAgendamento->save();
        return $this;
    }

    public function agendamentoFormandos($args) {
        extract($args);

        // Desagenda os demais
        $this->FormandoMapaHorario = ClassRegistry::init('FormandoMapaHorario');
        $this->FormandoMapaHorario->deleteAll(
            array('id_turma_mapa'=>$turmaMapaId)
        );

        $agendamentoComissao = $data1->format('Y-m-d H:i:00');

        reset($formandos);
        for($dataHorario = $data1; $dataHorario <= $data2;) {
            for($i=1; $i <= $formandosPorIntervalo; $i++){
                list($key, $value) = each($formandos);
                if(is_null($key))
                    break;

                $dataAdesao = $formandos[$key]['FormandoProfile']['data_adesao'];
                
                if($formandos[$key]['Usuario']['grupo'] == 'comissao')
                    $dataLiberacao = $data1;
                else
                    $dataLiberacao = $dataHorario;

                $strDataLiberacao = $dataLiberacao->format('Y-m-d H:i:00');
                $formandos[$key]['data_agendada'] = $dataLiberacao->format('d/m/Y H:i');

                $codigoFormando = $formandos[$key]['Formandos']['codigo_formando'];
                
                $this->FormandoMapaHorario->create();
                $this->FormandoMapaHorario->set('id_turma_mapa',$turmaMapaId);
                $this->FormandoMapaHorario->set('codigo_formando',$codigoFormando);
                $this->FormandoMapaHorario->set('data_liberacao',$strDataLiberacao);
                $this->FormandoMapaHorario->save();

                $formandos[$key]['data_adesao'] = 
                    (new DateTime($dataAdesao))->format('d/m/Y');
                $formandos[$key]['status'] = 
                    $this->retornarStatusFormando($formandos[$key]);
                foreach ($formandos[$key]['FormandoMesas'] as $mesa) {
                    $formandos[$key]['referencias'][] = $mesa['referencia'];
                }
                $formandos[$key]['referencias'] = implode(',', $formandos[$key]['referencias']);
            }
            $dataHorario->add(new DateInterval('PT'.$intervaloEscolhas.'M'));

            if($dataHorario->format('H:i') > $horaFinalizacao->format('H:i')){
                $dataHorario->modify('+1 day');                                                                                                                                         
                $dataHorario->setTime($horaInicio->format('H'),$horaInicio->format('i'));
            }
        }

        return $formandos;
    }

    private function retornarStatusFormando($formando) {
        if(!$formando['FormandoProfile']['realizou_checkout'])
            return 'Checkout Pendente';

        if(count($formando['FormandoMesas']) > 0)
            return 'Mesa(s) Escolhidas';

        return 'Agendado';
    }
}