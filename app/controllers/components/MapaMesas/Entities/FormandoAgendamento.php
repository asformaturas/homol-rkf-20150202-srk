<?php

class FormandoAgendamento {

	/**
	 * Identificador único do agendamento
	 * @var Array<MapaDeMesasTurma>
	 */
	private $id;
	
	/**
	 * Dia e hora em que inicia o agendamento
	 * @var DateTime
	 */
	private $dataInicio;

	/**
	 * Dia e hora em que encerra o agendamento
	 * @var DateTime
	 */
	private $dataFinalizacao;

	/**
	 * Intervalo em minutos entre cada grupo de formando
	 * @var Integer
	 */
	private $intervalo;

	/**
	 * Status do agendamento
	 * @var Integer
	 */
	private $status;

	public function __construct() {
	}

	public function setDataInicio(DateTime $dataInicio) {
		$this->dataInicio = $dataInicio;
	}

	public function getDataInicio() {
		return $this->dataInicio;
	}

	public function setDataFinalizacao(DateTime $dataFinalizacao) {
		$this->dataFinalizacao = $dataFinalizacao;
	}

	public function getDataFinalizacao() {
		return $this->dataFinalizacao;
	}

	public function addTurma(MapaDeMesasTurma $turma) {
		$this->turmas[] = $turmas;
	}

	public function getTurmas() {
		return $this->turmas;
	} 

	public function setStatus($status) {
		$this->status = (bool) $status;
	}
}