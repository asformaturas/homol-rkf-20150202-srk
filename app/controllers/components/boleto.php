<?php

class BoletoComponent extends Object {

    var $versao = 0;
    var $dadosboleto = array();
    var $codigoFormando;
    var $turmaId;
    var $ordemBoleto;
    var $turmasItau = array();
    var $turmasHsbc = array();

    function BoletoComponent() {
        $this->turmasItau = array('5233','6208','6334');
        $this->turmasHsbc = array('1');
        //Dados do cliente
        $this->dadosboleto["endereco1"] = "";
        $this->dadosboleto["endereco2"] = "";

        // INFORMACOES PARA O CLIENTE
        $this->dadosboleto["demonstrativo1"] = "BOLETO REFERENTE A EVENTOS DE FORMATURA.";
        $this->dadosboleto["demonstrativo2"] = "";
        $this->dadosboleto["demonstrativo3"] = "";
        $this->dadosboleto["instrucoes1"] = "BOLETO REFERENTE A EVENTOS DE FORMATURA.";
        $this->dadosboleto["instrucoes2"] = "";
        $this->dadosboleto["instrucoes3"] = "";
        $this->dadosboleto["instrucoes4"] = "";

        // DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
        $this->dadosboleto["quantidade"] = "";
        $this->dadosboleto["valor_unitario"] = "";
        $this->dadosboleto["aceite"] = "N";
        $this->dadosboleto["especie"] = "R$";
        $this->dadosboleto["especie_doc"] = "";

        // DADOS DA SUA CONTA - REAL
        //$this->dadosboleto["codigo_cliente"] = "4346920"; // Código do Cliente (PSK) (Somente 7 digitos)
        //$this->dadosboleto["ponto_venda"] = "4371"; // Ponto de Venda = Agencia
        //$this->dadosboleto["carteira"] = "102";  // Cobrança Simples - SEM Registro
        //$this->dadosboleto["carteira_descricao"] = "COBRAN&Ccedil;A SIMPLES - CSR";  // Descrição da Carteira
        // DADOS PERSONALIZADOS - REAL
        /*
          $this->dadosboleto["agencia"] = "0634"; // Num da agencia, sem digito
          $this->dadosboleto["conta"] = "2741987"; 	// Num da conta, sem digito
          $this->dadosboleto["carteira"] = "57";  // Código da Carteira
         */

        // DADOS PERSONALIZADOS - SANTANDER
        /*
          $this->dadosboleto["codigo_cliente"] = "4346920"; // Código do Cliente (PSK) (Somente 7 digitos)
          $this->dadosboleto["ponto_venda"] = "4371"; // Ponto de Venda = Agencia
          $this->dadosboleto["carteira"] = "102";
          $this->dadosboleto["agencia"] = "4371"; // Num da agencia, sem digito
          $this->dadosboleto["conta"] = "290000062"; 	// Num da conta, sem digito
         */
        $this->dadosboleto["carteira_descricao"] = "COBRAN&Ccedil;A SIMPLES - CSR";

        // SEUS DADOS
        $this->dadosboleto["identificacao"] = "&Aacute;S Eventos";
        $this->dadosboleto["cpf_cnpj"] = "CNPJ: 06.163.144.0001/45";
        $this->dadosboleto["endereco"] = "Avenida Jos&eacute; Maria Whitaker, 882 | Planalto Paulista";
        $this->dadosboleto["cidade_uf"] = "S&atilde;o Paulo/SP";

        $this->dadosboleto["data_documento"] = date("d/m/Y"); // Data de emissão do Boleto
        $this->dadosboleto["data_processamento"] = date("d/m/Y"); // Data de processamento do boleto (opcional)
    }
    

    function imprimir($webroot) {
        $this->dadosboleto["numero_documento"] = $this->gerarNumeroDocumento();
        $this->dadosboleto["nosso_numero"] = $this->dadosboleto["numero_documento"];
        //TODO: mudar para santander
        $layout = $this->retorna_layout($this->turmaId);
        App::import('Vendor', 'PHPBoleto', array('file' => $layout));
        imprimirBoleto($this->dadosboleto, $webroot);
    }

    function imprimirIgpm($webroot) {
        $layout = $this->retorna_layout($this->turmaId);
        App::import('Vendor', 'PHPBoleto', array('file' => $layout));
        imprimirBoleto($this->dadosboleto, $webroot);
    }

    function obterLinhaDigitavel() {
        if(empty($this->dadosboleto["numero_documento"])) {
            $this->dadosboleto["numero_documento"] = $this->gerarNumeroDocumento();
            $this->dadosboleto["nosso_numero"] = $this->dadosboleto["numero_documento"];
        }
        $layout = $this->retorna_layout($this->turmaId);
        App::import('Vendor', 'PHPBoleto', array('file' => $layout));
        return obterLinhaDigitavel($this->dadosboleto);
    }
    
    function obterDados() {
        if(empty($this->dadosboleto["numero_documento"])) {
            $this->dadosboleto["numero_documento"] = $this->gerarNumeroDocumento();
            $this->dadosboleto["nosso_numero"] = $this->dadosboleto["numero_documento"];
        }
        $layout = $this->retorna_layout($this->turmaId);
        App::import('Vendor', 'PHPBoleto', array('file' => $layout));
        return obterDados($this->dadosboleto);
    }

    /*
     * Método para gerar Código do boleto
     * 
     * cod_formando: Código do formando dentro da turma
     * turma_id : id da turma
     * cod_sequencial: próximoo Código de boleto a ser usado pelo formando
     */
    
    function retorna_layout($turmaId){
        if(in_array($this->turmaId,$this->turmasItau)){
            return 'boleto_itau.php';
        }
        if(in_array($this->turmaId,$this->turmasHsbc)){
            return 'boleto_hsbc.php';
        }
        return 'boleto_santander_banespa.php';
    }

    function gerarNumeroDocumento() {
        if (!in_array($this->turmaId,$this->turmasItau)) {
            $numero_documento = '0' . str_pad($this->turmaId, 5, 0, STR_PAD_LEFT) . str_pad($this->codigoFormando, 3, '0', STR_PAD_LEFT) . str_pad($this->ordemBoleto, 4, '0', STR_PAD_LEFT);
        } else {
            $numero_documento = str_pad($this->dadosboleto['usuarioId'], 5, 0, STR_PAD_RIGHT) . str_pad($this->ordemBoleto, 3, 0, STR_PAD_LEFT);
        }
        return $numero_documento;
    }

    //str_pad($this->dadosboleto['usuarioId'], 4, 0, STR_PAD_RIGHT) . str_pad($this->dadosboleto['ordemBoleto'], 4, 0, STR_PAD_LEFT)
}

?>