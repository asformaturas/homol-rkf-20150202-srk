<?php
class ManipulacaoDeArquivos extends Object {
	var $uses = array('Arquivo');
	
	// Passe um id existente de arquivo e ele fara o download para o usuario
	static function realizarDownload($id) {
		App::import('Model', 'Arquivo');
        $arquivoModel = new Arquivo();

		$arquivoModel->id = $id;
		$arquivo = $arquivoModel->read();
		if(!empty($arquivo)){
			header("Pragma: public"); // required 
			header("Expires: 0"); 
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
			header("Cache-Control: private",false); // required for certain browsers 
			header("Content-Type: " . $arquivo['Arquivo']['tipo']); 
			header("Content-Disposition: attachment; filename=\"" . $arquivo['Arquivo']['nome'] . "\";" ); 
			header("Content-Transfer-Encoding: binary"); 
			header("Content-Length: " . $arquivo['Arquivo']['tamanho']); 
			while (ob_get_level() > 0){
				ob_end_clean();
			}
			flush();
		    readfile($arquivoModel->getArquivo());
		} else {
		    // arquivo não existe no banco ou usuario nao tem permissao para acessa-lo
		    // este erro ocorrerá se alguém modificar os arquivos do diretório sem utilizar o sistema

		}


	}
}
?>