<?php

class ProtocoloComponent extends Object {
	
	var $uses = array('Protocolo','Usuario');
	
	function __construct() {
		if ($this->uses !== false)
			foreach($this->uses as $modelClass)
				$this->$modelClass = ClassRegistry::init($modelClass);
	}
	
	function geraProtocolo($tipo,$usuarioCriador,$usuarioId,$grupo) {
		$retorno = (FALSE);
		$protocolo = str_pad($usuarioId,7,'0',STR_PAD_LEFT);
		if($ultimoId = $this->Protocolo->find('first',array('order' => array('Protocolo.id desc'))))
			$protocolo.= str_pad($ultimoId['Protocolo']['id'],5,'0',STR_PAD_LEFT);
		else
			$protocolo.= str_pad('0',5,'0',STR_PAD_LEFT);
		$dados = array('protocolo' => $protocolo, 'tipo' => $tipo, 'usuario_criador' => $usuarioCriador, 'usuario_id' => $usuarioId, 'grupo' => $grupo);
		if($this->Protocolo->save($dados))
			$retorno = $protocolo;
		return $retorno;
	}
	
	function geraProtocoloCancelamento($dados) {
		$retorno = FALSE;
		$this->ProtocoloCancelamento = ClassRegistry::init('ProtocoloCancelamento');
		$this->Protocolo = ClassRegistry::init('Protocolo');
		$protocoloExistente = $this->Protocolo->find('first',array('conditions' => array("Protocolo.tipo = 'cancelamento'",'Protocolo.usuario_id' => $dados['usuario_id'],"status <> 'finalizado'")));
		if(!empty($protocoloExistente))
			$protocolo = $protocoloExistente['Protocolo']['protocolo'];
		else
			$protocolo = $this->geraProtocolo('cancelamento',$dados['usuario_protocolo'],$dados['usuario_id'],$dados['grupo']);
		if($protocolo !== FALSE) {
			$protocoloCancelamento = $this->ProtocoloCancelamento->find('first', array('conditions' => array('Protocolo.protocolo' => $protocolo)));
			if(empty($protocoloCancelamento)) {
				$protocolo = $this->Protocolo->find('first', array('conditions' => array(
						'Protocolo.protocolo' => $protocolo)));
				if($this->ProtocoloCancelamento->save(array('protocolo_id' => $protocolo['Protocolo']['id'])))
					$retorno = $protocolo['Protocolo']['protocolo'];
			} else {
				$retorno = $protocolo;
			}
		}
		return $retorno;
	}
	
	function geraProtocoloCheckout($dados) {
		$return = false;
		$this->ProtocoloCheckoutFormando = ClassRegistry::init('ProtocoloCheckoutFormando');
		$this->Protocolo = ClassRegistry::init('Protocolo');
		$protocoloExistente = $this->Protocolo->find('first',array('conditions' => array("Protocolo.tipo" => 'checkout','Protocolo.usuario_id' => $dados['usuario_id'],"status <> 'finalizado'")));
		if(!empty($protocoloExistente))
			$protocolo = $protocoloExistente['Protocolo']['protocolo'];
		else
			$protocolo = $this->geraProtocolo('checkout',$dados['usuario_protocolo'],$dados['usuario_id'],$dados['grupo']);
		if($protocolo !== FALSE) {
			$protocoloCheckoutFormando = $this->ProtocoloCheckoutFormando->find('first', array('conditions' => array('Protocolo.protocolo' => $protocolo)));
			if(empty($protocoloCheckoutFormando)) {
				$protocolo = $this->Protocolo->find('first', array('conditions' => array(
						'Protocolo.protocolo' => $protocolo)));
				if($this->ProtocoloCheckoutFormando->save(array('protocolo_id' => $protocolo['Protocolo']['id'])))
					$retorno = $protocolo['Protocolo']['protocolo'];
			} else {
				$retorno = $protocolo;
			}
		}
		return $retorno;
	}
}