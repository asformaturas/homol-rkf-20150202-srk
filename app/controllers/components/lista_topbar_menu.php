<?php

/*
  @author Vinícius Alves Oyama
  Component estático que retorna o menu do topbar
 */

class ListaTopbarMenuComponent extends Object {

    //atributos correspondentes a sidebar
    static public $configuracaoTopbarMenu = array(
        "titulo" => "Menu",
        "links" => array());

    static function obterTopbar($grupo,$turmaLogada = false) {
        switch (strtolower($grupo)) {
            case 'formando':
                ListaTopbarMenuComponent::adicionarLinkTopbar('Home', 'principal', 'index', $grupo,true);
                ListaTopbarMenuComponent::adicionarLinkTopbar('Meus Dados', 'usuarios', 'editar_dados', $grupo,true);
                ListaTopbarMenuComponent::adicionarLinkTopbar('Área Financeira', 'area_financeira', 'dados', $grupo,true);
                ListaTopbarMenuComponent::adicionarLinkTopbar('Eventos', 'eventos', 'listar', $grupo,true);
                ListaTopbarMenuComponent::adicionarLinkTopbar('Loja', 'campanhas', 'listar', $grupo,true);
            break;
            case 'comercial':
                ListaTopbarMenuComponent::adicionarLinkTopbar('Home', 'principal', 'index', $grupo,true);
                
                if($turmaLogada) {
                    ListaTopbarMenuComponent::adicionarLinkTopbar('Membros da Comissão', 'comissoes', 'listar', $grupo,true);
                    ListaTopbarMenuComponent::adicionarLinkTopbar('Relatório Financeiro',
                            'relatorio_financeiro', 'exibir', $grupo,true);
                    ListaTopbarMenuComponent::adicionarLinkTopbar('Cronograma', 'mensagens', 'cronograma', $grupo,true);
                } else {
                    ListaTopbarMenuComponent::adicionarLinkTopbar('Turmas', 'turmas', 'listar', false ,true);
                    ListaTopbarMenuComponent::adicionarLinkTopbar('Colégios', 'faculdades', 'relatorio', $grupo,true);
                }
            break;
            /* case 'planejamento':
                ListaTopbarMenuComponent::adicionarLinkTopbar('Home', 'principal', 'index', $grupo);
                ListaTopbarMenuComponent::adicionarLinkTopbar('Turmas', 'turmas', 'index', $grupo);
                ListaTopbarMenuComponent::adicionarLinkTopbar('Lembretes', 'lembretes', 'index', $grupo);
                ListaTopbarMenuComponent::adicionarLinkTopbar('Mensagens', 'itens', 'index', $grupo);
                break;
            */
            case 'planejamento':
                ListaTopbarMenuComponent::adicionarLinkTopbar('Home', 'principal', 'index', $grupo,true);
                if($turmaLogada){
                    ListaTopbarMenuComponent::adicionarLinkTopbar('Cronograma', 'mensagens', 'cronograma', $grupo,true);
                    ListaTopbarMenuComponent::adicionarLinkTopbar('Arquivos', 'arquivos', 'listar', $grupo,true);
                    ListaTopbarMenuComponent::adicionarLinkTopbar('Membros da Comissão', 'comissoes', 'listar', $grupo,true);
                    ListaTopbarMenuComponent::adicionarLinkTopbar('Relatório Financeiro',
                            'relatorio_financeiro', 'exibir', $grupo,true);
                }else{
                    ListaTopbarMenuComponent::adicionarLinkTopbar('Turmas', 'turmas', 'listar', false,true);
                }
            break;
            case 'video':
                ListaTopbarMenuComponent::adicionarLinkTopbar('Home', 'principal', 'index', $grupo,true);
                if($turmaLogada){
                    ListaTopbarMenuComponent::adicionarLinkTopbar('Fotos Telão', 'formandos', 'fotos_telao_listar', $grupo,true);
                }else{
                    ListaTopbarMenuComponent::adicionarLinkTopbar('Turmas', 'turmas', 'listar', $grupo,true);
                }
            break;
            case 'atendimento':
                if($turmaLogada){
                    ListaTopbarMenuComponent::adicionarLinkTopbar('Relatório Financeiro',
                            'relatorio_financeiro', 'exibir', $grupo,true);
                }else{
                    ListaTopbarMenuComponent::adicionarLinkTopbar('Home', 'principal', 'index', $grupo,true);
                    ListaTopbarMenuComponent::adicionarLinkTopbar('Formandos', 'formandos', 'listar', $grupo,true);
                    ListaTopbarMenuComponent::adicionarLinkTopbar('Turmas', 'turmas', 'listar', false,true);
                    ListaTopbarMenuComponent::adicionarLinkTopbar('Todos Formandos', 'usuarios', 'listar', $grupo,true);
                }
                ListaTopbarMenuComponent::adicionarLinkTopbar('Mensagens', 'mensagens', 'email', false,true);
            break;
            case 'super':
                ListaTopbarMenuComponent::adicionarLinkTopbar('Home', 'principal', 'index', $grupo,true);
                if($turmaLogada){
                    ListaTopbarMenuComponent::adicionarLinkTopbar('Relatório Financeiro',
                            'relatorio_financeiro', 'exibir', $grupo,true);
                }else{
                    ListaTopbarMenuComponent::adicionarLinkTopbar('Turmas', 'turmas', 'listar', $grupo,true);
                    ListaTopbarMenuComponent::adicionarLinkTopbar('Usuários', 'usuarios', 'listar', $grupo,true);
                    ListaTopbarMenuComponent::adicionarLinkTopbar('IGPM', 'igpm', 'listar', $grupo,true);
                    ListaTopbarMenuComponent::adicionarLinkTopbar('Relatório Fechamento',
                            'turmas', 'relatorio_fechamento_novo', $grupo,true);
                }
            break;
            case 'financeiro':
                ListaTopbarMenuComponent::adicionarLinkTopbar('Importar Pagamentos', 'pagamentos', 'upload_pagamentos', $grupo, true);
                ListaTopbarMenuComponent::adicionarLinkTopbar('Gerar Boletos', 'formandos', 'listar', $grupo, true);
                ListaTopbarMenuComponent::adicionarLinkTopbar('Gerar Boletos', 'checkout', 'recibos', $grupo, true);
                ListaTopbarMenuComponent::adicionarLinkTopbar('Listar Pagamentos', 'checkout', 'lista_pagamentos', $grupo, true);
            break;
            case 'comissao':
                ListaTopbarMenuComponent::adicionarLinkTopbar('Home', 'principal', 'index', $grupo,true);
                ListaTopbarMenuComponent::adicionarLinkTopbar('Cronograma', 'mensagens', 'cronograma', $grupo,true);
                ListaTopbarMenuComponent::adicionarLinkTopbar('Membros da Comissão', 'comissoes', 'listar', $grupo,true);
                ListaTopbarMenuComponent::adicionarLinkTopbar('Formandos', 'formandos', 'listar', $grupo,true);
                ListaTopbarMenuComponent::adicionarLinkTopbar('Meus Dados', 'usuarios', 'editar_dados', $grupo,true);
                ListaTopbarMenuComponent::adicionarLinkTopbar('Relatório Financeiro',
                            'relatorio_financeiro', 'exibir', $grupo,true);
            break;
            case 'producao':
                ListaTopbarMenuComponent::adicionarLinkTopbar('Home', 'principal', 'index', $grupo);
                ListaTopbarMenuComponent::adicionarLinkTopbar('Meus Dados', 'usuarios', 'editar_dados', $grupo,true);
            break;
            case 'vendedor':
                ListaTopbarMenuComponent::adicionarLinkTopbar('Nova Venda', 'pagmidas', 'index', $grupo);
            break;
        }
        ListaTopbarMenuComponent::adicionarLinkTopbar('Mudar senha', 'usuarios', 'alterar_senha', $grupo,true);
        //ListaTopbarMenuComponent::adicionarLinkTopbar('Parcerias', 'parcerias', 'index', $grupo);
        //ListaTopbarMenuComponent::adicionarLinkTopbar('Protocolos', 'solicitacoes', 'index', $grupo);
        return ListaTopbarMenuComponent::$configuracaoTopbarMenu;
    }

    static function setTituloTopbar($textoDoTitulo = 'titulo') {
        ListaTopbarMenuComponent::$configuracaoTopbarMenu['titulo'] = utf8_encode($textoDoTitulo);
    }

    static function adicionarLinkTopbar($textoDoLink = 'texto', $controllerDoLink = 'controller',
            $actionDoLink = 'action', $grupo = 'grupo',$novoLayout = false) {
        array_push(ListaTopbarMenuComponent::$configuracaoTopbarMenu['links'], array(
            'grupo' => $grupo,
            'texto' => utf8_encode($textoDoLink),
            'controller' => utf8_encode($controllerDoLink),
            'action' => utf8_encode($actionDoLink),
            'novoLayout' => $novoLayout
        ));
    }

}

?>
