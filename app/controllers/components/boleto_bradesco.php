<?php

App::import('Vendor', 'PDFMerger/PDFMerger');
// App::uses('Folder', 'Utility');

class BoletoBradescoComponent extends Component {

    var $components = [
        'Session'
    ];

    public $merchant_id = 100008006;
    public $chave_seguranca = '';
    public $media_type = 'application/json';
    public $charset = 'UTF-8';

    public $sandbox = false;
    public $registro = true;
    public $url_homologacao = 'https://homolog.meiosdepagamentobradesco.com.br/apiboleto/transacao';
    public $url_producao = 'https://meiosdepagamentobradesco.com.br/apiboleto/transacao';
    public $url_homologacao_sem_registro = 'https://homolog.meiosdepagamentobradesco.com.br/api/transacao';
    public $url_producao_sem_registro = 'https://meiosdepagamentobradesco.com.br/api/transacao';

    public $pedido_numero = 0;
    public $pedido_descricao = null;
    public $pedido_valor = 0;

    public $comprador_endereco_cep = null;
    public $comprador_endereco_logradouro = null;
    public $comprador_endereco_numero = null;
    public $comprador_endereco_complemento = null;
    public $comprador_endereco_bairro = null;
    public $comprador_endereco_cidade = null;
    public $comprador_endereco_uf = null;
    public $comprador_nome = null;
    public $comprador_documento = null;
    public $comprador_ip = null;
    public $comprador_user_agent = null;
    public $token_request_confirmacao_pagamento = null;

    public $boleto_beneficiario = null;
    public $boleto_carteira = null;
    public $boleto_nossoNumero = null;
    public $boleto_dataEmissao = null;
    public $boleto_dataVencimento = null;
    public $boleto_valorTitulo = null;
    public $boleto_urlLogotipo = null;
    public $boleto_mensagemCabecalho = null;
    public $boleto_tipoRenderizacao = null;

    public $boleto_instrucoes_instrucaoLinha1 = null;
    public $boleto_instrucoes_instrucaoLinha2 = null;
    public $boleto_instrucoes_instrucaoLinha3 = null;
    public $boleto_instrucoes_instrucaoLinha4 = null;
    public $boleto_instrucoes_instrucaoLinha5 = null;
    public $boleto_instrucoes_instrucaoLinha6 = null;
    public $boleto_instrucoes_instrucaoLinha7 = null;
    public $boleto_instrucoes_instrucaoLinha8 = null;
    public $boleto_instrucoes_instrucaoLinha9 = null;
    public $boleto_instrucoes_instrucaoLinha10 = null;
    public $boleto_instrucoes_instrucaoLinha11 = null;
    public $boleto_instrucoes_instrucaoLinha12 = null;
    public $valor_percentual_multa = null;
    public $valor_juros_mora = null;
    public $valor_iof = null;

    private $data_service_pedido = array();
    private $data_service_comprador_endereco = array();
    private $data_service_comprador = array();
    private $data_service_boleto_instrucoes = array();
    private $data_service_boleto = array();

    public $data_service_boleto_registro = null;

    public $jsonSubmited;
    public $jsonReceived;


    public function initialize( Controller $controller )
    {

        $this->Controller = $controller;
        if( Configure::read('debug') == 0 ){
            $this->sandbox = false;
            $this->token_request_confirmacao_pagamento = 'A8Qi8NeCvAQx8kS1V4mIN1QaOKplsN-0-oZ050JBXCk';
            $this->chave_seguranca = 'A8Qi8NeCvAQx8kS1V4mIN1QaOKplsN-0-oZ050JBXCk';
        }else{
            $this->sandbox = true;
            $this->token_request_confirmacao_pagamento = 'nEdNv2_Mb-8NhH_jRTN0ox806FtzBq88d8C4hMjN7Is';
            $this->chave_seguranca = 'nEdNv2_Mb-8NhH_jRTN0ox806FtzBq88d8C4hMjN7Is';
        }        

        // $this->PDFMerger = new PDFMerger();
    }

    /**
     * Bradesco Shopfacil.
     * @param string $code
     * @param string $key
     */
    public function setKeyShopFacil($merchantId, $chaveSeguranca)
    {
        $this->merchant_id = trim($merchantId);
        $this->chave_seguranca = trim($chaveSeguranca);
    }


    /**
     * @return array
     */
    public function getDataServicePedido()
    {
        return $this->data_service_pedido;
    }   

    /**
     * @param array $data_service_pedido
     * @return Shopfacil
     */
    public function setDataServicePedido($data_service_pedido)
    {
        $this->data_service_pedido = $data_service_pedido;
        return $this;
    }

    /**
     * @return array
     */
    public function getDataServiceCompradorEndereco()
    {
        return $this->data_service_comprador_endereco;
    }

    /**
     * @param array $data_service_comprador_endereco
     * @return Shopfacil
     */
    public function setDataServiceCompradorEndereco($data_service_comprador_endereco)
    {
        $this->data_service_comprador_endereco = $data_service_comprador_endereco;
        return $this;
    }

    /**
     * @return array
     */
    public function getDataServiceComprador()
    {
        return $this->data_service_comprador;
    }

    /**
     * @param array $data_service_comprador
     * @return Shopfacil
     */
    public function setDataServiceComprador($data_service_comprador)
    {
        $this->data_service_comprador = $data_service_comprador;
        return $this;
    }

    /**
     * @return array
     */
    public function getDataServiceBoletoInstrucoes()
    {
        return $this->data_service_boleto_instrucoes;
    }

    /**
     * @param array $data_service_boleto_instrucoes
     * @return Shopfacil
     */
    public function setDataServiceBoletoInstrucoes($data_service_boleto_instrucoes)
    {
        $this->data_service_boleto_instrucoes = $data_service_boleto_instrucoes;
        return $this;
    }

    /**
     * @return array
     */
    public function getDataServiceBoleto()
    {
        return $this->data_service_boleto;
    }

    /**
     * @param array $data_service_boleto
     * @return Shopfacil
     */
    public function setDataServiceBoleto($data_service_boleto)
    {
        $this->data_service_boleto = $data_service_boleto;
        return $this;
    }

    public function dataServicePedido()
    {
        $this->data_service_pedido = array(
            "numero" => $this->pedido_numero,
            "valor" => (double)$this->pedido_valor,
            //"valor" => (int) $this->boleto_valorTitulo,
            "descricao" => $this->pedido_descricao
        );

        return $this->data_service_pedido;
    }

    public function dataServiceCompradorEndereco()
    {
        $this->data_service_comprador_endereco = array(
            "cep" => $this->comprador_endereco_cep,
            "logradouro" => $this->comprador_endereco_logradouro,
            "numero" => $this->comprador_endereco_numero,
            "complemento" => $this->comprador_endereco_complemento,
            "bairro" => $this->comprador_endereco_bairro,
            "cidade" => $this->comprador_endereco_cidade,
            "uf" => $this->comprador_endereco_uf
        );

        return $this->data_service_comprador_endereco;

    }

    public function dataServiceComprador()
    {
        $this->data_service_comprador = array(
            "nome" => $this->comprador_nome,
            "documento" => $this->comprador_documento,
            "endereco" => $this->dataServiceCompradorEndereco(),
            "ip" => $_SERVER["REMOTE_ADDR"],
            "user_agent" => $_SERVER["HTTP_USER_AGENT"]
        );

        return $this->data_service_comprador;
    }

    public function dataServiceBoleto()
    {
        $this->data_service_boleto = array(
            "beneficiario" => $this->boleto_beneficiario,
            "carteira" => $this->boleto_carteira,
            "nosso_numero" => substr((string)$this->boleto_nossoNumero,-11),
            "data_emissao" => $this->boleto_dataEmissao,
            "data_vencimento" => $this->boleto_dataVencimento,
            "valor_titulo" => (int) $this->boleto_valorTitulo,
            "url_logotipo" => $this->boleto_urlLogotipo,
            "mensagem_cabecalho" => $this->boleto_mensagemCabecalho,
            "tipo_renderizacao" => $this->boleto_tipoRenderizacao,
            "instrucoes" => $this->dataServiceBoletoInstrucoes(),
            "registro" => $this->data_service_boleto_registro,
            // "valor_iof" => $this->valor_iof
        );

        return $this->data_service_boleto;

    }

    public function dataServiceBoletoInstrucoes()
    {
        $this->data_service_boleto_instrucoes = array(
            "instrucao_linha_1" => $this->boleto_instrucoes_instrucaoLinha1,
            "instrucao_linha_2" => $this->boleto_instrucoes_instrucaoLinha2,
            "instrucao_linha_3" => $this->boleto_instrucoes_instrucaoLinha3,
            "instrucao_linha_4" => $this->boleto_instrucoes_instrucaoLinha4,
            "instrucao_linha_5" => $this->boleto_instrucoes_instrucaoLinha5,
            "instrucao_linha_6" => $this->boleto_instrucoes_instrucaoLinha6,
            "instrucao_linha_7" => $this->boleto_instrucoes_instrucaoLinha7,
            "instrucao_linha_8" => $this->boleto_instrucoes_instrucaoLinha8,
            "instrucao_linha_9" => $this->boleto_instrucoes_instrucaoLinha9,
            "instrucao_linha_10" => $this->boleto_instrucoes_instrucaoLinha10,
            "instrucao_linha_11" => $this->boleto_instrucoes_instrucaoLinha11,
            "instrucao_linha_12" => $this->boleto_instrucoes_instrucaoLinha12
        );

        return $this->data_service_boleto_instrucoes;
    }

    public function serviceRequest( $dados_boleto )
    {
        $data_service_request = array(
            "merchant_id" => $this->merchant_id,
            "meio_pagamento" => "300",
            "pedido" => $this->dataServicePedido(),
            "comprador" => $this->dataServiceComprador(),
            "boleto" => $this->dataServiceBoleto(),
            "token_request_confirmacao_pagamento" => $this->token_request_confirmacao_pagamento
        );
        
        $data_post = json_encode($data_service_request);
        
        if ($this->sandbox) {
            $url = $this->url_homologacao;
        } else {

            if( $this->registro ){
                $url = $this->url_producao;
            }else{
                $url = $this->url_producao_sem_registro;
            }
        }

        //Configuracao do cabecalho da requisicao
        $headers = array();
        $headers[] = "Accept: " . $this->media_type;
        $headers[] = "Accept-Charset: " . $this->charset;
        $headers[] = "Accept-Encoding: " . $this->media_type;
        $headers[] = "Content-Type: " . $this->media_type . ";charset=" . $this->charset;
        $AuthorizationHeader = $this->merchant_id . ":" . $this->chave_seguranca;
        $AuthorizationHeaderBase64 = base64_encode($AuthorizationHeader);
        $headers[] = "Authorization: Basic " . $AuthorizationHeaderBase64;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_post);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);

        $this->jsonSubmited = $data_post;
        $this->jsonReceived = $result;

        // Salva log do boleto
        $this->PagamentosLog = ClassRegistry::init('PagamentosLog');
        // $dataSource = $this->BoletosLog->getDataSource();
        // $dataSource->begin();
        
        $dataSave = [
            'despesa_id' => $dados_boleto['despesa_id'],
            'boleto_id' => '00',
            'enviado' => $this->jsonSubmited,
            'recebido' => $this->jsonReceived,
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s')
        ];

        if(!$this->PagamentosLog->save( $dataSave )){
            throw new Exception('Erro ao salvar o log do boleto');
        } 
        
        // $dataSource->commit();

        return $result;
    }

    public function getBoletoBradesco($dados_boleto, $despesa_id, $usuario)
    {
        $this->Usuario = ClassRegistry::init('Usuario');

        $cliente = $this->Usuario->findById($dados_boleto['usuario_id']);

        $this->pedido_valor = str_replace(',','.', $dados_boleto['valor_cobrado']);
        $this->pedido_numero =  $dados_boleto['numero_documento'];
        $this->pedido_descricao = $dados_boleto['demonstrativo1'];
        $this->comprador_nome = $dados_boleto['sacado'];
        $this->comprador_documento = $this->OnlyNumbers($cliente['ViewFormandos']['cpf']);
        $this->comprador_endereco_cep = $this->OnlyNumbers($cliente['ViewFormandos']['end_cep']) ;
        $this->comprador_endereco_logradouro = $cliente['ViewFormandos']['end_rua'];
        $this->comprador_endereco_numero = $cliente['ViewFormandos']['end_numero'];
        $this->comprador_endereco_complemento = $cliente['ViewFormandos']['end_complemento'];
        $this->comprador_endereco_bairro = "na";
        $this->comprador_endereco_cidade = $cliente['ViewFormandos']['end_cidade'];
        $this->comprador_endereco_uf = $cliente['ViewFormandos']['end_uf'];
        $this->boleto_beneficiario = $dados_boleto['cedente'];
        $this->boleto_carteira = '26';
        $this->boleto_nossoNumero = $dados_boleto['nosso_numero'];
        $this->boleto_dataEmissao = date('Y-m-d');
        $this->boleto_dataVencimento = $this->dateToDbDate($dados_boleto['data_vencimento']);
        $this->boleto_valorTitulo = str_replace(',','', $dados_boleto['valor_cobrado']);
        $this->boleto_urlLogotipo = 'http://rkformaturas2.hospedagemdesites.ws/wp-content/uploads/2016/05/rk-formaturas-1.png';
        $this->boleto_mensagemCabecalho = $dados_boleto['demonstrativo1'];
        $this->boleto_tipoRenderizacao = 2;
        $this->boleto_instrucoes_instrucaoLinha1 = $dados_boleto['demonstrativo1'];
        $this->boleto_instrucoes_instrucaoLinha2 = $dados_boleto['demonstrativo2'];
        $this->boleto_instrucoes_instrucaoLinha3 = $dados_boleto['instrucoes_1'];
        $this->boleto_instrucoes_instrucaoLinha4 = '';
        $this->boleto_instrucoes_instrucaoLinha5 = "";
        $this->boleto_instrucoes_instrucaoLinha6 = "";
        $this->boleto_instrucoes_instrucaoLinha7 = "";
        $this->boleto_instrucoes_instrucaoLinha8 = "";
        $this->boleto_instrucoes_instrucaoLinha9 = "";
        $this->boleto_instrucoes_instrucaoLinha10 = "";
        $this->boleto_instrucoes_instrucaoLinha11 = "";
        $this->boleto_instrucoes_instrucaoLinha12 = "";

        $jurosMora = ( $dados_boleto['mora'] < 1 ) ? substr($dados_boleto['mora'], 0, 4) : $dados_boleto['mora'];

        if( $this->registro ){

            $this->data_service_boleto_registro = [
                "agencia_pagador" => "00000",
                "razao_conta_pagador" => "00000",
                "conta_pagador" => "00000000",
                "controle_participante" => "00",
                "qtde_dias_multa" => 1,
                "aplicar_multa" => $dados_boleto['vencido'],
                "valor_percentual_multa" => (int) $dados_boleto['multa']*1000,
                "valor_multa" => 0,
                // "valor_desconto_bonificacao" => 0,
                "debito_automatico" => false,
                "rateio_credito" => false,
                "endereco_debito_automatico" => "00",
                "tipo_ocorrencia" => "1",
                "especie_titulo" => "99",
                "primeira_instrucao" => "00",
                "segunda_instrucao" => "00",
                "qtde_dias_juros" => 1,
                "valor_juros_mora" => str_replace('.','',$jurosMora),
                "data_limite_desconto" => null,
                "valor_desconto" => 0,
                "valor_iof" => 0,
                "valor_abatimento" => 0,
                "tipo_inscricao_pagador" => "01",
                "sequencia_registro" => "00"
            ];
        }else{
            $this->data_service_boleto_registro = [
                "aplicar_multa" => true,
                "valor_percentual_multa" => (int) $dados_boleto['multa']*1000,
                "valor_juros_mora" => str_replace('.','',$jurosMora)
            ];
        }
        // var_dump($this->data_service_boleto_registro);die;
        if($dados_boleto['vencido']){
            unset($this->data_service_boleto_registro['valor_percentual_multa']);
            unset($this->data_service_boleto_registro['qtde_dias_multa']);
            unset($this->data_service_boleto_registro['valor_multa']);
        }  


        $return_api = $this->serviceRequest( $dados_boleto );
        

        $data = json_decode( $return_api );
        
        // Se houver data de atualização, pega um novo boleto
        if( @isset($data->boleto->data_atualizacao) )
            $data->status->codigo = '-395';

        switch ( $data->status->codigo ) {
            case "-527":

                throw new Exception("O CPF do formando não foi encontrado. <br/> Para gerar o boleto é necessário atualizar o cadastro do formando.");                
                break;
            case "-526":
            case "-514":
            case "-523":
            case "-527":
            case "-524":
            case "-525":

                throw new Exception('É necessário atualizar os dados de cadastro do formando.<br /><br /> ' . $data->status->mensagem); 
                break;
            case "-518":

                throw new Exception('Chave de acesso inválida. A chave informada no header não confere com a chave do meio de pagamento cadastrada na base');
                break;
            case "9300586":

                throw new Exception("Multa/Mora zerado. Entre em contato com o responsável pela turma.");
                break;
            case "-401":
            case "-549":
            case "-393":
            case "-394":
            case "-395":
            case "-397": 
            case "9300574":// Banco: boleto já foi registrado. Faz um novo boleto 
                $novoNossoNumero = $this->getNumeroBoleto( ++$dados_boleto['nosso_numero'] );

                $dados_boleto['nosso_numero'] = $dados_boleto['numero_documento'] = $novoNossoNumero['nosso_numero'] ;
                $dados_boleto['ordem_boleto'] += $novoNossoNumero['ordem_boleto'];

                return $this->getBoletoBradesco($dados_boleto, $despesa_id, $usuario);
                break;
            case "-527":
            
                    throw new Exception("Verifique se seu CPF está correto e tente novamente, caso o error persista entre em contato conosco!");
                break;
            default:
                
                if($data->status->codigo != 0){

                    throw new Exception($data->status->codigo . " - Estamos enfrentando dificuldades com o banco Bradesco. No momento, não é possível gerar os boletos, tente novamente mais tarde.");
                }
                break;
        }           

        $return = $dados_boleto;
        $return['linha_digitavel_formatada'] = $data->boleto->linha_digitavel_formatada;
        $return['linha_digitavel'] = $data->boleto->linha_digitavel;
        $return['token'] = $data->boleto->token;
        $return['url_acesso'] = $data->boleto->url_acesso;               

        return $return;
    }

    public function gerarBoleto($despesa_id, $usuario, $nosso_numero = false){
        //Instancia os models que serao utilizados
        $Despesa = ClassRegistry::init('Despesa');
        $Pagamento = ClassRegistry::init('Pagamento');
        $DespesasPagamento = ClassRegistry::init('DespesasPagamento');

        //Localiza a despesa
        $despesa = $Despesa->getBoleto($despesa_id, $usuario);
        
        $conditions = [
            'Despesa.id' => $despesa['Despesa']['id'],
        ];

        $dados_boleto = $Despesa->getDataDespesaBoleto($conditions);
        
        if (!$dados_boleto) {
            throw new Exception("Despesa nao encontrada");
        }

        //Localiza o ultimo boleto gerado pelo formando.
        $ultimo_boleto_gerado = $Despesa->getOldBoletoByDespesaId($despesa, $dados_boleto['valor_cobrado']);
        
        if($ultimo_boleto_gerado){

            return $ultimo_boleto_gerado['Pagamento']['url'];
        }
        
        if(!$dados_boleto){

            throw new Exception("Despesa nao encontrada"); 
        }

        $nosso_numero_inexistente;
        if($nosso_numero){
            $dados_boleto['nosso_numero'] = $nosso_numero;
        }

        $nosso_numero_inexistente = $this->getNumeroBoleto($dados_boleto['nosso_numero'], 0);
        $dados_boleto['nosso_numero'] = $dados_boleto['numero_documento'] = $nosso_numero_inexistente['nosso_numero'];
        $dados_boleto['ordem_boleto'] += $nosso_numero_inexistente['ordem_boleto'];
        

        try {

            $data = $this->getBoletoBradesco($dados_boleto, $despesa['Despesa']['id'], $usuario);

            $save = [
                'usuario_id' => $data['usuario_id'],
                'banco_id' => $data['banco_id'],
                'valor_nominal' => $this->RealToDb($data['valor_cobrado']),
                'multa_contrato' => $data['multa'],
                'valor_multa' => $this->RealToDb($data['valor_multa']),
                'mora' => $data['mora'],
                'codigo' => $data['nosso_numero'],
                'nosso_numero' => $data['nosso_numero'],
                'numero_documento' => $data['numero_documento'],
                'ordem_boleto' => $data['ordem_boleto'],
                'codigo_barras' => $data['linha_digitavel_formatada'],
                'linha_digitavel' => $data['linha_digitavel'],
                'despesa_vencimento' => $despesa['Despesa']['data_vencimento'],
                'despesa_valor' => $despesa['Despesa']['valor'],
                'data_documento' => $this->dateToDbDate($data['data_documento']),
                'dt_vencimento' => $this->dateToDbDate($data['data_vencimento']),
                'dt_cadastro' => date('Y-m-d H:i:s'),
                'token' => $data['token'],
                'url' => $data['url_acesso']
            ];
            
            $Pagamento->create();
            if(!$Pagamento->save($save)){

                throw new Exception("Estamos enfrentando dificuldades com o banco Bradesco. No momento, não é possível gerar os boletos, tente novamente mais tarde.");
            }

            $despesas_pagamentos = [
                'despesa_id' => $despesa_id,
                'pagamento_id' => $Pagamento->id,
                'valor' => $this->RealToDb($data['valor_cobrado']),
                'multa' => $this->RealToDb($data['valor_multa']),
                'correcao_igpm' => 0
            ];

            $DespesasPagamento->create();
            if(!$DespesasPagamento->save($despesas_pagamentos)){
                
                throw new Exception("Estamos enfrentando dificuldades com o banco Bradesco. No momento, não é possível gerar os boletos, tente novamente mais tarde.");
            }

            return $save['url'];

        } catch (Exception $e) {

            throw new Exception($e->getMessage());
        }
    }

    private function getNumeroBoleto( $numero, $contador = 1 ){

        $Despesa = ClassRegistry::init('Despesa');

        if( Configure::read('debug') ){

            if( substr($numero, 0, 1) != "9"){
                $numero = '9'.$numero;
            }
        }

        $Pagamento = ClassRegistry::init('Pagamento');
        $numero = str_pad($numero, 11, 0, STR_PAD_LEFT);

        $numeroExist = $Despesa->numeroBoletoExists( $numero );

        while($numeroExist)
        {
            $numeroExist = $Despesa->numeroBoletoExists( ++$numero );
            ++$contador;
        }

        $numero = str_pad($numero, 11, 0, STR_PAD_LEFT);
        return [
                    'nosso_numero' => $numero,
                    'ordem_boleto' => $contador
                ];
    }


    // public function gerarTodosBoletosEmPDF( $despesas, $usuario )
    // {
    //     $usuario_id =  $despesas[0]['Usuario'];
    //     $destination = Configure::read('FileDirectoryParcialPathBoletosFormandos').$despesas[0]['Usuario']['id'];
    //     $i = 0;

    //     $this->PDFMerger->addPDF(Configure::read('FileDirectoryParcialPathBoletosFormandos').'informativo.pdf');
    //     foreach( $despesas as $despesa ){
    //         try {
    //             $url = $this->gerarBoleto($despesa['Despesa']['id'], $usuario);

    //             // Faz o download dos boletos
    //             $ch = curl_init();
    //             curl_setopt($ch, CURLOPT_URL, $url);
    //             curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //             curl_setopt($ch, CURLOPT_SSLVERSION,1);
    //             $data = curl_exec ($ch);
    //             $error = curl_error($ch); 
    //             curl_close ($ch);
                
    //             $file_name = $destination.DS.$i.'.pdf';
                
    //             if (!is_dir($destination))
    //                 $dir = new Folder($destination, true, 0777);
                
    //             $file = fopen($file_name, "w+");
    //             fputs($file, $data);
    //             fclose($file);

    //             // Adiciona o PDF para ser mesclado(merge)
    //             $this->PDFMerger->addPDF( $file_name, '1' );
    //             $i++;
    //             sleep(2);
                
    //         } catch (AsException $e) {
    //             $this->Controller->Session->setFlash($e->getMessage());
    //             return $this->Controller->redirect(['controller'=>'formandos','action'=>'resumo_financeiro', $despesas[0]['Usuario']['id']]);
    //         }
    //     }

    //     // Faz o merge no PDF e devolve como download
    //     $pdf_nome = $despesas[0]['TurmasUsuario']['turma_id'].$despesas[0]['Formando']['codigo'].'-todos-boletos.pdf';
    //     return $this->PDFMerger->merge('download', $pdf_nome );

    // }

    public function dateToDbDate($data) {

        return preg_replace('#(\d{2})/(\d{2})/(\d{4})#', '$3-$2-$1', $data);
    }

    public function OnlyNumbers($field = null) {

        return preg_replace("/[^0-9]/", '', $field);
    }

    public function RealToDb($real = null) {

        $number = str_replace('R$','', $real);
        $number = str_replace('.','',$number);
        $number = str_replace(',','.',$number);
        $number = trim($number);

        return $number;
    }
}