<?php

class RelatorioEntregaComponent extends Object {

    var $uses = array('CampanhasUsuario', 'CampanhasExtra', 'CampanhasUsuarioCampanhasExtra', 'CheckoutUsuarioItem', 
        'ViewFormandos', 'Protocolo', 'CheckoutItem', 'Turma', 'Extra', 'Campanha', 'Parcelamento');

    var $components = array('Financeiro');


    function __construct() {
        if ($this->uses !== false)
            foreach ($this->uses as $modelClass)
                $this->$modelClass = ClassRegistry::init($modelClass);
    }
    
    function obterRelatorioEntrega($turmaId = false){
        App::import('Component', 'Financeiro');
        $financeiro = new FinanceiroComponent();
        $turma = $this->Turma->read(null, $turmaId);
        $formandos = $this->ViewFormandos->find('all',array(
                'conditions' => array(
                    'turma_id' => $turma['Turma']['id'],
                    'situacao' => 'ativo'
                ),
                'joins' => array(
                    array(
                        "table" => "protocolos",
                        "type" => "left",
                        "alias" => "Protocolo",
                        "conditions" => array(
                            "ViewFormandos.id = Protocolo.usuario_id",
                            "Protocolo.tipo = 'checkout'"
                        )
                    )
                ),
                'group' => array('ViewFormandos.id'),
                'fields' => array(
                    'ViewFormandos.*',
                    'Protocolo.*'
                )
        ));
        $this->CampanhasUsuarioCampanhasExtra->unbindModel(array(
            'belongsTo' => array('CampanhasUsuario')
        ),false);
        $this->CampanhasUsuario->bindModel(array(
            'hasMany' => array(
                'CampanhasUsuarioCampanhasExtra' => array(
                    'className' => 'CampanhasUsuarioCampanhasExtra',
                    'joinTable' => 'campanhas_usuarios_campanhas_extras',
                    'foreignKey' => 'campanhas_usuario_id'
                )
            )
        ),false);
        $this->CampanhasUsuario->recursive = 3;
        $relatorio = array();
        $formandosCheckout = 0;
        foreach($formandos as $formando) {
            if(empty($formando['Protocolo']['id'])) {
                $data = null;
                $statusAdesao = $financeiro->obterStatusDespesas(array('usuario_id' => $formando['ViewFormandos']['id']));
            } else {
                $data = date('d/m/Y',strtotime($formando['Protocolo']['data_cadastro']));
                $statusAdesao = 'checkout';
            }
            $itensContrato = $this->Parcelamento->findById($formando['ViewFormandos']['parcelamento_id']);
            if(!empty($itensContrato)){
                if(!empty($itensContrato['Parcelamento']['mesas_contrato'] && $itensContrato['Parcelamento']['convites_contrato'])){
                    $mesasContrato = $itensContrato['Parcelamento']['mesas_contrato'];
                    $convitesContrato = $itensContrato['Parcelamento']['convites_contrato'];
                    $nomeCampanha = $itensContrato['Parcelamento']['titulo'];
                }else{
                    $mesasContrato = 0;
                    $convitesContrato = 0;
                    $nomeCampanha = 'Contrato';
                }
            }else{
                $nomeCampanha = 'Não informado';
                $mesasContrato = 0;
                $convitesContrato = 0;
            }
            $relatorio[] = array(
                'nome' => $formando['ViewFormandos']['nome'],
                'codigo' => $formando['ViewFormandos']['codigo_formando'],
                'nomeCampanha' => $nomeCampanha,
                'tipo' => 'Contrato',
                'data' => $data,
                'status' => $statusAdesao,
                'mesas' => $mesasContrato,
                'convites' => $convitesContrato
            );
            $campanhas = $this->CampanhasUsuario->find('all',array(
                'conditions' => array(
                    'usuario_id' => $formando['ViewFormandos']['id'],
                    'cancelada' => 0
                ),
                'recursive' => 3
            ));

            foreach($campanhas as $campanha) {
                if(empty($formando['Protocolo']['id']))
                    $statusCampanha = $financeiro->obterStatusDespesas(
                        array(
                            'usuario_id' => $formando['ViewFormandos']['id'],
                            'campanhas_usuario_id' => $campanha['CampanhasUsuario']['id'],
                            'tipo' => 'extra'
                        ));
                else
                    $statusCampanha = 'checkout';
                foreach($campanha['CampanhasUsuarioCampanhasExtra'] as $extra) {
                    $this->Extra->unbindModelAll();
                    $this->Campanha->unbindModelAll();
                    $nomeExtra = $this->Extra->findById($extra['CampanhasExtra']['extra_id']);
                    $nomeCampanha = $this->Campanha->findById($extra['CampanhasExtra']['campanha_id']);                    
                    $item = array(
                        'nome' => $formando['ViewFormandos']['nome'],
                        'codigo' => $formando['ViewFormandos']['codigo_formando'],
                        'nomeCampanha' => $nomeCampanha['Campanha']['nome'],
                        'tipo' => !empty($nomeExtra['Extra']['nome']) ? "{$nomeExtra['Extra']['nome']}" : "",
                        'status' => $statusCampanha,
                        'data' => $data,
                        'mesas' => $extra['CampanhasExtra']['quantidade_mesas']*$extra['quantidade'],
                        'convites' => ($extra['CampanhasExtra']['quantidade_convites'] > 0 || $extra['CampanhasExtra']['quantidade_mesas'] > 0) ? $extra['CampanhasExtra']['quantidade_convites']*$extra['quantidade'] : $extra['quantidade'],
                    );
                    if(!empty($campanha['CampanhasUsuario']['atendente_id'])) {
                        $item['data'] = date('d/m/Y',strtotime($campanha['CampanhasUsuario']['data']));
                        $item['tipo'] = "{$nomeExtra['Extra']['nome']}";
                    }
                    $relatorio[] = $item;
                }
            }
            if(!empty($formando['Protocolo']['id'])) {
                $checkout = $this->CheckoutUsuarioItem->find('all',array(
                    'conditions' => array(
                        'usuario_id' => $formando['ViewFormandos']['id'],
                        'protocolo_id' => $formando['Protocolo']['id']
                    )
                ));
                $formandosCheckout++;
                foreach($checkout as $item) {
                    $relatorio[] = array(
                        'nome' => $formando['ViewFormandos']['nome'],
                        'codigo' => $formando['ViewFormandos']['codigo_formando'],
                        'nomeCampanha' => 'Checkout',
                        'tipo' => "{$item['CheckoutItem']['titulo']}",
                        'data' => $data,
                        'status' => 'checkout',
                        'mesas' => $item['CheckoutUsuarioItem']['quantidade']*$item['CheckoutItem']['quantidade_mesas'],
                        'convites' => ($item['CheckoutItem']['quantidade_convites'] > 0) ? $item['CheckoutItem']['quantidade_convites']*$item['CheckoutUsuarioItem']['quantidade'] : $item['CheckoutUsuarioItem']['quantidade'],
                    );
                }
            }
        }
        $campanhas = array();
        $campanhas[] = array(
            'campanha' => "Contrato",
            'item' => "Contrato",
            'convites' => $turma['Turma']['convites_contrato'],
            'mesas' => $turma['Turma']['mesas_contrato'],
            'total_mesas' => 0,
            'total_convites' => 0
        );
        $this->CampanhasExtra->bindModel(array(
            'belongsTo' => array('Campanha')
        ),false);
        $extras = $this->CampanhasExtra->find('all',array(
            'conditions' => array(
                'Campanha.turma_id' => $turma['Turma']['id']
            )
        ));
        $this->Extra->unbindModelAll();
        foreach($extras as $extra){ 
            $nomeExtra = $this->Extra->findById($extra['CampanhasExtra']['extra_id']);
            $campanhas[] = array(
                'campanha' => $extra['Campanha']['nome'],
                'item' => $nomeExtra['Extra']['nome'],
                'convites' => $extra['CampanhasExtra']['quantidade_convites'],
                'mesas' => $extra['CampanhasExtra']['quantidade_mesas'],
                'total_mesas' => 0,
                'total_convites' => 0
            );
        }
        $checkoutItens = $this->CheckoutItem->find('all',array(
            'conditions' => array(
                'turma_id' => $turma['Turma']['id']
            )
        ));
        foreach($checkoutItens as $checkoutItem)
            $campanhas[] = array(
                'campanha' => "Checkout",
                'item' => $checkoutItem['CheckoutItem']['titulo'],
                'convites' => $checkoutItem['CheckoutItem']['quantidade_convites'],
                'mesas' => $checkoutItem['CheckoutItem']['quantidade_mesas'],
                'total_mesas' => 0,
                'total_convites' => 0
            );
        foreach($campanhas as $c => $campanha){
            foreach($relatorio as $r){
                if($campanha['campanha'] == $r['nomeCampanha'] &&
                   $campanha['item'] == $r['tipo']){
                    $campanhas[$c]['total_mesas'] += $r['mesas'];
                    $campanhas[$c]['total_convites'] += $r['convites'];
                }
            }
        }
        return array('relatorio' => $relatorio, 'campanhas' => $campanhas, 'checkout' => $formandosCheckout);
    }
}
