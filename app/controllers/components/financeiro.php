<?php

class FinanceiroComponent extends Object {

    var $uses = array('Despesa', 'Pagamento', 'DespesaPagamento', 'Usuario', 'ViewFormandos',"FormandoProfile");

    function __construct() {
        if ($this->uses !== false)
            foreach ($this->uses as $modelClass)
                $this->$modelClass = ClassRegistry::init($modelClass);
    }
    
    /*
     * Identifica o status do formando em relacao a adesao ou campanha
     * valido - nenhuma despesa atrasada
     * ativo - ja pagou ao menos uma parcela
     * inadimplente - nao pagou nenhuma parcela
     */
    
    function obterStatusDespesas($conditions) {
        $conditionsDefault = array(
            'tipo' => 'adesao',
            "status <> 'cancelada'",
            'or' => array(
                'data_vencimento < now()',
                'status' => 'paga'
            )
        );
        $cond = array_merge($conditionsDefault, $conditions);
        $status = "inadimplente";
        $this->Despesa->recursive = 0;
        $despesas = $this->Despesa->find('all',array(
            'conditions' => $cond
        ));
        $dataAtual = strtotime('now');
        $qtdeDespesas = count($despesas);
        $qtdeDespesasPagas = 0;
        foreach($despesas as $despesa)
            if($despesa['Despesa']['status'] == 'paga')
                    $qtdeDespesasPagas++;
        if($qtdeDespesas == $qtdeDespesasPagas)
            $status = "valido";
        elseif($qtdeDespesasPagas > 0)
            $status = "ativo";
        return $status;
    }
    
    function obterValorAdesao($usuarioId) {
        $formando = $this->ViewFormandos->read(null,$usuarioId);
        $this->Despesa->recursive = 0;
        $valorAdesao = 0;
        $despesas = $this->Despesa->find('all',array(
            'conditions' => array(
                'Despesa.usuario_id' => $usuarioId,
                'tipo' => 'adesao',
                'status not ' => array('cancelada','renegociada')
            )
        ));
        foreach($despesas as $despesa)
            $valorAdesao+= $despesa['Despesa']['valor'];
        $valorAdesao = max(array(
            $valorAdesao,
            $formando['ViewFormandos']['valor_adesao']
        ));
        return $valorAdesao;
    }
    
    function obterValorAdesaoComIgpm($usuarioId) {
        $formando = $this->ViewFormandos->read(null,$usuarioId);
        $this->Despesa->recursive = 0;
        $valorAdesao = 0;
        $despesas = $this->Despesa->find('all',array(
            'conditions' => array(
                'Despesa.usuario_id' => $usuarioId,
                'tipo' => 'adesao',
                'status not ' => array('cancelada','renegociada')
            )
        ));
        foreach($despesas as $despesa){
            $valorAdesao+= $despesa['Despesa']['valor'];
            if($despesa['Despesa']['status_igpm'] == 'pago')
                $valorAdesao += $despesa['Despesa']['correcao_igpm'];
        }
        $valorAdesao = max(array(
            $valorAdesao,
            $formando['ViewFormandos']['valor_adesao']
        ));
        return $valorAdesao;
    }
    
    function obterValorExtras($usuarioId,$campanhaUsuarioId = false) {
        $this->Despesa->recursive = 0;
        $valorExtras = 0;
        $conditions = array(
            'Despesa.usuario_id' => $usuarioId,
            'tipo' => 'extra',
            'status not ' => array('cancelada','renegociada')
        );
        if($campanhaUsuarioId){
            $conditions['campanhas_usuario_id'] = $campanhaUsuarioId;
            $despesas = $this->Despesa->find('all',array(
                'conditions' => $conditions
            ));
        }else{
            $despesas = $this->Despesa->find('all',array(
                'conditions' => $conditions
            ));
        }
        foreach($despesas as $despesa)
            $valorExtras+= $despesa['Despesa']['valor'];
        return $valorExtras;
    }
    
    function obterValorExtrasEmAberto($usuarioId,$campanhaUsuarioId = false) {
        $this->Despesa->recursive = 0;
        $valorExtras = 0;
        $conditions = array(
            'Despesa.usuario_id' => $usuarioId,
            'tipo' => 'extra',
            'status' => array('aberta')
        );
        $despesas = $this->Despesa->find('all',array(
            'conditions' => $conditions
        ));
        foreach($despesas as $despesa)
            $valorExtras+= $despesa['Despesa']['valor'];
        return $valorExtras;
    }
    
    function obterValorIgpm($usuarioId) {
        $this->Despesa->recursive = 0;
        $valorIgpm = 0;
        $despesas = $this->Despesa->find('all',array(
            'conditions' => array(
                'Despesa.usuario_id' => $usuarioId,
                'tipo <> ' => 'extra',
                'status not ' => array('cancelada','renegociada')
            )
        ));
        foreach($despesas as $despesa)
            if($despesa['Despesa']['tipo'] == 'igpm')
                $valorIgpm+= $despesa['Despesa']['valor'];
            elseif($despesa['Despesa']['status_igpm'] != 'recriado')
                $valorIgpm+= $despesa['Despesa']['correcao_igpm'];
        return $valorIgpm;
    }
    
    function obterValorTotalIgpm($usuarioId) {
        $this->Despesa->recursive = 0;
        $valorIgpm = 0;
        $despesas = $this->Despesa->find('all',array(
            'conditions' => array(
                'Despesa.usuario_id' => $usuarioId,
                'tipo' => 'adesao'
            )
        ));
        foreach($despesas as $despesa)
                $valorIgpm+= $despesa['Despesa']['correcao_igpm'];
        return $valorIgpm;
    }
    
    function obterTotalPago($usuarioId) {
        $this->Pagamento->recursive = 2;
        $pagamentos = $this->Pagamento->find('all',array(
            'conditions' => array(
                'Pagamento.usuario_id' => $usuarioId,
                'Pagamento.status' => 'pago'
            )
        ));
        $totalPago = 0;
        foreach($pagamentos as $pagamento) {
            $valorPago = min(array(
                $pagamento['Pagamento']['valor_nominal'],
                $pagamento['Pagamento']['valor_pago']
            ));
            if(isset($pagamento['DespesaPagamento'])) {
                foreach($pagamento['DespesaPagamento'] as $despesa) {
                    $valorDespesa = $despesa['Despesa']['valor'];
                    if($despesa['Despesa']['status_igpm'] != 'recriado')
                        $valorDespesa+= $despesa['Despesa']['correcao_igpm'];
                    $valorPago = min(array(
                        $valorPago,
                        $valorDespesa
                    ));
                }
            }
            $totalPago+= $valorPago;
        }
        return $totalPago;
    }
    
    function obterValorPagoAdesao($usuarioId) {
        $despesas = $this->Despesa->find('all',array(
            'conditions' => array(
                'Despesa.usuario_id' => $usuarioId,
                'Despesa.tipo' => 'adesao',
                'Despesa.status <> ' => 'aberta'
            ),
            'joins' => array(
                array(
                    "table" => "despesas_pagamentos",
                    "type" => "inner",
                    "alias" => "DespesaPagamento",
                    "conditions" => array(
                        "DespesaPagamento.despesa_id = Despesa.id",
                    )
                ),
                array(
                    "table" => "pagamentos",
                    "type" => "left",
                    "alias" => "Pagamento",
                    "conditions" => array(
                        "Pagamento.id = DespesaPagamento.pagamento_id",
                    )
                )
            ),
            'fields' => array(
                'Despesa.*',
                'DespesaPagamento.*',
                'Pagamento.*'
            )
        ));
        $totalPago = 0;
        foreach($despesas as $despesa) {
            $valorPago = 0;
            $adesao = $despesa['Despesa']['valor'];
            if($despesa['Despesa']['status_igpm'] != 'recriado')
                $adesao+= $despesa['Despesa']['correcao_igpm'];
            if($despesa['Pagamento']['status'] == 'pago')
                $valorPago = min(array(
                    $adesao,
                    $despesa['Pagamento']['valor_nominal'],
                    $despesa['Pagamento']['valor_pago']
                ));
            $totalPago+= $valorPago;
        }
        return $totalPago;
    }
    
    function obterValorPagoCampanha($usuarioId,$campanhaUsuarioId = false) {
        $conditions = array(
            'Despesa.usuario_id' => $usuarioId,
            'tipo' => 'extra',
            'status <> ' => 'aberta'
        );
        if($campanhaUsuarioId)
            $conditions['campanhas_usuario_id'] = $campanhaUsuarioId;
        $this->Despesa->recursive = 2;
        $despesas = $this->Despesa->find('all',array(
            'conditions' => $conditions
        ));
        $totalPago = 0;
        foreach($despesas as $despesa) {
            $valorPago = 0;
            $adesao = $despesa['Despesa']['valor'];
            foreach($despesa['DespesaPagamento'] as $pagamento) {
                if($pagamento['Pagamento']['status'] == 'pago')
                    $valorPago = min(array(
                        $adesao,
                        $pagamento['Pagamento']['valor_nominal'],
                        $pagamento['Pagamento']['valor_pago']
                    ));
            }
            $totalPago+= $valorPago;
        }
        return $totalPago;
    }
    
    function obterValorPagoIgpm($usuarioId) {
        $totalPago = 0;
        $this->Despesa->unbindModelAll();
        $this->Pagamento->unbindModelAll();
        $despesas = $this->Despesa->find('all', array(
            'conditions' => array(
                'usuario_id' => $usuarioId,
                'tipo' => 'adesao',
                'status' => 'paga',
                'data_pagamento is not null',
                'correcao_igpm >' => 0,
                'status_igpm' => 'pago'
            )
        ));
        foreach($despesas as $despesa)
            $totalPago += $despesa['Despesa']['correcao_igpm'];
        $pagamentos = $this->Pagamento->find('first', array(
            'conditions' => array(
                'usuario_id' => $usuarioId,
                'tipo' => 'boleto_igpm',
                'status' => 'pago'
            ),
            'fields' => array('SUM(if(valor_pago < valor_nominal,valor_pago,valor_nominal)) as total_pago')
        ));
        if($pagamentos)
            $totalPago += $pagamentos[0]['total_pago'];
        return $totalPago;
    }
    
    function obterDespesasAdesao($usuarioId, $canceladas = false) {
        $options['conditions'] = array(
            'tipo' => 'adesao',
            'Despesa.usuario_id' => $usuarioId
        );
        if(!$canceladas)
            $options['conditions']['Despesa.status not'] = array('renegociada','cancelada');
        $this->Despesa->unbindModelAll();
        $despesas = $this->Despesa->find('all',$options);
        foreach($despesas as &$despesa) {
            $saldo = $despesa['Despesa']['valor'];
            if($despesa['Despesa']['status_igpm'] != 'recriado')
                $saldo+= $despesa['Despesa']['correcao_igpm'];
            $despesa['Pagamento'] = $this->obterPagamento($despesa['Despesa']['id']);
            $despesa['Despesa']['valor_pago'] = 0;
            if(!empty($despesa['Pagamento']))
                $despesa['Despesa']['valor_pago'] = min(array(
                    $despesa['Pagamento']['valor_nominal'],
                    $despesa['Pagamento']['valor_pago'],
                    $saldo
                ));
            $despesa['Despesa']['saldo_parcela'] = $saldo - $despesa['Despesa']['valor_pago'];
            $timeDespesa = strtotime(date('Y-m-d 23:59:59',strtotime($despesa['Despesa']['data_vencimento'])));
            if($despesa['Despesa']['status'] != 'aberta' && $despesa['Despesa']['status'] != 'renegociada' &&
                round($despesa['Despesa']['saldo_parcela']) > 0 &&
                $despesa['Despesa']['valor_pago'] > 0)
                $despesa['Despesa']['status'] = 'paga parcialmente';
            elseif($despesa['Despesa']['status'] == 'aberta' &&
                    $timeDespesa < strtotime('now'))
                $despesa['Despesa']['status'] = 'atrasada';
            $despesa = $despesa['Despesa'];
        }
        return $despesas;
    }
    
    function obterDespesaIgpm($usuarioId) {
        $this->Despesa->unbindModelAll();
        $despesa = $this->Despesa->find('first',array(
            'conditions' => array(
                'Despesa.usuario_id' => $usuarioId,
                'tipo' => 'igpm',
                'status' => array('aberta', 'paga'),
            )
        ));
        if($despesa) {
            $despesa['Despesa']['valor_pago'] = $this->obterValorPagoIgpm($usuarioId);
            $despesa['Despesa']['saldo_parcela'] = $despesa['Despesa']['valor'] - $despesa['Despesa']['valor_pago'];
            if($despesa['Despesa']['saldo_parcela'] > 0)
                $despesa['Despesa']['status'] = 'paga parcialmente';
            else
                $despesa['Despesa']['status'] = 'paga';
            $despesa = $despesa['Despesa'];
        }
        return $despesa;
    }
    
    function obterDespesaCancelamento($usuarioId) {
        $this->Despesa->unbindModelAll();
        $despesa = $this->Despesa->find('all',array(
            'conditions' => array(
                'Despesa.usuario_id' => $usuarioId,
                'tipo' => 'cancelamento'
            )
        ));
        return $despesa;
    }
    
    function obterDespesasExtras($campanhaUsuarioId, $canceladas = false) {
        $options['conditions'] = array(
            'tipo' => 'extra',
            'campanhas_usuario_id' => $campanhaUsuarioId
        );
        if(!$canceladas)
            $options['conditions']['Despesa.status not'] = array('renegociada','cancelada');
        $this->Despesa->unbindModelAll();
        $despesas = $this->Despesa->find('all',$options);
        foreach($despesas as &$despesa) {
            $saldo = $despesa['Despesa']['valor'];
            $despesa['Pagamento'] = $this->obterPagamento($despesa['Despesa']['id']);
            $despesa['Despesa']['valor_pago'] = 0;
            if(!empty($despesa['Pagamento']))
                $despesa['Despesa']['valor_pago'] = min(array(
                    $despesa['Pagamento']['valor_nominal'],
                    $despesa['Pagamento']['valor_pago'],
                    $saldo
                ));
            $despesa['Despesa']['saldo_parcela'] = $saldo - $despesa['Despesa']['valor_pago'];
            $timeDespesa = strtotime(date('Y-m-d 23:59:59',strtotime($despesa['Despesa']['data_vencimento'])));
            if($despesa['Despesa']['status'] != 'aberta' && $despesa['Despesa']['saldo_parcela'] > 0 &&
                $despesa['Despesa']['valor_pago'] > 0)
                $despesa['Despesa']['status'] = 'paga parcialmente';
            elseif($despesa['Despesa']['status'] == 'aberta' &&
                    $timeDespesa < strtotime('now'))
                $despesa['Despesa']['status'] = 'atrasada';
            $despesa = $despesa['Despesa'];
        }
        return $despesas;
    }
    
    function obterPagamentosNaoVinculados($usuarioId) {
        $pagamentos = $this->Pagamento->find('all',array(
            'conditions' => array(
                'Pagamento.status' => 'pago',
                'Pagamento.usuario_id' => $usuarioId
            )
        ));
        $pagamentos_nao_vinculados = array();
        foreach($pagamentos as $pagamento)
            if(sizeof($pagamento['DespesaPagamento']) == 0 &&
                $pagamento['Pagamento']['tipo'] != 'boleto_igpm')
                $pagamentos_nao_vinculados[] = $pagamento['Pagamento'];
        return $pagamentos_nao_vinculados;
    }
    
    function renegociarAdesao($usuarioId,$parcelamentoId,$valorTotal,$despesas) {
        $erro = false;
        foreach($despesas as $parcela => $d) {
            $despesa = (array)$d;
            $this->Despesa->create();
            if(!$this->Despesa->save(array(
                'Despesa' => array(
                    'usuario_id' => $usuarioId,
                    'valor' => $despesa['valor'],
                    'data_vencimento' => $despesa['data'],
                    'tipo' => 'adesao',
                    'status' => 'aberta',
                    'parcela' => $parcela+1,
                    'total_parcelas' => count($despesas),
                    'data_cadastro' => date('Y-m-d H:i:s')
                )
            )))
                $erro = true;
            elseif($parcela == 0)
                $primeiraDespesa = $this->Despesa->getLastInsertId();
        }
        if(!$erro) {
            $this->FormandoProfile->updateAll(
                array(
                    'FormandoProfile.parcelamento_id' => $parcelamentoId,
                    'FormandoProfile.valor_adesao' => $valorTotal,
                ),
                array(
                    'FormandoProfile.usuario_id' => $usuarioId
                )
            );
            $this->Despesa->updateAll(
                array(
                    'Despesa.status' => "'renegociada'",
                    'Despesa.status_igpm' => "'pago'",
                ),
                array(
                    'Despesa.usuario_id' => $usuarioId,
                    'Despesa.status' => array("aberta","paga"),
                    'Despesa.tipo' => array("adesao","igpm"),
                    "Despesa.id < {$primeiraDespesa}"
                )
            );
        }
        return !$erro;
    }
    
    private function obterPagamento($despesa) {
        $pagamento = array();
        $despesaPagamento = $this->DespesaPagamento->find('first',
                array('conditions' => array(
                    'DespesaPagamento.despesa_id' => $despesa,
                    'Pagamento.status' => 'pago')));
        if($despesa)
            $pagamento = $despesaPagamento['Pagamento'];
        return $pagamento;
    }
    
}