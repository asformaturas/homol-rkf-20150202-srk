<?php
class MapaDeMesasComponent extends Object {

    const MESA_TIPO_CONTRATO = 1;
    const MESA_TIPO_CAMPANHA = 2;
    const MESA_TIPO_CHECKOUT = 3;

    public function checarPermissaoParaEscolhaMesa($usuarioId, $eventoId) {

    }

    public function ajustarMapaParaEvento($turmaId, $eventoId, $mapaId) {

    }

     /**
     * Retorna numero de mesas que determinado usuário adiquiriu
     * @param  integer $usuarioId Id do usuário a ser consultado
     * @param  integer $eventoId  Id do evento
     * @param  integer $mesaTipo  Tipo de mesa que deve ser pesquisada
     * @return  array Tuplas com numero de mesas adquiridas em: 
     *                       Contrato; Campanha; Checkout;
     */
    public function retornaMesasCompradasPorUsuario($usuarioId, $eventoId, $mesaTipo = 0) {
        $retorno = array();
        $this->ViewFormandos = ClassRegistry::init('ViewFormandos');
        $this->Parcelamento = ClassRegistry::init('Parcelamento');
        if(!$mesaTipo || $mesaTipo==self::MESA_TIPO_CONTRATO){
            $this->TurmaMapa = ClassRegistry::init('TurmaMapa');
            $campos = array(
                'fields'     => array(
                    'Turma.mesas_contrato',
                    'TurmaMapa.parametros',
                ),
                'conditions' => array('TurmaMapa.evento_id' => $eventoId)
            );
            $this->TurmaMapa->bindTurma();
            $turma = $this->TurmaMapa->find('first',$campos);
            $parcelamento = $this->ViewFormandos->find('first', [
                    'fields' => '*',
                    'conditions' => [
                        'ViewFormandos.id' => $usuarioId
                    ],
                    'joins' => array(
                        array(
                            'table' => $this->Parcelamento->useTable,
                            'alias' => 'Parcelamento',
                            'type' => 'LEFT',
                            'conditions' => array('Parcelamento.id = ViewFormandos.parcelamento_id')
                        )
                    ),
                ]
            );
            $parametros = json_decode($turma['TurmaMapa']['parametros']);
            $parametros_mesas_contrato = (isset($parametros->mesas_contrato) ? $parametros->mesas_contrato : 0);
            $mesasContrato = (empty($parcelamento['Parcelamento']['mesas_contrato'])) ? $parcelamento['ViewFormandos']['mesas_contrato'] : $parcelamento['Parcelamento']['mesas_contrato'];
            $retorno['mesasContrato'] = $mesasContrato + $parametros_mesas_contrato;
        }

        if(!$mesaTipo || $mesaTipo==self::MESA_TIPO_CAMPANHA){
            $this->ViewRelatorioExtras = ClassRegistry::init('ViewRelatorioExtras');
            $this->CampanhasUsuario = ClassRegistry::init('CampanhasUsuario');
            $this->CampanhasUsuarioCampanhasExtra = ClassRegistry::init('CampanhasUsuarioCampanhasExtra');
            $this->CampanhasExtra = ClassRegistry::init('CampanhasExtra');
            $this->Extra = ClassRegistry::init('Extra');

            $campanhas = $this->CampanhasUsuario->find('all',array(
                'fields' => array(
                    'CampanhasUsuario.usuario_id',
                    'CampanhasExtra.quantidade_mesas',
                    'CampanhasUsuarioCampanhasExtra.quantidade',
                    'CampanhasUsuario.cancelada',
                ),
                'joins' => array(
                    array(
                        'table' => $this->CampanhasUsuarioCampanhasExtra->useTable,
                        'alias' => 'CampanhasUsuarioCampanhasExtra',
                        'type' => 'INNER',
                        'conditions' => array('CampanhasUsuario.id = CampanhasUsuarioCampanhasExtra.campanhas_usuario_id')
                    ),
                    array(
                        'table' => $this->CampanhasExtra->useTable,
                        'alias' => 'CampanhasExtra',
                        'type' => 'INNER',
                        'conditions' => array('CampanhasExtra.id = CampanhasUsuarioCampanhasExtra.campanhas_extra_id')
                    ),
                    array(
                        'table' => $this->Extra->useTable,
                        'alias' => 'Extra',
                        'type' => 'INNER',
                        'conditions' => array('CampanhasExtra.extra_id = Extra.id')
                    ),
                ),
                'conditions' => array(
                    'CampanhasUsuario.usuario_id' => $usuarioId,
                    'Extra.evento_id' => $eventoId,
                    'CampanhasExtra.quantidade_mesas > 0',
                    'CampanhasUsuario.cancelada' => 0,
                ),
            ));

            $mesasCampanhas = 0;
            foreach($campanhas as $campanha){
                $mesasCampanhas += $campanha['CampanhasExtra']['quantidade_mesas'] * $campanha['CampanhasUsuarioCampanhasExtra']['quantidade'];
            }
            $retorno['mesasCampanhas'] = $mesasCampanhas;
        }

        if(!$mesaTipo || $mesaTipo==self::MESA_TIPO_CHECKOUT){
            $this->CheckoutUsuarioItem = ClassRegistry::init('CheckoutUsuarioItem');

            $conditions = array(
                'CheckoutUsuarioItem.usuario_id' => $usuarioId,
                'CheckoutItem.evento_id'         => $eventoId,
                'CheckoutUsuarioItem.retirou'    => 1,
            );

            $extrasCheckout = $this->CheckoutUsuarioItem->find(
                'all', 
                array('conditions' => $conditions)
            );

            $mesasCheckout = 0;
            #TODO
            #Varificar possibilidade de usar "sum" ao invés de "all"
            #bucando traser apenas a soma das mesas
            foreach($extrasCheckout as $extraCheckout){
                $mesasCheckout += $extraCheckout['CheckoutUsuarioItem']['quantidade'] * $extraCheckout['CheckoutItem']['quantidade_mesas'];
            }
            $retorno['mesasCheckout'] = $mesasCheckout;
        }
        return $retorno ;
    }

    public function salvarLayoutMesas($turma_id, $evento_id, $mapa_id ) {

    }

    public function salvarEscolhaDeMesas($usuarioId, $turmaMapaId, $eventoId, $mesasEscolhidas) {
        $this->FormandoMapaMesa = ClassRegistry::init('FormandoMapaMesa');
        $this->TurmaMapa = ClassRegistry::init('TurmaMapa');
        $this->ViewFormandos = ClassRegistry::init('ViewFormandos');

        $usuario = $this->ViewFormandos->findById($usuarioId);
        $codigo_formando = $usuario['ViewFormandos']['codigo_formando'];
        
        $nomes = array();
        foreach($mesasEscolhidas as $mesa){
            $nomes[] = $mesa->nome;
        }
        // Se retornar mesa significa que já estão selecionadas
        $mesas = $this->FormandoMapaMesa->getMesaPorNomeTurmaMapa($nomes, $turmaMapaId, $codigo_formando);
        if(count($mesas) > 0){
            return false;
        }

        $quantidadeMesas = $this->retornaMesasCompradasPorUsuario($usuarioId, $eventoId);
        $quantidadeMesasTotal = array_sum($quantidadeMesas);

        if(count($mesasEscolhidas) > $quantidadeMesasTotal){
            return false;
        }

        // Desativa antigas escolhas de  mesas do evento específico
        $this->FormandoMapaMesa->updateAll(
            array('status' => 0),
            array(
                'codigo_formando' => $codigo_formando,
                'turma_mapa_id'   => $turmaMapaId
            )
        );

        // Persiste novas informações na base de dados
        $criacao = date('Y-m-d H:i:s');
        foreach($mesasEscolhidas as $mesa) {
            $this->FormandoMapaMesa->create();
            $this->FormandoMapaMesa->set('turma_mapa_id'  , $turmaMapaId);
            $this->FormandoMapaMesa->set('codigo_formando', $codigo_formando);
            $this->FormandoMapaMesa->set('mesa'           , $mesa->nome);
            $this->FormandoMapaMesa->set('criacao'        , $criacao);
            $this->FormandoMapaMesa->set('referencia'     , $mesa->letra.$mesa->numero);
            $mesas = $this->FormandoMapaMesa->getMesaPorNomeTurmaMapa($mesa->nome, $turmaMapaId, $codigo_formando);
            if(count($mesas) > 0){
                return false;
            }
            $this->FormandoMapaMesa->save();
        }
        return true;
    }

    public function carregarMesasSelecionadas($turma_mapa_id){
        $this->FormandoMapaMesa = ClassRegistry::init('FormandoMapaMesa');

        $this->FormandoMapaMesa->bindUsuario();
        $mesasSelecionadas = $this->FormandoMapaMesa->find('all', array(
            'conditions' => array(
                'FormandoMapaMesa.status' => 1,
                'FormandoMapaMesa.turma_mapa_id' => $turma_mapa_id
            ),
            'fields' => array(
                'FormandoMapaMesa.mesa',
                'ViewFormandos.nome',
                'FormandoMapaMesa.codigo_formando',
            ),
        ));

        $mesas = array();
        foreach($mesasSelecionadas as $mesa){
            $mesas[] = array(
                'nome' => $mesa['FormandoMapaMesa']['mesa'],
                'texto' => str_replace("'", "", $mesa['ViewFormandos']['nome']),
                'idFormando' => $mesa['FormandoMapaMesa']['codigo_formando'],
            );
        }
        $mesas = json_encode($mesas);

        return $mesas;
    }

    public function gerarAgendamentos($evento_id,$data_inicio,$data_finalizacao,
        $horas_dia) {

        // Retornar id de turma para seguintes consultas
        $this->TurmaMapa = ClassRegistry::init('TurmaMapa');
        $args = array(
                'fields'     => 'TurmaMapa.turma_id',
                'conditions' => array('TurmaMapa.evento_id' => $eventoId),
                'recursive' => -1
            );
        $turma = $this->TurmaMapa->find('first',$campos);
    }

    public function retornarIntervalos() {
        return array( 10 => '10 minutos', 20 => '20 minutos',
            30 => '30 minutos', 45 => '45 minutos', 60 => '60 minutos');
    }

    public function retornarEventosPorTurma($turmaId, $apenasNomes=true) {
        $this->FormandoMapaMesa = ClassRegistry::init('TurmaMapa');
        $this->TurmaMapa = new TurmaMapa();
        $this->TurmaMapa->bindEvento();
        $args = array(
            'fields'     => array('Evento.id,Evento.nome,Mapa.titulo'),
            'conditions' => array(
                'TurmaMapa.turma_id'=>$turmaId , 
                'TurmaMapa.status'=>1,
                'Mapa.id = TurmaMapa.mapa_id',
            )
        );
        $data = $this->TurmaMapa->find('all', $args);

        $eventos = array();
        foreach ($data as $evento){
            if($apenasNomes)
                $aux = $evento['Evento']['nome'];
            else{
                $aux = array(
                    'nome'   => $evento['Evento']['nome'],
                    'espaco' => $evento['Mapa']['titulo']
                    );
            }
            $eventos[$evento['Evento']['id']] = $aux;
        }

        return $eventos;
    }

    #TODO - Assim que possível passar isso para uma Model (Discutir a melhor forma)
    public function retornarFormandosPorTurma($turmaId, $grupo = NULL, $turmaMapaId = NULL) {
        $args = array(
                'fields' => array('FormandoProfile.usuario_id, Usuario.nome',
                   'FormandoProfile.data_adesao',
                   'FormandoProfile.realizou_checkout',
                   'Formandos.codigo_formando',
                   'FormandoMesas.id',
                   'FormandoMesas.referencia',
                   'FormandoHorario.data_liberacao',
                   'FormandoHorario.setor',
                   'Usuario.grupo'),
                'recursive'  => 1,
                'conditions' => array('FormandoProfile.data_adesao IS NOT NULL'),
                'order'      => array('FormandoProfile.data_adesao'),
                'joins'      => array(
                    array(
                        'table' => 'turmas_usuarios',
                        'alias' => 'TurmaUsuarios',
                        'type'  => 'INNER',
                        'conditions' => array(
                            'TurmaUsuarios.turma_id' => $turmaId,
                            'TurmaUsuarios.usuario_id = FormandoProfile.usuario_id'
                            )
                    ),
                    array(
                        'table' => 'vw_formandos',
                        'fields'=> array('codigo_formando'),
                        'alias' => 'Formandos',
                        'type'  => 'INNER',
                        'conditions' => array(
                            'Formandos.id = FormandoProfile.usuario_id'
                        )
                    ),
                    array(
                        'table' => 'formando_mapa_mesas',
                        'fields'=> array(
                            'codigo_formando',
                            'referencia',
                            ''),
                        'alias' => 'FormandoMesas',
                        'type'  => 'LEFT',
                        'conditions' => array(
                            'FormandoMesas.codigo_formando 
                            = Formandos.codigo_formando',
                            'FormandoMesas.status' => 1,
                            'FormandoMesas.turma_mapa_id' => $turmaMapaId
                        )
                    ),
                    array(
                        'table' => 'formando_mapa_horarios',
                        'alias' => 'FormandoHorario',
                        'type'  => 'LEFT',
                        'conditions' => array(
                            'FormandoHorario.codigo_formando 
                            = Formandos.codigo_formando',
                            'FormandoHorario.id_turma_mapa' => $turmaMapaId
                        )
                    )
                )
        );

        if(!is_null($grupo)){
            $args['conditions']['Formandos.grupo'] = $grupo;
        }
        
        $this->FormandoProfile = ClassRegistry::init('FormandoProfile');
        $formandosProfiles = $this->FormandoProfile->find('all',$args);

        $this->TurmaMapa = ClassRegistry::init('TurmaMapa');
        $evento_id = $this->TurmaMapa->find('first', array(
            'fields' => 'TurmaMapa.evento_id',
            'conditions' => array(
                'TurmaMapa.id' => $turmaMapaId
            ),
        ));

        $formandos = array();
        foreach ($formandosProfiles as $key => $formando) {
            $usuario_id = $formando['FormandoProfile']['usuario_id'];
            $formandos[$usuario_id]['Usuario'] = $formando['Usuario'];
            $formandos[$usuario_id]['Formandos'] = $formando['Formandos'];
            $formandos[$usuario_id]['FormandoProfile'] = $formando['FormandoProfile'];
            $formandos[$usuario_id]['FormandoHorario'] = $formando['FormandoHorario'];
            $status = $this->retornarStatusFormando($formando);
            $formandos[$usuario_id]['status'] = $status;

            $formandoMesas = $formando['FormandoMesas'];

            $quantidadeMesas = $this->retornaMesasCompradasPorUsuario($usuario_id, $evento_id['TurmaMapa']['evento_id']);
            $formandos[$usuario_id]['FormandoMesas']['quantidade_mesas'] = array_sum($quantidadeMesas);

            if(isset($formandos[$usuario_id]['FormandoMesas']['referencias']))
                $referencias = explode(',',$formandos[$usuario_id]['FormandoMesas']['referencias']);
            else
                $referencias = array();
            // foreach ($formandoMesas as $key => $value) {
                if($formandoMesas['referencia'])
                    $referencias[] = $formandoMesas['referencia'];
            // }
            $formandos[$usuario_id]['FormandoMesas']['referencias'] = implode(',',$referencias);
        }
        return $formandos;
    }

    public function salvarAgendamento($args) {
        $this->MapaMesasAgendamento = ClassRegistry::init(
            'MapaMesasAgendamento');

        $turmaMapaId = $args['turmaMapaId'];
        $dataInicio = $args['dataInicio'];
        $dataFinalizacao = $args['dataFinalizacao'];
        $horaInicio = $args['horaInicio'];
        $horaFinalizacao = $args['horaFinalizacao'];
        $dataAgendamento = (new DateTime())->format('Y-m-d H:i:s');

        // Cacela outros demais agendamnetos para o mapa que possa existir
        $this->MapaMesasAgendamento->cancelarAgendamento($turmaMapaId);

        // Registrar Agendamento
        $this->MapaMesasAgendamento->create();
        $this->MapaMesasAgendamento->set('turma_mapa_id', 
            $args['turmaMapaId']);
        $this->MapaMesasAgendamento->set('data_inicio', 
            $args['dataInicio']);
        $this->MapaMesasAgendamento->set('data_finalizacao', 
            $args['dataFinalizacao']);
        $this->MapaMesasAgendamento->set('hora_inicio',
            $args['horaInicio']);
        $this->MapaMesasAgendamento->set('hora_finalizacao', 
            $args['horaFinalizacao']);
        $this->MapaMesasAgendamento->set('data_agendamento', 
            $dataAgendamento);
        $this->MapaMesasAgendamento->save();
        return $this;
    }

    /**
     * Metodo responsável por registrar agendamento de dos formandos
     * @param  [type]
     * @return [type]
     */
    public function agendamentoFormandos($args) {
        extract($args);

        $turma = array();
        $dataComissao = clone $data1;
        $dataComissao = $dataComissao->sub(new DateInterval('P1D'));

        reset($formandos);
        for($dataHorario = clone $data1; $dataHorario <= $data2;) {
            for($i=1; $i <= $formandosPorIntervalo; $i++){
                list($key, $value) = each($formandos);
                if(is_null($key))
                    break;

                $dataAdesao = $formandos[$key]['FormandoProfile']['data_adesao'];
                $formandos[$key]['data_agendada'] = $dataHorario->format('d/m/Y H:i');
                
                if($formandos[$key]['Usuario']['grupo'] == 'comissao'){
                    $dataLiberacao = $dataComissao;
                }
                else{
                    $dataLiberacao = $dataHorario;
                }

                $strDataLiberacao = $dataLiberacao->format('Y-m-d H:i:00');
                $formandos[$key]['data_agendada'] = $dataLiberacao->format('d/m/Y H:i');
                $codigoFormando = $formandos[$key]['Formandos']['codigo_formando'];
                
                $this->salvarAgendamentoFormando($turmaMapaId,
                    $codigoFormando, $strDataLiberacao);

                $formandos[$key]['data_adesao'] = 
                    (new DateTime($dataAdesao))->format('d/m/Y');
                $formandos[$key]['status'] = 
                    $this->retornarStatusFormando($formandos[$key]);

                if($formandos[$key]['Usuario']['grupo'] == 'comissao')
                    $turma['comissao'][] = $formandos[$key];
                else
                    $turma['formandos'][] = $formandos[$key];
            }
            $dataHorario->add(new DateInterval('PT'.$intervaloEscolhas.'M'));

            if($dataHorario->format('H:i') > $horaFinalizacao->format('H:i')){
                $dataHorario->modify('+1 day');                                                                                                                                         
                $dataHorario->setTime($horaInicio->format('H'),$horaInicio->format('i'));
            }
        }

        return $turma;
    }

    private function retornarStatusFormando($formando) {
        if(!$formando['FormandoProfile']['realizou_checkout'])
            return 'Checkout Pendente';

        if(!empty($formando['FormandoMesas']['referencia']))
            return 'Mesa(s) Escolhidas';

        return 'Agendado';
    }

    /**
     * Persiste informações de agendamento do Formando na base
     * @param  [type] $turmaMapaId    [description]
     * @param  [type] $codigoFormando [description]
     * @param  [type] $dataLiberacao  [description]
     * @return [type]                 [description]
     */
    private function salvarAgendamentoFormando($turmaMapaId,
        $codigoFormando, $dataLiberacao) {
        $this->FormandoMapaHorario = ClassRegistry::init('FormandoMapaHorario');
        $this->FormandoMapaHorario->create();
        $this->FormandoMapaHorario->set('id_turma_mapa',$turmaMapaId);
        $this->FormandoMapaHorario->set('codigo_formando',$codigoFormando);
        $this->FormandoMapaHorario->set('data_liberacao',$dataLiberacao);
        $this->FormandoMapaHorario->save();

        return $this;
    }

    public function retornarFormandosAgendados($turmaMapaId) {
        $this->FormandoMapaHorario = ClassRegistry::init('FormandoMapaHorario');

        $args = array(
            'conditions' => array('id_turma_mapa'=>$turmaMapaId),
            'joins' => array(
                array(
                    'table' => 'vw_codigo_formandos',
                    'fields'=> array('codigo_formando'),
                    'alias' => 'Formandos',
                    'type'  => 'INNER',
                    'conditions' => array(
                        'Formandos.codigo_formando = FormandoMapaHorario.codigo_formando'
                    )
                ),
                array(
                    'table' => 'usuarios',
                    'alias' => 'Usuario',
                    'type'  => 'INNER',
                    'conditions' => array(
                        'Formandos.id = Usuario.id'
                        )
                )
            )
        );

        $formandos = $this->FormandoMapaHorario->find('all',$args);
        return $formandos;
    }

    public function retornarAgendamentosTurma($turmaId, $turmaMapaId = null, $eventoId = NULL) {
        $this->TurmaMapa = ClassRegistry::init('TurmaMapa');
        $this->MapaMesasAgendamento = ClassRegistry::init('MapaMesasAgendamento');

        $conditions = array(
            'TurmaMapa.turma_id'=>$turmaId,
            'MapaMesasAgendamento.status'=>1
        );
        if($turmaMapaId)
            $conditions['MapaMesasAgendamento.turma_mapa_id'] = $turmaMapaId;

        if($eventoId){
            $conditions['TurmaMapa.evento_id'] = $eventoId;            
        }
        $agendamentos = $this->MapaMesasAgendamento->find(
            'first',
            array(
                'conditions'=>$conditions
            )
        );

        return $agendamentos;
    }
}