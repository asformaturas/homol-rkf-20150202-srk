<?php

class GerenciadorDeDownloadComponent extends Object {
    
    var $components = array('Session');
    var $bytes = 199192;
    
    function iniciarDownload($arquivo,$nome,$mime) {
        $data = base64_encode(file_get_contents($arquivo));
        if($data) {
            $sessionId = "Arquivos.".Security::hash(microtime(true), 'sha1', true);
            $session = array(
                'mime' => $mime,
                'file' => $arquivo,
                'name' => $nome,
                'path' => $arquivo,
                'size' => strlen($data)
            );
            if($this->Session->write($sessionId,$session)) {
                return array(
                    'url' => "/arquivos/atualizar_download/$sessionId",
                    'read' => 0,
                    'size' => strlen($data)
                );
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    function atualizarDownload($sessionId,$bytes) {
        if($this->Session->check($sessionId)) {
            $session = $this->Session->read($sessionId);
            $chunk = base64_encode(file_get_contents($session['path'],true,null,$bytes,$this->bytes));
            $data = array(
                'chunk' => $chunk,
                'end' => false,
                'bytes' => $this->bytes
            );
            if(($bytes+$this->bytes) >= $session['size']) {
                $data['mime'] = $session['mime'];
                $data['name'] = $session['name'];
                $data['end'] = true;
                $this->Session->delete($sessionId);
                return $data;
            } else {
                return $data;
            }
        } else {
            return false;
        }
    }
    
}