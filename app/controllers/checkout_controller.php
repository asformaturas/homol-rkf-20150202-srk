<?php

class CheckoutController extends AppController {

    var $name = 'Checkout';
    var $uses = array('Despesa', 'Pagamento', 'DespesaPagamento',
        'FormandoProfile', 'CampanhasUsuario', 'ViewFormandos',
        'CheckoutFormandoPagamento','CheckoutRecibo','CheckoutUsuarioItem',
        'CheckoutReciboPagamento','Protocolo');
    var $helpers = array('Excel');
    var $components = array('Financeiro');
    var $nomeDoTemplateSidebar = 'Checkout';
    
    function obterSaldoCampanhasEAdesao($uid = false){
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $campanhas = $this->Financeiro->obterValorExtrasEmAberto($uid);
        $adesao = $this->Financeiro->obterValorPagoAdesao($uid);
        return json_encode(array('saldoCampanhas' => $campanhas,
               'adesao' => $adesao));
    }
    
    function planejamento_total_checkout_excel($turmaId = false){
        ini_set("memory_limit", "2048M");
        set_time_limit(0);
        $this->layout = false;
        $this->planejamento_total_checkout($turmaId);
    }
    
    function producao_total_checkout_excel($turmaId){
        $this->planejamento_total_checkout_excel($turmaId);
    }
    
    function atendimento_total_checkout_excel($turmaId){
        $this->planejamento_total_checkout_excel($turmaId);
    }
    
    function producao_total_checkout($turmaId){
        $this->planejamento_total_checkout($turmaId);
    }
    
    function atendimento_total_checkout($turmaId){
        $this->planejamento_total_checkout($turmaId);
    }
    
    function planejamento_total_checkout($turmaId = false){
        App::import('Component', 'RelatorioCheckout');
        $relatorioCheckout = new RelatorioCheckoutComponent();
        $this->set('relatorio', $relatorioCheckout->obterRelatorioCheckout($turmaId));
        $this->set('turma', $turmaId);
    }
    
    function atendimento_checkout_data(){
        $this->planejamento_checkout_data();
    }
    
    function producao_checkout_data(){
        $this->planejamento_checkout_data();
    }
    
    function planejamento_checkout_data(){
        $this->layout = 'metro/externo';
        $turma = $this->obterTurmaLogada();
        ini_set('memory_limit', '1024M');
        set_time_limit(0);
        $this->loadModel('CampanhasUsuario');
        $this->loadModel('CheckoutFormandoPagamento');
        $this->loadModel('CheckoutUsuarioItem');
        $this->loadModel('CampanhasUsuarioCampanhasExtra');
        $this->loadModel('CampanhasExtra');
        $this->loadModel('CampanhasUsuario');
        $formandos = $this->ViewFormandos->find('all',array(
                'conditions' => array(
                    'turma_id' => $turma['Turma']['id'],
                    'situacao' => 'ativo'
                ),
                'joins' => array(
                    array(
                        "table" => "protocolos",
                        "type" => "inner",
                        "alias" => "Protocolo",
                        "conditions" => array(
                            "ViewFormandos.id = Protocolo.usuario_id",
                            "Protocolo.tipo = 'checkout'"
                        )
                    )
                ),
                'group' => array('ViewFormandos.id'),
                'order' => array(
                    'Protocolo.data_cadastro' => 'asc',
                ),
                'fields' => array(
                    'ViewFormandos.*',
                    'Protocolo.*'
                )
        ));
        $this->CampanhasUsuarioCampanhasExtra->unbindModel(array(
            'belongsTo' => array('CampanhasUsuario')
        ),false);
        $this->CampanhasUsuario->bindModel(array(
            'hasMany' => array(
                'CampanhasUsuarioCampanhasExtra' => array(
                    'className' => 'CampanhasUsuarioCampanhasExtra',
                    'joinTable' => 'campanhas_usuarios_campanhas_extras',
                    'foreignKey' => 'campanhas_usuario_id'
                )
            )
        ),false);
        $this->CampanhasUsuario->recursive = 3;
        $relatorio = array();
        $totais = array();
        $totais['convites'] = 0;
        $totais['mesas'] = 0;
        $totais['meias'] = 0;
        foreach($formandos as $i => $formando){
            $protocolo = $this->Protocolo->find('first', array(
               'conditions' => array(
                   'usuario_id' => $formando['ViewFormandos']['id'],
                   'tipo' => 'checkout'
               )
            ));
            $itensCheckout = $this->CheckoutUsuarioItem->find('all', array(
                'conditions' => array(
                    'protocolo_id' => $protocolo['Protocolo']['id'],
                )
            ));
            $relatorio[$i]['codigo_formando'] = $formando['ViewFormandos']['codigo_formando'];
            $relatorio[$i]['nome'] = $formando['ViewFormandos']['nome'];
            $relatorio[$i]['total_mesas'] = $formando['ViewFormandos']['mesas_contrato'];
            $relatorio[$i]['total_convites'] = $formando['ViewFormandos']['convites_contrato'];
            $relatorio[$i]['total_meias'] = 0;
            $atendente = $this->Usuario->findById($protocolo['Protocolo']['usuario_criador']);
            $relatorio[$i]['atendente'] = ucfirst($atendente['Usuario']['nome']);
            $relatorio[$i]['data'] = date("d/m/Y  H:i:s", strtotime($protocolo['Protocolo']['data_cadastro']));
            $relatorio[$i]['filtro'] = $protocolo['Protocolo']['data_cadastro'];
            foreach ($itensCheckout as $itemCheckout) {
                $relatorio[$i]['total_convites'] += $itemCheckout['CheckoutUsuarioItem']['quantidade'] * $itemCheckout['CheckoutItem']['quantidade_convites'];
                $relatorio[$i]['total_mesas'] += $itemCheckout['CheckoutUsuarioItem']['quantidade'] * $itemCheckout['CheckoutItem']['quantidade_mesas'];
                if($itemCheckout['CheckoutItem']['quantidade_convites'] == 0 && 
                   $itemCheckout['CheckoutItem']['quantidade_mesas'] == 0){
                    if(strpos($itemCheckout['CheckoutItem']['titulo'], 'Meia') !== false ||
                       strpos($itemCheckout['CheckoutItem']['titulo'], 'MEIA') !== false ||
                       strpos($itemCheckout['CheckoutItem']['titulo'], 'Infantil') !== false ||
                       strpos($itemCheckout['CheckoutItem']['titulo'], 'INFANTIL') !== false)
                        $relatorio[$i]['total_meias'] += $itemCheckout['CheckoutUsuarioItem']['quantidade'];
                }
            }
            $campanhas = $this->CampanhasUsuario->find('all',array(
                'conditions' => array(
                    'usuario_id' => $formando['ViewFormandos']['id'],
                    'cancelada' => 0
                )
            ));
            $itensCampanhaRetirados = array();
            foreach($campanhas as $campanha) {
                foreach($campanha['CampanhasUsuarioCampanhasExtra'] as $extra) {
                    $relatorio[$i]['total_mesas'] += $extra['CampanhasExtra']['quantidade_mesas'] * $extra['quantidade'];
                    $relatorio[$i]['total_convites'] += $extra['CampanhasExtra']['quantidade_convites'] * $extra['quantidade'];
                    if($relatorio[$i]['total_mesas'] == 0 &&
                       $relatorio[$i]['total_convites'] == 0){
                        if(strpos($extra['CampanhasExtra']['Extra']['nome'], 'Meia') !== false ||
                           strpos($extra['CampanhasExtra']['Extra']['nome'], 'MEIA') !== false ||
                           strpos($extra['CampanhasExtra']['Extra']['nome'], 'Infantil') !== false ||
                           strpos($extra['CampanhasExtra']['Extra']['nome'], 'INFANTIL') !== false)
                            $relatorio[$i]['total_meias'] += $extra['quantidade'];
                    }
                }
            }
        }
        foreach($relatorio as $r){
            $totais['convites'] += $r['total_convites'];
            $totais['mesas'] += $r['total_mesas'];
            $totais['meias'] += $r['total_meias'];
        }
        foreach ($relatorio as $r) {
            $data = date("d-m-Y" , strtotime($r['filtro']));
            if (isset($result[$data])) {
               $result[$data][] = $r;
            } else {
               $result[$data] = array($r);
            }
        }
        $this->set('turma', $turma);
        $this->set('totais', $totais);
        $this->set('relatorio', $result);
    }
    
    function financeiro_lista_pagamentos() {
        $this->layout = false;
        $options = array(
            'limit' => 50,
            'joins' => array(
                array(
                    'type' => 'INNER',
                    'alias' => 'Atendente',
                    'table' => 'usuarios',
                    'conditions' => array(
                        'CheckoutFormandoPagamento.usuario_id = ' .
                        'Atendente.id'
                    )
                ),
                array(
                    'type' => 'LEFT',
                    'alias' => 'CheckoutReciboPagamento',
                    'table' => 'checkout_recibo_pagamentos',
                    'conditions' => array(
                        'CheckoutReciboPagamento.checkout_pagamento_id = ' .
                        'CheckoutFormandoPagamento.id'
                    )
                ),
                array(
                    'type' => 'LEFT',
                    'alias' => 'CheckoutRecibo',
                    'table' => 'checkout_recibos',
                    'conditions' => array(
                        'CheckoutRecibo.id = ' .
                        'CheckoutReciboPagamento.checkout_recibo_id'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'alias' => 'Protocolo',
                    'table' => 'protocolos',
                    'conditions' => array(
                        'CheckoutFormandoPagamento.protocolo_id = ' .
                        'Protocolo.id'
                    )
                ),
                array(
                    'type' => 'INNER',
                    'alias' => 'Formando',
                    'table' => 'vw_formandos',
                    'conditions' => array(
                        'Protocolo.usuario_id = ' .
                        'Formando.id'
                    )
                )
            ),
            'order' => array(
                'CheckoutFormandoPagamento.id' => 'desc'
            ),
            'fields' => array(
                'CheckoutFormandoPagamento.*',
                'CheckoutReciboPagamento.*',
                'CheckoutRecibo.*',
                'Protocolo.*',
                'Atendente.*',
                'Formando.*'
            )
        );
        $this->CheckoutFormandoPagamento->unbindModelAll();
        $this->paginate['CheckoutFormandoPagamento'] = $options;
        $pagamentos = $this->paginate('CheckoutFormandoPagamento');
        $this->set('pagamentos',$pagamentos);
    }
    
    function financeiro_pagamentos($tipo = 'cheque') {
        $this->layout = false;
        if(empty($this->data)) {
            $atendentes = $this->Usuario->find('list',array(
                'conditions' => array(
                    'grupo' => 'atendimento',
                    'ativo' => 1
                ),
                'fields' => array('Usuario.nome'),
                'order' => array('Usuario.id' => 'asc')
            ));
            $options = array(
                'order' => array('CheckoutFormandoPagamento.data_cadastro' => 'desc'),
                'conditions' => array(
                    'CheckoutReciboPagamento.id is null'
                ),
                'limit' => 50,
                'joins' => array(
                    array(
                        'type' => 'LEFT',
                        'alias' => 'CheckoutReciboPagamento',
                        'table' => 'checkout_recibo_pagamentos',
                        'conditions' => array(
                            'CheckoutReciboPagamento.checkout_pagamento_id = ' .
                            'CheckoutFormandoPagamento.id'
                        )
                    )
                )
            );
            if($tipo == 'cheque')
                $options['conditions']['CheckoutFormandoPagamento.tipo <>'] = 'comprovante';
            else
                $options['conditions']['CheckoutFormandoPagamento.tipo'] = 'comprovante';
            $atendentes['sem_recibo'] = 'Todos Atendentes';
            $atendentes = array_reverse($atendentes,true);
            $atendente = false;
            if($this->Session->check("filtros.{$this->params['prefix']}.checkout_pagamentos")) {
                $filtro = $this->Session->read("filtros.{$this->params['prefix']}.checkout_pagamentos");
                $this->data = $filtro;
                $atendente = $filtro;
                if($filtro['Protocolo']['usuario_criador'] != "sem_recibo") {
                    $options['conditions']['CheckoutFormandoPagamento.usuario_id'] = $filtro['Protocolo']['usuario_criador'];
                }
            }
            $this->Protocolo->unbindModel(array(
                'hasMany' => array(
                    'CheckoutFormandoPagamento',
                    'CheckoutUsuarioItem'
                )
            ),false);
            $this->CheckoutFormandoPagamento->recursive = 2;
            $this->CheckoutFormandoPagamento->Usuario->unbindModelAll();
            $this->paginate['CheckoutFormandoPagamento'] = $options;
            $pagamentos = $this->paginate('CheckoutFormandoPagamento');
            $this->set('atendentes',$atendentes);
            $this->set('atendente',$atendente);
            $this->set('pagamentos',$pagamentos);
        } else {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $this->Session->write("filtros.{$this->params['prefix']}.checkout_pagamentos",
                    $this->data);
            echo json_encode(array());
        }
    }
    
    function financeiro_recibos() {
        $this->layout = false;
        if(empty($this->data)) {
            $usuario = $this->Session->read('Usuario');
            $atendentes = $this->Usuario->find('list',array(
                'conditions' => array(
                    'grupo' => 'atendimento'
                ),
                'fields' => array('Usuario.nome'),
                'order' => array('Usuario.id' => 'asc')
            ));
            $options = array(
                'conditions' => array(
                    'CheckoutRecibo.financeiro_id' => $usuario['Usuario']['id']
                ),
                'limit' => 50,
                'fields' => array(
                    'CheckoutRecibo.*',
                    'Atendente.*',
                    "(select sum(valor) from checkout_recibo_pagamentos crp inner join checkout_formando_pagamentos" .
                    " cp on cp.id = crp.checkout_pagamento_id where crp.checkout_recibo_id = CheckoutRecibo.id) as valor"
                )
            );
            if($this->Session->check("filtros.{$this->params['prefix']}.checkout_recibos")) {
                $filtro = $this->Session->read("filtros.{$this->params['prefix']}.checkout_recibos");
                $this->data = $filtro;
                if(!empty($filtro['CheckoutRecibo']['atendente_id']))
                    $options['conditions']['CheckoutRecibo.atendente_id'] = $filtro['CheckoutRecibo']['atendente_id'];
            }
            $this->CheckoutRecibo->unbindModel(array(
                'hasMany' => array('CheckoutReciboPagamento')
            ),false);
            $this->CheckoutRecibo->recursive = 1;
            $this->paginate['CheckoutRecibo'] = $options;
            $recibos = $this->paginate('CheckoutRecibo');
            $this->set('atendentes',$atendentes);
            $this->set('recibos',$recibos);
        } else {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $this->Session->write("filtros.{$this->params['prefix']}.checkout_recibos",
                    $this->data);
            echo json_encode(array());
        }
    }
    
    function financeiro_gerar_recibo() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        if(isset($this->data['CheckoutRecibo'])) {
            $usuario = $this->Session->read('Usuario');
            $this->data['CheckoutRecibo']['financeiro_id'] = $usuario['Usuario']['id'];
            if(!$this->CheckoutRecibo->saveAll($this->data))
                $this->Session->setFlash('Erro gerar recibo', 'metro/flash/error');
            else {
                $mensagens = array(
                    'Recibo gerado com sucesso',
                    'Clique no link abaixo para imprimir o recibo',
                    "<br /><a href='/{$this->params['prefix']}/checkout/recibo/" .
                        $this->CheckoutRecibo->getLastInsertId() ."' " .
                        "class='fg-color-white strong' data-bind='click: loadThis'>" .
                        "Imprimir o recibo</a>"
                );
                $this->Session->setFlash($mensagens, 'metro/flash/success');
            }
        } else {
            $this->Session->setFlash('Erro ao receber os dados', 'metro/flash/error');
        }
    }
    
    function financeiro_recibo($reciboId, $layout = false) {
        $this->CheckoutRecibo->recursive = 4;
        $this->CheckoutRecibo->Atendente->unbindModelAll();
        $this->CheckoutRecibo->Financeiro->unbindModelAll();
        $this->CheckoutReciboPagamento->unbindModel(array(
            'belongsTo' => array('CheckoutRecibo')
        ),false);
        $this->Protocolo->unbindModel(array(
            'hasMany' => array('CheckoutFormandoPagamento','CheckoutUsuarioItem')
        ),false);
        $this->CheckoutReciboPagamento->unbindModel(array(
            'belongsTo' => array('CheckoutRecibo')
        ),false);
        $recibo = $this->CheckoutRecibo->read(null,$reciboId);
        $this->set('recibo',$recibo);
        if($layout == 'imprimir')
            $this->layout = "metro/impressao";
        else {
            $this->layout = false;
            if($layout == 'excel')
                $this->render('financeiro_recibo_excel');
        }
    }
    
    function planejamento_relatorio_vendas() {
        $this->layout = false;
        $turma = $this->obterTurmaLogada();
        $this->loadModel('Campanha');
        $this->loadModel('CampanhasUsuario');
        $this->loadModel('CampanhasExtra');
        $this->loadModel('CampanhasUsuarioCampanhasExtra');
        $this->CampanhasUsuarioCampanhasExtra->unbindModel(array(
            'belongsTo' => array('CampanhasUsuario')
        ),false);
        $this->CampanhasExtra->unbindModel(array(
            'belongsTo' => array('Campanha'),
            'hasAndBelongsToMany' => array('CampanhasUsuarioCampanhaExtra')
        ),false);
        $this->CampanhasExtra->bindModel(array(
            'belongsTo' => array('Extra')
        ),false);
        $this->CampanhasUsuario->bindModel(array(
            'hasMany' => array(
                'CampanhasUsuarioCampanhasExtra' => array(
                    'className' => 'CampanhasUsuarioCampanhasExtra',
                    'joinTable' => 'campanhas_usuarios_campanhas_extras',
                    'foreignKey' => 'campanhas_usuario_id'
                )
            ),
            'belongsTo' => array('Campanha')
        ),false);
        $this->Campanha->unbindModelAll();
        $this->CampanhasUsuario->recursive = 3;
        $options['joins'] = array(
            array(
                "table" => "protocolos",
                "type" => "inner",
                "alias" => "Protocolo",
                "conditions" => array(
                    "Protocolo.usuario_id = ViewFormandos.id",
                    "Protocolo.tipo = 'checkout'"
                )
            )
        );
        $options['conditions'] = array(
            'turma_id' => $turma['Turma']['id'],
            "situacao <> 'cancelado'"
        );
        $options['fields'] = array('ViewFormandos.*','Protocolo.*');
        $formandos = $this->ViewFormandos->find('all',$options);
        $lista = array();
        foreach($formandos as $formando) {
            $checkouts = $this->CheckoutUsuarioItem->find('all',array(
                'conditions' => array(
                    'protocolo_id' => $formando['Protocolo']['id']
                )
            ));
            foreach($checkouts as $checkout) {
                $lista[] = array(
                    'codigo_formando' => $formando['ViewFormandos']['codigo_formando'],
                    'nome' => $formando['ViewFormandos']['nome'],
                    'campanha' => 'Checkout',
                    'item' => $checkout['CheckoutItem']['titulo'],
                    'quantidade' => $checkout['CheckoutUsuarioItem']['quantidade'],
                    'valor' => abs($checkout['CheckoutUsuarioItem']['saldo'])
                );
            }
            $campanhas = $this->CampanhasUsuario->find('all',array(
                'conditions' => array(
                    'usuario_id' => $formando['ViewFormandos']['id'],
                    'cancelada' => 0
                )
            ));
            foreach($campanhas as $campanha) {
                foreach($campanha['CampanhasUsuarioCampanhasExtra'] as $extra) {
                    $lista[] = array(
                        'codigo_formando' => $formando['ViewFormandos']['codigo_formando'],
                        'nome' => $formando['ViewFormandos']['nome'],
                        'campanha' => $campanha['Campanha']['nome'],
                        'item' => $extra['CampanhasExtra']['Extra']['nome'],
                        'quantidade' => $extra['quantidade'],
                        'valor' => $extra['quantidade']*$extra['valor_unitario']
                    );
                }
            }
        }
        $this->set('lista',$lista);
    }
    
    function super_relatorio_por_atendente($atendenteId = false,$data = false) {
        $this->layout = false;
        if($atendenteId && $data) {
            $data = date('Y-m-d',$data);
            $this->Usuario->unbindModelAll();
            $atendente = $this->Usuario->read(null,$atendenteId);
            $this->Protocolo->recursive = 2;
            $checkouts = $this->Protocolo->find('all',array(
                'conditions' => array(
                    "date_format(data_cadastro,'%Y-%m-%d') = '$data'",
                    'usuario_criador' => $atendenteId
                )
            ));
            $dados = array(
                'atendimentos' => 0,
                'convites' => 0,
                'cheque' => 0,
                'valor_cheque' => 0,
                'dinheiro' => 0,
                'valor_dinheiro' => 0,
                'comprovante' => 0,
                'valor_comprovante' => 0,
            );
            $atendimentos = array();
            foreach($checkouts as $indice => $checkout) {
                $checkout['Formando']['convites'] = 0;
                $checkout['Formando']['tipo'] = 'Checkout';
                foreach($checkout['CheckoutUsuarioItem'] as $checkoutItem)
                    $checkout['Formando']['convites']+= $checkoutItem['quantidade']*$checkoutItem['CheckoutItem']['quantidade_convites'];
                foreach($checkout['CheckoutFormandoPagamento'] as $checkoutPagamento) {
                    if(!empty($checkoutPagamento['tipo'])) {
                        $dados[$checkoutPagamento['tipo']]++;
                        $dados["valor_{$checkoutPagamento['tipo']}"]+= $checkoutPagamento['valor'];
                    }
                }
                $dados['convites']+= $checkout['Formando']['convites'];
                $atendimentos[] = $checkout;
            }
            $this->loadModel('CampanhasUsuarioCampanhasExtra');
            $this->loadModel('CampanhasExtra');
            $this->CampanhasUsuarioCampanhasExtra->unbindModel(array(
                'belongsTo' => array('CampanhasUsuario')
            ),false);
            $this->CampanhasUsuario->bindModel(array(
                'hasMany' => array(
                    'CampanhasUsuarioCampanhasExtra' => array(
                        'className' => 'CampanhasUsuarioCampanhasExtra',
                        'joinTable' => 'campanhas_usuarios_campanhas_extras',
                        'foreignKey' => 'campanhas_usuario_id'
                    )
                ),
                'belongsTo' =>array(
                    'Atendente' => array(
                        'className' => 'Usuario',
                        'foreignKey' => 'atendente_id'
                    ),
                    'Formando' => array(
                        'className' => 'ViewFormandos',
                        'foreignKey' => 'usuario_id'
                    ),
                )
            ),false);
            $this->CampanhasUsuario->recursive = 3;
            $this->Usuario->unbindModelAll();
            $this->CampanhasUsuario->Atendente->unbindModelAll();
            $campanhas = $this->CampanhasUsuario->find('all',array(
                'conditions' => array(
                    "date_format(data,'%Y-%m-%d') = '$data'",
                    'CampanhasUsuario.atendente_id' => $atendenteId
                )
            ));
            if($campanhas) {
                foreach($campanhas as $campanha) {
                    $atendimento['Formando'] = $campanha['Formando'];
                    $atendimento['Formando']['convites'] = 0;
                    $atendimento['Formando']['tipo'] = 'Venda Antecipada';
                    $atendimento['Protocolo']['data_cadastro'] = $campanha['CampanhasUsuario']['data'];
                    foreach($campanha['CampanhasUsuarioCampanhasExtra'] as $extra)
                        $atendimento['Formando']['convites']+= $extra['quantidade']*$extra['CampanhasExtra']['quantidade_convites'];
                    $atendimentos[] = $atendimento;
                }
            }
            $dados['atendimento'] = count($atendimentos);
            $this->set('atendente',$atendente);
            $this->set('data',date('d/m/Y',strtotime($data)));
            $this->set('resumo',$dados);
            $this->set('atendimentos',$atendimentos);
            $this->render('super_checkout_atendente');
        } elseif(!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $inicio = date('Y-m-d H:i:s',$this->data['inicio']);
            $fim = date('Y-m-d H:i:s',$this->data['fim']);
            $conditions = array(
                "data_cadastro >= '$inicio'",
                "data_cadastro <= '$fim'",
                'tipo' => 'checkout'
            );
            if($this->turmaEstaLogada()) {
                $turma = $this->obterTurmaLogada();
                $conditions['Formando.turma_id'] = $turma['Turma']['id'];
            }
            $checkouts = $this->Protocolo->find('all',array(
                'conditions' => $conditions,
                'group' => array("day(data_cadastro)","usuario_criador")
            ));
            $lista = array();
            $datas = array();
            foreach($checkouts as $checkout) {
                $data = substr($checkout['Protocolo']['data_cadastro'],0,10);
                if(!isset($datas[$data]))
                    $datas[$data] = array();
                if(!in_array($checkout['Atendente']['id'],$datas[$data]))
                    $datas[$data][] = $checkout['Atendente']['id'];
                $lista[] = array(
                    'id' => $checkout['Protocolo']['id'],
                    'data' => strtotime($checkout['Protocolo']['data_cadastro']),
                    'title' => $checkout['Atendente']['nome'],
                    'usuario_id' => $checkout['Atendente']['id'],
                    'allDay' => false
                );
            }
            $this->CampanhasUsuario->unbindModelAll();
            $this->CampanhasUsuario->bindModel(array(
                'belongsTo' =>array(
                    'Atendente' => array(
                        'className' => 'Usuario',
                        'foreignKey' => 'atendente_id'
                    )
                )
            ),false);
            $campanhas = $this->CampanhasUsuario->find('all',array(
                'conditions' => array(
                    'CampanhasUsuario.atendente_id is not null',
                    "data >= '$inicio'",
                    "data <= '$fim'"
                ),
                'group' => array("day(data)","atendente_id")
            ));
            if($campanhas) {
                foreach($campanhas as $campanha) {
                    $data = substr($campanha['CampanhasUsuario']['data'],0,10);
                    if(!isset($datas[$data]))
                        $datas[$data] = array();
                    if(!in_array($campanha['CampanhasUsuario']['atendente_id'],$datas[$data]))
                        $lista[] = array(
                            'id' => $campanha['CampanhasUsuario']['id'],
                            'data' => strtotime($campanha['CampanhasUsuario']['data']),
                            'title' => $campanha['Atendente']['nome'],
                            'usuario_id' => $campanha['CampanhasUsuario']['atendente_id'],
                            'allDay' => false
                        );
                }
            }
            echo json_encode(array('lista' => $lista,'inicio' => $inicio,'fim' => $fim));
        }
    }

    function formando_index() {
        $usuario = $this->Session->read('Usuario');
        $this->ViewFormandos->id = $usuario['Usuario']['id'];
        $formando = $this->ViewFormandos->read();
        if (!empty($formando)) {
            $this->set('formando', $formando['ViewFormandos']);
            $turma = $this->Turma->find('first', array('conditions' => array('id' => $formando['ViewFormandos']['turma_id'])));
            $this->set('turma', $turma);
            $this->Despesa->recursive = 2;
            $adesoes = $this->Despesa->find('all', array('conditions' =>
                array('Despesa.usuario_id' => $formando['ViewFormandos']['id'], 'Despesa.tipo' => 'adesao', "Despesa.status != 'cancelada'")));
            $this->set('despesas_adesao', $adesoes);
            $totalDespesasAdesao = 0;
            $totalDespesasExtras = 0;
            $totalIGPM = 0;
            $saldoTotal = 0;
            $totalPago = 0;

            foreach ($adesoes as $adesao) {
                $totalDespesasAdesao += $adesao['Despesa']['valor'];
                $totalIGPM +=!empty($adesao['Despesa']['correcao_igpm']) ? $adesao['Despesa']['correcao_igpm'] : 0;
                if ($adesao['Despesa']['status'] == 'aberta') {
                    $saldoTotal-= $adesao['Despesa']['valor'] + $adesao['Despesa']['correcao_igpm'];
                } elseif (sizeof($adesao['DespesaPagamento']) != 0 && $adesao['Despesa']['status'] == 'paga') {
                    $valorPago = $adesao['DespesaPagamento'][0]['Pagamento']['valor_nominal'] > $adesao['DespesaPagamento'][0]['Pagamento']['valor_pago'] ? $adesao['DespesaPagamento'][0]['Pagamento']['valor_pago'] : $adesao['DespesaPagamento'][0]['Pagamento']['valor_nominal'];
                    $valorPago = $valorPago > $adesao['Despesa']['valor'] ? $adesao['Despesa']['valor'] : $valorPago;
                    $totalPago+= $valorPago;
                    $valorParcela = $adesao['Despesa']['valor'] + $adesao['Despesa']['correcao_igpm'];
                    $saldoTotal+= $valorPago - $valorParcela;
                }
            }

            $this->Despesa->bindModel(array('belongsTo' => array('CampanhasUsuario')), false);
            $this->Despesa->recursive = 2;
            $this->CampanhasUsuario->bindModel(array('belongsTo' => array('Campanha')), false);
            $extras = $this->Despesa->find('all', array('conditions' => array('Despesa.usuario_id' => $formando['ViewFormandos']['id'], 'Despesa.tipo' => 'extra')));
            $this->set('despesas_extras', $extras);

            foreach ($extras as $extra) {
                $totalDespesasExtras += $extra['Despesa']['valor'];
                $totalIGPM +=!empty($extra['Despesa']['correcao_igpm']) ? $extra['Despesa']['correcao_igpm'] : 0;
                if ($extra['Despesa']['status'] == 'aberta') {
                    $saldoTotal-= $extra['Despesa']['valor'];
                } elseif (sizeof($extra['DespesaPagamento']) != 0 && $extra['Despesa']['status'] == 'paga') {
                    $valorPago = $extra['DespesaPagamento'][0]['Pagamento']['valor_nominal'] > $extra['DespesaPagamento'][0]['Pagamento']['valor_pago'] ? $extra['DespesaPagamento'][0]['Pagamento']['valor_pago'] : $extra['DespesaPagamento'][0]['Pagamento']['valor_nominal'];
                    $valorPago = $valorPago > $extra['Despesa']['valor'] ? $extra['Despesa']['valor'] : $valorPago;
                    $totalPago+= $valorPago;
                    $saldoTotal+= $valorPago - $extra['Despesa']['valor'];
                }
            }

            $pagamentos = $this->Pagamento->find('all', array('conditions' => array('Pagamento.status' => 'pago', 'Pagamento.usuario_id' => $formando['ViewFormandos']['id'])));
            $totalDespesas = $totalDespesasExtras + $totalDespesasAdesao + $totalIGPM;
            $this->set('totalDespesasAdesao', $totalDespesasAdesao);
            $this->set('totalDespesasExtras', $totalDespesasExtras);
            $this->set('totalDespesas', $totalDespesas);
            $this->set('totalIGPM', $totalIGPM);
            $this->set('totalPago', $totalPago);
            $this->set('saldoTotal', $saldoTotal);

            App::import('Controller', 'Formandos');
            $Formandos = new FormandosController;
            $Formandos->constructClasses();
            $this->Despesa->bindModel(array('belongsTo' => array('CampanhasUsuario')), false);
            $this->Despesa->recursive = 3;
            $this->CampanhasUsuario->bindModel(
                    array(
                'belongsTo' => array('Campanha'),
                'hasMany' => array(
                    'CampanhasUsuariosCampanhasExtras' => array('foreignKey' => 'campanhas_usuario_id')
                )
                    ), false);
            $extras = $this->Despesa->find('all', array('conditions' => array('Despesa.usuario_id' => $formando['ViewFormandos']['id'], 'Despesa.tipo' => 'extra')));
            $arrayDeCampanhasAderidas = array();

            foreach ($extras as $despesaExtra) {
                if (!in_array($despesaExtra['CampanhasUsuario']['campanha_id'], $arrayDeCampanhasAderidas))
                    $arrayDeCampanhasAderidas[] = $despesaExtra['CampanhasUsuario']['campanha_id'];
            }

            $resumoExtras = $Formandos->montar_resumo_extras($extras, $arrayDeCampanhasAderidas);
            $this->set('resumoExtras', $resumoExtras);
            $dataAssinaturaContrato = date('d/m/Y', strtotime($turma['Turma']['data_assinatura_contrato']));
            $dataUmAnoAposAssinatura = date('d/m/Y', mktime(0, 0, 0, date('m', strtotime($turma['Turma']['data_assinatura_contrato'])), date('d', strtotime($turma['Turma']['data_assinatura_contrato'])), (int) date('Y', strtotime($turma['Turma']['data_assinatura_contrato'])) + 1));
            $dataDoisAnosAposAssinatura = date('d/m/Y', mktime(0, 0, 0, date('m', strtotime($turma['Turma']['data_assinatura_contrato'])), date('d', strtotime($turma['Turma']['data_assinatura_contrato'])), (int) date('Y', strtotime($turma['Turma']['data_assinatura_contrato'])) + 2));
            $textoIGPM = "Conforme consta em contrato, os valores s&atilde;o corrigidos de 12 em 12 meses a partir da assinatura do contrato entre";
            $textoIGPM.= " a comiss&atilde;o de formatura e a RK formaturas.<br /><br /> O &iacute;ndice utilizado &eacute; o IGPM.";
            $textoIGPM.= " O seu contrato foi assinado entre a RK e a comiss&atilde;o em $dataAssinaturaContrato";
            $textoIGPM.= ".<br /><br />Sendo assim, caso voc&ecirc; tenha aderido com o valor inicial, todas as parcelas n&atilde;o pagas at&eacute; $dataUmAnoAposAssinatura";
            $textoIGPM.= " sofrer&atilde;o reajustes.<br /><br />Caso voc&ecirc; j&aacute; tenha aderido com o valor corrigido, as parcelas s&oacute; ser&atilde;o corrigidas";
            $textoIGPM.= " a partir do pr&oacute;ximo ciclo de 12 meses, ou seja ap&oacute;s $dataDoisAnosAposAssinatura, caso voc&ecirc; possua valores em aberto ap&oacute;s essa data.";
            $this->set('textoIGPM', $textoIGPM);
        } else {
            $this->Session->setFlash("Erro ao efetuar checkout. Entre em contato conosco", 'flash_erro');
            $this->redirect("/{$this->params['prefix']}");
        }
    }
    
    function atendimento_listar() {
        $usuario = $this->Session->read('Usuario');
        $this->loadModel('Protocolo');
        $this->Protocolo->bindModel(array(
            'hasOne' => array(
                'ViewFormandos' => array(
                    'foreignKey' => false,
                    'conditions' => array('Protocolo.usuario_id = ViewFormandos.id'))
            )
        ), false);
        $options['conditions'] = array(
            'usuario_criador' => $usuario['Usuario']['id'],
            'tipo' => 'checkout'
        );
        if(!empty($this->data)) {
            if(!empty($this->data['filtro-id'])) {
                if($this->data['filtro-tipo'] == 'turma_id')
                    $options['conditions']['ViewFormandos.turma_id'] = $this->data['filtro-id'];
                elseif($this->data['filtro-tipo'] == 'data_cadastro')
                    $options['conditions']["date_format(Protocolo.data_cadastro,'%d/%m/%Y')"] = $this->data['filtro-id'];
            }
        }
        $protocolos = $this->Protocolo->find('all',$options);
        $listaCheckout = array();
        foreach($protocolos as $protocolo) {
            $pagamento = $this->CheckoutFormandoPagamento->find('first',array(
                'conditions' => array('protocolo_id' => $protocolo['Protocolo']['id']),
                'group' => array('protocolo_id'),
                'fields' => array(
                    "sum(if(CheckoutFormandoPagamento.tipo = 'cheque',valor,0)) as total_cheque",
                    "sum(if(CheckoutFormandoPagamento.tipo = 'cheque',1,0)) as qtde_cheque",
                    "sum(if(CheckoutFormandoPagamento.tipo = 'dinheiro',valor,0)) as total_dinheiro"
                )
            ));
            $listaCheckout[] = array(
                'formando' => $protocolo['ViewFormandos']['nome'],
                'data' => $protocolo['Protocolo']['data_cadastro'],
                'codigo' => $protocolo['ViewFormandos']['codigo_formando'],
                'cheque' => array('total' => $pagamento[0]['total_cheque'],
                    'qtde' => $pagamento[0]['qtde_cheque']),
                'dinheiro' => $pagamento[0]['total_dinheiro'],
                'protocolo' => $protocolo['Protocolo']['protocolo']
            );
        }
        $this->set('filtroBusca',array('turma_id' => 'Turma','data_cadastro' => 'Data'));
        $this->set('listaCheckout',$listaCheckout);
    }
    
    function atendimento_realizar($usuarioId) {
        $this->layout = 'metro/externo';
        $this->loadModel('CheckoutItem');
        $this->loadModel('CheckoutUsuarioItem');
        $formando = $this->Usuario->read(null,$usuarioId);
        $protocolo = $this->Protocolo->find('count',array(
            'conditions' => array(
                'Protocolo.usuario_id' => $usuarioId,
                'Protocolo.tipo' => 'checkout'
            )
        ));
        if($protocolo > 0) {
            $this->Session->setFlash("Checkout do formando já foi realizado", 'metro/flash/error');
            $this->redirect('/');
        }
        $this->set('formando',$formando);
        $this->data['Usuario'] = $formando['Usuario'];
        $this->data['FormandoProfile'] = $formando['FormandoProfile'];
        $despesasAdesao = $this->Financeiro->obterDespesasAdesao($usuarioId);
        $this->set('despesasAdesao',$despesasAdesao);
        $ultimaParcela = end($despesasAdesao);
        $qtdeParcelas = ceil((strtotime($ultimaParcela['data_vencimento']) - strtotime('-1 month')) / (60 * 60 * 24) / 30);
        $qtdeParcelas = $qtdeParcelas < 4 ? 4 : $qtdeParcelas;
        $adesao = ($this->Financeiro->obterValorAdesaoComIgpm($usuarioId)) - $this->Financeiro->obterValorPagoAdesao($usuarioId);
        if($adesao == 0)
            $qtdeParcelas = 1;
        $this->set('qtdeParcelas', $qtdeParcelas);
        $igpm = $this->Financeiro->obterDespesaIgpm($usuarioId);
        $this->set('igpm',$igpm);
        $this->loadModel("ViewRelatorioExtras");
        $extras = $this->ViewRelatorioExtras->find('all',array(
            'conditions' => array('usuario_id' => $usuarioId),
            'order' => array('data_compra' => 'asc')
        ));
        $campanhas = array();
        foreach($extras as $extra) {
            $campanhaId = $extra['ViewRelatorioExtras']['campanha_id'];
            $campanhaUsuarioId = $extra['ViewRelatorioExtras']['campanhas_usuario_id'];
            if(!isset($campanhas[$campanhaId]))
                $campanhas[$campanhaId] = array(
                    'nome' => $extra['ViewRelatorioExtras']['campanha'],
                    'compras' => array()
                );
            if(!isset($campanhas[$campanhaId]['compras'][$campanhaUsuarioId]))
                $campanhas[$campanhaId]['compras'][$campanhaUsuarioId] = array(
                    'data_compra' => $extra['ViewRelatorioExtras']['data_compra'],
                    'valor_campanha' => $extra['ViewRelatorioExtras']['valor_campanha'],
                    'valor_pago' => $extra['ViewRelatorioExtras']['valor_pago'],
                    'status' => $extra['ViewRelatorioExtras']['status'],
                    'cancelada' => $extra['ViewRelatorioExtras']['cancelada'],
                    'itens' => array()
                );
            $campanhas[$campanhaId]['compras'][$campanhaUsuarioId]['itens'][] = array(
                'evento' => $extra['ViewRelatorioExtras']['evento'],
                'nome' => $extra['ViewRelatorioExtras']['extra'],
                'quantidade' => $extra['ViewRelatorioExtras']['quantidade_total'],
                'quantidade_mesas' => $extra['ViewRelatorioExtras']['quantidade_mesas'],
                'quantidade_convites' => $extra['ViewRelatorioExtras']['quantidade_convites']
            );
        }
        foreach($campanhas as &$campanha)
            foreach($campanha['compras'] as $campanhaUsuarioId => &$compra)
                $compra['despesas'] = $this->Financeiro->obterDespesasExtras($campanhaUsuarioId,true);
        $this->set('campanhas',$campanhas);
        $itensExtras = $this->CheckoutItem->find('all',array(
            'conditions' => array(
                'turma_id' => $formando['ViewFormandos']['turma_id']
            )
        ));
        $listaItens = array();
        if ($itensExtras) {
            $listaItens = array();
            foreach ($itensExtras as $indice => $itemExtra) {
                $itensComprados = $this->CheckoutUsuarioItem->find('first', array(
                    'conditions' => array(
                        'checkout_item_id' => $itemExtra['CheckoutItem']['id']
                    ),
                    'fields' => array(
                        'sum(CheckoutUsuarioItem.quantidade) as total'
                    )
                ));
                $qtdeRestante = $itemExtra['CheckoutItem']['quantidade'] - $itensComprados[0]['total'];
                $itemExtra['CheckoutItem']['itens_por_pessoa'] = min(array(
                        $qtdeRestante,
                        $itemExtra['CheckoutItem']['itens_por_pessoa']
                    ));
                if ($itemExtra['CheckoutItem']['itens_por_pessoa'] > 0)
                    $listaItens[] = $itensExtras[$indice]['CheckoutItem'];
            }
        }
        $this->set('itensExtras', $listaItens);
        $totalAdesao = $this->Financeiro->obterValorAdesao($usuarioId);
        $totalExtras = $this->Financeiro->obterValorExtras($usuarioId);
        $totalIgpm = $this->Financeiro->obterValorIgpm($usuarioId);
        $totalPago = $this->Financeiro->obterTotalPago($usuarioId);
        $saldo = $totalPago-($totalExtras+$totalIgpm+$totalAdesao);
        $this->set('saldo',$saldo);
        $interesseAlbum = array(
            'muito_interesse' => 'Muito Interessado',
            'interesse' => 'Interessado',
            'pouco_interesse' => 'Pouco Interessado',
            'nenhum_interesse' => 'Não Quero Ver Meu Álbum'
        );
        $formaPagamento = array(
            'dinheiro' => 'À vista em dinheiro',
            'cheque' => 'Uma vez em cheque',
            'parcelar' => 'Parcelar Valor'
        );
        $tipoPagamento = array(
            'cheque' => 'Cheque',
            'dinheiro' => 'Dinheiro',
            'comprovante' => 'Comprovante'
        );
        $this->set('interesseAlbum', $interesseAlbum);
        $this->set('formaPagamento', $formaPagamento);
        $this->set('tipoPagamento', $tipoPagamento);
    }
    
    function saldo_formando($usuarioId) {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array();
        $usuario = $this->Usuario->read(null,$usuarioId);
        if($usuario) {
            $json['saldo'] = array(
                'adesao' => floatval(floor($this->Financeiro->obterValorAdesao($usuarioId)))*-1,
                'igpm' => floatval(floor($this->Financeiro->obterValorIgpm($usuarioId)))*-1,
                'extras' => floatval(floor($this->Financeiro->obterValorExtras($usuarioId)))*-1,
                'pago' => floatval(floor($this->Financeiro->obterTotalPago($usuarioId)))
            );
        } else {
            $json['mensagem'] = 'Erro ao buscar formando';
        }
        echo json_encode($json);
    }

    function atendimento_iniciar($usuarioId) {
        $usuario = $this->Session->read('Usuario');
        $this->loadModel('CheckoutItem');
        $this->loadModel('CheckoutUsuarioItem');
        $this->ViewFormandos->id = $usuarioId;
        $formando = $this->ViewFormandos->read();
        if (!empty($formando)) {
            $this->loadModel('ProtocoloCheckoutFormando');
            $protocolo = $this->ProtocoloCheckoutFormando->find('first', array('conditions' => array('Protocolo.usuario_id' => $formando['ViewFormandos']['id'], 'tipo' => 'checkout')));

            if (!empty($protocolo)) {
                $this->Session->setFlash("Checkout do formando já foi realizado", 'flash_erro');
                $this->redirect("/{$this->params['prefix']}");
            }

            $this->set('formando', $formando['ViewFormandos']);
            $turma = $this->Turma->find('first', array('conditions' => array('id' => $formando['ViewFormandos']['turma_id'])));
            $this->set('turma', $turma);
            $this->Despesa->recursive = 2;
            $adesoes = $this->Despesa->find('all',array(
                'conditions' => array(
                    'Despesa.usuario_id' => $formando['ViewFormandos']['id'],
                    'Despesa.tipo' => 'adesao',
                    'Despesa.status not' => array('cancelada','renegociada'),
                )
            ));
            $this->set('despesas_adesao', $adesoes);
            $ultimaParcela = end($adesoes);
            $qtdeParcelas = ceil((strtotime($ultimaParcela['Despesa']['data_vencimento']) - strtotime('-1 month')) / (60 * 60 * 24) / 30);
            $qtdeParcelas = $qtdeParcelas < 4 ? 4 : $qtdeParcelas;

            $itensExtras = $this->CheckoutItem->find('all', array('conditions' => array('turma_id' => $formando['ViewFormandos']['turma_id'])));

            if (!empty($itensExtras)) {
                $itensExtrasFormatado = array();
                foreach ($itensExtras as $indice => $itemExtra) {
                    $itensComprados = $this->CheckoutUsuarioItem->find('first', array(
                        'conditions' => array('checkout_item_id' => $itemExtra['CheckoutItem']['id']),
                        'fields' => array('sum(CheckoutUsuarioItem.quantidade) as total')));
                    $qtdeRestante = $itemExtra['CheckoutItem']['quantidade'] - $itensComprados[0]['total'];
                    $qtdeMaximaItens = $qtdeRestante > $itemExtra['CheckoutItem']['itens_por_pessoa'] ? $itemExtra['CheckoutItem']['itens_por_pessoa'] : $qtdeRestante;
                    if ($qtdeMaximaItens > 0) {
                        $itensExtras[$indice]['CheckoutItem']['itens_por_pessoa'] = $qtdeMaximaItens;
                        $itensExtrasFormatado[] = $itensExtras[$indice]['CheckoutItem'];
                    }
                }
                $this->set('itensExtras', $itensExtrasFormatado);
            } else {
                $this->set('itensExtras', false);
            }
            $totalDespesasAdesao = 0;
            $totalDespesasExtras = 0;
            $totalIGPM = array('despesa_aberta' => 0, 'despesa_paga' => 0);
            $totalPagoAdesao = 0;
            $totalPagoExtras = 0;
            $totalPagoNaoVinculado = 0;

            foreach ($adesoes as $adesao) {
                $totalDespesasAdesao += $adesao['Despesa']['valor'];
                if ($adesao['Despesa']['status'] == 'aberta' && $adesao['Despesa']['status_igpm'] != "pago")
                    $totalIGPM['despesa_aberta'] +=!empty($adesao['Despesa']['correcao_igpm']) ? $adesao['Despesa']['correcao_igpm'] : 0;
                elseif ($adesao['Despesa']['status'] == 'paga' && $adesao['Despesa']['status_igpm'] != "pago")
                    $totalIGPM['despesa_paga'] +=!empty($adesao['Despesa']['correcao_igpm']) ? $adesao['Despesa']['correcao_igpm'] : 0;
                if (sizeof($adesao['DespesaPagamento']) != 0 && $adesao['Despesa']['status'] == 'paga') {
                    $valorPago = $adesao['DespesaPagamento'][0]['Pagamento']['valor_nominal'] > $adesao['DespesaPagamento'][0]['Pagamento']['valor_pago'] ? $adesao['DespesaPagamento'][0]['Pagamento']['valor_pago'] : $adesao['DespesaPagamento'][0]['Pagamento']['valor_nominal'];
                    $valorPago = $valorPago > $adesao['Despesa']['valor'] ? $adesao['Despesa']['valor'] : $valorPago;
                    $totalPagoAdesao+= $valorPago;
                }
            }
            
            $canceladas = $this->Despesa->find('all', array('conditions' =>
                array('Despesa.usuario_id' => $formando['ViewFormandos']['id'], 'Despesa.tipo' => 'adesao',
                    "Despesa.status" => 'cancelada')));
            foreach($canceladas as $cancelada) {
                if (sizeof($cancelada['DespesaPagamento']) != 0) {
                    $valorPago = $cancelada['DespesaPagamento'][0]['Pagamento']['valor_nominal'] > $cancelada['DespesaPagamento'][0]['Pagamento']['valor_pago'] ? $cancelada['DespesaPagamento'][0]['Pagamento']['valor_pago'] : $cancelada['DespesaPagamento'][0]['Pagamento']['valor_nominal'];
                    $valorPago = $valorPago > $cancelada['Despesa']['valor'] ? $cancelada['Despesa']['valor'] : $valorPago;
                    $totalPagoAdesao+= $valorPago;
                }
            }

            $despesaIgpm = $this->Despesa->find('first', array('conditions' => array(
                    'Despesa.tipo' => 'igpm',
                    'Despesa.usuario_id' => $formando['ViewFormandos']['id'],
                    "Despesa.status != 'cancelada'")));
            $this->set('despesaIgpm', $despesaIgpm);
            if (!empty($despesaIgpm)) {
                $totalIGPM['despesa_paga'] = $despesaIgpm['Despesa']['valor'];
                if (sizeof($despesaIgpm['DespesaPagamento']) != 0 && $despesaIgpm['Despesa']['status'] == 'paga') {
                    $valorPago = $despesaIgpm['DespesaPagamento'][0]['Pagamento']['valor_nominal'] > $despesaIgpm['DespesaPagamento'][0]['Pagamento']['valor_pago'] ? $despesaIgpm['DespesaPagamento'][0]['Pagamento']['valor_pago'] : $despesaIgpm['DespesaPagamento'][0]['Pagamento']['valor_nominal'];
                    $valorPago = $valorPago > $despesaIgpm['Despesa']['valor'] ? $despesaIgpm['Despesa']['valor'] : $valorPago;
                    $totalPagoAdesao+= $valorPago;
                }
            }
            $this->Despesa->bindModel(array('belongsTo' => array('CampanhasUsuario')), false);
            $this->Despesa->recursive = 2;
            $this->CampanhasUsuario->bindModel(array('belongsTo' => array('Campanha')), false);
            $extras = $this->Despesa->find('all', array('conditions' => array('Despesa.usuario_id' => $formando['ViewFormandos']['id'], 'Despesa.tipo' => 'extra')));
            $this->set('despesas_extras', $extras);

            foreach ($extras as $extra) {
                if ($extra['Despesa']['status'] != "cancelada")
                    $totalDespesasExtras += $extra['Despesa']['valor'];
                if (sizeof($extra['DespesaPagamento']) != 0 && $extra['Despesa']['status'] != 'aberta') {
                    $valorPago = $extra['DespesaPagamento'][0]['Pagamento']['valor_nominal'] > $extra['DespesaPagamento'][0]['Pagamento']['valor_pago'] ? $extra['DespesaPagamento'][0]['Pagamento']['valor_pago'] : $extra['DespesaPagamento'][0]['Pagamento']['valor_nominal'];
                    $valorPago = $valorPago > $extra['Despesa']['valor'] ? $extra['Despesa']['valor'] : $valorPago;
                    $totalPagoExtras+= $valorPago;
                }
            }

            $pagamentos = $this->Pagamento->find('all', array('conditions' => array('Pagamento.status' => 'pago', 'Pagamento.usuario_id' => $formando['ViewFormandos']['id'])));
            $pagamentos_nao_vinculados = array();
            foreach ($pagamentos as $pagamento) {
                if (sizeof($pagamento['DespesaPagamento']) == 0) {
                    $pagamentos_nao_vinculados[] = $pagamento;
                    $totalPagoNaoVinculado += $pagamento['Pagamento']['valor_nominal'];
                }
            }

            $totalPago = $totalPagoAdesao + $totalPagoExtras + $totalPagoNaoVinculado;
            $totalDespesasAdesao = max(array(
                $formando["ViewFormandos"]['valor_adesao'],
                $totalDespesasAdesao
            ));
            $totalDespesas = $totalDespesasExtras + $totalDespesasAdesao + array_sum($totalIGPM);
            $saldoAdesao = $totalPagoAdesao - ($totalDespesasAdesao + array_sum($totalIGPM));
            $saldoExtras = $totalPagoExtras - $totalDespesasExtras;
            $saldoTotal = $totalPago - $totalDespesas;
            $qtdeParcelas = $saldoAdesao != 0 ? $qtdeParcelas : 0;
            
            $usuarioId = $formando["ViewFormandos"]['id'];
            
            $totalAdesao = $this->Financeiro->obterValorAdesao($usuarioId);
            $totalExtras = $this->Financeiro->obterValorExtras($usuarioId);
            $totalIgpm = $this->Financeiro->obterValorIgpm($usuarioId);
            $totalPagoAdesao = $this->Financeiro->obterValorPagoAdesao($usuarioId);
            $totalPagoExtras = $this->Financeiro->obterValorPagoCampanha($usuarioId);
            $totalPagoIgpm = $this->Financeiro->obterValorPagoIgpm($usuarioId);
            $totalPago = $this->Financeiro->obterTotalPago($usuarioId);
            $saldoTotal = $totalPago-($totalExtras+$totalIgpm+$totalAdesao);
            $saldoAdesao = ($totalPagoAdesao+$totalPagoIgpm)-($totalAdesao+$totalIgpm);
            $saldoExtras = $totalPagoExtras-$totalExtras;
            $igpm = $this->Financeiro->obterDespesaIgpm($usuarioId);
            $this->set('despesaIgpm',$igpm);
            
            $this->set('qtdeParcelas', $qtdeParcelas);
            $this->set('boletos', $pagamentos_nao_vinculados);
            $this->set('totalDespesasAdesao', $totalAdesao);
            $this->set('totalDespesasExtras', $totalExtras);
            $this->set('totalDespesas', $totalAdesao+$totalExtras+$totalIgpm);
            $this->set('totalIGPM', $totalIgpm);
            $this->set('totalPagoIGPM', $totalPagoIgpm);
            $this->set('totalPago', $totalPago);
            $this->set('saldoAdesao', $saldoAdesao);
            $this->set('saldoExtras', $saldoExtras);
            $this->set('saldoTotal', $saldoTotal);
            $this->set('interesse_album', array(
                'muito_interesse' => 'Muito Interessado',
                'interesse' => 'Interessado',
                'pouco_interesse' => 'Pouco Interessado',
                'nenhum_interesse' => 'Não Quero Ver Meu Álbum'));
            $this->loadModel('ViewRelatorioExtras');
            $resumoExtras = $this->ViewRelatorioExtras->find('all', array('conditions' => array(
                    'ViewRelatorioExtras.codigo_formando' => $formando['ViewFormandos']['codigo_formando'])));
            $this->set('resumoExtras', $resumoExtras);
            $dataAssinaturaContrato = date('d/m/Y', strtotime($turma['Turma']['data_assinatura_contrato']));
            $dataUmAnoAposAssinatura = date('d/m/Y', mktime(0, 0, 0, date('m', strtotime($turma['Turma']['data_assinatura_contrato'])), date('d', strtotime($turma['Turma']['data_assinatura_contrato'])), (int) date('Y', strtotime($turma['Turma']['data_assinatura_contrato'])) + 1));
            $dataDoisAnosAposAssinatura = date('d/m/Y', mktime(0, 0, 0, date('m', strtotime($turma['Turma']['data_assinatura_contrato'])), date('d', strtotime($turma['Turma']['data_assinatura_contrato'])), (int) date('Y', strtotime($turma['Turma']['data_assinatura_contrato'])) + 2));
            $textoIGPM = "Conforme consta em contrato, os valores s&atilde;o corrigidos de 12 em 12 meses a partir da assinatura do contrato entre";
            $textoIGPM.= " a comiss&atilde;o de formatura e a RK formaturas.<br /><br /> O &iacute;ndice utilizado &eacute; o IGPM.";
            $textoIGPM.= " O seu contrato foi assinado entre a RK e a comiss&atilde;o em $dataAssinaturaContrato";
            $textoIGPM.= ".<br /><br />Sendo assim, caso voc&ecirc; tenha aderido com o valor inicial, todas as parcelas n&atilde;o pagas at&eacute; $dataUmAnoAposAssinatura";
            $textoIGPM.= " sofrer&atilde;o reajustes.<br /><br />Caso voc&ecirc; j&aacute; tenha aderido com o valor corrigido, as parcelas s&oacute; ser&atilde;o corrigidas";
            $textoIGPM.= " a partir do pr&oacute;ximo ciclo de 12 meses, ou seja ap&oacute;s $dataDoisAnosAposAssinatura, caso voc&ecirc; possua valores em aberto ap&oacute;s essa data.";
            $this->set('textoIGPM', $textoIGPM);
        } else {
            $this->Session->setFlash("Usuário não encontrado", 'flash_erro');
            $this->redirect("/{$this->params['prefix']}");
        }
    }

    function planejamento_itens() {
        $this->atendimento_itens();
    }
    
    function planejamento_cadastrar_itens(){
        
        $turma = $this->obterTurmaLogada();
        if (empty($turma)) {
            $this->Session->setFlash('Selecione um turma para cadastrar itens.', 'metro/flash/error');
        } else {
            $this->loadModel('CheckoutItem');
            if ($this->data) {
                if (empty($this->data['CheckoutItem']['itens'])) {
                    if (!$this->CheckoutItem->deleteAll(array('CheckoutItem.turma_id' => $turma['Turma']['id']), false))
                        $this->Session->setFlash('Erro ao apagar itens. Tente novamente mais tarde.', 'metro/flash/error');
                    else
                        $this->Session->setFlash('Itens apagados com sucesso.', 'metro/flash/success');
                } else {
                    $erros = array();
                    foreach ($this->data['CheckoutItem']['itens'] as $indice => $item) {
                        $item['turma_id'] = $turma['Turma']['id'];
                        $this->CheckoutItem->id = isset($item['id']) ? $item['id'] : null;
                        $item['valor'] = number_format(str_replace(',', '.', $item['valor']), 2, '.', '');
                        if (!$this->CheckoutItem->save($item))
                            array_push($erros, "Erro ao salvar " . ($indice + 1) . "&ordm; item.");
                    }
                    $this->autoRender = false;
                    Configure::write(array('debug' => 0));
                    if (empty($erros)) {
                        $this->Session->setFlash('Itens salvos com sucesso.', 'metro/flash/success');
                    } else {
                        $mensagens = "";
                        foreach ($erros as $erro)
                            $mensagens.= "$erro<br />";
                        $this->Session->setFlash($mensagens, 'metro/flash/error');
                    }
                }
                    echo json_encode(array());
                    exit();
            }
            $this->set('turma', $turma);
            $itens = $this->CheckoutItem->find('all', array('conditions' => array('turma_id' => $turma['Turma']['id'])));
            $this->set('itens', $itens);
        }
    }

    function super_convites() {
        $this->planejamento_convites();
    }
    
    function planejamento_listar(){
        $this->layout = false;
        $turma = $this->obterTurmaLogada();
        $this->loadModel('CheckoutItem');
        $itens = $this->CheckoutItem->find('all', array('conditions' => array('turma_id' => $turma['Turma']['id'])));
        $this->set('itens', $itens);
    }
    
    function planejamento_alterar($id = false){
        $this->layout = false;
        $this->loadModel('CheckoutItem');
        $checkoutItem = $this->CheckoutItem->find('first', array(
            'conditions' => array(
                'CheckoutItem.id' => $id
                )
        ));
        if (!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            if(!strpos($this->data['CheckoutItem']['valor'],".")&&(strpos($this->data['CheckoutItem']['valor'],",")))
                $this->data['CheckoutItem']['valor'] = substr_replace($this->data['CheckoutItem']['valor'], '.', strpos($this->data['CheckoutItem']['valor'], ","), 1);
            if(!empty($this->data['CheckoutItem']['brinde'])){
                $this->data['CheckoutItem']['nome'] = "Brinde - " . $this->data['Extra']['brinde'];
                unset($this->data['CheckoutItem']['brinde']);
            }elseif(!empty($this->data['CheckoutItem']['outros'])){
                $this->data['CheckoutItem']['nome'] = "Outros - " . $this->data['Extra']['outros'];
                unset($this->data['CheckoutItem']['outros']);
            }
            if ($this->CheckoutItem->save($this->data['CheckoutItem'])) {
                $this->Session->setFlash('Item alterado com sucesso.', 'metro/flash/success');
            } else {
                $this->Session->setFlash('Erro ao alterar o item.', 'metro/flash/error');
            }
        }else {
            if (empty($checkoutItem)) {
                $this->Session->setFlash('Extra não existente.', 'metro/flash/error');
            } else {
                $this->data = $checkoutItem;
            }
        }
        $this->set('extras', array('' => 'Selecione', 'Convite Extra 1' => 'Convite Extra 1', 'Convite Infantil 1' => 'Convite Infantil 1', 'Mesa Extra 1' => 'Mesa Extra 1', 
                                    'Combo 05 1' => 'Combo 05 1', 'Combo 06 1' => 'Combo 06 1', 'Combo 08 1' => 'Combo 08 1', 'Combo 10 1' => 'Combo 10 1', 
                                    'Convite Extra 2' => 'Convite Extra 2', 'Convite Infantil 2' => 'Convite Infantil 2', 'Mesa Extra 2' => 'Mesa Extra 2', 
                                    'Combo 05 2' => 'Combo 05 2', 'Combo 06 2' => 'Combo 06 2', 'Combo 08 2' => 'Combo 08 2', 'Combo 10 2' => 'Combo 10 2', 'Combo' => 'Combo', 'Convite Luxo' => 'Convite Luxo', 'Brinde' => 'Brinde', 'Outros' => 'Outros'));
        $this->set('checkoutItem', $checkoutItem);
    }
    
    function planejamento_inserir_itens(){
        $this->layout = false;
        $turma = $this->obterTurmaLogada();
        if(!empty($this->data)){
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $this->loadModel('CheckoutItem');
            if(!strpos($this->data['CheckoutItem']['valor'],".")&&(strpos($this->data['CheckoutItem']['valor'],",")))
                $this->data['CheckoutItem']['valor'] = substr_replace($this->data['CheckoutItem']['valor'], '.', strpos($this->data['CheckoutItem']['valor'], ","), 1);
            if(!empty($this->data['CheckoutItem']['brinde']))
                $this->data['CheckoutItem']['titulo'] = "Brinde - {$this->data['CheckoutItem']['brinde']}";
            elseif(!empty($this->data['CheckoutItem']['outros']))
                $this->data['CheckoutItem']['titulo'] = "Outros - {$this->data['CheckoutItem']['outros']}";
            unset($this->data['CheckoutItem']['outros']);
            unset($this->data['CheckoutItem']['brinde']);
            if($this->CheckoutItem->save($this->data['CheckoutItem']))
                $this->Session->setFlash('Item salvo com sucesso.', 'metro/flash/success');
            else
                $this->Session->setFlash('O item não foi salvo.', 'metro/flash/error');
        }
        $this->loadModel('Evento');
        $eventos = $this->Evento->find('all', array(
           'conditions' => array(
               'Evento.turma_id' => $turma['Turma']['id']
           ) 
        ));
        if(!empty($eventos)){
            $e = array();
            foreach($eventos as $ev)
                $e[$ev['Evento']['id']] = $ev['Evento']['nome'];
        }else{
            $e[] = "Nenhum evento cadastrado";
        }
        $this->set('eventos', $e);
        $this->set('extras', array('' => 'Selecione', 'Convite Extra 1' => 'Convite Extra 1', 'Convite Infantil 1' => 'Convite Infantil 1', 'Mesa Extra 1' => 'Mesa Extra 1', 
                                    'Combo 05 1' => 'Combo 05 1', 'Combo 06 1' => 'Combo 06 1', 'Combo 08 1' => 'Combo 08 1', 'Combo 10 1' => 'Combo 10 1', 
                                    'Convite Extra 2' => 'Convite Extra 2', 'Convite Infantil 2' => 'Convite Infantil 2', 'Mesa Extra 2' => 'Mesa Extra 2', 
                                    'Combo 05 2' => 'Combo 05 2', 'Combo 06 2' => 'Combo 06 2', 'Combo 08 2' => 'Combo 08 2', 'Combo 10 2' => 'Combo 10 2', 'Combo' => 'Combo', 'Convite Luxo' => 'Convite Luxo', 'Brinde' => 'Brinde', 'Outros' => 'Outros'));
        $this->set('turma', $turma);
    }

    function atendimento_itens() {
        $turma = $this->obterTurmaLogada();
        if (empty($turma)) {
            $this->Session->setFlash('Selecione um turma para cadastrar itens.', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}");
        } else {
            $this->loadModel('CheckoutItem');
            if ($this->data) {
                if (empty($this->data['CheckoutItem']['itens'])) {
                    if (!$this->CheckoutItem->deleteAll(array('CheckoutItem.turma_id' => $turma['Turma']['id']), false))
                        $this->Session->setFlash('Erro ao apagar itens. Tente novamente mais tarde.', 'flash_erro');
                    else
                        $this->Session->setFlash('Itens apagados com sucesso.', 'flash_sucesso');
                } else {
                    $erros = array();
                    foreach ($this->data['CheckoutItem']['itens'] as $indice => $item) {
                        $this->calcularQuantidadeConvites($item);
                        $item['turma_id'] = $turma['Turma']['id'];
                        $this->CheckoutItem->id = isset($item['id']) ? $item['id'] : null;
                        $item['valor'] = number_format(str_replace(',', '.', $item['valor']), 2, '.', '');
                        if (!$this->CheckoutItem->save($item))
                            array_push($erros, "Erro ao salvar " . ($indice + 1) . "&ordm; item.");
                    }
                    if (empty($erros)) {
                        $this->Session->setFlash('Itens salvos com sucesso.', 'flash_sucesso');
                    } else {
                        $mensagens = "";
                        foreach ($erros as $erro)
                            $mensagens.= "$erro<br />";
                        $this->Session->setFlash($mensagens, 'flash_erro');
                    }
                }
            }
            $this->set('turma', $turma);
            $itens = $this->CheckoutItem->find('all', array('conditions' => array('turma_id' => $turma['Turma']['id'])));
            $this->set('itens', $itens);
        }
    }

    function formando_finalizar() {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $usuario = $this->Session->read('Usuario');
        $formando = $this->FormandoProfile->find('first', array('conditions' => array('usuario_id' => $usuario['Usuario']['id'])));
        $json = array('error' => true);
        if (!empty($formando)) {
            $this->data['FormandoProfile']['realizou_checkout'] = 1;
            $this->data['Usuario']['nome'] = $this->formata_nome($this->data['Usuario']['nome']);
            $dateTime = $this->create_date_time_from_format('d-m-Y', $this->data['FormandoProfile']['data-nascimento']);
            if ($dateTime)
                $this->data['FormandoProfile']['data_nascimento'] = date_format($dateTime, 'Y-m-d');
            else
                $this->data['FormandoProfile']['data_nascimento'] = $formando['FormandoProfile']['data_nascimento'];
            unset($this->data['FormandoProfile']['data-nascimento']);
            $this->data['FormandoProfile']['cpf'] = str_replace(array(",", ".", "-"), "", $this->data['FormandoProfile']['cpf']);
            $this->data['FormandoProfile']['end_cep'] = str_replace(array(",", ".", "-"), "", $this->data['FormandoProfile']['end_cep']);
            foreach ($this->data['FormandoProfile'] as $campo => $valor)
                $this->data['FormandoProfile'][$campo] = is_numeric($valor) ? $valor : "'$valor'";
            foreach ($this->data['Usuario'] as $campo => $valor)
                $this->data['Usuario'][$campo] = is_numeric($valor) ? $valor : "'$valor'";
            $this->FormandoProfile->updateAll($this->data['FormandoProfile'], array('FormandoProfile.id' => $formando['FormandoProfile']['id']));
            $this->Usuario->updateAll($this->data['Usuario'], array('Usuario.id' => $formando['FormandoProfile']['usuario_id']));
            $json = array('error' => false);
        } else {
            $json['message'] = "Ocorreu um erro com os nossos servidores. Por favor tente mais tarde";
        }
        echo json_encode($json);
    }

    function formando_imprimir() {
        $usuario = $this->Session->read('Usuario');
        $this->layout = 'impressao';
        $this->formando_index($usuario['Usuario']['id']);
    }
    
    function atendimento_cadastrar() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('mensagens' => array());
        if($this->data) {
            $this->loadModel('Protocolo');
            $this->loadModel('ProtocoloCheckoutFormando');
            $this->loadModel('CheckoutUsuarioItem');
            App::import('Component', 'Protocolo');
            $protocoloComponent = new ProtocoloComponent();
            $usuarioLogado = $this->Session->read('Usuario');
            $formando = $this->Usuario->read(null,$this->data['Usuario']['id']);
            $protocolo = $protocoloComponent->geraProtocoloCheckout(array(
                'usuario_protocolo' => $usuarioLogado['Usuario']['id'],
                'usuario_id' => $formando['Usuario']['id'],
                'grupo' => 'formando')
            );
            if($protocolo) {
                $protocoloCheckoutFormando = $this->ProtocoloCheckoutFormando->find('first', array(
                    'conditions' => array(
                        'Protocolo.protocolo' => $protocolo
                )));
                $json['protocolo'] = $protocoloCheckoutFormando['ProtocoloCheckoutFormando']['protocolo_id'];
                $protocoloCheckoutFormando['ProtocoloCheckoutFormando']['turma_id'] = $formando['ViewFormandos']['turma_id'];
                $protocoloCheckoutFormando['ProtocoloCheckoutFormando']['saldo_adesao'] = $this->data['saldo'];
                if(!$this->ProtocoloCheckoutFormando->save($protocoloCheckoutFormando))
                    $json['mensagens'][] = "Erro ao atualizar dados do checkout";
                unset($this->Usuario->validate['confirmar']);
                unset($this->Usuario->validate['senha']);
                if(!$this->Usuario->save(array('Usuario' => $this->data['Usuario'])))
                    $json['mensagens'][] = "Erro ao alterar dados do usuário";
                
                $this->FormandoProfile->validate = array();
                $this->data['FormandoProfile']['realizou_checkout'] = 1;
                if(!$this->FormandoProfile->save(array('FormandoProfile' => $this->data['FormandoProfile']),
                        array('validate' => 'only')))
                    $json['mensagens'][] = "Erro ao alterar dados do formando";
                if(isset($this->data['itens']))
                    foreach($this->data['itens'] as $item) {
                        $itemInserir = array(
                            'protocolo_id' => $protocoloCheckoutFormando['ProtocoloCheckoutFormando']['protocolo_id'],
                            'usuario_id' => $formando['Usuario']['id'],
                            'checkout_item_id' => $item['id'],
                            'quantidade' => $item['quantidade'],
                            'saldo' => $item['saldo'],
                            'retirou' => 1,
                            'data_cadastro' => date('Y-m-d H:i:s')
                        );
                        $this->CheckoutUsuarioItem->create();
                        if (!$this->CheckoutUsuarioItem->save($itemInserir))
                            $json['mensagens'][] = "Erro ao cadastrar item {$item['titulo']}<br />";
                    }
                foreach ($this->data['pagamentos'] as $parcela => $pagamento) {
                    $pagamento['data_cadastro'] = date('Y-m-d H:i:s');
                    $pagamento['protocolo_id'] = $protocoloCheckoutFormando['Protocolo']['id'];
                    $pagamento['valor'] = number_format(str_replace(',', '.', $pagamento['valor']), 2, '.', '');
                    $pagamento['referente'] = substr($pagamento['referente'], 9);
                    $pagamento['usuario_id'] = $usuarioLogado['Usuario']['id'];
                    $pagamento['data_vencimento'] = !empty($pagamento['data_vencimento'])?implode('-',array_reverse(explode("/",$pagamento['data_vencimento']))):date('Y-m-d');
                    $this->CheckoutFormandoPagamento->create();
                    if (!$this->CheckoutFormandoPagamento->save($pagamento))
                        $json['mensagens'][] = "Erro ao cadastrar " . ($parcela+1) . "&ordm; pagamento<br />";
                }
            } else {
                $json['mensagens'][] = "Erro ao gerar protocolo";
            }
        } else {
            $this->redirect("/{$this->params['prefix']}/");
        }
        echo json_encode($json);
    }

    function atendimento_finalizar() {
        $this->layout = false;
        $this->autoRender = false;
     //   Configure::write(array('debug' => 0));
        if (isset($_POST['formando'])) {
            $this->loadModel('Protocolo');
            $this->loadModel('ProtocoloCheckoutFormando');
            $this->loadModel('CheckoutUsuarioItem');
            App::import('Component', 'Protocolo');
            $protocoloComponent = new ProtocoloComponent();
            $usuarioLogado = $this->Session->read('Usuario');
            $formando = $_POST['formando'];
            $pagamentos = $_POST['pagamentos']['parcelas'];
            $json = array('error' => true);
            if ($protocolo = $protocoloComponent->geraProtocoloCheckout(array('usuario_protocolo' => $usuarioLogado['Usuario']['id'], 'usuario_id' => $formando['id'], 'grupo' => 'formando'))) {
                $this->Despesa->updateAll(
                        array('Despesa.status' => "'cancelada'"), array('Despesa.status' => "'aberta'", 'Despesa.usuario_id' => $formando['id'])
                );
                $dadosFormando = array(
                    'turma_id' => $formando['turma_id'],
                    'saldo_adesao' => $_POST['adesao']['saldo'],
                    'saldo_campanhas' => $_POST['campanhas']['saldo']
                );
                $protocoloCheckoutFormando = $this->ProtocoloCheckoutFormando->find('first', array('conditions' => array('Protocolo.protocolo' => $protocolo)));
                $this->ProtocoloCheckoutFormando->id = $protocoloCheckoutFormando['ProtocoloCheckoutFormando']['id'];
                if ($this->ProtocoloCheckoutFormando->save($dadosFormando)) {
                    $json = array('protocolo' => $protocoloCheckoutFormando['ProtocoloCheckoutFormando']['protocolo_id'], 'error' => false);
                    $erros = 0;
                    $alertas = array();
                    $_POST['FormandoProfile']['realizou_checkout'] = 1;
                    $_POST['Usuario']['nome'] = $this->formata_nome($_POST['Usuario']['nome']);
                    $dateTime = $this->create_date_time_from_format('d-m-Y', $_POST['FormandoProfile']['data-nascimento']);
                    if ($dateTime)
                        $_POST['FormandoProfile']['data_nascimento'] = date_format($dateTime, 'Y-m-d');
                    else
                        $_POST['FormandoProfile']['data_nascimento'] = $formando['FormandoProfile']['data_nascimento'];
                    unset($_POST['FormandoProfile']['data-nascimento']);
                    $_POST['FormandoProfile']['cpf'] = str_replace(array(",", ".", "-"), "", $_POST['FormandoProfile']['cpf']);
                    $_POST['FormandoProfile']['end_cep'] = str_replace(array(",", ".", "-"), "", $_POST['FormandoProfile']['end_cep']);
                    foreach ($_POST['FormandoProfile'] as $campo => $valor)
                        $_POST['FormandoProfile'][$campo] = is_numeric($valor) ? $valor : "'$valor'";
                    foreach ($_POST['Usuario'] as $campo => $valor)
                        $_POST['Usuario'][$campo] = is_numeric($valor) ? $valor : "'$valor'";
                    if (!$this->FormandoProfile->updateAll($_POST['FormandoProfile'], array('FormandoProfile.usuario_id' => $formando['id'])))
                        $alertas['formando'] = "Erro ao atualizar dados do usuário";
                    if (!$this->Usuario->updateAll($_POST['Usuario'], array('Usuario.id' => $formando['id'])))
                        $alertas['formando'] = "Erro ao atualizar dados do formando";
                    $itens = isset($_POST['itens']) ? $_POST['itens'] : array();
                    foreach ($itens as $item) {
                        if (isset($item['comprados'])) {
                            if ($item['comprados'] > 0) {
                                $itemInserir = array(
                                    'protocolo_id' => $protocoloCheckoutFormando['ProtocoloCheckoutFormando']['protocolo_id'],
                                    'usuario_id' => $formando['id'],
                                    'checkout_item_id' => $item['id'],
                                    'quantidade' => $item['comprados'],
                                    'saldo' => $item['saldo'],
                                    'data_cadastro' => date('Y-m-d H:i:s')
                                );
                                $this->CheckoutUsuarioItem->create();
                                if (!$this->CheckoutUsuarioItem->save($itemInserir))
                                    $alertas['itens'].= "Erro ao cadastrar item {$item['titulo']}<br />";
                            }
                        }
                    }
                    foreach ($pagamentos as $pagamento) {
                        $referencia = "";
                        $parcela = $pagamento['id'] + 1;
                        debug($pagamento);
                        foreach ($pagamento['itens'] as $item)
                            $referencia.= "$item;";
                        $pagamento['referente'] = substr($referencia, 0, -1);
                        $pagamento['data_cadastro'] = date('Y-m-d H:i:s');
                        $pagamento['valor'] = number_format(str_replace(',', '.', $item['valor']), 2, '.', '');
                        $pagamento['protocolo_id'] = $protocoloCheckoutFormando['Protocolo']['id'];
                        $pagamento['usuario_id'] = $usuarioLogado['Usuario']['id'];
                        $pagamento['data_vencimento'] = implode('-',array_reverse(explode("/",$pagamento['data_vencimento'])));
                        $pagamento['tipo'] = $pagamento['forma_pagamento'];
                        unset($pagamento['itens']);
                        unset($pagamento['id']);
                        $this->CheckoutFormandoPagamento->create();
                      //  if (!$this->CheckoutFormandoPagamento->save($pagamento))
                      //      $alertas['parcelas'].= "Erro ao cadastrar $parcela&ordm; pagamento<br />";
                    }
                    if (!empty($alertas))
                        $json['alertas'] = $alertas;
                } else {
                    $json['message'] = "Ocorreu um erro em nossos servidores. Por favor tente novamente mais tarde. COD 002";
                }
            } else {
                $json['message'] = "Ocorreu um erro em nossos servidores. Por favor tente novamente mais tarde. COD 002";
            }
            echo json_encode($json);
        }
    }

    function atendimento_comprar_itens($usuarioId = false) {
        $this->layout = false;
        $usuarioLogado = $this->obterUsuarioLogado();
        $usuarioId = !$usuarioId ? (isset($_POST['formando']) ? $_POST['formando']['id'] : false) : $usuarioId;
        if (!$usuarioId) {
            $this->Session->setFlash("Formando não encontrado", 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/formandos");
        } else {
            $this->loadModel('CheckoutItem');
            $this->loadModel('CheckoutUsuarioItem');
            $this->loadModel('ProtocoloCheckoutFormando');
            $this->ViewFormandos->id = $usuarioId;
            $formando = $this->ViewFormandos->read();
            $this->set('formando', $formando['ViewFormandos']);
            $protocolo = $this->ProtocoloCheckoutFormando->find('first', array('conditions' => array('Protocolo.usuario_id' => $formando['ViewFormandos']['id'], 'tipo' => 'checkout')));
            $this->set('protocolo',$protocolo);
            if (!$protocolo) {
                $this->Session->setFlash("Protocolo de checkout não encontrado", 'flash_erro');
                $this->redirect("/{$this->params['prefix']}/formandos");
            } elseif (isset($_POST['formando'])) {
                $this->layout = false;
                $this->autoRender = false;
                Configure::write(array('debug' => 0));
                $json = array('erro' => true);
                $pagamento = $_POST['pagamento'];
                if ($pagamento['forma_pagamento'] == 'cheque')
                    $dateTime = $this->create_date_time_from_format('d-m-Y', str_replace("/", '-', $pagamento['data_base']));
                else
                    $dateTime = false;
                $erro = true;
                $mensagens = array();
                if ($dateTime === false && $pagamento['forma_pagamento'] == 'cheque') {
                    array_push($mensagens, 'Data base digitada não é válida<br />Por favor corrija.');
                } else {
                    $referencia = array();
                    $this->CheckoutUsuarioItem->begin();
                    $this->CheckoutFormandoPagamento->begin();
                    $itens = array();
                    foreach($_POST['itens'] as $item) {
                        $itens[] = array(
                            'CheckoutUsuarioItem' => array(
                                'protocolo_id' => $protocolo['ProtocoloCheckoutFormando']['protocolo_id'],
                                'usuario_id' => $formando['ViewFormandos']['id'],
                                'atendente_id' => $usuarioLogado['Usuario']['id'],
                                'checkout_item_id' => $item['id'],
                                'quantidade' => $item['comprados'],
                                'saldo' => $item['saldo'],
                                'data_cadastro' => date('Y-m-d H:i:s'),
                                'retirou' => 1,
                                'pos_checkout' => 1
                            )
                        );
                        array_push($referencia, $item['titulo']);
                    }
                    if(!$this->CheckoutUsuarioItem->saveAll($itens))
                        array_push($mensagens, "Erro ao comprar extras");
                    $referencia = rtrim(implode(';', $referencia), ";");
                    $pagamentos = array();
                    for ($a = 0; $a < $pagamento['parcelas']; $a++) {
                        if ($pagamento['forma_pagamento'] == "cheque")
                            list($ano, $mes, $dia) = explode('-', $dateTime->format('Y-m-d'));
                        else
                            list($ano, $mes, $dia) = explode('-', date('Y-m-d'));
                        $pagamentos[] = array(
                            'CheckoutFormandoPagamento' => array(
                                'data_cadastro' => date('Y-m-d H:i:s'),
                                'pos_checkout' => 1,
                                'protocolo_id' => $protocolo['ProtocoloCheckoutFormando']['protocolo_id'],
                                'usuario_id' => $usuarioLogado['Usuario']['id'],
                                'tipo' => $pagamento['forma_pagamento'],
                                'referente' => $referencia,
                                'valor' => $pagamento['valor'],
                                'obs' => $pagamento['obs'],
                                'data_vencimento' => date('Y-m-d', mktime(0, 0, 0, $mes + $a, $dia, $ano))
                            )
                        );
                    }
                    if(!$this->CheckoutFormandoPagamento->saveAll($pagamentos))
                        array_push($mensagens, "Erro ao cadastrar pagamentos");
                    else
                        $json['pagamentos'] = $pagamentos;
                    if(!empty($mensagens)) {
                        $this->CheckoutUsuarioItem->rollback();
                        $this->CheckoutFormandoPagamento->rollback();
                        $json['mensagem'] = implode("<br />", $mensagens);
                    } else {
                        $json['erro'] = false;
                        $this->CheckoutUsuarioItem->commit();
                        $this->CheckoutFormandoPagamento->commit();
                        $mensagens = "Compra realizada com sucesso.<br />";
                        $mensagens.= "O protocolo da compra permanece o mesmo do checkout, {$protocolo['Protocolo']['protocolo']}.<br />";
                        $mensagens.= "Clique <a href='/{$this->params['prefix']}/solicitacoes/checkout/{$protocolo['Protocolo']['protocolo']}' class='submit button'>aqui";
                        $mensagens.= "</a> para mais informações";
                        $erro = false;
                    }
                    echo json_encode($json);
                }
            } else {
                $itensExtras = $this->CheckoutItem->find('all', array('conditions' => array('turma_id' => $formando['ViewFormandos']['turma_id'])));
                if (!empty($itensExtras)) {
                    $itensExtrasFormatado = array();
                    foreach ($itensExtras as $indice => $itemExtra) {
                        $itensComprados = $this->CheckoutUsuarioItem->find('first', array(
                            'conditions' => array('checkout_item_id' => $itemExtra['CheckoutItem']['id']),
                            'fields' => array('sum(CheckoutUsuarioItem.quantidade) as total')));
                        $qtdeRestante = $itemExtra['CheckoutItem']['quantidade'] - $itensComprados[0]['total'];
                        $qtdeMaximaItens = $qtdeRestante > $itemExtra['CheckoutItem']['itens_por_pessoa'] ? $itemExtra['CheckoutItem']['itens_por_pessoa'] : $qtdeRestante;
                        if ($qtdeMaximaItens > 0) {
                            $itensExtras[$indice]['CheckoutItem']['itens_por_pessoa'] = $qtdeMaximaItens;
                            $itensExtrasFormatado[] = $itensExtras[$indice]['CheckoutItem'];
                        }
                    }
                    $this->set('itensExtras', $itensExtrasFormatado);
                } else {
                    $this->set('itensExtras', false);
                }
            }
        }
    }


    // function atendimento_comprar_itens($usuarioId = false) {
    //    $this->layout = false;
    // }

    function atendimento_cancelar_campanha() {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        if (isset($_POST['campanha']) && isset($_POST['usuario'])) {
            $campanhaUsuarioId = $_POST['campanha'];
            $usuario = $_POST['usuario'];
            $this->loadModel('ViewRelatorioExtras');
            $campanhaUsuario = $this->ViewRelatorioExtras->find('first', array('conditions' => array("campanhas_usuario_id" => $campanhaUsuarioId)));
            $this->Despesa->begin();
            $this->CampanhasUsuario->begin();
            try {
                $this->Despesa->updateAll(
                        array('Despesa.status' => "'cancelada'"), array('Despesa.campanhas_usuario_id' => $campanhaUsuarioId)
                );
                $this->CampanhasUsuario->updateAll(
                        array('CampanhasUsuario.cancelada' => 1), array('CampanhasUsuario.id' => $campanhaUsuarioId)
                );
                $this->Despesa->commit();
                $this->CampanhasUsuario->commit();
                $totalReembolso = $campanhaUsuario['ViewRelatorioExtras']['valor_campanha'] - $campanhaUsuario['ViewRelatorioExtras']['valor_pago'];
                $totalReembolso = $campanhaUsuario['ViewRelatorioExtras']['valor_pago'] - $totalReembolso;
                echo json_encode(array('error' => false, 'totalCampanha' => $campanhaUsuario['ViewRelatorioExtras']['valor_campanha']));
            } catch (Exception $e) {
                $this->Despesa->rollback();
                $this->CampanhasUsuario->rollback();
                echo json_encode(array('error' => true, 'message' => 'Erro ao cancelar compra'));
            }
        } else {
            echo json_encode(array('error' => true, 'message' => 'Dados nao informados'));
        }
    }

    function atendimento_reverter_cancelamento_campanha() {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        if (isset($_POST['campanha']) && isset($_POST['usuario'])) {
            $campanhaUsuarioId = $_POST['campanha'];
            $usuario = $_POST['usuario'];
            $this->loadModel('ViewRelatorioExtras');
            $campanhaUsuario = $this->ViewRelatorioExtras->find('first', array('conditions' => array("campanhas_usuario_id" => $campanhaUsuarioId)));
            $this->Despesa->begin();
            $this->CampanhasUsuario->begin();
            try {
                $this->Despesa->recursive = 2;
                $despesas = $this->Despesa->find('all', array('conditions' => array('campanhas_usuario_id' => $campanhaUsuarioId)));
                $idDespesas = array('pagas' => array(), 'abertas' => array());
                foreach ($despesas as $despesa) {
                    $status = 'aberta';
                    if (sizeof($despesa['DespesaPagamento']) != 0){
                        if ($despesa['DespesaPagamento'][0]['Pagamento']['status'] == 'pago'){
                            $status = 'paga';
                            $idDespesas['pagas'][] = $despesa['Despesa']['id'];
                        }else{
                            $idDespesas['abertas'][] = $despesa['Despesa']['id'];
                        }
                    }else{
                        $idDespesas['abertas'][] = $despesa['Despesa']['id'];
                    }
                    $this->Despesa->updateAll(
                            array('Despesa.status' => "'$status'"), array('Despesa.id' => $despesa['Despesa']['id'])
                    );
                }
                $this->CampanhasUsuario->updateAll(
                        array('CampanhasUsuario.cancelada' => 0), array('CampanhasUsuario.id' => $campanhaUsuarioId)
                );
                $this->Despesa->commit();
                $this->CampanhasUsuario->commit();
                $totalReembolso = $campanhaUsuario['ViewRelatorioExtras']['valor_campanha'] - $campanhaUsuario['ViewRelatorioExtras']['valor_pago'];
                $totalReembolso = $campanhaUsuario['ViewRelatorioExtras']['valor_campanha'] - $totalReembolso;
                echo json_encode(array('error' => false, 'totalCampanha' => $campanhaUsuario['ViewRelatorioExtras']['valor_campanha'], 'idDespesas' => $idDespesas));
            } catch (Exception $e) {
                $this->Despesa->rollback();
                $this->CampanhasUsuario->rollback();
                echo json_encode(array('error' => true, 'message' => 'Erro ao cancelar compra'));
            }
        } else {
            echo json_encode(array('error' => true, 'message' => 'Dados nao informados'));
        }
    }
    
    function atendimento_marcar_itens_retirados() {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('erro' => true);
        if(!empty($this->data)) {
            $formando = $this->ViewFormandos->read(null, $this->data['usuario']);
            $itens = $this->data['itens'];
            if(!empty($itens)) {
                $this->loadModel('CampanhasUsuarioCampanhasExtra');
                $this->loadModel('CheckoutUsuarioItem');
                $this->CampanhasUsuarioCampanhasExtra->begin();
                $this->CheckoutUsuarioItem->begin();
                $this->FormandoProfile->begin();
                $erro = false;
                try {
                    foreach($itens as $item) {
                        if($item['tipo'] == 'contrato') {
                            if(!$this->FormandoProfile->updateAll(
                                array('FormandoProfile.retirou_itens_contrato' => 1),
                                array('FormandoProfile.usuario_id' => $this->data['usuario'])
                            ))
                                $erro = true;
                        } elseif($item['tipo'] == 'checkout') {
                            if(!$this->CheckoutUsuarioItem->updateAll(
                                array('CheckoutUsuarioItem.retirou' => 1),
                                array('CheckoutUsuarioItem.id' => $item['id'])
                            ))
                                $erro = true;
                        } elseif($item['tipo'] == 'campanha') {
                            if(!$this->CampanhasUsuarioCampanhasExtra->updateAll(
                                array('CampanhasUsuarioCampanhasExtra.retirou' => 1),
                                array('CampanhasUsuarioCampanhasExtra.id' => $item['id'])
                            ))
                                $erro = true;
                        }
                    }
                } catch (Exception $ex) {
                    $erro = true;
                }
                if($erro) {
                    $this->CampanhasUsuarioCampanhasExtra->rollback();
                    $this->CheckoutUsuarioItem->rollback();
                    $this->FormandoProfile->rollback();
                    $json['mensagem'] = 'Erro ao marcar itens retirados';
                } else {
                    $json['itens'] = $itens;
                    $json['erro'] = false;
                    $this->CampanhasUsuarioCampanhasExtra->commit();
                    $this->CheckoutUsuarioItem->commit();
                    $this->FormandoProfile->commit();
                }
            }
        } else {
            $json['mensagem'] = 'Erro ao buscar os itens';
        }
        echo json_encode($json);
    }

    private function retiraPontoEVirgula($str) {
        return str_replace(";", ".", $str);
    }

    private function formataStringPagamento($pagamento) {
        $string = "";
        foreach ($pagamento as $campo => $valor)
            $string .= "$campo=$valor;";
        return substr($string, 0, -1);
    }

    private function formata_nome($nome) {
        $nome = mb_strtolower($nome, "UTF-8");
        $nome = preg_replace("/[,\.\-\/()]/", "", $nome);
        $nome = trim(mb_convert_case($nome, MB_CASE_TITLE, "UTF-8"));
        return $nome;
    }

    function validarData($data) {
        return $data == date('Y-m-d', strtotime($data));
    }
    
    private function calcularQuantidadeConvites(&$item) {
        if(preg_match('/(convite)/i',$item['titulo']))
            $item['quantidade_convites'] = 1;
        if(preg_match('/(mesa|pacote)/i',$item['titulo']))
            $item['quantidade_mesas'] = 1;
        if(preg_match('/(combo|pacote)/i',$item['titulo']))
            $item['quantidade_convites'] = 10;
        if(preg_match("/(?P<quantidade>\d{1,2}) (mesa+)/i",$item['titulo'],$m))
            $item['quantidade_mesas'] = $m['quantidade'];
        if(preg_match("/(?P<quantidade>\d{1,2}) (convite+)/i",$item['titulo'],$m))
            $item['quantidade_convites'] = $m['quantidade'];
        if(preg_match('/(meia)/i',$item['titulo']))
            $item['quantidade_convites'] = 0;
    }

}
