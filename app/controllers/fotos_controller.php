<?php

class FotosController extends Controller {
    
    var $components = array("SimpleImage");
    var $uses = array("Arquivo");
    var $info;
    
    function beforeFilter() {
        parent::beforeFilter();
        ini_set('memory_limit', "200M");
        set_time_limit(0);
    }
    
    function notFound($width,$height = false) {
        if(!$height) $height = $width;
        $this->error($width, $height);
    }
    
    function carregar($f,$width,$height = false) {
        $this->autoRender = false;
        $path = WWW_ROOT . base64_decode($f);
        $this->info = @getimagesize($path);
        if(!$height) $height = $width;
        if($this->info) {
            $new = $this->getFileWidthDimensions($path, $width, $height);
            if(file_exists($new)) {
                $this->info = @getimagesize($new);
                $this->load($new);
            } else {
                try {
                    $this->create($path,$new,$width,$height);
                    $this->info = @getimagesize($new);
                    $this->load($new);
                } catch (Exception $ex) {
                    $this->error($width, $height);
                }
            }
        } else {
            $this->error($width, $height);
        }
    }
    
    function fit($f,$width,$height = false) {
        $this->autoRender = false;
        $path = WWW_ROOT . base64_decode($f);
        $this->info = @getimagesize($path);
        if(!$height) $height = $width;
        if($this->info) {
            $new = $this->getFileWidthDimensions($path, $width, $height,'_fit');
            if(file_exists($new)) {
                $this->info = @getimagesize($new);
                $this->load($new);
            } else {
                try {
                    $this->createFit($path,$new,$width,$height);
                    $this->info = @getimagesize($new);
                    $this->load($new);
                } catch (Exception $ex) {
                    $this->error($width, $height);
                }
            }
        } else {
            $this->error($width, $height);
        }
    }
    
    function crop($f,$width,$height = false, $pos = false) {
        $this->autoRender = false;
        $path = WWW_ROOT . base64_decode($f);
        $this->info = @getimagesize($path);
        $type = !$pos ? "_crop" : "_{$pos}";
        if(!$height) $height = $width;
        if($this->info) {
            $new = $this->getFileWidthDimensions($path, $width, $height,$type);
            if(file_exists($new)) {
                $this->info = @getimagesize($new);
                $this->load($new);
            } else {
                try {
                    $this->createCrop($path,$new,$width,$height,$pos,$type);
                    $this->info = @getimagesize($new);
                    $this->load($new);
                } catch (Exception $ex) {
                    $this->error($width, $height);
                }
            }
        } else {
            $this->error($width, $height);
        }
    }
    
    private function getFileWidthDimensions($path,$width,$height,$mode = '') {
        $arquivo = $this->Arquivo->obterNomeDoArquivo($path);
        $ext = $this->Arquivo->obterExtensaoDoArquivo($arquivo);
        $tamanho = "_{$width}_{$height}{$mode}";
        $nome = str_replace("{$ext}", "{$tamanho}{$ext}", $arquivo);
        return str_replace($arquivo,$nome,$path);
    }
    
    private function load($arquivo) {
        $url = Router::url('/',true) . str_replace(WWW_ROOT,"",$arquivo);
        header("Location: {$url}");
    }
    
    private function create($src,$dst,$width,$height) {
        $this->SimpleImage->load($src);
        $bigger = $width > $height ? $width : $height;
        if($this->SimpleImage->get_orientation() == 'portrait') {
            $this->SimpleImage->fit_to_width($bigger);
        } else {
            $this->SimpleImage->fit_to_height($bigger);
        }
        $this->SimpleImage->save($dst);
    }
    
    private function createFit($src,$dst,$width,$height) {
        $this->SimpleImage->load($src);
        if($this->SimpleImage->get_orientation() == 'portrait') {
            $this->SimpleImage->fit_to_height($height);
        } else {
            $this->SimpleImage->fit_to_width($width);
        }
        $this->SimpleImage->save($dst);
    }
    
    private function createCrop($src,$dst,$width,$height,$pos = false) {
        $this->SimpleImage->load($src);
        $bigger = $width > $height ? $width : $height;
        if($this->SimpleImage->get_orientation() == 'portrait') {
            $this->SimpleImage->fit_to_width($bigger);
        } else {
            $this->SimpleImage->fit_to_height($bigger);
        }
        if(!$pos) {
            $x1 = 0;
            $y1 = 0;
        } else {
            $x1 = ($this->SimpleImage->get_width() - $width) / 2;
            $y1 = ($this->SimpleImage->get_height() - $height) / 2;
        }
        $this->SimpleImage->crop($x1,$y1,$width+$x1,$height+$y1);
        $this->SimpleImage->save($dst);
    }
    
    private function error($width,$height) {
        $path = WWW_ROOT . "img/error/{$width}_{$height}.png";
        $this->info = @getimagesize($path);
        if(!$this->info) {
            $fonte = $width < $height ? $width : $height;
            try {
                $this->SimpleImage->create($width, $height, "#F2F2F2");
                $this->SimpleImage->text('?', WWW_ROOT.'fonts/Arial Black.ttf', $fonte/2, '#A3A3A3', 'center', 0, 0);
                $this->SimpleImage->save($path);
                $this->info = $this->SimpleImage->get_original_info();
                $this->load($path);
            } catch (Exception $ex) {
                header("HTTP/1.0 404 Not Found");
                exit();
            }
        } else {
            $this->load($path);
        }
    }
    
}