<?php

class UsuariosController extends AppController {

    var $name = 'Usuarios';
    // Padrão de paginação
    var $paginate = array(
        'limit' => 25,
        'order' => array(
            'Usuario.id' => 'desc'
        )
    );
    var $nomeDoTemplateSidebar = 'usuarios';
    var $uses = array('UsuarioConta','UsuarioAcesso');
    var $components = array('Email');
    var $horas_validade_chave_troca_senha = 48;
    var $app = array('login','editar_dados','verificar_email',
        'adicionar_dispositivo','desativar_dispositivo','foto_perfil');

    function beforeFilter() {
        $this->Auth->allow('recuperar_senha', 'autenticacao_por_cpf', 'trocar_senha','login',
                'login_antigo','login_site','login_facebook','verificar_facebook',
                'conectar_facebook','entrar_deslogado','cancelar_envio_email',
                'confirmar_envio_email','cadastrar','funcionario');
        parent::beforeFilter();
        $this->Auth->autoRedirect = false;
    }
    
    function app_foto_perfil($usuarioId) {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('foto_perfil' => '');
        if($usuarioId) {
            $this->Usuario->unbindModelAll();
            $usuario = $this->Usuario->read(null,$usuarioId);
            if($usuario) {
                if(!empty($usuario['Usuario']['diretorio_foto_perfil'])) {
                    App::import('Component', 'FotoPerfil');
                    $this->FotoPerfil = new FotoPerfilComponent();
                    $arquivos = $this->FotoPerfil->obterArquivos($usuario['Usuario']['diretorio_foto_perfil']);
                    if(!empty($arquivos)) {
                        $menor = min(array_keys($arquivos));
                        $json['foto_perfil'] = $arquivos[$menor];
                    }
                }
            }
        }
        echo json_encode($json);
    }
    
    function app_desativar_dispositivo() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('erro' => true);
        if(!empty($this->data)) {
            $this->loadModel('UsuarioDispositivo');
            $data['UsuarioDispositivo'] = $this->data;
            $dispositivo = $this->UsuarioDispositivo->find('first',array(
                'conditions' => $data['UsuarioDispositivo']
            ));
            if($dispositivo) {
                $json['erro'] = !$this->UsuarioDispositivo->updateAll(
                    array('UsuarioDispositivo.ativo' => 0),
                    $data['UsuarioDispositivo']
                );
            }
        }
        echo json_encode($json);
    }
    
    function app_adicionar_dispositivo() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('erro' => true);
        if(!empty($this->data)) {
            $this->loadModel('UsuarioDispositivo');
            $data['UsuarioDispositivo'] = $this->data;
            $dispositivo = $this->UsuarioDispositivo->find('first',array(
                'conditions' => $data['UsuarioDispositivo']
            ));
            if(!$dispositivo) {
                if($this->UsuarioDispositivo->save($data)) {
                    $acesso = array(
                        'UsuarioAcesso' => array(
                            'usuario_id' => $data['UsuarioDispositivo']['usuario_id'],
                            'data_login' => date('Y-m-d H:i:s'),
                            'origem' => $data['UsuarioDispositivo']['tipo']
                        )
                    );
                    $this->UsuarioAcesso->save($acesso);
                    $json['erro'] = false;
                }
            } else {
                $acesso = array(
                    'UsuarioAcesso' => array(
                        'usuario_id' => $dispositivo['UsuarioDispositivo']['usuario_id'],
                        'data_login' => date('Y-m-d H:i:s'),
                        'origem' => $dispositivo['UsuarioDispositivo']['tipo']
                    )
                );
                $this->UsuarioAcesso->save($acesso);
                if($dispositivo['UsuarioDispositivo']['ativo'] == 0) {
                    $dispositivo['UsuarioDispositivo']['ativo'] = 1;
                    $json['erro'] = !$this->UsuarioDispositivo->save($dispositivo);
                } else {
                    $json['erro'] = false;
                }
            }
        }
        echo json_encode($json);
    }
    
    function app_verificar_email() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('erro' => true);
        if(isset($this->data['email'])) {
            $email = $this->Usuario->find('count', array(
                'conditions' => array(
                    'Usuario.email' => $this->data['email']
                )
            ));
            $json['erro'] = $email > 0;
        }
        echo json_encode($json);
    }
    
    function rh_alterar_ramal(){
        $this->layout = false;
        if(!empty($this->data)){
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $this->Usuario->id = $this->data['id'];
            $ramal = $this->data['ramal'];
            $verificaRamal = $this->Usuario->find('count', array('conditions' => array('Usuario.ramal' => $ramal)));
            if($verificaRamal > 0){
                echo json_encode(array('error' => 1));
            }else{
                $this->Usuario->saveField('ramal', $ramal);
                echo json_encode(array('error' => 0));
            }
        }
    }

    private function _listar(){
        $this->layout = false;
        $usuario = $this->Session->read('Usuario');
        if (!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
                $this->Session->write("filtros.{$usuario['Usuario']['grupo']}.funcionarios", $this->data['Usuario']);
        } else {
            $this->Usuario->unbindModelAll();
            $this->Usuario->bindModel(array(
                'hasOne' => array('FormandoProfile')
            ), false);
            $options['order'] = array('Usuario.id' => 'asc');
            $options['conditions'] = array(
                "grupo <> 'formando'",
                "grupo <> 'comissao'",
            );
            $filtro = $this->Session->read("filtros.{$usuario['Usuario']['grupo']}.funcionarios");
            if($filtro) {
                $this->data['Usuario'] = $filtro;
                foreach ($filtro as $chave => $valor)
                    $options['conditions']['lower(nome) LIKE '] = "%".strtolower($valor)."%";
            }
            $options['limit'] = 50;
            $this->paginate['Usuario'] = $options;
            $funcionarios = $this->paginate('Usuario');
            $this->set('usuario', $usuario);
            $this->set('funcionarios', $funcionarios);
            $this->render('_listar');
        }
    }
    
    function comercial_listar(){
        $this->_listar();
    }
    
    function criacao_listar(){
        $this->_listar();
    }
    
    function foto_listar(){
        $this->_listar();
    }
    
    function planejamento_listar(){
        $this->_listar();
    }
    
    function producao_listar(){
        $this->_listar();
    }
    
    function rh_listar(){
        $this->_listar();
    }
    
    function video_listar(){
        $this->_listar();
    }
    
    function funcionario(){
        $this->layout = 'metro/externo';
        $this->Turma->id = 13;
        $turma = $this->Turma->read();
        
        if (!empty($this->data['Usuario'])) {
            
            unset($this->FormandoProfile->validate);
            $this->data['FormandoProfile']['pertence_comissao'] = 0;
            $this->data['FormandoProfile']['end_cep'] = 00000-000;
            $this->data['FormandoProfile']['realizou_checkout'] = 0;
            $dataNascimento = str_replace('/', '-', $this->data['FormandoProfile']['data_nascimento']);
            $dateTime = $this->create_date_time_from_format('d-m-Y', $dataNascimento);
            $this->data['FormandoProfile']['data_nascimento'] = date_format($dateTime, 'Y-m-d');
            
            if ($this->data['Usuario']['foto_src'] != '') {
                $this->loadModel('Arquivo');
                $dir = 'upload/foto_perfil/';
                $path = WWW_ROOT . $dir;
                $nome_foto = 'u' . Security::hash(microtime(true) * 1000, 'sha1', true);
                $arquivo = $path . $nome_foto;
                if ($this->Arquivo->saveBase64File($this->data['Usuario']['foto_src'], $arquivo)){
                    $this->data['Usuario']['diretorio_foto_perfil'] = $dir . $nome_foto;
                }
            }
            if ($this->Usuario->saveAll($this->data)) {
                $this->Session->setFlash(__('Cadastro efetuado com sucesso.', true), 'metro/flash/success');
                $this->render("obrigado");
            } else {
                $this->Session->setFlash('Ocorreu um erro ao efetuar o cadastro.', 'metro/flash/error');
            }
    
        }
        $this->set('tiposgrupos', $this->Usuario->grupos);
        $this->set('turma', $turma);
    }
    
    
    function obrigado() {

    }
    
    function cancelar_envio_email($usuarioId) {
        $this->layout = 'metro/deslogado';
        $this->Usuario->unbindModelAll();
        $usuario = $this->Usuario->read(null,base64_decode($usuarioId));
        $this->set('usuario',$usuario);
        $this->set('usuarioId',$usuarioId);
        if($usuario) {
            $this->loadModel('ViewFormandos');
            $formando = $this->ViewFormandos->read(null,base64_decode($usuarioId));
            if($formando) {
                $this->Turma->unbindModelAll();
                $turma = $this->Turma->read(null,$formando['ViewFormandos']['turma_id']);
                $this->set('turma',$turma);
            } else {
                $this->set('turma',false);
            }
            if($usuario['Usuario']['autoriza_emails'] == 0) {
                $this->Session->setFlash('Usuario não recebera mensagens do sistema', 'metro/flash/alert');
                $this->render('confirmar_envio_email');
            }
        } else {
            $this->Session->setFlash('Usuario não encontrado', 'metro/flash/error');
            $this->render('confirmar_envio_email');
        }
    }
    
    function confirmar_envio_email($usuarioId,$confirmacao) {
        $this->layout = 'metro/deslogado';
        $this->Usuario->unbindModelAll();
        $usuario = $this->Usuario->read(null,base64_decode($usuarioId));
        $this->set('usuario',$usuario);
        $this->set('usuarioId',$usuarioId);
        if($usuario) {
            $this->loadModel('ViewFormandos');
            $formando = $this->ViewFormandos->read(null,base64_decode($usuarioId));
            if($formando) {
                $this->Turma->unbindModelAll();
                $turma = $this->Turma->read(null,$formando['ViewFormandos']['turma_id']);
                $this->set('turma',$turma);
            } else {
                $this->set('turma',false);
            }
            if($usuario['Usuario']['autoriza_emails'] != $confirmacao) {
                $usuario['Usuario']['autoriza_emails'] = $confirmacao;
                if(!$this->Usuario->save($usuario, array('validate' => false))) {
                    $mensagem = array(
                        'Ocorreu um erro ao atualizar seu perfil',
                        'Por favor entre em contato conosco',
                        'tel: (11) 5594-3043'
                    );
                    $this->Session->setFlash($mensagem, 'metro/flash/error');
                } elseif($confirmacao == 1) {
                    $mensagem = array(
                        'Perfil atualizado com sucesso',
                        'Obrigado por confirmar o recebimento de nossos emails',
                        'É importanta para mantê-lo atualizado'
                    );
                    $this->Session->setFlash($mensagem, 'metro/flash/info');
                } else {
                    $mensagem = array(
                        'Perfil atualizado com sucesso',
                        'Você não receberá mensagens do sistema a partir de agora'
                    );
                    $this->Session->setFlash($mensagem, 'metro/flash/success');
                }
            } elseif($confirmacao == 1) {
                $this->Session->setFlash('Perfil configurado para receber mensagens do sistema',
                    'metro/flash/alert');
            } else {
                $this->Session->setFlash('Perfil configurado para não receber mensagens do sistema',
                    'metro/flash/alert');
            }
        } else {
            $this->Session->setFlash('Usuario não encontrado', 'metro/flash/error');
        }
    }
    
    function buscar_usuarios() {
        $this->autoLayout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $this->Usuario->recursive = -1;
        $q = strtolower($_POST['query']);
        $usuarios = $this->Usuario->find('all',array('conditions' => array("lower(Usuario.nome) like('%$q%')")));
        echo json_encode(array('usuarios' => $usuarios));
    }
    
    function entrar_deslogado($usuarioId, $url = '', $interno = 0) {
        $this->autoRender = false;
        $this->Usuario->recursive = 1;
        $usuario = $this->Usuario->read(null,$usuarioId);
        if($usuario) {
            if($usuario['Usuario']['ativo'] == 0) {
                $this->Session->setFlash('Usuario inativo');
                $this->redirect("/usuarios/login");
            }
            $turma = false;
            if(isset($usuario['Turma'][0]))
                $turma['Turma'] = $usuario['Turma'][0];
			
            if($turma) {
                if($turma['Turma']['status'] == 'descartada') {
                    $this->Session->setFlash('Turma foi descartada');
                    $this->redirect("/usuarios/logout");
                }
            } else {
                $this->Session->setFlash('Turma não encontrada');
                $this->redirect("/usuarios/logout");
            }
            $this->Auth->login($usuario['Usuario']['id']);
            $this->Session->write('Usuario',$this->Auth->user());
            $this->setarTurmaLogada($turma['Turma']['id']);

            if(!empty($url) && $interno == 0)
                $this->redirect('/'.base64_decode($url));
            elseif(!empty($url) && $interno == 1) {
                $this->Session->write('redirecionar',base64_decode($url));
            }
            $this->redirect("/{$usuario['Usuario']['grupo']}");
        } else {
            $this->Session->setFlash('Usuario nao encontrado');
            $this->redirect("/usuarios/login");
        }
    }
    
    function _alterar_senha($usuarioId = false) {
        if($this->params['isAjax'] === true)
            $this->layout = false;
        else
            $this->layout = 'metro/default';
        if (!empty($this->data['Usuario'])) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $usuario = $this->Usuario->findById($this->data['Usuario']['id']);
            $this->Usuario->id = $usuario['Usuario']['id'];
            $this->Usuario->saveField('senha', Security::hash($this->data['Usuario']['senha'], 'sha1', true));
            $this->Usuario->saveField('trocar_senha', 0);
            if($this->Auth->user('trocar_senha') == 1) {
                $this->Auth->login($this->data['Usuario']['id']);
            }
            if(isset($this->data['Usuario']['exibirFlash']))
                $this->Session->setFlash("Senha alterada com sucesso", 'metro/flash/success');
            echo json_encode(array('error' => 0));
        } else {
            if($usuarioId)
                $usuario = $this->Usuario->read(null,$usuarioId);
            else
                $usuario = $this->Session->read('Usuario');
            $this->data['Usuario']['id'] = $usuario['Usuario']['id'];
            $this->set('usuario',$usuario);
            $this->render('_alterar_senha');
        }
    }
    
    function atendimento_alterar_senha() {
        $this->_alterar_senha();
    }
    
    function comissao_alterar_senha() {
        $this->_alterar_senha();
    }
    
    function comercial_alterar_senha() {
        $this->_alterar_senha();
    }
     
    function planejamento_alterar_senha() {
        $this->_alterar_senha();
    }
    
    function criacao_alterar_senha() {
        $this->_alterar_senha();
    }
    
    function foto_alterar_senha() {
        $this->_alterar_senha();
    }
    
    function video_alterar_senha() {
        $this->_alterar_senha();
    }
    
    function rh_alterar_senha(){
        $this->_alterar_senha();
    }
    
    function formando_alterar_senha() {
        $this->_alterar_senha();
    }
    
    function super_alterar_senha() {
        $this->_alterar_senha();
    }
    
    function super_alterar_senha_usuario($usuarioId = false) {
        if($this->params['isAjax'] === true)
            $this->layout = false;
        else
            $this->layout = 'metro/default';
        if (!empty($this->data['Usuario'])) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $this->Usuario->id = $this->data['Usuario']['id'];
            $this->Usuario->saveField('senha', Security::hash($this->data['Usuario']['senha'], 'sha1', true));
            $this->Usuario->saveField('trocar_senha', 0);
            if($this->Auth->user('trocar_senha') == 1) {
                $this->Auth->login($this->data['Usuario']['id']);
            }
            if(isset($this->data['Usuario']['exibirFlash']))
                $this->Session->setFlash("Senha alterada com sucesso", 'metro/flash/success');
            echo json_encode(array('error' => 0));
        }else{
            $usuario = $this->Usuario->findById($usuarioId);
            $this->data['Usuario']['id'] = $usuario['Usuario']['id'];
            $this->set('usuario',$usuario);
            $this->render('super_alterar_senha_usuario');
        }
    }
    
    function atendimento_alterar_senha_usuario($usuarioId) {
        $this->super_alterar_senha_usuario($usuarioId);
    }

    function _alterar_imagem() {
        if (!$this->RequestHandler->isAjax())
            $this->redirect("/{$this->params['prefix']}/");
        $this->aumentar_memoria("128M");
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $imagem = base64_decode($_POST['imagem']);
        $usuario = $this->Usuario->read(null, $_POST['usuario']);
        $nomeArquivo = "u" . Security::hash(microtime(true) * 1000, 'sha1', true);
        $dir = "upload/foto_perfil/{$nomeArquivo}.{$_POST['extensao']}";
        $path = APP . "webroot/$dir";
        $url = $this->webroot . $dir;
        if (!empty($usuario['Usuario']['diretorio_foto_perfil']))
            unlink(APP . "webroot/{$usuario['Usuario']['diretorio_foto_perfil']}");
        if (file_put_contents($path, $imagem)) {
            $this->Usuario->id = $_POST['usuario'];
            if (!$this->Usuario->saveField('diretorio_foto_perfil', $dir)) {
                $json = array('error' => true, 'mensagem' => 'Erro ao salvar imagem');
            } else {
                App::import('Component', 'FotoPerfil');
                $this->FotoPerfil = new FotoPerfilComponent();
                $this->FotoPerfil->criarArquivos($path,$nomeArquivo,".jpeg");
                $usuario = $this->Session->read('Usuario');
                $usuario['Usuario']['diretorio_foto_perfil'] = $dir;
                $this->Session->write('Usuario', $usuario);
                $json = array('error' => false, 'url' => $url, 'path' => $path);
            }
        } else {
            $json = array('error' => true, 'mensagem' => 'Erro ao salvar imagem');
        }
        echo json_encode($json);
    }

    function formando_alterar_imagem() {
        $this->_alterar_imagem();
    }
    
    function comissao_alterar_imagem() {
        $this->_alterar_imagem();
    }
    
    function super_entrar($usuarioId) {
        if($this->Auth->user('grupo') == 'super') {
            $usuario = $this->Usuario->read(null,$usuarioId);
            if($usuario) {
                $this->Session->write('UsuarioLogado',$this->Auth->user());
                $this->Session->write('Usuario',$usuario);
            } else {
                $this->Session->setFlash("Usuario nao encontrado", 'metro/flash/error');
            }
        }
        $usuarioLogado = $this->obterUsuarioLogado();
        $this->redirect("/{$usuarioLogado['Usuario']['grupo']}");
    }

    function atendimento_entrar($usuarioId) {
        if($this->Auth->user('grupo') == 'atendimento') {
            $usuario = $this->Usuario->read(null,$usuarioId);
            if($usuario) { 
                $this->Session->write('UsuarioLogado',$this->Auth->user());
                $this->Session->write('Usuario',$usuario);
            } else {
                $this->Session->setFlash("Usuario nao encontrado", 'metro/flash/error');
            }
        }
        $usuarioLogado = $this->obterUsuarioLogado();
        $this->redirect("/{$usuarioLogado['Usuario']['grupo']}");
    }
    
    function planejamento_entrar($usuarioId) {
        if($this->Auth->user('grupo') == 'planejamento') {
            $usuario = $this->Usuario->read(null,$usuarioId);
            if($usuario) {
                $this->Session->write('UsuarioLogado',$this->Auth->user());
                $this->Session->write('Usuario',$usuario);
            } else {
                $this->Session->setFlash("Usuario nao encontrado", 'metro/flash/error');
            }
        }
        $usuarioLogado = $this->obterUsuarioLogado();
        $this->redirect("/{$usuarioLogado['Usuario']['grupo']}");
    }
    
    function sair() {
        $perfis = array('super', 'atendimento','planejamento');
        if(in_array($this->Auth->user('grupo'), $perfis)) {
            if($this->Session->check('UsuarioLogado')) {
                if($this->Session->check('turma'))
                    $this->Session->delete('turma');
                $this->Session->delete('UsuarioLogado');
                $usuario = $this->Session->read('UsuarioLogado');
            } else {
                $usuario = $this->Auth->user();
            }
            $this->Session->write('Usuario',$usuario);
        }
        $usuarioLogado = $this->obterUsuarioLogado();
        $this->redirect("/{$usuarioLogado['Usuario']['grupo']}");
    }

    function login() {
        if($this->Auth->user()) {
            $this->Session->write('Usuario',$this->Auth->user());
            $this->redirect("/{$this->Auth->user('grupo')}");
        } elseif($this->siteDeTurma && empty($this->data))
            $this->redirect('/turmas/site');
        else
            $this->layout = 'metro/login';
        $this->Session->delete('exibirSwitchComissaoFormando');
        $this->Session->delete('mudouGrupo');
    }
    
    function login_site() {
        $this->layout = false;
    }
    
    function login_antigo() {
        $this->layout = 'metro/login';
        if($this->Auth->user())
            $this->redirect("/{$this->Auth->user('grupo')}");
        if (!empty($this->data['Usuario'])) {
            if(!empty($this->data['Usuario']['login_antigo']) && !empty($this->data['Usuario']['senha_antiga'])){
            $this->loadModel('FormandoProfile');
            $this->FormandoProfile->recursive = 0;
            $formando = $this->FormandoProfile->find('all', array('conditions' => array('FormandoProfile.login_antigo' => $this->data['Usuario']['login_antigo'], 'FormandoProfile.senha_antiga' => $this->data['Usuario']['senha_antiga'])));
            if ($formando != null) {
                if (count($formando) == 1) {
                    $turma = $this->obterTurmaLogada();
                    if(!$turma) {
                        $this->Usuario->recursive = 1;
                        $usuario = $this->Usuario->read(null,$formando[0]['FormandoProfile']['usuario_id']);
                        if(isset($usuario['Turma'][0]))
                            $turma['Turma'] = $usuario['Turma'][0];
                    }
                    if($turma) {
                        if($turma['Turma']['status'] == 'descartada') {
                            $this->Session->setFlash('Turma foi descartada');
                            $this->redirect("/usuarios/logout");
                        }
                    } else {
                        $this->Session->setFlash('Turma não encontrada');
                        $this->redirect("/usuarios/logout");
                    }
                    $this->Auth->login($formando[0]['FormandoProfile']['usuario_id']);
                    $this->Session->write('Usuario',$this->Auth->user());
                    $contagem = $formando[0]['FormandoProfile']['email_enviado'];
                    if ($contagem < 3 && false) {
                        $this->enviarEmailInformandoNovaSenha($this->data['Usuario']['senha_antiga'], $formando[0]['Usuario']);
                        $contagem = $contagem + 1;
                        $query = "UPDATE formandos_profile SET email_enviado = $contagem WHERE usuario_id = " . $formando[0]['FormandoProfile']['usuario_id'] . ";";
                        $this->FormandoProfile->query($query);
                    }
                    $this->redirect('/formando');
                }
                else
                    $this->Session->setFlash(__('Login com dados duplicado. Entre em contato com a RK Formaturas.', true), 'flash_erro');
            }
            else
                $this->Session->setFlash(__('Login ou senha invalidos. Tente novamente.', true), 'flash_erro');
        }else
            $this->Session->setFlash(__('Login ou senha invalidos. Tente novamente.', true), 'flash_erro');
        }
    }
    
    function verificar_facebook() {
        $this->layout = false;
        if(isset($this->data['facebook'])) {
            $facebook = $this->data['facebook'];
            $this->set('facebook',$facebook);
            $conta = $this->UsuarioConta->find('first',array(
                'conditions' => array(
                    'uid' => $facebook['id'],
                    'tipo' => 'facebook'
                )
            ));
            $this->set('conta',$conta);
            if($conta) {
                $logado = $this->Auth->login($conta['Usuario']['id']);
                $this->set('logado',$logado);
                if($logado)
                    $this->Session->write('Usuario',$this->Auth->user());
            } else {
                $this->Usuario->recursive = 1;
                $nomes = explode(' ',strtolower($facebook['name']));
                foreach($nomes as $i => $n)
                    if(strlen($n) < 3) unset($nomes[$i]);
                $usuario = $this->Usuario->find('first',array(
                    'conditions_verificar_nomes' => array(
                        'OR' => array(
                            'Usuario.email' => $facebook['email'],
                            "lower(Usuario.nome) like('%".implode('%',$nomes)."%')",
                            "lower(Usuario.nome) like('%".strtolower($facebook['first_name'])."%".
                                strtolower($facebook['last_name'])."%')"
                        ),
                        'Usuario.ativo' => 1
                    ),
                    'conditions' => array(
                        'Usuario.email' => $facebook['email'],
                        'Usuario.ativo' => 1
                    )
                ));
                $this->set('usuario',$usuario);
            }
        }
    }
    
    function conectar_facebook() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('erro' => true,'mensagem' => array());
        if(isset($this->data['usuario']) && isset($this->data['facebook'])) {
            $facebook = $this->data['facebook'];
            $usuario = $this->Usuario->read(null,$this->data['usuario']);
            $conta = $this->UsuarioConta->find('first',array(
                'conditions' => array(
                    'uid' => $facebook['id'],
                    'tipo' => 'facebook'
                )
            ));
            if(!$usuario) {
                $json['mensagem'][] = "Usuario n&atilde;o encontrado";
            } elseif($conta) {
                if($conta['UsuarioConta']['usuario_id'] != $usuario['Usuario']['id']) {
                    $json['mensagem'][] = "Esta conta foi cadastrada por outro formando";
                } elseif($conta['UsuarioConta']['ativo'] == 0) {
                    $json['mensagem'][] = "Esta conta j&aacute; foi vinculada ao seu login. Ative ela para poder us&aacute;-la";
                } else {
                    $json['mensagem'][] = "Esta conta j&aacute; foi vinculada ao seu login";
                }
            } else {
                $usuarioConta['UsuarioConta'] = array(
                    'usuario_id' => $usuario['Usuario']['id'],
                    'tipo' => 'facebook',
                    'uid' => $facebook['id'],
                    'email' => $facebook['email'],
                    'data_cadastro' => date('Y-m-d H:i:s')
                );
                if(!$this->UsuarioConta->save($usuarioConta)) {
                    $json['mensagem'][] = "Erro ao enviar dados. Tente novamente mais tarde";
                } else {
                    if(empty($usuario['Usuario']['diretorio_foto_perfil']) ||
                        !file_exists(APP . "webroot/{$usuario['Usuario']['diretorio_foto_perfil']}")) {
                        $url = "https://graph.facebook.com/{$facebook['id']}/picture?width=600&height=600";
                        $img = file_get_contents($url);
                        if($img) {
                            $dir = "upload/foto_perfil/u" . Security::hash(microtime(true) * 1000, 'sha1', true) . ".jpeg";
                            if(file_put_contents(APP."webroot/$dir", $img)) {
                                $usuario['Usuario']['diretorio_foto_perfil'] = $dir;
                                $this->Usuario->id = $usuario['Usuario']['id'];
                                $this->Usuario->saveField('diretorio_foto_perfil',$usuario['Usuario']['diretorio_foto_perfil']);
                            }
                        }
                    }
                    if(!$this->Auth->user()) {
                        if(!$this->Auth->login($usuario['Usuario']['id']))
                            $json['mensagem'][] = "Erro ao completar login. Tente novamente";
                        else {
                            $json['erro'] = false;
                            $this->Session->write('Usuario',$usuario);
                        }
                    } else
                        $json['erro'] = false;
                }
            }
        }
        echo json_encode($json);
    }
    
    function app_editar_dados() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('erro' => true);
        if(!empty($this->data)) {
            unset($this->Usuario->FormandoProfile->validate['forma_pagamento']);
            unset($this->Usuario->FormandoProfile->validate['parcelas']);
            $this->data['Usuario']['data_ultima_alteracao'] = date('Y-m-d H:i:s', strtotime('now'));
            if($this->Usuario->saveAll($this->data)) {
                $usuario = $this->Usuario->read(null,$this->data['Usuario']['id']);
                $turma = $this->Turma->read(null,$usuario['ViewFormandos']['turma_id']);
                $usuario['Turma'] = $turma['Turma'];
                if(isset($this->data['Usuario']['foto_perfil'])) {
                    $imagem = base64_decode($this->data['Usuario']['foto_perfil']);
                    $nomeArquivo = "u" . Security::hash(microtime(true) * 1000, 'sha1', true);
                    $dir = "upload/foto_perfil/{$nomeArquivo}.jpeg";
                    $path = APP . "webroot/$dir";
                    if (!empty($usuario['Usuario']['diretorio_foto_perfil']))
                        unlink(APP . "webroot/{$usuario['Usuario']['diretorio_foto_perfil']}");
                    if(file_put_contents($path, $imagem)) {
                        $this->Usuario->id = $this->data['Usuario']['id'];
                        if (!$this->Usuario->saveField('diretorio_foto_perfil', $dir)) {
                            $json['mensagem'] = 'Erro ao salvar imagem na base de dados';
                        } else {
                            App::import('Component', 'FotoPerfil');
                            $this->FotoPerfil = new FotoPerfilComponent();
                            $this->FotoPerfil->criarArquivos($path,$nomeArquivo,".jpeg");
                            $usuario['Usuario']['diretorio_foto_perfil'] = $dir;
                            $json['erro'] = false;
                        }
                    } else {
                        $json['mensagem'] = 'Erro ao salvar imagem no servidor';
                    }
                } else {
                    $json['erro'] = false;
                }
                $json['usuario'] = $usuario;
            } else {
                $json['mensagem'] = array();
                if (isset($this->Usuario->validationErrors['Usuario']))
                    $json['mensagem'] = array_merge($json['mensagem'],array_values($this->Usuario->validationErrors['Usuario']));
                if (isset($this->Usuario->validationErrors['FormandoProfile']))
                    $json['mensagem'] = array_merge($json['mensagem'],array_values($this->Usuario->validationErrors['FormandoProfile']));
            }
            $json['data'] = $this->data;
        }
        echo json_encode($json);
    }
    
    function app_login() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('usuario' => false);
        if(!empty($this->data)) {
            $usuario = $this->Usuario->find('first',array(
                'conditions' => array(
                    'Usuario.email' => $this->data['email'],
                    'Usuario.senha' => Security::hash($this->data['senha'], 'sha1', true)
                )
            ));
            if($usuario) {
                $turma = $this->Turma->read(null,$usuario['ViewFormandos']['turma_id']);
                if(!$turma) {
                    $json['mensagem'] = "Turma não encontrada";
                } elseif($turma['Turma']['status'] == 'descartada') {
                    $json['mensagem'] = "Turma foi descartada";
                } else {
                    $usuario['Turma'] = $turma['Turma'];
                    $json['usuario'] = $usuario;
                }
            }
        }
        echo json_encode($json);
    }
    
    function comissao_editar_dados($exibir = 'pessoais') {
        $this->formando_editar_dados($exibir);
    }
    
    function comercial_editar_dados($exibir = 'pessoais',$uid){
        $this->FormandoProfile->validate = array();
        $this->formando_editar_dados($exibir,$uid);
    }

    function formando_editar_dados($exibir = 'pessoais',$uid = false) {
        $this->layout = false;
        $turma = $this->obterTurmaLogada();
        $turmaId = (isset($turma["Turma"]["id"])) ?: $turma["Turma"]["id"];
        $this->loadModel('CursoTurma');
        $this->loadModel('ViewFormandos');
        $this->loadModel('Pagamento');
        $this->loadModel('Despesa');
        $uid = $uid ? $uid : $this->Session->read('Usuario.Usuario.id');
        if (!$turma) {
            $turma = $this->ViewFormandos->find('first', array('conditions' => array('id' => $uid)));
            $turmaId = $turma['ViewFormandos']['turma_id'];
            $turma = $this->CursoTurma->findByTurmaId($turma['ViewFormandos']['id']);
            $turma = $turma['Curso']['Turma'];
        }
        $this->Usuario->id = $uid;
        $this->set('turma', $turma);
        $this->set('exibir', $exibir);
        $this->Usuario->unBindModel(array('hasMany' => array('Despesa','Cupom'),
            'hasAndBelongsToMany' => array('Turma','Campanhas')), false);
        if (!empty($this->data)) {
            $uid = !empty($this->data["Usuario"]["id"]) ? $this->data["Usuario"]["id"] : $this->Session->read('Usuario.Usuario.id');
            $this->Usuario->contain('FormandoProfile');
            $usuarioLido = $this->Usuario->findById($uid);
            $this->data['Usuario']['id'] = $uid;
            if (isset($this->data['FormandoProfile']['diretorio_foto_perfil'])) {
                if (substr_count($this->data['FormandoProfile']['diretorio_foto_perfil'], "temp") > 0) {
                    $diretorioFinalDaFotoDoProfile = str_replace('temp', 'foto_perfil', $this->data['FormandoProfile']['diretorio_foto_perfil']);
                    copy($this->data['FormandoProfile']['diretorio_foto_perfil'], $diretorioFinalDaFotoDoProfile);
                    unlink($this->data['FormandoProfile']['diretorio_foto_perfil']);
                    $this->data['FormandoProfile']['diretorio_foto_perfil'] = $diretorioFinalDaFotoDoProfile;
                }
            }

            foreach ($this->data['Usuario'] as $coluna => $valor)
                $usuarioLido['Usuario'][$coluna] = $valor;

            foreach ($this->data['FormandoProfile'] as $coluna => $valor)
                $usuarioLido['FormandoProfile'][$coluna] = $valor;

            if ($this->data['FormandoProfile']['data_nascimento'] != "")
                $usuarioLido['FormandoProfile']['data_nascimento'] = implode('-', array_reverse(
                                explode('/', $this->data['FormandoProfile']['data_nascimento'])));
            else
                $usuarioLido['FormandoProfile']['data_nascimento'] = null;

            $usuarioLido['FormandoProfile']['sexo'] = isset($this->data['FormandoProfile']['sexo']) ? 'M' : 'F';

            if($this->validateCpf($this->data['FormandoProfile']['cpf']) == 1){

                $usuarioLido['FormandoProfile']['cpf'] = str_replace(array(",", ".", "-"), "", $this->data['FormandoProfile']['cpf']);
            }else{

                $this->Session->setFlash('CPF Inválido.', 'metro/flash/error');
                exit();
            }

            // Remover formatação do CEP
            $usuarioLido['FormandoProfile']['end_cep'] = str_replace(array(",", ".", "-"), "", $this->data['FormandoProfile']['end_cep']);

            $usuarioLido['Usuario']['data_ultima_alteracao'] = date('Y-m-d H:i:s', strtotime('now'));

            if (!isset($usuarioLido['Usuario']['confirmar']))
                unset($usuarioLido['Usuario']['senha']);
            // $formaPagamento = $this->FormandoProfile->find('first', array('conditions' => array('FormandoProfile.usuario_id' => $uid)));
            // if($this->data['FormandoProfile']['forma_pagamento'] != $formaPagamento['FormandoProfile']['forma_pagamento']){
                // if($this->data['FormandoProfile']['forma_pagamento']){
                //     $usuarioLido['FormandoProfile']['forma_pagamento'] = $this->data['FormandoProfile']['forma_pagamento'];
                //     $despesaAberta = $this->Despesa->find('first', array('conditions' => array(
                //                         'Despesa.status' => 'aberta', 
                //                         'Despesa.tipo' => 'adesao', 
                //                         'Despesa.usuario_id' => $usuarioLido['FormandoProfile']['usuario_id']
                //                      )));
                //     $pagamentoAberto = $this->Pagamento->find('first', array('conditions' => array(
                //                         'Pagamento.id' => $despesaAberta['DespesaPagamento'][0]['pagamento_id']
                //                         )));
                    // if($this->data['FormandoProfile']['forma_pagamento'] == 'boleto_impresso'){
                    //     if(!empty($despesaAberta)){
                    //         $despesaAberta['Despesa']['valor'] = $despesaAberta['Despesa']['valor'] + 25.00;
                    //         $this->Despesa->id = $despesaAberta['Despesa']['id'];
                    //         $this->Despesa->saveField('valor', $despesaAberta['Despesa']['valor']);
                    //         if(!empty($despesaAberta['DespesaPagamento'])){
                    //             $pagamentoAberto['Pagamento']['valor_nominal'] = $pagamentoAberto['Pagamento']['valor_nominal'] + 25.00;
                    //             $this->Pagamento->id = $pagamentoAberto['Pagamento']['id'];
                    //             $this->Pagamento->saveField('valor_nominal', $pagamentoAberto['Pagamento']['valor_nominal'] );
                    //         }
                    //     }
                    // }else{
                    //     if(!empty($despesaAberta)){
                    //         $despesaAberta['Despesa']['valor'] = $despesaAberta['Despesa']['valor'] - 20.00;
                    //         $this->Despesa->id = $despesaAberta['Despesa']['id'];
                    //         $this->Despesa->saveField('valor', $despesaAberta['Despesa']['valor']);
                    //         if(!empty($despesaAberta['DespesaPagamento'])){
                    //             $pagamentoAberto['Pagamento']['valor_nominal'] = $pagamentoAberto['Pagamento']['valor_nominal'] - 20.00;
                    //             $this->Pagamento->id = $pagamentoAberto['Pagamento']['id'];
                    //             $this->Pagamento->saveField('valor_nominal', $pagamentoAberto['Pagamento']['valor_nominal'] );
                    //         }
                    //     }
                    // }
            //     }
            // }
            unset($this->Usuario->FormandoProfile->validate['forma_pagamento']);
            unset($this->Usuario->FormandoProfile->validate['parcelas']);
            if ($this->Usuario->saveAll($usuarioLido)) {
                $this->Session->setFlash('Dados atualizados com sucesso.', 'metro/flash/success');
            } else {
                $usuario = array();
                $formando = array();
                if (isset($this->Usuario->validationErrors['Usuario']))
                    $usuario = $this->Usuario->validationErrors['Usuario'];
                if (isset($this->Usuario->validationErrors['FormandoProfile']))
                    $formando = $this->Usuario->validationErrors['FormandoProfile'];
                echo json_encode(array_merge($usuario, $formando));
                $this->Session->setFlash(array_merge($usuario, $formando), 'metro/flash/error');
            }
            exit();
        } else {
            $this->data = $this->Usuario->read();
            $dateTime = $this->create_date_time_from_format('Y-m-d', $this->data['FormandoProfile']['data_nascimento']);
            if ($dateTime != null)
                $this->data['FormandoProfile']['data_nascimento'] = date_format($dateTime, 'd/m/Y');
            unset($this->data['Usuario']['senha']);
        }
        $this->CursoTurma->contain(array('Curso.Faculdade.Universidade'));
        $cursos_turmas = $this->CursoTurma->find('all', array('conditions' => array('turma_id' => $turmaId)));
        $select_cursos_turmas = array();

        foreach ($cursos_turmas as $curso_turma) {
            $universidade = $curso_turma['Curso']['Faculdade']['Universidade']['nome'];
            $faculdade = $curso_turma['Curso']['Faculdade']['nome'];
            $curso = $curso_turma['Curso']['nome'];
            $turno = ucfirst($curso_turma['CursoTurma']['turno']);
            if ($curso != 'Curso Geral') {
                $select_cursos_turmas[$curso_turma['CursoTurma']['id']] = $curso . ' - ' . $faculdade . ' - ' . $universidade . ' - ' . $turno;
            }
        }
        $this->set('lista_curso_turma_id', $select_cursos_turmas);

        if (!$this->data)
            $this->Session->setFlash('Usuário não existente,', 'flash_erro');
        $this->Usuario->recursive = 1;
        $usuario = $this->Usuario->read();
        $this->set('usuario', $usuario);
        $this->set('fields', $this->FormandoProfile->fields);
        $totalIGPM = array('despesas_pagas' => 0, 'despesas_abertas' => 0);
        $this->loadModel('Despesa');
        $adesoes = $this->Despesa->find('all', array(
            'conditions' => array('Despesa.usuario_id' => $usuario['Usuario']['id'],
                'Despesa.tipo' => 'adesao', "Despesa.status <> 'cancelada'")));
        foreach ($adesoes as $index => $adesao)
            if ($adesao['Despesa']['status'] == 'aberta')
                $totalIGPM['despesas_abertas'] +=!empty($adesao['Despesa']['correcao_igpm']) ? $adesao['Despesa']['correcao_igpm'] : 0;
            else
                $totalIGPM['despesas_pagas'] +=!empty($adesao['Despesa']['correcao_igpm']) ? $adesao['Despesa']['correcao_igpm'] : 0;
        $igpm = $this->Despesa->find('all', array(
            'conditions' => array('Despesa.usuario_id' => $usuario['Usuario']['id'],
                'Despesa.tipo' => 'igpm', "Despesa.status <> 'cancelada'")));
        if (!empty($igpm))
            $totalIGPM['despesas_pagas'] = $igpm[0]['Despesa']['valor'];
        $this->set('totalIGPM', array_sum($totalIGPM));
    }
    
    function atendimento_editar_dados($exibir = false, $uid = false){
        $this->FormandoProfile->validate = array();
        $this->formando_editar_dados($exibir = 'pessoais',$uid);
    }
    
    function financeiro_editar_dados($exibir = false, $uid = false){
        $this->FormandoProfile->validate = array();
        $this->formando_editar_dados($exibir = 'pessoais',$uid);
    }

    function formando_mudar_senha() {
        if (!empty($this->data['Usuario'])) {
            $usuarioLogado = $this->Session->read('Usuario');
            if ($this->data['Usuario']['senha_nova'] == $this->data['Usuario']['senha_nova_confirmar']) {
                $this->Usuario->recursive = -1;
                $usuarioDoBanco = $this->Usuario->findById($usuarioLogado['Usuario']['id']);

                if ($usuarioDoBanco['Usuario']['senha'] == Security::hash($this->data['Usuario']['senha_atual'], 'sha1', true)) {
                    if ($this->data['Usuario']['senha_nova'] != '') {
                        $this->Usuario->id = $usuarioLogado['Usuario']['id'];
                        $this->Usuario->saveField('senha', Security::hash($this->data['Usuario']['senha_nova'], 'sha1', true));
                        $this->Session->setFlash("Senha alterada com sucesso.", 'flash_sucesso');
                    }
                    else
                        $this->Session->setFlash("Senha não pode ser vazia.", 'flash_erro');
                }
                else
                    $this->Session->setFlash("Senha atual errada.", 'flash_erro');
            }
            else
                $this->Session->setFlash("Senha nova não confere com a confirmação.", 'flash_erro');
        }
    }

    function atendimento_mudar_senha() {
        $this->formando_mudar_senha();
    }

    function comissao_mudar_senha() {
        $this->formando_mudar_senha();
    }

    function comissao_mudar_grupo() {
        $usuarioLogado = $this->Session->read('Usuario');
        if ($usuarioLogado["Usuario"]["grupo"] == "comissao") {
            $this->Session->write('Usuario.Usuario.grupo', "formando");
            $this->Session->write('mudouGrupo', true);
        }

        $this->redirect("/{$usuarioLogado["Usuario"]["grupo"]}");
    }

    function formando_mudar_grupo() {
        $usuarioLogado = $this->Session->read('Usuario');
        if ($usuarioLogado["Usuario"]["grupo"] == "formando") {
            $this->Usuario->recursive = 0;
            $usuarioDoBanco = $this->Usuario->find('first', array('conditions' => array('Usuario.id' => $usuarioLogado['Usuario']['id'])));

            if ($usuarioDoBanco["Usuario"]["grupo"] == "comissao") {
                $this->Session->write('Usuario.Usuario.grupo', "comissao");
                $this->Session->delete('mudouGrupo');
            }
        }

        $this->redirect("/{$usuarioLogado["Usuario"]["grupo"]}");
    }

    function planejamento_mudar_senha() {
        $this->formando_mudar_senha();
    }

    function comercial_mudar_senha() {
        $this->formando_mudar_senha();
    }

    function super_mudar_senha() {
        $this->formando_mudar_senha();
    }

    function financeiro_mudar_senha() {
        $this->formando_mudar_senha();
    }

    private function enviarEmailInformandoNovaSenha($novaSenha, $usuario) {
        $env = Configure::read('env');
        if(!empty($env))
            if($env == 'desenvolvimento')
                return;
        $this->Email->smtpOptions = array(
            'port' => '587',
            'timeout' => '30',
            'host' => 'smtp.rkformaturas.com.br',
            'username' => 'notificacoesrk@rkformaturas.com.br',
            'password' => 'ate55943043'
        );

        $this->Email->from = 'RK Formaturas - Sistema Online <notificacoesrk@rkformaturas.com.br>';
        $this->Email->to = $usuario['nome'] . ' - ' . '<' . $usuario['email'] . '>';
        $this->Email->subject = 'Recuperação de senha';

        $mensagem =
                "Seja bem vindo ao novo Sistema Online da RK Formaturas!\n\n" .
                "O login no sistema usando os dados antigos ( nome e cpf ) será aceito até o final de maio," .
                "depois desse prazo será aceito apenas o login utilizando seu e-mail e sua senha. \n" .
                "Você pode acessar o novo sistema pelo endereço: \n" .
                Router::url('/', true) . ". \n\nÉ possível alterar sua senha através do link 'mudar senha'." .
                "\n\n Atenciosamente, \n\n" .
                "Equipe RK Formaturas";

        /* Set delivery method */
        $this->Email->delivery = 'smtp';

        /* Do not pass any args to send() */
        $emailEnviado = $this->Email->send($mensagem);

        /* Check for SMTP errors. */
        $this->set('smtp_errors', $this->Email->smtpError);
    }

    function recuperar_senha() {
        $this->layout = 'metro/login';
         // Caso o campo esteja vazio, não avançar.
        if (!empty($this->data['Usuario']['email'])) {
            // PRECISO MELHORAR BUSCA PARA TRAZER APENAS UM RESULTADO.
            $this->Usuario->recursive = -1;
            $usuario = $this->Usuario->findByEmail($this->data['Usuario']['email']);

            if ($usuario != null) {
				App::import('Component', 'Mailer');
				$email = new MailerComponent();
                $this->Usuario->id = $usuario['Usuario']['id'];

                // Gera a chave de recuperacao, e a salva no banco.
                $chave = Security::hash($this->data['Usuario']['email'] . strtotime('now'), 'sha1', true) . strtotime('now');

                if (!$this->Usuario->saveField('chave_de_recuperacao', $chave)) {
                    $this->Session->setFlash("Erro ao guardar chave de recuperação de senha.", 'metro/flash/error');
                } else {
					
                    $mensagem =
                            "Este email é referente a um pedido de recuperação de senha do site <a href='http://www.rkformaturas.com.br'>www.rkformaturas.com.br</a> <br /><br />" .
                            "Atenção! <br /><br />" .
                            "Caso você não tenha solicitado este serviço, por favor, desconsidere este email. <br /><br />" .
                            "Caso contrário, copie a chave de recuperação abaixo. <br />" .
                            $chave . "<br /><br />" .
                            "Clique no link e prossiga com o processo de recuperação de senha.<br /><br />" .
                            '<a href="' . Router::url('/', true) . 'usuarios/trocar_senha/">'. Router::url('/', true) . 'usuarios/trocar_senha/</a>';


                    $email->enviarEmail($usuario['Usuario']['email'], 'Recuperação de senha', $mensagem);//$this->Email->send($mensagem);

                }
				$this->Session->setFlash("Email enviado com sucesso.", 'metro/flash/success');
                $this->redirect('/usuarios/login');
				
            }else{
				
                $this->Session->setFlash("Email informado não cadastrado.", 'metro/flash/error');
				
			}
        }
    }

    function autenticacao_por_cpf() {
        // Desabilitar autenticacao por CPF. As pessoas de comissao se cadastram sem cpf
        //TODO: Depois que atualizar todos os cadastros, retirar isso
        $chave = $this->data['Usuario']['chave_de_recuperacao'];
        $this->redirect("/usuarios/trocar_senha/$chave");

        $this->loadModel('FormandoProfile');
        if (!empty($this->data)) {
            $chave = $this->data['Usuario']['chave_de_recuperacao'];
            $usuario = $this->Usuario->findByChaveDeRecuperacao($chave);
            $formando = $this->FormandoProfile->findByUsuarioId($usuario['Usuario']['id']);
            $cpf = ereg_replace("[^0-9]", "", $this->data['Usuario']['cpf']);
            if ($cpf == $formando['FormandoProfile']['cpf']) {
                $this->redirect("/usuarios/trocar_senha/$chave");
            } else {
                $this->Session->setFlash(__('CPF incorreto.', true), 'flash_erro');
            }
        } else {
            $chave = $this->params['pass'][0];

            //$this->redirect ('/usuarios/login');
        }

        $this->set('chave', $chave);

        $this->layout = 'login';
    }

    function trocar_senha() {
        $this->layout = 'metro/externo';
        // Verificar se a chave expirou
        $timestamp = substr($this->data['Usuario']['chave_de_recuperacao'], 40);
        $tempo_desde_geracao_da_chave = ((strtotime('now') - $timestamp ) / 3600);
        $habilitar_verificacao_de_tempo = false;
        if ($habilitar_verificacao_de_tempo && $tempo_desde_geracao_da_chave > $this->horas_validade_chave_troca_senha) {
            // Chave expirada
            $usuario = $this->Usuario->findByChaveDeRecuperacao($this->data['Usuario']['chave_de_recuperacao']);
            if ($usuario != null) {
                $this->Usuario->saveField('chave_de_recuperacao', null);
            }
            $this->Session->setFlash("Chave de troca de senha expirada.", 'metro/flash/error');
            $this->redirect('/usuarios/login');
        }else{
            $usuario = $this->Usuario->findByChaveDeRecuperacao($this->data['Usuario']['chave_de_recuperacao']);
            if ($usuario != null) {
                if ($this->data['Usuario']['senha'] != '') {
                    $this->Usuario->id = $usuario['Usuario']['id'];

                    if ($this->data['Usuario']['senha'] == $this->data['Usuario']['confirmar']) {

                        $this->data['Usuario']['senha'] = Security::hash($this->data['Usuario']['senha'], 'sha1', true);

                        $this->data['Usuario']['chave_de_recuperacao'] = null;

                        if ($this->Usuario->save($this->data, null, array('senha', 'confirmar', 'chave_de_recuperacao'))) {
                            $this->Session->setFlash("Senha alterada com sucesso.", 'metro/flash/success');
                            $this->redirect('/usuarios/login');
                        } else {
                            $this->Session->setFlash("Erro ao alterar senha.", 'metro/flash/error');
                        }
                    } else {
                        $this->Session->setFlash("As duas senhas não conferem", 'metro/flash/error');
                    }
                }
            }
        }
    }

    function logout() {
        $usuario = $this->Auth->user();
        $this->Usuario->updateAll(
            array('Usuario.ultimo_acesso' => "'".date("Y-m-d H:i:s")."'"),
            array("Usuario.id" => $usuario['Usuario']['id'])
        );
        if($this->Session->check('usuario_acesso')) {
            $acesso = $this->Session->read('usuario_acesso');
            $acesso['data_logout'] = date('Y-m-d H:i:s');
            $this->UsuarioAcesso->save($acesso);
            $this->Session->delete('usuario_acesso');
        }
        $this->logoutSession();
        $this->redirect($this->Auth->logout());
    }
    
    private function logoutSession() {
        if($this->turmaEstaLogada())
            $this->Session->delete('turma');
        if($this->Session->check('UsuarioLogado'))
            $this->Session->delete('UsuarioLogado');
        if($this->Session->check('filtros'))
            $this->Session->delete('filtros');
        if($this->Session->check('exibirSwitchComissaoFormando'))
            $this->Session->delete('exibirSwitchComissaoFormando');
    }

    function super_index() {
        $this->set('grupos', array_merge(array('' => 'Todos'), $this->Usuario->grupos));
        $this->set('niveis', array_merge(array('' => 'Todos'), $this->Usuario->niveis));
        $this->set('ativo', array('1' => 'Ativo', '0' => 'Inativo'));
        $usuarioLogado = $this->obterUsuarioLogado();
        $this->set('usuarios', $this->paginate('Usuario'));
        $this->set('usuarioLogado', $usuarioLogado);
    }
    
    function super_listar(){
        $this->layout = false;
        $usuario = $this->Session->read('Usuario');
        if (!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
                $this->Session->write("filtros.{$usuario['Usuario']['grupo']}.usuarios", $this->data);
        } else {
            $this->Usuario->unbindModelAll();
            $this->Usuario->bindModel(array(
                'hasOne' => array(
                    'ViewFormandos' => array(
                        'className' => 'ViewFormandos',
                        'joinTable' => 'vw_formandos',
                        'foreignKey' => 'id',
                        'unique' => false
                    )
                )
            ), false);
            $options['order'] = array('Usuario.id' => 'asc');
            $usuarioId = array($usuario['Usuario']['id']);
            if($this->Session->check("filtros.{$usuario['Usuario']['grupo']}.usuarios")) {
                $config = $this->Session->read("filtros.{$usuario['Usuario']['grupo']}.usuarios");
                if(isset($config['Usuario']['id']))
                    $usuarioId[] = $config['Usuario']['id'];
                foreach($config as $modelo => $filtro)
                    foreach($filtro as $chave => $valor)
                        if($valor != '')
                            if($chave == 'id')
                                $options['conditions']["$modelo.$chave"] = $valor;
                            else
                                $options['conditions'][] = "lower($modelo.$chave) like('%".strtolower($valor)."%')";
                $this->data = $config;
            }
            $options['limit'] = 50;
            $this->paginate['Usuario'] = $options;
            $usuarios = $this->paginate('Usuario');
            $this->set('usuarioLogado', $usuario);
            $this->set('usuarios', $usuarios);
            $this->set('grupos', array_merge(array('' => 'Todos'), $this->Usuario->grupos));
            $this->set('niveis', array_merge(array('' => 'Todos'), $this->Usuario->niveis));
            $this->set('ativo', array('' => 'Todos', '1' => 'Ativo', '0' => 'Inativo'));
            $this->render('super_listar');
        }
    }
    
    function atendimento_listar(){
        $this->layout = false;
        $usuario = $this->obterUsuarioLogado();
        if (!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
                $this->Session->write("filtros.{$usuario['ViewFormandos']['grupo']}.usuarios", $this->data);
        } else {
            $this->loadModel('ViewFormandos');
            $this->ViewFormandos->unbindModelAll();
            $options['conditions']['grupo'] = array('comissao','formando');
            $options['order'] = array('ViewFormandos.id' => 'asc');
            $usuarioId = array($usuario['ViewFormandos']['id']);
            if($this->Session->check("filtros.{$usuario['ViewFormandos']['grupo']}.usuarios")) {
                $config = $this->Session->read("filtros.{$usuario['ViewFormandos']['grupo']}.usuarios");
                if(isset($config['ViewFormandos']['id']))
                    $usuarioId[] = $config['ViewFormandos']['id'];
                foreach($config as $modelo => $filtro)
                    foreach($filtro as $chave => $valor)
                        if($valor != '')
                            if($chave == 'id')
                                $options['conditions']["$modelo.$chave"] = $valor;
                            else
                                $options['conditions'][] = "lower($modelo.$chave) like('%".strtolower($valor)."%')";
                $this->data = $config;
            }
            $options['fields'] = array(
                'ViewFormandos.*',
                "(select if(checkout = 'aberto',1,0) from turmas t where ViewFormandos.turma_id = t.id) as checkout",
                "(select if((select count(0) from protocolos p where ViewFormandos.id = " .
                "p.usuario_id and p.tipo = 'checkout' limit 1) > 0,1,0)) as fez_checkout"
            );
            $options['limit'] = 50;
            $this->paginate['ViewFormandos'] = $options;
            $usuarios = $this->paginate('ViewFormandos');
            $this->set('usuarioLogado', $usuario);
            $this->set('formandos', $usuarios);
            $this->set('grupos', array_merge(array('' => 'Todos'), $this->Usuario->grupos));
            $this->set('niveis', array_merge(array('' => 'Todos'), $this->Usuario->niveis));
            $this->set('ativo', array('' => 'Todos', '1' => 'Ativo', '0' => 'Inativo'));
        }
    }
    
    function financeiro_listar(){
        $this->atendimento_listar();
    }
    
    function super_inserir() {
        $this->layout = false;
        if (!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $this->Usuario->create();
            $this->data['Usuario']['registro_timestamp_criacao'] = DboSource::expression('NOW()');
            if ($this->Usuario->save($this->data)) {
                $this->Session->setFlash("Usuário inserido com sucesso.", 'metro/flash/success');
            } else {
                $this->Session->setFlash("Ocorreu um erro ao inserir o usuário.", 'metro/flash/error');
            }
        }
        $this->set('grupos', $this->Usuario->grupos);
        $this->set('niveis', $this->Usuario->niveis);
    }

    function super_procurar() {
        $chave = empty($this->data['Usuarios']['chave']) ? "" : $this->data['Usuarios']['chave'];
        $chaves = preg_split("[, -.]", $this->data['Usuarios']['chave']);
        $nivel = $this->data['Usuarios']['nivel'];
        $grupo = $this->data['Usuarios']['grupo'];

        function construirQuery($c) {
            return array('Usuario.email LIKE' => '%' . $c . '%');
        }

        $chaves = array_map('construirQuery', $chaves);

        $conditions = array();

        if (!empty($chaves)) {
            $conditions = array(
                'AND' => array(array('OR' => $chaves),
                    'Usuario.grupo LIKE' => '%' . $grupo . '%',
                    'Usuario.nivel LIKE' => '%' . $nivel . '%'
                ));
        }

        $this->set('usuarios', $this->paginate('Usuario', $conditions));
        $usuarioLogado = $this->obterUsuarioLogado();
        $this->set('usuarioLogado', $usuarioLogado);
        $this->set('grupos', array_merge(array('' => 'Todos'), $this->Usuario->grupos));
        $this->set('niveis', array_merge(array('' => 'Todos'), $this->Usuario->niveis));
        $this->set('chave', $chave);
        $this->render('super_index');
    }

    function super_adicionar() {
        if (!empty($this->data)) {
            $this->Usuario->create();
            if ($this->Usuario->save($this->data)) {
                $this->Session->setFlash(__('O usuário foi salvo com sucesso', true), 'flash_sucesso');
                $this->redirect("/{$this->params['prefix']}/usuarios/visualizar/{$this->Usuario->id}");
            } else {
                $this->Session->setFlash(__('Ocorreu um erro ao salvar o usuário.', true), 'flash_erro');
            }
        }
        $this->set('grupos', $this->Usuario->grupos);
        $this->set('nivels', $this->Usuario->niveis);
    }

    function super_editar($id = null) {
        $this->Usuario->id = $id;
        if (!empty($this->data)) {
            if ($this->data['Usuario']['senha'] == Security::hash('', 'sha1', true) && $this->data['Usuario']['confirmar'] == '') {
                unset($this->data['Usuario']['senha']);
                unset($this->data['Usuario']['confirmar']);
            }
            $this->Usuario->bindModel(array(
                'hasAndBelongsToMany' => array(
                    'UniversidadeUsuario' => array(
                        'className' => 'Universidade',
                        'joinTable' => 'universidades_usuarios',
                        'foreignKey' => 'usuario_id',
                        'associationForeignKey' => 'universidade_id',
                        'unique' => false
                    )
                )
            ), false);
            if ($this->Usuario->save($this->data)) {
                if(isset($this->data['universidadesRemovidas'])) {
                    foreach($this->data['universidadesRemovidas'] as $universidadeRemovida)
                        $this->Usuario->query("delete from universidades_usuarios where " .
                            "usuario_id = {$this->data['Usuario']['id']} and " .
                            "universidade_id = {$universidadeRemovida}");
                }
                $this->Session->setFlash('O usuario foi salvo com sucesso.', 'flash_sucesso');
                $this->redirect("/{$this->params['prefix']}/usuarios/visualizar/{$this->Usuario->id}");
            } else {
                $this->Session->setFlash(__('Ocorreu um erro ao salvar o usuário.', true), 'flash_erro');
            }
        }
        else
            $this->data = $this->Usuario->read();

        if (!$this->data)
            $this->Session->setFlash('Usuário não existente,', 'flash_erro');
        elseif($this->data['Usuario']['grupo'] == 'comercial') {
            $this->loadModel('Universidade');
            $universidades = $this->Universidade->find('list',array(
                'fields' => array('nome')
            ));
            $this->Usuario->unbindModelAll();
            $this->Usuario->bindModel(array(
                'hasAndBelongsToMany' => array(
                    'UniversidadeUsuario' => array(
                        'className' => 'Universidade',
                        'joinTable' => 'universidades_usuarios',
                        'foreignKey' => 'usuario_id',
                        'associationForeignKey' => 'universidade_id',
                        'unique' => false
                    )
                )
            ), false);
            $this->Usuario->recursive = 1;
            $this->data = $this->Usuario->read();
            $selecionados = array();
            foreach($this->data['UniversidadeUsuario'] as $universidadeUsuario)
                array_push($selecionados, $universidadeUsuario['id']);
            $this->set('universidades',$universidades);
            $this->set('selecionados',$selecionados);
        }

        $this->set('grupos', $this->Usuario->grupos);
        $this->set('nivels', $this->Usuario->niveis);
    }
    
    function super_alterar($id = null) {
        $this->layout = false;
        Configure::write(array('debug' => 0));
        $this->Usuario->id = $id;
        if (!empty($this->data)) {
            $this->Usuario->bindModel(array(
                'hasAndBelongsToMany' => array(
                    'UniversidadeUsuario' => array(
                        'className' => 'Universidade',
                        'joinTable' => 'universidades_usuarios',
                        'foreignKey' => 'usuario_id',
                        'associationForeignKey' => 'universidade_id',
                        'unique' => false
                    )
                )
            ), false);
            if ($this->Usuario->save($this->data)) {
                $this->Session->setFlash('Usuário foi alterado com sucesso.', 'metro/flash/success');
            } else {
                $this->Session->setFlash('Ocorreu um erro ao alterar o usuário.', 'metro/flash/error');
            }
        }else{
            $this->Usuario->unbindModelAll();
            $this->Usuario->bindModel(array(
                'hasAndBelongsToMany' => array(
                    'UniversidadeUsuario' => array(
                        'className' => 'Universidade',
                        'joinTable' => 'universidades_usuarios',
                        'foreignKey' => 'usuario_id',
                        'associationForeignKey' => 'universidade_id',
                        'unique' => false
                    )
                )
            ), false);
            $this->Usuario->recursive = 1;
            $this->data = $this->Usuario->read();
        }

        $this->set('grupos', $this->Usuario->grupos);
        $this->set('niveis', $this->Usuario->niveis);
    }
    
    function super_trocar_turma($id = null){
        $usuario = $this->Usuario->read(null, $id);
        $flash = false;
        if(!empty($this->data)){
            if($usuario['FormandoProfile']['aderido'] == 1){
                $this->loadModel('ViewFormandos');
                $codigoFormando = $this->ViewFormandos->find('all', array(
                    'conditions' => array(
                        'ViewFormandos.turma_id' => $this->data['TurmaUsuarios']['turma_id']
                    ),
                    'order' => array('ViewFormandos.codigo_formando' => 'desc')
                ));
                $codigo = $codigoFormando[0]['ViewFormandos']['codigo_formando'] + 1;
                $proximoCodigo = substr($codigo, 4);
                $this->FormandoProfile->id = $usuario['FormandoProfile']['id'];
                $this->FormandoProfile->saveField('codigo_formando', $proximoCodigo);
                $flash = "Troca de turma efetuada com sucesso, o novo código do formando é <b style='color: black'>{$codigo}<b>.";
            }
            $this->loadModel('TurmasUsuarios');
            if(empty($flash))
                $flash = "Troca de turma efetuada com sucesso.";
            if($this->TurmasUsuarios->updateAll(
                array('TurmasUsuarios.turma_id' => $this->data['TurmaUsuarios']['turma_id']),
                array("TurmasUsuarios.usuario_id" => $id))){
                $this->Session->setFlash($flash, 'metro/flash/success');
            }else{
                $this->Session->setFlash("Ocorreu um erro ao trocar a turma do usuário.", 'metro/flash/error');
            }
        }
        $this->Turma->unBindModelAll();
        $t = array();
        $turmas = $this->Turma->find('all');
        foreach($turmas as $turma)
            $t[$turma['Turma']['id']] = $turma['Turma']['id'] . " - " . $turma['Turma']['nome'];
        $this->set('turmas', $t);
        $this->set('usuario', $usuario);
    }

    function super_visualizar($id = null) {
        $usuario = $this->Usuario->find(array('Usuario.id' => $id));

        if (!$usuario) {
            $this->Session->setFlash('Usuário não existente,', 'flash_erro');
        }
        $this->set('usuario', $this->Usuario->find(array('Usuario.id' => $id)));
    }

    function super_deletar($id = null) {

        if ($this->Usuario->delete($id))
            $this->Session->setFlash('Usuário excluido com sucesso.', 'flash_sucesso');
        else
            $this->Session->setFlash('Ocorreu um erro ao excluir o usuario', 'flash_erro');

        $this->redirect("/{$this->params['prefix']}/usuarios");
    }
    
    function super_exigir_troca_senha($usuarioId) {
        if($this->Usuario->updateAll(
            array('Usuario.trocar_senha' => 1),
            array("Usuario.id" => $usuarioId)))
            $this->Session->setFlash('O Usuário será obrigado a alterar a senha nos proximo login', 'flash_sucesso');
        else
            $this->Session->setFlash('Ocorreu um erro ao exigir a troca de senha', 'flash_erro');
        $this->redirect("/{$this->params['prefix']}/usuarios");
    }

    function super_procurar_por_email_grupo($grupo = 'todos', $email = "") {

        Configure::write('debug', '0');
        $this->layout = null;

        $chaves = preg_split("[, -.]", $email);

        $chaves = array_map('construirQuery', $chaves);

        $conditions = $chaves;
        if (preg_match('/tod[oaOA]s/i', $grupo)) {
            $this->set('usuarios', $this->Usuario->find('all', array('conditions' => $conditions)));
        } else {
            $conditions = array('AND' => array($conditions, array('Usuario.grupo' => $grupo)));
            $this->set('usuarios', $this->Usuario->find('all', array('conditions' => $conditions)));
        }
    }

    function atendimento_editar($uid = false) {
        $uid = !empty($this->data["Usuario"]["id"]) ? $this->data["Usuario"]["id"] : $uid;
        $this->set("uid", $uid);
        $this->formando_editar($uid);
    }

    function formando_editar($uid = false) {

        $turma = $this->obterTurmaLogada();

        $this->pageTitle = "Formando - Editar dados";
        $this->loadModel('CursoTurma');
        $uid = $uid ? $uid : $this->Session->read('Usuario.Usuario.id');
        

        if (!$turma) {
            $t = $this->Turma->query("select turma_id from turmas_usuarios TurmaUsuario where usuario_id = {$uid}");
            $turma = $this->Turma->read(null,$t[0]['TurmaUsuario']['turma_id']);
        }

        $this->Usuario->id = $uid;
        $turmaId = $turma["Turma"]["id"];
        $this->Usuario->unBindModel(array('hasMany' => array('Despesa'), 'hasAndBelongsToMany' => array('Turma')), false);
        if (!empty($this->data)) {
            $uid = !empty($this->data["Usuario"]["id"]) ? $this->data["Usuario"]["id"] : $this->Session->read('Usuario.Usuario.id');
            $this->Usuario->contain('FormandoProfile');
            $usuarioLido = $this->Usuario->findById($uid);
            if ($this->params['prefix'] == 'formando') {
                $usuarioLido['Usuario']['confirmar'] = $usuarioLido['Usuario']['senha'];
                unset($usuarioLido['Usuario']['senha']);
                unset($usuarioLido['Usuario']['confirmar']);
                unset($this->data['Usuario']['senha']);
                unset($this->data['Usuario']['confirmar']);
            }
            $this->data['Usuario']['id'] = $uid;

            if (substr_count($this->data['FormandoProfile']['diretorio_foto_perfil'], "temp") > 0) {
                $diretorioFinalDaFotoDoProfile = str_replace('temp', 'foto_perfil', $this->data['FormandoProfile']['diretorio_foto_perfil']);
                copy($this->data['FormandoProfile']['diretorio_foto_perfil'], $diretorioFinalDaFotoDoProfile);
                unlink($this->data['FormandoProfile']['diretorio_foto_perfil']);
                $this->data['FormandoProfile']['diretorio_foto_perfil'] = $diretorioFinalDaFotoDoProfile;
            }

            foreach ($this->data['FormandoProfile'] as $coluna => $valor)
                $usuarioLido['FormandoProfile'][$coluna] = $this->data['FormandoProfile'][$coluna];

            if ($this->data['FormandoProfile']['data-nascimento'] != "") {
                $arrayData = explode('-', $this->data['FormandoProfile']['data-nascimento']);
                $usuarioLido['FormandoProfile']['data_nascimento'] = "{$arrayData[2]}-{$arrayData[1]}-{$arrayData[0]}";
            }

            if($this->validateCpf($this->data['FormandoProfile']['cpf']) == 1){

                $usuarioLido['FormandoProfile']['cpf'] = str_replace(array(",", ".", "-"), "", $this->data['FormandoProfile']['cpf']);
            }else{

                $this->Session->setFlash('CPF Inválido.', 'metro/flash/error');
                exit();
            }

            // Remover formatação do CEP
            $usuarioLido['FormandoProfile']['end_cep'] = str_replace(array(",", ".", "-"), "", $this->data['FormandoProfile']['end_cep']);

            $usuarioLido['Usuario']['nome'] = $this->data['Usuario']['nome'];
            $usuarioLido['Usuario']['email'] = $this->data['Usuario']['email'];
            $usuarioLido['Usuario']['data_ultima_alteracao'] = date('Y-m-d H:i:s', strtotime('now'));


            unset($this->Usuario->FormandoProfile->validate['forma_pagamento']);
            unset($this->Usuario->FormandoProfile->validate['parcelas']);
            if ($this->params['prefix'] == 'atendimento') {
                $usuarioLido['Usuario']['confirmar'] = null;
                if ($this->data['Usuario']['senha'] == Security::hash($this->data['Usuario']['confirmar'], 'sha1', true)) {
                    $usuarioLido['Usuario']['senha'] = $this->data['Usuario']['senha'];
                    $usuarioLido['Usuario']['confirmar'] = $this->data['Usuario']['confirmar'];
                }
            }
            if ($this->Usuario->saveAll($usuarioLido)) {
                $this->Session->setFlash('Dados atualizados com sucesso.', 'flash_sucesso');
                $this->data['Usuario']['confirmar'] = $this->data['Usuario']['senha'];
            } else {
                unset($this->data['Usuario']['senha']);
                unset($this->data['Usuario']['confirmar']);
                $this->Session->setFlash(__('Ocorreu um erro ao atualizar os dados.', true), 'flash_erro');
            }
        } else {
            $this->data = $this->Usuario->read();
            $dateTime = $this->create_date_time_from_format('Y-m-d', $this->data['FormandoProfile']['data_nascimento']);
            if ($dateTime != null)
                $this->data['FormandoProfile']['data-nascimento'] = date_format($dateTime, 'd-m-Y');
            $this->data['Usuario']['confirmar'] = $this->data['Usuario']['senha'];
        }

        // Criar Lista de cursos_turmas
        //$this->CursoTurma->recursive = 0;
        $this->CursoTurma->contain(array('Curso.Faculdade.Universidade'));
        $cursos_turmas = $this->CursoTurma->find('all', array('conditions' => array('turma_id' => $turmaId)));
        $select_cursos_turmas = array();

        foreach ($cursos_turmas as $curso_turma) {
            $universidade = $curso_turma['Curso']['Faculdade']['Universidade']['nome'];
            $faculdade = $curso_turma['Curso']['Faculdade']['nome'];
            $curso = $curso_turma['Curso']['nome'];
            if ($curso != 'Curso Geral') {
                $select_cursos_turmas[$curso_turma['CursoTurma']['id']] = "$curso  - $faculdade - $universidade - {$curso_turma['CursoTurma']['turno']}";
            }
        }
        //debug($select_cursos_turmas);
        $this->set('lista_curso_turma_id', $select_cursos_turmas);

        if (!$this->data)
            $this->Session->setFlash('Usuário não existente,', 'flash_erro');
    }

    public function validateCpf($cpf = null) {
 
        // Verifica se um número foi informado
        if(empty($cpf)) {
            return 0;
        }

        // Elimina possivel mascara
        $cpf = preg_replace('/[^\da-z]/i', '', $cpf);
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

        // Verifica se o numero de digitos informados é igual a 11 
        if (strlen($cpf) != 11) {
            return 0;
        }
        // Verifica se nenhuma das sequências invalidas abaixo 
        // foi digitada. Caso afirmativo, retorna falso
        else if ($cpf == '00000000000' || 
            $cpf == '11111111111' || 
            $cpf == '22222222222' || 
            $cpf == '33333333333' || 
            $cpf == '44444444444' || 
            $cpf == '55555555555' || 
            $cpf == '66666666666' || 
            $cpf == '77777777777' || 
            $cpf == '88888888888' || 
            $cpf == '99999999999') {
            return 0;
         // Calcula os digitos verificadores para verificar se o
         // CPF é válido
         } else {   

            for ($t = 9; $t < 11; $t++) {

                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf{$c} != $d) {
                    return 0;
                }
            }

            return 1;
        }
    }

}

?>
