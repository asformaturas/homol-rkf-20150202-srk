<?php

App::import('Controller', 'Formandos');

class AreaFinanceiraController extends AppController {

    var $name = 'AreaFinanceira';
    var $uses = array('Despesa', 'Pagamento', 'DespesaPagamento',
        'FormandoProfile', 'CampanhasUsuario', 'Usuario', 'ViewFormandos');
    var $components = array('Financeiro', 'Mailer', 'BoletoBradesco');
    var $nomeDoTemplateSidebar = 'AreaFinanceira';
    var $app = array('dados','gerar_codigo');
    
    var $paginate = array(
        'limit' => 50,
        'order' => array(
            'Despesa.id' => 'asc'
        )
    );

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow(
            'homologacao',
            'homologacao_bradesco',
            'https://homolog.meiosdepagamentobradesco.com.br'
        );
    }

    public function startup( $controller ){

        App::import('Component', 'BoletoBradesco');

        $this->BoletoBradesco = new BoletoBradesco();
    }

    function atendimento_reverter_cancelamento($uid) {
        $this->formando_reverter_cancelamento($uid);
    }
    
    function atendimento_cheque(){
        $this->layout = false;
        $this->autoRender = false;
        if(!empty($this->data)){
            $despesa = $this->Despesa->findById($this->data['despesa']);
            $this->Despesa->id = $despesa['Despesa']['id'];
            if($despesa['Despesa']['cheque'] == 0){
                $this->Despesa->saveField('cheque', 1);
            }else{
                $this->Despesa->saveField('cheque', 0);
            }
        }
    }


    function formando_reverter_cancelamento($uid = false) {
        $this->autoRender = false;
        $usuario = $this->Session->read('Usuario');
        $uid = $uid ? $uid : $usuario['Usuario']['id'];
        $this->Despesa->recursive = 0;
        $despesaCancelamento = $this->Despesa->find('first', array('conditions' => array('Despesa.usuario_id' => $uid, 'Despesa.tipo' => 'cancelamento')));
        if (!empty($despesaCancelamento)) {
            $this->Despesa->id = $despesaCancelamento['Despesa']['id'];
            if (!$this->Despesa->saveField('status', 'cancelada')) {
                $this->Session->setFlash('Erro ao cancelar despesa de cancelamento. Por favor entre em contato conosco.', 'flash_erro');
                $this->redirect("/{$this->params['prefix']}/solicitacoes");
            } else {
                $this->Despesa->id = null;
            }
        }
        $options = array('Despesa.status' => 'renegociada', 'Despesa.tipo' => 'adesao');
        if (!$this->Despesa->updateAll(array('Despesa.status' => "'aberta'"), $options)) {
            $this->Session->setFlash('Erro ao reabrir parcelas. Por favor entre em contato conosco.', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/solicitacoes");
        } else {
            if (!$this->FormandoProfile->updateAll(array('FormandoProfile.situacao' => "'ativo'"), array('FormandoProfile.usuario_id' => $uid))) {
                $this->Session->setFlash('Erro ao ativar formando. Por favor entre em contato conosco.', 'flash_erro');
                $this->redirect("/{$this->params['prefix']}/solicitacoes");
            } else {
                $this->loadModel('Protocolo');
                if (!$this->Protocolo->updateAll(array('Protocolo.status' => "'finalizado'"), array('Protocolo.usuario_id' => $uid, 'Protocolo.tipo' => 'cancelamento', 'Protocolo.grupo' => 'formando'))) {
                    $this->Session->setFlash('Erro ao finalizar protocolo. Por favor entre em contato conosco.', 'flash_erro');
                    $this->redirect("/{$this->params['prefix']}/solicitacoes");
                } else {
                    $this->Session->setFlash('Cancelamento de contrato revertido com sucesso.', 'flash_sucesso');
                    $this->redirect("/{$this->params['prefix']}/solicitacoes");
                }
            }
        }
    }

    function atendimento_renegociacao($uid) {
        $this->formando_renegociacao($uid);
    }
    
    function atendimento_cancelar_extra($extraId){
        $extra = $this->Despesa->find('first', array('conditions' => array('Despesa.id' => $extraId)));
        $this->Despesa->id = $extra['Despesa']['id'];
        $this->Despesa->saveField('status', 'cancelada');
        $this->CampanhasUsuario->id = $extra['Despesa']['campanhas_usuario_id'];
        $this->CampanhasUsuario->saveField('cancelada', 1);
        $this->Despesa->saveField('status', 'cancelada');
    }
    
    function cancelar_campanha() {
        $json = array('erro' => true);
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        if(isset($this->data['campanha_usuario'])) {
            $campanhaUsuario = $this->CampanhasUsuario->read(null,$this->data['campanha_usuario']);
            if($campanhaUsuario) {
                if($this->Despesa->updateAll(
                    array('Despesa.status' => "'cancelada'"),
                    array('Despesa.campanhas_usuario_id' => $this->data['campanha_usuario'])
                )) {
                    if($this->CampanhasUsuario->updateAll(
                        array('cancelada' => 1),
                        array('CampanhasUsuario.id' => $this->data['campanha_usuario'])
                    )) {
                        $json['erro'] = false;
                    }
                }
            }
        }
        echo json_encode($json);
    }
    
    function descancelar_campanha() {
        $json = array('erro' => true);
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        if(isset($this->data['campanha_usuario'])) {
            $campanhaUsuario = $this->CampanhasUsuario->read(null,$this->data['campanha_usuario']);
            if($campanhaUsuario) {
                $despesas = $this->Financeiro->obterDespesasExtras($this->data['campanha_usuario'],true);
                foreach($despesas as $despesa) {
                    if($despesa['valor_pago'] > 0)
                        $this->Despesa->updateAll(
                            array('Despesa.status' => "'paga'"),
                            array('Despesa.id' => $despesa['id'])
                        );
                    else
                        $this->Despesa->updateAll(
                            array('Despesa.status' => "'aberta'"),
                            array('Despesa.id' => $despesa['id'])
                        );
                }
                if($this->CampanhasUsuario->updateAll(
                    array('cancelada' => 0),
                    array('CampanhasUsuario.id' => $this->data['campanha_usuario'])
                )) {
                    $json['erro'] = false;
                }
            }
        }
        echo json_encode($json);
    }
    
    function atendimento_vincular_pagamento() {
        $json = array('erro' => true);
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        if(isset($this->data['despesa']) && isset($this->data['pagamento'])) {
            $despesa = $this->Despesa->read(null,$this->data['despesa']);
            $pagamento = $this->Pagamento->read(null,$this->data['pagamento']);
            if($despesa && $pagamento) {
                if($despesa['Despesa']['status'] == 'aberta')
                    $despesa['Despesa']['status'] = 'paga';
                if(empty($despesa['Despesa']['status_igpm']))
                    $despesa['Despesa']['status_igpm'] = 'nao_pago';
                if(!empty($pagamento['Pagamento']['dt_liquidacao']))
                    $despesa['Despesa']['data_pagamento'] = $pagamento['Pagamento']['dt_liquidacao'];
                $despesaPagamento = array(
                    'DespesaPagamento' => array(
                        'despesa_id' => $despesa['Despesa']['id'],
                        'pagamento_id' => $pagamento['Pagamento']['id'],
                        'valor' => $despesa['Despesa']['valor'],
                        'multa' => 0.00,
                        'correcao_igpm' => 0.00
                    )
                );
                if($this->DespesaPagamento->save($despesaPagamento))
                    if($this->Despesa->save($despesa))
                        $json['erro'] = false;
            }
        }
        echo json_encode($json);
    }
    
    function super_vincular_pagamento(){
        $this->atendimento_vincular_pagamento();
    }

    function financeiro_vincular_pagamento(){
        $this->atendimento_vincular_pagamento();
    }
    
    function super_remover_pagamento($id = false){
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('erro' => true);
        if(!empty($id)){
            $this->Pagamento->delete($id);
            $json['erro'] = false;
        }else{
            $json['erro'] = false;
        }
        echo json_encode($json);
    }
    
    function super_inserir_despesas($uid = false){
        if(!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $erro = false;
            $usuarioId = $uid;
            $valorTotal = $this->data['valor'];
            $despesas = $this->data['despesas'];
            foreach($despesas as $parcela => $d) {
                $despesa = (array)$d;
                $this->Despesa->create();
                if(!$this->Despesa->save(array(
                    'Despesa' => array(
                        'usuario_id' => $usuarioId,
                        'valor' => $despesa['valor'],
                        'data_vencimento' => $despesa['data'],
                        'tipo' => 'adesao',
                        'status' => 'aberta',
                        'parcela' => $parcela+1,
                        'total_parcelas' => count($despesas),
                        'data_cadastro' => date('Y-m-d H:i:s')
                    )
                )))
                    $erro = true;
                elseif($parcela == 0)
                    $primeiraDespesa = $this->Despesa->getLastInsertId();
            }
            $this->recalcularParcelas($usuarioId);
            if(!$erro){
                $this->FormandoProfile->updateAll(
                    array(
                        'FormandoProfile.valor_adesao' => $valorTotal,
                        'FormandoProfile.parcelas_adesao' => $totalDespesas,
                    ),
                    array(
                        'FormandoProfile.usuario_id' => $usuarioId
                    )
                );
            }
        }
    }
    
    function super_remover_despesa(){
        if(!empty($this->data)){
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $despesa = $this->Despesa->find('first', array(
                'conditions' => array(
                    'Despesa.id' => $this->data['despesa']
                ),
                'recursive' => 2
            ));
            if(array_key_exists("DespesaPagamento", $despesa)){
                $this->Pagamento->delete($despesa['DespesaPagamento'][0]['Pagamento']['id']);
            }
            $this->Despesa->delete($despesa['Despesa']['id']);
            $this->recalcularParcelas($this->data['uid']);
        }
    }
    
    function super_desvincular_pagamento(){
        if(!empty($this->data)){
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $despesa = $this->Despesa->find('first', array(
                'conditions' => array(
                    'Despesa.id' => $this->data['despesa']
                ),
                'recursive' => 2
            ));
            $this->DespesaPagamento->delete(array('DespesaPagamento.id' => $despesa['DespesaPagamento'][0]['id']));
            $this->Despesa->updateAll(
                array(
                    'Despesa.status' => "'aberta'",
                    'Despesa.data_pagamento' => NULL,
                ),
                array(
                    'Despesa.id' => $despesa['Despesa']['id']
                )
            );
        }
    }
    
    function recalcularParcelas($uid = false){
        $despesas = $this->Despesa->find('all', array(
            'conditions' => array(
                'Despesa.usuario_id' => $uid,
                'Despesa.status not' => array('cancelada', 'renegociada') 
            )
        ));
        $totalDespesas = count($despesas);
        foreach($despesas as $key => $despesa){
            $this->Despesa->id = $despesa['Despesa']['id'];
            $this->Despesa->saveField('total_parcelas', $totalDespesas);
            $this->Despesa->saveField('parcela', $key + 1);
        }
    }
    
    function super_inserir_pagamentos($uid = false){
        $this->layout = false;
        if(!empty($_POST)){
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            if(array_key_exists('Pagamento', $_POST))
                $pagamentos = array_merge($_POST['Pagamento'], $_POST['data']['Pagamento']);
            else
                $pagamentos = $_POST['data']['Pagamento'];
            $insert = array();
            foreach($pagamentos as $pagamento){
                $dateTime = str_replace('/','-',$pagamento[0]);
                $dateTime = $this->create_date_time_from_format('d-m-Y', $dateTime);
                $vencimento = date_format($dateTime, 'Y-m-d');
                $dateTime = str_replace('/','-',$pagamento[1]);
                $dateTime = $this->create_date_time_from_format('d-m-Y', $dateTime);
                $liquidacao = date_format($dateTime, 'Y-m-d');
                $this->Pagamento->create();
                $insert['Pagamento']  = array(
                        'usuario_id' => $uid,
                        'dt_cadastro' => DboSource::expression('NOW()'),
                        'dt_vencimento' => $vencimento,
                        'dt_liquidacao' => $liquidacao,
                        'tipo' => 'boleto',
                        'status' => 'pago',
                        'valor_nominal' => str_replace(',',"",($pagamento[2])),
                        'valor_pago' => str_replace(',',"",($pagamento[3])),
                        'observacao' => 'Pagamento inserido pelo Financeiro.'
                );
                if($this->Pagamento->save($insert))
                    $this->Session->setFlash('Pagamentos inseridos com sucesso.',
                            'metro/flash/success');
                else
                    $this->Session->setFlash('Erro ao inserir pagamentos.',
                            'metro/flash/error');
            }
        }
            
    }

    function atendimento_index($uid) {
        $this->formando_index($uid);
    }

    function formando_renegociacao($uid = false) {
        $usuario = $this->Session->read('Usuario');
        $uid = $uid ? $uid : $usuario['Usuario']['id'];
        $formando = $this->ViewFormandos->find('first', array('conditions' => array('id' => $uid)));
        $this->set('formando', $formando);
        $totalAdesao = $this->Financeiro->obterValorAdesao($uid);
        $totalPagoAdesao = $this->Financeiro->obterValorPagoAdesao($uid);
        $this->loadModel('Parcelamento');
        $this->Parcelamento->recursive = 0;
        $filterParcelamento = $formando['ViewFormandos']['parcelamento_id'] == "" ? "Parcelamento.id = Parcelamento.id" : "Parcelamento.id = {$formando['ViewFormandos']['parcelamento_id']}";
        $parcelamento = $this->Parcelamento->find('first', array('conditions' => array("Turma.id = {$formando['ViewFormandos']['turma_id']}", $filterParcelamento, "Parcelamento.data > date_sub(now(),interval 3 month)")));
        $tituloParcelamento = empty($parcelamento['Parcelamento']['titulo']) ? "(Parcelamento.titulo is null or Parcelamento.titulo = 'Padrão')" : "Parcelamento.titulo = '{$parcelamento['Parcelamento']['titulo']}'";
        $parcelamentos = $this->Parcelamento->find('all', array('conditions' => array("Turma.id = {$formando['ViewFormandos']['turma_id']}", $tituloParcelamento, "Parcelamento.data > date_sub(now(),interval 3 month)")));
        $parcelas = $this->Parcelamento->find('all', array('conditions' => array("Turma.id = {$formando['ViewFormandos']['turma_id']}",
                $tituloParcelamento, "Parcelamento.data > date_sub(now(),interval 3 month)"), 'group' => 'Parcelamento.data'));
        $valorEmAberto = $totalAdesao - $totalPagoAdesao;
        $this->set('valorTotal', $totalAdesao);
        $this->set('valorPago', $totalPagoAdesao);
        $this->set('valorEmAberto', $valorEmAberto);
        $this->set("parcelamentos", $parcelamentos);
        $this->set('parcelas', $parcelas);
    }
    
    function atendimento_tabela_pagamentos($usuarioId) {
        $this->layout = false;
        $formando = $this->ViewFormandos->read(null,$usuarioId);
        $turma = $this->Turma->read(null,$formando['ViewFormandos']['turma_id']);
        $this->set('turma',$turma);
        $this->loadModel('Parcelamento');
        $this->Parcelamento->recursive = -1;
        $parcelamentos = $this->Parcelamento->find('all', array(
            'conditions' => array(
                'turma_id' => $formando['ViewFormandos']['turma_id'],
                'data >=' => date('Y-m-\0\1', strtotime('now'))
            )
        ));
        $this->set('parcelamentos',$parcelamentos);
    }
    
    function planejamento_plano_diferenciado() {
        $this->autoRender = false;
        Configure::write("debug",0);
        if($this->data["plano_id"] && $this->data["status"]) {
            $this->loadModel("ParcelamentoDiferenciado");
            $plano = $this->ParcelamentoDiferenciado->read(null,$this->data["plano_id"]);
            $plano["ParcelamentoDiferenciado"]["status"] = $this->data["status"];
            $plano["ParcelamentoDiferenciado"]["data_alteracao"] = date("Y-m-d H:i:s");
            if($this->data["status"] == "aceito") {
                $plano["ParcelamentoDiferenciado"]["plano"] = json_decode($plano["ParcelamentoDiferenciado"]["parcelamento"]);
                if($this->Financeiro->renegociarAdesao(
                        $plano["ParcelamentoDiferenciado"]["usuario_id"],
                        $plano["ParcelamentoDiferenciado"]["plano"]->parcelamento,
                        $plano["ParcelamentoDiferenciado"]["plano"]->valor,
                        $plano["ParcelamentoDiferenciado"]["plano"]->despesas)) {
                    $this->ParcelamentoDiferenciado->save($plano);
                    $this->Session->setFlash('As despesas do formando foram renegociadas com sucesso',
                            'metro/flash/success');
                } else
                    $this->Session->setFlash('Erro ao renegociar as despesas do formando',
                        'metro/flash/error');
            } else {
                if($this->ParcelamentoDiferenciado->save($plano))
                    $this->Session->setFlash('Status alterado com sucesso',
                        'metro/flash/success');
                else
                    $this->Session->setFlash('Erro ao alterar status do plano',
                        'metro/flash/error');
            }
        }
    }
    
    function planejamento_planos_diferenciados($planoId = false) {
        $this->layout = false;
        $this->loadModel("ParcelamentoDiferenciado");
        if(!$planoId) {
            $this->paginate["ParcelamentoDiferenciado"] = array(
                "order" => array(
                    "ParcelamentoDiferenciado.id" => "desc"
                )
            );
            $planos = $this->paginate('ParcelamentoDiferenciado');
            $this->set("planos",$planos);
        } else {
            $plano = $this->ParcelamentoDiferenciado->read(null,$planoId);
            $totalPago = 0;
            if($plano) {
                $usuarioId = $plano["ParcelamentoDiferenciado"]["usuario_id"];
                $totalPago = $this->Financeiro->obterValorPagoAdesao($usuarioId) + $this->Financeiro->obterValorPagoIgpm($usuarioId);
                $plano["ParcelamentoDiferenciado"]["plano"] = json_decode($plano["ParcelamentoDiferenciado"]["parcelamento"]);
                $this->loadModel("Parcelamento");
                $parcelamento = $this->Parcelamento->read(null,$plano["ParcelamentoDiferenciado"]["plano"]->parcelamento);
                if($parcelamento)
                    $plano["ParcelamentoOriginal"] = $parcelamento["Parcelamento"];
            }
            $this->set("plano",$plano);
            $this->set("totalPago",$totalPago);
            $this->render("planejamento_plano_diferenciado");
        }
    }
    
    function atendimento_renegociar($usuarioId) {
        $formando = $this->ViewFormandos->read(null,$usuarioId);
        $atendente = $this->obterUsuarioLogado();
        if($this->data && $formando) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            if($this->data['diferenciado'] == 1) {
                $this->loadModel("ParcelamentoDiferenciado");
                if($this->ParcelamentoDiferenciado->save(array(
                    "ParcelamentoDiferenciado" => array(
                        'usuario_id' => $formando['ViewFormandos']['id'],
                        "atendente_id" => $atendente["Usuario"]['id'],
                        'parcelamento' => json_encode($this->data),
                        "data_cadastro" => date("Y-m-d H:i:s")
                    )
                )))
                    $this->Session->setFlash('Parcelamento salvo com sucesso',
                        'metro/flash/success');
                else
                    $this->Session->setFlash('Erro ao salvar dados',
                        'metro/flash/error');
            } else {
                if($this->Financeiro->renegociarAdesao(
                        $usuarioId,
                        $this->data['parcelamento'],
                        $this->data['valor'],
                        $this->data['despesas'])) {
                    $this->Session->setFlash('Parcelas renegociadas com sucesso',
                        'metro/flash/success');
                } else {
                    $this->Session->setFlash('Erro ao criar despesas. Tente novamente mais tarde',
                        'metro/flash/error');
                }
            }
        } else {
            $this->layout = false;
            $this->set('formando',$formando['ViewFormandos']);
            $totalPago = $this->Financeiro->obterValorPagoAdesao($usuarioId) + $this->Financeiro->obterValorPagoIgpm($usuarioId);
            $this->set('totalPago',$totalPago);
            $parcelamento = false;
            if(!empty($formando['ViewFormandos']['parcelamento_id'])) {
                $this->loadModel('Parcelamento');
                $parcelamento = $this->Parcelamento->read(null,$formando['ViewFormandos']['parcelamento_id']);
            }
            $this->set('parcelamento',$parcelamento);
        }
    }
    
    function planos($usuarioId) {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $formando = $this->ViewFormandos->read(null,$usuarioId);
        $this->set('formando',$formando['ViewFormandos']);
        $turma = $this->Turma->read(null,$formando['ViewFormandos']['turma_id']);
        $this->set('turma',$turma['Turma']);
        $this->loadModel('Parcelamento');
        $this->Parcelamento->recursive = -1;
        $conditions = array(
            'turma_id' => $formando['ViewFormandos']['turma_id'],
            'data >=' => date('Y-m-\0\1', strtotime('now'))
        );
        $parcelamentos = $this->Parcelamento->find('list', array(
            'conditions' => $conditions,
            'group' => 'titulo',
            'fields' => array("titulo")
        ));
        $planos = array();
        if($parcelamentos) {
            foreach($parcelamentos as $parcelamento) {
                if(empty($planos[$parcelamento])) $planos[$parcelamento] = array();
                $cdt = !empty($parcelamento) ? array_merge($conditions,array("titulo = '{$parcelamento}'")) : $conditions;
                $meses = $this->Parcelamento->find('list', array(
                    'conditions' => $cdt,
                    'group' => "date_format(Parcelamento.data,'%Y%m')",
                    'fields' => array("data")
                ));
                foreach($meses as $mes) {
                    $planos[$parcelamento][$mes] = $this->Parcelamento->find('all', array(
                        'conditions' => array_merge($cdt,array("data" => $mes)),
                    ));
                }
            }
        }
        echo json_encode(array("planos" => $planos));
    }

    function atendimento_renegociar_() {
        $this->formando_renegociar($_POST['usuario']);
    }

    function formando_renegociar($uid = false) {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $retorno = array('erro' => false);
        $this->loadModel('Parcelamento');
        $this->Parcelamento->recursive = 0;
        $this->Parcelamento->id = $_POST['parcelamento'];
        $parcelamento = $this->Parcelamento->read();
        $formando = $this->FormandoProfile->find('first', array('conditions' => array('usuario_id' => $uid)));
        $this->FormandoProfile->id = $formando['FormandoProfile']['id'];
        $this->FormandoProfile->saveField('valor_adesao', $parcelamento['Parcelamento']['valor']);
        $this->FormandoProfile->saveField('dia_vencimento_parcelas', $_POST['diaVencimento']);
        $this->FormandoProfile->saveField('parcelas_adesao', $parcelamento['Parcelamento']['parcelas']);
        $usuario = $this->Session->read('Usuario');
        $uid = $uid ? $uid : $usuario['Usuario']['id'];
        $this->Despesa->recursive = 2;
        $despesasDoFormando = $this->Despesa->find('all',array(
            'conditions' => array(
                'Despesa.usuario_id' => $uid,
                'Despesa.status not' => array('cancelada','renegociada'),
                'tipo' => 'adesao'
            )
        ));
        foreach ($despesasDoFormando as $despesaDoFormando) {
            //apaga pagamentos em aberto
            if($despesaDoFormando["Despesa"]["status"] == "aberta" && false)
                foreach ($despesaDoFormando['DespesaPagamento'] as $despesaPagamento)
                    $this->Pagamento->delete($despesaPagamento['pagamento_id']);
            $this->Despesa->updateAll(
                array('Despesa.status' => "'renegociada'"),
                array('Despesa.id' => $despesaDoFormando['Despesa']['id']));
        }
        $despesaParaAdicionar = array(
            'usuario_id' => $uid,
            'valor' => $_POST['valorParcela'],
            'data_cadastro' => date('Y-m-d H:i:s'),
            'tipo' => 'adesao',
            'total_parcelas' => $parcelamento['Parcelamento']['parcelas'],
            'multa' => 0,
            'status_igpm' => 'nao_virado'
        );
        $dataParcela = $this->create_date_time_from_format('Y-m-d', $parcelamento['Parcelamento']['data']);
        $despesas = array();
        for ($a = 1; $a <= $parcelamento['Parcelamento']['parcelas']; $a++) {
            $mes = (date_format($dataParcela, 'm') + ($a - 1)) % 12 == 0 ? 12 : (date_format($dataParcela, 'm') + ($a - 1)) % 12;
            $ano = date('Y', mktime(0, 0, 0, date_format($dataParcela, 'm') + ($a - 1), $_POST['diaVencimento'], date_format($dataParcela, 'Y')));
            $diasDoMes = cal_days_in_month(CAL_GREGORIAN, $mes, $ano);
            $diaVencimento = $_POST['diaVencimento'] > $diasDoMes ? $diasDoMes : $_POST['diaVencimento'];
            $despesaParaAdicionar['parcela'] = $a;
            $despesaParaAdicionar['data_vencimento'] = date('Y-m-d', mktime(0, 0, 0, date_format($dataParcela, 'm') + ($a - 1), $diaVencimento, date_format($dataParcela, 'Y')));
            if (!$this->Despesa->save($despesaParaAdicionar))
                $retorno['erro'] = true;
            $despesas[] = $despesaParaAdicionar;
            $this->Despesa->id = null;
        }
        echo json_encode($despesas);
    }
    
    function atendimento_cancelamento($uid = false){
        $this->layout = false;
        Configure::write(array('debug' => 0));
        $usuario = $this->Session->read('Usuario');
        $uid = $uid ? $uid : $usuario['Usuario']['id'];
        $this->FormandoProfile->recursive = 1;
        $formando = $this->FormandoProfile->find('first', array('conditions' => "FormandoProfile.usuario_id = $uid"));
        $this->set('formando', $formando);
        $this->Despesa->recursive = 2;
        $despesasComAdesao = $this->Despesa->find('all', array('conditions' => array('Despesa.usuario_id' => $uid, 'Despesa.tipo' => 'adesao')));
        $saldoEmAberto = 0;
        foreach ($despesasComAdesao as $despesaComAdesao) {
            if ($despesaComAdesao['Despesa']['status'] == 'aberta') {
                $saldoEmAberto+= $despesaComAdesao['Despesa']['valor'];
            } elseif (isset($despesaComAdesao['DespesaPagamento'][0]['Pagamento']) && $despesaComAdesao['Despesa']['status'] == 'paga') {
                $valorPago = $despesaComAdesao['DespesaPagamento'][0]['Pagamento']['valor_nominal'];
                $valorPago = $valorPago > $despesaComAdesao['Despesa']['valor'] ? $despesaComAdesao['Despesa']['valor'] : $valorPago;
                $valo_igmp = 0; //alterar com a entrada de IGPM
                $saldoEmAberto += ($despesaComAdesao['Despesa']['valor'] + $valo_igmp) - $valorPago;
            }
        }
        $this->set('saldoEmAberto', $saldoEmAberto);
        $pagamentos = $this->Pagamento->find('all', array('conditions' => array('Pagamento.status' => 'pago', 'Pagamento.usuario_id' => $uid)));
        $totalPago = 0;
        foreach ($pagamentos as $pagamento) {
            if (sizeof($pagamento['DespesaPagamento']) == 0) {
                if ($pagamento['Pagamento']['codigo'] != "") {
                    if ((int) substr($pagamento['Pagamento']['codigo'], 1, 1) < 5)
                        $totalPago+= $pagamento['Pagamento']['valor_nominal'];
                }
            } else {
                $totalPago+= $pagamento['Pagamento']['valor_nominal'];
            }
        }
        $this->set('totalPago', $totalPago);
    }

   // function atendimento_cancelamento($uid) {
   //     $this->formando_cancelamento($uid);
   // }

    function formando_cancelamento($uid = false) {
        $usuario = $this->Session->read('Usuario');
        $uid = $uid ? $uid : $usuario['Usuario']['id'];
        $this->FormandoProfile->recursive = 1;
        $formando = $this->FormandoProfile->find('first', array('conditions' => "FormandoProfile.usuario_id = $uid"));
        $this->set('formando', $formando);
        $this->Despesa->recursive = 2;
        $despesasComAdesao = $this->Despesa->find('all', array('conditions' => array('Despesa.usuario_id' => $uid, 'Despesa.tipo' => 'adesao')));
        $saldoEmAberto = 0;
        foreach ($despesasComAdesao as $despesaComAdesao) {
            if ($despesaComAdesao['Despesa']['status'] == 'aberta') {
                $saldoEmAberto+= $despesaComAdesao['Despesa']['valor'];
            } elseif (isset($despesaComAdesao['DespesaPagamento'][0]['Pagamento']) && $despesaComAdesao['Despesa']['status'] == 'paga') {
                $valorPago = $despesaComAdesao['DespesaPagamento'][0]['Pagamento']['valor_nominal'];
                $valorPago = $valorPago > $despesaComAdesao['Despesa']['valor'] ? $despesaComAdesao['Despesa']['valor'] : $valorPago;
                $valo_igmp = 0; //alterar com a entrada de IGPM
                $saldoEmAberto += ($despesaComAdesao['Despesa']['valor'] + $valo_igmp) - $valorPago;
            }
        }
        $this->set('saldoEmAberto', $saldoEmAberto);
        $pagamentos = $this->Pagamento->find('all', array('conditions' => array('Pagamento.status' => 'pago', 'Pagamento.usuario_id' => $uid)));
        $totalPago = 0;
        foreach ($pagamentos as $pagamento) {
            if (sizeof($pagamento['DespesaPagamento']) == 0) {
                if ($pagamento['Pagamento']['codigo'] != "") {
                    if ((int) substr($pagamento['Pagamento']['codigo'], 1, 1) < 5)
                        $totalPago+= $pagamento['Pagamento']['valor_nominal'];
                }
            } else {
                $totalPago+= $pagamento['Pagamento']['valor_nominal'];
            }
        }
        $this->set('totalPago', $totalPago);
    }

    function cancelar() {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $retorno = array('error' => true);
        $this->loadModel('Protocolo');
        $this->loadModel('ProtocoloCancelamento');
        $usuarioLogado = $this->obterUsuarioLogado();
        $usuarioId = $_POST['usuario'];
        $this->Usuario->recursive = 1;
        $this->Usuario->unBindModel(array('hasMany' => array('Despesa')), false);
        $usuario = $this->Usuario->find('first', array('conditions' => array("Usuario.id" => $usuarioId)));
        $mes = $usuario['Turma'][0]['semestre_formatura'] == '1' ? 6 : 12;
        $ano = $usuario['Turma'][0]['ano_formatura'];
        $dias = cal_days_in_month(CAL_GREGORIAN, $mes, $ano);
        $time = mktime(0, 0, 0, $mes, $dias, $ano) - mktime(0, 0, 0, date('m'), date('d'), date('Y'));
        if ($time / 60 / 60 / 24 < 90) {
            $retorno['mensagem'] = "<div class='alert alert-block fade in'>" .
                        "Caro formando, cl&aacute;usulas do seu contrato dizem que s&oacute; &eacute; poss&iacute;vel realizar cancelamento " .
                        "at&eacute; tr&ecirc;s meses antes do t&eacute;rmino do ano letivo de conclus&atilde;o do curso.<br />" .
                        "Se realmente deseja cancelar o contrato justifique abaixo:" .
                        "</div>";
            App::import('Component', 'Protocolo');
            $protocolo = new ProtocoloComponent();
            $protocoloCriado = $protocolo->geraProtocoloCancelamento(array('usuario_id' => $usuario['Usuario']['id'], 'grupo' => 'formando', 'usuario_protocolo' => $usuarioLogado['Usuario']['id']));
            $protocoloCancelamento = $this->ProtocoloCancelamento->find('first', array('conditions' => array('Protocolo.protocolo' => $protocoloCriado)));
            $this->Protocolo->bindModel(array('hasOne' => array('ProtocoloCancelamento')), false);
            $prot = $this->Protocolo->find('first', array('conditions' => array('usuario_id' => $usuarioId)));
            $this->Protocolo->id = $protocoloCancelamento['Protocolo']['id'];
            $this->Protocolo->saveField('status', 'processando');
            $this->ProtocoloCancelamento->id = $prot['ProtocoloCancelamento']['id'];
            $this->ProtocoloCancelamento->saveField('valor_cancelamento', $_POST['despesa']);
        } else {
            App::import('Component', 'Protocolo');
            $protocolo = new ProtocoloComponent();
            if (!$protocoloCriado = $protocolo->geraProtocoloCancelamento(array('usuario_id' => $usuario['Usuario']['id'], 'grupo' => 'formando', 'usuario_protocolo' => $usuarioLogado['Usuario']['id']))) {
                $retorno['message'] = "Ocorreu um erro ao gerar um protocolo para o cancelamento do seu contrato. Por favor entre em contato conosco.";
            } else {
                $retorno['protocolo'] = $protocoloCriado;
                $despesasAbertas = $this->Despesa->find('all', array('conditions' => array('Despesa.usuario_id' => $usuarioId, 'Despesa.status' => 'aberta')));
                $totalParcelas = 0;
                $gerarDespesa = TRUE;
                foreach ($despesasAbertas as $despesaAberta) {
                    if ($gerarDespesa === TRUE && $despesaAberta['Despesa']['tipo'] == 'cancelamento')
                        $gerarDespesa = FALSE;
                    $this->Despesa->id = $despesaAberta['Despesa']['id'];
                    $this->Despesa->saveField('status', 'cancelada');
                    $this->Despesa->id = null;
                    $totalParcelas = $despesaAberta['Despesa']['totalParcelas'];
                }
                $valorAdesao = $this->Financeiro->obterValorAdesao($usuarioId) + $this->Financeiro->obterValorIgpm($usuarioId);
                $pagoAdesao = $this->Financeiro->obterValorPagoAdesao($usuarioId) + $this->Financeiro->obterValorPagoIgpm($usuarioId);
                if(($pagoAdesao > ($valorAdesao*0.2))){
                    $this->Protocolo->saveField('status', 'finalizado');
                    $saldoCancelamento = 0.00;
                }else{
                    $this->Protocolo->saveField('status', 'processando');
                    $saldoCancelamento = ($valorAdesao*0.2) - $pagoAdesao;
                }
                $protocoloCancelamento = $this->ProtocoloCancelamento->find('first', array('conditions' => array('Protocolo.protocolo' => $protocoloCriado)));
                if ($saldoCancelamento > 10 && $gerarDespesa === TRUE) {
                    $this->Protocolo->id = $protocoloCancelamento['Protocolo']['id'];
                    $this->Protocolo->id = null;
                    $vencimento = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') + 7, date('Y')));
                    $dados = array(
                        'usuario_id' => $usuarioId,
                        'valor' => $saldoCancelamento,
                        'data_cadastro' => DboSource::expression('NOW()'),
                        'data_vencimento' => $vencimento,
                        'tipo' => 'cancelamento',
                        'parcela' => 1,
                        'total_parcelas' => 1,
                        'multa' => 0.00,
                        'status_igpm' => 'nao_virado'
                    );
                    if (!$this->Despesa->save($dados))
                        $this->ProtocoloCancelamento->updateAll(array('observacoes' => "'Gerar boleto de cancelamento do formando'"), array('id' => $protocoloCancelamento['ProtocoloCancelamento']['id']));
                    $this->ProtocoloCancelamento->updateAll(
                            array('ProtocoloCancelamento.ordem_pagamento' => "0",'ProtocoloCancelamento.valor_cancelamento' => $saldoCancelamento), array('ProtocoloCancelamento.id' => $protocoloCancelamento['ProtocoloCancelamento']['id']));
                } else {
                    $this->ProtocoloCancelamento->updateAll(
                            array('ProtocoloCancelamento.ordem_pagamento' => "0"), array('ProtocoloCancelamento.id' => $protocoloCancelamento['ProtocoloCancelamento']['id']));
                    $this->Protocolo->updateAll(
                            array('Protocolo.status' => "'processando'"), array('Protocolo.id' => $protocoloCancelamento['Protocolo']['id']));
                }
                $this->FormandoProfile->updateAll(
                        array('FormandoProfile.situacao' => "'cancelado'"), array('FormandoProfile.id' => $usuario['FormandoProfile']['id']));
                $this->Despesa->id = null;
                $retorno['error'] = false;
            }
        }
        echo json_encode($retorno);
    }
    
    function atendimento_email_justificativa($uid = false) {
        $this->layout = false;
        $usuario = $this->Session->read('Usuario');
        $this->loadModel('ViewFormandos');
        $formando = $this->ViewFormandos->find('first', array('conditions' => array('ViewFormandos.id' => $uid)));
        $fid = $this->FormandoProfile->find('first', array('conditions' => array('usuario_id' => $uid)));
        $this->FormandoProfile->id = $fid['FormandoProfile']['id'];
        $justificativa = $this->data;
        if($this->FormandoProfile->saveField("cancelamento_justificado", 1) &&
            $this->FormandoProfile->saveField("obs_cancelamento", $justificativa)){
            $mailer = new MailerComponent();
            $mailer->enviarEmailCancelamentoJustificativa($usuario, $formando, $justificativa);
        }else{
            $this->Session->setFlash('Usuário não encontrado.', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/area_financeira");
        }
    }
    function cancelar_justificado(){
        $options['joins'] = array(
            array(
                'table' => 'formandos_profile',
                'alias' => 'FormandoProfile',
                'type' => 'inner',
                'conditions' => array(
                    "ViewFormandos.id = FormandoProfile.usuario_id"
                )
            ),
        );
        $options['conditions'] = array('FormandoProfile.cancelamento_justificado' => 1,
                                       'FormandoProfile.situacao' => 'ativo');
        $options['order'] = array('ViewFormandos.codigo_formando' => 'asc');
        $options['fields'] = array(
            'ViewFormandos.*',
            'FormandoProfile.*'
        );
        $options['limit'] = 50;
        $this->paginate['ViewFormandos'] = $options;
        $formandos = $this->paginate('ViewFormandos');
        $this->set('formandos', $formandos);
    }
    
    function cancelar_cem($uid = false){
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $retorno = array('error' => true);
        $usuarioLogado = $this->obterUsuarioLogado();
        $this->Usuario->recursive = 1;
        $this->Usuario->unBindModel(array('hasMany' => array('Despesa')), false);
        $usuario = $this->Usuario->read(null,$uid);
        $this->loadModel('Protocolo');
        $this->loadModel('ProtocoloCancelamento');
        App::import('Component', 'Protocolo');
        $protocolo = new ProtocoloComponent();
        if (!$protocoloCriado = $protocolo->geraProtocoloCancelamento(array('usuario_id' => $usuario['Usuario']['id'], 'grupo' => 'formando', 'usuario_protocolo' => $usuarioLogado['Usuario']['id']))) {
            $retorno['message'] = "Ocorreu um erro ao gerar um protocolo para o cancelamento do seu contrato. Por favor entre em contato conosco.";
        } else {
            $protocoloCancelamento = $this->ProtocoloCancelamento->find('first', array('conditions' => array('Protocolo.protocolo' => $protocoloCriado)));
            $despesasAbertas = $this->Despesa->find('all', array('conditions' => array('Despesa.usuario_id' => $usuario['Usuario']['id'], 'Despesa.tipo' => array('igpm','adesao'))));
            foreach ($despesasAbertas as $despesaAberta) {
                $this->Despesa->id = $despesaAberta['Despesa']['id'];
                $this->Despesa->saveField('status', 'cancelada');
                $this->Despesa->id = null;
            }
            $this->Protocolo->id = $protocoloCancelamento['Protocolo']['id'];
            $this->Protocolo->id = null;
            $valorAdesao = $this->Financeiro->obterValorAdesao($usuario['Usuario']['id']) + $this->Financeiro->obterValorIgpm($usuario['Usuario']['id']);
            $pagoAdesao = $this->Financeiro->obterValorPagoAdesao($usuario['Usuario']['id']) + $this->Financeiro->obterValorPagoIgpm($usuario['Usuario']['id']);
            if($pagoAdesao)
                $saldoCancelamento = $valorAdesao - $pagoAdesao;
            else
                $saldoCancelamento = $valorAdesao;
            $vencimento = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') + 15, date('Y')));
            $dados = array(
                'usuario_id' => $usuario['Usuario']['id'],
                'valor' => $saldoCancelamento,
                'data_cadastro' => DboSource::expression('NOW()'),
                'data_vencimento' => $vencimento,
                'tipo' => 'cancelamento',
                'parcela' => 1,
                'total_parcelas' => 1,
                'multa' => 0.00,
                'status_igpm' => 'nao_virado'
            );
            if ($this->Despesa->save($dados)){
                $this->ProtocoloCancelamento->id = $protocoloCancelamento['ProtocoloCancelamento']['id'];
                $this->ProtocoloCancelamento->updateAll(array('observacao' => "'Gerar boleto de cancelamento do formando'"), array('ProtocoloCancelamento.id' => $protocoloCancelamento['ProtocoloCancelamento']['id']));
            }
            $this->FormandoProfile->id = $usuario['FormandoProfile']['id'];
            $this->FormandoProfile->saveField('obs_cancelamento', $usuario['FormandoProfile']['obs_cancelamento'] . ' - Cancelamento com devolução de 100% do valor.');
            $this->Protocolo->id = $protocoloCancelamento['Protocolo']['id'];
            $this->Protocolo->saveField('status', 'finalizado');
            $this->Protocolo->id = null;
            $this->ProtocoloCancelamento->updateAll(
                    array('ProtocoloCancelamento.ordem_pagamento' => "0"), array('ProtocoloCancelamento.id' => $protocoloCancelamento['ProtocoloCancelamento']['id'],
                        'ProtocoloCancelamento.observacao' => NULL, 'ProtocoloCancelamento.valor_cancelamento' => $saldoCancelamento));
            $this->FormandoProfile->updateAll(
                    array('FormandoProfile.situacao' => "'cancelado'"), array('FormandoProfile.id' => $usuario['FormandoProfile']['id']));
            $this->Despesa->id = null;
            $this->FormandoProfile->id = null;
            $this->Mailer->enviarEmailCancelamentoJustificadoFormando($usuario);
            $this->Session->setFlash('Formando cancelado com sucesso.',
                    'metro/flash/success');
        }
    }
    
    function cancelar_vinte($uid = false){
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $retorno = array('error' => true);
        $usuarioLogado = $this->obterUsuarioLogado();
        $usuario = $this->Usuario->read(null,$uid);
        $this->Usuario->recursive = 1;
        $this->Usuario->unBindModel(array('hasMany' => array('Despesa')), false);
        $this->loadModel('Protocolo');
        $this->loadModel('ProtocoloCancelamento');
        App::import('Component', 'Protocolo');
        $protocolo = new ProtocoloComponent();
        if (!$protocoloCriado = $protocolo->geraProtocoloCancelamento(array('usuario_id' => $usuario['Usuario']['id'], 'grupo' => 'formando', 'usuario_protocolo' => $usuarioLogado['Usuario']['id']))) {
            $retorno['message'] = "Ocorreu um erro ao gerar um protocolo para o cancelamento do seu contrato. Por favor entre em contato conosco.";
        } else {
            $retorno['protocolo'] = $protocoloCriado;
            $despesasAbertas = $this->Despesa->find('all', array('conditions' => array('Despesa.usuario_id' => $usuario['Usuario']['id'], 'Despesa.tipo' => 'adesao')));
            $totalParcelas = 0;
            $gerarDespesa = TRUE;
            foreach ($despesasAbertas as $despesaAberta) {
                if ($gerarDespesa === TRUE && $despesaAberta['Despesa']['tipo'] == 'cancelamento')
                    $gerarDespesa = FALSE;
                $this->Despesa->id = $despesaAberta['Despesa']['id'];
                $this->Despesa->saveField('status', 'cancelada');
                $this->Despesa->id = null;
                $totalParcelas = $despesaAberta['Despesa']['total_parcelas'];
            }
            $valorAdesao = $this->Financeiro->obterValorAdesao($usuario['Usuario']['id']) + $this->Financeiro->obterValorIgpm($usuario['Usuario']['id']);
            $pagoAdesao = $this->Financeiro->obterValorPagoAdesao($usuario['Usuario']['id']) + $this->Financeiro->obterValorPagoIgpm($usuario['Usuario']['id']);
            $saldoCancelamento = ($valorAdesao-$pagoAdesao)*0.2;
            if(($pagoAdesao > ($valorAdesao*0.2))){
                $this->Protocolo->saveField('status', 'finalizado');
                $saldoCancelamento = 0.00;
            }else{
                $this->Protocolo->saveField('status', 'processando');
                $saldoCancelamento = ($valorAdesao*0.2) - $pagoAdesao;
            }
            $protocoloCancelamento = $this->ProtocoloCancelamento->find('first', array('conditions' => array('Protocolo.protocolo' => $protocoloCriado)));
            if ($saldoCancelamento > 10 && $gerarDespesa === TRUE) {
                $this->Protocolo->id = $protocoloCancelamento['Protocolo']['id'];
                $this->Protocolo->saveField('status', 'pendente');
                $this->Protocolo->id = null;
                $vencimento = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') + 15, date('Y')));
                $dados = array(
                    'usuario_id' => $usuario['Usuario']['id'],
                    'valor' => $saldoCancelamento,
                    'data_cadastro' => DboSource::expression('NOW()'),
                    'data_vencimento' => $vencimento,
                    'tipo' => 'cancelamento',
                    'parcela' => 1,
                    'total_parcelas' => 1,
                    'multa' => 0.00,
                    'status_igpm' => 'nao_virado'
                );
                if ($this->Despesa->save($dados)){
                    $this->ProtocoloCancelamento->id = $protocoloCancelamento['ProtocoloCancelamento']['id'];
                    $this->ProtocoloCancelamento->updateAll(array('observacao' => "'Gerar boleto de cancelamento do formando'"), array('ProtocoloCancelamento.id' => $protocoloCancelamento['ProtocoloCancelamento']['id']));
                }
            } else {
                $this->ProtocoloCancelamento->updateAll(
                        array('ProtocoloCancelamento.ordem_pagamento' => "0"), array('ProtocoloCancelamento.id' => $protocoloCancelamento['ProtocoloCancelamento']['id'],
                            'ProtocoloCancelamento.observacao' => NULL));
                $this->Protocolo->updateAll(
                        array('Protocolo.status' => "'processando'"), array('Protocolo.id' => $protocoloCancelamento['Protocolo']['id']),array('Protocolo.usuario_id' => $usuario['Usuario']['id']));
            }
            $this->FormandoProfile->updateAll(
                    array('FormandoProfile.situacao' => "'cancelado'"), array('FormandoProfile.usuario_id' => $usuario['Usuario']['id']));
            $this->Despesa->id = null;
            $this->Mailer->enviarEmailCancelamentoJustificadoFormando($usuario);
            $retorno['error'] = false;
            $this->Session->setFlash('Formando cancelado com sucesso.',
                    'metro/flash/success');
        }
    }
    
    function cancelar_isento($uid = false){
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $retorno = array('error' => true);
        $usuarioLogado = $this->obterUsuarioLogado();
        $this->Usuario->recursive = 1;
        $this->Usuario->unBindModel(array('hasMany' => array('Despesa')), false);
        $usuario = $this->Usuario->read(null,$uid);
        $this->loadModel('Protocolo');
        $this->loadModel('ProtocoloCancelamento');
        App::import('Component', 'Protocolo');
        $protocolo = new ProtocoloComponent();
        if (!$protocoloCriado = $protocolo->geraProtocoloCancelamento(array('usuario_id' => $usuario['Usuario']['id'], 'grupo' => 'formando', 'usuario_protocolo' => $usuarioLogado['Usuario']['id']))) {
            $retorno['message'] = "Ocorreu um erro ao gerar um protocolo para o cancelamento do seu contrato. Por favor entre em contato conosco.";
        } else {
            $despesasAbertas = $this->Despesa->find('all', array('conditions' => array('Despesa.usuario_id' => $usuario['Usuario']['id'], 'Despesa.tipo' => array('igpm','adesao'))));
            $totalParcelas = 0;
            foreach ($despesasAbertas as $despesaAberta) {
                $this->Despesa->id = $despesaAberta['Despesa']['id'];
                $this->Despesa->saveField('status', 'cancelada');
                $this->Despesa->id = null;
            }
            $pagoAdesao = $this->Financeiro->obterValorPagoAdesao($usuario['Usuario']['id']) + $this->Financeiro->obterValorPagoIgpm($usuario['Usuario']['id']);
            $this->FormandoProfile->id = $usuario['FormandoProfile']['id'];
            $this->FormandoProfile->saveField('obs_cancelamento', $usuario['FormandoProfile']['obs_cancelamento'] . ' - Cancelamento com devolução de 100% do valor.');
            $protocoloCancelamento = $this->ProtocoloCancelamento->find('first', array('conditions' => array('Protocolo.protocolo' => $protocoloCriado)));
            $this->Protocolo->id = $protocoloCancelamento['Protocolo']['id'];
            $this->Protocolo->saveField('status', 'pendente');
            $this->Protocolo->id = null;
            $this->ProtocoloCancelamento->updateAll(
                    array('ProtocoloCancelamento.ordem_pagamento' => "1"), array('ProtocoloCancelamento.id' => $protocoloCancelamento['ProtocoloCancelamento']['id'],
                        'ProtocoloCancelamento.observacao' => NULL, 'ProtocoloCancelamento.valor_cancelamento' => $pagoAdesao));
            $this->FormandoProfile->updateAll(
                    array('FormandoProfile.situacao' => "'cancelado'"), array('FormandoProfile.id' => $usuario['FormandoProfile']['id']));
            $this->Despesa->id = null;
            $this->FormandoProfile->id = null;
            $this->Mailer->enviarEmailCancelamentoJustificadoFormando($usuario);
            $this->Session->setFlash('Formando cancelado com sucesso.',
                    'metro/flash/success');
        }
    }
    
    function atendimento_listar_dados_bancarios(){
        $options['joins'] = array(
            array(
                'table' => 'protocolos',
                'alias' => 'Protocolo',
                'type' => 'inner',
                'conditions' => array(
                    "ViewFormandos.id = Protocolo.usuario_id"
                )
            ),
            array(
                'table' => 'protocolos_cancelamentos',
                'alias' => 'ProtocoloCancelamento',
                'type' => 'inner',
                'conditions' => array(
                    "Protocolo.id = ProtocoloCancelamento.protocolo_id"
                )
            ),
        );
        $options['conditions'] = array('Protocolo.status' => 'pendente',
                                       'ProtocoloCancelamento.valor_cancelamento >' => 0,
                                       'ProtocoloCancelamento.ordem_pagamento' => 1);
        $options['order'] = array('ViewFormandos.codigo_formando' => 'asc');
        $options['fields'] = array(
            'ViewFormandos.*',
            'Protocolo.*',
            'ProtocoloCancelamento.*'
        );
        $options['limit'] = 50;
        $this->paginate['ViewFormandos'] = $options;
        $formandos = $this->paginate('ViewFormandos');
        $this->set('formandos', $formandos);
    }
    
    function atendimento_dados_bancarios($uid = false){
        $this->layout = false;
       // Configure::write(array('debug' => 0));
        $this->loadModel('Protocolo');
        $this->loadModel('ProtocoloCancelamento');
        $formando = $uid;
        $this->Protocolo->bindModel(array('hasOne' => array('ProtocoloCancelamento')), false);
        $protocolo = $this->Protocolo->find('first', array('conditions' => array('usuario_id' => $formando)));
        if($protocolo['ProtocoloCancelamento']['banco'] == null){
            if($this->data){
                $this->Protocolo->id = $protocolo['Protocolo']['id'];
                $this->ProtocoloCancelamento->id = $protocolo['ProtocoloCancelamento']['id'];
                if($this->ProtocoloCancelamento->save($this->data)){
                    $this->Protocolo->saveField('status', 'pendente');
                    $this->Session->setFlash('Dados enviados com Sucesso.',
                        'metro/flash/success');
                }else{
                    $this->Session->setFlash('Um erro ocorreu durante o envio dos dados.', 'metro/flash/error');
                }
                
            }
        }else{
            $this->Session->setFlash('Seus dados para recebimento já foram enviados. Aguarde enquanto o setor financeiro da continuidade ao processo.', 'metro/flash/success');
        }
        $this->set('protocolo', $protocolo);
        $this->set('formando', $formando);
        $this->render('atendimento_dados_bancarios');
    }
    
    function formando_dados_bancarios(){
        $this->layout = false;
        Configure::write(array('debug' => 0));
        $this->loadModel('Protocolo');
        $this->loadModel('ProtocoloCancelamento');
        $formando = $this->obterUsuarioLogado();
        $this->Protocolo->bindModel(array('hasOne' => array('ProtocoloCancelamento')), false);
        $protocolo = $this->Protocolo->find('first', array('conditions' => array('usuario_id' => $formando['FormandoProfile']['usuario_id'])));
        if($protocolo['ProtocoloCancelamento']['banco'] == null){
            if($this->data){
                $this->Protocolo->id = $protocolo['Protocolo']['id'];
                $this->ProtocoloCancelamento->id = $protocolo['ProtocoloCancelamento']['id'];
                if($this->ProtocoloCancelamento->save($this->data)){
                    $this->Protocolo->saveField('status', 'pendente');
                    $this->Session->setFlash('Dados enviados com Sucesso.',
                        'metro/flash/success');
                }else{
                    $this->Session->setFlash('Um erro ocorreu durante o envio dos dados.', 'metro/flash/error');
                }
                
            }
        }else{
            $this->Session->setFlash('Seus dados para recebimento já foram enviados. Aguarde enquanto o setor financeiro da continuidade ao processo.', 'metro/flash/success');
        }
        $this->set('protocolo', $protocolo);
        $this->set('formando', $formando);
    }
    
    function financeiro_deposito_formando(){
        $this->layout = false;
        Configure::write(array('debug' => 0));
        $this->loadModel('Protocolo');
        $this->loadModel('ProtocoloCancelamento');
        $this->Protocolo->bindModel(array('hasOne' => array('ProtocoloCancelamento')), false);
        $protocolos = $this->Protocolo->find('all', array('conditions' => array('tipo' => 'cancelamento', 'status' => 'pendente')));
        $this->set('protocolos', $protocolos);
    }
    
    function deposito_formando_finalizar($uid = false){
        $this->layout = false;
        Configure::write(array('debug' => 0));
        $this->loadModel('Protocolo');
        $this->loadModel('ProtocoloCancelamento');
        $this->Protocolo->bindModel(array('hasOne' => array('ProtocoloCancelamento')), false);
        $protocolo = $this->Protocolo->find('first', array('conditions' => array('usuario_id' => $uid)));
        $this->Protocolo->id = $protocolo['Protocolo']['id'];
        if($this->Protocolo->saveField('status', 'finalizado')){
            $this->ProtocoloCancelamento->id = $protocolo['ProtocoloCancelamento']['id'];
            $this->ProtocoloCancelamento->saveField('ordem_pagamento', '0');
            $this->Session->setFlash('Cancelamento finalizado com sucesso',
                'metro/flash/success');
        }else{
            $this->Session->setFlash('Um erro ocorreu durante o envio dos dados.', 'metro/flash/error');
        }
        $this->render('deposito_formando_finalizar');
    }

    function formando_parcelar_igpm() {
        $usuario = $this->Session->read('Usuario');
        $this->atendimento_parcelar_igpm($usuario['Usuario']['id']);
    }

    function atendimento_parcelar_igpm($usuarioId) {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        if (isset($_POST['parcelas'])) {
            $formando = $this->ViewFormandos->find('first', array('conditions' => array('id' => $usuarioId)));
            $parcelas = $_POST['parcelas'];
            App::import('Component', 'Igpm');
            $igpm = new IgpmComponent();
            $despesa = $igpm->obterDespesa($formando['ViewFormandos']);
            if (!empty($despesa)) {
                $saldo = $despesa['Despesa']['valor'] - $igpm->obterTotalPago($formando['ViewFormandos']);
                $valorParcela = $saldo / $parcelas;
                App::import('Component', 'Boleto');
                $boleto = new BoletoComponent();
                $listaParcelas = array();
                $mensagem = "<strong>Caro formando</strong>, você parcelou o reajuste de igpm referente às parcelas:<br /><br />";
                $meses = 1;
                for ($a = 0; $a < $parcelas; $a++) {
                    if ($igpm->gerarParcela($formando['ViewFormandos'], $valorParcela, date('Y-m-d', strtotime("+{$meses} month")))) {
                        $parcela = $this->Pagamento->find('first', array('conditions' => array('Pagamento.id' => $this->Pagamento->getLastInsertId())));
                        $boleto->dadosboleto["data_vencimento"] = date('d/m/Y', strtotime("+{$meses} month"));
                        $boleto->dadosboleto["valor_boleto"] = number_format($parcela['Pagamento']['valor_nominal'], 2, ',', '.');
                        $boleto->dadosboleto["numero_documento"] = $parcela['Pagamento']['codigo'];
                        $boleto->dadosboleto["nosso_numero"] = $parcela['Pagamento']['codigo'];
                        $listaParcelas[] = array(
                            'valor' => $parcela['Pagamento']['valor_nominal'],
                            'codigo' => $parcela['Pagamento']['id'],
                            'linha_digitavel' => $boleto->obterLinhaDigitavel()
                        );
                    }
                    $meses = $meses + 1;
                }
                $despesas = $this->Despesa->find('all', array('conditions' =>
                    array('status' => 'paga', 'correcao_igpm > 0', "status_igpm <> 'pago'",
                        'usuario_id' => $usuarioId)));
                foreach ($despesas as $despesa)
                    $mensagem.= "<strong>{$despesa['Despesa']['parcela']}</strong>, ";
                $mensagem = substr($mensagem, 0, -2);
                $mensagem.= "<br /><br />Você poderá pagar as parcelas pela internet usando a linha digitável,<br />";
                $mensagem.= "ou se preferir poderá imprimir o boleto e pagar até a data de vencimento em qualquer agência bancária<br /><br />";
                $mensagem.= "*A data de vencimento dos boletos será dia posterior à sua emissão";
                echo json_encode(array('parcelas' => $listaParcelas, 'mensagem' => $mensagem));
            } else {
                echo json_encode(array());
            }
        } else {
            echo json_encode(array());
        }
    }
    
    function atendimento_parcelar_extra($usuarioId) {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        if (isset($_POST['parcelas'])) {
            $formando = $this->ViewFormandos->find('first', array('conditions' => array('id' => $usuarioId)));
            $parcelas = $_POST['parcelas'];
            $despesa = $extra->obterDespesa($formando['ViewFormandos']);
            if (!empty($despesa)) {
                $saldo = $despesa['Despesa']['valor'] - $extra->obterTotalPago($formando['ViewFormandos']);
                $valorParcela = $saldo / $parcelas;
                App::import('Component', 'Boleto');
                $boleto = new BoletoComponent();
                $listaParcelas = array();
                $mensagem = "<strong>Caro formando</strong>, você parcelou o reajuste de igpm referente às parcelas:<br /><br />";
                for ($a = 0; $a < $parcelas; $a++) {
                    if ($extra->gerarParcela($formando['ViewFormandos'], $valorParcela)) {
                        $parcela = $this->Pagamento->find('first', array('conditions' => array('Pagamento.id' => $this->Pagamento->getLastInsertId())));
                        $boleto->dadosboleto["data_vencimento"] = date('d/m/Y', strtotime("+1 day"));
                        $boleto->dadosboleto["valor_boleto"] = number_format($parcela['Pagamento']['valor_nominal'], 2, ',', '.');
                        $boleto->dadosboleto["numero_documento"] = $parcela['Pagamento']['codigo'];
                        $boleto->dadosboleto["nosso_numero"] = $parcela['Pagamento']['codigo'];
                        $listaParcelas[] = array(
                            'valor' => $parcela['Pagamento']['valor_nominal'],
                            'codigo' => $parcela['Pagamento']['id'],
                            'linha_digitavel' => $boleto->obterLinhaDigitavel()
                        );
                    }
                }
            }
        }
    }
    
    function formando_parcelar_restante_igpm() {
        $usuario = $this->Session->read('Usuario');
        $this->atendimento_parcelar_restante_igpm($usuario['Usuario']['id']);
    }
    
    function atendimento_parcelar_restante_igpm($usuarioId) {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        if (isset($_POST['parcelas'])) {
            $formando = $this->ViewFormandos->find('first',
                    array('conditions' => array('id' => $usuarioId)));
            $parcelas = $_POST['parcelas'];
            App::import('Component', 'Igpm');
            $igpm = new IgpmComponent();
            $despesa = $igpm->obterDespesa($formando['ViewFormandos']);
            if (!empty($despesa)) {
                $igpmComponent = new IgpmComponent();
                $parcelasIgpm = $igpmComponent->obterParcelas($formando['ViewFormandos']);
                $totalParcelasIgpm = 0;
                foreach ($parcelasIgpm as $indice => $parcelaIgpm)
                    $totalParcelasIgpm+= $parcelaIgpm['Pagamento']['valor_nominal'];
                $saldo = $despesa['Despesa']['valor'] - $totalParcelasIgpm;
                $valorParcela = $saldo / $parcelas;
                App::import('Component', 'Boleto');
                $boleto = new BoletoComponent();
                $listaParcelas = array();
                $meses = 1;
                for ($a = 0; $a < $parcelas; $a++) {
                    if ($igpmComponent->gerarParcela($formando['ViewFormandos'], $valorParcela, date('Y-m-d', strtotime("+{$meses} month")))) {
                        $parcela = $this->Pagamento->find('first', array('conditions' => array('Pagamento.id' => $this->Pagamento->getLastInsertId())));
                        $boleto->dadosboleto["data_vencimento"] = date('d/m/Y', strtotime("+{$meses} month"));;
                        $boleto->dadosboleto["valor_boleto"] = number_format($parcela['Pagamento']['valor_nominal'], 2, ',', '.');
                        $boleto->dadosboleto["numero_documento"] = $parcela['Pagamento']['codigo'];
                        $boleto->dadosboleto["nosso_numero"] = $parcela['Pagamento']['codigo'];
                        $listaParcelas[] = array(
                            'valor' => $parcela['Pagamento']['valor_nominal'],
                            'codigo' => $parcela['Pagamento']['id'],
                            'linha_digitavel' => $boleto->obterLinhaDigitavel()
                        );
                    }
                    $meses = $meses + 1;
                }
                echo json_encode(array('parcelas' => $listaParcelas));
            } else {
                echo json_encode(array());
            }
        } else {
            echo json_encode(array());
        }
    }
    
    function atendimento_dados($usuarioId,$tipo = false) {
        $this->layout = false;
        $formando = $this->ViewFormandos->read(null,$usuarioId);
        $this->set('formando',$formando['ViewFormandos']);
        $usuario = $this->obterUsuarioLogado();
        $this->set('usuario', $usuario);
        $this->Turma->unbindModelAll();
        $turma = $this->Turma->read(null,$formando['ViewFormandos']['turma_id']);
        $this->set('turma', $turma['Turma']);
        $this->loadModel('UsuarioAcesso');
        $this->UsuarioAcesso->unbindModelAll();
        $acesso = $this->UsuarioAcesso->find('all', array(
                'conditions' => array(
                    'UsuarioAcesso.usuario_id' => $usuarioId
                )
            )
        );
        $this->set('acesso', $acesso);
        if(!$tipo) {
            $this->loadModel('Protocolo');
            $this->Protocolo->recursive = 0;
            $protocolo = $this->Protocolo->find('first',array(
                'conditions' => array(
                    'usuario_id' => $usuarioId,
                    'tipo' => 'checkout'
                )
            ));
            $mapa = false;
            if($protocolo) {
                $this->loadModel('EventoMapa');
                $mapa = $this->EventoMapa->find('first',array(
                    'conditions' => array(
                        'Evento.turma_id' => $turma['Turma']['id']
                    )
                ));
            }
            $this->set('mapa',$mapa);
            $this->set('protocolo',$protocolo);
        } elseif($tipo == 'adesao') {
            $despesas = $this->Financeiro->obterDespesasAdesao($usuarioId,true);
            $this->set('despesas',$despesas);
            $igpm = $this->Financeiro->obterDespesaIgpm($usuarioId);
            $this->set('igpm',$igpm);
            $cancelamento = $this->Financeiro->obterDespesaCancelamento($usuarioId);
            $this->set('cancelamento',$cancelamento);
            $this->render('dados_formandos/adesao');
        } elseif($tipo == 'resumo') {
            $parcelamento = false;
            if(!empty($formando['ViewFormandos']['parcelamento_id'])) {
                $this->loadModel('Parcelamento');
                $parcelamento = $this->Parcelamento->read(null,$formando['ViewFormandos']['parcelamento_id']);
            }
            $this->set('parcelamento',$parcelamento);
            $totalAdesao = $this->Financeiro->obterValorAdesao($usuarioId);
            $totalExtras = $this->Financeiro->obterValorExtras($usuarioId);
            $totalIgpm = $this->Financeiro->obterValorIgpm($usuarioId);
            $totalPago = $this->Financeiro->obterTotalPago($usuarioId);
            $saldo = $totalPago-($totalExtras+$totalIgpm+$totalAdesao);
            $this->set('totalAdesao',$totalAdesao);
            $this->set('totalIGPM',$totalIgpm);
            $this->set('totalExtras',$totalExtras);
            $this->set('totalPago',$totalPago);
            $this->set('saldoAtual',$saldo);
            $this->render('dados_formandos/resumo');
        } elseif($tipo == 'igpm') {
            $textoParcelas = '';
            $this->Despesa->unbindModelAll();
            $despesasRecriadas = $this->Despesa->find('first',array(
                'conditions' => array(
                    'Despesa.usuario_id' => $usuarioId,
                    'Despesa.status_igpm' => 'recriado',
                    'Despesa.status not' => array('cancelada','renegociada')
                ),
                'fields' => array(
                    "group_concat(Despesa.parcela) as despesas"
                ),
                'order' => array('Despesa.parcela' => 'asc')
            ));
            if(!empty($despesasRecriadas[0]['despesas']))
                $textoParcelas = "Esse valor é referente a quitação dos valores" .
                    "de IGPM dos boletos que foram pagos sem o devido ajuste.<br />" .
                    "No seu caso, corresponde a(s) parcela(s) {$despesasRecriadas[0]['despesas']}";
            $this->set('textoParcelas',$textoParcelas);
            $despesaIgpm = $this->Financeiro->obterDespesaIgpm($usuarioId);
            $this->set('igpm',$despesaIgpm);
            $this->Pagamento->unbindModelAll();
            $parcelas = $this->Pagamento->find('all',array(
                'conditions' => array(
                    'usuario_id' => $usuarioId,
                    'tipo' => 'boleto_igpm'
                )
            ));
            $dataAssinaturaContrato = date('d/m/Y',strtotime($turma['Turma']['data_assinatura_contrato']));
            $arrayData = explode("/",$dataAssinaturaContrato);
            $arrayData[2]++;
            $dataUmAnoAposAssinatura = implode("/",$arrayData);
            $arrayData[2]++;
            $dataDoisAnosAposAssinatura = implode("/",$arrayData);
            $textoIGPM = "Conforme consta em contrato, os valores s&atilde;o corrigidos de 12 em 12 meses a partir da assinatura do contrato entre";
            $textoIGPM.= " a comiss&atilde;o de formatura e a &Aacute;S formaturas. O &iacute;ndice utilizado &eacute; o IGPM.";
            $textoIGPM.= " O seu contrato foi assinado entre a &Aacute;S e a comiss&atilde;o em $dataAssinaturaContrato";
            $textoIGPM.= ".Sendo assim, caso voc&ecirc; tenha aderido com o valor inicial, todas as parcelas n&atilde;o pagas at&eacute; $dataUmAnoAposAssinatura";
            $textoIGPM.= " sofrer&atilde;o reajustes. Caso voc&ecirc; j&aacute; tenha aderido com o valor corrigido, as parcelas s&oacute; ser&atilde;o corrigidas";
            $textoIGPM.= " a partir do pr&oacute;ximo ciclo de 12 meses, ou seja ap&oacute;s $dataDoisAnosAposAssinatura, caso voc&ecirc; possua valores em aberto ap&oacute;s essa data.";
            $this->set('textoIGPM', $textoIGPM);
            $this->set('parcelas',$parcelas);
            $this->render('dados_formandos/igpm');
        } elseif($tipo == 'extras') {
            $this->loadModel("ViewRelatorioExtras");
            $extras = $this->ViewRelatorioExtras->find('all',array(
                'conditions' => array('usuario_id' => $usuarioId),
                'order' => array('data_compra' => 'asc')
            ));
            $campanhas = array();
            foreach($extras as $extra) {
                $campanhaId = $extra['ViewRelatorioExtras']['campanha_id'];
                $campanhaUsuarioId = $extra['ViewRelatorioExtras']['campanhas_usuario_id'];
                if(!isset($campanhas[$campanhaId]))
                    $campanhas[$campanhaId] = array(
                        'nome' => $extra['ViewRelatorioExtras']['campanha'],
                        'compras' => array()
                    );
                if(!isset($campanhas[$campanhaId]['compras'][$campanhaUsuarioId]))
                    $campanhas[$campanhaId]['compras'][$campanhaUsuarioId] = array(
                        'data_compra' => $extra['ViewRelatorioExtras']['data_compra'],
                        'valor_campanha' => $extra['ViewRelatorioExtras']['valor_campanha'],
                        'valor_pago' => $extra['ViewRelatorioExtras']['valor_pago'],
                        'status' => $extra['ViewRelatorioExtras']['status'],
                        'cancelada' => $extra['ViewRelatorioExtras']['cancelada'],
                        'itens' => array()
                    );
                $campanhas[$campanhaId]['compras'][$campanhaUsuarioId]['itens'][] = array(
                    'evento' => $extra['ViewRelatorioExtras']['evento'],
                    'nome' => $extra['ViewRelatorioExtras']['extra'],
                    'quantidade' => $extra['ViewRelatorioExtras']['quantidade_total']
                );
            }
            foreach($campanhas as &$campanha)
                foreach($campanha['compras'] as $campanhaUsuarioId => &$compra)
                    $compra['despesas'] = $this->Financeiro->obterDespesasExtras($campanhaUsuarioId,true);
            $this->set('campanhas',$campanhas);
            $this->render('dados_formandos/extras');
        } elseif($tipo == 'nao_vinculados') {
            $pagamentos = $this->Financeiro->obterPagamentosNaoVinculados($usuarioId);
            $this->set('pagamentos',$pagamentos);
            if(count($pagamentos) > 0) {
                $lista = array('' => 'Selecione');
                $this->Despesa->unbindModelAll();
                $despesasNaoPagas = $this->Despesa->find('all',array(
                    'conditions' => array(
                        'Despesa.usuario_id' => $usuarioId,
                        'Despesa.status <>' => 'paga',
                        'Despesa.tipo <>' => 'igpm'
                    ),
                    'order' => array('Despesa.id')
                ));
                $this->CampanhasUsuario->bindModel(array('belongsTo' => array('Campanha')),false);
                foreach($despesasNaoPagas as $despesa) {
                    if($despesa['Despesa']['tipo'] == 'adesao') {
                        $lista['Adesao'][$despesa['Despesa']['id']] =
                            date('d/m/Y',strtotime($despesa['Despesa']['data_vencimento'])) .
                            " - R$" . number_format($despesa['Despesa']['valor'], 2, ',', '.');
                    } elseif($despesa['Despesa']['tipo'] == 'extra' &&
                        !empty($despesa['Despesa']['campanhas_usuario_id'])) {
                        $campanhaUsuario = $this->CampanhasUsuario->read(null,$despesa['Despesa']['campanhas_usuario_id']);
                        if(!empty($campanhaUsuario['Campanha']['nome'])) {
                            $lista[$campanhaUsuario['Campanha']['nome']][$despesa['Despesa']['id']] =
                                date('d/m/Y',strtotime($despesa['Despesa']['data_vencimento'])) .
                                " - R$" . number_format($despesa['Despesa']['valor'], 2, ',', '.');
                        } else {
                            $lista['Extra'][$despesa['Despesa']['id']] =
                                date('d/m/Y',strtotime($despesa['Despesa']['data_vencimento'])) .
                                " - R$" . number_format($despesa['Despesa']['valor'], 2, ',', '.');
                        }
                    } else {
                        $lista['Extra'][$despesa['Despesa']['id']] =
                            date('d/m/Y',strtotime($despesa['Despesa']['data_vencimento'])) .
                            " - R$" . number_format($despesa['Despesa']['valor'], 2, ',', '.');
                    }
                }
                $this->set('despesas',$lista);
            }
            $this->render('dados_formandos/nao_vinculados');
        }elseif($tipo == 'inserir_despesas') {
            $this->set('totalPago', 0);
            $this->render('dados_formandos/inserir_despesas');
        }elseif($tipo == 'inserir_pagamentos') {
            $this->render('dados_formandos/inserir_pagamentos');
        }
    }
    
    function super_dados($id, $tipo = false) {
        $this->atendimento_dados($id, $tipo);
    }
    
    function financeiro_dados($id, $tipo = false) {
        $this->atendimento_dados($id, $tipo);
    }
    
    function app_dados() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('erro' => true);
        if(isset($this->data['usuario']) && isset($this->data['tipo'])) {
            $usuario = $this->Usuario->read(null,$this->data['usuario']);
            $turma = @$this->Turma->read(null,$usuario['ViewFormandos']['turma_id']);
            if(!$usuario) {
                $json['mensagem'] = 'Usuário não encontrado';
            } elseif(!$turma) {
                $json['mensagem'] = 'Turma não encontrada';
            } elseif($this->data['tipo'] == 'resumo') {
                $totalAdesao = $this->Financeiro->obterValorAdesao($usuario['Usuario']['id']);
                $totalPagoAdesao = $this->Financeiro->obterValorPagoAdesao($usuario['Usuario']['id']);
                $totalExtras = $this->Financeiro->obterValorExtras($usuario['Usuario']['id']);
                $totalPagoExtras = $this->Financeiro->obterValorPagoCampanha($usuario['Usuario']['id']);
                $totalIgpm = $this->Financeiro->obterValorIgpm($usuario['Usuario']['id']);
                $totalPagoIgpm = $this->Financeiro->obterValorPagoIgpm($usuario['Usuario']['id']);
                $totalPago = $this->Financeiro->obterTotalPago($usuario['Usuario']['id']);
                $saldo = $totalPago-($totalExtras+$totalIgpm+$totalAdesao);
                $json = array(
                    'erro' => false,
                    'dados' => array(
                        'adesao' => array(
                            'valor' => $totalAdesao,
                            'pago' => $totalPagoAdesao
                        ),
                        'extras' => array(
                            'valor' => $totalExtras,
                            'pago' => $totalPagoExtras
                        ),
                        'igpm' => array(
                            'valor' => $totalIgpm,
                            'pago' => $totalPagoIgpm
                        ),
                        'saldo' => $saldo,
                        'pago' => $totalPago
                    )
                );
            } elseif($this->data['tipo'] == 'adesao') {
                $adesoes = $this->Despesa->find('all', array(
                    'conditions' => array(
                        'Despesa.usuario_id' => $usuario['Usuario']['id'],
                        'Despesa.tipo' => 'adesao',
                        'Despesa.status not ' => array('cancelada','renegociada')
                    )
                ));
                $despesas = array();
                foreach($adesoes as $adesao) {
                    $saldo = $adesao['Despesa']['valor'];
                    if($adesao['Despesa']['status_igpm'] != 'recriado')
                        $saldo+= $adesao['Despesa']['correcao_igpm'];
                    if($adesao['Despesa']['status'] == 'paga') {
                        $adesao['Pagamento'] = $this->obterPagamento($adesao["Despesa"]['id']);
                        $adesao['Despesa']['valor_pago'] = min(array(
                            $adesao['Pagamento']['valor_nominal'],
                            $adesao['Pagamento']['valor_pago'],
                            $saldo
                        ));
                    } else {
                        $pagamentoBoleto = $this->obterPagamentoBoleto($adesao['Despesa']['id']);
                        if($pagamentoBoleto) {
                            $pagamento = $this->Pagamento->read(null,$pagamentoBoleto);
                            if($pagamento) {
                                App::import('Component', 'Boleto');
                                $boleto = new BoletoComponent();
                                $this->obterCodigoBoleto($boleto,$pagamento,$usuario,$turma);
                                $adesao['Despesa']['codigo_barras'] = $boleto->obterLinhaDigitavel();
                            }
                        }
                        $adesao['Despesa']['valor_pago'] = 0;
                    }
                    $adesao['Despesa']['saldo_parcela'] = $saldo - $adesao['Despesa']['valor_pago'];
                    $despesas[] = $adesao['Despesa'];
                }
                $igpm = $this->Despesa->find('first', array(
                    'conditions' => array(
                        'Despesa.usuario_id' => $usuario['Usuario']['id'],
                        'Despesa.tipo' => 'igpm'
                    )
                ));
                if($igpm) {
                    App::import('Component', 'Igpm');
                    $igpmComponent = new IgpmComponent();
                    $igpm = $igpmComponent->obterDespesa($usuario['ViewFormandos']);
                    $totalPagoIgpm = $igpmComponent->obterTotalPago($usuario['ViewFormandos']);
                    $igpm['Despesa']['saldo'] = $igpm['Despesa']['valor']-$totalPagoIgpm;
                    $igpm["Despesa"]['valor_pago'] = $totalPagoIgpm;
                    $igpm = $igpm['Despesa'];
                }
                $json = array(
                    'erro' => false,
                    'despesas' => $despesas,
                    'igpm' => $igpm
                );
            } elseif($tipo == 'campanhas') {
                $despesas = $this->Despesa->find('all', array(
                    'conditions' => array(
                        'Despesa.usuario_id' => $usuario['Usuario']['id'],
                        'Despesa.tipo' => 'extra',
                        'Despesa.status not ' => array('cancelada','renegociada')
                    )
                ));
                $extras = array();
                foreach($despesas as $despesa) {
                    if($despesa['Despesa']['status'] == 'paga') {
                        $despesa['Pagamento'] = $this->obterPagamento($despesa["Despesa"]['id']);
                        $despesa['Despesa']['valor_pago'] = min(array(
                            $despesa['Pagamento']['valor_nominal'],
                            $despesa['Pagamento']['valor_pago'],
                            $despesa['Despesa']['valor']
                        ));
                    } else {
                        $despesa['Despesa']['valor_pago'] = 0;
                    }
                    $despesa['Despesa']['saldo_parcela'] = $despesa['Despesa']['valor'] -
                            $despesa['Despesa']['valor_pago'];
                    if(!($despesa['Despesa']['status'] == 'cancelada' && $this->params['prefix'] == 'formando'))
                        $extras[] = $despesa['Despesa'];
                }
                $this->set('despesasExtras',$extras);
                $this->loadModel("ViewRelatorioExtras");
                $relatorioExtras = $this->ViewRelatorioExtras->find('all',array(
                    'conditions' => array('usuario_id' => $usuario['Usuario']['id'])
                ));
                $this->set('resumoExtras', $relatorioExtras);
                $this->render('/elements/formando/area_financeira/extras');
            }
        }
        echo json_encode($json);
    }

    function formando_dados($id = false,$tipo = false, $q = false) {
        $this->set('tipo', $tipo);
        $this->autoLayout = false;
        if(!$id)
            $usuario = $this->Session->read('Usuario');
        else
            $usuario = $this->Usuario->read(null,$id);
        $formando = $this->ViewFormandos->read(null,$usuario['Usuario']['id']);
        $this->set('formando', $formando["ViewFormandos"]);
        if($tipo == 'adesao' && !$q) {
            $adesoes = $this->Despesa->find('all', array(
                'conditions' => array(
                    'Despesa.usuario_id' => $usuario['Usuario']['id'],
                    'Despesa.tipo' => 'adesao',
                    'Despesa.status not ' => array('cancelada','renegociada')
                )
            ));
            $despesas = array();
            foreach($adesoes as $adesao) {
                $saldo = $adesao['Despesa']['valor'];
                if($adesao['Despesa']['status_igpm'] != 'recriado')
                    $saldo+= $adesao['Despesa']['correcao_igpm'];
                if($adesao['Despesa']['status'] == 'paga') {
                    $adesao['Pagamento'] = $this->obterPagamento($adesao["Despesa"]['id']);
                    $adesao['Despesa']['valor_pago'] = min(array(
                        $adesao['Pagamento']['valor_nominal'],
                        $adesao['Pagamento']['valor_pago'],
                        $saldo
                    ));
                } else {
                    $adesao['Despesa']['valor_pago'] = 0;
                }
                $adesao['Despesa']['saldo_parcela'] = $saldo - $adesao['Despesa']['valor_pago'];
                if(!($adesao['Despesa']['status'] == 'cancelada' && $this->params['prefix'] == 'formando'))
                    $despesas[] = $adesao['Despesa'];
            }
            $igpm = $this->Despesa->find('first', array(
                'conditions' => array(
                    'Despesa.usuario_id' => $usuario['Usuario']['id'],
                    'Despesa.tipo' => 'igpm'
                )
            ));
            if($igpm) {
                App::import('Component', 'Igpm');
                $igpmComponent = new IgpmComponent();
                $igpm = $igpmComponent->obterDespesa($formando['ViewFormandos']);
                $totalPagoIgpm = $igpmComponent->obterTotalPago($formando['ViewFormandos']);
                $igpm['Despesa']['saldo'] = $igpm['Despesa']['valor']-$totalPagoIgpm;
                $igpm["Despesa"]['valor_pago'] = $totalPagoIgpm;
                $igpm = $igpm['Despesa'];
            }
            $this->set('adesoes',$despesas);
            $this->set('igpm',$igpm);
            $this->render('/elements/formando/area_financeira/adesao');
        } elseif($tipo == 'resumo' && !$q) {
            $totalAdesao = $this->Financeiro->obterValorAdesao($usuario['Usuario']['id']);
            $totalExtras = $this->Financeiro->obterValorExtras($usuario['Usuario']['id']);
            $totalIgpm = $this->Financeiro->obterValorIgpm($usuario['Usuario']['id']);
            $totalPago = $this->Financeiro->obterTotalPago($usuario['Usuario']['id']);
            $turma = $this->Turma->find('first', array(
                'conditions' => array(
                    'Turma.id' => $formando['ViewFormandos']['turma_id'])));
            $this->set('turma', $turma['Turma']);
            $saldo = $totalPago-($totalExtras+$totalIgpm+$totalAdesao);
            $this->set('totalAdesao',$totalAdesao);
            $this->set('totalIGPM',$totalIgpm);
            $this->set('totalExtras',$totalExtras);
            $this->set('totalPago',$totalPago);
            $this->set('saldoAtual',$saldo);
            $this->render('/elements/formando/area_financeira/resumo');
        } elseif($tipo == 'igpm' && !$q) {
            App::import('Component', 'Igpm');
            $igpmComponent = new IgpmComponent();
            $igpm = $igpmComponent->obterDespesa($formando['ViewFormandos']);
            $totalPagoIgpm = 0;
            if (empty($igpm)) {
                if ($igpmComponent->gerarDespesa($formando['ViewFormandos']))
                    $igpm = $igpmComponent->obterDespesa($formando['ViewFormandos']);
                else
                    $igpm = false;
            }
            if(!empty($igpm)) {
                $igpm = $igpmComponent->obterDespesa($formando['ViewFormandos']);
                $this->Despesa->recursive = 0;
                $despesas = $this->Despesa->find('all',array(
                    'conditions' => array(
                        'Despesa.usuario_id' => $usuario['Usuario']['id'],
                        'Despesa.tipo' => 'adesao',
                        'Despesa.status_igpm' => 'recriado',
                        "Despesa.status <> 'cancelada'"
                    )
                ));
                $parcelasIgpmRecriadas = array();
                foreach($despesas as $despesa)
                    $parcelasIgpmRecriadas[] = $despesa['Despesa']['parcela'];
                if (!empty($parcelasIgpmRecriadas))
                    $textoParcelaIgpm = "Essa parcela é referente a quitação dos valores de IGPM dos boletos " .
                            "que foram pagos sem o devido ajuste. <br />No seu caso, corresponde a(s) parcela(s) " .
                            rtrim(implode(", ", $parcelasIgpmRecriadas), ', ');
                else
                    $textoParcelaIgpm = "";
                $this->set('textoParcelaIgpm', $textoParcelaIgpm);
                $totalPagoIgpm = $igpmComponent->obterTotalPago($formando['ViewFormandos']);
                $parcelasIgpm = $igpmComponent->obterParcelas($formando['ViewFormandos']);
                $totalParcelasIgpm = 0;
                if (!empty($parcelasIgpm)) {
                    App::import('Component', 'Boleto');
                    $boleto = new BoletoComponent();
                    foreach ($parcelasIgpm as $indice => $parcelaIgpm) {
                        $boleto->dadosboleto["data_vencimento"] = date('d/m/Y', strtotime("+1 day"));
                        $boleto->dadosboleto["valor_boleto"] = number_format(
                                $parcelaIgpm['Pagamento']['valor_nominal'], 2, ',', '.');
                        $boleto->dadosboleto["numero_documento"] = $parcelaIgpm['Pagamento']['codigo'];
                        $boleto->dadosboleto["nosso_numero"] = $parcelaIgpm['Pagamento']['codigo'];
                        $parcelasIgpm[$indice]['Pagamento']['linha_digitavel'] = $boleto->obterLinhaDigitavel();
                        $totalParcelasIgpm+= $parcelaIgpm['Pagamento']['valor_nominal'];
                    }
                }
                $this->set('parcelasIgpm', $parcelasIgpm);
                $this->set('totalParcelasIgpm', $totalParcelasIgpm);
                $igpm['Despesa']['saldo'] = $igpm['Despesa']['valor']-$totalPagoIgpm;
                $igpm = $igpm['Despesa'];
            }
            $turma = $this->Turma->find('first', array('conditions' => array('id' => $formando['ViewFormandos']['turma_id'])));
            $dataAssinaturaContrato = date('d/m/Y',strtotime($turma['Turma']['data_assinatura_contrato']));
            $arrayData = explode("/",$dataAssinaturaContrato);
            $arrayData[2]++;
            $dataUmAnoAposAssinatura = implode("/",$arrayData);
            $arrayData[2]++;
            $dataDoisAnosAposAssinatura = implode("/",$arrayData);
            $textoIGPM = "Conforme consta em contrato, os valores s&atilde;o corrigidos de 12 em 12 meses a partir da assinatura do contrato entre";
            $textoIGPM.= " a comiss&atilde;o de formatura e a RK formaturas. O &iacute;ndice utilizado &eacute; o IGPM.";
            $textoIGPM.= " O seu contrato foi assinado entre a RK e a comiss&atilde;o em $dataAssinaturaContrato";
            $textoIGPM.= ". Sendo assim, caso voc&ecirc; tenha aderido com o valor inicial, todas as parcelas n&atilde;o pagas at&eacute; $dataUmAnoAposAssinatura";
            $textoIGPM.= " sofrer&atilde;o reajustes. Caso voc&ecirc; j&aacute; tenha aderido com o valor corrigido, as parcelas s&oacute; ser&atilde;o corrigidas";
            $textoIGPM.= " a partir do pr&oacute;ximo ciclo de 12 meses, ou seja ap&oacute;s $dataDoisAnosAposAssinatura, caso voc&ecirc; possua valores em aberto ap&oacute;s essa data.";
            $this->set('textoIGPM', $textoIGPM);
            $this->set('totalPagoIgpm', $totalPagoIgpm);
            $this->set('igpm', $igpm);
            $this->render('/elements/formando/area_financeira/igpm');
        } elseif($tipo == 'campanhas'  && !$q) {
            $despesas = $this->Despesa->find('all', array(
                'conditions' => array(
                    'Despesa.usuario_id' => $usuario['Usuario']['id'],
                    'Despesa.tipo' => 'extra'
                )
            ));
            $extras = array();
            $cid = NULL;
            foreach($despesas as $i => $despesa) {
                if($despesa['Despesa']['status'] == 'paga') {
                    $despesa['Pagamento'] = $this->obterPagamento($despesa["Despesa"]['id']);
                    $despesa['Despesa']['valor_pago'] = min(array(
                        $despesa['Pagamento']['valor_nominal'],
                        $despesa['Pagamento']['valor_pago'],
                        $despesa['Despesa']['valor']
                    ));
                } else {
                    $despesa['Despesa']['valor_pago'] = 0;
                }
                $despesa['Despesa']['saldo_parcela'] = $despesa['Despesa']['valor'] -
                        $despesa['Despesa']['valor_pago'];
                if(!($despesa['Despesa']['status'] == 'cancelada' && $this->params['prefix'] == 'formando')){
                    $extras[$i] = $despesa['Despesa'];
                    $vencimento = $despesa['Despesa']['data_vencimento'];
                    $hoje = date('Y-m-d');
                    if(date("Y-m-d", strtotime($vencimento)) < date("Y-m-d", strtotime($hoje)) &&
                       $despesa['Despesa']['parcela'] == 1 && $despesa['Despesa']['status'] == 'aberta'){
                        $extras[$i]['conciliacao'] = 1;
                        $cid = $despesa['Despesa']['campanhas_usuario_id'];
                    }
                    if($cid == $despesa['Despesa']['campanhas_usuario_id'])
                        $extras[$i]['conciliacao'] = 1;
                    else
                        $extras[$i]['conciliacao'] = 0;
                }
            }
            $this->set('despesasExtras',$extras);
            $this->loadModel("ViewRelatorioExtras");
            $relatorioExtras = $this->ViewRelatorioExtras->find('all',array(
                'conditions' => array('usuario_id' => $usuario['Usuario']['id'])
            ));
            $this->set('resumoExtras', $relatorioExtras);
            $this->render('/elements/formando/area_financeira/extras');
        } elseif($tipo == 'nao_vinculados'  && !$q) {
            $pagamentos = $this->Pagamento->find('all',array(
                'conditions' => array(
                    'Pagamento.status' => 'pago',
                    'Pagamento.usuario_id' => $usuario['Usuario']['id']
                )
            ));
            $pagamentos_nao_vinculados = array();
            foreach($pagamentos as $pagamento)
                if(sizeof($pagamento['DespesaPagamento']) == 0 &&
                        $pagamento['Pagamento']['tipo'] != 'boleto_igpm')
                    $pagamentos_nao_vinculados[] = $pagamento['Pagamento'];
            $this->set('naoVinculados', $pagamentos_nao_vinculados);
            $this->render('/elements/formando/area_financeira/nao_vinculados');
        }
        $this->autoLayout = false;
    }
    
    private function obterPagamentoPagoDeDespesa($despesa) {
        foreach($despesa['DespesaPagamento'] as $despesaPagamento) {
            if($despesaPagamento['Pagamento']['status'] == 'pago') {
                $despesa['DespesaPagamento'][0] = $despesaPagamento;
                continue;
            }
        }
        return $despesa;
    }

    function formando_index($uid = false) {
        if (!$uid) {
            $usuario = $this->Session->read('Usuario');
            $uid = $usuario['Usuario']['id'];
        }
        $this->ViewFormandos->bindModel(array(
            'belongsTo' => array(
                'Parcelamento' => array('foreignKey' => 'parcelamento_id')
            )
        ), false);
        $formando = $this->ViewFormandos->read(null,$uid);
        $formando["ViewFormandos"]['Parcelamento'] = $formando['Parcelamento'];
        $this->set('formando', $formando["ViewFormandos"]);
        $turma = $this->Turma->read(null,$formando['ViewFormandos']['turma_id']);
        $this->set('turma', $turma);
        $this->Despesa->recursive = 2;
        $cancelado = false;
        if ($formando['ViewFormandos']['situacao'] == 'cancelado') {
            $this->loadModel('Protocolo');
            $protocolo = $this->Protocolo->find('first', array('conditions' => array('Protocolo.usuario_id' => $formando['ViewFormandos']['id'], 'Protocolo.tipo' => 'cancelamento', "Protocolo.status <> 'finalizado'")));
            $this->set('protocolo', $protocolo['Protocolo']['protocolo']);
            $cancelado = true;
        }

        $this->set('cancelado', $cancelado);
        $gerarBoleto = true;

        if ($cancelado || $formando['ViewFormandos']['realizou_checkout'] == '1')
            $gerarBoleto = false;
        $status = array();
        if($this->params['prefix'] != 'atendimento')
            $status = array('renegociada','cancelada');
        $this->set('gerarBoleto', $gerarBoleto);
        $this->Despesa->bindModel(array('belongsTo' => array('CampanhasUsuario')), false);
        $this->Despesa->recursive = 2;
        $this->CampanhasUsuario->bindModel(array('belongsTo' => array('Campanha')), false);
        $extras = $this->Despesa->find('all', array(
            'conditions' => array(
                'Despesa.usuario_id' => $formando['ViewFormandos']['id'],
                'Despesa.tipo' => 'extra',
                'Despesa.status not' => $status
            )
        ));
        $this->set('despesas_extras', $extras);
        $pagamentos = $this->Pagamento->find('all', array('conditions' => array('Pagamento.status' => 'pago', 'Pagamento.usuario_id' => $formando['ViewFormandos']['id'])));
        $pagamentos_nao_vinculados = array();
        
        foreach ($pagamentos as $pagamento)
            if (sizeof($pagamento['DespesaPagamento']) == 0)
                if ($pagamento['Pagamento']['tipo'] != 'boleto_igpm')
                    $pagamentos_nao_vinculados[] = $pagamento;

        $this->set('boletos', $pagamentos_nao_vinculados);
        $parcelasIgpmRecriada = array();
        $adesoes = $this->Despesa->find('all', array(
            'conditions' => array(
                'Despesa.usuario_id' => $formando['ViewFormandos']['id'],
                'Despesa.tipo' => 'adesao',
                'Despesa.status not' => $status
            )
        ));
        $despesaIgpm = false;
        foreach ($adesoes as $adesao)
            if($adesao['Despesa']['status'] == "paga" && $adesao['Despesa']['status_igpm'] != "pago")
                $despesaIgpm = true;
        $this->set('despesas_adesao', $adesoes);
        $textoParcelaIgpm = "";
        if (!empty($parcelasIgpmRecriada))
            $textoParcelaIgpm = "Essa parcela é referente a quitação dos valores de IGPM dos boletos " .
                    "que foram pagos sem o devido ajuste. <br />No seu caso, corresponde a(s) parcela(s) " .
                    rtrim(implode(", ", $parcelasIgpmRecriada), ', ');
        $this->set('textoParcelaIgpm', $textoParcelaIgpm);
        $gerarBoletoIgpm = true;
        $this->set('gerarBoletoIgpm', $gerarBoletoIgpm);
        App::import('Component', 'Igpm');
        $igpmComponent = new IgpmComponent();
        if ($despesaIgpm || $igpmComponent->obterDespesa($formando['ViewFormandos'])) {
            $despesaIgpm = $igpmComponent->obterDespesa($formando['ViewFormandos']);
            if (empty($despesaIgpm)) {
                if ($igpmComponent->gerarDespesa($formando['ViewFormandos']))
                    $despesaIgpm = $igpmComponent->obterDespesa($formando['ViewFormandos']);
                else
                    $despesaIgpm = false;
            }
        }
        if ($despesaIgpm) {
            $despesaIgpm = $igpmComponent->obterDespesa($formando['ViewFormandos']);
            $this->set('totalPagoIgpm', $igpmComponent->obterTotalPago($formando['ViewFormandos']));
            $parcelasIgpm = $igpmComponent->obterParcelas($formando['ViewFormandos']);
            if (!empty($parcelasIgpm)) {
                App::import('Component', 'Boleto');
                $boleto = new BoletoComponent();
                foreach ($parcelasIgpm as $indice => $parcelaIgpm) {
                    $boleto->dadosboleto["data_vencimento"] = date('d/m/Y', strtotime("+1 day"));
                    $boleto->dadosboleto["valor_boleto"] = number_format($parcelaIgpm['Pagamento']['valor_nominal'], 2, ',', '.');
                    $boleto->dadosboleto["numero_documento"] = $parcelaIgpm['Pagamento']['codigo'];
                    $boleto->dadosboleto["nosso_numero"] = $parcelaIgpm['Pagamento']['codigo'];
                    $parcelasIgpm[$indice]['Pagamento']['linha_digitavel'] = $boleto->obterLinhaDigitavel();
                }
            }
            $this->set('parcelasIgpm', $parcelasIgpm);
        }
        $this->set('despesaIgpm', $despesaIgpm);
        $totalAdesao = $this->Financeiro->obterValorAdesao($formando['ViewFormandos']['id']);
        $totalExtras = $this->Financeiro->obterValorExtras($formando['ViewFormandos']['id']);
        $totalIgpm = $this->Financeiro->obterValorIgpm($formando['ViewFormandos']['id']);
        $totalPago = $this->Financeiro->obterTotalPago($formando['ViewFormandos']['id']);
        $saldoAtual = $totalPago-($totalExtras+$totalIgpm+$totalAdesao);
        $this->set('totalPago', $totalPago);
        $this->set('saldoAtual', $saldoAtual);
        $this->set('totalAdesao', $totalAdesao);
        $this->set('totalIGPM',$totalIgpm);
        $this->loadModel("ViewRelatorioExtras");
        $relatorioExtras = $this->ViewRelatorioExtras->find('all', array('conditions' => array('usuario_id' => $formando['ViewFormandos']['id'])));
        $this->set('resumoExtras', $relatorioExtras);
        $Formandos = new FormandosController;
        $Formandos->constructClasses();
        $this->Despesa->bindModel(array('belongsTo' => array('CampanhasUsuario')), false);
        $this->Despesa->recursive = 3;
        $this->CampanhasUsuario->bindModel(
            array(
                'belongsTo' => array('Campanha'),
                'hasMany' => array(
                    'CampanhasUsuariosCampanhasExtras' => array('foreignKey' => 'campanhas_usuario_id')
                )
            ), false);
        $extras = $this->Despesa->find('all', array('conditions' => array('Despesa.usuario_id' => $formando['ViewFormandos']['id'], 'Despesa.tipo' => 'extra')));
        $arrayDeCampanhasAderidas = array();

        foreach($extras as $despesaExtra)
            if(!in_array($despesaExtra['CampanhasUsuario']['campanha_id'], $arrayDeCampanhasAderidas))
                $arrayDeCampanhasAderidas[] = $despesaExtra['CampanhasUsuario']['campanha_id'];
        $dataAssinaturaContrato = date('d/m/Y',strtotime($turma['Turma']['data_assinatura_contrato']));
        $arrayData = explode("/",$dataAssinaturaContrato);
        $arrayData[2]++;
        $dataUmAnoAposAssinatura = implode("/",$arrayData);
        $arrayData[2]++;
        $dataDoisAnosAposAssinatura = implode("/",$arrayData);
        $textoIGPM = "Conforme consta em contrato, os valores s&atilde;o corrigidos de 12 em 12 meses a partir da assinatura do contrato entre";
        $textoIGPM.= " a comiss&atilde;o de formatura e a RK formaturas.<br /><br /> O &iacute;ndice utilizado &eacute; o IGPM.";
        $textoIGPM.= " O seu contrato foi assinado entre a RK e a comiss&atilde;o em $dataAssinaturaContrato";
        $textoIGPM.= ".<br /><br />Sendo assim, caso voc&ecirc; tenha aderido com o valor inicial, todas as parcelas n&atilde;o pagas at&eacute; $dataUmAnoAposAssinatura";
        $textoIGPM.= " sofrer&atilde;o reajustes.<br /><br />Caso voc&ecirc; j&aacute; tenha aderido com o valor corrigido, as parcelas s&oacute; ser&atilde;o corrigidas";
        $textoIGPM.= " a partir do pr&oacute;ximo ciclo de 12 meses, ou seja ap&oacute;s $dataDoisAnosAposAssinatura, caso voc&ecirc; possua valores em aberto ap&oacute;s essa data.";
        $this->set('textoIGPM', $textoIGPM);
    }

    function formando_imprimir() {
        $this->formando_index();
        $this->layout = 'impressao';
    }

    function atendimento_gerar_boleto_cancelamento($usuarioId, $despesaId) {
        $this->formando_gerar_boleto_cancelamento($usuarioId, $despesaId);
    }

    function formando_gerar_boleto_cancelamento($usuarioId, $despesaId) {
        $despesa = $this->Despesa->find('first', array('conditions' => array('Despesa.tipo' => 'cancelamento', 'Despesa.status' => 'aberta', 'Despesa.usuario_id' => $usuarioId, 'Despesa.id' => $despesaId)));
        $formando = $this->FormandoProfile->find('first', array('conditions' => array('FormandoProfile.usuario_id' => $usuarioId)));
        if (!empty($despesa)) {

            $this->gerar_boleto($despesa['Despesa']['id'], $usuarioId);
        } else {
            $this->Session->setFlash('Erro ao gerar boleto de cancelamento de contrato. Por favor entre em contato conosco.', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/area_financeira");
        }
    }

    // function atendimento_gerar_boleto($idDaDespesa) {
    //     $this->redirect("/{$this->params['prefix']}/boletos/gerar/{$idDaDespesa}");
    //     $despesa = $this->Despesa->find('first', array('conditions' => array("Despesa.id = $idDaDespesa")));
    //     $this->FormandoProfile->recursive = 2;
    //     App::import('Component', 'Boleto');
    //     $usuario = $this->FormandoProfile->find('first', array('conditions' => array("FormandoProfile.usuario_id = {$despesa['Despesa']['usuario_id']}")));
    //     $this->financeiro_gerar_boleto($usuario, $idDaDespesa);
    // }
    
    function app_gerar_codigo() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('erro' => true, 'mensagem' => array());
        if(isset($this->data['despesa'])) {
            $despesaId = $this->data['despesa'];
            $despesa = $this->Despesa->read(null,$despesaId);
            if($despesa) {
                if($despesa['Despesa']['status'] == 'aberta') {
                    $usuario = $this->Usuario->read(null,$despesa['Despesa']['usuario_id']);
                    if($usuario) {
                        $turma = $this->Turma->read(null,$usuario['ViewFormandos']['turma_id']);
                        App::import('Component', 'Boleto');
                        $boleto = new BoletoComponent();
                        $pagamentoBoleto = $this->obterPagamentoBoleto($despesaId);
                        if($pagamentoBoleto === false) {
                            $ultimoBoletoGerado = $this->pegaNumeroUltimoBoletoGeradoDoFormando($usuario['Usuario']['id']);
                            $numeroDoProximoBoleto = (int) ($ultimoBoletoGerado[0]['ordemDoUltimoBoleto']) + 1;
                            if($numeroDoProximoBoleto > 999) {
                                $json['mensagem'][] = "Erro ao gerar boleto";
                                $json['mensagem'][] = "Entre em contato com seu representante - Cod 0";
                            } elseif(!$this->gerarPagamentoBoleto($boleto, $despesa, $usuario, $turma, $numeroDoProximoBoleto)) {
                                $json['mensagem'][] = "Erro ao gerar boleto";
                                $json['mensagem'][] = "Entre em contato com seu representante - Cod 1";
                            } else {
                                $codigo = $boleto->obterLinhaDigitavel();
                                if($codigo) {
                                    $json = array(
                                        'erro' => false,
                                        'codigo_barras' => $codigo
                                    );
                                } else {
                                    $json['mensagem'][] = "Erro ao gerar boleto";
                                    $json['mensagem'][] = "Entre em contato com seu representante - Cod 2";
                                }
                            }
                        } else {
                            $pagamento = $this->Pagamento->read(null,$pagamentoBoleto);
                            if($pagamento) {
                                $this->obterCodigoBoleto($boleto,$pagamento,$usuario,$turma);
                                $codigo = $boleto->obterLinhaDigitavel();
                                if($codigo) {
                                    $json = array(
                                        'erro' => false,
                                        'codigo_barras' => $codigo
                                    );
                                } else {
                                    $json['mensagem'][] = "Erro ao gerar boleto";
                                    $json['mensagem'][] = "Entre em contato com seu representante - Cod 4";
                                }
                            } else {
                                $json['mensagem'][] = "Erro ao gerar boleto";
                                $json['mensagem'][] = "Entre em contato com seu representante - Cod 3";
                            }
                        }
                    } else {
                        $json['mensagem'][] = "Formando não encontrado";
                    }
                } else {
                    $json['mensagem'][] = "Esse boleto já foi pago";
                }
            } else {
                $json['mensagem'][] = "Despesa não encontrada";
            }
        }
        echo json_encode($json);
    }
    
    private function obterPagamentoBoleto($despesaId) {
        $this->DespesaPagamento->recursive = 1;
        $ultimaDespesaPagamentoCriada = $this->DespesaPagamento->find('first', array(
            'order' => array('DespesaPagamento.id DESC'),
            'conditions' => array(
                'Pagamento.tipo' => 'boleto',
                'DespesaPagamento.despesa_id' => $despesaId
            )
        ));
        if(!($ultimaDespesaPagamentoCriada == false))
            return $ultimaDespesaPagamentoCriada['DespesaPagamento']['pagamento_id'];
        else
            return false;
    }
    
    private function gerarPagamentoBoleto(&$boleto,$despesa,$usuario,$turma,$numeroDoProximoBoleto) {
        $this->FormandoProfile->recursive = -1;
        $formandoProfile = $this->FormandoProfile->find('first', array(
            'conditions' => array('usuario_id' => $usuario['Usuario']['id'])
        ));
        $boleto->dadosboleto['usuarioId'] = $usuario['Usuario']['id'];
        $boleto->codigoFormando = $formandoProfile['FormandoProfile']['codigo_formando'];
        $boleto->turmaId = $turma['Turma']['id'];
        $boleto->ordemBoleto = $numeroDoProximoBoleto;
        $boleto->dadosboleto["data_vencimento"] = date('d/m/Y', strtotime($despesa['Despesa']['data_vencimento']));
        $boleto->dadosboleto["valor_boleto"] = number_format($despesa['Despesa']['valor'] + $despesa['Despesa']['correcao_igpm'], 2, ',', '.');

        $pagamento = array(
            'usuario_id' => $usuario['Usuario']['id'],
            'valor_nominal' => $despesa['Despesa']['valor'] + $despesa['Despesa']['correcao_igpm'],
            'codigo' => $boleto->gerarNumeroDocumento(),
            'dt_cadastro' => date('Y-m-d H:i:s'),
            'dt_vencimento' => $despesa['Despesa']['data_vencimento'],
            'tipo' => 'boleto',
            'status' => 'aberto',
        );

        $despesa_pagamento = array(
            'despesa_id' => $despesa['Despesa']['id'],
            'valor' => $despesa['Despesa']['valor'],
            'multa' => $despesa['Despesa']['multa'],
            'correcao_igpm' => $despesa['Despesa']['correcao_igpm'] + 0
        );
        $dados['Pagamento'] = $pagamento;
        $dados['DespesaPagamento'] = array($despesa_pagamento);

        $this->Pagamento->bindModel(array(
            'hasMany' => array(
                'DespesaPagamento' =>array('foreignKey' => 'pagamento_id')
            )
        ));

        $this->Despesa->bindModel(array(
            'hasMany' => array(
                'DespesaPagamento' => array('foreignKey' => 'despesa_id')
            )
        ));
        
        return $this->Pagamento->saveAll($dados);
    }
    
    private function obterCodigoBoleto(&$boleto,$pagamento,$usuario,$turma) {
        $boleto->dadosboleto['usuarioId'] = $usuario['Usuario']['id'];
        $boleto->codigoFormando = substr($pagamento['Pagamento']['codigo'], 6, 3);
        $boleto->turmaId = $turma['Turma']['id'];
        $boleto->ordemBoleto = substr($pagamento['Pagamento']['codigo'], -3);
        $boleto->dadosboleto["data_vencimento"] = date('d/m/Y', strtotime($pagamento['Pagamento']['dt_vencimento']));
        $boleto->dadosboleto["valor_boleto"] = number_format($pagamento['Pagamento']['valor_nominal'], 2, ',', '.');
    }

    // function formando_gerar_boleto($idDaDespesa,$verificado = 0) {
    //     $this->redirect("/{$this->params['prefix']}/boletos/gerar/{$idDaDespesa}");
    //     $usuario = $this->Session->read('Usuario');
    //     $turma = $this->Session->read('turma');

    //     $this->DespesaPagamento->recursive = 1;
    //     $ultimaDespesaPagamentoCriada = $this->DespesaPagamento->find('first', array('order' => array('DespesaPagamento.id DESC'), 'conditions' => array('Pagamento.tipo' => 'boleto', 'DespesaPagamento.despesa_id' => $idDaDespesa)));
    //     App::import('Component', 'BoletoConta');
    //     $this->boletoConta = new BoletoContaComponent();
    //     $this->boletoConta->despesaId = $idDaDespesa;
    //     if ($ultimaDespesaPagamentoCriada == false) {
    //         $this->Despesa->id = $idDaDespesa;
    //         $this->Despesa->read();
    //         $despesa = $this->Despesa->find('first', array('conditions' => array('Despesa.id' => $idDaDespesa, 'Despesa.usuario_id' => $usuario['Usuario']['id'])));
    //         $despesa = $despesa['Despesa'];

    //         $ultimoBoletoGerado = $this->pegaNumeroUltimoBoletoGeradoDoFormando($usuario['Usuario']['id']);
    //         $numeroDoProximoBoleto = (int) ($ultimoBoletoGerado[0]['ordemDoUltimoBoleto']) + 1;
    //         $this->FormandoProfile->recursive = -1;
    //         $formandoProfile = $this->FormandoProfile->find('first', array(
    //             'conditions' => array('usuario_id' => $usuario['Usuario']['id'])
    //         ));

    //         App::import('Component', 'Boleto');

    //         $boleto = new BoletoComponent();
    //         $boleto->dadosboleto['usuarioId'] = $formandoProfile['FormandoProfile']['usuario_id'];
    //         $boleto->codigoFormando = $formandoProfile['FormandoProfile']['codigo_formando'];
    //         $boleto->turmaId = $turma['Turma']['id'];
    //         $boleto->ordemBoleto = $numeroDoProximoBoleto;
    //         $boleto->dadosboleto["data_vencimento"] = date('d/m/Y', strtotime($despesa['data_vencimento'])); // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
    //         //$boleto->dadosboleto["valor_boleto"] = str_replace('.', ',', $despesa['valor'] + $despesa['correcao_igpm']); 	// Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgula
    //         $boleto->dadosboleto["valor_boleto"] = number_format($despesa['valor'] + $despesa['correcao_igpm'], 2, ',', '.');
    //         $boleto->dadosboleto["sacado"] = $usuario['Usuario']['nome'];
    //         $multa = number_format($turma['Turma']['multa_por_atraso'], 2, ',', '');
    //         $boleto->dadosboleto["instrucoes2"] = "AP&Oacute;S O VENCIMENTO APLICAR MULTA DE $multa% SOBRE O VALOR. INCLUIR JUROS/MORA de R$ 0,10 AO DIA.";
    //         $boleto->dadosboleto["demonstrativo2"] = "AP&Oacute;S O VENCIMENTO APLICAR MULTA DE $multa% SOBRE O VALOR. INCLUIR JUROS/MORA de R$ 0,10 AO DIA.";

    //         $pagamento = array(
    //             'usuario_id' => $usuario['Usuario']['id'],
    //             'valor_nominal' => $despesa['valor'] + $despesa['correcao_igpm'],
    //             'codigo' => $boleto->gerarNumeroDocumento(),
    //             'dt_cadastro' => date('Y-m-d H:i:s'),
    //             'dt_vencimento' => $despesa['data_vencimento'],
    //             'tipo' => 'boleto',
    //             'status' => 'aberto',
    //         );

    //         $despesa_pagamento = array(
    //             'despesa_id' => $despesa['id'],
    //             'valor' => $despesa['valor'],
    //             'multa' => $despesa['multa'],
    //             'correcao_igpm' => $despesa['correcao_igpm'] + 0
    //         );
    //         $dados['Pagamento'] = $pagamento;
    //         $dados['DespesaPagamento'] = array($despesa_pagamento);

    //         $this->Pagamento->bindModel(array('hasMany' => array('DespesaPagamento' =>
    //                 array('foreignKey' => 'pagamento_id'))));

    //         $this->Despesa->bindModel(array('hasMany' => array('DespesaPagamento' =>
    //                 array('foreignKey' => 'despesa_id'))));

    //         if ($numeroDoProximoBoleto > 999) {
    //             $return = false;
    //         } else {
    //             if ($this->Pagamento->saveAll($dados)) {
    //                 $turma = $this->Turma->read(null,$turma['Turma']['id']);
    //                 $this->boletoConta->iniciar(false, $usuario, $turma);
    //                 $codigoAntigo = $boleto->obterLinhaDigitavel();
    //                 $codigoNovo = $this->boletoConta->obterCodigoDeBarras();
    //                 if($codigoAntigo != $codigoNovo) {
    //                     $this->enviarErroBoleto($usuario, $this->boletoConta->despesaId, $codigoAntigo, $codigoNovo);
    //                 }
    //                 if($verificado == 0) {
    //                     $this->layout = 'metro/externo';
    //                     $this->set('boleto',$boleto->obterDados());
    //                     $this->set('despesa',$this->boletoConta->despesaId);
    //                     $this->set('banco',$this->boletoConta->dadosboleto['nome_configuracao']);
    //                     $this->render('verificar_boleto');
    //                 } else {
    //                     $this->autoRender = false;
    //                     $boleto->imprimir($this->webroot);
    //                 }
    //             } else {
    //                 $return = false;
    //             }
    //         }
    //     } else {
    //         $this->formando_segunda_via($ultimaDespesaPagamentoCriada['DespesaPagamento']['pagamento_id'],$verificado);
    //     }
    // }

    function formando_gerar_boleto_com_multa($idDaDespesa, $verificado = 0) {
        $this->redirect("/{$this->params['prefix']}/boletos/gerar_com_multa/{$idDaDespesa}");
    }

    function atendimento_gerar_boleto_com_multa($idDaDespesa, $verificado = 0) {
        $this->redirect("/{$this->params['prefix']}/boletos/gerar_com_multa/{$idDaDespesa}");
        $this->layout = false;
        $this->autoRender = false;
        $despesa = $this->Despesa->find("first", array('conditions' => array('Despesa.id' => $idDaDespesa)));
        $dataVencimento = $despesa["Despesa"]["data_vencimento"];
        $multaAntiga = $despesa["Despesa"]["multa"];
        $diasAtraso = (strtotime(date('Y-m-d')) - strtotime($dataVencimento)) / (60 * 60 * 24);
        $this->Despesa->id = $despesa['Despesa']['id'];
        $usuario = $this->FormandoProfile->find('first', array('conditions' => array('FormandoProfile.usuario_id' => $despesa['Despesa']['usuario_id'])));
        $porcent = $usuario['Usuario']['Turma'][0]['multa_por_atraso'] / 100;
        if ($diasAtraso > 0) {
            $multa = ($despesa['Despesa']['valor'] * $porcent) + ($diasAtraso * 0.1);
            $this->Despesa->saveField('multa', $multa);
        }
        App::import('Component', 'Boleto');
        $this->gerar_boleto_com_multa($usuario, $idDaDespesa,$verificado);
        $this->Despesa->saveField('multa', $multaAntiga);
        $this->Despesa->id = false;
    }

    private function gerar_boleto_com_multa($usuario, $idDaDespesa,$verificado = 0) {
        $return = true;
        $formando = $usuario["FormandoProfile"];
        $this->DespesaPagamento->recursive = 1;
        $ultimaDespesaPagamentoCriada = $this->DespesaPagamento->find('first', array('order' => array('DespesaPagamento.id DESC'), 'conditions' => array('Pagamento.tipo' => 'boleto', 'DespesaPagamento.despesa_id' => $idDaDespesa)));
        App::import('Component', 'BoletoConta');
        $this->boletoConta = new BoletoContaComponent();
        $this->boletoConta->despesaId = $idDaDespesa;
        if ($ultimaDespesaPagamentoCriada == false) {
            $this->Despesa->id = $idDaDespesa;
            $this->Despesa->read();
            $despesa = $this->Despesa->find('first', array('conditions' => array('Despesa.id' => $idDaDespesa, 'Despesa.usuario_id' => $usuario['Usuario']['id'])));
            $despesa = $despesa['Despesa'];
            $ultimoBoletoGerado = $this->pegaNumeroUltimoBoletoGeradoDoFormando($usuario['Usuario']['id']);
            $numeroDoProximoBoleto = (int) ($ultimoBoletoGerado[0]['ordemDoUltimoBoleto']) + 1;

            $this->FormandoProfile->recursive = -1;
            $formandoProfile = $this->FormandoProfile->find('first', array(
                'conditions' => array('usuario_id' => $usuario['Usuario']['id'])
            ));
            App::import('Component', 'Boleto');

            $boleto = new BoletoComponent();
            $boleto->dadosboleto['usuarioId'] = $formandoProfile['FormandoProfile']['usuario_id'];
            $boleto->codigoFormando = $formandoProfile['FormandoProfile']['codigo_formando'];
            $boleto->turmaId = $usuario['Usuario']['Turma'][0]['id'];
            $boleto->ordemBoleto = $numeroDoProximoBoleto;
            $boleto->dadosboleto["data_vencimento"] = date('d/m/Y'); // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
            //$boleto->dadosboleto["valor_boleto"] = str_replace('.', ',', $despesa['valor'] + $despesa['correcao_igpm'] + $despesa['multa']); 	// Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgula
            $boleto->dadosboleto["valor_boleto"] = number_format($despesa['valor'] + $despesa['correcao_igpm'] + $despesa['multa'], 2, ',', '.');
            $boleto->dadosboleto["sacado"] = $usuario['Usuario']['nome'];
            $multa = number_format($usuario['Usuario']['Turma'][0]['multa_por_atraso'], 2, ',', '');
            $boleto->dadosboleto["instrucoes2"] = "AP&Oacute;S O VENCIMENTO APLICAR MULTA DE $multa% SOBRE O VALOR. INCLUIR JUROS/MORA de R$ 0,10 AO DIA.";
            $boleto->dadosboleto["demonstrativo2"] = "AP&Oacute;S O VENCIMENTO APLICAR MULTA DE $multa% SOBRE O VALOR. INCLUIR JUROS/MORA de R$ 0,10 AO DIA.";

            $pagamento = array(
                'usuario_id' => $usuario['Usuario']['id'],
                'valor_nominal' => $despesa['valor'] + $despesa['correcao_igpm'],
                'codigo' => $boleto->gerarNumeroDocumento(),
                'dt_cadastro' => date('Y-m-d H:i:s'),
                'dt_vencimento' => $despesa['data_vencimento'],
                'tipo' => 'boleto',
                'status' => 'aberto',
            );

            $despesa_pagamento = array(
                'despesa_id' => $despesa['id'],
                'valor' => $despesa['valor'],
                'multa' => 0,
                'correcao_igpm' => $despesa['correcao_igpm'] + 0
            );

            $dados['Pagamento'] = $pagamento;
            $dados['DespesaPagamento'] = array($despesa_pagamento);

            $this->Pagamento->bindModel(array('hasMany' => array('DespesaPagamento' =>
                    array('foreignKey' => 'pagamento_id'))));

            $this->Despesa->bindModel(array('hasMany' => array('DespesaPagamento' =>
                    array('foreignKey' => 'despesa_id'))));

            if ($numeroDoProximoBoleto > 999) {
                $return = false;
            } else {
                if ($this->Pagamento->saveAll($dados)) {
                    $turma = $this->obterTurmaLogada();
                    $this->boletoConta->iniciar(false, $usuario, $turma);
                    $codigoAntigo = $boleto->obterLinhaDigitavel();
                    if($verificado == 0) {
                        $this->layout = 'metro/externo';
                        $this->set('boleto',$boleto->obterDados());
                        $this->set('despesa',$this->boletoConta->despesaId);
                        $this->set('banco',$this->boletoConta->dadosboleto['nome_configuracao']);
                        $this->render('verificar_boleto');
                    } else {
                        $this->autoRender = false;
                        $boleto->imprimir($this->webroot);
                    }
                } else {
                    $return = false;
                }
            }
        } else {
            $return = $this->segunda_via_com_multa($usuario, $ultimaDespesaPagamentoCriada['DespesaPagamento']['pagamento_id'],$verificado);
        }
        return $return;
    }

    function formando_gerar_boleto_igpm($idParcela) {
        $this->atendimento_gerar_boleto_igpm($idParcela);
    }

    function atendimento_gerar_boleto_igpm($idParcela) {
      //  $this->redirect("/{$this->params['prefix']}/boletos/gerar_igpm/{$idParcela}");
        App::import('Component', 'Igpm');
        $igpm = new IgpmComponent();
        $parcela = $igpm->obterParcelaPorId($idParcela);
        if (empty($parcela)) {
            $this->Session->setFlash('Parcela de IGPM não encontrada.', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/area_financeira");
        } else {
            $formando = $this->ViewFormandos->find('first', array('conditions' => array('id' => $parcela['Pagamento']['usuario_id'])));
            $this->gerar_boleto($idParcela, $parcela['Pagamento']['usuario_id']);
            // App::import('Component', 'Boleto');
            // $boleto = new BoletoComponent();
            // $dateTime = $this->create_date_time_from_format('d-m-Y', $parcela['Pagamento']['dt_vencimento']);
            // $data = date_format($dateTime, 'd/m/Y');
            // $boleto->dadosboleto["data_vencimento"] = $data;
            // $boleto->dadosboleto["valor_boleto"] = number_format($parcela['Pagamento']['valor_nominal'], 2, ',', '.');
            // $boleto->dadosboleto["numero_documento"] = $parcela['Pagamento']['codigo'];
            // $boleto->dadosboleto["nosso_numero"] = $parcela['Pagamento']['codigo'];
            // $boleto->dadosboleto["sacado"] = $formando['ViewFormandos']['nome'];
            // $turma = $this->Turma->read(null, $formando['ViewFormandos']['turma_id']);
            // $multa = number_format($turma['Turma']['multa_por_atraso'], 2, ',', '');
            // $boleto->dadosboleto["instrucoes2"] = "AP&Oacute;S O VENCIMENTO APLICAR MULTA DE $multa% SOBRE O VALOR. INCLUIR JUROS/MORA de R$ 0,10 AO DIA.";
            // $boleto->dadosboleto["demonstrativo2"] = "AP&Oacute;S O VENCIMENTO APLICAR MULTA DE $multa% SOBRE O VALOR. INCLUIR JUROS/MORA de R$ 0,10 AO DIA.";
            // $this->autoRender = false;
            // $boleto->imprimirIgpm($this->webroot);
        }
    }

    function pegaNumeroUltimoBoletoGeradoDoFormando($usuario_id,$codigo = false) {
        /*
        $numero = $this->Pagamento->find('first', array(
            'conditions' => array('usuario_id' => $usuario_id, "tipo like('boleto%')"),
            'fields' => array('IFNULL(MAX(RIGHT(codigo,3)),0) as ordemDoUltimoBoleto')
        ));
        */
        if(!$codigo) {
            $numero = $this->Pagamento->find('first', array(
                'conditions' => array(
                    'usuario_id' => $usuario_id,
                    "tipo like('boleto%')"),
                'fields' => array('count(0) as ordemDoUltimoBoleto')
            ));
            $codigo = str_pad((int)$numero[0]['ordemDoUltimoBoleto']+1,3,'0',STR_PAD_LEFT);
        } else {
            $numero[] = array('ordemDoUltimoBoleto' => $codigo);
            $codigo = str_pad($codigo,3,'0',STR_PAD_LEFT);
        }
        
        $verificacao = $this->Pagamento->find('first', array(
            'conditions' => array(
                'usuario_id' => $usuario_id,
                "tipo like('boleto%')",
                'IFNULL(RIGHT(codigo,3),0)' => $codigo)
        ));
        
        if($verificacao) {
            $numero = $this->pegaNumeroUltimoBoletoGeradoDoFormando(
                    $usuario_id,$numero[0]['ordemDoUltimoBoleto']+1);
        }

        return $numero;
    }

    function formando_segunda_via($idDoPagamento,$verificado = 0) {
        //debug("Gerando 2a via: " . $idDoPagamento);
        $usuario = $this->Session->read('Usuario');
        $turma = $this->obterTurmaLogada();
        $this->Pagamento->recursive = 2;

        $this->Pagamento->bindModel(array('hasMany' => array('DespesaPagamento' =>
                array('foreignKey' => 'pagamento_id'))));

        $this->Despesa->bindModel(array('hasMany' => array('DespesaPagamento' =>
                array('foreignKey' => 'despesa_id'))));

        $pagamento = $this->Pagamento->find('first', array('conditions' => array('Pagamento.usuario_id' => $usuario['Usuario']['id'], 'Pagamento.id' => $idDoPagamento)));
        if (empty($pagamento)) {
            $this->Session->setFlash('Ocorreu um erro ao gerar o boleto.', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/area_financeira");
        }
        $despesa = $pagamento['DespesaPagamento'][0]['Despesa'];

        if ($despesa['valor'] + $despesa['correcao_igpm'] + 0 != $pagamento['Pagamento']['valor_nominal'] + 0) {
            if ($this->Pagamento->updateAll(
                            array('Pagamento.valor_nominal' => $despesa['valor'] + $despesa['correcao_igpm']), array('Pagamento.id' => $pagamento['Pagamento']['id'])
                    ))
                $return = $this->DespesaPagamento->updateAll(
                        array('DespesaPagamento.correcao_igpm' => $despesa['correcao_igpm'],
                    'DespesaPagamento.valor' => $despesa['valor']), array('DespesaPagamento.id' => $pagamento['DespesaPagamento'][0]['id'])
                );
            else
                return false;
            $pagamento = $this->Pagamento->find('first', array('conditions' => array('Pagamento.usuario_id' => $usuario['Usuario']['id'],
                    'Pagamento.id' => $idDoPagamento)));
        }

        App::import('Component', 'Boleto');

        $boleto = new BoletoComponent();
        $boleto->dadosboleto['usuarioId'] = $pagamento['Pagamento']['usuario_id'];
        $boleto->codigoFormando = substr($pagamento['Pagamento']['codigo'], 6, 3);
        $boleto->turmaId = $turma['Turma']['id'];
        $boleto->ordemBoleto = substr($pagamento['Pagamento']['codigo'], -3);
        $boleto->dadosboleto["data_vencimento"] = date('d/m/Y', strtotime($pagamento['Pagamento']['dt_vencimento'])); // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
        //$boleto->dadosboleto["valor_boleto"] = str_replace('.', ',', $pagamento['Pagamento']['valor_nominal']); 	// Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgula
        $boleto->dadosboleto["valor_boleto"] = number_format($pagamento['Pagamento']['valor_nominal'], 2, ',', '.');
        $boleto->dadosboleto["sacado"] = $usuario['Usuario']['nome'];
        $multa = number_format($turma['Turma']['multa_por_atraso'], 2, ',', '');
        $boleto->dadosboleto["instrucoes2"] = "AP&Oacute;S O VENCIMENTO APLICAR MULTA DE $multa% SOBRE O VALOR. INCLUIR JUROS/MORA de R$ 0,10 AO DIA.";
        $boleto->dadosboleto["demonstrativo2"] = "AP&Oacute;S O VENCIMENTO APLICAR MULTA DE $multa% SOBRE O VALOR. INCLUIR JUROS/MORA de R$ 0,10 AO DIA.";

        $valorDespesaMaisIGPM = $despesa['valor'] + $despesa['correcao_igpm'] + 0;
        $valorDoBoletoJaGerado = $pagamento['Pagamento']['valor_nominal'] + 0;
        
        $turma = $this->Turma->read(null,$turma['Turma']['id']);
        $this->boletoConta->iniciar(false, $usuario, $turma);
        $codigoAntigo = $boleto->obterLinhaDigitavel();
        $codigoNovo = $this->boletoConta->obterCodigoDeBarras();
        if($codigoAntigo != $codigoNovo) {
            $this->enviarErroBoleto($usuario, $this->boletoConta->despesaId, $codigoAntigo, $codigoNovo);
        }
        if($verificado == 0) {
            $this->layout = 'metro/externo';
            $this->set('boleto',$boleto->obterDados());
            $this->set('despesa',$this->boletoConta->despesaId);
            $this->set('banco',$this->boletoConta->dadosboleto['nome_configuracao']);
            $this->render('verificar_boleto');
        } else {
            $this->autoRender = false;
            $boleto->imprimir($this->webroot);
        }
    }
    
    function relatar_pagamento($despesaId) {
        $this->layout = 'metro/externo';
        $usuario = $this->obterUsuarioLogado();
        $despesa = $this->Despesa->read(null,$despesaId);
        $texto = date('d/m/Y H:i:s') . "\n\n{$usuario['Usuario']['nome']} - " .
            "{$usuario['ViewFormandos']['codigo_formando']}\nDespesa paga não conta no sistema\n\n";
        if($despesa) {
            $texto.= "Dados da despesa\nParcela: {$despesa['Despesa']['parcela']}\n" .
                "Valor: R$" . number_format($despesa['Despesa']['valor'], 2, ',', '.') .
                "\nVencimento: " . date('d/m/Y',strtotime($despesa['Despesa']['data_vencimento']));
        } else {
            $texto.= "Dados da despesa não encontrados";
        }
        App::import('Component', 'mail');
        $mailer = new Mail();
        $mailer->IsHTML(false);
        $mailer->FromName = utf8_decode($usuario['Usuario']['nome']);
        $mailer->AddAddress("monica@rkformaturas.com.br","Monica");
        $mailer->AddAddress("atendimento@rkformaturas.com.br","RK Sistema");
        $mailer->Subject = utf8_decode("Formando - Pagamento não inserido");
        $mailer->Body = utf8_decode($texto);
        $this->set('resposta',$mailer->Send());
    }
    
    function formando_enviar_boleto_errado() {
        $this->layout = 'metro/externo';
        $usuario = $this->Session->read('Usuario');
        $turma = $this->Session->read('turma');
        if(!isset($this->data['FormandoProfile']['texto'])) {
            $this->Session->setFlash('Erro ao receber dados',
                    'metro/flash/error');
        } else {
            $this->Despesa->bindModel(array('belongsTo' => array('ViewFormandos' => array(
                'foreignKey' => 'usuario_id'))),false);
            $despesa = $this->Despesa->read(null,$this->data['FormandoProfile']['despesa']);
            App::import('Component', 'mail');
            $mailer = new Mail();
            $mailer->IsHTML(false);
            $mailer->FromName = utf8_decode("Formando Erro Boleto");
            $mailer->AddAddress("monica@rkformaturas.com.br","Monica");
            $mailer->AddAddress("atendimento@rkformaturas.com.br","RK Sistema");
            $mensagem = date('d/m/Y H:i:s') . "\n\n";
            $mensagem.= Configure::read('url_site') . "\n\n";
            $mensagem.= "Formando encontrou um erro ao gerar boleto\n\n";
            $mensagem.= str_pad("Mensagem",15," "). "{$this->data['FormandoProfile']['texto']}\n\n";
            $mensagem.= str_pad("Despesa",15," "). "{$this->data['FormandoProfile']['despesa']}\n";
            if($despesa) {
                $mensagem.= str_pad("Formando",15," "). "{$despesa['ViewFormandos']['nome']}\n";
                $mensagem.= str_pad("Cod",15," "). "{$despesa['ViewFormandos']['codigo_formando']}\n";
                $mensagem.= str_pad("Parcela",15," "). "{$despesa['Despesa']['parcela']}\n";
                $mensagem.= str_pad("Valor",15," "). "{$despesa['Despesa']['valor']}\n";
                $mensagem.= str_pad("Vencimento",15," "). "{$despesa['Despesa']['data_vencimento']}\n";
            } else {
                $mensagem.= "Dados da despesa e do formando nao encontrados\n";
            }
            $mailer->Subject = "Formando - Erro ao gerar boleto";
            $mailer->Body = utf8_decode($mensagem);
            $this->set('resposta',$mailer->Send());
        }
        $this->render('resposta_envio_boleto');
    }
    
    private function enviarErroBoleto($formando,$despesa,$codigoAntigo,$codigoNovo) {
        App::import('Component', 'mail');
        $mailer = new Mail();
        $mailer->IsHTML(false);
        $mailer->FromName = utf8_decode("RK Formaturas Erro Boleto");
        $mailer->AddAddress("lucas.andraweb@gmail.com","Lucas Andrade");
        $mensagem = date('d/m/Y H:i:s') . "\n\n";
        $mensagem.= Configure::read('url_site') . "\n\n";
        $mensagem.= "Houve um erro ao gerar boleto para o formando {$formando['Usuario']['nome']}\n\n";
        $mensagem.= str_pad("Despesa:",13," "). "{$despesa}\n";
        $mensagem.= str_pad("Cod Antigo:",13," ") . "{$codigoAntigo}\n";
        $mensagem.= str_pad("Cod Novo:",13," ") . "{$codigoNovo}\n";
        $mailer->Subject = "Erro Boleto";
        $mailer->Body = utf8_decode($mensagem);
        $mailer->Send();
    }

    function financeiro_listar_formandos_sem_boletos() {
        $options["conditions"] = array("forma_pagamento" => "boleto_impresso", "impresso" => "0", "valor_adesao > 0", "Despesa.status" => "aberta");
        $options['joins'] = array(
            array(
                "table" => "despesas",
                "type" => "inner",
                "alias" => "Despesa",
                "conditions" => array("FormandoProfile.usuario_id = Despesa.usuario_id")
            )
        );
        $options['group'] = array('FormandoProfile.id');
        $formandos = $this->FormandoProfile->find("all", $options);
        $this->set("formandos", $formandos);
    }

    function financeiro_finaliza_geracao_boletos($uid = false) {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $options["conditions"] = array("forma_pagamento" => "boleto_impresso", "valor_adesao > 0", "Despesa.status" => "aberta");
        $options['joins'] = array(
            array(
                "table" => "despesas",
                "type" => "inner",
                "alias" => "Despesa",
                "conditions" => array("FormandoProfile.usuario_id = Despesa.usuario_id")
            )
        );
        if ($uid)
            $options['conditions'][] = "FormandoProfile.usuario_id = $uid";
        $options['group'] = array('FormandoProfile.id');
        $formandos = $this->FormandoProfile->find("all", $options);
        foreach ($formandos as $i => $formando) {
            $this->FormandoProfile->id = $formando["FormandoProfile"]["id"];
            $this->FormandoProfile->saveField("enviado", 1);
        }
        $this->Session->setFlash('Boletos salvos com sucesso!', 'flash_sucesso');
        $this->redirect("/{$this->params['prefix']}/formandos/lista_formandos_boletos_impressao");
    }

    // function financeiro_gerar_boleto($usuario, $idDaDespesa) {
    //     $return = true;
    //     $formando = $usuario["FormandoProfile"];
    //     $this->DespesaPagamento->recursive = 1;
    //     $ultimaDespesaPagamentoCriada = $this->DespesaPagamento->find('first', array('order' => array('DespesaPagamento.id DESC'), 'conditions' => array('Pagamento.tipo' => 'boleto', 'DespesaPagamento.despesa_id' => $idDaDespesa)));
    //     if ($ultimaDespesaPagamentoCriada == false) {
    //         $this->Despesa->id = $idDaDespesa;
    //         $this->Despesa->read();
    //         $despesa = $this->Despesa->find('first', array('conditions' => array('Despesa.id' => $idDaDespesa, 'Despesa.usuario_id' => $usuario['Usuario']['id'])));
    //         $despesa = $despesa['Despesa'];
    //         $ultimoBoletoGerado = $this->pegaNumeroUltimoBoletoGeradoDoFormando($usuario['Usuario']['id']);
    //         $numeroDoProximoBoleto = (int) ($ultimoBoletoGerado[0]['ordemDoUltimoBoleto']) + 1;
    //         $this->FormandoProfile->recursive = -1;
    //         $formandoProfile = $this->FormandoProfile->find('first', array(
    //             'conditions' => array('usuario_id' => $usuario['Usuario']['id'])
    //         ));
    //         App::import('Component', 'Boleto');

    //         $boleto = new BoletoComponent();
    //         $boleto->dadosboleto['usuarioId'] = $formandoProfile['FormandoProfile']['usuario_id'];
    //         $boleto->codigoFormando = $formandoProfile['FormandoProfile']['codigo_formando'];
    //         $boleto->turmaId = $usuario['Usuario']['Turma'][0]['id'];
    //         $boleto->ordemBoleto = $numeroDoProximoBoleto;
    //         $boleto->dadosboleto["data_vencimento"] = date('d/m/Y', strtotime($despesa['data_vencimento'])); // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
    //         //$boleto->dadosboleto["valor_boleto"] = str_replace('.', ',', $despesa['valor'] + $despesa['correcao_igpm']);	// Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgula
    //         $boleto->dadosboleto["valor_boleto"] = number_format($despesa['valor'] + $despesa['correcao_igpm'], 2, ',', '.');
    //         $boleto->dadosboleto["sacado"] = $usuario['Usuario']['nome'];
    //         $multa = number_format($usuario['Usuario']['Turma'][0]['multa_por_atraso'], 2, ',', '');
    //         $boleto->dadosboleto["instrucoes2"] = "AP&Oacute;S O VENCIMENTO APLICAR MULTA DE $multa% SOBRE O VALOR. INCLUIR JUROS/MORA de R$ 0,10 AO DIA.";
    //         $boleto->dadosboleto["demonstrativo2"] = "AP&Oacute;S O VENCIMENTO APLICAR MULTA DE $multa% SOBRE O VALOR. INCLUIR JUROS/MORA de R$ 0,10 AO DIA.";

    //         $pagamento = array(
    //             'usuario_id' => $usuario['Usuario']['id'],
    //             'valor_nominal' => $despesa['valor'] + $despesa['correcao_igpm'],
    //             'codigo' => $boleto->gerarNumeroDocumento(),
    //             'dt_cadastro' => date('Y-m-d H:i:s'),
    //             'dt_vencimento' => $despesa['data_vencimento'],
    //             'tipo' => 'boleto',
    //             'status' => 'aberto',
    //             'valor_pago' => 0.00
    //         );

    //         $despesa_pagamento = array(
    //             'despesa_id' => $despesa['id'],
    //             'valor' => $despesa['valor'],
    //             'multa' => $despesa['multa'],
    //             'correcao_igpm' => $despesa['correcao_igpm'] + 0
    //         );

    //         $dados['Pagamento'] = $pagamento;
    //         $dados['DespesaPagamento'] = array($despesa_pagamento);

    //         $this->Pagamento->bindModel(array('hasMany' => array('DespesaPagamento' =>
    //                 array('foreignKey' => 'pagamento_id'))));

    //         $this->Despesa->bindModel(array('hasMany' => array('DespesaPagamento' =>
    //                 array('foreignKey' => 'despesa_id'))));

    //         if ($numeroDoProximoBoleto > 999) {
    //             $return = false;
    //         } else {
    //             if ($this->Pagamento->saveAll($dados)) {
    //                 $this->autoRender = false;
    //                 $boleto->imprimir($this->webroot);
    //             } else {
    //                 $return = false;
    //             }
    //         }
    //     } else {
    //         $return = $this->financeiro_segunda_via_lote($usuario, $ultimaDespesaPagamentoCriada['DespesaPagamento']['pagamento_id']);
    //     }
    //     return $return;
    // }

    function financeiro_segunda_via($usuario, $idDoPagamento) {
        $return = true;
        $turma = $usuario["CursoTurma"]["Turma"];
        $this->Pagamento->recursive = 2;
        $this->Pagamento->bindModel(array('hasMany' => array('DespesaPagamento' =>
                array('foreignKey' => 'pagamento_id'))));
        $this->Despesa->bindModel(array('hasMany' => array('DespesaPagamento' =>
                array('foreignKey' => 'despesa_id'))));
        $pagamento = $this->Pagamento->find('first', array('conditions' => array('Pagamento.usuario_id' => $usuario['Usuario']['id'], 'Pagamento.id' => $idDoPagamento)));
        if (empty($pagamento)) {
            $this->Session->setFlash('Ocorreu um erro ao gerar o boleto.', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/area_financeira");
        }
        $despesa = $pagamento['DespesaPagamento'][0]['Despesa'];
        $boleto = new BoletoComponent();
        $boleto->codigoFormando = substr($pagamento['Pagamento']['codigo'], 6, 3);
        $boleto->turmaId = $turma['id'];
        $boleto->ordemBoleto = substr($pagamento['Pagamento']['codigo'], 9, 4);
        $boleto->dadosboleto["data_vencimento"] = date('d/m/Y', strtotime($pagamento['Pagamento']['dt_vencimento'])); // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
        //$boleto->dadosboleto["valor_boleto"] = str_replace('.', ',', $pagamento['Pagamento']['valor_nominal']); 	// Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgula
        $boleto->dadosboleto["valor_boleto"] = number_format($pagamento['Pagamento']['valor_nominal'], 2, ',', '.');
        $boleto->dadosboleto["sacado"] = $usuario['Usuario']['nome'];
        $multa = number_format($turma['multa_por_atraso'], 2, ',', '');
        $boleto->dadosboleto["instrucoes2"] = "AP&Oacute;S O VENCIMENTO APLICAR MULTA DE $multa% SOBRE O VALOR. INCLUIR JUROS/MORA de R$ 0,10 AO DIA.";
        $boleto->dadosboleto["demonstrativo2"] = "AP&Oacute;S O VENCIMENTO APLICAR MULTA DE $multa% SOBRE O VALOR. INCLUIR JUROS/MORA de R$ 0,10 AO DIA.";
        $valorDespesaMaisIGPM = $despesa['valor'] + $despesa['correcao_igpm'] + 0;
        $valorDoBoletoJaGerado = $pagamento['Pagamento']['valor_nominal'] + 0;

        if ($despesa['valor'] + $despesa['correcao_igpm'] + 0 != $pagamento['Pagamento']['valor_nominal'] + 0) {
            $usuario = $this->Session->read('Usuario');
            $turma = $this->Session->read('turma');

            $ultimoBoletoGerado = $this->pegaNumeroUltimoBoletoGeradoDoFormando($usuario['Usuario']['id']);

            $numeroDoProximoBoleto = (int) ($ultimoBoletoGerado[0]['ordemDoUltimoBoleto']) + 1;

            $this->FormandoProfile->recursive = -1;
            $formandoProfile = $this->FormandoProfile->find('first', array(
                'conditions' => array('usuario_id' => $usuario['Usuario']['id'])
            ));

            $boleto = new BoletoComponent();

            $boleto->codigoFormando = $formandoProfile['FormandoProfile']['codigo_formando'];
            $boleto->turmaId = $turma['Turma']['id'];
            $boleto->ordemBoleto = $numeroDoProximoBoleto;
            $boleto->dadosboleto["data_vencimento"] = date('d/m/Y', strtotime($despesa['data_vencimento'])); // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
            //$boleto->dadosboleto["valor_boleto"] = str_replace('.', ',', $despesa['valor'] + $despesa['correcao_igpm']); 	// Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgula
            $boleto->dadosboleto["valor_boleto"] = number_format($despesa['valor'] + $despesa['correcao_igpm'], 2, ',', '.');
            $boleto->dadosboleto["sacado"] = $usuario['Usuario']['nome'];
            $multa = number_format($turma['Turma']['multa_por_atraso'], 2, ',', '');
            $boleto->dadosboleto["instrucoes2"] = "AP&Oacute;S O VENCIMENTO APLICAR MULTA DE $multa% SOBRE O VALOR. INCLUIR JUROS/MORA de R$ 0,10 AO DIA.";
            $boleto->dadosboleto["demonstrativo2"] = "AP&Oacute;S O VENCIMENTO APLICAR MULTA DE $multa% SOBRE O VALOR. INCLUIR JUROS/MORA de R$ 0,10 AO DIA.";

            $pagamento = array(
                'usuario_id' => $usuario['Usuario']['id'],
                'valor_nominal' => $despesa['valor'] + $despesa['correcao_igpm'],
                'codigo' => $boleto->gerarNumeroDocumento(),
                'dt_cadastro' => date('Y-m-d H:i:s'),
                'dt_vencimento' => $despesa['data_vencimento'],
                'tipo' => 'boleto',
                'status' => 'aberto',
            );

            $despesa_pagamento = array(
                'despesa_id' => $despesa['id'],
                'valor' => $despesa['valor'],
                'multa' => $despesa['multa'],
                'correcao_igpm' => $despesa['correcao_igpm'] + 0
            );
            $dados['Pagamento'] = $pagamento;
            $dados['DespesaPagamento'] = array($despesa_pagamento);

            if ($numeroDoProximoBoleto > 999) {
                $return = false;
            } else {
                if ($this->Pagamento->saveAll($dados)) {
                    $pagamento = $this->Pagamento->read();
                } else {
                    $return = false;
                }
            }
        }
        $this->autoRender = false;
        $boleto->imprimir($this->webroot);
        return $return;
    }

    function financeiro_segunda_via_lote($usuario, $idDoPagamento) {
        $return = true;
        $turma = $usuario["CursoTurma"]["Turma"];
        $this->Pagamento->recursive = 2;
        $this->Pagamento->bindModel(array('hasMany' => array('DespesaPagamento' =>
                array('foreignKey' => 'pagamento_id'))));
        $this->Despesa->bindModel(array('hasMany' => array('DespesaPagamento' =>
                array('foreignKey' => 'despesa_id'))));
        $pagamento = $this->Pagamento->find('first', array('conditions' => array('Pagamento.usuario_id' => $usuario['Usuario']['id'], 'Pagamento.id' => $idDoPagamento)));
        if (empty($pagamento))
            return false;
        $despesa = $pagamento['DespesaPagamento'][0]['Despesa'];
        if ($despesa['valor'] + $despesa['correcao_igpm'] != $pagamento['Pagamento']['valor_nominal']) {
            if ($this->Pagamento->updateAll(
                            array('Pagamento.valor_nominal' => $despesa['valor'] + $despesa['correcao_igpm']), array('Pagamento.id' => $pagamento['Pagamento']['id'])
                    ))
                $return = $this->DespesaPagamento->updateAll(
                        array('DespesaPagamento.correcao_igpm' => $despesa['correcao_igpm'],
                    'DespesaPagamento.valor' => $despesa['valor']), array('DespesaPagamento.id' => $pagamento['DespesaPagamento'][0]['id'])
                );
            else
                return false;
        }
        $boleto = new BoletoComponent();
        $boleto->dadosboleto['usuarioId'] = $pagamento['Pagamento']['usuario_id'];
        $boleto->codigoFormando = substr($pagamento['Pagamento']['codigo'], 6, 3);
        $boleto->turmaId = $turma['id'];
        $boleto->ordemBoleto = substr($pagamento['Pagamento']['codigo'], 9, 4);
        $boleto->dadosboleto["data_vencimento"] = date('d/m/Y', strtotime($pagamento['Pagamento']['dt_vencimento'])); // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
        //$boleto->dadosboleto["valor_boleto"] = str_replace('.', ',', $pagamento['Pagamento']['valor_nominal']); 	// Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgula
        $boleto->dadosboleto["valor_boleto"] = number_format($pagamento['Pagamento']['valor_nominal'], 2, ',', '.');
        $boleto->dadosboleto["sacado"] = $usuario['Usuario']['nome'];
        $multa = number_format($turma['multa_por_atraso'], 2, ',', '');
        $boleto->dadosboleto["instrucoes2"] = "AP&Oacute;S O VENCIMENTO APLICAR MULTA DE $multa% SOBRE O VALOR. INCLUIR JUROS/MORA de R$ 0,10 AO DIA.";
        $boleto->dadosboleto["demonstrativo2"] = "AP&Oacute;S O VENCIMENTO APLICAR MULTA DE $multa% SOBRE O VALOR. INCLUIR JUROS/MORA de R$ 0,10 AO DIA.";
        $valorDespesaMaisIGPM = $despesa['valor'] + $despesa['correcao_igpm'] + 0;
        $valorDoBoletoJaGerado = $pagamento['Pagamento']['valor_nominal'] + 0;
        $this->autoRender = false;
        $boleto->imprimir($this->webroot);
        return $return;
    }

    private function segunda_via_com_multa($usuario, $idDoPagamento,$verificado = 0) {
        $return = true;
        $usuarioTurma = $this->Turma->read(null,$usuario['Usuario']['ViewFormandos']['turma_id']);
        $turma = $usuarioTurma['Turma'];
        $this->Pagamento->recursive = 2;
        $this->Pagamento->bindModel(array('hasMany' => array('DespesaPagamento' =>
                array('foreignKey' => 'pagamento_id'))));
        $this->Despesa->bindModel(array('hasMany' => array('DespesaPagamento' =>
                array('foreignKey' => 'despesa_id'))));
        $pagamento = $this->Pagamento->find('first', array('conditions' => array('Pagamento.usuario_id' => $usuario['Usuario']['id'], 'Pagamento.id' => $idDoPagamento)));
        if (empty($pagamento))
            return false;
        $despesa = $pagamento['DespesaPagamento'][0]['Despesa'];
        $boleto = new BoletoComponent();
        $boleto->dadosboleto['usuarioId'] = $pagamento['Pagamento']['usuario_id'];
        $boleto->codigoFormando = substr($pagamento['Pagamento']['codigo'], 6, 3);
        $boleto->turmaId = substr($pagamento['Pagamento']['codigo'], 1, 5);
        $boleto->ordemBoleto = substr($pagamento['Pagamento']['codigo'], 9, 4);
        $boleto->dadosboleto["data_vencimento"] = date('d/m/Y'); // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
        //$boleto->dadosboleto["valor_boleto"] = str_replace('.', ',', $pagamento['Pagamento']['valor_nominal'] + $despesa['multa']); 	// Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgula
        $boleto->dadosboleto["valor_boleto"] = number_format($pagamento['Pagamento']['valor_nominal'] + $despesa['multa'], 2, ',', '.');
        $boleto->dadosboleto["sacado"] = $usuario['Usuario']['nome'];
        $multa = number_format($turma['multa_por_atraso'], 2, ',', '');
        $boleto->dadosboleto["instrucoes2"] = "AP&Oacute;S O VENCIMENTO APLICAR MULTA DE $multa% SOBRE O VALOR. INCLUIR JUROS/MORA de R$ 0,10 AO DIA.";
        $boleto->dadosboleto["demonstrativo2"] = "AP&Oacute;S O VENCIMENTO APLICAR MULTA DE $multa% SOBRE O VALOR. INCLUIR JUROS/MORA de R$ 0,10 AO DIA.";
        $valorDespesaMaisIGPM = $despesa['valor'] + $despesa['correcao_igpm'] + 0;
        $valorDoBoletoJaGerado = $pagamento['Pagamento']['valor_nominal'] + 0;

        if ($despesa['valor'] + $despesa['correcao_igpm'] + 0 != $pagamento['Pagamento']['valor_nominal'] + 0) {
            $usuario = $this->Session->read('Usuario');
            $turma = $this->Session->read('turma');

            $ultimoBoletoGerado = $this->pegaNumeroUltimoBoletoGeradoDoFormando($usuario['Usuario']['id']);

            $numeroDoProximoBoleto = (int) ($ultimoBoletoGerado[0]['ordemDoUltimoBoleto']) + 1;

            $this->FormandoProfile->recursive = -1;
            $formandoProfile = $this->FormandoProfile->find('first', array(
                'conditions' => array('usuario_id' => $usuario['Usuario']['id'])
            ));

            $boleto = new BoletoComponent();

            $boleto->codigoFormando = $formandoProfile['FormandoProfile']['codigo_formando'];
            $boleto->turmaId = $turma['Turma']['id'];
            $boleto->ordemBoleto = $numeroDoProximoBoleto;
            $boleto->dadosboleto["data_vencimento"] = date('d/m/Y'); // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
            //$boleto->dadosboleto["valor_boleto"] = str_replace('.', ',', $despesa['valor'] + $despesa['correcao_igpm'] + $despesa['multa']); 	// Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgula
            $boleto->dadosboleto["valor_boleto"] = number_format($despesa['valor'] + $despesa['correcao_igpm'] + $despesa['multa'], 2, ',', '.');
            $boleto->dadosboleto["sacado"] = $usuario['Usuario']['nome'];
            $multa = number_format($turma['Turma']['multa_por_atraso'], 2, ',', '');
            $boleto->dadosboleto["instrucoes2"] = "AP&Oacute;S O VENCIMENTO APLICAR MULTA DE $multa% SOBRE O VALOR. INCLUIR JUROS/MORA de R$ 0,10 AO DIA.";
            $boleto->dadosboleto["demonstrativo2"] = "AP&Oacute;S O VENCIMENTO APLICAR MULTA DE $multa% SOBRE O VALOR. INCLUIR JUROS/MORA de R$ 0,10 AO DIA.";

            $pagamento = array(
                'usuario_id' => $usuario['Usuario']['id'],
                'valor_nominal' => $despesa['valor'] + $despesa['correcao_igpm'],
                'codigo' => $boleto->gerarNumeroDocumento(),
                'dt_cadastro' => date('Y-m-d H:i:s'),
                'dt_vencimento' => $despesa['data_vencimento'],
                'tipo' => 'boleto',
                'status' => 'aberto',
            );

            $despesa_pagamento = array(
                'despesa_id' => $despesa['id'],
                'valor' => $despesa['valor'],
                'multa' => $despesa['multa'],
                'correcao_igpm' => $despesa['correcao_igpm'] + 0
            );
            $dados['Pagamento'] = $pagamento;
            $dados['DespesaPagamento'] = array($despesa_pagamento);

            if ($numeroDoProximoBoleto > 999) {
                return false;
            } else {
                if ($this->Pagamento->saveAll($dados)) {
                    $pagamento = $this->Pagamento->read();
                } else {
                    return false;
                }
            }
        }
        $this->boletoConta->iniciar(false, $usuario, $usuarioTurma);
        if($verificado == 0) {
            $this->layout = 'metro/externo';
            $this->set('boleto',$boleto->obterDados());
            $this->set('despesa',$this->boletoConta->despesaId);
            $this->set('banco',$this->boletoConta->dadosboleto['nome_configuracao']);
            $this->render('verificar_boleto');
        } else {
            $this->autoRender = false;
            $boleto->imprimir($this->webroot);
        }
        return $return;
    }

    function atendimento_despesa_alterar_data() {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $dados['error'] = true;
        $this->Despesa->id = $_POST['despesa'];
        $despesa = $this->Despesa->read(null,$this->Despesa->id);
        $data = implode("-",array_reverse(explode("/", $_POST['data'])));
        if($this->Despesa->saveField("data_vencimento", $data)) {
            $dados['error'] = false;
            foreach($despesa['DespesaPagamento'] as $despesaPagamento) {
                $pagamento = $this->Pagamento->read(null,$despesaPagamento['pagamento_id']);
                if($pagamento) {
                    $this->Pagamento->id = $despesaPagamento['pagamento_id'];
                    if(!$this->Pagamento->saveField("dt_vencimento", $data))
                        $dados['error'] = true;
                }
            }
        }
        echo json_encode($dados);
    }

    function super_despesa_alterar_data(){

        $this->atendimento_despesa_alterar_data();
    }

    function atendimento_despesa_alterar_valor() {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $dados['error'] = true;
        $this->Despesa->recursive = 0;
        $despesa = $this->Despesa->find('first', array('conditions' => "id = {$_POST['despesa']}"));
        $this->FormandoProfile->recursive = 0;
        $formando = $this->FormandoProfile->find('first', array('conditions' => "usuario_id = {$despesa['Despesa']['usuario_id']}"));
        $this->Despesa->id = $_POST['despesa'];
        $this->FormandoProfile->id = $formando['FormandoProfile']['id'];
        $dados['diferenca'] = $_POST['valor'] - $despesa['Despesa']['valor'];
        if ($this->Despesa->saveField("valor", $_POST['valor'])) {
            $despesaPagamento = $this->DespesaPagamento->find('first', array('conditions' => array("DespesaPagamento.despesa_id = {$_POST['despesa']}")));
            $this->Pagamento->id = $despesaPagamento["DespesaPagamento"]["pagamento_id"];
            if ($this->Pagamento->saveField("valor_nominal", $_POST['valor']))
                $dados['error'] = false;
        }
        echo json_encode($dados);
    }

    function atendimento_ordem_pagamento($protocolo) {
        $this->loadModel('ProtocoloCancelamento');
        $this->loadModel('Protocolo');
        $this->layout = 'impressao_talao';
        $protocolo = $this->ProtocoloCancelamento->find('first', array('conditions' => array('Protocolo.protocolo' => $protocolo)));
        if ($protocolo['Protocolo']['status'] == 'finalizado') {
            $this->Session->setFlash('Protocolo já foi finalizado.', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/solicitacoes/cancelamento/{$protocolo['Protocolo']['protocolo']}");
        } elseif ($protocolo['ProtocoloCancelamento']['ordem_pagamento'] == 1) {
            $this->Session->setFlash('Ordem de pagamento j&aacute; foi gerada.', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/solicitacoes/cancelamento/{$protocolo['Protocolo']['protocolo']}");
        } elseif (empty($protocolo['ProtocoloCancelamento']['nome_titular']) || empty($protocolo['ProtocoloCancelamento']['cpf_titular']) ||
                empty($protocolo['ProtocoloCancelamento']['banco']) || empty($protocolo['ProtocoloCancelamento']['agencia']) ||
                empty($protocolo['ProtocoloCancelamento']['conta'])) {
            $this->Session->setFlash('Complete os dados da conta favorecida.', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/solicitacoes/cancelamento/{$protocolo['Protocolo']['protocolo']}");
        } elseif (!$this->ProtocoloCancelamento->updateAll(
                        array('ProtocoloCancelamento.ordem_pagamento' => "1"), array('ProtocoloCancelamento.id' => $protocolo['ProtocoloCancelamento']['id']))) {
            $this->Session->setFlash('Erro ao gerar ordem de pagamento. Entre em contato com o Administrador.', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/solicitacoes/cancelamento/{$protocolo['Protocolo']['protocolo']}");
        } elseif (!$this->Protocolo->updateAll(
                        array('Protocolo.status' => "'finalizado'"), array('Protocolo.id' => $protocolo['Protocolo']['id']))) {
            $this->Session->setFlash('Erro ao gerar ordem de pagamento. Entre em contato com o Administrador.', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/solicitacoes/cancelamento/{$protocolo['Protocolo']['protocolo']}");
        } else {
            $usuario = $this->ViewFormandos->find('first', array('conditions' => array('id' => $protocolo['Protocolo']['usuario_id'])));
            $this->set('usuario', $usuario);
            $this->set('protocolo', $protocolo);
            $pagamentos = $this->Pagamento->find('all', array('conditions' =>
                array('Pagamento.status' => 'pago', 'Pagamento.usuario_id' => $protocolo['Protocolo']['usuario_id'])));
            $totalPago = 0;
            foreach ($pagamentos as $pagamento) {
                if (sizeof($pagamento['DespesaPagamento']) == 0) {
                    if ($pagamento['Pagamento']['codigo'] != "") {
                        if ((int) substr($pagamento['Pagamento']['codigo'], 1, 1) < 5)
                            $totalPago+= $pagamento['Pagamento']['valor_nominal'];
                    }
                } else {
                    $totalPago+= $pagamento['Pagamento']['valor_nominal'];
                }
            }
            $this->set('totalPago', $totalPago);
            $saldoCancelamento = ($usuario['ViewFormandos']['valor_adesao'] * 0.2) - $totalPago;
            if ($saldoCancelamento >= 0) {
                $this->Session->setFlash('Formando não tem valor a receber.', 'flash_erro');
                $this->redirect("/{$this->params['prefix']}/solicitacoes/cancelamento/{$protocolo['Protocolo']['protocolo']}");
            } else {
                $this->set('saldoCancelamento', $saldoCancelamento * -1);
            }
        }
    }
    
    function super_procurar_formando() {
        $this->layout = false;
        if ($this->data) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $formando = $this->ViewFormandos->find('first', array('conditions' => array('ViewFormandos.codigo_formando' => $this->data['AreaFinanceira']['codigo_formando'])));
            echo json_encode(array('id' => $formando['ViewFormandos']['id']));
        }else{
            $this->render('super_procurar_formando');
        }
    }

    function super_despesas_formando($usuarioId) {
        $this->formando_index($usuarioId);
    }

    function super_buscar_formando() {
        if ($this->data) {
            $formando = $this->ViewFormandos->find('first', array('conditions' => array('codigo_formando' => $this->data['codigo_formando'])));
            if (!empty($formando))
                $this->redirect("/{$this->params['prefix']}/area_financeira/despesas_formando/{$formando['ViewFormandos']['id']}");
            else
                $this->Session->setFlash("Código do formando {$this->data['codigo_formando']} não encontrado.", 'flash_erro');
        }
    }

    function super_desativar_formando($usuarioId = null, $turma_id = null) {
        if (!$this->Usuario->delete($usuarioId)) {
            $formando = $this->ViewFormandos->find('first', array('conditions' => array('id' => $usuarioId)));
            $this->set('formando', $formando);
            //	$this->Pagamento->deleteAll(array('Pagamento.usuario_id' => $usuarioId));
            $this->Despesa->updateAll(array('Despesa.status' => "'cancelada'"), array('Despesa.usuario_id' => $usuarioId));
            $this->FormandoProfile->updateAll(array('FormandoProfile.situacao' => "'cancelado'"), array('FormandoProfile.usuario_id' => $usuarioId));
            $this->Usuario->updateAll(array('Usuario.ativo' => 0), array('Usuario.id' => $usuarioId));
            $this->Session->setFlash('Formando desativado com sucesso.', 'flash_sucesso');
        } else {
            $this->Session->setFlash('Ocorreu um erro ao desativar o formando.', 'flash_erro');
        }
        $this->redirect("/{$this->params['prefix']}/area_financeira/buscar_formando");
    }

    function super_buscar_turma() {
        
    }

    function super_despesas_turma($turmaId = null) {
        $formandos = $this->ViewFormandos->find('all', array('conditions' => array('turma_id' => $turmaId, 'situacao' => 'ativo')));
        $relatorioFormandos = array();
        foreach ($formandos as $formando) {
            if ($formando['ViewFormandos']['situacao'] != 'cancelado') {
                $adesoes = $this->Despesa->find('all', array('conditions' => array('Despesa.usuario_id' => $formando['ViewFormandos']['id'], 'Despesa.tipo' => 'adesao')));
                $this->Despesa->bindModel(array('belongsTo' => array('CampanhasUsuario')), false);
                $this->Despesa->recursive = 2;
                $this->CampanhasUsuario->bindModel(array('belongsTo' => array('Campanha')), false);
                $extras = $this->Despesa->find('all', array('conditions' =>
                    array('Despesa.usuario_id' => $formando['ViewFormandos']['id'], 'Despesa.tipo' => 'extra')));
                $pagamentos = $this->Pagamento->find('all', array('conditions' => array('Pagamento.status' => 'pago',
                        'Pagamento.usuario_id' => $formando['ViewFormandos']['id'])));
                $pagamentos_nao_vinculados = array();
                $totalAdesao = 0;
                $pagoAdesao = 0;
                $totalExtras = 0;
                $pagoExtras = 0;
                $totalPago = 0;
                $totalPagoNaoVinculado = 0;
                $parcelasAbertas = 0;
                $totalIGPM = array('despesa_aberta' => 0, 'despesa_paga' => 0);
                $totalDespesasEmAberto = 0;
                foreach ($adesoes as $index => $adesao) {
                    $totalAdesao += $adesao['Despesa']['valor'];
                    if ($adesao['Despesa']['status'] == 'aberta') {
                        $totalIGPM['despesa_aberta'] +=!empty($adesao['Despesa']['correcao_igpm']) ? $adesao['Despesa']['correcao_igpm'] : 0;
                        $totalDespesasEmAberto += $adesao['Despesa']['valor'] + $adesao['Despesa']['multa'] + $adesao['Despesa']['correcao_igpm'];
                        $parcelasAbertas++;
                    } elseif ($adesao['Despesa']['status'] == 'paga') {
                        $totalIGPM['despesa_paga'] +=!empty($adesao['Despesa']['correcao_igpm']) ? $adesao['Despesa']['correcao_igpm'] : 0;
                    }
                    if (sizeof($adesao['DespesaPagamento']) != 0 && $adesao['Despesa']['status'] == 'paga') {
                        $valorPago = $adesao['DespesaPagamento'][0]['Pagamento']['valor_nominal'] > $adesao['DespesaPagamento'][0]['Pagamento']['valor_pago'] ? $adesao['DespesaPagamento'][0]['Pagamento']['valor_pago'] : $adesao['DespesaPagamento'][0]['Pagamento']['valor_nominal'];
                        $valorPago = $valorPago > $adesao['Despesa']['valor'] ? $adesao['Despesa']['valor'] : $valorPago;
                        $pagoAdesao+= $valorPago;
                    }
                }
                foreach ($extras as $extra) {
                    $totalExtras += $extra['Despesa']['valor'];
                    if ($extra['Despesa']['status'] == 'aberta') {
                        $totalDespesasEmAberto += $extra['Despesa']['valor'];
                    }
                    if (sizeof($extra['DespesaPagamento']) != 0 && $extra['Despesa']['status'] == 'paga') {
                        $valorPago = $extra['DespesaPagamento'][0]['Pagamento']['valor_nominal'] > $extra['DespesaPagamento'][0]['Pagamento']['valor_pago'] ? $extra['DespesaPagamento'][0]['Pagamento']['valor_pago'] : $extra['DespesaPagamento'][0]['Pagamento']['valor_nominal'];
                        $valorPago = $valorPago > $extra['Despesa']['valor'] ? $extra['Despesa']['valor'] : $valorPago;
                        $pagoExtras+= $valorPago;
                    }
                }
                App::import('Component', 'Igpm');
                $igpmComponent = new IgpmComponent();
                $despesaIgpm = $this->Despesa->find('first', array('conditions' => array('tipo' => 'igpm',
                        'Despesa.usuario_id' => $formando['ViewFormandos']['id'])));
                $totalPagoIgpm = 0;
                if ($despesaIgpm) {
                    $totalIGPM['despesa_paga'] = $despesaIgpm['Despesa']['valor'];
                    $totalPagoIgpm = $igpmComponent->obterTotalPago($formando['ViewFormandos']);
                }
                foreach ($pagamentos as $pagamento)
                    if (sizeof($pagamento['DespesaPagamento']) == 0)
                        $pagamentos_nao_vinculados[] = $pagamento;
                $this->loadModel("ViewRelatorioExtras");
                $relatorioExtras = $this->ViewRelatorioExtras->find('all', array('conditions' => array('usuario_id' => $formando['ViewFormandos']['id'])));
                $totalPago = $pagoAdesao + $pagoExtras + $totalPagoNaoVinculado;
                $totalDespesas = $totalExtras + $totalAdesao + array_sum($totalIGPM);
                $relatorioFormandos[] = array(
                    'nome' => $formando['ViewFormandos']['nome'],
                    'codigo_formando' => $formando['ViewFormandos']['codigo_formando'],
                    'situacao' => $formando['ViewFormandos']['situacao'],
                    'data_adesao' => $formando['ViewFormandos']['data_adesao'],
                    'qtde_parcelas' => isset($adesoes[0]) ? $adesoes[0]['Despesa']['total_parcelas'] : 0,
                    'parcelas_abertas' => $parcelasAbertas,
                    'total_adesao' => $totalAdesao,
                    'pago_adesao' => $pagoAdesao,
                    'total_extras' => $totalExtras,
                    'pago_extras' => $pagoExtras,
                    'total_aberto' => $totalDespesasEmAberto,
                    'extras' => $relatorioExtras,
                    'total_igpm' => array_sum($totalIGPM),
                    'pago_igpm' => $totalPagoIgpm,
                    'extras' => $relatorioExtras
                );
            }
            $this->set('formandos', $relatorioFormandos);
        }
    }
    
    private function obterPagamento($despesa) {
        $despesaPagamento = $this->DespesaPagamento->find('first',
                array('conditions' => array(
                    'DespesaPagamento.despesa_id' => $despesa,
                    'Pagamento.status' => 'pago')));
        return $despesaPagamento['Pagamento'];
    }
    
    function super_validar_igpm() {
        ini_set('memory_limit', '2048M');
        set_time_limit(0);
        Configure::write(array('debug' => 0));
        $this->autoRender = false;
        $this->Despesa->recursive = 0;
        $despesasIgpm = $this->Despesa->find('all',
                array('conditions' => array('tipo' => 'igpm')));
        $usuarios = array();
        foreach($despesasIgpm as $despesaIgpm)
            $usuarios[$despesaIgpm['Despesa']['usuario_id']] = array(
                'despesa' => $despesaIgpm['Despesa']['valor'],
                'soma' => 0,
                'despesas' => array()
            );
        $despesasPagas = $this->Despesa->find('all',
            array('conditions' =>
                array(
                    'status' => 'paga',
                    'tipo' => 'adesao',
                    'correcao_igpm is not null',
                    "correcao_igpm >" => "0",
                    'Despesa.usuario_id' => array_keys($usuarios),
                )
            )
        );
        foreach($despesasPagas as $despesaPaga) {
            $despesaPaga['Pagamento'] = $this->obterPagamento($despesaPaga["Despesa"]['id']);
            $usuario = $despesaPaga["Despesa"]['usuario_id'];
            $valorCreditado = min(array(
                $despesaPaga['Pagamento']['valor_nominal'],
                $despesaPaga['Pagamento']['valor_pago'],
                //$despesaPaga['Despesa']['valor']
            ));
            $saldo = array_sum(array(
                $despesaPaga['Despesa']['valor'],
                $despesaPaga['Despesa']['correcao_igpm'],
                //$despesaPaga['Despesa']['multa']
            ));
            $diferenca = $saldo - $valorCreditado;
            $usuarios[$usuario]['despesas'][] = array(
                'valor' => $despesaPaga['Despesa']['valor'],
                'igpm' => $despesaPaga['Despesa']['correcao_igpm'],
                'multa' => $despesaPaga['Despesa']['multa'],
                'saldo' => $saldo,
                'credito' => $valorCreditado,
                'diferenca' => $diferenca
            );
            if(floor($diferenca) > 0)
                $usuarios[$usuario]['soma']+=
                        $despesaPaga['Despesa']['correcao_igpm'];
        }
        header("Content-type: text/plain");
        echo sprintf("%s;%s;%s;\n",'usuario','despesa','soma');
        ksort($usuarios);
        foreach($usuarios as $id => $usuario)
            echo sprintf("%s;%s;%s;\n",$id,
                    "R$".number_format($usuario['despesa'], 2, ',', '.'),
                    "R$".number_format($usuario['soma'], 2, ',', '.'));
    }
    
    function super_validar_igpm_usuario($uid) {
        Configure::write(array('debug' => 0));
        $this->autoRender = false;
        $this->Despesa->recursive = 0;
        $despesasIgpm = $this->Despesa->find('all',
                array('conditions' => array('tipo' => 'igpm','usuario_id' => $uid)));
        $usuarios = array();
        foreach($despesasIgpm as $despesaIgpm)
            $usuarios[$despesaIgpm['Despesa']['usuario_id']] = array(
                'despesa' => $despesaIgpm['Despesa']['valor'],
                'soma' => 0,
                'despesas' => array()
            );
        $despesasPagas = $this->Despesa->find('all',
            array('conditions' =>
                array(
                    'status' => 'paga',
                    'tipo' => 'adesao',
                    'correcao_igpm is not null',
                    "correcao_igpm >" => "0",
                    'Despesa.usuario_id' => $uid,
                )
            )
        );
        foreach($despesasPagas as $despesaPaga) {
            $despesaPaga['Pagamento'] = $this->obterPagamento($despesaPaga["Despesa"]['id']);
            $usuario = $despesaPaga["Despesa"]['usuario_id'];
            $valorCreditado = min(array(
                $despesaPaga['Pagamento']['valor_nominal'],
                $despesaPaga['Pagamento']['valor_pago'],
                //$despesaPaga['Despesa']['valor']
            ));
            $saldo = array_sum(array(
                $despesaPaga['Despesa']['valor'],
                $despesaPaga['Despesa']['correcao_igpm'],
                //$despesaPaga['Despesa']['multa']
            ));
            $diferenca = $saldo - $valorCreditado;
            $v = array(
                'parcela' => $despesaPaga['Despesa']['parcela'],
                'valor' => $despesaPaga['Despesa']['valor'],
                'igpm' => $despesaPaga['Despesa']['correcao_igpm'],
                'multa' => $despesaPaga['Despesa']['multa'],
                'boleto' => $despesaPaga['Pagamento']['valor_nominal'],
                'pago' => $despesaPaga['Pagamento']['valor_pago'],
                'saldo' => $saldo,
                'credito' => $valorCreditado,
                'diferenca' => $diferenca,
                'somou' => 'Nao'
            );
            if(floor($diferenca) > 0) {
                $usuarios[$usuario]['soma']+=
                        $despesaPaga['Despesa']['correcao_igpm'];
                $v['somou'] = 'Sim';
            }
            $usuarios[$usuario]['despesas'][] = $v;
        }
        header("Content-type: text/plain");
        echo sprintf("%-5s%-10s%-10s%-10s%-10s%-10s%-10s%-10s%-10s%-10s\n",'parcela','despesa','igpm','multa','saldo','nominal','pago','creditado','diferenca','somou');
        foreach($usuarios as $id => $usuario)
            foreach($usuario['despesas'] as $despesa)
                echo sprintf("%-5s%-10s%-10s%-10s%-10s%-10s%-10s%-10s%-10s%-10s\n",
                        $despesa['parcela'],
                        "R$".number_format($despesa['valor'], 2, ',', '.'),
                        "R$".number_format($despesa['igpm'], 2, ',', '.'),
                        "R$".number_format($despesa['multa'], 2, ',', '.'),
                        "R$".number_format($despesa['saldo'], 2, ',', '.'),
                        "R$".number_format($despesa['boleto'], 2, ',', '.'),
                        "R$".number_format($despesa['pago'], 2, ',', '.'),
                        "R$".number_format($despesa['credito'], 2, ',', '.'),
                        "R$".number_format($despesa['diferenca'], 2, ',', '.'),
                        $despesa['somou']);
    }
    
    function super_validar_valor_turma($turmaId = false) {
        if($turmaId) {
            $this->autoRender = false;
            $formandos = $this->ViewFormandos->find('all',array(
                'conditions' => array('turma_id' => $turmaId)
            ));
            App::import('Component', 'financeiro');
            $financeiro = new FinanceiroComponent();
            header("Content-type: text/plain");
            echo sprintf("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;\n",
                    'id','codigo','valor_contrato',
                    'valor_despesas_adesao',
                    'valor_igpm','valor_extras',
                    'pago_adesao','pago_igpm',
                    'pago_extras','pago_nao_vinculado',
                    'pago_cancelada','saldo');
            foreach($formandos as $formando) {
                $dados = $financeiro->resumoFormando($formando['ViewFormandos']['codigo_formando']);
                $saldo = array_sum($dados['despesa']);
                $pago = array_sum($dados['pagamento']);
                echo sprintf("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;\n",
                    $formando['ViewFormandos']['id'],
                    $formando['ViewFormandos']['codigo_formando'],
                    $formando['ViewFormandos']['valor_adesao'],
                    $dados['despesa']['adesao'],
                    $dados['despesa']['igpm'],
                    $dados['despesa']['extra'],
                    $dados['pagamento']['adesao'],
                    $dados['pagamento']['igpm'],
                    $dados['pagamento']['extra'],
                    $dados['pagamento']['nao_vinculados'],
                    $dados['pagamento']['canceladas'],
                    ($saldo-$pago));
            }
        }
    }
    
    function super_pagamentos(){
        $this->layout = false;
        header("Content-Type: text/html; charset=UTF-8",true);
        set_time_limit(0);
        ini_set("memory_limit", "4096M");
        $pagamentos = $this->Pagamento->find('all');
        $this->set('pagamentos', $pagamentos);
    }

    public function gerar_boleto($despesa_id, $usuario_id) {
        try {

            $usuario = $this->Usuario->findById($usuario_id);
            
            $url = $this->BoletoBradesco->gerarBoleto($despesa_id, $usuario);

            $this->redirect( $url );
            
        } catch (Exception $e) {
            
            $this->Session->setFlash($e->getMessage(), 'metro/flash/error');

            $this->redirect("/");
        }
    }

    public function atendimento_gerar_boleto($despesa_id, $usuario_id) {

        $this->gerar_boleto($despesa_id, $usuario_id);
    }

    public function formando_gerar_boleto($despesa_id, $usuario_id) {

        $this->gerar_boleto($despesa_id, $usuario_id);
    }

    public function financeiro_gerar_boleto($despesa_id, $usuario_id) {

        $this->gerar_boleto($despesa_id, $usuario_id);
    }

}
