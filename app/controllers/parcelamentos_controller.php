<?php

class ParcelamentosController extends AppController {

    var $name = 'Parcelamentos';
    var $uses = array('Parcelamento', 'Turma');
    var $nomeDoTemplateSidebar = 'parcelamentos';
    
    private function _listar() {
        $this->layout = false;
        $turma = $this->Session->read('turma');
        $this->Parcelamento->recursive = -1;
        $cond = array('turma_id' => $turma['Turma']['id']);
        $parcelamentos = $this->Parcelamento->find('all', array(
            'conditions' => $cond,
            'fields' => array('data', 'parcelas', 'valor', 'titulo', 'id'),
            'group' => array('titulo')
        ));
        $this->set("parcelamentos", $parcelamentos);
        $this->render('_listar');
    }
    
    private function _inserir(){
        $this->layout = false;
        if(!empty($this->data)){
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            App::import('Vendor', 'PHPExcel', array('file' => 'PHPExcel' . DS . 'IOFactory.php'));
            $insert_data = array();

            /*
              $insert_data = array(
              array('turma_id' => 25, 'data' => '2011-10-03', 'parcelas' => 8 , 'valor' => '2430.32'),
              array('turma_id' => 25, 'data' => '2011-10-04', 'parcelas' => 9 , 'valor' => '1234.56'),
              );
             */
            $this->loadModel('Arquivo');
            $data = $_POST['data']['Parcelamaker'];
            $this->Session->read('Usuario');
            $turma = $this->obterTurmaLogada();
            $validate = $this->Parcelamento->find('count', array('conditions' => array('turma_id' => $turma['Turma']['id'], 'titulo' => $this->data['Parcelamaker']['titulo'])));
            if ($this->data['Parcelamaker']['titulo'] == "") {
                $this->Session->setFlash('Digite um t&iacute;tulo para este parcelamento.', 'metro/flash/error');
            } elseif ($this->data['mesas'] == "") {
                $this->Session->setFlash('Preencha as mesas do contrato.', 'metro/flash/error');
            } elseif ($this->data['convites'] == "") {
                $this->Session->setFlash('Preencha as convites do contrato.', 'metro/flash/error');
            } elseif ($validate > 0) {
                $this->Session->setFlash('J&aacute; existe um Parcelamaker cadastrado com este t&iacute;titulo para esta turma.', 'metro/flash/error');
            } else {
                //tratar arquivo enviado
                    $data['nome_secreto'] = $this->Arquivo->generateUniqueId();
                    $data['diretorio'] = $this->Arquivo->file_path();
                    $data['path'] = $data['diretorio'].
                        $data['nome_secreto'];
                    if($this->Arquivo->saveBase64File($data['src'],$data['path'])) {
                        $arquivo = $this->Arquivo->Create();
                        $arquivo = $data['path'];
                    }else{
                        echo "erro";
                    }
                //echo "Arquivo = $arquivo 2";

                    if (!file_exists($arquivo)) {
                        echo 'nao encontrou!!!';
                        exit("Arquivo não encontrado.\n");
                    }
                    
                    ini_set('memory_limit', '256M');
                    $dados = array();

                    $nome_arquivo = $this->data['Parcelamaker']['arquivo'];
                    $arr_arquivo = explode('.', $nome_arquivo);
                    $extensao = $arr_arquivo[count($arr_arquivo) - 1];
                    $objReader = null;
                    
                    switch ($extensao) {
                        case 'xlsx' :
                            $objReader = new PHPExcel_Reader_Excel2007();
                            break;
                        case 'xls' :
                            $objReader = new PHPExcel_Reader_Excel5();
                            break;
                        case 'ods' :
                            $objReader = new PHPExcel_Reader_OOCalc();
                            break;
                    }

                    $objReader->setReadDataOnly(true);
                    $objPHPExcel = $objReader->load($arquivo);
                    $objPHPExcel->setActiveSheetIndex(0);
                    $rowIterator = $objPHPExcel->getActiveSheet()->getRowIterator();
                    $parcelasLinha = 0;

                    $colunas = array();

                   foreach ($rowIterator as $row) {
                //Pular a primeira linha
                if ($row->getRowIndex() == 1)
                    continue;
                //global $dados,$count,$parcelasLinha;
                // Se for a linha 2, armazenar os meses de começo de pagamento	
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(true);
                foreach ($cellIterator as $cell) {

                  

                    if ($row->getRowIndex() != 2) {
                        // A partir da 3a linha
                        //Se for a primeira coluna, guarda o número de parcelas da linha
                        if ($cell->getColumn() == 'A') {
                            $parcelasLinha = $cell->getCalculatedValue();
                            continue;
                        }
                        if ($parcelasLinha > 0) {
                            
                            global $dados, $count;
                            //salvar o valor total da parcela na matriz principal
                            $vlr = $cell->getCalculatedValue();
                            //if($vlr == '') debug("Valor: " . $vlr . '#' . $colunas[$cell->getColumn()]);
                            if ($vlr != '') {

                                if (!isset($dados[$colunas[$cell->getColumn()]])) {
                                    $dados[$colunas[$cell->getColumn()]] = array();
                                }
                                $valor_celula = $cell->getCalculatedValue();

                                if ($valor_celula == 'z')
                                    $valor_celula = 0;

                                if ($cell->getCalculatedValue() != '')
                                    $dados[$colunas[$cell->getColumn()]][$parcelasLinha] = $valor_celula;
                            }
                        }
                    } else {
                        // é uma célula com mês de pagamento
                        //Ignorar a célula cabeçalho "Parcelas"
                        if ($cell->getColumn() > 1)
                            continue;

                        $colunas[$cell->getColumn()] = PHPExcel_Style_NumberFormat::toFormattedString($cell->getCalculatedValue(), 'MM/YYYY');
                    }
                }
            }

            //  Terminou de processar, transformar a matriz 'dados' em inserts pro banco de dados:
            $datas = array_keys($dados);

            for ($i = 0; $i < sizeof($datas); $i++) {
                $parcelas = array_keys($dados[$datas[$i]]);

                $inseridos = 0;

                for ($j = 0; $j < sizeof($parcelas); $j++) {
                    $insData = $datas[$i];
                    $mData = explode('/', $insData);
                    $insDataSQL = $mData[1] . '-' . $mData[0] . '-01';
                    $insParcelas = $parcelas[$j];
                    $insValorTotal = $dados[$insData][$insParcelas];

                    $insert_data[] = array(
                        'turma_id' => $turma['Turma']['id'], 
                        'data' => "$insDataSQL", 'parcelas' => $insParcelas, 
                        'valor' => "$insValorTotal", 
                        'titulo' => $this->data['Parcelamaker']['titulo'],
                        'mesas_contrato' => $this->data['mesas'],
                        'convites_contrato' => $this->data['convites']
                    );
                }
            }


                    //Antes de inserir, apaga tudo o que havia anteriormente
                    //$this->Parcelamento->deleteAll(array('turma_id' => $turma['Turma']['id']));

                    if ($this->Parcelamento->saveAll($insert_data)) {
                        $this->Session->setFlash('Parcelamaker processado com sucesso.', 'metro/flash/success');
                        unlink($data['diretorio']);
                    } else {
                        $this->Session->setFlash('Ocorreu um erro ao processar o Parcelamaker.', 'metro/flash/error');
                    }
                    json_encode(array());
            }
        }
    }
    
    function planejamento_inserir(){
        $this->_inserir();
    }
    
    function comissao_listar() {
        $this->_listar();
    }
    
    function planejamento_listar(){
        $this->_listar();
    }
    
    

    function planejamento_index() {
        $turma = $this->Session->read('turma');
        $this->Parcelamento->recursive = -1;
        $cond = array('turma_id' => $turma['Turma']['id']);
        $parcelamentos = $this->Parcelamento->find('all', array('conditions' => $cond, 'fields' => array('data', 'parcelas', 'valor', 'titulo', 'id'), 'group' => array('titulo')));
        $this->set("parcelamentos", $parcelamentos);
    }
    
    private function _exibir($idParcelamento) {
        $turma = $this->Session->read('turma');
        $this->Parcelamento->recursive = -1;
        $parcelamaker = $this->Parcelamento->find('first', array(
            'conditions' => array('Parcelamento.id' => $idParcelamento)));
        $cond = array(
            'turma_id' => $turma['Turma']['id'],
            'titulo' => $parcelamaker['Parcelamento']['titulo']
        );
        $parcelamentos = $this->Parcelamento->find('all',array(
            'conditions' => $cond,
            'fields' => array('data', 'parcelas', 'valor', 'titulo'))
        );

        $dados = array();
        $lista_parcelas = array();

        foreach ($parcelamentos as $parcelamento):
            $data = $parcelamento['Parcelamento']['data'];
            setlocale(LC_TIME, 'pt_BR');
            $aux = strftime('%b/%y', strtotime($data));

            $parcelas = $parcelamento['Parcelamento']['parcelas'];
            $valor = $parcelamento['Parcelamento']['valor'];

            if (!in_array($parcelas, $lista_parcelas))
                array_push($lista_parcelas, $parcelas);
            $dados[$aux][$parcelas] = $valor;
        endforeach;
        
        $this->set('dados', $dados);
        $this->set('listaParcelas', $lista_parcelas);
        $this->set('parcelamaker', $parcelamaker);
        $this->render('_exibir');
    }
    
    function comissao_exibir($idParcelamento) {
        $this->planejamento_exibir($idParcelamento);
    }
    
    function planejamento_exibir($idParcelamento) {
        $this->_exibir($idParcelamento);
    }

    function planejamento_visualizar($idParcelamento) {
        $turma = $this->Session->read('turma');
        $this->Parcelamento->recursive = -1;
        $parcelamaker = $this->Parcelamento->find('first', array('conditions' => array('Parcelamento.id' => $idParcelamento)));
        $cond = array('turma_id' => $turma['Turma']['id'], 'titulo' => $parcelamaker['Parcelamento']['titulo']);
        $parcelamentos = $this->Parcelamento->find('all', array('conditions' => $cond, 'fields' => array('data', 'parcelas', 'valor', 'titulo')));

        $dados = array();
        $lista_parcelas = array();

        foreach ($parcelamentos as $parcelamento):
            $data = $parcelamento['Parcelamento']['data'];
            setlocale(LC_TIME, 'pt_BR');
            $aux = strftime('%b/%y', strtotime($data));

            $parcelas = $parcelamento['Parcelamento']['parcelas'];
            $valor = $parcelamento['Parcelamento']['valor'];

            if (!in_array($parcelas, $lista_parcelas))
                array_push($lista_parcelas, $parcelas);
            $dados[$aux][$parcelas] = $valor;
        endforeach;
        $this->set('dados', $dados);
        $this->set('listaParcelas', $lista_parcelas);
        $this->set('parcelamaker', $parcelamaker);
    }

    function comissao_index() {
        $this->planejamento_index();
    }

    function planejamento_apagar($idParcelamento) {
        $this->layout = false;
        $turma = $this->obterTurmaLogada();
        if(!empty($idParcelamento)){
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $this->Parcelamento->unbindModelAll();
            $this->loadModel('ViewFormandos');
            $parcelamaker = $this->Parcelamento->find('first', array('conditions' => array('Parcelamento.id' => $idParcelamento)));
            $cond = array('turma_id' => $turma['Turma']['id'], 'titulo' => $parcelamaker['Parcelamento']['titulo']);
            $idsParcelamaker = $this->Parcelamento->find('all', array(
                'conditions' => $cond
            ));
            $ids = array();
            foreach($idsParcelamaker as $idParcelamaker)
                $ids[] = $idParcelamaker['Parcelamento']['id'];
            $formandos = $this->ViewFormandos->find('all', array(
                'conditions' => array(
                    'ViewFormandos.turma_id' => $turma['Turma']['id'],
                    'FormandoProfile.parcelamento_id' => $ids
                ), 
                'joins' => array(
                    array(
                        "table" => "formandos_profile",
                        "type" => "inner",
                        "alias" => "FormandoProfile",
                        "conditions" => array(
                            "ViewFormandos.id = FormandoProfile.usuario_id",
                        )
                    )
                ),
                'fields' => array(
                    'ViewFormandos.*',
                    'FormandoProfile.*'
                )
            ));
            if(count($formandos) == 0){
                if ($this->Parcelamento->deleteAll($cond, false)){
                    $this->Session->setFlash('Parcelamaker deletado com sucesso.', 'metro/flash/success');
                }else{
                    $this->Session->setFlash('Ocorreu um erro ao deletar o parcelamaker.', 'metro/flash/error');
                }
            }else{
                $cdFormando = array();
                foreach($formandos as $formando)
                    $cdFormando[] += $formando['ViewFormandos']['codigo_formando'] . "<br />";
                $msg = "O parcelamento não pode ser deletado, pois há formandos utilizando-o. Segue lista abaixo: <br />" . implode("<br />",$cdFormando);
                $this->Session->setFlash($msg, 'metro/flash/error');
            }
        }
    }

    function planejamento_upload() {
        
    }

    function planejamento_processa_excel() {
        App::import('Vendor', 'PHPExcel', array('file' => 'PHPExcel' . DS . 'IOFactory.php'));
        $insert_data = array();

        /*
          $insert_data = array(
          array('turma_id' => 25, 'data' => '2011-10-03', 'parcelas' => 8 , 'valor' => '2430.32'),
          array('turma_id' => 25, 'data' => '2011-10-04', 'parcelas' => 9 , 'valor' => '1234.56'),
          );
         */

        $turma = $this->Session->read('turma');
        $validate = $this->Parcelamento->find('count', array('conditions' => array('turma_id' => $turma['Turma']['id'], 'titulo' => $this->data['Parcelamaker']['titulo'])));
        if ($this->data['Parcelamaker']['titulo'] == "") {
            $this->Session->setFlash('Digite um t&iacute;tulo para este parcelamento.', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/parcelamentos/upload");
        } elseif ($validate > 0) {
            $this->Session->setFlash('J&aacute; existe um Parcelamaker cadastrado com este t&iacute;titulo para esta turma.', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/parcelamentos/");
        } else {
            //tratar arquivo enviado
            if ($this->data['Parcelamaker']['arquivo']['error'] > 0) {
                echo "Error: " . $this->data['Parcelamaker']['arquivo']['error'] . "<br />";
            } else {
                $arquivo = $this->data['Parcelamaker']['arquivo']['tmp_name'];
            }

            //echo "Arquivo = $arquivo 2";

                if (!file_exists($arquivo)) {
                    echo 'nao encontrou!!!';
                    exit("Arquivo não encontrado.\n");
                }
                ini_set('memory_limit', '256M');
                $dados = array();
                $nome_arquivo = $this->data['Parcelamaker']['arquivo']['name'];
                $arr_arquivo = explode('.', $nome_arquivo);
                $extensao = $arr_arquivo[count($arr_arquivo) - 1];

                $objReader = null;

                switch ($extensao) {
                    case 'xlsx' :
                        $objReader = new PHPExcel_Reader_Excel2007();
                        break;
                    case 'xls' :
                        $objReader = new PHPExcel_Reader_Excel5();
                        break;
                    case 'ods' :
                        $objReader = new PHPExcel_Reader_OOCalc();
                        break;
                }

                $objReader->setReadDataOnly(true);

                $objPHPExcel = $objReader->load($arquivo);
                $objPHPExcel->setActiveSheetIndex(0);
                $rowIterator = $objPHPExcel->getActiveSheet()->getRowIterator();

                $parcelasLinha = 0;

                $colunas = array();

                foreach ($rowIterator as $row) {
                    //Pular a primeira linha
                    if ($row->getRowIndex() == 1)
                        continue;

                    //global $dados,$count,$parcelasLinha;
                    // Se for a linha 2, armazenar os meses de começo de pagamento	
                    $cellIterator = $row->getCellIterator();
                    $cellIterator->setIterateOnlyExistingCells(true);

                    foreach ($cellIterator as $cell) {



                        if ($row->getRowIndex() != 2) {
                            // A partir da 3a linha
                            //Se for a primeira coluna, guarda o número de parcelas da linha
                            if ($cell->getColumn() == 'A') {
                                $parcelasLinha = $cell->getCalculatedValue();
                                debug("P: $parcelasLinha");
                                continue;
                            }


                            if ($parcelasLinha > 0) {
                                global $dados, $count;
                                //salvar o valor total da parcela na matriz principal
                                $vlr = $cell->getCalculatedValue();
                                //if($vlr == '') debug("Valor: " . $vlr . '#' . $colunas[$cell->getColumn()]);
                                if ($vlr != '') {

                                    if (!isset($dados[$colunas[$cell->getColumn()]])) {
                                        $dados[$colunas[$cell->getColumn()]] = array();
                                    }
                                    //debug($cell->getColumn());
                                    $valor_celula = $cell->getCalculatedValue();

                                    if ($valor_celula == 'z')
                                        $valor_celula = 0;

                                    if ($cell->getCalculatedValue() != '')
                                        $dados[$colunas[$cell->getColumn()]][$parcelasLinha] = $valor_celula;
                                }
                            }
                        } else {
                            // é uma célula com mês de pagamento
                            //Ignorar a célula cabeçalho "Parcelas"
                            if ($cell->getColumn() > 1)
                                continue;

                            $colunas[$cell->getColumn()] = PHPExcel_Style_NumberFormat::toFormattedString($cell->getCalculatedValue(), 'MM/YYYY');
                        }
                    }
                }

                //  Terminou de processar, transformar a matriz 'dados' em inserts pro banco de dados:
                $datas = array_keys($dados);

                //debug($dados);

                for ($i = 0; $i < sizeof($datas); $i++) {
                    $parcelas = array_keys($dados[$datas[$i]]);

                    $inseridos = 0;

                    for ($j = 0; $j < sizeof($parcelas); $j++) {
                        $insData = $datas[$i];
                        $mData = explode('/', $insData);
                        $insDataSQL = $mData[1] . '-' . $mData[0] . '-01';
                        $insParcelas = $parcelas[$j];
                        $insValorTotal = $dados[$insData][$insParcelas];

                        $insert_data[] = array('turma_id' => $turma['Turma']['id'], 'data' => "$insDataSQL", 'parcelas' => $insParcelas, 'valor' => "$insValorTotal", 'titulo' => $this->data['Parcelamaker']['titulo']);
                    }
                }

                //Antes de inserir, apaga tudo o que havia anteriormente
                //$this->Parcelamento->deleteAll(array('turma_id' => $turma['Turma']['id']));


                if ($this->Parcelamento->saveAll($insert_data)) {
                    $this->Session->setFlash('Parcelamaker processado com sucesso.', 'flash_sucesso');
                    $this->redirect("/{$this->params['prefix']}/parcelamentos/");
                } else {
                    $this->Session->setFlash('Ocorreu um erro ao processar o Parcelamaker.', 'flash_erro');
                }
        }
    }

    function letraColunaToNumero($letra) {
        debug($letra . "#" . intval($letra));
        return intVal($letra);
    }

}

?>
