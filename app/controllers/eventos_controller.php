<?php

class EventosController extends AppController {

    var $name = 'Eventos';
    var $uses = array('TiposEvento', 'Evento','EventoMapa',
        'EventoMapaColuna','EventoMapaLocal','ViewFormandos', 'Parcelamento');
    var $helpers = array('Calendario');
    var $components = array('MapaDeMesas');
    

    /**
     * Primeira view , aonde vai ter 2 combobox para opção de escolha entre:
     * Tipo de eventos, e Local do evento
     * 
     * @param  boolean $id  [ID da turma]
     * @param  boolean $uid [description]
     * @return [type]       [description]
     */
    function _selecionar_evento ($id = false, $uid = false){
        $this->layout = 'metro/externo';
        if($id){
            $turma = $id;
        }else{
            $t = $this->obterTurmaLogada();
            $turma = $t['Turma']['id'];
        }
        $this->Evento->unbindModel(array(
            'belongsTo' => array('Turma'),
            'hasMany' => array('Extra'),
        ));
        $eventos = $this->Evento->find('all', array(
            'conditions' => array(
                'Evento.turma_id' => $turma,
                'Evento.tipos_evento_id' => array(1,13)
            )
        ));

        $todos = array();
        foreach($eventos as $evento => $ev)
            $todos[$ev['Evento']['id']] = $ev['Evento']['nome'];

        //Carrega os mapas no segundo select
        //Carrega a Model dinamicamente
        $this->loadModel('Mapas');

        //executa a pesquisa de mapas por titulo
        $mapas =  $this->Mapas->find('list', array('fields' => 'Mapas.titulo'));

        $usuario = $this->obterUsuarioLogado();
        $data_liberacao = null;
        $this->loadModel('FormandoMapaHorario');
        if ($usuario['Usuario']['grupo'] == 'formando' || $usuario['Usuario']['grupo'] == 'comissao'){
            $codigoFormando = $usuario['ViewFormandos']['codigo_formando'];
            $horario = $this->FormandoMapaHorario->find('first',array(
                'conditions'=> array(
                    'codigo_formando' => $codigoFormando
                    )
                )
            );
            $data_liberacao = $horario['FormandoMapaHorario']['data_liberacao'];
        }

        $css_stylesheets = array('metro/css/min/metroui/tile.css?v=0.2');

        $this->set('data_liberacao', $data_liberacao);
        $this->set('mapas', $mapas);
        $this->set('eventos', $todos);
        $this->set('uid', $uid);
        $this->set('css_stylesheets', $css_stylesheets);
    }

    /**
     * Primeira view , aonde vai ter 2 combobox para opção de escolha entre:
     * Tipo de eventos, e Local do evento
     * 
     * @param  boolean $id  [ID da turma]
     * @param  boolean $uid [description]
     * @return [type]       [description]
     */
    function _mapa_mesas ($id = false, $uid = false){
        $this->layout = 'metro/externo';
        if($id){
            $turma = $id;
        }else{
            $t = $this->obterTurmaLogada();
            $turma = $t['Turma']['id'];
        }
        $this->Evento->unbindModel(array(
            'belongsTo' => array('Turma'),
            'hasMany' => array('Extra'),
        ));
        $eventos = $this->Evento->find('all', array(
            'conditions' => array(
                'Evento.turma_id' => $turma
            )
        ));
    }

    /**
     * Salva o mapa 
     * @return [type] [json]
     */
    
    function producao_salvar_layout(){

        if (!empty($_POST)) {
            $this->autoRender = false;
            $this->loadModel('TurmaMapa');
            
            $turmaMapa = $this->TurmaMapa->find('first', array (
                    'conditions' => array( 
                        'TurmaMapa.id' => $_POST['TurmaMapaID']
                    )
                )
            );

            //realiza o update do mapa
            if(!empty($turmaMapa)){
                $this->TurmaMapa->id = $turmaMapa ['TurmaMapa']['id'];
                $this->TurmaMapa->saveField('mapa_layout', $_POST['MapaLayout']);
                $setores = array();
                $quantidade_mesas = 0;
                $mapa_layout = json_decode($_POST['MapaLayout']);
                foreach($mapa_layout as $grupo){
                    if($grupo->tipo == 'grupo_mesas'){
                        $setores[$grupo->setor] = $grupo->setor;
                        $quantidade_mesas += ($grupo->linhas * $grupo->colunas);
                    }
                }
                sort($setores);
                $this->TurmaMapa->saveField('mapa_setores', json_encode($setores));
                $this->TurmaMapa->saveField('quantidade_mesas', $quantidade_mesas);
                $this->TurmaMapa->saveField('status', 1);
                
                // Inativa os outros mapas
                $mapas = $this->TurmaMapa->find('all', array(
                    'conditions' => array(
                        'TurmaMapa.turma_id' => $turmaMapa['TurmaMapa']['turma_id'],
                        'TurmaMapa.evento_id' => $turmaMapa['TurmaMapa']['evento_id'],
                        'NOT' => array(
                            'TurmaMapa.id' => $turmaMapa['TurmaMapa']['id'],
                        )
                    ),
                ));

                foreach ($mapas as $value) {
                    $this->TurmaMapa->mudar_status($value['TurmaMapa']['id'], 0);
                }
            }
            $this->producao_mapa($turmaMapa['TurmaMapa']['evento_id'], $turmaMapa['TurmaMapa']['mapa_id']);
            $this->render('producao_mapa');
        }
    }
    
    function formando_salvarMesas(){
        if (empty($_POST))
            return false;

        App::import('Component', 'MapaDeMesas');
        $mapaMesas = new MapaDeMesasComponent();

        $this->autoRender = false;
        $this->loadModel('TurmaMapa');
        
        $turmaMapaId = $_POST['TurmaMapaID'];
        $turmaMapa = $this->TurmaMapa->findById($turmaMapaId);

        $sucesso = false;

        if(!empty($turmaMapa)){
            $mesasEscolhidas = json_decode($_POST['mesasEscolhidas']);
            $usuario         = $this->obterUsuarioLogado();

            $sucesso = $mapaMesas->salvarEscolhaDeMesas($usuario['Usuario']['id'], $turmaMapaId, $turmaMapa['TurmaMapa']['evento_id'], $mesasEscolhidas);
        }

        $this->formando_mapa($turmaMapa['TurmaMapa']['evento_id']);
        $this->set('sucesso', $sucesso);
        $this->render('formando_mapa');
    }
    
    function _mapa_eventos($id = false){

        $evento = $this->Evento->find('first', array('conditions' => array('Evento.id' => $id)));
        $this->set('evento', $evento);

    }

    function planejamento_mapa_mesas(){
        $this->_selecionar_evento();
    }
    
    function planejamento_selecionar_evento(){
        $this->_selecionar_evento();
    }
    
    function atendimento_selecionar_evento($id, $uid){
        $this->_selecionar_evento($id, $uid);
    }
    
    function formando_selecionar_evento(){

        // Verifica se formando realizou o checkout
        $usuario = $this->obterUsuarioReal();

        if(($usuario['Usuario']['grupo']=='formando' || $usuario['Usuario']['grupo']=='comissao')
            && $usuario['ViewFormandos']['realizou_checkout'] == 0) {
            $this->layout = false;
            $this->render('formando_mapa_sem_checkout');
            return;
        }

        $this->_selecionar_evento();

        $t = $this->obterTurmaLogada();
        $turma = $t['Turma']['id'];

        $eventos = $this->MapaDeMesas->retornarEventosPorTurma($turma);
        $this->set('eventos',$eventos);
    }
    
    function producao_selecionar_evento(){
        $this->_selecionar_evento();
    }

    function comercial_mapa_eventos($id = false){
        $this->_mapa_eventos($id);
    }
    
    function criacao_mapa_eventos($id = false){
        $this->_mapa_eventos($id);
    }
    
    function foto_mapa_eventos($id = false){
        $this->_mapa_eventos($id);
    }
    
    function planejamento_mapa_eventos($id = false){
        $this->_mapa_eventos($id);
    }
    
    function producao_mapa_eventos($id = false){
        $this->_mapa_eventos($id);
    }
    
    function rh_mapa_eventos($id = false){
        $this->_mapa_eventos($id);
    }
    
    function video_mapa_eventos($id = false){
        $this->_mapa_eventos($id);
    }
    
    function formando_mapas($id = false) {
        
        $this->layout = false;
        $turma = $this->obterTurmaLogada();
        $this->Evento->unbindModel(array(
            'belongsTo' => array('Turma'),
            'hasMany' => array('Extra'),
        ),false);
        $eventos = $this->Evento->find('first',array(
            'conditions' => array(
                'Evento.id' => $id,
            )
        ));
        $eventosComMapa = array();
        foreach($eventos as $evento)
            if(!empty($evento['EventoMapa']))
                $eventosComMapa[$evento['Evento']['id']] = $evento['Evento']['nome'];
        if(count($eventosComMapa) == 1)
            $this->redirect("/{$this->params['prefix']}/eventos/mapa/".array_shift(array_keys($eventosComMapa)));
        $this->set('eventos',$eventosComMapa);
        
        $this->render('formando_mapa');

    }
    
    private function mesasDisponiveisFormando($usuario) {
        $mesas = 0;
        if(!empty($usuario['ViewFormandos']['mesas_contrato']))
            $mesas+= $usuario['ViewFormandos']['mesas_contrato'];
        $this->loadModel('Protocolo');
        $this->Protocolo->recursive = 2;
        $protocolo = $this->Protocolo->find('first',array(
            'conditions' => array(
                'usuario_id' => $usuario['ViewFormandos']['id'],
                'tipo' => 'checkout'
            )
        ));
        if($protocolo)
            foreach($protocolo['CheckoutUsuarioItem'] as $checkoutItem)
                $mesas+= $checkoutItem['quantidade']*$checkoutItem['CheckoutItem']['quantidade_mesas'];
        $this->loadModel('CampanhasUsuarioCampanhasExtra');
        $this->loadModel('CampanhasExtra');
        $this->loadModel('CampanhasUsuario');
        $this->CampanhasUsuarioCampanhasExtra->unbindModel(array(
            'belongsTo' => array('CampanhasUsuario')
        ),false);
        $this->CampanhasExtra->unbindModel(array(
            'belongsTo' => array('Campanha'),
            'hasAndBelongsToMany' => array('CampanhasUsuarioCampanhaExtra')
        ),false);
        $this->CampanhasUsuario->bindModel(array(
            'hasMany' => array(
                'CampanhasUsuarioCampanhasExtra' => array(
                    'className' => 'CampanhasUsuarioCampanhasExtra',
                    'joinTable' => 'campanhas_usuarios_campanhas_extras',
                    'foreignKey' => 'campanhas_usuario_id'
                )
            )
        ),false);
        $this->CampanhasUsuario->recursive = 3;
        $campanhas = $this->CampanhasUsuario->find('all',array(
            'conditions' => array(
                'usuario_id' => $usuario['ViewFormandos']['id'],
                'cancelada' => 0
            )
        ));
        foreach($campanhas as $campanha)
            foreach($campanha['CampanhasUsuarioCampanhasExtra'] as $extra)
                $mesas+= $extra['CampanhasExtra']['quantidade_mesas']*$extra['quantidade'];
        return $mesas;
    }
    
    function comissao_mapas() {
        $this->formando_mapas();
    }
    
    function comissao_mapa($eventoId) {
        $this->formando_mapa($eventoId);
    }
    
    function planejamento_escolher_local() {
        $this->formando_escolher_local('reserva_id');
    }
    
    function formando_escolher_local($campo = 'usuario_id') {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        if(isset($this->data['formando'])) {
            $usuario = $this->Usuario->read(null,$this->data['formando']);
            if(!$usuario) {
                $this->Session->setFlash('Usuario nao encontrado', 'metro/flash/error');
            } else {
                $lugares = isset($this->data['lugares']) ? $this->data['lugares'] : array();
                $descartes = isset($this->data['descartes']) ? $this->data['descartes'] : array();
                if(!empty($descartes)) {
                    if(!$this->EventoMapaLocal->updateAll(
                        array(
                            "EventoMapaLocal.{$campo}" => $this->data['formando'],
                            'EventoMapaLocal.data_escolhido' => null,
                        ),
                        array('EventoMapaLocal.id' => $descartes)
                    )) {
                        $this->Session->setFlash('Erro ao descartar lugares', 'metro/flash/error');
                        goto fim;
                    }
                }
                $hoje = date('Y-m-d H:i:s');
                $escolhidos = $this->EventoMapaLocal->find('count',array(
                    'conditions' => array(
                        'EventoMapaLocal.usuario_id' => $this->data['formando']
                    )
                ));
                $mesas = $this->mesasDisponiveisFormando($usuario);
                $indisponiveis = $this->EventoMapaLocal->find('all',array(
                    'conditions' => array(
                        'EventoMapaLocal.id' => $lugares,
                        "EventoMapaLocal.usuario_id is not null"
                    )
                ));
                if($indisponiveis) {
                    $this->Session->setFlash('Mesas selecionadas por outro formando',
                            'metro/flash/error');
                } elseif((count($lugares)+$escolhidos) > $mesas) {
                    $this->Session->setFlash('Formando ultrapassou o limite dispon&iacute;vel de mesas',
                            'metro/flash/error');
                } else {
                    if($this->EventoMapaLocal->updateAll(
                        array(
                            "EventoMapaLocal.{$campo}" => $this->data['formando'],
                            'EventoMapaLocal.data_escolhido' => "'$hoje'",
                        ),
                        array('EventoMapaLocal.id' => $lugares)
                    ))
                        $this->Session->setFlash('Lugares escolhidos/reservados com sucesso', 'metro/flash/success');
                    else
                        $this->Session->setFlash('Erro ao escolher/reservar lugares', 'metro/flash/error');
                }
            }
        } else {
            $this->Session->setFlash('Erro ao receber os dados', 'metro/flash/error');
        }
        fim:
            echo json_encode(array());
    }
    
    function atendimento_escolher_local() {
        $this->formando_escolher_local();
    }
    
    function comissao_escolher_local() {
        $this->formando_escolher_local();
    }
    
    function planejamento_mapa_lista($id = false, $tipo = 'web') {
        $turma = $this->obterTurmaLogada();
        $evento = $this->Evento->obterEventoMapa($id);
        $this->set('mapa',$evento);
        $this->set('tipo',$tipo);
        $this->set('turma',$turma);
        if($evento) {
            $this->ViewFormandos->bindModel(array(
                'hasMany' => array(
                    'EventoMapaLocal' => array(
                        'foreignKey' => 'usuario_id'
                    )
                )
            ),false);
            $this->ViewFormandos->recursive = 2;
            $formandos = $this->ViewFormandos->find('all',array(
                'conditions' => array(
                    'ViewFormandos.turma_id' => $turma['Turma']['id'],
                    'ViewFormandos.ativo' => 1,
                    'Protocolo.tipo' => 'checkout'
                ),
                'joins' => array(
                    array(
                        "table" => "protocolos",
                        "type" => "inner",
                        "alias" => "Protocolo",
                        "conditions" => array(
                            "ViewFormandos.id = Protocolo.usuario_id"
                        )
                    )
                ),
                'order' => array(
                    'ViewFormandos.nome' => 'asc'
                )
            ));
            $this->set('formandos',$formandos);
        }
        $this->layout = $tipo == 'web' ? false : 'metro/externo';
        
        
        $this->render("mapa_lista");
    }
    
    function planejamento_mapa_relatorio($id = false, $tipo = 'web') {
        $turma = $this->obterTurmaLogada();
        $evento = $this->Evento->obterEventoMapa($id);
        $this->set('mapa',$evento);
        $this->set('turma',$turma);
        if($evento) {
            $this->ViewFormandos->bindModel(array(
                'hasMany' => array(
                    'EventoMapaLocal' => array(
                        'foreignKey' => 'usuario_id'
                    )
                )
            ),false);
            $this->ViewFormandos->recursive = 2;
            $conditions = array(
                'turma_id' => $turma['Turma']['id'],
                "Protocolo.tipo = 'checkout'"
            );
            $joins = array(
                array(
                    "table" => "protocolos",
                    "type" => "inner",
                    "alias" => "Protocolo",
                    "conditions" => array(
                        "ViewFormandos.id = Protocolo.usuario_id"
                    )
                )
            );
            if($tipo == "web") {
                $this->layout = false;
                $options = array(
                    'conditions' => $conditions,
                    'joins' => $joins,
                    'limit' => 100
                );
                $this->paginate['ViewFormandos'] = $options;
                $formandos = $this->paginate('ViewFormandos');
            } elseif($tipo == 'imprimir') {
                $this->layout = 'metro/externo';
                $formandos = $this->ViewFormandos->find('all',array(
                    'conditions' => $conditions,
                    'joins' => $joins,
                    'order' => 'codigo_formando'
                ));
            }
            $this->set('formandos',$formandos);
        }

        $this->render("mapa_relatorio_$tipo");
    }
    
    function _mapa($id = false, $mapa_id = false) {
        App::import('Component', 'MapaDeMesas');
        $mapaMesas = new MapaDeMesasComponent();

        $this->layout = false;
        $usuario = $this->obterUsuarioLogado();
        $evento = $this->Evento->obterEventoMapa($id);
        $turma = $this->obterTurmaLogada();
       
        $this->loadModel('TurmaMapa');
        $this->loadModel('FormandoMapaHorario');
        $conditions = array(
            'TurmaMapa.turma_id' => $turma['Turma']['id'],
            'TurmaMapa.evento_id' => $id,
        );

        if($usuario['Usuario']['grupo'] == 'formando' 
            || $usuario['Usuario']['grupo'] == 'planejamento' 
            || $usuario['Usuario']['grupo'] == 'atendimento' 
            || ($usuario['Usuario']['grupo'] == 'producao' && !empty($_POST['Consultar']))){
            $conditions['TurmaMapa.status'] = 1;
        }else{
            $mapa_id = ($mapa_id ? $mapa_id : $_POST['Mapa']);
            $conditions['Mapa.id'] = $mapa_id;
        }

        $mapa = $this->TurmaMapa->find('first',array(
            'conditions' =>$conditions,
        ));        
        
        if ($usuario['Usuario']['grupo'] == 'producao') {
            if (!$mapa) {
                $condicoes = array(
                    'turma_id' => $turma['Turma']['id'],
                    'evento_id' => $id,
                    'mapa_id' => $mapa_id,
                    'status' => 0
                );

                $turma_mapa = $this->TurmaMapa->save($condicoes);
                $mapa = $this->TurmaMapa->read();
            }
            
        }

        if($usuario['Usuario']['grupo'] == 'formando'){
            $formado_mapa_horario = $this->FormandoMapaHorario->find('first', array(
                'conditions' => array(
                    'codigo_formando' => $usuario['ViewFormandos']['codigo_formando'],
                    'id_turma_mapa' => $mapa['TurmaMapa']['id'],
                )
            ));
            $this->set('formado_mapa_horario', $formado_mapa_horario);
        }

        $mapa_parametros = $mapa['Mapa']['parametros'] ? $mapa['Mapa']['parametros'] : '{}';
        $mesasSelecionadas = $mapaMesas->carregarMesasSelecionadas($mapa['TurmaMapa']['id']);

        $this->set("mapa_src",$mapa['Mapa']['url_imagem'] );
        $this->set("mapa_parametros",$mapa_parametros );
        $this->set("turmaMapa", $mapa['TurmaMapa']['id']);
        $this->set('mapaLayout', $mapa['TurmaMapa']['mapa_layout']);
        $this->set('mesasSelecionadas', $mesasSelecionadas);
        $this->set('usuario', $usuario);
        $this->set('webroot', $this->webroot);
        $this->set('evento',$evento);

        if($usuario['Usuario']['grupo'] == 'atendimento' || ($usuario['Usuario']['grupo'] == 'producao' && !empty($_POST['Consultar']))){
            $this->render('planejamento_mapa');
        }
    }

    function producao_mapa($id = false, $mapa_id = false){
        $this->_mapa($id, $mapa_id);
    }

    function planejamento_mapa($id = false){
        $this->_mapa($id);
    }

    function atendimento_mapa($id = false) {
        $this->_mapa($id);
    }
    
    function formando_mapa($id = false){
        $this->_mapa($id);

        $usuario = $this->obterUsuarioLogado();
        $usuarioReal = $this->obterUsuarioReal();
        $turma   = $this->obterTurmaLogada();

        // Componente MapaDeMesas
        $eventoId  = $id;
        $usuarioId = $usuario['Usuario']['id'];
        App::import('Component', 'MapaDeMesas');
        $mapaMesas = new MapaDeMesasComponent();

        $quantidadeMesas = $mapaMesas->retornaMesasCompradasPorUsuario(
            $usuarioId,$eventoId);

        $mesasCheckout['contrato']   = $quantidadeMesas['mesasContrato'];
        $mesasCampanhas['campanhas'] = $quantidadeMesas['mesasCampanhas'];
        $mesasCheckout['checkout']   = $quantidadeMesas['mesasCheckout'];

        $this->set('mesas', array_merge($mesasCheckout, $mesasCampanhas, array('contrato' => $mesasCheckout['contrato'])));
        $this->set('usuarioReal', $usuarioReal);
    }
    
    function planejamento_mapa_reiniciar($id = false) {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $turma = $this->obterTurmaLogada();
        $evento = $this->Evento->obterEventoMapa($id);
        if(isset($evento['EventoMapa']['id'])) {
            if($this->EventoMapa->delete($evento['EventoMapa']['id']))
                $this->Session->setFlash('Mapa removido com sucesso', 'metro/flash/success');
            else
                $this->Session->setFlash('Erro ao remover mapa', 'metro/flash/error');
        } else {
            $this->Session->setFlash('Mapa não encontrado', 'metro/flash/error');
        }
        echo json_encode(array());
    }
    
    function planejamento_mapa_reservar($id = false) {
        $turma = $this->obterTurmaLogada();
        $evento = $this->Evento->obterEventoMapa($id);
        if($evento) {
            if(isset($this->data['reservas'])) {
                $this->autoRender = false;
                Configure::write(array('debug' => 0));
                if($this->EventoMapaLocal->saveAll($this->data['reservas']))
                    $this->Session->setFlash('Dados alterados com sucesso', 'metro/flash/success');
                else
                    $this->Session->setFlash('Erro ao alterar dados', 'metro/flash/error');
                echo json_encode(array());
            } else {
                $this->EventoMapaColuna->recursive = 2;
                $colunas = $this->EventoMapaColuna->find('all',array(
                    'conditions' => array(
                        'evento_mapa_id' => $evento['EventoMapa']['id']
                    )
                ));
                $this->set('colunas',$colunas);
                $this->set('mapa',$evento);
                $this->set('turma',$turma);
            }
        }
    }
    
    function planejamento_mapa_selecionar_locais($id = false) {
        $this->layout = false;



        $evento = $this->Evento->obterEventoMapa($id);
        if(!empty($this->data) && $evento) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $dimensoes = $this->data['mapa'];
            $evento['EventoMapa']['largura'] = $dimensoes['largura'];
            $evento['EventoMapa']['altura'] = $dimensoes['altura'];
            if(!$this->EventoMapa->save($evento)) {
                $this->Session->setFlash('Erro ao alterar mapa', 'metro/flash/error');
            } else {
                $locais = $this->data['locais'];
                $letras = $this->data['colunas'];
                $colunas = array();
                $hoje = date('Y-m-d H:i:s');
                foreach($letras as $letra) {
                    $coluna = $this->EventoMapaColuna->find('first',array(
                        'conditions' => array(
                            'evento_mapa_id' => $evento['EventoMapa']['id'],
                            'nome' => $letra
                        )
                    ));
                    if(!$coluna) {
                        $coluna = array(
                            'EventoMapaColuna' => array(
                                'evento_mapa_id' => $evento['EventoMapa']['id'],
                                'nome' => $letra,
                                'data_cadastro' => $hoje
                            )
                        );
                        $this->EventoMapaColuna->create();
                        if($this->EventoMapaColuna->save($coluna))
                            $colunas[$letra] = $this->EventoMapaColuna->getLastInsertId();
                    } else {
                        $colunas[$letra] = $coluna['EventoMapaColuna']['id'];
                    }
                }
                if(count($colunas) != count($letras)) {
                    $this->Session->setFlash('Erro ao inserir colunas', 'metro/flash/error');
                } else {
                    $mensagens = array();
                    if(!empty($locais['remover'])) {
                        if(!$this->EventoMapaLocal->deleteAll(
                                array('EventoMapaLocal.id' => $locais['remover'])))
                            $mensagens[] = 'Erro ao remover locais';
                    }
                    if(!empty($locais['inserir'])) {
                        $inserir = array();
                        foreach($locais['inserir'] as $local) {
                            $local['evento_mapa_coluna_id'] = $colunas[$local['coluna']];
                            $local['data_cadastro'] = $hoje;
                            $inserir[] = array('EventoMapaLocal' => $local);
                        }
                        if(!$this->EventoMapaLocal->saveAll($inserir))
                            $mensagens[] = 'Erro ao inserir locais';
                    }
                    if(!empty($locais['alterar'])) {
                        if(!$this->EventoMapaLocal->saveAll($locais['alterar']))
                            $mensagens[] = 'Erro ao alterar locais';
                    }
                    if(empty($mensagens))
                        $this->Session->setFlash("Dados alterados com sucesso", 'metro/flash/success');
                    else
                        $this->Session->setFlash($mensagens, 'metro/flash/error');
                }
            }
            echo json_encode(array());
        } else {
            $letras = range('A','Z');
            $this->set('letras',$letras);
            $this->set('mapa',$evento);
            $this->EventoMapaColuna->recursive = 2;
            $colunas = $this->EventoMapaColuna->find('all',array(
                'conditions' => array(
                    'evento_mapa_id' => $evento['EventoMapa']['id']
                )
            ));
            $this->set('colunas',$colunas);
        }
    }
    
    function mapa_desalocar_formando($id = false){
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        if(!empty($id)){
            $this->EventoMapaLocal->id = $id;
            $this->EventoMapaLocal->saveField('usuario_id', '');
            $this->Session->setFlash('Formando desalocado com sucesso.', 'metro/flash/success');
        }else{
            $this->Session->setFlash('Ocorreu um erro ao desalocar o formando.', 'metro/flash/error');
        }
    }
    
    private function _inserir(){
        $this->layout = false;
        $turma = $this->Session->read('turma');
        if (!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $this->data['Evento']['turma_id'] = $turma['Turma']['id'];
            
            $dateTime = str_replace('/','-',$this->data['Evento']['data-hora']);
            $dateTime = $this->create_date_time_from_format('d-m-Y', $dateTime);
            $data = date_format($dateTime, 'Y-m-d');
            $this->data['Evento']['data'] = $data.' '.$this->data['Evento']['hora'];

            if(substr($this->data['Evento']['hora'], 0, 2) > substr($this->data['Evento']['hora_fim'], 0, 2)){
                $data_fim = date('Y-m-d',strtotime('+1 day', strtotime($data)));
                $this->data['Evento']['data_fim'] = $data_fim.' '.$this->data['Evento']['hora_fim'];
            }else{
                $this->data['Evento']['data_fim'] = $data.' '.$this->data['Evento']['hora_fim'];
            }
            unset($this->data['Evento']['data-hora']);

            if (isset($this->data['Evento']['horario_valsa_aux'])) {
                $this->data['Evento']['horario_valsa'] = $this->data['Evento']['horario_valsa_aux'];
                unset($this->data['Evento']['horario_valsa_aux']);
            }
            
            if (isset($this->data['TiposEvento']['nome'])) {
                $tipoEvento = array(
                    'nome' => $this->data['TiposEvento']['nome'],
                    'ativo' => 1,
                    'template' => 'evento'
                );
                if($this->TiposEvento->save($tipoEvento))
                    $this->data['Evento']['tipos_evento_id'] = $this->TiposEvento->getLastInsertId();
            }
            
            if($this->data['Evento']['tipos_evento_id'] == '1'){
                unset($this->data['Evento']['beca']);
                unset($this->data['Evento']['cor_da_faixa']);
                unset($this->data['Evento']['capelo']);
            }
            
            if($this->data['Evento']['tipos_evento_id'] == '2'){
                unset($this->data['Evento']['horario_valsa']);
            }
            
            if($this->data['Evento']['tipos_evento_id'] > '2'){
                unset($this->data['Evento']['horario_valsa']);
                unset($this->data['Evento']['beca']);
                unset($this->data['Evento']['cor_da_faixa']);
                unset($this->data['Evento']['capelo']);
            }
            if ($this->Evento->save($this->data)) {
                $this->Session->setFlash('Evento criado com sucesso.', 'metro/flash/success');
            }else{
                $this->Session->setFlash('Ocorreu um erro ao criar o Evento.', 'metro/flash/error');
            }
            echo json_encode(array());
        }
        
        $this->set('tiposeventos', $this->TiposEvento->find('list', array('fields' => 'TiposEvento.nome')));
    }
    
    function planejamento_inserir(){
        $this->_inserir();
    }
    
    private function _eventos(){
        $this->layout = false;
        $usuario = $this->Session->read('Usuario');
        if (!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
                $this->Session->write("filtros.{$usuario['Usuario']['grupo']}.eventos", $this->data['Evento']);
        } else {
            $options['order'] = array('Evento.data' => 'asc');
            $filtro = $this->Session->read("filtros.{$usuario['Usuario']['grupo']}.eventos");
            if($filtro) {
                $this->data['Evento'] = $filtro;
                foreach ($filtro as $chave => $valor)
                    $options['conditions']['MONTH(Evento.data) LIKE '] = strtolower($valor);
            }
            $options['limit'] = 20;
            $this->paginate['Evento'] = $options;
            $eventos = $this->paginate('Evento');
            $this->set('eventos', $eventos);
        }
        $this->set('meses',$meses = array(null,'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio',
            'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'));
    }


    function comercial_eventos(){
        $this->_eventos();
    }
    
    function criacao_eventos(){
        $this->_eventos();
    }
    
    function foto_eventos(){
        $this->_eventos();
    }
    
    function planejamento_eventos(){
        $this->_eventos();
    }
    
    function producao_eventos(){
        $this->_eventos();
    }
    
    function rh_eventos(){
        $this->_eventos();
    }
    
    function video_eventos(){
        $this->_eventos();
    }

    function planejamento_index() {
        $turma = $this->Session->read('turma');

        $cond = array(
            'Evento.turma_id' => $turma['Turma']['id']
        );
        $this->set('eventos', $this->paginate('Evento', $cond));
    }

    function comissao_index() {
        $this->planejamento_index();
    }

    function atendimento_index() {
        $this->planejamento_index();
    }
    
    private function _listar($turmaId = false) {
        if (!$turmaId)
            $turma = $this->Session->read('turma');
        else
            $turma = $this->Turma->read(null, $turmaId);
        $cond = array(
            'Evento.turma_id' => $turma['Turma']['id']
        );
        $this->layout = false;
        $this->set('turma', $turma);
        $this->set('eventos', $this->Evento->find('all', array('conditions' => $cond, 'group' => 'Evento.id')));
        $this->set('tiposeventos', $this->TiposEvento->find('list', array('fields' => 'TiposEvento.nome')));
    }
    
    private function _alterar($id = false){
        $this->layout = false;
        $this->Evento->id = $id;
        $evento = $this->Evento->findById($id);
        $turma = $this->Session->read('turma');
        if (!empty($this->data)){
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
			
            $dateTime = str_replace('/','-',$this->data['Evento']['data-hora']);
            $time = $this->create_date_time_from_format('d-m-Y', $dateTime);
            $data = date_format($time, 'Y-m-d');
            $this->data['Evento']['data'] = $data.' '.$this->data['Evento']['hora'];
            if(substr($this->data['Evento']['hora'], 0, 2) > substr($this->data['Evento']['hora_fim'], 0, 2)){
                $data_fim = date('Y-m-d',strtotime('+1 day', strtotime($data)));
                $this->data['Evento']['data_fim'] = $data_fim.' '.$this->data['Evento']['hora_fim'];
            }else{
                $this->data['Evento']['data_fim'] = $data.' '.$this->data['Evento']['hora_fim'];
            }
            unset($this->data['Evento']['data-hora']);

            $this->data['Evento']['turma_id'] = $turma['Turma']['id'];
            
            if (isset($this->data['Evento']['horario_valsa_aux'])) {
                $this->data['Evento']['horario_valsa'] = $this->data['Evento']['horario_valsa_aux'];
                unset($this->data['Evento']['horario_valsa_aux']);
            }
            
            if (isset($this->data['TiposEvento']['nome'])) {
                $tipoEvento = array(
                    'nome' => $this->data['TiposEvento']['nome'],
                    'ativo' => 1,
                    'template' => 'evento'
                );
                
                if($this->TiposEvento->save($tipoEvento))
                    $this->data['Evento']['tipos_evento_id'] = $this->TiposEvento->getLastInsertId();
            }
            if(isset($this->data['Evento']['tipos_evento_id'])){
                if($this->data['Evento']['tipos_evento_id'] == 1)
                    $this->data['Evento']['alerta_telao'] = 1;
                else
                    $this->data['Evento']['alerta_telao'] = 0;

                
                if($this->data['Evento']['tipos_evento_id'] == '1'){
                    unset($this->data['Evento']['beca']);
                    unset($this->data['Evento']['cor_da_faixa']);
                    unset($this->data['Evento']['capelo']);
                }
                
                if($this->data['Evento']['tipos_evento_id'] == '2'){
                    unset($this->data['Evento']['horario_valsa']);
                }
                
                if($this->data['Evento']['tipos_evento_id'] > '2'){
                    unset($this->data['Evento']['horario_valsa']);
                    unset($this->data['Evento']['beca']);
                    unset($this->data['Evento']['cor_da_faixa']);
                    unset($this->data['Evento']['capelo']);
                }
            }
			
			unset($this->data['Evento']['hora']);
			unset($this->data['Evento']['hora_fim']);

			if(isset($this->data['Evento']['horario_valsa']) && $this->data['Evento']['horario_valsa'] == '00:00')
				unset($this->data['Evento']['horario_valsa']);

			$this->data['Evento']['hora_fim'] = $this->data['Evento']['data'];
			
			$evento['Evento']['data'] = date('Y-m-d H:i', strtotime($evento['Evento']['data']));

			$diff = array_diff($this->data['Evento'], $evento['Evento']);
            
            if ($this->Evento->save($this->data)) {
				
				if(!empty($diff)){
					
					$data = [];
					$antigo = [];
					
					foreach($diff as $i => $d){
						
						$data[] = [
							$i => $d
						];
						
						$antigo[] = [
							$i => $evento['Evento'][$i],
						];
					}
					
					$a = $this->enviarEmailAlteracaoEvento($data, $this->obterUsuarioLogado(), $evento, $antigo);
				}

                $this->Session->setFlash('Evento foi salvo corretamente. ' . $a, 'metro/flash/success');
            } else {
                $this->Session->setFlash('Um erro ocorreu na atualização do evento.', 'metro/flash/error');
            }

            $checked = $$this->data['Evento']['alerta_telao'];
        }else{
            $evento['Evento']['data-hora'] = date("d/m/Y", strtotime($evento['Evento']['data']));
            $evento['Evento']['hora'] = substr($evento['Evento']['data'], 11);
            $evento['Evento']['hora_fim'] = substr($evento['Evento']['data_fim'], 11);
            $checked = $evento['Evento']['alerta_telao'];
            $this->data = $evento;
        }
        
        $this->set('turma', $turma);
        $this->set('evento', $evento);
        $this->set('checked', $checked);
        $this->set('tiposeventos', $this->TiposEvento->find('list', array('fields' => 'TiposEvento.nome')));
        $this->set('tipoescolhido', $this->TiposEvento->find(array('TiposEvento.id' => $evento['Evento']['tipos_evento_id'])));
    }
	
	private function enviarEmailAlteracaoEvento($data, $usuario, $evento, $antigo) {
        App::import('Component', 'mail');
        $this->Mailer = new Mail();

		$this->Mailer->IsHTML(true);
        $this->Mailer->FromName = utf8_decode('RK Formaturas - Sistema Online');
        $this->Mailer->AddAddress('rubia.moreira@eventos.as', 'Rubia Burque Moreira');
        $this->Mailer->Subject = utf8_decode('Alteração de Evento');
		$mensagem = "O usuário {$usuario['Usuario']['nome']} alterou os seguintes campos do evento <b>{$evento['Evento']['turma_id']} - {$evento['Evento']['nome']}</b><br /><br />";
		
		foreach($data as $i => $d){
			
			$mensagem.= "Campo <b>".key($d)."</b><br />";
			$mensagem.= "De: ". $antigo[$i][key($d)]."<br />";
			$mensagem.= "Para: ".array_values($d)[0]."<br /><br />";
		}
		
        $this->Mailer->Body = utf8_decode($mensagem);
		$a = 'asdsd';
        if($this->Mailer->Send())
			$a = 'Email enviado para Rubia informando as alterações.';
		
		return $a;
	}
    
    function planejamento_alterar($id = false){
        $this->_alterar($id);
    }

    function formando_listar() {
        $turma = $this->obterTurmaLogada();
        $this->_listar($turma['Turma']['id']);
    }

    function planejamento_listar() {
        $turma = $this->obterTurmaLogada();
        $this->_listar($turma['Turma']['id']);
    }
    
    function comissao_listar() {
        $turma = $this->obterTurmaLogada();
        $this->_listar($turma['Turma']['id']);
    }
    
    function foto_listar() {
        $turma = $this->obterTurmaLogada();
        $this->_listar($turma['Turma']['id']);
    }
    
    function foto_inserir_fotografos($eventoId = false){
        $this->layout = false;
        if(!empty($this->data)){
            $this->autoRender = false;
            $this->Evento->id = $eventoId;
            if($this->Evento->saveField('informacoes_fotografos', nl2br($this->data['Evento']['informacoes_fotografos'])))
                $this->Session->setFlash('Informações de Fotógrafos inseridas com sucesso.', 'metro/flash/success');
            else
                $this->Session->setFlash('Erro ao inserir Informações de Fotógrafos.', 'metro/flash/error');
        }else{
            $this->data = $this->Evento->read(null, $eventoId);
        }
    }

    function planejamento_escolher() {
        if (!empty($this->data)) {
            if (isset($this->data['EscolherTipo']['tipos_evento_id']))
                $this->redirect("/{$this->params['prefix']}/eventos/adicionar/{$this->data['EscolherTipo']['tipos_evento_id']}");
        }
        $this->set('tiposeventos', $this->TiposEvento->find('list', array('fields' => 'TiposEvento.nome')));
    }

    function planejamento_adicionar($tipo_evento = null) {
        $turma = $this->Session->read('turma');
        $this->TiposEvento->id = $tipo_evento;
        $tipoEvento = $this->TiposEvento->read();

        if (!empty($this->data)) {
            $this->data['Evento']['turma_id'] = $turma['Turma']['id'];
            $this->data['Evento']['tipos_evento_id'] = $tipo_evento;

            $dateTime = $this->create_date_time_from_format('d-m-Y H:i', $this->data['Evento']['data-hora']);
            $this->data['Evento']['data'] = date_format($dateTime, 'Y-m-d H:i:s');
            unset($this->data['Evento']['data-hora']);

            if (isset($this->data['Evento']['horario_valsa_aux'])) {
                $this->data['Evento']['horario_valsa'] = $this->data['Evento']['horario_valsa_aux'];
                unset($this->data['Evento']['horario_valsa']);
            }


            if ($this->Evento->save($this->data['Evento'])) {
                $this->Session->setFlash('Evento criado com sucesso.', 'flash_sucesso');
                $this->redirect("/{$this->params['prefix']}/eventos/visualizar/{$this->Evento->getLastInsertId()}");
            }
            else
                $this->Session->setFlash('Ocorreu um erro ao criar o Evento.', 'flash_erro');
        }

        $this->data['Evento']['data-hora'] = date('d-m-Y H:i');
        $this->data['Evento']['tipo_evento_id'] = $tipoEvento['TiposEvento']['id'];
        $this->set('tipoEvento', $tipoEvento);
    }

    function planejamento_visualizar($id) {
        $this->Evento->id = $id;
        $evento = $this->Evento->read();

        $this->TiposEvento->id = $evento['Evento']['tipos_evento_id'];
        $tipoEvento = $this->TiposEvento->read();

        $this->set('tipoEvento', $tipoEvento);
        $this->set('evento', $evento);
    }

    function comissao_visualizar($id) {
        $this->planejamento_visualizar($id);
    }

    function atendimento_visualizar($id) {
        $this->planejamento_visualizar($id);
    }

    function planejamento_editar($id) {
        $this->Evento->id = $id;

        if (!empty($this->data)) {
            $dateTime = $this->create_date_time_from_format('d-m-Y H:i', $this->data['Evento']['data-hora']);
            $this->data['Evento']['data'] = date_format($dateTime, 'Y-m-d H:i:s');
            unset($this->data['Evento']['data-hora']);


            if (isset($this->data['Evento']['horario_valsa_aux'])) {
                $this->data['Evento']['horario_valsa'] = $this->data['Evento']['horario_valsa_aux'] . ':00';
                unset($this->data['Evento']['horario_valsa_aux']);
            }

            if($this->data['Evento']['tipos_evento_id'] == 1)
                $this->data['Evento']['alerta_telao'] = 1;
            else
                $this->data['Evento']['alerta_telao'] = 0;

            if ($this->Evento->save($this->data)) {
                $this->Session->setFlash('Evento foi salvo corretamente.', 'flash_sucesso');
                $this->redirect("/{$this->params['prefix']}/eventos/visualizar/$id");
            } else {
                $this->Session->setFlash('Um erro ocorreu na atualização do evento.', 'flash_erro');
            }
        } else {
            $this->data = $this->Evento->read();
            if (empty($this->data)) {
                $this->redirect("/{$this->params['prefix']}/eventos/index");
                $this->Session->setFlash('Evento não existente.', 'flash_erro');
            }

            $this->data['Evento']['horario_valsa_aux'] = substr($this->data['Evento']['horario_valsa'], 0, 5);

            $dateTime = $this->create_date_time_from_format('Y-m-d H:i:s', $this->data['Evento']['data']);
            $this->data['Evento']['data-hora'] = date_format($dateTime, 'd-m-Y H:i');
        }


        $this->TiposEvento->id = $this->data['Evento']['tipos_evento_id'];
        $tipoEvento = $this->TiposEvento->read();

        $this->set('tipoEvento', $tipoEvento);
    }

    function planejamento_excluir($id) {
        if ($this->Evento->delete($id)) {
            $this->Session->setFlash('Evento excluído com sucesso.', 'flash_sucesso');
            $this->redirect("/{$this->params['prefix']}/eventos/index");
        } else {
            $this->Session->setFlash('Erro ao excluir evento.', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/eventos/visualizar/$id");
        }
    }

    function formando_index() {
        $this->planejamento_index();
    }

    function formando_visualizar($id) {
        $this->planejamento_visualizar($id);
    }

}

?>
