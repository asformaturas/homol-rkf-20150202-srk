<?php

class PendenciasController extends AppController {

    var $name = "Pendencias";
    var $uses = array("Usuario", "Turma", "FormandoProfile", "Pagamento", "ViewFormandos", "CursoTurma", "Protocolo", "ProtocoloCancelamento");

    function atendimento_index() {
        ini_set('memory_limit', '256M');
        $usuario = $this->Session->read('Usuario');
        $qtdePendencias = 0;
        $turmas = $this->TurmasUsuario->find('all', array('conditions' => array('TurmasUsuario.usuario_id' => $usuario['Usuario']['id'])));
        $this->FormandoProfile->recursive = 1;
        $pendencias = array(
            'revisar_cancelamentos' => array('titulo' => 'Revisar Cancelamentos', 'itens' => array()),
            'vincular_pagamentos' => array('titulo' => 'Vincular Pagamentos', 'itens' => array())
        );
        $formandos = array();
        foreach ($turmas as $turma) {
            $listaFormandos = $this->FormandoProfile->find('all', array('conditions' => array('CursoTurma.turma_id' => $turma['TurmasUsuario']['turma_id'])));
            foreach ($listaFormandos as $formando)
                array_push($formandos, array_merge($formando['FormandoProfile'], $formando['Usuario']));
        }
        foreach ($formandos as $formando) {
            if ($formando['situacao'] == 'cancelado') {
                $conditions = array('Protocolo.usuario_id' => $formando['usuario_id'], 'Protocolo.tipo' => 'cancelamento', "Protocolo.status <> 'finalizado'");
                if ($protocolo = $this->ProtocoloCancelamento->find('first', array('conditions' => $conditions))) {
                    $qtdePendencias++;
                    $item = "<li><a href='/{$this->params['prefix']}/solicitacoes/visualizar/{$protocolo['Protocolo']['protocolo']}' target='_blank'>Protocolo {$protocolo['Protocolo']['protocolo']} - {$formando['nome']}</a></li>";
                    $pendencias['revisar_cancelamentos']['itens'][] = $item;
                }
            }
            $pagamentos = $this->Pagamento->find('all', array('conditions' => array('Pagamento.status' => 'pago', 'Pagamento.usuario_id' => $formando['usuario_id'])));
            foreach ($pagamentos as $pagamento) {
                if (sizeof($pagamento['DespesaPagamento']) == 0) {
                    $qtdePendencias++;
                    $item = "<li><a href='/{$this->params['prefix']}/vincular_pagamentos/listar_despesas_formando/{$formando['usuario_id']}' target='_blank'>Vincular pagamento do(a) formando(a) - {$formando['nome']}</a></li>";
                    $pendencias['vincular_pagamentos']['itens'][] = $item;
                    break;
                }
            }
        }
        $this->set('qtdePendencias', $qtdePendencias);
        $this->set('pendencias', $pendencias);
    }

    function formando_index() {
        $this->pageTitle = 'Mensagens/Pend&rcirc;ncias';
        $this->loadModel('Item');
        $this->Assunto->recursive = 0;
        $this->Item->recursive = 0;
        $this->Cronograma->recursive = 0;
        $turma = $this->obterTurmaLogada();
        $usuario = $this->Session->read('Usuario');
        $grupo = $usuario['Usuario']['grupo'];
        $grupoDoItem = $grupo;
        $qtdePendencias = 0;
        $pendencias = array(
            'itens' => array('titulo' => 'Revisar Cancelamentos', 'itens' => array())
        );
        if ($grupo == 'comissao')
            ($turma['Turma']['status'] == 'fechada') ? $grupoDoItem = 'planejamento' : $grupoDoItem = 'comercial';
        if ($turma) {
            $itens = $this->Item->find('all', array(
                'conditions' => array(
                    'grupo' => $grupoDoItem
                ),
                'fields' => array('id', 'nome')
            ));
            $itens_com_pendencias = array();
            $itens_geral = array();
            foreach ($itens as $item):
                $assuntos = $this->Assunto->find('all', array(
                    'conditions' => array(
                        'item_id' => $item['Item']['id'],
                        'turma_id' => $turma['Turma']['id']
                    ),
                    'fields' => array('pendencia')
                ));
                foreach ($assuntos as $assunto):
                    if ($grupo == 'comissao') {
                        if (strcmp($assunto['Assunto']['pendencia'], 'Comissao') == 0)
                            $qtdePendencias++;
                    } else {
                        
                    }
                endforeach;
                $item_aux['nome'] = $item['Item']['nome'];
                $item_aux['quantidadeDeAssuntosPentendentes'] = $pendencias;
                $item_aux['id'] = $item['Item']['id'];
                $itens_geral[$pendencias][] = $item_aux;
            endforeach;
            sort($itens_geral);
            array_reverse($itens_geral, true);
            $itens_adicionados = 0;
            foreach ($itens_geral as $lista_itens) :
                foreach ($lista_itens as $item) :
                    $this->incluirItemNoSidebar($item['nome'], $item['quantidadeDeAssuntosPentendentes'], $item['id']);
                    $itens_adicionados++;
                endforeach;
            endforeach;
        }
    }

    function planejamento_index() {
        $this->layout = false;
        if(!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $this->Session->write("filtros.{$this->params['prefix']}.pendencias",
                    $this->data);
        } else {
            $usuario = $this->Session->read('Usuario');
            $options = array(
                'group' => array('Turma.id'),
                'conditions' => array(
                    'Turma.status' => 'fechada'
                ),
                'joins' => array(
                    array(
                        'table' => 'turmas_usuarios',
                        'alias' => 'TurmaUsuario',
                        'foreignKey' => false,
                        'conditions' => array(
                            'Turma.id = TurmaUsuario.turma_id',
                            "TurmaUsuario.usuario_id = {$usuario['Usuario']['id']}"
                        )
                    )
                ),
                'fields' => array('Turma.nome')
            );
            $this->Turma->unbindModelAll();
            $turmas = $this->Turma->find('list', $options);
            $this->set('turmas', $turmas);
            $turmas = array_keys($turmas);
            $options = array(
                'group' => array(
                    "Turma.id having(date_format(max(Parcelamento.data),'%Y%m') < '" . date('Ym', strtotime('now')) .
                        "' or Parcelamento.id is null)"
                ),
                'conditions' => array(
                    'Turma.id' => $turmas
                ),
                'fields' => array(
                    'Turma.id',
                    'Parcelamento.id'
                ),
                'joins' => array(
                    array(
                        'table' => 'parcelamentos',
                        'alias' => 'Parcelamento',
                        'type' => 'left',
                        'foreignKey' => false,
                        'conditions' => array(
                            'Turma.id = Parcelamento.turma_id'
                        )
                    )
                )
            );
            $parcelaMaker = $this->Turma->find('list',$options);
            if(count($parcelaMaker) > 0) {
                $mensagemDeErroParcelamaker = "As seguintes turmas n&atilde;o possuem parcelamaker: " .
                        implode(", ", array_keys($parcelaMaker)) . ".";
                $this->Session->setFlash($mensagemDeErroParcelamaker, 'metro/flash/alert');
            }
            $options = array(
                'conditions' => array(
                    'Assunto.turma_id' => $turmas,
                    'Assunto.pendencia' => 'As',
                    'Assunto.resolvido' => 0
                ),
                'order' => array('Assunto.turma_id,Assunto.item_id'),
                'limit' => 100
            );
            if($this->Session->check("filtros.{$this->params['prefix']}.pendencias")) {
                $config = $this->Session->read("filtros.{$this->params['prefix']}.pendencias");
                foreach($config as $modelo => $filtro)
                    foreach($filtro as $chave => $valor)
                        if($valor != '')
                            if($chave == 'id')
                                $options['conditions']["$modelo.$chave"] = $valor;
                            else
                                $options['conditions'][] = "lower($modelo.$chave) like('%".strtolower($valor)."%')";
                $this->data = $config;
            }
            $this->loadModel('Assunto');
            $this->paginate['Assunto'] = $options;
            $assuntos = $this->paginate('Assunto');
            $this->set('assuntos',$assuntos);
        }
    }
    
    function planejamento_parcelamentos() {
        $this->layout = false;
        if(!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $this->Session->write("filtros.{$this->params['prefix']}.pendencias_parcelamentos",
                    $this->data);
        } else {
            $usuario = $this->Session->read('Usuario');
            $options = array(
                'group' => array('Turma.id'),
                'conditions' => array(
                    'Turma.status' => 'fechada'
                ),
                'joins' => array(
                    array(
                        'table' => 'turmas_usuarios',
                        'alias' => 'TurmaUsuario',
                        'foreignKey' => false,
                        'conditions' => array(
                            'Turma.id = TurmaUsuario.turma_id',
                            "TurmaUsuario.usuario_id = {$usuario['Usuario']['id']}"
                        )
                    )
                ),
                'fields' => array('Turma.nome')
            );
            $this->Turma->unbindModelAll();
            $turmas = $this->Turma->find('list', $options);
            $this->set('turmas', $turmas);
            $turmas = array_keys($turmas);
            $options = array(
                'group' => array(
                    "Turma.id having(date_format(max(Parcelamento.data),'%Y%m') < '" . date('Ym', strtotime('now')) .
                        "' or Parcelamento.id is null)"
                ),
                'conditions' => array(
                    'Turma.id' => $turmas
                ),
                'fields' => array(
                    'Turma.id',
                    'Parcelamento.id'
                ),
                'joins' => array(
                    array(
                        'table' => 'parcelamentos',
                        'alias' => 'Parcelamento',
                        'type' => 'left',
                        'foreignKey' => false,
                        'conditions' => array(
                            'Turma.id = Parcelamento.turma_id'
                        )
                    )
                )
            );
            $parcelaMaker = $this->Turma->find('list',$options);
            if(count($parcelaMaker) > 0) {
                $mensagemDeErroParcelamaker = "As seguintes turmas n&atilde;o possuem parcelamaker: " .
                        implode(", ", array_keys($parcelaMaker)) . ".";
                $this->Session->setFlash($mensagemDeErroParcelamaker, 'metro/flash/alert');
            }
            $options = array(
                'conditions' => array(
                    'Turma.id' => $turmas,
                    'Turma.status' => 'fechada'
                ),
                'order' => array('Turma.id'),
                'limit' => 100,
                'group' => array('Turma.id'),
                'fields' => array(
                    'Turma.*',
                    '(select max(data_aplicacao) from igpm_turmas where turma_id = Turma.id and Turma.data_igpm is not null) as data_ultima_aplicacao',
                    '(select date_add(coalesce((select max(data_aplicacao) from igpm_turmas where turma_id = Turma.id),Turma.data_assinatura_contrato), interval 1 year)) as data_proxima_aplicacao',
                    '(select max(data) from parcelamentos where Turma.id = turma_id) as data_ultimo_parcelamento',
                    '(select count(0) from igpm_turmas where turma_id = Turma.id and Turma.data_igpm is not null) as aplicacoes',
                ),
            );
            $this->paginate['Turma'] = $options;
            $turmas = $this->paginate('Turma');
            $this->set('turmas',$turmas);
        }
    }
    
    function comissao_index() {
        $this->layout = false;
        $turma = $this->obterTurmaLogada();
        $options = array(
            'conditions' => array(
                'Assunto.turma_id' => $turma['Turma']['id'],
                'Assunto.pendencia' => 'Comissao',
                'Assunto.resolvido' => 0
            ),
            'order' => array('Assunto.turma_id,Assunto.item_id'),
            'limit' => 100
        );
        $this->loadModel('Assunto');
        $this->paginate['Assunto'] = $options;
        $assuntos = $this->paginate('Assunto');
        $this->set('assuntos',$assuntos);
    }

}
