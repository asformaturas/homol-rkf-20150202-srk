<?php
class AssuntosController extends AppController {

	var $name = 'Assuntos';
	
	// Padrão de paginação
	var $paginate = array(
		'limit' => 25,
		'order' => array(
   			'Assunto.id' => 'desc'
			)
		);
	
	var $uses = array('Item', 'Assunto', 'Mensagem', 'Usuario', 'Arquivo');
	
	var $nomeDoTemplateSidebar = 'mensagens';
	

	/* ================== PARTE COMERCIAL ================== */
	function comercial_reabrir($id = null) {
		$assunto = $this->Assunto->findById($id);
		
		if($assunto) {
			$this->Assunto->id = $id;
			if($this->Assunto->saveField('resolvido', false)) {
				$this->Session->setFlash("Assunto reaberto com sucesso!", 'flash_sucesso');
				$this->redirect("/{$this->params['prefix']}/assuntos/visualizar/{$id}");
			}	
		}
		 
		$this->Session->setFlash("Não foi possível reabrir o assunto.", 'flash_erro');
		$this->redirect("/{$this->params['prefix']}/assuntos/visualizar/{$id}");	
		
	}
	
	function planejamento_reabrir($id = null) {
		$this->comercial_reabrir($id);
	}
	
	function comercial_adicionar($id = null) {
		$turma = $this->Session->read('turma');
		
		if($turma == null) {
			$this->Session->setFlash("Antes de criar um assunto, uma turma deve ser selecionada", 'flash_erro');
			$this->redirect("/{$this->params['prefix']}/turmas/");
		} else {
			$this->set('item', $this->Item->findById($id));
			$this->set('pendencias', $this->Mensagem->pendencias_comercial);
			if(!empty($this->data)) {
			
				//TODO: modificar para session
				if($turma != null) {
					$this->data['Assunto']['turma_id'] = $turma['Turma']['id'];
				}
				else {
					$this->data['Assunto']['turma_id'] = $this->data['Assunto']['turma'];
				}
			
				$this->Assunto->create();
				if ($this->data['Mensagem']['texto'] != "") {
					if ($this->Assunto->save($this->data)) {
				
						$this->Mensagem->create();
				
						$this->data['Mensagem']['assunto_id'] = $this->Assunto->id;
						$this->data['Mensagem']['usuario_id'] = $this->Session->read('Usuario.Usuario.id');
						$this->data['Mensagem']['data'] = date('Y-m-d H:i:s');
						$this->data['Mensagem']['texto'] = htmlspecialchars($this->data['Mensagem']['texto']);
						$this->data['Mensagem']['turma_id'] = $this->data['Assunto']['turma_id'];
						if($this->Mensagem->save($this->data)){
							if($this->adicionar_arquivos($turma['Turma']['id'])){
								$this->Session->setFlash(__('O assunto foi salvo com sucesso', true), 'flash_sucesso');
								$this->redirect("/{$this->params['prefix']}/itens/visualizar/" . $this->data['Assunto']['item_id']);
							} else {
								$this->Session->setFlash(__('Ocorreu um erro ao salvar a mensagem do assunto.', true), 'flash_erro');
							}
						};
					} else {
						$this->Session->setFlash(__('Ocorreu um erro ao salvar o assunto.', true), 'flash_erro');
					}
				} else {
					$this->Session->setFlash(__('Ocorreu um erro ao salvar o assunto. Mensagem obrigatoria.', true), 'flash_erro');
				}
			}
		}
	}
	
	function comercial_visualizar($id = null, $options = null) {
		if (empty($options['conditions'])) $options['conditions'] = array();
		
		$mensagem_erro = false;
	
		if (!empty($id)) {
			$this->Assunto->recursive = 1;
			$assunto = $this->Assunto->find('first', array('conditions' => array('Assunto.id' => $id)));

			
			if (!empty($assunto)) {
		        $usuario = $this->Session->read('Usuario');
				$this->Usuario->Turma->recursive = 0;
				$turmas_do_usuario = $this->Usuario->Turma->find('all', array('Turma.usuario_id' => $usuario['Usuario']['id']));
				$acessivel = false;
				
				foreach ($turmas_do_usuario as $t) {
					if ($t['Turma']['id'] == $assunto['Turma']['id']) {
						$acessivel = true;
					} 
				}
				
				
				if ($acessivel) {

					$this->set('assunto', $assunto);
					$this->paginate = array(
						'limit' => 25,
						'order' => array(
							'Mensagem.id' => 'desc'
						)
					);

					$this->Mensagem->recursive = 2;
					$this->set('mensagens', $this->paginate('Mensagem', 
						array('AND' => array('Mensagem.assunto_id' => $id, 
							  $options['conditions']))
						));
					
				} else {
					$mensagem_erro = true;
				}
				
				
			} else {
				$mensagem_erro = true;
			}
		} else {
			$mensagem_erro = true;
		}
		
		if ($mensagem_erro) {
			$this->Session->setFlash("O assunto selecionado não existe", 'flash_erro');
		}
	}
	
	
	function comercial_procurar() {
		if (!empty($this->data['Mensagem']['chave']) && !empty($this->data['Mensagem']['assunto_id'])) {
			$id = $this->data['Mensagem']['assunto_id'];
			$chave = $this->data['Mensagem']['chave'];
			$chaves = preg_split("/[, -.]/", $chave);
			$chaves = array_unique($chaves);
			
			$empty_key = array_search('', $chaves);
			if (!empty($empty_key)) unset($chaves[$empty_key]);

			function construirQuery($c) {
	            return array('Mensagem.id LIKE' => '%' . $c . '%');
	        }
			$chaves = array_map('construirQuery', $chaves);
			
			$conditions = (empty($chaves)) ? array() : array('OR' => $chaves);
			
			
			$this->comercial_visualizar($id, array('conditions' => $conditions));
		} else {
			$this->comercial_visualizar(
				empty($this->data['Mensagem']['assunto_id']) ? 
				"" : $this->data['Mensagem']['assunto_id']
			);
		}
		$this->render('comercial_visualizar');
	}
	
	
	
	
	
	/* ================== PARTE COMISSAO ================== */
	
	function comissao_adicionar($id = null) {
		if(!empty($this->data)) {
			
			$formando = $this->Usuario->find('first', array('conditions' => array('Usuario.id' => $this->Auth->user('id'))));
			$this->data['Assunto']['turma_id'] = $formando['Turma'][0]['id'];
			$this->Assunto->create();
			if ($this->data['Mensagem']['texto'] != "") {
				if ($this->Assunto->save($this->data)) {
				
					$this->Mensagem->create();
				
					$this->data['Mensagem']['assunto_id'] = $this->Assunto->id;
					$this->data['Mensagem']['formando_id'] = $this->Session->read('Usuario.Usuario.id');
					$this->data['Mensagem']['data'] = date('Y-m-d H:i:s');
					$this->data['Mensagem']['texto'] = htmlspecialchars($this->data['Mensagem']['texto']);
					$this->data['Mensagem']['turma_id'] = $this->data['Assunto']['turma_id'];
				
					$this->Mensagem->save($this->data);
				
					$this->Session->setFlash(__('O assunto foi salvo com sucesso', true), 'flash_sucesso');
					$this->redirect("/{$this->params['prefix']}/itens/visualizar/" . $this->data['Assunto']['item_id']);
				} else {
					$this->Session->setFlash(__('Ocorreu um erro ao salvar o assunto.', true), 'flash_erro');
				}
			} else {
				$this->Session->setFlash(__('Ocorreu um erro ao salvar o assunto. Mensagem obrigatoria.', true), 'flash_erro');
			}
		}
		
		$this->set('item', $this->Item->find('first', array('conditions' => array('Item.id' => $id))));
		$this->set('pendencias', $this->Assunto->pendencias);
	}
	
	function comissao_visualizar($id = null) {
		$usuario = $this->Session->read('Usuario');
		
		//print_r($usuario);
		
		$formando = $this->Session->read('Usuario');
		$turma = $this->Session->read('turma');
		$this->Mensagem->recursive = 2;
		$this->paginate = array(
			'limit' => 20,
			'conditions' => array(
				'Mensagem.turma_id' => $turma['Turma']['id'],
				'Mensagem.assunto_id' => $id
			),
			'order' => array(
				'Mensagem.id' => 'desc'
			)
		);
		$this->set('assunto', $this->Assunto->find('first', 
						array('conditions' => array('AND' => array('Assunto.id' => $id, 
									'Assunto.turma_id' => $turma['Turma']['id'])))));

		$this->set('mensagens', $this->paginate('Mensagem'));
	}
	
	
	
	
	
	
	/* ================== PARTE PLANEJAMENTO ================== */
	
	
	function planejamento_adicionar($id = null) {
		$turma = $this->Session->read('turma');
		
		if($turma == null) {
			$this->Session->setFlash("Antes de criar um assunto, uma turma deve ser selecionada", 'flash_erro');
			$this->redirect("/{$this->params['prefix']}/turmas/");
		} else {
			$this->set('item', $this->Item->findById($id));
			$this->set('pendencias', $this->Mensagem->pendencias);
			if(!empty($this->data)) {
			
				//TODO: modificar para session
				if($turma != null) {
					$this->data['Assunto']['turma_id'] = $turma['Turma']['id'];
				}
				else {
					$this->data['Assunto']['turma_id'] = $this->data['Assunto']['turma'];
				}
			
				$this->Assunto->create();
				if ($this->data['Mensagem']['texto'] != "") {
					if ($this->Assunto->save($this->data)) {
				
						$this->Mensagem->create();
				
						$this->data['Mensagem']['assunto_id'] = $this->Assunto->id;
						$this->data['Mensagem']['usuario_id'] = $this->Session->read('Usuario.Usuario.id');
						$this->data['Mensagem']['data'] = date('Y-m-d H:i:s');
						$this->data['Mensagem']['texto'] = htmlspecialchars($this->data['Mensagem']['texto']);
						$this->data['Mensagem']['turma_id'] = $this->data['Assunto']['turma_id'];
						if($this->Mensagem->save($this->data)){
							if($this->adicionar_arquivos($turma['Turma']['id'])){
								$this->Session->setFlash(__('O assunto foi salvo com sucesso', true), 'flash_sucesso');
								$this->redirect("/{$this->params['prefix']}/itens/visualizar/" . $this->data['Assunto']['item_id']);
							} else {
								$this->Session->setFlash(__('Ocorreu um erro ao salvar a mensagem do assunto.', true), 'flash_erro');
							}
						};
					} else {
						$this->Session->setFlash(__('Ocorreu um erro ao salvar o assunto.', true), 'flash_erro');
					}
				} else {
					$this->Session->setFlash(__('Ocorreu um erro ao salvar o assunto. Mensagem obrigatoria.', true), 'flash_erro');
				}
			}
		}
	}
	
	function planejamento_visualizar($id = null, $options = null) {
		if (empty($options['conditions'])) $options['conditions'] = array();
		
		$mensagem_erro = false;
	
		if (!empty($id)) {
			$this->Assunto->recursive = 1;
			$assunto = $this->Assunto->find('first', array('conditions' => array('Assunto.id' => $id)));

			
			if (!empty($assunto)) {
		        $usuario = $this->Session->read('Usuario');
				$this->Usuario->Turma->recursive = 0;
				$turmas_do_usuario = $this->Usuario->Turma->find('all', array('Turma.usuario_id' => $usuario['Usuario']['id']));
				$acessivel = false;
				
				foreach ($turmas_do_usuario as $t) {
					if ($t['Turma']['id'] == $assunto['Turma']['id']) {
						$acessivel = true;
					} 
				}
				
				
				if ($acessivel) {

					$this->set('assunto', $assunto);
					$this->paginate = array(
						'limit' => 25,
						'order' => array(
							'Mensagem.id' => 'desc'
						)
					);

					$this->Mensagem->recursive = 2;
					$this->set('mensagens', $this->paginate('Mensagem', 
						array('AND' => array('Mensagem.assunto_id' => $id, 
							  $options['conditions']))
						));
					
				} else {
					$mensagem_erro = true;
				}
				
				
			} else {
				$mensagem_erro = true;
			}
		} else {
			$mensagem_erro = true;
		}
		
		if ($mensagem_erro) {
			$this->Session->setFlash("O assunto selecionado não existe", 'flash_erro');
		}
	}
	
	function planejamento_procurar() {
		if (!empty($this->data['Mensagem']['chave']) && !empty($this->data['Mensagem']['assunto_id'])) {
			$id = $this->data['Mensagem']['assunto_id'];
			$chave = $this->data['Mensagem']['chave'];
			$chaves = preg_split("/[, -.]/", $chave);
			$chaves = array_unique($chaves);

			$empty_key = array_search('', $chaves);
			if (!empty($empty_key)) unset($chaves[$empty_key]);

			function construirQuery($c) {
	            return array('Mensagem.id LIKE' => '%' . $c . '%');
	        }
			$chaves = array_map('construirQuery', $chaves);
			
			$conditions = (empty($chaves)) ? array() : array('OR' => $chaves);
			
			
			$this->planejamento_visualizar($id, array('conditions' => $conditions));
		} else {
			$this->planejamento_visualizar(
				empty($this->data['Mensagem']['assunto_id']) ? 
				"" : $this->data['Mensagem']['assunto_id']
			);
		}
		$this->render('planejamento_visualizar');
	}
	
	
	function planejamento_ajax_mensagens() {
		$this->layout = false;
		$this->autoRender = false;
		Configure::write(array('debug' => 0));
		$json = array('error' => true,'message' => array());
		if(!isset($_POST['assunto'])) {
			array_push($json['message'],"Dados inválidos");
		} else {
			$assunto = $this->Assunto->read(null,$_POST['assunto']);
			if(empty($assunto)) {
				array_push($json['message'],"Assunto não encontrado. Ele pode ter sido removido ou desativado");
			} else {
				$this->Mensagem->unbindModel(array('belongsTo' => array('Assunto')), false);
				$this->Usuario->unbindModel(array(
					'hasOne' => array('FormandoProfile'),
					'hasAndBelongsToMany' => array('Turma','Campanhas'),
					'hasMany' => array('Cupom','Despesa')),false);
				$mensagens = $this->Mensagem->find('all',
					array('conditions' =>
						array('assunto_id' => $assunto['Assunto']['id'],'Mensagem.turma_id' => $assunto["Assunto"]["turma_id"]),
					'limit' => '0,30',
					'order' => array('Mensagem.data desc, Mensagem.id desc')));
				$json = array('error' => false,'mensagens' => $mensagens, 'assunto' => $assunto["Assunto"]);
			}
		}
		echo json_encode($json);
	}
	
	
	
	/* ================== PARTE SUPER ================== */
	
	
	function super_index() {
		/*$this->paginate = array(
			'limit' => 20,
			'conditions' => array(
				'Mensagem.usuario_id' => $this->Auth->user('id'),
				'Mensagem.turma_id' => 12
			),
			'contain' => array(
				'Assunto' => array('Item')
			),
			'order' => array(
				'Mensagem.id' => 'desc'
			)
		);*/
		
		$this->set('assuntos', $this->paginate('Assunto'));
	}
	
	function super_adicionar($id = null) {
		if(!empty($this->data)) {
			
			$this->data['Assunto']['item_id'] = $this->data['Assunto']['itens'];
			//TODO: modificar para session
			$this->data['Assunto']['turma_id'] = 12;
			unset($this->data['Assunto']['itens']);
			
			$this->Assunto->create();
			if ($this->Assunto->save($this->data)) {
				$this->Session->setFlash(__('O assunto foi salvo com sucesso', true), 'flash_sucesso');
				$this->redirect(array('controller' => 'itens', 'action' => 'visualizar', $this->data['Assunto']['item_id']));
			} else {
				$this->Session->setFlash(__('Ocorreu um erro ao salvar o assunto.', true), 'flash_erro');
			}
		}
		
		$itens = array();
		foreach($this->Item->find('all') as $item) {
			$itens[$item['Item']['id']] = $item['Item']['nome'];
		}
		$this->set('itens', $itens);
		$this->set('pendencias', $this->Assunto->pendencias);
	}
	
	function super_editar($id = null) {
		
//		$this->Universidade->id = $id;
//		
//		if (!empty($this->data)) {
//			
//			if ($this->Universidade->save($this->data['Universidade'])){
//				$this->Session->setFlash('Universidade foi Salva corretamente');
//				$this->redirect( array('action' => 'visualizar', $id) );
//			}
//			
//		} else 
//			$this->data = $this->Universidade->read();
//			
//		if(!$this->data)
//			$this->Session->setFlash('Universidade não existente');
//			
	}
	
	function super_visualizar($id = null) {
		$this->paginate = array(
			'limit' => 20,
			'conditions' => array(
				'Assunto.turma_id' => 12,
				'Assunto.item_id' => $id
			),
			'contain' => array('Mensagem'),
			'order' => array(
				'Assunto.id' => 'desc'
			)
		);
		
		$this->set('item', $this->Item->find('first', array('conditions' => array('Item.id' => $id))));
		$this->set('assuntos', $this->paginate('Assunto'));
	}
	
	function super_deletar($id = null) {
		
//		if ($this->Universidade->delete($id))
//			$this->Session->setFlash('Deletado: universidade número ' . $id);
//		else
//			// TODO melhorar estas frases de erro
//			// Talvez colocá-las em um lugar unificado seja interessante
//			$this->Session->setFlash('Erro ao deletar, universidade selecionada é valida?');
//			
//		$this->redirect(array('action' => 'index'));
//		
	}
	
	private function adicionar_arquivos($id){
		$result = true;
		$arquivos_ids = array();
		foreach ($this->data['Assunto'] as $key => $value) {

			if (preg_match("/arquivo[\d]+/i", $key) && $result == true && !empty($value['name'])) {
				$arquivo_id = $this->gravar_arquivo($value, $this->data['Mensagem']['turma_id']);
				if(!empty($arquivo_id)) {
					array_push($arquivos_ids, $arquivo_id);
				} else {
					$result = false;
					break;
				}
			}
		}
		
		if ($result == false) {
			$this->Mensagem->delete($this->Mensagem->id);
			foreach ($arquivos_ids as $id_arq) {
				$this->Arquivo->delete($id_arq);
			}
		}
		
		return $result;
	}
	

	private function gravar_arquivo($arquivoEnviado, $turma_id){
		
		if(!$turma_id && !is_uploaded_file($arquivoEnviado['tmp_name'])) return false;
		
		$usuario = $this->Session->read('Usuario');
		
		$arquivo = $this->Arquivo->create();

		$arquivo['Arquivo']['nome'] = $arquivoEnviado['name'];
		$arquivo['Arquivo']['tipo'] = $arquivoEnviado['type'];
		$arquivo['Arquivo']['tamanho'] = $arquivoEnviado['size'];   
		$arquivo['Arquivo']['usuario_id'] = $usuario['Usuario']['id'];
		$arquivo['Arquivo']['tmp_name'] = $arquivoEnviado['tmp_name'];
		$arquivo['Arquivo']['turma_id'] = $turma_id; 
		$arquivo['Arquivo']['formando_id'] = null;
		$arquivo['Mensagem'] = array('id' => $this->Mensagem->id);
		if($this->Arquivo->save($arquivo)) {
			return $this->Arquivo->id;
		} else {
			//debug($this->Arquivo);
			return null;
		}
			
	}
	
}

?>