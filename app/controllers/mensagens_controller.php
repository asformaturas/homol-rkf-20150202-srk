<?php

class MensagensController extends AppController {

    var $name = 'mensagens';
    var $uses = array('Mensagem', 'Item', 'Assunto', 'Arquivo', 'Usuario', 'MensagemUsuario', 'ViewFormandos');
    var $components = array('Mailer');
    var $nomeDoTemplateSidebar = 'mensagens';
    var $app = array('listar','enviar','enviar_contato','get','marcar_lida');
    
    function beforeFilter() {
        $this->Auth->allow('logo_mensagem_usuario','responder_deslogado');
        parent::beforeFilter();
    }
    
    function planejamento_fale_conosco_atendentes() {
        $this->layout = false;
        $usuario = $this->obterUsuarioLogado();
        if($usuario['Usuario']['nivel'] == 'basico') {
            $this->Session->setFlash('Página permitidas apenas aos administradores', 'metro/flash/error');
            $this->redirect("/{$usuario['Usuario']['grupo']}");
        }
        $atendentes = $this->Usuario->find('all',array(
            'conditions' => array(
                'Usuario.grupo' => 'atendimento',
                'Usuario.ativo' => 1
            )
        ));
        App::import('Component', 'FaleConosco');
        $this->FaleConosco = new FaleConoscoComponent();
        foreach($atendentes as $indice => $atendente) {
            $atendentes[$indice]['atendimentos'] = $this->FaleConosco->detalhesPorAtendente($atendente['Usuario']['id']);
            $atendentes[$indice]['nao_respondidas'] =
                count($this->FaleConosco->mensagensNaoRespondidasPorAtendente($atendente['Usuario']['id']));
        }
        $this->set('atendentes',$atendentes);
    }
    
    function planejamento_fale_conosco_atendente($atendenteId) {
        $this->layout = false;
        $atendente = $this->Usuario->read(null,$atendenteId);
        App::import('Component', 'FaleConosco');
        $this->FaleConosco = new FaleConoscoComponent();
        $mensagens = $this->FaleConosco->mensagensRespondidasPorAtendente($atendenteId);
        $perguntas = array();
        $tempoTotal = 0;
        $mediaTotal = 0;
        if($mensagens) {
            foreach($mensagens as $mensagem) {
                $resposta = array(
                    'pergunta' => false,
                    'resposta' => array(
                        'Mensagem' => $mensagem['Mensagem'],
                        'Assunto' => $mensagem['Assunto'],
                        'Usuario' => $mensagem['Usuario']
                    ),
                    'tempo' => 0
                );
                if(isset($mensagem['MensagemUsuario'][0]['Usuario'])) {
                    $resposta['pergunta'] = $this->FaleConosco->perguntaRespondida(
                        $mensagem['MensagemUsuario'][0]['Usuario']['id'],
                        $atendenteId,
                        $mensagem['Mensagem']['id']);
                    $tempo = $this->FaleConosco->tempoDeResposta($resposta['pergunta']['Mensagem']['data'],
                            $mensagem['Mensagem']['data']);
                    $resposta['tempo'] = $tempo['label'];
                    $tempoTotal+= $tempo['segundos'];
                    $perguntas[] = $resposta;
                }
            }
            $tempoMedio = floor($tempoTotal/count($perguntas));
            $mediaTotal = $this->FaleConosco->converterSegundosParaTexto($tempoMedio);
        }
        $naoRespondidas = $this->FaleConosco->mensagensNaoRespondidasPorAtendente($atendenteId);
        $this->set('atendente',$atendente);
        $this->set('perguntas',$perguntas);
        $this->set('naoRespondidas',$naoRespondidas);
        $this->set('mediaTotal',$mediaTotal);
    }
    
    function logo_mensagem_usuario($mensagemUsuarioId = false) {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        if($mensagemUsuarioId) {
            $this->MensagemUsuario->id = $mensagemUsuarioId;
            $this->MensagemUsuario->saveField('lida',1);
        }
        header("Content-Type: image/jpeg");
        readfile(IMAGES.'logo_rk.jpg');
    }
    
    function atendimento_historico($usuarioId) {
        $atendente = $this->obterUsuarioLogado();
        $this->set("atendente",$atendente);
        $this->layout = false;
        $this->Mensagem->unbindModelAll();
        $this->Mensagem->bindModel(array(
            "hasAndBelongsToMany" => array(
                'Arquivo' => array(
                    'className' => 'Arquivo',
                    'joinTable' => 'arquivos_mensagens',
                    'foreignKey' => 'mensagem_id',
                    'associationForeignKey' => 'arquivo_id',
                    'unique' => true
                )
            )
        ),false);
        $options = array(
            'conditions' => array(
                "MensagemUsuario.usuario_id" => $usuarioId,
                "Item.grupo" => "atendimento",
                "or" => array(
                    "Usuario.grupo" => "atendimento",
                    "Usuario.id" => $usuarioId
                )
            ),
            "fields" => array("Mensagem.*","Assunto.*","Usuario.*"),
            "group" => array("Mensagem.id"),
            "order" => array("Mensagem.id" => "asc"),
            "joins" => array(
                array(
                    'table' => 'usuarios',
                    'alias' => 'Usuario',
                    'type' => 'inner',
                    'foreignKey' => false,
                    'conditions' => array(
                        'Mensagem.usuario_id = Usuario.id'
                    )
                ),
                array(
                    'table' => 'mensagens_usuarios',
                    'alias' => 'MensagemUsuario',
                    'type' => 'inner',
                    'foreignKey' => false,
                    'conditions' => array(
                        'MensagemUsuario.mensagem_id = Mensagem.id'
                    )
                ),
                array(
                    'table' => 'assuntos',
                    'alias' => 'Assunto',
                    'type' => 'inner',
                    'foreignKey' => false,
                    'conditions' => array(
                        'Mensagem.assunto_id = Assunto.id'
                    )
                ),
                array(
                    'table' => 'itens',
                    'alias' => 'Item',
                    'type' => 'inner',
                    'foreignKey' => false,
                    'conditions' => array(
                        'Assunto.item_id = Item.id'
                    )
                )
            )
        );
        $mensagens = $this->Mensagem->find("all",$options);
        $this->set("mensagens",$mensagens);
    }
    
    function atendimento_buscar_formando() {
        if($this->data) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $json = array(
                "formando" => $this->ViewFormandos->find("first",array(
                    "conditions" => array(
                        "codigo_formando" => $this->data["codigo_formando"]
                    )
                )),
                "mensagem" => ""
            );
            if($json["formando"]) {
                if($json["formando"]["ViewFormandos"]["ativo"] == 0 || $json["formando"]["ViewFormandos"]["situacao"] == "cancelado") {
                    $json["mensagem"] = "Este formando foi cancelado";
                    $json["formando"] = false;
                }
            } else {
                $json["mensagem"] = "Formando não encontrado";
            }
            echo json_encode($json);
        } else {
            $this->layout = false;
        }
    }
    
    function atendimento_enviar($usuarioId) {
        $this->layout = false;
        $atendente = $this->obterUsuarioLogado();
        $formando = $this->ViewFormandos->read(null,$usuarioId);
        $this->set("formando",$formando);
        if($atendente && $formando) {
            if($this->data) {
                $this->autoRender = false;
                Configure::write(array('debug' => 0));
                $json = array("mensagem" => "","erro" => true);
                $turma = $this->Turma->read(null,$formando["ViewFormandos"]["turma_id"]);
                $item = $this->Item->get(array(
                    'nome' => "Fale Conosco",
                    'grupo' => 'atendimento'
                ),array(
                    'nome' => "Fale Conosco",
                    'grupo' => 'atendimento'
                ));
                if($item) {
                    $assunto = $this->Assunto->get(array(
                        'item_id' => $item['Item']['id'],
                        'turma_id' => $turma['Turma']['id'],
                        'Assunto.nome' => "Contato"
                    ),array(
                        'item_id' => $item['Item']['id'],
                        'turma_id' => $turma['Turma']['id'],
                        'nome' => "Contato",
                        'pendencia' => 'RK'
                    ));
                    if($assunto) {
                        $data['Mensagem']['turma_id'] = $assunto['Assunto']['turma_id'];
                        $data['Mensagem']['assunto_id'] = $assunto['Assunto']['id'];
                        $data['Mensagem']['usuario_id'] = $atendente['Usuario']['id'];
                        $data['Mensagem']['data'] = date('Y-m-d H:i:s');
                        $data['Mensagem']['pendente'] = 0;
                        $data['Mensagem']['texto'] = $this->data['Mensagem']['texto'];
                        $data['Arquivo'] = $this->data['Arquivo'];
                        $result = $this->_criar($data);
                        if(!$result['erro']) {
                            $usuarios = array(
                                array(
                                    "Usuario" => array(
                                        "id" => $atendente["Usuario"]["id"]
                                    )
                                ),
                                array(
                                    "Usuario" => array(
                                        "id" => $formando["ViewFormandos"]["id"]
                                    )
                                )
                            );
                            $this->adicionarMensagemUsuario($usuarios,$atendente['Usuario']['id']);
                            $this->notificarUsuarios($this->Mensagem->getLastInsertId());
                            $json["erro"] = false;
                        } else {
                            $json["mensagem"] = "Erro ao inserir mensagem. Tente novamente";
                        }
                    } else {
                        $json["mensagem"] = "Erro ao inserir o assunto do contato. Tente novamente.";
                    }
                } else {
                    $json["mensagem"] = "Erro ao inserir item de contato. Tente novamente.";
                }
                echo json_encode($json);
            }
        }
    }
    
    function formulario_contato() {
        $this->layout = false;
        if($this->eFuncionario()) $this->redirect("/");
        $turma = $this->obterTurmaLogada();
        $usuario = $this->obterUsuarioLogado();
        if($this->data) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $item = $this->Item->get(array(
                'nome' => "Fale Conosco",
                'grupo' => 'atendimento'
            ),array(
                'nome' => "Fale Conosco",
                'grupo' => 'atendimento'
            ));
            if(!$item) {
                $this->Session->setFlash('Erro ao enviar mensagem - cod 1', 'metro/flash/error');
            } else {
                $assunto = $this->Assunto->get(array(
                    'item_id' => $item['Item']['id'],
                    'turma_id' => $turma['Turma']['id'],
                    'Assunto.nome' => $this->Assunto->atendimento[$this->data['Mensagem']['assunto']]
                ),array(
                    'item_id' => $item['Item']['id'],
                    'turma_id' => $turma['Turma']['id'],
                    'nome' => $this->Assunto->atendimento[$this->data['Mensagem']['assunto']],
                    'pendencia' => 'RK'
                ));
                if(!$assunto) {
                    $this->Session->setFlash('Erro ao enviar mensagem - cod 2', 'metro/flash/error');
                } else {
                    $data['Mensagem']['turma_id'] = $assunto['Assunto']['turma_id'];
                    $data['Mensagem']['assunto_id'] = $assunto['Assunto']['id'];
                    $data['Mensagem']['usuario_id'] = $usuario['Usuario']['id'];
                    $data['Mensagem']['data'] = date('Y-m-d H:i:s');
                    $data['Mensagem']['texto'] = $this->data['Mensagem']['texto'];
                    $data['Arquivo'] = $this->data['Arquivo'];
                    $result = $this->_criar($data);
                    if(!$result['erro']) {
                        $grupos = array('atendimento');
                        $usuarios = $this->obterUsuariosParaNotificacao(
                            $usuario['Usuario']['id'],
                            $assunto['Assunto']['turma_id'],
                            $grupos);
                        $usuarios[] = $usuario;
                        $this->adicionarMensagemUsuario($usuarios,$usuario['Usuario']['id']);
                        $result['mensagem'][] = "Mensagem enviada com sucesso";
                        $this->Session->setFlash($result['mensagem'], 'metro/flash/success');
                    } else {
                        $this->Session->setFlash('Erro ao enviar mensagem - cod 3', 'metro/flash/error');
                    }
                }
            }
            echo json_encode(array());
        } else {
            $this->set('usuario',$usuario);
            $this->set('turma',$turma);
            $this->set('assuntos',$this->Assunto->atendimento);
        }
    }
    
    function responder_deslogado($mensagemUsuarioId) {
        $this->layout = 'metro/deslogado';
        $this->Usuario->unbindModelAll();
        $mensagemUsuario = $this->MensagemUsuario->read(null,base64_decode($mensagemUsuarioId));
        if(empty($this->data)) {
            $this->Assunto->unbindModel(array('hasMany' => array('Mensagem'), 'belongsTo' => array('Turma')), false);
            $this->Mensagem->unbindModel(array('hasAndBelongsToMany' => array('Arquivo')), false);
            $this->Mensagem->bindModel(array('belongsTo' => array('Assunto')), false);
            $mensagem = $this->Mensagem->read(null,$mensagemUsuario['Mensagem']['id']);
            $this->Turma->unbindModelAll();
            $turma = $this->Turma->read(null,$mensagemUsuario['Mensagem']['turma_id']);
            $usuario = $this->Usuario->read(null,$mensagemUsuario['Usuario']['id']);
            $this->data['Mensagem'] = array(
                'usuario_id' => $usuario['Usuario']['id'],
                'assunto_id' => $mensagem['Assunto']['id'],
                'turma_id' => $mensagem['Mensagem']['turma_id']
            );
            $this->set('usuario',$usuario);
            $this->set('turma',false);
            $this->set('mensagem',$mensagem);
        } else {
            $data = $this->data;
            $assunto = $this->Assunto->find('first', array(
                    'conditions' => array(
                        'Assunto.id' => $data['Mensagem']['assunto_id'])));
            $data['Mensagem']['data'] = date('Y-m-d H:i:s');
            $data['Mensagem']['texto'] = htmlspecialchars($data['Mensagem']['texto']);
            $data['Arquivo'] = (!empty($this->data['Arquivo'])) ? $this->data['Arquivo'] : array();
            $result = $this->_criar($data);
            $usuario = $this->Usuario->read(null,$mensagemUsuario['Usuario']['id']);
            $this->Turma->unbindModelAll();
            $turma = $this->Turma->read(null,$mensagemUsuario['Mensagem']['turma_id']);
            $this->set('usuario',$usuario);
            $this->set('turma',false);
            if(!$result['erro']) {
                $item = $this->Item->read(null,$assunto['Assunto']['item_id']);
                if($item) {
                    if($item['Item']['grupo'] == 'atendimento') {
                        $grupos = array('atendimento');
                    } elseif(!in_array($usuario['Usuario']['grupo'], array('comissao','formando'))) {
                        $grupos = array($usuario['Usuario']['grupo']);
                        $grupos[] = 'comissao';
                    } else {
                        $grupos[] = $item['Item']['grupo'];
                        $grupos[] = 'comissao';
                    }
                } else {
                    $grupos = array($mensagemUsuario['Usuario']['grupo']);
                }
                $usuarios = $this->obterUsuariosParaNotificacao(
                        $usuario['Usuario']['id'],
                        $turma['Turma']['id'],
                        $grupos);
                $usuarios[] = $usuario;
                $this->adicionarMensagemUsuario($usuarios,$usuario['Usuario']['id']);
                $this->notificarUsuarios($this->Mensagem->getLastInsertId());
                $this->Session->setFlash('Mensagem Enviada', 'metro/flash/success');
                $this->set('usuarioCriptografado',base64_encode($usuario['Usuario']['id']));
                $this->render('resposta_deslogado');
            } else {
                $this->Session->setFlash('Erro ao Enviar Mensagem', 'metro/flash/error');
                $this->Assunto->unbindModel(array('hasMany' => array('Mensagem'), 'belongsTo' => array('Turma')), false);
                $this->Mensagem->unbindModel(array('hasAndBelongsToMany' => array('Arquivo')), false);
                $this->Mensagem->bindModel(array('belongsTo' => array('Assunto')), false);
                $mensagem = $this->Mensagem->read(null,$mensagemUsuario['Mensagem']['id']);
                $usuario = $this->Usuario->read(null,$mensagemUsuario['Usuario']['id']);
                $this->data['Mensagem'] = array(
                    'usuario_id' => $usuario['Usuario']['id'],
                    'assunto_id' => $mensagem['Assunto']['id'],
                    'turma_id' => $mensagem['Mensagem']['turma_id']
                );
                $this->set('mensagem',$mensagem);
            }
        }
        $this->set('mensagemUsuarioId',$mensagemUsuarioId);
    }
    
    function responder($mensagemId) {
        $this->layout = false;
        $this->MensagemUsuario->recursive = 3;
        $this->Usuario->unbindModel(array(
            'hasAndBelongsToMany' => array('Campanhas', 'Turma'),
            'hasOne' => array('FormandoProfile'),
            'hasMany' => array('Despesa', 'Cupom')
        ), false);
        $this->Assunto->unbindModel(array('hasMany' => array('Mensagem'), 'belongsTo' => array('Turma')), false);
        $this->Mensagem->unbindModel(array('hasAndBelongsToMany' => array('Arquivo')), false);
        $this->Mensagem->bindModel(array('belongsTo' => array('Assunto')), false);
        $mensagem = $this->Mensagem->read(null,$mensagemId);
        $texto = str_pad('',40, "-", STR_PAD_LEFT) . '<br /><br />';
        $texto = html_entity_decode($mensagem['Mensagem']['texto']);
        $this->set('texto',$texto);
        $data = array(
            'Mensagem' => array(
                'assunto_id' => $mensagem['Mensagem']['assunto_id'],
                'mensagem_id' => $mensagem['Mensagem']['id']
            ),
            'Assunto' => array(
                'id' => $mensagem['Mensagem']['assunto_id'],
                'nome' => $mensagem['Assunto']['nome'],
                'Item' => array(
                    'id' => $mensagem['Assunto']['item_id'],
                    'nome' => $mensagem['Assunto']['Item']['nome']
                )
            )
        );
        $this->data = $data;
        $usuario = $this->Session->read('Usuario');
        if($usuario['Usuario']['grupo'] == 'planejamento') {
            array_pop($this->Assunto->pendencias);
            $this->set('pendencias',$this->Assunto->pendencias);
            $this->set('resolvido',array('Não','Sim'));
        }
        $this->set('usuario',$usuario);
        $this->render('_responder');
    }
    
    function adicionar() {
        $this->layout = false;
        $usuario = $this->Session->read('Usuario');
        if($usuario['Usuario']['grupo'] == 'planejamento')
            $this->set('pendencias',$this->Assunto->pendencias);
        $this->set('usuario',$usuario);
        $this->render('_adicionar');
    }
    
    function adicionar_informativo() {
        if($this->eFuncionario()) {
            $this->layout = false;
            $usuario = $this->obterUsuarioLogado();
            $this->set('usuario',$usuario);
            $this->set('grupos',$this->Usuario->grupos['formandos']);
            $this->render('_adicionar_informativo');
        }
    }
    
    private function _criar($data) {
        $this->Mensagem->create();
        $return = array('erro' => true,'mensagem' => array());
        if($this->Mensagem->save($data)) {
            $return['erro'] = false;
            $mensagem = array('id' => $this->Mensagem->getLastInsertId());
            if(isset($data['Arquivo']))
                foreach($data['Arquivo'] as $arquivo) {
                    $arquivo['nome_secreto'] = $this->Arquivo->generateUniqueId();
                    $arquivo['diretorio'] = $this->Arquivo->file_path();
                    $arquivo['path'] = $arquivo['diretorio'].
                            $arquivo['nome_secreto'];
                    if($this->Arquivo->saveBase64File($arquivo['src'],
                            $arquivo['path'])) {
                        $this->Arquivo->create();
                        $arquivo['usuario_id'] = $data['Mensagem']['usuario_id'];
                        $arquivo['turma_id'] = $data['Mensagem']['turma_id'];
                        $arquivo['formando_id'] = null;
                        $arquivo['criado'] = date('Y-m-d H:i:s');
                        if(!$this->Arquivo->save(array('Arquivo' => $arquivo,'Mensagem' => $mensagem),
                                array('callbacks' => false))) {
                            $return['mensagem'][] = "Erro ao inserir anexos";
                            break;
                        }
                    }
                }
        } elseif(isset($this->Mensagem->validationErrors)) {
            $return['mensagem'] = array_merge($return['mensagem'],array_values($this->Mensagem->validationErrors));
        }
        return $return;
    }
    
    function app_enviar() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('erro' => true,'mensagem' => array());
        if(isset($this->data['Mensagem'])) {
            $mensagem = $this->data['Mensagem'];
            $this->Assunto->unbindModel(array(
                'belongsTo' => array('Turma')
            ),false);
            $assunto = $this->Assunto->read(null,$mensagem['assunto_id']);
            $usuario = $this->Usuario->read(null,$mensagem['usuario_id']);
            if(!$usuario) {
                $json['mensagem'][] = "Usuário não encontrado";
            } elseif(!$assunto) {
                $json['mensagem'][] = "Assunto não encontrado";
            } elseif($assunto['Assunto']['resolvido'] == true) {
                $json['mensagem'][] = 'O assunto está marcado como resolvido, não é possível adicionar novas mensagens';
            } else {
                $data['Mensagem']['turma_id'] = $assunto['Assunto']['turma_id'];
                $data['Mensagem']['assunto_id'] = $assunto['Assunto']['id'];
                $data['Mensagem']['usuario_id'] = $usuario['Usuario']['id'];
                $data['Mensagem']['data'] = date('Y-m-d H:i:s');
                $data['Mensagem']['texto'] = $mensagem['texto'];
                $json = $this->_criar($data);
                if(!$json['erro']) {
                    $mensagemId = $this->Mensagem->getLastInsertId();
                    if($assunto['Item']['grupo'] == 'atendimento') {
                        $grupos = array('atendimento');
                    } else {
                        $grupos = array($usuario['Usuario']['grupo']);
                        $grupos[] = $assunto['Item']['grupo'];
                    }
                    $usuarios = $this->obterUsuariosParaNotificacao(
                        $usuario['Usuario']['id'],
                        $assunto['Assunto']['turma_id'],
                        $grupos);
                    $usuarios[] = $usuario;
                    $this->adicionarMensagemUsuario($usuarios,$usuario['Usuario']['id']);
                    $this->MensagemUsuario->recursive = 0;
                    $mensagemUsuario = $this->MensagemUsuario->find('first',array(
                        'conditions' => array(
                            'MensagemUsuario.mensagem_id' => $mensagemId,
                            'MensagemUsuario.usuario_id' => $usuario['Usuario']['id']
                        )
                    ));
                    if($mensagemUsuario)
                        $json['mensagem_usuario_id'] = $mensagemUsuario['MensagemUsuario']['id'];
                }
            }
        }
        echo json_encode($json);
    }
    
    function app_enviar_contato() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('erro' => true,'mensagem' => array());
        if(isset($this->data['Mensagem'])) {
            $mensagem = $this->data['Mensagem'];
            $usuario = $this->Usuario->read(null,$mensagem['usuario_id']);
            if(!$usuario) {
                $json['mensagem'][] = "Usuário não encontrado";
            } else {
                $item = $this->Item->get(array(
                    'nome' => "Fale Conosco",
                    'grupo' => 'atendimento'
                ),array(
                    'nome' => "Fale Conosco",
                    'grupo' => 'atendimento'
                ));
                if(!$item) {
                    $json['mensagem'][] = "Erro ao enviar mensagem - cod 1";
                } else {
                    $assunto = $this->Assunto->get(array(
                        'item_id' => $item['Item']['id'],
                        'turma_id' => $this->data['Mensagem']['turma_id'],
                        'Assunto.nome' => $this->Assunto->atendimento[$this->data['Mensagem']['assunto']]
                    ),array(
                        'item_id' => $item['Item']['id'],
                        'turma_id' => $this->data['Mensagem']['turma_id'],
                        'nome' => $this->Assunto->atendimento[$this->data['Mensagem']['assunto']],
                        'pendencia' => 'RK'
                    ));
                    if(!$assunto) {
                        $json['mensagem'][] = "Erro ao enviar mensagem - cod 1";
                    } else {
                        $data['Mensagem']['turma_id'] = $assunto['Assunto']['turma_id'];
                        $data['Mensagem']['assunto_id'] = $assunto['Assunto']['id'];
                        $data['Mensagem']['usuario_id'] = $usuario['Usuario']['id'];
                        $data['Mensagem']['data'] = date('Y-m-d H:i:s');
                        $data['Mensagem']['texto'] = $this->data['Mensagem']['texto'];
                        $json = $this->_criar($data);
                        if(!$json['erro']) {
                            $mensagemId = $this->Mensagem->getLastInsertId();
                            $grupos = array('atendimento');
                            $usuarios = $this->obterUsuariosParaNotificacao(
                                $usuario['Usuario']['id'],
                                $assunto['Assunto']['turma_id'],
                                $grupos);
                            $usuarios[] = $usuario;
                            $this->adicionarMensagemUsuario($usuarios,$usuario['Usuario']['id']);
                            $this->MensagemUsuario->recursive = 0;
                            $mensagemUsuario = $this->MensagemUsuario->find('first',array(
                                'conditions' => array(
                                    'MensagemUsuario.mensagem_id' => $mensagemId,
                                    'MensagemUsuario.usuario_id' => $usuario['Usuario']['id']
                                )
                            ));
                            if($mensagemUsuario)
                                $json['mensagem_usuario_id'] = $mensagemUsuario['MensagemUsuario']['id'];
                        } else {
                            $this->Session->setFlash('Erro ao enviar mensagem - cod 3', 'metro/flash/error');
                        }
                    }
                }
            }
        }
        echo json_encode($json);
    }
    
    function contato_inserir() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        if(isset($this->data['Mensagem']['mensagem_id'])) {
            set_time_limit(0);
            ini_set('memory_limit', '512M');
            $turma = $this->obterTurmaLogada();
            $usuario = $this->Session->read('Usuario');
            $data = $this->data;
            $this->Mensagem->bindModel(array('belongsTo' => array('Assunto')), false);
            $mensagemRespondida = $this->Mensagem->read(null,$data['Mensagem']['mensagem_id']);
            if(!$mensagemRespondida) {
                $this->Session->setFlash("Mensagem não encontrada", 'metro/flash/error');
            } else {
                $data['Mensagem']['pendente'] = $usuario['Usuario']['grupo'] == 'atendimento' ? 0 : 1;
                $data['Mensagem']['turma_id'] = $mensagemRespondida['Mensagem']['turma_id'];
                $data['Mensagem']['usuario_id'] = $usuario['Usuario']['id'];
                $data['Mensagem']['data'] = date('Y-m-d H:i:s');
                $data['Arquivo'] = $this->data['Arquivo'];
                $result = $this->_criar($data);
                if(!$result['erro']) {
                    if($usuario['Usuario']['grupo'] == 'atendimento') {
                        $mensagemRespondida['Mensagem']['pendente'] = 0;
                        $this->Mensagem->save($mensagemRespondida);
                    }
                    $usuarios = array($usuario);
                    $usuarios[] = array(
                        'Usuario' => $mensagemRespondida['Usuario']
                    );
                    $this->adicionarMensagemUsuario($usuarios,$usuario['Usuario']['id']);
                    $this->notificarUsuarios($this->Mensagem->getLastInsertId());
                    $result['mensagem'][] = "Mensagem enviada com sucesso";
                    $this->Session->setFlash($result['mensagem'], 'metro/flash/success');
                } else {
                    $this->Session->setFlash('Erro ao Enviar Mensagem', 'metro/flash/error');
                }
            }
        }
    }
    
    function inserir() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        if($this->data) {
            set_time_limit(0);
            ini_set('memory_limit', '512M');
            $turma = $this->obterTurmaLogada();
            $usuario = $this->Session->read('Usuario');
            if($usuario['Usuario']['grupo'] == 'atendimento' || $usuario['Usuario']['grupo'] == 'formando') {
                $this->contato_inserir();
            } else {
                $grupos = array('comissao');
                $data = $this->data;
                if(isset($data['Assunto']['nome'])) {
                    $assunto = $this->Assunto->find('first',array(
                        'conditions' => array(
                            'Assunto.turma_id' => $turma['Turma']['id'],
                            'Assunto.item_id' => $data['Item']['id'],
                            'Assunto.nome' => trim($data['Assunto']['nome'])
                        )
                    ));
                    if(empty($assunto)) {
                        $data['Assunto']['item_id'] = $data['Item']['id'];
                        $data['Assunto']['usuario_id'] = $usuario['Usuario']['id'];
                        $data['Assunto']['turma_id'] = $turma['Turma']['id'];
                        $this->Assunto->create();
                        if ($this->Assunto->save($data))
                            $data['Mensagem']['assunto_id'] = $this->Assunto->getLastInsertId();
                        else
                            $this->Session->setFlash("Erro ao criar Assunto", 'metro/flash/error');
                    } else {
                        $this->Session->setFlash('Ja foi aberto um Assunto com esse nome', 'metro/flash/error');
                    }
                }
                if(isset($data['Mensagem']['assunto_id'])) {
                    $assunto = $this->Assunto->find('first', array(
                        'conditions' => array(
                            'Assunto.id' => $data['Mensagem']['assunto_id'])));
                    if($assunto['Assunto']['resolvido'] == true) {
                        $this->Session->setFlash('O assunto está marcado como resolvido, não é possível adicionar novas mensagens',
                                'metro/flash/error');
                    } else {
                        $data['Mensagem']['turma_id'] = $assunto['Assunto']['turma_id'];
                        $data['Mensagem']['usuario_id'] = $usuario['Usuario']['id'];
                        $data['Mensagem']['data'] = date('Y-m-d H:i:s');
                        $data['Mensagem']['texto'] = htmlspecialchars($data['Mensagem']['texto']);
                        $data['Arquivo'] = (!empty($this->data['Arquivo'])) ? $this->data['Arquivo'] : array();
                        $result = $this->_criar($data);
                        if(!$result['erro']) {
                            $assunto['Assunto']['pendencia'] = $data['Assunto']['pendencia'];
                            $assunto['Assunto']['resolvido'] = $data['Assunto']['resolvido'];
                            $this->Assunto->save($assunto);
                            $item = $this->Item->read(null,$assunto['Assunto']['item_id']);
                            if($item)
                                $grupos[] = $item['Item']['grupo'];
                            $usuarios = $this->obterUsuariosParaNotificacao(
                                    $usuario['Usuario']['id'],
                                    $assunto['Assunto']['turma_id'],
                                    $grupos);
                            $usuarios[] = $usuario;
                            $this->adicionarMensagemUsuario($usuarios,$usuario['Usuario']['id']);
                            $this->notificarUsuarios($this->Mensagem->getLastInsertId());
                            $result['mensagem'][] = "Mensagem enviada com sucesso";
                            $this->Session->setFlash($result['mensagem'], 'metro/flash/success');
                        } else {
                            $this->Session->setFlash('Erro ao Enviar Mensagem', 'metro/flash/error');
                        }
                    }
                }
            }
        }
        echo json_encode(array());
    }
    
    function inserir_informativo() {
        $this->autoRender = false;
        set_time_limit(0);
        ini_set('memory_limit', '512M');
        Configure::write(array('debug' => 0));
        $turma = $this->obterTurmaLogada();
        $usuario = $this->obterUsuarioLogado();
        if($this->data && $this->eFuncionario()) {
            $grupos = $this->data['Mensagem']['grupos'];
            unset($this->data['Mensagem']['grupos']);
            $data = $this->data;
            $item = $this->Item->get(array(
                'nome' => "Informativos",
                'grupo' => $usuario['Usuario']['grupo']
            ),array(
                'nome' => "Informativos",
                'grupo' => $usuario['Usuario']['grupo']
            ));
            if(!$item) {
                $this->Session->setFlash('Erro ao enviar informativo - cod 1', 'metro/flash/error');
            } else {
                $assunto = $this->Assunto->get(array(
                    'item_id' => $item['Item']['id'],
                    'turma_id' => $turma['Turma']['id'],
                    'Assunto.nome' => 'Informativo'
                ),array(
                    'item_id' => $item['Item']['id'],
                    'turma_id' => $turma['Turma']['id'],
                    'nome' => 'Informativo',
                    'pendencia' => "Informativo"
                ));
                if(!$assunto) {
                    $this->Session->setFlash('Erro ao enviar mensagem - cod 2', 'metro/flash/error');
                } else {
                    $data['Mensagem']['turma_id'] = $assunto['Assunto']['turma_id'];
                    $data['Mensagem']['pendente'] = 0;
                    $data['Mensagem']['assunto_id'] = $assunto['Assunto']['id'];
                    $data['Mensagem']['usuario_id'] = $usuario['Usuario']['id'];
                    $data['Mensagem']['data'] = date('Y-m-d H:i:s');
                    $data['Mensagem']['texto'] = $this->data['Mensagem']['texto'];
                    $data['Arquivo'] = $this->data['Arquivo'];
                    $result = $this->_criar($data);
                    if(!$result['erro']) {
                        $usuarios = $this->obterUsuariosParaNotificacao(
                            $usuario['Usuario']['id'],
                            $assunto['Assunto']['turma_id'],
                            $grupos);
                        $usuarios[] = $usuario;
                        $this->adicionarMensagemUsuario($usuarios,$usuario['Usuario']['id']);
                        $this->notificarUsuarios($this->Mensagem->getLastInsertId());
                        $result['mensagem'][] = "Mensagem enviada com sucesso";
                        $this->Session->setFlash($result['mensagem'], 'metro/flash/success');
                    } else {
                        $this->Session->setFlash('Erro ao enviar mensagem - cod 3', 'metro/flash/error');
                    }
                }
            }
        }
        echo json_encode(array());
    }
    
    function informativos() {
        $this->autoLayout = false;
        $usuario = $this->obterUsuarioLogado();
        $this->set('usuario',$usuario);
        $this->set('tipo','informativos');
        $this->set('exibirCronograma',false);
        if($this->turmaEstaLogada())
            $this->render('_email');
        else
            $this->render('_email_sem_turma');
    }
    
    function email() {
        $this->autoLayout = false;
        $usuario = $this->obterUsuarioLogado();
        $this->set('usuario',$usuario);
        $this->set('tipo','');
        $this->set('exibirCronograma',$usuario['Usuario']['grupo'] != 'atendimento' && $usuario['Usuario']['grupo'] != 'formando');
        if($this->turmaEstaLogada()) 
            $this->render('_email');
        else
            $this->render('_email_sem_turma');
    }
    
    private function _obterItensPorGrupo($grupo) {
        if($grupo == 'comercial') {
            $itens = $this->Item->find('all',array(
                'conditions' => array("Item.nome like('Comercial:%')")
            ));
        } else {
            $turma = $this->obterTurmaLogada();
            if($grupo == 'comissao')
                $itens = array_merge($this->Item->obterPorTurmaEGrupo($turma['Turma']['id'],'comercial'),
                        $this->Item->obterPorTurmaEGrupo($turma['Turma']['id'],'planejamento'));
            else
                $itens = $this->Item->obterPorTurmaEGrupo($turma['Turma']['id'],$grupo);
        }
        return $itens;
    }
    
    private function _cronograma() {
        $this->layout = false;
        $turma = $this->obterTurmaLogada();
        $usuario = $this->Session->read('Usuario');
        $itens = $this->_obterItensPorGrupo($usuario['Usuario']['grupo']);
        $agrupados = array();
        foreach($itens as $item) {
            $pendencias = $this->Assunto->find('first',array(
                'conditions' => array(
                    'turma_id' => $turma['Turma']['id'],
                    'item_id' => $item['Item']['id']
                ),
                'fields' => array(
                    "sum(if(Assunto.pendencia = 'RK',1,0)) as as_eventos",
                    "sum(if(Assunto.pendencia = 'Comissao',1,0)) as comissao",
                    "sum(if(Assunto.pendencia <> 'Informativo',resolvido,0)) as resolvidos",
                    "sum(if(Assunto.pendencia <> 'Informativo',1,0)) as total"
                ),
                'group' => array('item_id')
            ));
            if(strpos($item['Item']['nome'], ':') === FALSE) {
                $agrupados[][] = array(
                    'nome' => $item['Item']['nome'],
                    'pendencias' => $pendencias,
                    'id' => $item['Item']['id']
                );
            } else {
                $tipo = strtoupper(trim(array_shift(explode(':', $item['Item']['nome']))));
                $nome = trim(array_pop(explode(':', $item['Item']['nome'])));
                $agrupados[$tipo][] = array(
                    'nome' => $nome,
                    'pendencias' => $pendencias,
                    'id' => $item['Item']['id']
                );
            }
        }
        $this->set('itens',$agrupados);
        $this->set('usuario',$usuario);
        $this->render('_cronograma');
    }
    
    function comissao_cronograma() {
        $this->_cronograma();
    }
    
    function planejamento_cronograma() {
        $this->_cronograma();
    }
    
    function criacao_cronograma() {
        $this->_cronograma();
    }
    
    function rh_cronograma() {
        $this->_cronograma();
    }
    
    function producao_cronograma() {
        $this->_cronograma();
    }
    
    function foto_cronograma() {
        $this->_cronograma();
    }
    
    function video_cronograma() {
        $this->_cronograma();
    }
    
    function comercial_cronograma() {
        $turma = $this->obterTurmaLogada();
        $itens = $this->Item->find('all',array(
            'conditions' => array("Item.nome like('Comercial%')")
        ));
        foreach($itens as $item) {
            $nome = trim(array_pop(explode(':', $item['Item']['nome'])));
            $assunto = $this->Assunto->find('count',array(
                'conditions' => array(
                    'Assunto.turma_id' => $turma['Turma']['id'],
                    'Assunto.item_id' => $item['Item']['id'],
                    'Assunto.nome' => $nome
                )
            ));
            if($assunto == 0) {
                $novoAssunto['Assunto'] = array(
                    'turma_id' => $turma['Turma']['id'],
                    'item_id' => $item['Item']['id'],
                    'nome' => $nome,
                    'pendencia' => 'RK'
                );
                $this->Assunto->create();
                $this->Assunto->save($novoAssunto);
            }
        }
        $this->_cronograma();
    }
    
    private function _carregar_assuntos($itemId) {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $turma = $this->obterTurmaLogada();
        $this->Assunto->recursive = -1;
        $assuntos = $this->Assunto->find('all',
                array('conditions' => array('item_id' => $itemId,'turma_id' => $turma['Turma']['id'])));
        echo json_encode(array('assuntos' => $assuntos));
    }
    
    function comissao_carregar_assuntos($itemId) {
        $this->_carregar_assuntos($itemId);
    }
    
    function comercial_carregar_assuntos($itemId) {
        $this->_carregar_assuntos($itemId);
    }
    
    function planejamento_carregar_assuntos($itemId) {
        $this->_carregar_assuntos($itemId);
    }
    
    function criacao_carregar_assuntos($itemId) {
        $this->_carregar_assuntos($itemId);
    }
    
    function rh_carregar_assuntos($itemId) {
        $this->_carregar_assuntos($itemId);
    }
    
    function producao_carregar_assuntos($itemId) {
        $this->_carregar_assuntos($itemId);
    }
    
    function foto_carregar_assuntos($itemId) {
        $this->_carregar_assuntos($itemId);
    }
    
    function video_carregar_assuntos($itemId) {
        $this->_carregar_assuntos($itemId);
    }
    
    private function _carregar_assuntos_nao_resolvidos($itemId) {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $turma = $this->obterTurmaLogada();
        $this->Assunto->recursive = -1;
        $assuntos = $this->Assunto->find('all',
                array('conditions' => array('item_id' => $itemId,
                    'turma_id' => $turma['Turma']['id'],
                    'resolvido' => 0)));
        echo json_encode(array('assuntos' => $assuntos));
    }
    
    function comissao_carregar_assuntos_nao_resolvidos($itemId) {
        $this->_carregar_assuntos_nao_resolvidos($itemId);
    }
    
    function comercial_carregar_assuntos_nao_resolvidos($itemId) {
        $this->_carregar_assuntos_nao_resolvidos($itemId);
    }
    
    function planejamento_carregar_assuntos_nao_resolvidos($itemId) {
        $this->_carregar_assuntos_nao_resolvidos($itemId);
    }
    
    function criacao_carregar_assuntos_nao_resolvidos($itemId) {
        $this->_carregar_assuntos_nao_resolvidos($itemId);
    }
    
    function foto_carregar_assuntos_nao_resolvidos($itemId) {
        $this->_carregar_assuntos_nao_resolvidos($itemId);
    }
    
    function rh_carregar_assuntos_nao_resolvidos($itemId) {
        $this->_carregar_assuntos_nao_resolvidos($itemId);
    }
    
    function producao_carregar_assuntos_nao_resolvidos($itemId) {
        $this->_carregar_assuntos_nao_resolvidos($itemId);
    }
    
    function video_carregar_assuntos_nao_resolvidos($itemId) {
        $this->_carregar_assuntos_nao_resolvidos($itemId);
    }
    
    function _carregar_itens_nao_resolvidos() {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $turma = $this->obterTurmaLogada();
        $usuario = $this->Session->read('Usuario');
        if($usuario['Usuario']['grupo'] == 'comercial')
            $itens = $this->_obterItensPorGrupo($usuario['Usuario']['grupo']);
        else
            $itens = $this->Item->obterNaoResolvidosPorTurmaId(
                $turma['Turma']['id']);
        echo json_encode(array('itens' => $itens));
    }
    
    function comissao_carregar_itens_nao_resolvidos() {
        $this->_carregar_itens_nao_resolvidos();
    }
    
    function comercial_carregar_itens_nao_resolvidos() {
        $this->_carregar_itens_nao_resolvidos();
    }
    
    function planejamento_carregar_itens_nao_resolvidos() {
        $this->_carregar_itens_nao_resolvidos();
    }
    
    function criacao_carregar_itens_nao_resolvidos() {
        $this->_carregar_itens_nao_resolvidos();
    }
    
    function foto_carregar_itens_nao_resolvidos() {
        $this->_carregar_itens_nao_resolvidos();
    }
    
    function rh_carregar_itens_nao_resolvidos() {
        $this->_carregar_itens_nao_resolvidos();
    }
    
    function producao_carregar_itens_nao_resolvidos() {
        $this->_carregar_itens_nao_resolvidos();
    }
    
    function video_carregar_itens_nao_resolvidos() {
        $this->_carregar_itens_nao_resolvidos();
    }
    
    function _carregar($assuntoId) {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $turma = $this->obterTurmaLogada();
        $this->Usuario->unbindModelAll();
        $this->Mensagem->bindModel(array('belongsTo' => array('Assunto')),false);
        $this->Assunto->unbindModel(array('belongsTo' => array('Turma'),'hasMany' => array('Mensagem')), false);
        $mensagens = $this->Mensagem->find('all',array(
            'conditions' => array(
                'Mensagem.assunto_id' => $assuntoId,
                'Mensagem.turma_id' => $turma['Turma']['id']
            )
        ));
        echo json_encode(array('mensagens' => $mensagens));
    }
    
    function comissao_carregar($assuntoId) {
        $this->_carregar($assuntoId);
    }
    
    function comercial_carregar($assuntoId) {
        $this->_carregar($assuntoId);
    }
    
    function planejamento_carregar($assuntoId) {
        $this->_carregar($assuntoId);
    }
    
    function criacao_carregar($assuntoId) {
        $this->_carregar($assuntoId);
    }
    
    function rh_carregar($assuntoId) {
        $this->_carregar($assuntoId);
    }
    
    function producao_carregar($assuntoId) {
        $this->_carregar($assuntoId);
    }
    
    function foto_carregar($assuntoId) {
        $this->_carregar($assuntoId);
    }
    
    function video_carregar($assuntoId) {
        $this->_carregar($assuntoId);
    }
    
    function _historico_assunto($assuntoId) {
        $this->layout = false;
        $this->Usuario->unbindModelAll();
        $this->Mensagem->bindModel(array('belongsTo' => array('Assunto')),false);
        $this->Assunto->unbindModel(array('belongsTo' => array('Turma'),'hasMany' => array('Mensagem')), false);
        $mensagens = $this->Mensagem->find('all',array(
            'conditions' => array(
                'Mensagem.assunto_id' => $assuntoId
            )
        ));
        $this->set('mensagens',$mensagens);
        $this->render('_historico_assunto');
    }
    
    function planejamento_historico_assunto($assuntoId) {
        $this->_historico_assunto($assuntoId);
    }
    
    function criacao_historico_assunto($assuntoId) {
        $this->_historico_assunto($assuntoId);
    }
    
    function foto_historico_assunto($assuntoId) {
        $this->_historico_assunto($assuntoId);
    }
    
    function rh_historico_assunto($assuntoId) {
        $this->_historico_assunto($assuntoId);
    }
    
    function producao_historico_assunto($assuntoId) {
        $this->_historico_assunto($assuntoId);
    }
    
    function video_historico_assunto($assuntoId) {
        $this->_historico_assunto($assuntoId);
    }
    
    function comissao_historico_assunto($assuntoId) {
        $this->_historico_assunto($assuntoId);
    }

    function atualizar() {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        if (isset($_POST['mensagens'])) {
            foreach ($_POST['mensagens'] as $m) {
                $this->MensagemUsuario->read(null, $m['mensagem']['MensagemUsuario']['id']);
                $this->MensagemUsuario->set('favorita', $m['mensagem']['MensagemUsuario']['favorita']);
                $this->MensagemUsuario->set('lida', $m['mensagem']['MensagemUsuario']['lida']);
                $this->MensagemUsuario->set('descartada', $m['mensagem']['MensagemUsuario']['descartada']);
                $this->MensagemUsuario->save();
            }
            if (!isset($this->noEcho))
                echo json_encode(array('error' => false));
        }
    }
    
    function app_marcar_lida() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('erro' => true);
        if(isset($this->data['mensagem_usuario_id'])) {
            $this->MensagemUsuario->unbindModelAll();
            $mensagem = $this->MensagemUsuario->read(null,$this->data['mensagem_usuario_id']);
            if($mensagem) {
                if($mensagem['MensagemUsuario']['lida'] == 0) {
                    $mensagem['MensagemUsuario']['lida'] = 1;
                    $json['erro'] = !$this->MensagemUsuario->save($mensagem);
                } else {
                    $json['erro'] = false;
                }
            }
        }
        echo json_encode($json);
    }
    
    function app_get() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('mensagem' => false);
        if(isset($this->data['mensagem_usuario_id'])) {
            $this->MensagemUsuario->recursive = 3;
            $this->Usuario->unbindModelAll();
            $this->Assunto->unbindModel(array('hasMany' => array('Mensagem'),
                'belongsTo' => array('Turma')), false);
            $this->MensagemUsuario->unbindModel(array(
                'belongsTo' => array('Usuario')
            ),false);
            $this->Mensagem->bindModel(array('belongsTo' => array('Assunto')), false);
            $mensagem = $this->MensagemUsuario->read(null,$this->data['mensagem_usuario_id']);
            if($mensagem) {
                $item = array(
                    "Mensagem" => array(
                        'id' => $mensagem['Mensagem']['id'],
                        'mensagem_usuario_id' => $mensagem['MensagemUsuario']['id'],
                        'data_cadastro' => $mensagem['MensagemUsuario']['data_cadastro'],
                        'lida' => $mensagem['MensagemUsuario']['lida'],
                        'favorita' => $mensagem['MensagemUsuario']['favorita'],
                        'texto' => html_entity_decode($mensagem["Mensagem"]['texto']),
                        'assunto' => $mensagem["Mensagem"]['Assunto']['nome'],
                        'assunto_id' => $mensagem["Mensagem"]['Assunto']['id'],
                        'item' => $mensagem["Mensagem"]['Assunto']['Item']['nome'],
                        'pode_responder' => ($mensagem['Mensagem']['Assunto']['pendencia'] != "Informativo" &&
                                $mensagem['Mensagem']['Assunto']['resolvido'] == 0)
                    ),
                    "Usuario" => array(
                        'id' => $mensagem['Mensagem']['Usuario']['id'],
                        'nome' => $mensagem['Mensagem']['Usuario']['nome'],
                        'grupo' => $mensagem['Mensagem']['Usuario']['grupo'],
                        'email' => $mensagem['Mensagem']['Usuario']['email'],
                        'diretorio_foto_perfil' => $mensagem['Mensagem']['Usuario']['diretorio_foto_perfil'],
                    ),
                    "Anexos" => array()
                );
                foreach($mensagem['Mensagem']['Arquivo'] as $arquivo)
                    $item['Anexos'][] = array(
                        "id" => $arquivo['id'],
                        "nome" => $arquivo['nome']
                    );
                $json['mensagem'] = $item;
            }
        }
        echo json_encode($json);
    }
    
    function app_listar() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array();
        if(isset($this->data['usuario'])) {
            $options = array(
                'conditions' => array(
                    'MensagemUsuario.usuario_id' => $this->data['usuario']
                )
            );
            if(isset($this->data['mensagem_usuario_id']))
                $options['conditions'][] = "MensagemUsuario.id > {$this->data['mensagem_usuario_id']}";
            $this->MensagemUsuario->unbindModelAll();
            $mensagens = $this->MensagemUsuario->find('list',$options);
            if($mensagens)
                $json['mensagens'] = $mensagens;
            else
                $json['mensagens'] = false;
        }
        echo json_encode($json);
    }

    function listar($tipo = 'todas') {
        $this->layout = false;
        $this->autoRender = false;
        set_time_limit(0);
        ini_set('memory_limit', '60M');
        Configure::write(array('debug' => 0));
        $json = array('error' => true, 'message' => array());
        $usuario = $this->Session->read('Usuario');
        $turma = $this->obterTurmaLogada();
        if (!$usuario) {
            array_push($json['message'], "Usuário não está logado. Por favor faça login e tente novamente.");
        } else {
            $this->MensagemUsuario->recursive = 3;
            $this->Usuario->unbindModelAll();
            $this->Assunto->unbindModel(array('hasMany' => array('Mensagem'), 'belongsTo' => array('Turma')), false);
            $this->Mensagem->unbindModel(array('hasAndBelongsToMany' => array('Arquivo')), false);
            $this->Mensagem->bindModel(array('belongsTo' => array('Assunto')), false);
            $conditions = array(
                'MensagemUsuario.usuario_id' => $usuario['Usuario']['id']
            );
            if($turma)
                $conditions['Mensagem.turma_id'] = $turma['Turma']['id'];
            if($tipo == 'informativos') {
                // $c = array('Assunto.pendencia' => 'Informativo');
                if($turma)
                    $c['Assunto.turma_id'] = $turma['Turma']['id'];
                $assuntos = $this->Assunto->find('list',array(
                    'conditions' => $c
                ));
                if($assuntos)
                    $conditions['Mensagem.assunto_id'] = $assuntos;
            }
            $mensagens = $this->MensagemUsuario->find('all', array('conditions' => $conditions,
                'order' => array('MensagemUsuario.data_cadastro desc, Mensagem.data desc'),
                'limit' => 1000));
            foreach ($mensagens as $indice => $mensagem) {
                $mensagens[$indice]["Mensagem"]['texto'] = html_entity_decode($mensagens[$indice]["Mensagem"]['texto']);
                $mensagens[$indice]["MensagemUsuario"]['time'] = strtotime($mensagens[$indice]["MensagemUsuario"]['data_cadastro']);
                $anexos = $this->Mensagem->query("select count(0) as qtde from arquivos_mensagens where mensagem_id = {$mensagem['Mensagem']['id']}");
                $mensagens[$indice]["Mensagem"]['anexos'] = $anexos[0][0]['qtde'];
            }
        }
        echo json_encode(array('mensagens' => $mensagens));
    }
    
    function exibir($mensagemUsuarioId) {
        $this->layout = false;
        $usuario = $this->Session->read('Usuario');
        $this->MensagemUsuario->recursive = 3;
        $this->Usuario->unbindModelAll();
        $this->Assunto->unbindModel(array('belongsTo' => array('Turma'),'hasMany' => array('Mensagem')), false);
        $this->Mensagem->bindModel(array(
            'belongsTo' => array('Assunto'),
        ), false);
        $mensagem = $this->MensagemUsuario->read(null,$mensagemUsuarioId);
        if($mensagem) {
            $mensagem['MensagemUsuario']['lida'] = 1;
            $this->MensagemUsuario->save($mensagem);
            if($mensagem['Mensagem']['Assunto']['pendencia'] != "Informativo" &&
                $mensagem['Mensagem']['Assunto']['resolvido'] == 0 &&
                $mensagem['Mensagem']['usuario_id'] != $usuario['Usuario']['id'])
                $this->set('podeResponder',true);
            else
                $this->set('podeResponder',false);
        }
        $this->set('mensagem',$mensagem);
        $this->set('usuario',$usuario);
        $this->render('_exibir');
    }

    function listar_usuarios() {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $usuario = $this->Session->read('Usuario');
        $formando = $this->ViewFormandos->read(null, $usuario['Usuario']['id']);
        $formandos = $this->ViewFormandos->find('all', array('conditions' => array('turma_id' => $formando['ViewFormandos']['turma_id'], 'NOT' => array('id' => ($formando['ViewFormando']['id'])))));
        echo json_encode(array('usuarios' => $formandos));
    }

    /* ================== PARTE SUPER ================== */

    function super_index() {
        $this->paginate = array(
            'limit' => 20,
            'conditions' => array(
                'Mensagem.usuario_id' => $this->Session->read('Usuario.Usuario.id'),
                'Mensagem.turma_id' => 12
            ),
            'contain' => array(
                'Assunto' => array('Item')
            ),
            'order' => array(
                'Mensagem.id' => 'desc'
            )
        );
        $this->set('mensagens', $this->paginate('Mensagem'));
    }

    function super_procurar() {
        $chave = empty($this->data['Mensagens']['chave']) ? "" : $this->data['Mensagens']['chave'];
        $chaves = explode(" ", $this->data['Mensagens']['chave']);

        function construirQuery($c) {
            return array('Mensagem.id' => $c);
        }

        $chaves = array_map('construirQuery', $chaves);

        $conditions = empty($chaves) ? array() : array('OR' => $chaves);

        $this->set('mensagens', $this->paginate('Mensagem', $conditions));

        $this->set('chave', $chave);
        $this->render('super_index');
    }

    function super_adicionar() {
        if (!empty($this->data)) {
            $this->Mensagem->create();

            if (!empty($this->data['Mensagem']['assunto'])) {
                $this->data['Mensagem']['assunto_id'] = $this->data['Mensagem']['assunto'];
            }
            $this->data['Mensagem']['turma_id'] = 12;
            $this->data['Mensagem']['usuario_id'] = $this->Session->read('Usuario.Usuario.id');
            $this->data['Mensagem']['data'] = date('Y-m-d H:i:s');

            $this->data['Mensagem']['texto'] = htmlspecialchars($this->data['Mensagem']['texto']);

            if ($this->Mensagem->save($this->data)) {
                $this->Session->setFlash(__('A mensagem foi salva com sucesso', true), 'flash_sucesso');

                $assunto = $this->Assunto->find('first', array('conditions' => array('Assunto.id' => $this->data['Mensagem']['assunto_id'])));
                $assunto['Assunto']['pendencia'] = $this->data['Mensagem']['pendencia'];
                $this->Assunto->save($assunto);

                $this->redirect('/super/mensagens');
            } else {
                $this->Session->setFlash(__('Ocorreu um erro ao salvar a mensagem.', true), 'flash_erro');
            }
        }
        $itens = array('' => '-');
        foreach ($this->Item->find('all') as $item) {
            $itens[$item['Item']['id']] = $item['Item']['nome'];
        }
        $this->set('pendencias', array('Rk' => 'RK', 'Comissao' => 'Comissão', 'Informativo' => 'Informativo'));
        $this->set('assuntos', array());
        $this->set('itens', $itens);
    }

    function super_responder($id = null) {
        if (!empty($this->data)) {
            $this->Mensagem->create();

            $this->data['Mensagem']['usuario_id'] = $this->Session->read('Usuario.Usuario.id');
            $this->data['Mensagem']['data'] = date('Y-m-d H:i:s');
            $this->data['Mensagem']['texto'] = htmlspecialchars($this->data['Mensagem']['texto']);
            if ($this->Mensagem->save($this->data)) {
                $this->Session->setFlash(__('A mensagem foi salva com sucesso', true), 'flash_sucesso');
                $this->redirect('/super/mensagens');
            } else {
                $this->Session->setFlash(__('Ocorreu um erro ao salvar a mensagem.', true), 'flash_erro');
            }
        }

        $this->set('mensagem', $this->Mensagem->find('first', array('conditions' => array('Mensagem.id' => $id))));
        $this->set('pendencias', array('Rk' => 'RK', 'Comissao' => 'Comissão', 'Informativo' => 'Informativo'));
    }

    function super_editar($id = null) {

        $this->Mensagem->id = $id;
        $this->Mensagem->recursive = 2;
        if (!empty($this->data)) {

            if ($this->Mensagem->save($this->data['Mensagem'])) {
                $this->Session->setFlash('A mensagem foi salva com sucesso', 'flash_sucesso');
                $this->redirect("/{$this->params['prefix']}/mensagens/visualizar/{$this->Mensagem->id}");
            }
        }
        else
            $this->data = $this->Mensagem->read();

        if (!$this->data)
            $this->Session->setFlash('Mensagem não existente', 'flash_erro');
    }

    function super_visualizar($id = null) {
        $this->set('mensagem', $this->Mensagem->find('first', array(
                    'conditions' => array('Mensagem.id' => $id),
                    'contain' => array(
                        'Assunto' => array('Item')
                    )
        )));
    }

    function super_deletar($id = null) {

//		if ($this->Universidade->delete($id))
//			$this->Session->setFlash('Deletado: universidade número ' . $id);
//		else
//			// TODO melhorar estas frases de erro
//			// Talvez colocá-las em um lugar unificado seja interessante
//			$this->Session->setFlash('Erro ao deletar, universidade selecionada é valida?');
//			
//		$this->redirect(array('action' => 'index'));
//		
    }

    /* ================== PARTE API ================== */

    function ajax_assuntos($id = null) {
        echo json_encode($this->Assunto->find('all', array('conditions' => array('Assunto.item_id' => $id))));
        Configure::write('debug', 0);
        $this->autoRender = false;
        exit();
    }

    /* ================== PARTE PRIVATE ================== */


    /* esta funcao deve ser chamada da action */

    private function adicionar_arquivos($id) {
        $result = true;
        $arquivos_ids = array();
        foreach ($this->data['Mensagem'] as $key => $value) {

            if (preg_match("/arquivo[\d]+/i", $key) && $result == true && !empty($value['name'])) {
                $arquivo_id = $this->gravar_arquivo($value, $this->data['Mensagem']['turma_id']);
                if (!empty($arquivo_id)) {
                    array_push($arquivos_ids, $arquivo_id);
                } else {
                    $result = false;
                    break;
                }
            }
        }

        if ($result == false) {
            $this->Mensagem->delete($this->Mensagem->id);
            foreach ($arquivos_ids as $id_arq) {
                $this->Arquivo->delete($id_arq);
            }
        }

        return $result;
    }

    private function gravar_arquivo($arquivoEnviado, $turma_id) {
        
        if (!$turma_id && !is_uploaded_file($arquivoEnviado['tmp_name']))
            return false;
        $usuario = $this->Session->read('Usuario');
        
        $arquivo = $this->Arquivo->create();

        $arquivo['Arquivo']['nome'] = $arquivoEnviado['name'];
        $arquivo['Arquivo']['tipo'] = $arquivoEnviado['type'];
        $arquivo['Arquivo']['tamanho'] = $arquivoEnviado['size'];
        $arquivo['Arquivo']['usuario_id'] = $usuario['Usuario']['id'];
        $arquivo['Arquivo']['tmp_name'] = $arquivoEnviado['tmp_name'];
        $arquivo['Arquivo']['turma_id'] = $turma_id;
        $arquivo['Arquivo']['formando_id'] = null;
        $arquivo['Mensagem'] = array('id' => $this->Mensagem->id);
        if ($this->Arquivo->save($arquivo)) {
            return $this->Arquivo->id;
        } else {
            debug($this->Arquivo->validationErrors);
            return null;
        }
    }
    
    private function obterUsuariosParaNotificacao($usuarioLogado,$turmaLogada,$grupo) {
        $this->Usuario->bindModel(array('hasOne' => array('TurmasUsuario')), false);
        $this->Usuario->unBindModel(array('hasMany' => array('Despesa'),
            'hasOne' => array('FormandoProfile')), false);
        $this->Usuario->recursive = 0;
        $options['conditions'] = array(
            'TurmasUsuario.turma_id' => $turmaLogada,
            'Usuario.grupo' => $grupo,
            'Usuario.ativo' => 1,
            "Usuario.id <> " => $usuarioLogado
        );
        return $this->Usuario->find('all',$options);
    }

    private function notificarUsuarios($mensagemId) {
        $notificacoes = Configure::read('notificacoes');
        if($notificacoes['dispositivos']) {
            $caminho = APP."../cake/console/cake.php mensagens enviarNotificacoes {$mensagemId}";
            $comando = "php {$caminho} > /dev/null 2>&1 &";
            exec($comando);
        }
        if($notificacoes['emails']) {
            $caminho = APP."../cake/console/cake.php mensagens enviarEmails {$mensagemId}";
            $comando = "php {$caminho} > /dev/null 2>&1 &";
            exec($comando);
        }
        return;
    }

    private function adicionarMensagemUsuario($usuarios,$usuarioLida) {
        $mensagem = $this->Mensagem->read(null, $this->Mensagem->getLastInsertId());
        if (!empty($mensagem)) {
            foreach ($usuarios as $usuario) {
                $save = array(
                    'usuario_id' => $usuario['Usuario']['id'],
                    'mensagem_id' => $mensagem['Mensagem']['id'],
                    'data_cadastro' => date('Y-m-d H:i:s'));
                if ($usuario['Usuario']['id'] == $usuarioLida)
                    $save['lida'] = 1;
                $this->MensagemUsuario->create();
                $this->MensagemUsuario->save($save);
            }
        }
    }

}

?>
