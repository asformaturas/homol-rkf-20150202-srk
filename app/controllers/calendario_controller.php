<?php

class CalendarioController extends AppController {
    
    var $name = 'Calendario';
    var $uses = array('Usuario','Evento','Atividade','AtividadeUsuario',
        'AtividadeTurma','AtividadeLocal','Turma','ViewFormandos');
    
    function inserir_fotografo(){
        $this->layout = false;
        if(!empty($this->data)){
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $this->Evento->id = $this->data['Evento']['id'];
            $json = 1;
            if($this->Evento->saveField('informacoes_fotografos', nl2br($this->data['Evento']['informacoes_fotografos'])))
                $json = 0;
            echo json_encode($json);
        }
    }
    
    function google($eid){
        $this->layout = false;
        if(!empty($eid)){
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $this->Evento->id = $eid;
            $json = 0;
            if($this->Evento->saveField('google', 1))
                $json = 1;
            echo json_encode(array('data' => $json));
        }
    }
    
    function exibir() {
        $this->layout = false;
        $usuario = $this->Session->read('Usuario');
        $this->set('podeFiltrar',!in_array($usuario['Usuario']['grupo'],array('comissao','formando','video','foto','marketing')));
        $this->set('podeAlocar',$usuario['Usuario']['nivel'] == 'administrador');
        $this->render('_exibir');
    }
    
    function listar($inicio,$fim) {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $usuario = $this->obterUsuarioLogado();
        $this->loadModel('TurmasUsuario');
        $turmas = $this->TurmasUsuario->find('list',array(
            'conditions' => array('TurmasUsuario.usuario_id' => $usuario['Usuario']['id']),
            'joins' => array(
                array(
                    'table' => 'turmas',
                    'foreignKey' => false,
                    'alias' => 'Turma',
                    'conditions' => array(
                        'Turma.id = TurmasUsuario.turma_id'
                    )
                )
            ),
            'fields' => array('Turma.id','Turma.id')
        ));
        $inicio = date('Y-m-d H:i:s',$inicio);
        $fim = date('Y-m-d H:i:s',$fim);
        $lista = array();
        if($usuario['Usuario']['grupo'] == 'video'){
            $this->listarEventos($usuario['Usuario']['id'],$inicio,$fim,$lista);
        }elseif($usuario['Usuario']['grupo'] == 'foto'){
            $this->listarEventos($usuario['Usuario']['id'],$inicio,$fim,$lista);
        }elseif($usuario['Usuario']['grupo'] == 'marketing'){
            $this->listarEventos($usuario['Usuario']['id'],$inicio,$fim,$lista);
        }else{
            $this->listarAtividades($usuario,$turmas,$inicio,$fim,$lista);
            $this->listarEventos($usuario['Usuario']['id'],$inicio,$fim,$lista);
        }
        echo json_encode(array('lista' => $lista));
    }
    
    function listarAtividades($uid,$turmas,$dataInicio,$dataFim,&$lista) {
        $atividades = $this->Atividade->listarAtividadesPorUsuario($uid,$turmas,$dataInicio,$dataFim);
        $turma = $this->obterTurmaLogada();
        $usuario = $this->obterUsuarioLogado();
            foreach($atividades as $atividade) {
                if($usuario['Usuario']['grupo'] == 'comissao'){
                    if($atividade['AtividadeTurma']['turma_id'] == $turma['Turma']['id']){
                        if(substr($atividade['Atividade']['tipo'],0,7) == 'reuniao') {
                            $background = "#357ebd";
                            $filtro = 'reuniao';
                        } elseif($atividade['Atividade']['tipo'] == 'evento_as') {
                            $background = "#4cae4c";
                            $filtro = 'interno';
                        } else {
                            $background = '#eea236';
                            $filtro = 'outros';
                        }
                        $lista[] = array(
                            'id' => "atividade_{$atividade['Atividade']['id']}",
                            'data_inicio' => strtotime($atividade['Atividade']['data_inicio']),
                            'data_fim' => strtotime($atividade['Atividade']['data_fim']),
                            'title' => $atividade['Atividade']['nome'],
                            'usuario_id' => $atividade['Atividade']['usuario_id'],
                            'usuario_grupo' => $atividade['Usuario']['grupo'],
                            'backgroundColor' => $background,
                            'filtro' => $filtro,
                            'allDay' => false
                        );
                    }   
                }else{
                    if(substr($atividade['Atividade']['tipo'],0,7) == 'reuniao') {
                        $background = "#357ebd";
                        $filtro = 'reuniao';
                    } elseif($atividade['Atividade']['tipo'] == 'evento_as') {
                        $background = "#4cae4c";
                        $filtro = 'interno';
                    } else {
                        $background = '#eea236';
                        $filtro = 'outros';
                    }
                    $lista[] = array(
                        'id' => "atividade_{$atividade['Atividade']['id']}",
                        'data_inicio' => strtotime($atividade['Atividade']['data_inicio']),
                        'data_fim' => strtotime($atividade['Atividade']['data_fim']),
                        'title' => $atividade['Atividade']['nome'],
                        'usuario_id' => $atividade['Atividade']['usuario_id'],
                        'usuario_grupo' => $atividade['Usuario']['grupo'],
                        'backgroundColor' => $background,
                        'filtro' => $filtro,
                        'allDay' => false
                    );
                }
            }  
    }
    
    function listarEventos($usuarioId,$dataInicio,$dataFim,&$lista) {
        $this->Evento->unbindModel(array('hasMany' => array('Extra')),false);
        $usuario = $this->obterUsuarioLogado();
        if(in_array($usuario['Usuario']['grupo'],array('video','foto','marketing')) || $usuario['Usuario']['nivel'] == 'administrador'){
            $conditions = array(
                'TurmasUsuario.turma_id = Evento.turma_id',
            );
        }else{
            $conditions = array(
                'TurmasUsuario.turma_id = Evento.turma_id',
                "TurmasUsuario.usuario_id = {$usuarioId}",
            );
        }
        $eventos = $this->Evento->find('all',array(
            'conditions' => array(
                "data >= '$dataInicio'",
                "data <= '$dataFim'",
            ),
            'joins' => array(
                array(
                    'table' => 'turmas_usuarios',
                    'alias' => 'TurmasUsuario',
                    'type' => 'inner',
                    'foreignKey' => false,
                    'conditions' => $conditions
                )
            ),
            'group' => array('Evento.id')
        ));
        foreach($eventos as $evento)
            $lista[] = array(
                'id' => "evento_{$evento['Evento']['id']}",
                'data_inicio' => strtotime($evento['Evento']['data']),
                'data_fim' => strtotime($evento['Evento']['data']),
                'title' => "{$evento['Evento']['turma_id']} - {$evento['TiposEvento']['nome']}",
                'usuario_id' => 0,
                'google' => $evento['Evento']['google'],
                'usuario_grupo' => 'planejamento',
                'backgroundColor' => '#d43f3a',
                'filtro' => 'evento',
                'allDay' => false
            );
    }
    
    function visualizar($tipo,$id) {
        $this->layout = false;
        $usuario = $this->Session->read('Usuario');
        if($tipo == 'atividade') {
            $this->Usuario->unbindModelAll();
            $this->Atividade->recursive = 2;
            $atividade = $this->Atividade->read(null,$id);
            $this->set('atividade',$atividade);
            $this->set('podeEditar',$atividade['Atividade']['usuario_id'] == $usuario['Usuario']['id']);
            $this->render('_atividade');
        } elseif($tipo == 'evento') {
            $evento = $this->Evento->read(null,$id);
            $formandos = $this->ViewFormandos->find('all', array(
                'conditions' => array(
                    'ViewFormandos.turma_id' => $evento['Evento']['turma_id']
                )
            ));
            $this->Usuario->bindModel(array('hasOne' => array('TurmasUsuario')), false);
            $planejamento = $this->Usuario->find('all',array(
                'conditions' => array(
                    'TurmasUsuario.turma_id' => $evento['Evento']['turma_id'],
                    'Usuario.grupo' => 'planejamento',
                    'Usuario.ativo' => 1
                )
            ));
            $this->set('formandos', count($formandos));
            $this->set('evento',$evento);
            $this->set('usuario',$usuario);
            $this->set('planejamento',$planejamento);
            $this->render('_evento');
        }
    }
    
    function editar($atividadeId = 0,$dataInicio = false) {
        $this->layout = false;
        $usuario = $this->Session->read('Usuario');
        if(!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $atividade = $this->data;
            $atividade['Atividade']['usuario_id'] = $usuario['Usuario']['id'];
            $atividade['Atividade']['data_inicio'] = $this->Atividade->formataDataHora($this->data['Atividade']['data_inicio']);
            if(!empty($this->data['Atividade']['data_fim']))
                $atividade['Atividade']['data_fim'] = $this->Atividade->formataDataHora($this->data['Atividade']['data_fim']);
            else
                $atividade['Atividade']['data_fim'] = null;
            if(in_array($usuario['Usuario']['grupo'],array('comissao','formando'))
                && empty($atividade['Atividade']['id'])) {
                $formando = $this->ViewFormandos->read(null,$usuario['Usuario']['id']);
                if($formando) {
                    $atividade['AtividadeTurma'][] = array(
                        'turma_id' => $formando['ViewFormandos']['turma_id']
                    );
                } else {
                    $this->Session->setFlash('Erro ao recuperar informações do formando', 'metro/flash/error');
                }
            }
            if($this->Atividade->saveAll($atividade)) {
                if(!empty($atividade['remover_turmas']))
                    $this->AtividadeTurma->deleteAll($atividade['remover_turmas']);
                if(empty($atividade['Atividade']['id']))
                    $this->Session->setFlash('Evento criado com sucesso', 'metro/flash/success');
                else
                    $this->Session->setFlash('Evento atualizado com sucesso', 'metro/flash/success');
            } else
                $this->Session->setFlash('Erro ao inserir evento', 'metro/flash/error');
            echo json_encode(array());
        } else {
            if(!in_array($usuario['Usuario']['grupo'],array('comissao','formando'))) {
                $this->loadModel('TurmasUsuario');
                $turmas = $this->TurmasUsuario->find('all',array(
                    'conditions' => array('usuario_id' => $usuario['Usuario']['id']),
                    'joins' => array(
                        array(
                            'table' => 'turmas',
                            'foreignKey' => false,
                            'alias' => 'Turma',
                            'conditions' => array(
                                'Turma.id = TurmasUsuario.turma_id'
                            )
                        )
                    ),
                    'fields' => array('Turma.*')
                ));
                $listaTurmas = array();
                foreach($turmas as $turma)
                    $listaTurmas[$turma['Turma']['id']] =
                        "{$turma['Turma']['id']} - {$turma['Turma']['nome']}";
                $this->set('turmas',$listaTurmas);
                $grupos = $this->Usuario->grupos;
                $grupos['funcionarios']['funcionarios'] = 'Todos';
                $this->set('grupos',$grupos);
                $this->set('tipo',$this->Atividade->tipo);
                $this->set('podeConvidar',true);
            } else {
                $this->set('podeConvidar',false);
                $this->set('tipo',array('outros' => 'Outros'));
            }
            $this->set('local',$this->Atividade->local);
            $atividade = $this->Atividade->read(null,$atividadeId);
            if($atividadeId != 0 && !$atividade)
                $this->Session->setFlash('Evento não encontrado', 'metro/flash/error');
            elseif($atividade) {
                $atividade['Atividade']['data_inicio'] = date('d/m/Y H:i',strtotime($atividade['Atividade']['data_inicio']));
                if(!empty($this->data['Atividade']['data_fim']))
                    $atividade['Atividade']['data_fim'] = date('d/m/Y H:i',strtotime($atividade['Atividade']['data_fim']));
                if(!in_array($atividade['Atividade']['local'], $this->Atividade->local) &&
                    !empty($atividade['Atividade']['local'])) {
                    $atividade['Atividade']['local-manual'] = $atividade['Atividade']['local'];
                    $atividade['Atividade']['local'] = 'outros';
                }
            }
            $this->data = $atividade;
            $this->set('atividade',$atividade);
            $this->render('_editar');
        }
    }
    
    function apagar() {
        $this->layout = false;
        Configure::write(array('debug' => 0));
        if(isset($this->data['id'])) {
            if($this->Atividade->delete($this->data['id']))
                $this->Session->setFlash('Evento apagado com sucesso', 'metro/flash/success');
            else
                $this->Session->setFlash('Erro ao apagar evento', 'metro/flash/error');
        }
        echo json_encode(array());
    }
    
    function alocar_salas($dia = false) {
        $this->layout = false;
        if(!empty($this->data)) {
            Configure::write(array('debug' => 0));
            $this->autoRender = false;
            $json = array('error' => 1);
            $this->Atividade->id = $this->data['atividade'];
            if($this->Atividade->saveField('local_id',$this->data['local']))
                $json['error'] = 0;
            echo json_encode($json);
        } else {
            if(!$dia) $dia = strtotime('now');
            $this->AtividadeUsuario->unbindModel(array('belongsTo' => array('Atividade')),false);
            $this->Usuario->unbindModelAll();
            $this->Atividade->recursive = 1;
            $dataInicio = date('Y-m-d 00:00:00',$dia);
            $dataFim = date('Y-m-d 23:59:59',$dia);
            $atividades = $this->Atividade->find('all',array(
                'conditions' => array(
                    "data_inicio >= '$dataInicio'",
                    "data_inicio <= '$dataFim'",
                )
            ));
            $this->AtividadeLocal->unbindModelAll();
            $locais = $this->AtividadeLocal->find('list',array(
                'fields' => array('AtividadeLocal.nome')
            ));
            $this->set('atividades',$atividades);
            $locais[0] = "Selecione";
            ksort($locais);
            $this->set('locais',$locais);
            $this->set('dia',$dia);
            $this->render('_alocar_salas');
        }
    }
    
    function imprimir_salas($dia) {
        $this->layout = 'metro/externo';
        $this->AtividadeUsuario->unbindModel(array('belongsTo' => array('Atividade')),false);
        $this->Usuario->unbindModelAll();
        $this->Atividade->recursive = 1;
        $dataInicio = date('Y-m-d 00:00:00',$dia);
        $dataFim = date('Y-m-d 23:59:59',$dia);
        $atividades = $this->Atividade->find('all',array(
            'conditions' => array(
                "data_inicio >= '$dataInicio'",
                "data_inicio <= '$dataFim'",
            ),
            'order' => array(
                'data_inicio' => 'asc'
            )
        ));
        $this->set('atividades',$atividades);
        $this->set('dia',$dia);
        $this->render('_imprimir_salas');
    }
    
}