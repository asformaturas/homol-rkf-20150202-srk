<?php

class ComissoesController extends AppController {

    var $components = array('Email');
    var $name = 'Comissoes';
    var $uses = array('EventoMapa', 'FormandoProfile', 'Usuario');
    // Padrão de paginação
    var $paginate = array(
        'limit' => 20,
        'order' => array(
            'Usuario.nome' => 'asc'
        )
    );

    private function _listar() {
        $turma = $this->obterTurmaLogada();
        $this->layout = false;
        $this->Usuario->bindModel(array(
            'hasOne' => array('TurmasUsuario')
                ), false);
        $cond = array(
            'TurmasUsuario.turma_id' => $turma['Turma']['id'],
            'Usuario.grupo' => 'comissao');
        $this->set('usuarios', $this->paginate('Usuario', $cond));
        $this->set('permissao', in_array($this->params['prefix'], array('planejamento', 'comercial')));
        $this->render('_listar');
    }

    private function _alterar_status($usuarioId, $ativo) {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $this->Usuario->id = $usuarioId;
        $usuario = $this->Usuario->read();
        if($usuario){
            if($this->Usuario->saveField('ativo',$ativo)){
                if($ativo == 0){
                    $this->Session->setFlash('Usuário alterado com sucesso.', 'metro/flash/success');
                }else{
                    $this->enviarEmailComissao($usuario);
                    $this->Session->setFlash('Usuário alterado com sucesso. Um email foi enviado para o 
                        usuário informando a liberação do acesso ao espaço da comissão.', 'metro/flash/alert');
                }
            }else{
                $this->Session->setFlash("Erro ao alterar usuário", 'metro/flash/error');
            }
        } else {
            $this->Session->setFlash("Usuário não encontrado", 'metro/flash/error');
        }
        echo json_encode(array());
    }

    function comercial_alterar_status($usuarioId, $ativo) {
        $this->_alterar_status($usuarioId, $ativo);
    }

    function planejamento_alterar_status($usuarioId, $ativo) {
        $this->_alterar_status($usuarioId, $ativo);
    }

    function planejamento_listar() {
        $this->_listar();
    }

    function comissao_listar() {
        $this->_listar();
    }

    function comercial_listar() {
        $this->_listar();
    }

    private function _exibir($usuarioId) {
        $this->layout = false;
        $this->FormandoProfile->contain(array('Usuario'));
        $usuario = $this->FormandoProfile->find(array('FormandoProfile.usuario_id' => $usuarioId));
        $this->set('usuario', $usuario);
        $this->render('_exibir');
    }

    function planejamento_alterar($id = null) {
        $this->layout = false;
        
        $this->loadModel('Parcelamento');
        
        $this->FormandoProfile->contain(array('Usuario'));
        $usuario = $this->FormandoProfile->find(array('FormandoProfile.usuario_id' => $id));
        $this->data = $usuario;
        
        /* Envia o valor da adesao, se cadastrada, aa view. */
        $valor = ($usuario['FormandoProfile']['valor_adesao_comissao']);
        $valor_comissao = ($valor == null ? 'N&atilde;o cadastrado' : 'R$' . $valor);
        $this->set('valor_adesao_comissao', $valor_comissao);
        /* Envia o numero de parcelas, se cadastrado, aa view. */
        $num = ($usuario['FormandoProfile']['max_parcelas_comissao']);
        $num_parcelas = ($num == null ? 'N&atilde;o cadastrado' : $num);
        $this->set('max_parcelas_comissao', $num_parcelas);
        $this->set('usuario', $usuario);
        
        $turma = $this->Session->read('turma');
        
        $parcelamentos = $this->Parcelamento->find('list', array(
                'fields' => 'Parcelamento.titulo',
                'conditions' => array(
                    'Parcelamento.turma_id' => $turma['Turma']['id']
                ),
                'group' => 'Parcelamento.titulo'
            )
        );

        $this->set('parcelamentos', $parcelamentos);

        $this->Usuario->setValidation('comissao_editar');
        $this->FormandoProfile->setValidation('comissao_editar');
    }

    function comissao_exibir($usuarioId) {
        $this->_exibir($usuarioId);
    }

    function comercial_exibir($usuarioId) {
        $this->_exibir($usuarioId);
    }

    function comercial_index() {
        $turma = $this->Session->read('turma');

        $this->Usuario->bindModel(array('hasOne' => array('TurmasUsuario')), false);

        $cond = array(
            'TurmasUsuario.turma_id' => $turma['Turma']['id'],
            'Usuario.grupo' => 'comissao');

        $this->set('formandos', $this->paginate('Usuario', $cond));
    }

    function planejamento_index() {
        $this->comercial_index();
    }

    function planejamento_editar($id = null) {
        $this->comercial_editar($id);
    }

    function planejamento_ativar($id = null) {
        $this->comercial_ativar($id);
    }

    function planejamento_desativar($id = null) {
        $this->comercial_desativar($id);
    }

    function planejamento_visualizar($id = null) {
        $this->comercial_visualizar($id);
    }

    function formando_index() {
        $this->comercial_index();
    }

    function formando_visualizar($id = null) {
        $this->comercial_visualizar($id);
    }

    function comissao_index() {
        $this->comercial_index();
    }

    function comissao_visualizar() {
        $usuario = $this->Session->read('Usuario');

        $id = $usuario['Usuario']['id'];

        $this->comercial_visualizar($id);
    }

    function comissao_editar() {

        $usuario = $this->Session->read('Usuario');

        $id = $usuario['Usuario']['id'];
        $this->Usuario->id = $id;

        $fp = $this->FormandoProfile->find('first', array('conditions' => array('usuario_id' => $id)));

        $this->FormandoProfile->id = $fp['FormandoProfile']['id'];


        if (!empty($this->data)) {
            $this->data['Usuario']['id'] = $id;
            $this->data['FormandoProfile']['id'] = $fp['FormandoProfile']['id'];

            $this->Usuario->setValidation('comissao_editar');
            $this->FormandoProfile->setValidation('comissao_editar');


            //if ($this->FormandoProfile->updateAll($array_dados,array('FormandoProfile.usuario_id' => $id))) {
            if ($this->Usuario->saveAll($this->data)) {
                $this->Session->setFlash('Dados atualizados com sucesso!', 'flash_sucesso');
                //$this->redirect( "/{$this->params['prefix']}/comissoes/visualizar");
            } else {
                $this->Session->setFlash("Ocorreu um erro ao salvar os dados do membro da comissão.", 'flash_erro');
                debug($this->FormandoProfile->validationErrors);
            }
        }
        if (!$this->data)
            $this->data = $this->Usuario->read();

        if (!$this->data)
            $this->Session->setFlash('Membro da Comissao não existente.', 'flash_erro');

        $this->set('usuarioEditandoEhDaComissao', true);
    }

    function comercial_adicionar() {

        if (!empty($this->data)) {
            $this->data['FormandoProfile']['data_entrada_comissao'] = date("Y-m-d");
            if ($this->FormandoProfile->updateAll(array('FormandoProfile.anotacoes_comissao' => '"' . $this->data["FormandoProfile"]["anotacoes_comissao"] . '"'), array("FormandoProfile.id" => $this->data["FormandoProfile"]["id"]))) {
                $this->Session->setFlash('Membro da Comissao foi salvo com sucesso.', 'flash_sucesso');
                $this->redirect("/{$this->params['prefix']}/comissoes/visualizar/" . $this->FormandoProfile->id);
            }
            else
                $this->Session->setFlash('Ocorreu um erro ao salvar o membro da comissao.', 'flash_erro');
        }
    }

    function comercial_editar($id = null) {
        $this->layout = false;
        if ($id == null && isset($this->data['FormandoProfile']['id'])) {
            $id = $this->data['FormandoProfile']['id'];
        }

        $this->FormandoProfile->recursive = 0;
        $formando = $this->FormandoProfile->find('first', array('conditions' => array('FormandoProfile.usuario_id' => $id)));

        $this->FormandoProfile->id = $formando['FormandoProfile']['id'];

        if (!empty($this->data) && $formando) {

            /* Trata o valor que veio da view, em caso de campo em branco. */
            $valor_adesao = $this->RealToDb($this->data['FormandoProfile']['valor_adesao_comissao']);
            $max_parcelas = $this->data['FormandoProfile']['max_parcelas_comissao'];

            if (empty($valor_adesao))
                $valor_adesao = "null";
            if (empty($max_parcelas))
                $max_parcelas = "null";

            if ($valor_adesao == 0) {
                $valor_adesao = "0.0";
            }

            if ($this->data) {
                $this->autoRender = false;
                Configure::write(array('debug' => 0));
                if ($this->FormandoProfile->updateAll(
                                array(
                            'FormandoProfile.anotacoes_comissao' => '"' . $this->data["FormandoProfile"]["anotacoes_comissao"] . '"',
                            'FormandoProfile.parcelamento_id' => $this->data['FormandoProfile']['parcelamento_id'],
                            'FormandoProfile.valor_adesao_comissao' => $valor_adesao,
                            'FormandoProfile.valor_adesao' => $valor_adesao,
                            'FormandoProfile.max_parcelas_comissao' => $max_parcelas
                                ), array("FormandoProfile.id" => $formando['FormandoProfile']['id'])
                        )) {
                    debug("Atualizou!");
                    $this->Session->setFlash('Dados atualizados com sucesso.', 'metro/flash/success');
                } else {
                    debug("Erro ao atualizar");
                    $this->Session->setFlash('Ocorreu um erro ao atualizar as informações do membro da comissão.', 'metro/flash/error');
                }
            }
            echo json_encode(array());
            exit();
        }

        if (!$this->data)
            $this->Session->setFlash('Membro da Comissao não existente.', 'flash_erro');
    }

    function comercial_visualizar($usuario_id = null) {

        if (!empty($this->data)) {
            $this->data['FormandoProfile']['data_entrada_comissao'] = date("Y-m-d");
            if ($this->FormandoProfile->save($this->data)) {
                $this->Session->setFlash('Membro da Comissao foi salvo com sucesso.', 'flash_sucesso');
                $this->redirect("/{$this->params['prefix']}/comissoes/visualizar/");
            } else {
                $this->Session->setFlash('Ocorreu um erro ao salvar o membro da comissao.', 'flash_erro');
            }
        } else {
            $this->FormandoProfile->contain(array('Usuario'));
            $formando = $this->FormandoProfile->find(array('FormandoProfile.usuario_id' => $usuario_id));

            if (!$formando)
                $this->Session->setFlash('Membro da Comissão não existente.', 'flash_erro');

            $this->set('formando', $formando);
            /* Envia o valor da adesao, se cadastrada, aa view. */
            $valor_comissao = ($formando['FormandoProfile']['valor_adesao_comissao']);
            $valor_comissao = ($valor_comissao == null ? 'N&atilde;o cadastrado' : 'R$' . $valor_comissao);
            $this->set('valor_adesao_comissao', $valor_comissao);
            /* Envia o numero de parcelas, se cadastrado, aa view. */
            $num_parcelas = ($formando['FormandoProfile']['max_parcelas_comissao']);
            $num_parcelas = ($num_parcelas == null ? 'N&atilde;o cadastrado' : $num_parcelas);
            $this->set('max_parcelas_comissao', $num_parcelas);
        }
    }

    function comercial_ativar($id = null) {
        $this->layout = false;
        if(!empty($this->data)){
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            if ($this->Usuario->updateAll(array("Usuario.ativo" => 1), array("Usuario.id" => $id, "Usuario.grupo" => "comissao"))){
                $this->Session->setFlash('Membro da Comissao ativado com sucesso', 'metro/flash/success');
            }else{
                $this->Session->setFlash('Ocorreu um erro ao ativar o membro da comissao.', 'metro/flash/error');
            }
        }
        echo json_encode(array());
    }

    function comercial_desativar($id = null) {
        if ($this->Usuario->updateAll(array("Usuario.ativo" => 0), array("Usuario.id" => $id, "Usuario.grupo" => "comissao")))
            $this->Session->setFlash('Membro da Comissao desativado com sucesso', 'flash_sucesso');
        else
            $this->Session->setFlash('Ocorreu um erro ao desativar o membro da comissao.', 'flash_erro');

        $this->redirect("/{$this->params['prefix']}/comissoes");
    }
    
    private function enviarEmailComissao($data) {
        $this->Email->smtpOptions = array(
            'port' => '465',
            'timeout' => '30',
            'host' => 'ssl://smtp.gmail.com',
            'username' => 'emailprojetoas',
            'password' => 'enviando123emails'
        );

        $this->Email->from = 'RK Formaturas - Sistema Online <notificacoesrk@rkformaturas.com.br>';
        $this->Email->to = $data['Usuario']['nome'] . ' - ' . '<' . $data['Usuario']['email'] . '>';
        $this->Email->subject = 'Ativação Comissão';
        $usuario = str_pad("Usuário", 10, ' ', STR_PAD_RIGHT);
        $senha = str_pad("Senha", 10, ' ', STR_PAD_RIGHT);
        $pass = sha1($data['Usuario']['senha']);
        debug($pass);
        $mensagem = "Seja bem vindo membro da comissão {$data['Usuario']['nome']}!!!\n";
        $mensagem.= "Seu usuário foi ativado com sucesso pelo responsável de sua turma.\n";
        $mensagem.= "No sistema você poderá entrar em contato com outros formandos e membros da comissão, ";
        $mensagem.= "tirar suas dúvidas, acompanhar o processo de planejamento e produção, etc.\n\n";
        $mensagem.= "Acesse o sistema no endereço " . Router::url('/', true) . " e comece a fazer parte da experiência RK Formaturas\n\n";
        $this->Email->delivery = 'smtp';
        $emailEnviado = $this->Email->send($mensagem);
    }

}

?>
