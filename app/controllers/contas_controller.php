<?php

class ContasController extends AppController {

    var $name = 'Contas';
    var $uses = array('BancoConta', 'Banco');
    var $paginate = array(
        'limit' => 55,
        'order' => array(
            'BancoConta.cedente' => 'asc'
        )
    );
    var $nomeDoTemplateSidebar = 'contas';

    function super_index() {
        $this->set('contas', $this->paginate('BancoConta'));
    }

    function super_editar($contaId = false) {
        if (!empty($this->data)) {
            if ($this->BancoConta->save($this->data)) {
                $this->Session->setFlash('Conta salva com sucesso', 'flash_sucesso');
                $this->redirect("/{$this->params['prefix']}/contas");
            } else
                $this->Session->setFlash('Ocorreu um erro ao salvar a conta', 'flash_erro');
        } elseif ($contaId) {
            $this->data = $this->BancoConta->read(null, $contaId);
            $this->set('conta', $this->data);
        } else {
            $this->set('conta', $contaId);
        }
        $bancos = $this->Banco->find('list', array(
            'fields' => array('nome')
        ));
        $this->set('bancos', $bancos);
    }
    
    function super_turmas() {
        $this->Turma->unbindModelAll();
        $options = array(
            'limit' => 100,
            'joins' => array(
                array(
                    'table' => 'banco_contas',
                    'alias' => 'BancoConta',
                    'type' => 'inner',
                    'foreignKey' => false,
                    'conditions' => array(
                        'Turma.banco_conta_id = BancoConta.id'
                    )
                ),
                array(
                    'table' => 'bancos',
                    'alias' => 'Banco',
                    'type' => 'inner',
                    'foreignKey' => false,
                    'conditions' => array(
                        'BancoConta.banco_id = Banco.id'
                    )
                )
            ),
            'fields' => array(
                'Turma.*',
                'BancoConta.*',
                'Banco.*',
            ),
            'order' => array('Turma.id' => 'desc')
        );
        $this->paginate['Turma'] = $options;
        $turmas = $this->paginate('Turma');
        $this->set('turmas', $turmas);
        $contas = $this->BancoConta->find('all', array(
            'recursive' => 1
        ));
        $lista = array();
        foreach($contas as $conta) {
            if(!isset($lista[$conta["Banco"]["nome"]]))
                $lista[$conta["Banco"]["nome"]] = array();
            $lista[$conta["Banco"]["nome"]][$conta["BancoConta"]["id"]] = "{$conta["BancoConta"]["agencia"]} - {$conta["BancoConta"]["conta"]}";
        }
        $this->set('contas', $lista);
    }
    
    function super_turmas_atualizar() {
        $this->Turma->id = $this->data['Turma']['id'];
        if($this->Turma->saveField('banco_conta_id',$this->data['Turma']['banco_conta_id']))
            $this->Session->setFlash('Turma atualizada com sucesso', 'flash_sucesso');
        else
            $this->Session->setFlash('Erro ao atualizar turma', 'flash_erro');
        $this->redirect("/{$this->params['prefix']}/contas/turmas");
    }
    
    function super_iniciar_turmas() {
        $this->loadModel('Turma');
        $conta = $this->BancoConta->find('first',array(
            'conditions' => array(
                'padrao' => 1
            )));
        if(!$conta) {
            $this->Session->setFlash('Nao existe uma conta padrao pra turmas', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/contas");
        }
        $columns = $this->Turma->getDataSource()->describe($this->Turma);
        if(!isset($columns['banco_conta_id'])) {
            try {
                $this->Turma->query("alter table turmas add banco_conta_id int(11) unsigned not null after multa_por_atraso;");
                $this->redirect("/{$this->params['prefix']}/contas/iniciar_turmas");
            } catch(Exception $e) {
                $this->Session->setFlash('Erro ao criar campo no MySQL', 'flash_erro');
                $this->redirect("/{$this->params['prefix']}/contas");
            }
        }
        $turmas = $this->Turma->find('count',array(
            'conditions' => array(
                "banco_conta_id" => 0
            )));
        if($turmas > 0) {
            try {
                $this->Turma->query("update turmas set banco_conta_id = {$conta['BancoConta']['id']};");
                $this->redirect("/{$this->params['prefix']}/contas/iniciar_turmas");
            } catch(Exception $e) {
                $this->Session->setFlash('Erro ao criar campo no MySQL', 'flash_erro');
                $this->redirect("/{$this->params['prefix']}/contas");
            }
        }
        
        if(!isset($columns['banco_conta_id']['key'])) {
            try {
                $this->Turma->query("ALTER TABLE turmas ADD CONSTRAINT `turmas_ibfk_2` FOREIGN KEY (`banco_conta_id`) " .
                        "REFERENCES `banco_contas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;");
                $this->Session->setFlash('Turmas atualizadas com sucesso', 'flash_sucesso');
                $this->redirect("/{$this->params['prefix']}/contas");
            } catch(Exception $e) {
                $this->Session->setFlash('Erro ao criar relacionamento no MySQL', 'flash_erro');
                $this->redirect("/{$this->params['prefix']}/contas");
            }
        }
    }
    
    function super_modelo_boleto($contaId) {
        $conta = $this->BancoConta->read(null, $contaId);
        if ($conta) {
            $this->autoRender = false;
            App::import('Component', 'BoletoConta');
            $boleto = new BoletoContaComponent();
            $boleto->dadosboleto = array_merge($boleto->dadosboleto,$conta['BancoConta']);
            $boleto->dadosboleto['codigoFormando'] = '0000000';
            $boleto->dadosboleto['turmaId'] = 1;
            $boleto->dadosboleto['usuarioId'] = 1;
            $boleto->dadosboleto['ordemBoleto'] = 1;
            $boleto->dadosboleto["ponto_venda"] = $conta['BancoConta']['agencia'];
            $boleto->dadosboleto["codigo_banco"] = $conta['Banco']['codigo'];
            $boleto->dadosboleto["carteira_descricao"] = strtoupper($conta['BancoConta']['carteira_descricao']);
            $boleto->dadosboleto["identificacao"] = $conta['BancoConta']['cedente'];
            $cpfCnpj = strlen($conta['BancoConta']['cpf_cnpj']) == 11 ? 'CPF' : 'CNPJ';
            $boleto->dadosboleto["cpf_cnpj"] = $cpfCnpj.": ".$boleto->formatCPFCNPJ($conta['BancoConta']['cpf_cnpj']);
            $boleto->dadosboleto["endereco"] = $conta['BancoConta']['endereco'];
            $boleto->dadosboleto["cidade_uf"] = "{$conta['BancoConta']['cidade']}/{$conta['BancoConta']['uf']}";
            $boleto->dadosboleto["data_documento"] = date("d/m/Y");
            $boleto->dadosboleto["data_processamento"] = date("d/m/Y");
            $boleto->dadosboleto["data_vencimento"] = date('d/m/Y');
            $boleto->dadosboleto["valor_boleto"] = number_format(0, 2, ',', '.');
            $boleto->dadosboleto["sacado"] = 'Sacado da Silva';
            $boleto->dadosboleto["instrucoes2"] = "AP&Oacute;S O VENCIMENTO APLICAR MULTA DE 10% SOBRE O VALOR. INCLUIR JUROS/MORA de R$ 0,10 AO DIA.";
            $boleto->dadosboleto["demonstrativo2"] = "AP&Oacute;S O VENCIMENTO APLICAR MULTA DE 10% SOBRE O VALOR. INCLUIR JUROS/MORA de R$ 0,10 AO DIA.";
            $boleto->gerarBoleto($conta['Banco']['nome_configuracao'],$this->webroot);
        } else {
            $this->Session->setFlash('Conta nao encontrada', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/contas");
        }
    }

}
