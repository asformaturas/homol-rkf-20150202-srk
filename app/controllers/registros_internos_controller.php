<?php

class RegistrosInternosController extends AppController {

    var $name = 'RegistrosInternos';
    var $uses = array('Turma', 'RegistrosContato', 'FormandoProfile');
    var $nomeDoTemplateSidebar = 'turmas';
    var $paginate = array(
        'limit' => 20,
        'order' => array(
            'RegistrosContato.data' => 'desc'
        )
    );

    function beforeFilter() {
        parent::beforeFilter();
        if(!($this->Session->read('Usuario')))
            $this->redirect("/usuarios/login");
    }
    
    function comercial_observacoes_festa() {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('erro' => 1);
        if(!empty($this->data['festa'])) {
            $festa = $this->RegistrosContato->read(null,$this->data['festa']);
            if($festa) {
                if(!empty($festa['RegistrosContato']['observacoes']))
                    $observacoes = json_decode($festa['RegistrosContato']['observacoes']);
                else
                    $observacoes = new stdClass();
                $observacoes->{$this->data['turma']} = $this->data['obs'];
                $observacoes = (array) $observacoes;
                $festa['RegistrosContato']['observacoes'] = json_encode($observacoes);
                if($this->RegistrosContato->save($festa))
                    $json['erro'] = 0;
            }
        }
        echo json_encode($json);
    }
    
    function comercial_incluir_previsao() {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('erro' => 1);
        $usuario = $this->Session->read('Usuario');
        $turma = $this->obterTurmaLogada();
        if(isset($this->data['data']) && $usuario) {
            $data = implode('-',array_reverse(explode('/',$this->data['data'])));
            if($data == date('Y-m-d', strtotime($data))) {
                $this->loadModel('TurmaPrevisaoFechamento');
                $cadastro = date('Y-m-d H:i:s');
                $dados = array(
                    'turma_id' => $turma['Turma']['id'],
                    'usuario_id' => $usuario['Usuario']['id'],
                    'data_previsao' => $data,
                    'data_cadastro' => $cadastro
                );
                if($this->TurmaPrevisaoFechamento->save($dados)) {
                    $json = array(
                        'erro' => 0,
                        'cadastro' => date('d/m/Y à\s H:i:s',strtotime($cadastro)),
                        'usuario' => $usuario['Usuario']['nome']
                    );
                } else {
                    $json['mensagem'] = "Erro ao inserir data";
                }
            } else {
                $json['mensagem'] = 'Data inválida';
            }
        } else {
            $json['mensagem'] = 'Erro ao recuperar data';
        }
        echo json_encode($json);
    }
    
    private function _editar() {
        $this->layout = false;
        $usuario = $this->Session->read('Usuario');
        $turma = $this->obterTurmaLogada();
        $this->Turma->id = $turma['Turma']['id'];
        if(!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            if($this->Turma->save($this->data)) {
                $this->setarTurmaLogada($turma['Turma']['id']);
                $this->Session->setFlash("Registro Foi Salvo Com Sucesso", 'metro/flash/success');
            } else
                $this->Session->setFlash("Erro ao Salvar Registro", 'metro/flash/error');
            echo json_encode(array());
        } else {
            $this->data = $this->Turma->read();
            $this->set('expectativa_fechamentos', $this->Turma->expectativa_fechamento);
            $this->render('_editar');
        }
    }
    
    function comercial_editar() {
        $this->_editar();
    }
    
    private function _listar() {
        $this->layout = false;
        $usuario = $this->Session->read('Usuario');
        $turma = $this->obterTurmaLogada();
        if($usuario['Usuario']['grupo'] == 'comercial') {
            $this->Turma->unbindModelAll();
            $this->Turma->bindModel(array(
                'hasMany' => array('TurmaPrevisaoFechamento')
            ), false);
            $this->Turma->recursive = 2;
        }
        $turma = $this->Turma->read(null,$turma['Turma']['id']);
        $this->loadModel('ViewFormandos');
        $festas = $this->ViewFormandos->find('all',array(
            'conditions' => array(
                'ViewFormandos.turma_id' => $turma['Turma']['id']
            ),
            'joins' => array(
                array(
                    'table' => 'registros_contatos_usuarios',
                    'type' => 'inner',
                    'alias' => 'RegistroContatoUsuario',
                    'foreignKey' => false,
                    'conditions' => array(
                        "RegistroContatoUsuario.usuario_id = ViewFormandos.id"
                    )
                ),
                array(
                    'table' => 'registros_contatos',
                    'alias' => 'RegistrosContato',
                    'type' => 'inner',
                    'foreignKey' => false,
                    'conditions' => array(
                        "RegistroContatoUsuario.registro_contato_id = RegistrosContato.id",
                        "RegistrosContato.tipo = 'festa'"
                    )
                )
            ),
            'fields' => array(
                'RegistrosContato.*',
                'count(RegistroContatoUsuario.id) as convidados',
                'sum(RegistroContatoUsuario.compareceu) as compareceram'
            ),
            'group' => array('ViewFormandos.turma_id','registro_contato_id')
        ));
        if($usuario['Usuario']['nivel'] != 'administrador')
            $options['joins'] = array(
                array(
                    'table' => 'turmas_usuarios',
                    'alias' => 'TurmaUsuario',
                    'foreignKey' => false,
                    'conditions' => array(
                        "TurmaUsuario.turma_id = RegistrosContato.turma_id",
                        "TurmaUsuario.usuario_id = {$usuario['Usuario']['id']}"
                    )
                )
            );
        $options['conditions'] = array(
            'RegistrosContato.turma_id' => $turma['Turma']['id']
        );
        $this->RegistrosContato->unbindModel(array(
            'belongsTo' => array('Turma')
        ),false);
        $this->RegistrosContato->recursive = 2;
        $this->Usuario->unbindModelAll();
        $this->paginate['RegistrosContato'] = $options;
        $reunioes = $this->RegistrosContato->find('count', array(
            'conditions' => array(
                'tipo' => 'reuniao',
                'turma_id' => $turma['Turma']['id']
        )));
        $ligacoes = $this->RegistrosContato->find('count', array(
            'conditions' => array(
                'tipo' => 'telefone',
                'turma_id' => $turma['Turma']['id']
        )));
        $registros = $this->paginate('RegistrosContato');
        $this->set('registros', $registros);
        $this->set('reunioes', $reunioes);
        $this->set('ligacoes', $ligacoes);
        $this->set('turma',$turma);
        $this->set('festas',$festas);
        $this->render('_listar');
    }
    
    function comercial_listar() {
        $this->_listar();
    }

    function comercial_index() {
        $session_turma = $this->Session->read('turma');
        $id = $session_turma['Turma']['id'];

        $turma = $this->Turma->find(array('id' => $id));

        if (!$turma) {
            $this->Session->setFlash('Turma não existente');
        }

        $this->set('turma', $turma);



        $this->paginate = array(
            'limit' => 20,
            'conditions' => array(
                'RegistrosContato.usuario_id' => $this->Session->read('Usuario.Usuario.id'),
                'RegistrosContato.turma_id' => $id
            ),
            'order' => array(
                'RegistrosContato.data' => 'desc'
            )
        );

        $reunioes = $this->RegistrosContato->find('count', array(
            'conditions' => array(
                'tipo' => 'reuniao',
                'turma_id' => $id
        )));

        $ligacoes = $this->RegistrosContato->find('count', array(
            'conditions' => array(
                'tipo' => 'telefone',
                'turma_id' => $id
        )));

        //$this->set('registros',$this->RegistrosContato->find(array('turma_id' => $id)));
        $this->set('registros', $this->paginate('RegistrosContato'));
        $this->set('reunioes', $reunioes);
        $this->set('ligacoes', $ligacoes);
    }

}

?>
