<?php

class RegistrosContatosController extends AppController {

    var $name = 'RegistrosContatos';
    var $helpers = array('Calendario', 'Html', 'Excel');
    var $nomeDoTemplateSidebar = 'turmas';
    var $tipo_select = array('telefone' => 'Telefone', 'email' => 'E-mail', 'reuniao' => 'Reunião', 'msn' => 'MSN', 'outros' => 'Outros');

    function beforeFilter() {
        parent::beforeFilter();
    }
    
    function confirmar_presenca($registroContatoUsuarioId = false) {
        $this->layout = false;
        $this->loadModel('RegistroContatoUsuario');
        if(isset($this->data['id'])) {
            Configure::write(array('debug' => 0));
            $this->autoRender = false;
            $registro = $this->RegistroContatoUsuario->read(null,$this->data['id']);
            $registro['RegistroContatoUsuario']['data_confirmacao'] = date('Y-m-d H:i:s');
            $registro['RegistroContatoUsuario']['confirmou_presenca'] = $this->data['confirmacao'];
            if($this->RegistroContatoUsuario->save($registro))
                echo json_encode(array('erro' => 0));
            else
                echo json_encode(array('erro' => 1));
        } else {
            $registro = $this->RegistroContatoUsuario->read(null,$registroContatoUsuarioId);
            $convidados = $this->RegistroContatoUsuario->find('count',array(
                'conditions' => array(
                    'RegistroContatoUsuario.registro_contato_id' => $registro['RegistrosContato']['id'],
                    "RegistroContatoUsuario.usuario_id <> {$registro['Usuario']['id']}",
                    "RegistroContatoUsuario.data_confirmacao is not null",
                    'RegistroContatoUsuario.confirmou_presenca' => 1
                )
            ));
            $this->set('registro',$registro);
            $this->set('convidados',$convidados);
        }
    }
    
    function comercial_festa_status_turmas($festaId) {
        $this->layout = false;
        $this->loadModel('ViewFormandos');
        $festa = $this->RegistrosContato->read(null,$festaId);
        $ids = $this->ViewFormandos->find('all',array(
            'conditions' => array(
                'RegistrosContato.registro_contato_id' => $festaId
            ),
            'joins' => array(
                array(
                    'table' => 'registros_contatos_usuarios',
                    'alias' => 'RegistrosContato',
                    'foreignKey' => false,
                    'type' => 'inner',
                    'conditions' => array(
                        "RegistrosContato.usuario_id = ViewFormandos.id"
                    )
                ),
                array(
                    'table' => 'turmas',
                    'alias' => 'Turma',
                    'foreignKey' => false,
                    'type' => 'inner',
                    'conditions' => array(
                        "ViewFormandos.turma_id = Turma.id"
                    )
                )
            ),
            'group' => 'turma_id',
            'fields' => array(
                'Turma.*',
                'if(Turma.data_assinatura_contrato < RegistrosContato.data_cadastro,1,0) as fechamento_antes_festa',
                'count(0) as convidados',
                'sum(RegistrosContato.compareceu) as compareceram',
                'sum(RegistrosContato.confirmou_presenca) as confirmaram',
                "(select group_concat(distinct nome separator ', ') from usuarios u inner join " .
                    "turmas_usuarios tu on usuario_id = u.id where grupo = 'comercial' " .
                    "and ViewFormandos.turma_id = Turma.id and u.ativo = 1 and u.nivel = 'basico') as consultores"
            )
        ));
        $this->set('festa',$festa);
        $this->set('turmas',$ids);
    }
    
    function comercial_festa_convidados_excel($festaId) {
        $this->layout = false;
        $this->RegistrosContato->unbindModel(array(
            'belongsTo' => array('Turma')
        ),false);
        $this->RegistrosContato->recursive = 2;
        $this->Usuario->unbindModelAll();
        $festa = $this->RegistrosContato->read(null,$festaId);
        $this->set('festa',$festa);
    }
    
    function comercial_festa_lista_convidados($festaId) {
        $this->layout = 'metro/externo';
        $this->RegistrosContato->unbindModel(array(
            'belongsTo' => array('Turma')
        ),false);
        $this->RegistrosContato->recursive = 2;
        $this->Usuario->unbindModelAll();
        $festa = $this->RegistrosContato->read(null,$festaId);
        $this->set('festa',$festa);
    }
    
    function comercial_festas() {
        $this->layout = false;
        $usuario = $this->Session->read('Usuario');
        $turma = $this->obterTurmaLogada();
        $options['conditions'] = array(
            'RegistrosContato.tipo' => 'festa'
        );
        $options['order'] = array(
            'RegistrosContato.data desc'
        );
        $permissao = false;
        if($usuario['Usuario']['nivel'] == 'administrador')
            $permissao = true;
        $this->set('permissao',$permissao);
        $this->RegistrosContato->unbindModel(array(
            'belongsTo' => array('Turma')
        ),false);
        $this->RegistrosContato->recursive = 2;
        $this->Usuario->unbindModelAll();
        $this->paginate['RegistrosContato'] = $options;
        $registros = $this->paginate('RegistrosContato');
        $this->set('registros', $registros);
    }
    
    function comercial_festa($festaId = false) {
        $this->layout = false;
        $turma = $this->obterTurmaLogada();
        $usuario = $this->Session->read('Usuario');
        if(!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $this->data['RegistrosContato']['tipo'] = 'festa';
            $dataRegistro = str_replace('/','-',$this->data['RegistrosContato']['data-hora']);
            $dateTime = $this->create_date_time_from_format('d-m-Y',$dataRegistro);
            if($dateTime)
                $this->data['RegistrosContato']['data'] = date_format($dateTime,'Y-m-d H:i:s');
            if(empty($this->data['RegistrosContato']['id'])) {
                $this->RegistrosContato->create();
                $this->data['RegistrosContato']['usuario_id'] = $usuario['Usuario']['id'];
            }
            if($this->RegistrosContato->saveAll($this->data)) {
                if(isset($this->data['RegistrosContato']['id']) && isset($this->data['remover_usuarios'])) {
                    $this->RegistrosContato->query("delete from registros_contatos_usuarios where " .
                        "registro_contato_id = {$this->data['RegistrosContato']['id']} and " .
                        "usuario_id in(".implode(",",$this->data['remover_usuarios']).")");
                }
                if(isset($this->data['RegistrosContato']['id']))
                    $festaId = $this->data['RegistrosContato']['id'];
                else
                    $festaId = $this->RegistrosContato->getLastInsertId();
                if(!empty($this->data['RegistroContatoUsuario'])) {
                    foreach($this->data['RegistroContatoUsuario'] as $registroUsuario) {
                        $this->enviarEmailConviteFesta($festaId, $registroUsuario['usuario_id']);
                    }
                }
                $this->Session->setFlash("Registro de Festa Foi Salvo Com Sucesso", 'metro/flash/success');
            } else {
                $erros = array('Erro ao salvar dados');
                if(!empty($this->RegistrosContato->validationErrors))
                    $erros = array_merge($erros,array_values($this->RegistrosContato->validationErrors));
                $this->Session->setFlash($erros, 'metro/flash/error');
            }
            echo json_encode(array());
        } else {
            $turmasConvidadas = array();
            $listaTurmas = array();
            if($festaId) {
                $this->data = $this->RegistrosContato->read(null,$festaId);
                $this->data['RegistrosContato']['data-hora'] = date('d/m/Y',strtotime($this->data['RegistrosContato']['data']));
                $this->loadModel('ViewFormandos');
                $options['joins'] = array(
                    array(
                        'table' => 'registros_contatos_usuarios',
                        'alias' => 'RegistrosContato',
                        'foreignKey' => false,
                        'type' => 'inner',
                        'conditions' => array(
                            "RegistrosContato.usuario_id = ViewFormandos.id"
                        )
                    )
                );
                $options['conditions'] = array(
                    'RegistrosContato.registro_contato_id' => $festaId,
                );
                $options['group'] = array('ViewFormandos.turma_id');
                $options['fields'] = array('ViewFormandos.turma_id','count(0) as convidados');
                $turmas = $this->ViewFormandos->find('all',$options);
                foreach($turmas as $turma)
                    $turmasConvidadas[$turma['ViewFormandos']['turma_id']]['convidados'] = $turma[0]['convidados'];
                $idsTurmas = array_keys($turmasConvidadas);
                $this->Usuario->unbindModel(array('hasAndBelongsToMany' => array('Campanha')),false);
                $this->Usuario->recursive = 1;
                $turmasUsuario = $this->Usuario->read(null,$usuario['Usuario']['id']);
                $nova = array();
                foreach($turmasUsuario['Turma'] as $turma)
                    if(in_array($turma['id'], $idsTurmas))
                        $nova[$turma['id']] = array(
                            'nome' => $turma['nome'],
                            'convidados' => $turmasConvidadas[$turma['id']]['convidados']
                        );
                    else
                        $listaTurmas[$turma['id']] = "{$turma['id']} - {$turma['nome']}";
                $this->set('turmasConvidadas',$nova);
                $this->set('listaTurmas',$listaTurmas);
                $this->set('turmaLogada',$this->obterTurmaLogada());
            }
            $this->set('festaId',$festaId);
            $this->set('usuario',$usuario);
        }
    }
    
    private function enviarEmailConviteFesta($festaId,$usuarioId) {
        $festa = $this->RegistrosContato->read(null,$festaId);
        $usuario = $this->Usuario->read(null,$usuarioId);
        if($usuario && $festa) {
            App::import('Component', 'mail');
            $this->Mail = new Mail();
            $this->Mail->From = 'notificacoesrk@rkformaturas.com.br';
            $this->Mail->FromName = utf8_decode('RK Formaturas - Sistema Online');
            $this->Mail->AddAddress($usuario['Usuario']['email'],
                        $usuario['Usuario']['nome']);
            $this->Mail->Subject = 'Convite para festa';
            $local = str_pad("Local: ", 10, ' ', STR_PAD_RIGHT);
            $data = str_pad("Data: ", 10, ' ', STR_PAD_RIGHT);
            $urlSite = Configure::read('url_site');
            $mensagem = "{$usuario['Usuario']['nome']}, você convidado para uma festa!!!<br />";
            $mensagem.= "{$festa['RegistrosContato']['assunto']}<br /><br />";
            $mensagem.= "$local {$festa['RegistrosContato']['local']}<br />";
            $mensagem.= "$data " . date('d/m/Y',strtotime($festa['RegistrosContato']['data'])) . "<br />";
            $mensagem.= "Para confirmar presença na festa acesso o sistema online clicando no botão abaixo<br /><br />";
            $mensagem.= "<a style='background:#b91d1d;color:white;padding:5px;text-decoration:none' ".
                        "href='$urlSite/usuarios/entrar_deslogado/" .
                        base64_encode($usuario['Usuario']['id']) . "'" .
                        ">Acessar Sistema</a><br /><br /><br /><br />";
            $this->Mail->Body    = utf8_decode($mensagem);
            return $this->Mail->send();
        }
    }
    
    function comercial_festa_formandos($turmaId,$festaId = '') {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $this->loadModel('ViewFormandos');
        $options['joins'] = array(
            array(
                'table' => 'registros_contatos_usuarios',
                'alias' => 'RegistrosContato',
                'foreignKey' => false,
                'type' => 'left',
                'conditions' => array(
                    "RegistrosContato.registro_contato_id = '$festaId'",
                    "RegistrosContato.usuario_id = ViewFormandos.id"
                )
            )
        );
        $options['conditions'] = array(
            'ViewFormandos.turma_id' => $turmaId,
            'ViewFormandos.ativo' => 1,
            'ViewFormandos.grupo' => 'comissao'
        );
        $options['group'] = array('ViewFormandos.id');
        $options['fields'] = array('ViewFormandos.*','RegistrosContato.id','RegistrosContato.compareceu');
        $formandos = $this->ViewFormandos->find('all',$options);
        echo json_encode(array('formandos' => $formandos));
    }
    
    function comercial_festa_compareceu() {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $this->loadModel('RegistroContatoUsuario');
        $json = array('erro' => 1);
        $this->RegistroContatoUsuario->id = $this->data['id'];
        if($this->RegistroContatoUsuario->saveField('compareceu', $this->data['compareceu']))
            $json['error'] = 0;
        echo json_encode($json);
    }
    
    private function _apagar() {
        if(!empty($this->data)) {
            $this->layout = false;
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            if($this->RegistrosContato->delete($this->data['RegistrosContato']['id']))
                $this->Session->setFlash("Registro de Contato Apagado Com Sucesso", 'metro/flash/success');
            else
                $this->Session->setFlash("Erro ao Apagar Registro de Contato", 'metro/flash/error');
            echo json_encode(array());
        }
    }
    
    function comercial_apagar() {
        $this->_apagar();
    }
    
    private function _editar($contatoId = false) {
        $this->layout = false;
        $usuario = $this->obterUsuarioLogado();
        $turma = $this->obterTurmaLogada();
        if(!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $this->data['RegistrosContato']['turma_id'] = $turma['Turma']['id'];
            $this->data['RegistrosContato']['usuario_id'] = $usuario['Usuario']['id'];
            $dataRegistro = str_replace('/','-',$this->data['RegistrosContato']['data-hora']);
            $dateTime = $this->create_date_time_from_format('d-m-Y',$dataRegistro);
            if($dateTime)
                $this->data['RegistrosContato']['data'] = date_format($dateTime,'Y-m-d H:i:s');
            if(!isset($this->data['RegistrosContato']['id']))
                $this->RegistrosContato->create();
            if($this->RegistrosContato->saveAll($this->data)) {
                if(isset($this->data['RegistrosContato']['id']) && isset($this->data['participantesRemovidos'])) {
                    foreach($this->data['participantesRemovidos'] as $comissaoRemovido)
                        $this->RegistrosContato->query("delete from registros_contatos_usuarios where " .
                                "registro_contato_id = {$this->data['RegistrosContato']['id']} and " .
                                "usuario_id = {$comissaoRemovido}");
                }
                $this->Session->setFlash("Registro de Contato Foi Salvo Com Sucesso", 'metro/flash/success');
            } else
                $this->Session->setFlash("Erro ao Salvar Registro de Contato", 'metro/flash/error');
            echo json_encode(array());
        } else {
            $this->loadModel('ViewFormandos');
            $comissao = $this->ViewFormandos->find('list',array(
                'conditions' => array(
                    'turma_id' => $turma['Turma']['id'],
                    'grupo' => 'comissao',
                    'situacao' => 'ativo'
                ),
                'fields' => array('nome')
            ));
            $selecionados = array();
            if(!($comissao)) {
                $comissao = array('Turma sem comissao');
            }
            if($contatoId) {
                $this->RegistrosContato->id = $contatoId;
                $registro = $this->RegistrosContato->read();
                $registro['RegistrosContato']['data-hora'] = date('d/m/Y',strtotime($registro['RegistrosContato']['data']));
                $this->data = $registro;
                foreach($registro['RegistroContatoUsuario'] as $registroUsuario)
                    array_push($selecionados, $registroUsuario['usuario_id']);
            }
            $this->set('comissao',$comissao);
            $this->set('usuario',$usuario);
            $this->set('selecionados',$selecionados);
            $this->set('tipo_select', $this->tipo_select);
            $this->set('contatoId',$contatoId);
            $this->render('_editar');
        }
    }
    
    function comercial_editar($contatoId = false) {
        $this->_editar($contatoId);
    }
    
    private function _exibir($idContato) {
        $this->layout = false;
        $this->RegistrosContato->recursive = 2;
        $this->Usuario->unbindModelAll();
        $this->Turma->unbindModelAll();
        $this->RegistrosContato->id = $idContato;
        $registro = $this->RegistrosContato->read();
        $this->set('registro',$registro);
        $this->render('_exibir');
    }
    
    function comercial_exibir($idContato) {
        $this->_exibir($idContato);
    }

    function comercial_index() {
        
    }
    
    function comercial_timeline() {
        $this->layout = false;
        $turma = $this->obterTurmaLogada();
        $this->RegistrosContato->recursive = 0;
        $grafico = array();
        $options = array(
            'conditions' => array(
                'RegistrosContato.turma_id' => $turma['Turma']['id']
            ),
            'group' => array("date_format(RegistrosContato.data,'%Y-%m-%d')"),
            'fields' => array(
                'RegistrosContato.data',
                'count(0) as qtde',
                'UNIX_TIMESTAMP(RegistrosContato.data) as time'
            ),
            'order' => array('RegistrosContato.data' => 'asc')
        );
        foreach($this->tipo_select as $tipo => $nome) {
            $options['conditions']['RegistrosContato.tipo'] = $tipo;
            $registros = $this->RegistrosContato->find('all',$options);
            if($registros)
                $grafico[$tipo] = array('data' => $registros,'nome' => $nome);
        }
        $this->set('grafico',$grafico);
    }
    
    function comercial_estatisticas() {
        $this->layout = false;
        $turma = $this->obterTurmaLogada();
        $this->loadModel('ViewFormandos');
        $tipos = array();
        $this->tipo_select['todos'] = 'Todos';
        foreach($this->tipo_select as $tipo => $nome)
            $tipos[$tipo] = 0;
        $comissoes = $this->ViewFormandos->find('all',array(
            'conditions' => array(
                'ViewFormandos.turma_id' => $turma['Turma']['id'],
                'ViewFormandos.ativo' => 1,
                'ViewFormandos.grupo' => 'comissao'
            )
        ));
        $usuarios = array();
        foreach($comissoes as $comissao)
            $usuarios[$comissao['ViewFormandos']['id']] = array(
                'nome' => $comissao['ViewFormandos']['nome'],
                'data' => $tipos
            );
        $usuarios['sem_comissao'] = array(
            'nome' => 'Sem Comissao',
            'data' => $tipos
        );
        $registros = $this->RegistrosContato->find('all',array(
            'conditions' => array('RegistrosContato.turma_id' => $turma['Turma']['id'])
        ));
        foreach($registros as $registro) {
            if(count($registro['RegistroContatoUsuario']) > 0) {
                foreach($registro['RegistroContatoUsuario'] as $registroUsuario) {
                    $usuarios[$registroUsuario['usuario_id']]['data']['todos']++;
                    $usuarios[$registroUsuario['usuario_id']]['data'][$registro['RegistrosContato']['tipo']]++;
                }
            } else {
                $usuarios['sem_comissao']['data']['todos']++;
                $usuarios['sem_comissao']['data'][$registro['RegistrosContato']['tipo']]++;
            }
        }
        $this->set('usuarios',$usuarios);
        $this->set('tipos',$this->tipo_select);
    }

    function comercial_adicionar() {
        $usuario = $this->Session->read('Usuario');
        $turma = $this->ObterTurmaLogada();

        if (!empty($this->data)) {
            $dados = $this->data;
            if (!empty($dados['RegistrosContato']['data-hora'])) {
                $dateTime = $this->create_date_time_from_format('d-m-Y H:i', $dados['RegistrosContato']['data-hora']);
                $dados['RegistrosContato']['data'] = date_format($dateTime, 'Y-m-d H:i:s');
            }
            unset($dados['RegistrosContato']['data-hora']);
            $dados['RegistrosContato']['turma_id'] = $turma['Turma']['id'];
            $dados['RegistrosContato']['usuario_id'] = $usuario['Usuario']['id'];

            $this->RegistrosContato->create();
            if ($this->RegistrosContato->saveAll($dados)) {
                $this->Session->setFlash(__('O registro de contato foi adicionado corretamente.', true), 'flash_sucesso');
                $this->redirect("/{$this->params['prefix']}/RegistrosInternos");
            } else {
                $this->Session->setFlash(__('Ocorreu um erro ao salvar o registro de contato', true), 'flash_erro');
            }
        } else {
            $this->set('usuario_id', $usuario['Usuario']['id']);
        }

        $this->set('tipo_select', $this->tipo_select);
    }

    function comercial_editar_old($id = null) {
        $this->RegistrosContato->id = $id;
        $usuario = $this->Session->read('Usuario');
        $turma = $this->Session->read('turma');

        if (!empty($this->data)) {
            $dateTime = $this->create_date_time_from_format('d-m-Y H:i', $this->data['RegistrosContato']['data-hora']);

            //            $this->RegistrosContato->data = date_format($dateTime, 'Y-m-d H:i:s');
            //             unset($this->RegistrosContato['data-hora']);
            // $this->RegistrosContato->turma_id = $turma['Turma']['id'];
            //  			$this->RegistrosContato->usuario_id = $usuario['Usuario']['id'];
            $this->data['RegistrosContato']['tipo'] = utf8_decode($this->data['RegistrosContato']['tipo']);
            $this->data['RegistrosContato']['data'] = date_format($dateTime, 'Y-m-d H:i:s');
            unset($this->data['RegistrosContato']['data-hora']);
            $this->data['RegistrosContato']['turma_id'] = $turma['Turma']['id'];
            $this->data['RegistrosContato']['usuario_id'] = $usuario['Usuario']['id'];

            if ($this->RegistrosContato->save($this->data)) {
                $this->Session->setFlash('Registro de contato foi salvo corretamente.');
                $this->redirect("/{$this->params['prefix']}/RegistrosInternos");
            } else {
                $this->Session->setFlash('Um erro ocorreu na atualização de dados.');
            }
        } else {
            $this->data = $this->RegistrosContato->read();
            if (empty($this->data)) {
                $this->redirect("/{$this->params['prefix']}/RegistrosInternos");
                $this->Session->setFlash('Registro de contato não existente.');
            }

            $dateTime = $this->create_date_time_from_format('Y-m-d H:i:s', $this->data['RegistrosContato']['data']);
            $this->data['RegistrosContato']['data-hora'] = date_format($dateTime, 'd-m-Y H:i');
        }

        $this->set('tipo_select', $this->tipo_select);
    }

    function comercial_deletar($id = null) {

        if ($this->RegistrosContato->delete($id))
            $this->Session->setFlash('Registro de contato removido.');
        else
        // TODO melhorar estas frases de erro
        // Talvez colocá-las em um lugar unificado seja interessante
            $this->Session->setFlash('Erro ao remover registro de contato.');

        $this->redirect("/{$this->params['prefix']}/RegistrosInternos");
    }

    function comercial_visualizar($id = null) {
        $this->RegistrosContato->id = $id;

        $this->data = $this->RegistrosContato->read();
        if (empty($this->data)) {
            $this->redirect("/{$this->params['prefix']}");
            $this->Session->setFlash('Registro de contato não existente.');
        }

        $dateTime = $this->create_date_time_from_format('Y-m-d H:i:s', $this->data['RegistrosContato']['data']);
        $this->data['RegistrosContato']['data-hora'] = date_format($dateTime, 'd-m-Y H:i');

        $this->set('registro', $this->data);
    }

}

?>