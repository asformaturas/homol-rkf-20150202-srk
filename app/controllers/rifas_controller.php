<?php

class RifasController extends AppController {

    var $name = "Rifas";
    var $uses = array('Rifa', 'RifaTurma', 'Cupom', 'CupomApp');
    var $nomeDoTemplateSidebar = 'rifas';
    // Padrão de paginação
    var $paginate = array(
        'limit' => 20,
        'order' => array(
            'Rifa.id' => 'desc'
        )
    );

    function formando_cupons() {
        $this->autoLayout = false;
        $usuario = $this->Session->read('Usuario');
        $turma = $this->obterTurmaLogada();
        $numeroCuponsDoFormando = $this->Rifa->Cupom->find('count',
            array(
                'conditions' => array(
                    'usuario_id' => $usuario['Usuario']['id'],
                    'not' => array('status' => 'cancelado')
                )
            )
        );
        if ($numeroCuponsDoFormando > 0) {
            $this->paginate = array('limit' => 50, 'order' => array('Cupom.numero asc'));
            $conditions = array('usuario_id' => $usuario['Usuario']['id']);
            $cupons = $this->Paginate('Rifa.Cupom', $conditions);
            $rifa = $this->Rifa->findById($cupons[0]['Rifa']['id']);
            $datetimeSorteio = $this->create_date_time_from_format('Y-m-d', $rifa['Rifa']['data_sorteio']);
            $rifa['Rifa']['data_sorteio'] = date_format($datetimeSorteio, "d/m/Y");
            $this->set('rifa', $rifa);
            if ($cupons) {
                $cupons1 = array_slice($cupons, 0, ceil(sizeof($cupons) / 2), false);
                $cupons2 = array_slice($cupons, sizeof($cupons1), floor(sizeof($cupons) / 2), false);
            }
            $this->set('cupons1', $cupons1);
            $this->set('cupons2', $cupons2);
        } else {
            $rifas_ativas = $this->RifaTurma->find('count', array(
                'conditions' => array(
                    'turma_id' => $turma['Turma']['id'],
                    'ativo' => 1,
                    'Rifa.ativa' => 1,
                    'Rifa.data_sorteio >' =>
                    date('Y-m-d', strtotime('+1 day')
                    )
            )));
            $cupomApp = $this->CupomApp->find('first', array(
                'conditions' => array(
                    'usuario_id' => $usuario['Usuario']['id']
                )
            ));
            if($rifas_ativas == 0) {
                $this->Session->setFlash('A turma não possui nenhuma rifa ativa.', 'metro/flash/error');
            } else if ($rifas_ativas > 1) {
                $this->Session->setFlash('Erro na listagem de rifas da turma.', 'metro/flash/error');
            } else {
                $rifa = $this->RifaTurma->find('first', array('conditions' => array('turma_id' => $turma['Turma']['id'], 'ativo' => 1)));
                $this->set('rifa', $rifa);
                $this->set('cupomApp', $cupomApp);
                if(!$cupomApp && !$this->Session->check('cupons_gerados'))
                    $this->render('formando_solicitar');
                else {
                    if($this->Session->check('cupons_gerados'))
                        $this->Session->delete('cupons_gerados');
                    $this->render('formando_aguardar');
                }
            }
        }
    }
    
    function formando_solicitar_old() {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $usuario = $this->Session->read('Usuario');
        $turma = $this->obterTurmaLogada();
        if ($this->data['Termos']['aceito'] == 1) {
            $rifa = $this->RifaTurma->find('first', array('conditions' => array('turma_id' => $turma['Turma']['id'], 'ativo' => 1)));
            if ($rifa) {
                $resultado = $this->gerar_cupons($usuario['Usuario']['id'], $rifa);
                if(!$resultado) {
                    sleep(5);
                    $resultado = $this->gerar_cupons($usuario['Usuario']['id'], $rifa);
                }
                if(!$resultado)
                    $this->Session->setFlash('Ocorreu um erro ao gerar os cupons.', 'metro/flash/error');
                else
                    $this->Session->setFlash('Cupons gerados com sucesso!', 'metro/flash/success');
            } else {
                $this->Session->setFlash('Erro na listagem de rifas da turma.', 'metro/flash/error');
            }
        } else {
            $this->Session->setFlash('Para gerar os cupons é necessário aceitar os termos e condições da Rifa Online.', 'metro/flash/warning_error');
        }
        echo json_encode(array());
    }

    function formando_solicitar() {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $usuario = $this->Session->read('Usuario');
        $turma = $this->obterTurmaLogada();
        $processoIniciado = $this->processoIniciado($usuario['Usuario']['id']);
        if(isset($this->data['Termos']['aceito'])) {
            if($this->data['Termos']['aceito'] == 1 && !$processoIniciado) {
                $rifa = $this->RifaTurma->find('first', array(
                    'conditions' => array(
                        'turma_id' => $turma['Turma']['id'], 'ativo' => 1
                    )
                ));
                if($rifa) {
                    $this->iniciarCupons($usuario['Usuario']['id'], $rifa['Rifa']['id']);
                    $this->Session->setFlash("Cupons enviados com sucesso",'metro/flash/success');
                    $this->Session->write("cupons_gerados",true);
                } else {
                    $this->Session->setFlash('Erro na listagem de rifas da turma.', 'metro/flash/error');
                }
            } else if($processoIniciado) {
                $this->Session->setFlash('Os cupons estão sendo processados. Aguarde o final do processo', 'metro/flash/error');
            } else {
                $this->Session->setFlash('Para gerar os cupons é necessário aceitar os termos e condições da Rifa Online.', 'metro/flash/warning_error');
            }
        }
        echo json_encode(array());
    }

    private function processoIniciado($id) {
        $retorno = false;
        $processo = shell_exec("ps -eaf | grep 'gerarCupons {$id}'");
        if (strpos($processo, 'cake.php') !== FALSE)
            $retorno = true;
        return $retorno;
    }
    
    private function iniciarCupons($usuarioId,$rifaId) {
        $caminho = APP."../cake/console/cake.php rifas gerarCupons ";
        $comando = "php {$caminho} {$usuarioId} {$rifaId} > /dev/null 2>&1 &";
        exec($comando);
        return $comando;
    }

    function formando_index() {
        $usuario = $this->Session->read('Usuario');

        // Verificar se o formando já possui algum cupom gerado
        $numeroCuponsDoFormando = $this->Rifa->Cupom->find('count', array('conditions' => array('usuario_id' => $usuario['Usuario']['id'], 'not' => array('status' => 'cancelado'))));

        $turma = $this->obterTurmaLogada();
        if ($numeroCuponsDoFormando > 0) {

            // se o formando já gerou, listar os cupons e mostrar botão 'imprimir'
            $this->paginate = array('limit' => 50, 'order' => array('Cupom.numero asc'));
            $conditions = array('usuario_id' => $usuario['Usuario']['id']);
            $cupons = $this->Paginate('Rifa.Cupom', $conditions);

            $rifa = $this->Rifa->findById($cupons[0]['Rifa']['id']);
            $datetimeSorteio = $this->create_date_time_from_format('Y-m-d', $rifa['Rifa']['data_sorteio']);
            $rifa['Rifa']['data_sorteio'] = date_format($datetimeSorteio, "d/m/Y");

            $this->set('rifa', $rifa);

            if ($cupons) {
                $cupons1 = array_slice($cupons, 0, ceil(sizeof($cupons) / 2), false);
                $cupons2 = array_slice($cupons, sizeof($cupons1), floor(sizeof($cupons) / 2), false);
            }
            $this->set('cupons1', $cupons1);
            $this->set('cupons2', $cupons2);
            $this->render('formando_index');
        } else {
            // Verificar se a turma tem alguma rifa ativa
            $rifas_ativas = $this->RifaTurma->find('count', array('conditions' => array('turma_id' => $turma['Turma']['id'], 'ativo' => 1, 'Rifa.ativa' => 1, 'Rifa.data_sorteio >' => date('Y-m-d', strtotime('+1 day')))));

            if ($rifas_ativas == 0) {
                $this->Session->setFlash('A turma não possui nenhuma rifa ativa.', 'flash_erro');
                $this->redirect("/{$this->params['prefix']}/principal");
            } else if ($rifas_ativas > 1) {
                $this->Session->setFlash('Erro na listagem de rifas da turma.', 'flash_erro');
                $this->redirect("/{$this->params['prefix']}/principal");
            }

            $rifa = $this->RifaTurma->find('first', array('conditions' => array('turma_id' => $turma['Turma']['id'], 'ativo' => 1)));
            $datetimeSorteio = $this->create_date_time_from_format('Y-m-d', $rifa['Rifa']['data_sorteio']);
            $rifa['Rifa']['data_sorteio'] = date_format($datetimeSorteio, "d/m/Y");

            $this->set('rifa', $rifa);

            if (isset($this->data['Termos']['aceito'])) {
                if ($this->data['Termos']['aceito'] == false) {
                    $this->Session->setFlash('Para gerar os cupons é necessário aceitar os termos e condições da Rifa Online.', 'flash_erro');
                    $this->redirect("/{$this->params['prefix']}/rifas");
                } else {
                    $resultado = $this->gerar_cupons($usuario['Usuario']['id'], $rifa);
                    if (!$resultado) {
                        // Pode ter ocorrido deadlock. Tentar novamente
                        debug("Tentando novamente");
                        sleep(5);
                        $resultado = $this->gerar_cupons($usuario['Usuario']['id'], $rifa);
                        if (!$resultado) {
                            $this->Session->setFlash('Ocorreu um erro ao gerar os cupons.', 'flash_erro');
                        } else {
                            $this->Session->setFlash('Cupons gerados com sucesso!', 'flash_sucesso');
                            $this->redirect("/{$this->params['prefix']}/rifas");
                        }
                    } else {
                        $this->Session->setFlash('Cupons gerados com sucesso!', 'flash_sucesso');
                        $this->redirect("/{$this->params['prefix']}/rifas");
                    }
                }
            }
            // Se o formando ainda não gerou cupons, exibir tela de termos e botão 'gerar'
            $this->render('formando_termos');
        }
    }

    function formando_atualiza_nome() {

        $cupom_id = $this->data['cupom_id'];
        $nome = $this->data['nome'];

        $this->layout = 'ajax';
        $this->atualiza_nome($cupom_id, $nome);
        $this->render('atualiza_nome');
    }

    function atualiza_nome($cupom_id, $nome) {
        Configure::write('debug', 0);
        $this->layout = 'ajax';

        $usuario = $this->Session->read('Usuario');

        // Verificar se o cupom pertence ao usuário logado
        $this->Cupom->id = $cupom_id;

        if ($this->Cupom->read()) {

            // TODO: Verificar se o status é 'aberto'

            if ($this->Cupom->data['Cupom']['usuario_id'] == $usuario['Usuario']['id']) {
                if ($this->Cupom->data['Cupom']['status'] == 'aberto') {
                    $this->Cupom->set('nome_comprador', $nome);
                    $this->Cupom->set('status', 'vendido');

                    if ($this->Cupom->save()) {
                        $this->set('resposta', 'sucesso');
                    } else {
                        $this->set('resposta', 'erro');
                    }
                } else {
                    $this->set('resposta', 'vendido');
                }
            } else {
                $this->set('resposta', 'not_authorized');
            }
        } else {
            $this->set('resposta', 'not_found');
        }
    }

    function formando_imprimir() {

        Configure::write('debug', 0);

        $this->layout = 'impressao_talao';


        $usuario = $this->Session->read('Usuario');

        // Verificar se o formando já possui algum cupom gerado
        $contagem = $this->Rifa->Cupom->find('count', array('conditions' => array('usuario_id' => $usuario['Usuario']['id'], 'not' => array('status' => 'cancelado'))));


        if ($contagem > 0) {
            // se o formando já gerou, listar os cupons e mostrar botão 'imprimir'
            $conditions = array('usuario_id' => $usuario['Usuario']['id'], 'status' => 'aberto');
            $cupons = $this->Rifa->Cupom->find('all', array('conditions' => $conditions));

            $data_sorteio_formatada = $this->create_date_time_from_format('Y-m-d', $cupons[0]['Rifa']['data_sorteio']);
            $this->set('data_sorteio_formatada', date_format($data_sorteio_formatada, 'd/m/Y'));
            $this->set('cupons', $cupons);
        } else {
            $this->set('cupons', null);
        }
    }

    function formando_imprimir_cartela() {
        Configure::write('debug', 0);

        $this->layout = 'impressao_talao';

        $usuario = $this->Session->read('Usuario');

        // Verificar se o formando já possui algum cupom gerado
        $contagem = $this->Rifa->Cupom->find('count', array('conditions' => array('usuario_id' => $usuario['Usuario']['id'], 'not' => array('status' => 'cancelado'))));


        if ($contagem > 0) {
            // se o formando já gerou, listar os cupons e mostrar botão 'imprimir'
            $conditions = array('usuario_id' => $usuario['Usuario']['id']);
            $cupons = $this->Rifa->Cupom->find('all', array('conditions' => $conditions));
            $data_sorteio_formatada = $this->create_date_time_from_format('Y-m-d', $cupons[0]['Rifa']['data_sorteio']);
            $this->set('data_sorteio_formatada', date_format($data_sorteio_formatada, 'd/m/Y'));
            $this->set('cupons', $cupons);
        } else {
            $this->set('cupons', null);
        }
    }
    
    function super_relatorio() {
        $this->paginate = array('limit' => 100);
        $cupons = $this->paginate('Cupom');
        $rifas = $this->Rifa->find('list',array('fields' => array('Rifa.nome')));
        $this->set('cupons',$cupons);
        $this->set('rifas',$rifas);
    }
    
    function super_relatorio_excel($rifaId) {
        $this->layout = false;
        set_time_limit(0);
        ini_set("memory_limit", "2048M");
        $cupons = $this->Cupom->find('all',array('conditions' => array('rifa_id' => $rifaId)));
        $this->set('cupons',$cupons);
    }
    
    function super_listar(){
        $this->layout = false;
        $usuario = $this->Session->read('Usuario');
        if (!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
                $this->Session->write("filtros.{$usuario['Usuario']['grupo']}.rifas", $this->data['Rifa']);
        } else {
            $options['order'] = array('Rifa.data_criacao' => 'desc');
            $filtro = $this->Session->read("filtros.{$usuario['Usuario']['grupo']}.rifas");
            if($filtro) {
                $this->data['Rifa'] = $filtro;
                foreach ($filtro as $chave => $valor)
                    $options['conditions']['lower(Rifa.nome) LIKE '] = "%".strtolower($valor)."%";
            }
            $options['limit'] = 30;
            $this->paginate['Rifa'] = $options;
            $rifas = $this->paginate('Rifa');
            $this->set('rifas', $rifas);
            $this->render('super_listar');
        }
    }
    
    function super_alterar_status($rifaId = false,$ativo = false) {
        $this->layout = false;
        $this->autoRender = false;
       // Configure::write(array('debug' => 0));
        $this->Rifa->id = $rifaId;
        $rifa = $this->Rifa->read();
        if($rifa){
            if($this->Rifa->saveField('ativa',$ativo)){
                $this->Session->setFlash('Rifa alterada com sucesso.', 'metro/flash/success');
            }else{
                $this->Session->setFlash("Erro ao Ativar/Desativar rifa.",
                    'metro/flash/error');
            }
        } else {
            $this->Session->setFlash("Usuário não encontrado",
                    'metro/flash/error');
        }
        echo json_encode(array());
    }
    
    function super_inserir(){
        $this->layout = false;
        if (!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $dateTime = str_replace('/','-',$this->data['Rifa']['data']);
            $dateTime = $this->create_date_time_from_format('d-m-Y', $dateTime);
            $this->data['Rifa']['data_sorteio'] = date_format($dateTime, 'Y-m-d');
            $this->data['Rifa']['data_criacao'] = DboSource::expression('NOW()');
            $this->data['Rifa']['ativa'] = 1;
            unset($this->data['Rifa']['data']);
            if ($this->Rifa->save($this->data['Rifa'])) 
                $this->Session->setFlash('Dados salvos com sucesso', 'metro/flash/success');
            else
                $this->Session->setFlash('Ocorreu um erro ao salvar a rifa.', 'metro/flash/error');
        }
    }
    
    function super_turmas(){
        $this->layout = false;
        $usuario = $this->Session->read('Usuario');
        if (!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
                $this->Session->write("filtros.{$usuario['Usuario']['grupo']}.turmas", $this->data['RifaTurma']);
        } else {
            $options['order'] = array('RifaTurma.turma_id' => 'desc');
            $filtro = $this->Session->read("filtros.{$usuario['Usuario']['grupo']}.turmas");
            if($filtro) {
                $this->data['RifaTurma'] = $filtro;
                foreach ($filtro as $chave => $valor)
                    $options['conditions']['lower(RifaTurma.turma_id) LIKE '] = "%".strtolower($valor)."%";
            }
            $options['limit'] = 30;
            $this->paginate['RifaTurma'] = $options;
            $turmas = $this->paginate('RifaTurma');
            $this->set('turmas', $turmas);
            $this->render('super_turmas');
        }
    }
    
    function super_alterar_status_turmas($turmaId = false,$ativo = false) {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $rifaTurmaId = $this->RifaTurma->find('first', array('conditions' => array('RifaTurma.turma_id' => $turmaId)));
        debug($rifaTurmaId);
        $this->RifaTurma->id = $rifaTurmaId['RifaTurma']['id'];
        if($rifaTurmaId){
            if($this->RifaTurma->saveField('ativo', $ativo)){
                $this->Session->setFlash('Turma alterada com sucesso.', 'metro/flash/success');
            }else{
                $this->Session->setFlash("Erro ao Ativar/Desativar turma.",
                    'metro/flash/error');
            }
        } else {
            $this->Session->setFlash("Usuário não encontrado",
                    'metro/flash/error');
        }
        echo json_encode(array());
    }
    
    function super_relacionar(){
        $this->layout = false;
        if (!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            if ($this->RifaTurma->save($this->data['RifaTurma'])) 
                $this->Session->setFlash('Dados salvos com sucesso', 'metro/flash/success');
            else
                $this->Session->setFlash('Ocorreu um erro ao salvar o vínculo.', 'metro/flash/error');
        }else{
            $turmas = $this->RifaTurma->Turma->find('list', array(
                'conditions' => array('status' => array('fechada', 'aberta')),
                'order' => array('id desc'),
                'fields' => array('id', 'nome')
            ));

            $keys = array_keys($turmas);
            foreach ($keys as $key) {
                $turmas[$key] = $key . ' - ' . $turmas[$key];
            }

            $rifas = $this->Rifa->find('list', array(
                'conditions' => array('ativa' => 1),
                'order' => array('id desc'),
                'fields' => array('id', 'nome')
            ));

            $cupons = array(50 => 50, 100 => 100);
            $status = array('1' => 'Ativo', '0' =>'Inativo');
            $this->set('status', $status);
            $this->set('cupons', $cupons);
            $this->set('rifas', $rifas);
            $this->set('turmas', $turmas);
        }
    }

    function super_index() {


        $rifas = $this->paginate('Rifa');

        $this->set('rifas', $rifas);
    }

    function super_editar($id = null) {
        
    }

    function super_visualizar($id = null) {
        if ($id) {
            $this->Rifa->id = $id;
            $this->Rifa->read();
            $dtSorteio = $this->create_date_time_from_format('Y-m-d', $this->Rifa->data['Rifa']['data_sorteio']);
            $this->Rifa->data['Rifa']['data-sorteio'] = date_format($dtSorteio, 'd-m-Y');

            $this->set('rifa', $this->Rifa->data);
        } else {
            $this->Session->setFlash('Rifa não encontrada', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/rifas");
        }
    }

    function super_desativar($id = null) {
        if ($id) {
            if ($this->Rifa->updateAll(array("Rifa.ativa" => 0), array("Rifa.id" => $id)))
                $this->Session->setFlash('Rifa desativada com sucesso.', 'flash_sucesso');
            else
                $this->Session->setFlash('Erro ao desativar a rifa.', 'flash_erro');
        } else {
            $this->Session->setFlash('Rifa não encontrada', 'flash_erro');
        }
        $this->redirect("/{$this->params['prefix']}/rifas");
    }

    function super_ativar($id = null) {
        if ($id) {
            if ($this->Rifa->updateAll(array("Rifa.ativa" => 1), array("Rifa.id" => $id)))
                $this->Session->setFlash('Rifa ativada com sucesso.', 'flash_sucesso');
            else
                $this->Session->setFlash('Erro ao ativar a rifa.', 'flash_erro');
        } else {
            $this->Session->setFlash('Rifa não encontrada', 'flash_erro');
        }
        $this->redirect("/{$this->params['prefix']}/rifas");
    }

    function super_buscar_cupom($id = null) {
        
    }

    function super_vincular() {
        //$this->Rifa->RifaTurma->find('')
        $this->paginate = array(
            'limit' => 20,
            'order' => array(
                'RifaTurma.id' => 'desc'
            )
        );



        $this->set('rifasturmas', $this->paginate('RifaTurma'));
    }

    function super_adicionar() {
        if (!empty($this->data)) {
            $dateTime = $this->create_date_time_from_format("d-m-Y", $this->data['Rifa']['data-sorteio']);
            //unset($this->data['Rifa']['data-sorteio']);

            if ($dateTime)
                $this->data['Rifa']['data_sorteio'] = date_format($dateTime, 'Y-m-d');
            else
                $this->data['Rifa']['data_sorteio'] = null;

            $dados = $this->data;

            $dados['Rifa']['data_criacao'] = date('Y-m-d', strtotime('now'));
            $dados['Rifa']['ativa'] = 1;

            if ($this->Rifa->saveAll($dados)) {
                $this->Session->setFlash('Rifa criada com sucesso.', 'flash_sucesso');
                $this->redirect("/{$this->params['prefix']}/rifas");
            } else {
                $this->Session->setFlash('Ocorreu um erro ao criar a Rifa.', 'flash_erro');
            }
        }
    }

    /* Vinculação de Rifas e Turmas */

    function super_vincular_adicionar() {

        if (!empty($this->data)) {
            //debug($this->data);
            if ($this->RifaTurma->saveAll($this->data)) {
                $this->Session->setFlash('Rifa vinculada à turma com sucesso!', 'flash_sucesso');
                $this->redirect("/{$this->params['prefix']}/rifas/vincular");
            } else {
                $this->Session->setFlash('Ocorreu um erro ao vincular a Rifa à Turma.', 'flash_erro');
                debug($this->RifaTurma->validationErrors);
            }
        }

        $turmas = $this->RifaTurma->Turma->find('list', array(
            'conditions' => array('status' => array('fechada', 'aberta')),
            'order' => array('id desc'),
            'fields' => array('id', 'nome')
        ));

        $keys = array_keys($turmas);
        foreach ($keys as $key) {
            $turmas[$key] = $key . ' - ' . $turmas[$key];
        }

        $rifas = $this->Rifa->find('list', array(
            'conditions' => array('ativa' => 1),
            'order' => array('id desc'),
            'fields' => array('id', 'nome')
        ));

        $cupons = array(50 => 50, 100 => 100);
        $ativos = array(1 => 'Sim', 0 => 'Não');

        $this->set('ativos', $ativos);
        $this->set('cupons', $cupons);
        $this->set('rifas', $rifas);
        $this->set('turmas', $turmas);
    }

    function super_vincular_visualizar($id) {
        
    }

    function super_vincular_editar($id) {
        
    }

    function super_vincular_desativar($id) {
        $this->RifaTurma->id = $id;
        $this->RifaTurma->read();

        if ($this->RifaTurma->saveField('ativo', 0)) {
            $this->Session->setFlash('Rifa da turma desativada com sucesso!', 'flash_sucesso');
        } else {
            $this->Session->setFlash('Ocorreu um erro ao desativar a Rifa da Turma.', 'flash_erro');
            debug($this->RifaTurma->validationErrors);
        }
        $this->redirect("/{$this->params['prefix']}/rifas/vincular");
    }

    function super_vincular_ativar($id) {
        $this->RifaTurma->id = $id;
        $this->RifaTurma->read();

        if ($this->RifaTurma->saveField('ativo', 1)) {
            $this->Session->setFlash('Rifa da turma ativada com sucesso!', 'flash_sucesso');
        } else {
            $this->Session->setFlash('Ocorreu um erro ao ativar a Rifa da Turma.', 'flash_erro');
            debug($this->RifaTurma->validationErrors);
        }
        $this->redirect("/{$this->params['prefix']}/rifas/vincular");
    }

    private function gerar_cupons($usuario_id, $rifa) {
        $cont = 0;



        $num_cupons = $rifa['RifaTurma']['cupons'];
        $cupons_gerados = array();

        $exp_numero = DboSource::expression("( SELECT  IF(RIGHT(IFNULL(MAX(c.numero),0),5) >= " . str_pad($this->Rifa->numero_maximo, 5, '0', STR_PAD_LEFT) . ", MAX(c.numero) + 100000 - RIGHT(IFNULL(MAX(c.numero),0),5), IFNULL(MAX(c.numero),0)) FROM cupons c WHERE rifa_id = " . $rifa['Rifa']['id'] . ") + 1", false);

        for ($i = 1; $i <= $num_cupons; $i++) {
            $cupons_gerados[] = array(
                'Cupom' => array(
                    'numero' => $exp_numero,
                    'status' => 'aberto',
                    'usuario_id' => $usuario_id,
                    'rifa_id' => $rifa['Rifa']['id']
                )
            );
        }


        $dados = $cupons_gerados;

        if (@$this->Cupom->saveAll($dados)) {
            return true;
        } else {
            return false;
        }
    }

}

?>