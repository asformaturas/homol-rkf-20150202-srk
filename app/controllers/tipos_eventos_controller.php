<?php

/*****************************/
/* O TiposEventos controller é responsável por gerenciar os tipos de eventos cadastrados no banco de dados. */
/* Apenas o super usuário pode editar as informações referenciadas pelo model TiposEvento. */
/*****************************/

class TiposEventosController  extends AppController {
	var $name = 'TiposEventos';
	
	var $uses = array('TiposEvento');
	
	var $nomeDoTemplateSidebar = 'turmas';
	
	function super_index() {
		$this->set('tiposeventos', $this->paginate('TiposEvento'));
	}
	
	function super_adicionar() {
		if (!empty($this->data)) {
			if(!isset($this->data['TipoEvento']['template']))
				$this->data['TipoEvento']['template'] = 'evento';
			
			if ($this->TiposEvento->save($this->data)) {
				$this->Session->setFlash(__('O tipo de evento foi salvo com sucesso', true), 'flash_sucesso');
				$this->redirect("/{$this->params['prefix']}/tipos_eventos");
			} else {
				$this->Session->setFlash(__('Ocorreu um erro ao salvar o tipo de evento.', true), 'flash_erro');
			}
		}
		
		
		//Deixar 'Evento Geral' como default
		$this->data['TiposEvento']['template'] = 'evento';
		$this->set('templates', array('evento' => 'Evento Geral', 'festa' => 'Festa', 'colacao' => 'Colação'));
		
	}
	
	function super_editar($id = null) {
		
		$this->TiposEvento->id = $id;
		
		if (!empty($this->data)) {
			
			if ($this->TiposEvento->save($this->data['TiposEvento'])){
				$this->Session->setFlash('Tipo de evento foi salvo com sucesso.', 'flash_sucesso');
				$this->redirect("/{$this->params['prefix']}/tipos_eventos");
			}
			
		} else 
			$this->data = $this->TiposEvento->read();
			
		if(!$this->data)
			$this->Session->setFlash('Tipo de evento não existente.', 'flash_erro');
			
		$this->set('templates', array('evento' => 'Evento Geral', 'festa' => 'Festa', 'colacao' => 'Colação'));	
		
	}
	
}

?>