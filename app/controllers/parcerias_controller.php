<?php

class ParceriasController extends AppController {

    var $name = 'Parcerias';
    var $uses = array('Parceiro', "Categoria", 'ParceriaTurma',
        'Parceria', 'Turma', 'FotoParceria', 'FotoParceiro','ParceriaCategoria');
    var $helpers = array('Cores');
    var $caminho_raiz_fotos = 'parcerias/fotos';
    var $paginate = array(
        'limit' => 25,
        'order' => array(
            'Parceria.id' => 'desc'
        ),
        'contain' => array('Parceiro')
    );
    var $app = array('listar','categorias','categoria_parcerias',
        'get','novidades','voucher');
    
    function app_voucher() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('error' => true);
        if(isset($this->data['parceria_id']) && isset($this->data['usuario_id'])) {
            $this->loadModel('ParceriaUsuario');
            if($this->ParceriaUsuario->save($this->data))
                $json["error"] = false;
        }
        echo json_encode($json);
    }
    
    function app_get() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array();
        if(isset($this->data['parceria_id'])) {
            $this->Parceria->unbindModelAll();
            $this->Parceria->bindModel(array(
                "belongsTo" => array(
                    'Parceiro' => array(
                        "className" => "Parceiro",
                        'fields' => array(
                            "Parceiro.nome",
                            "Parceiro.logo",
                            "Parceiro.site",
                            "Parceiro.descricao"
                        )
                    )
                ),
                "hasMany" => array(
                    "FotoParceria" => array(
                        "className" => "FotoParceria",
                        "fields" => array(
                            "FotoParceria.nome"
                        )
                    )
                )
            ),false);
            $parceria = $this->Parceria->read(null,$this->data['parceria_id']);
            $json['parceria'] = $parceria;
        }
        echo json_encode($json);
    }
    
    function app_novidades() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array();
        if(isset($this->data['turma_id'])) {
            $this->Parceria->unbindModelAll();
            $this->Parceria->bindModel(array(
                "belongsTo" => array(
                    'Parceiro' => array(
                        "className" => "Parceiro",
                    )
                )
            ),false);
            $options = array(
                'conditions' => array(
                    "Parceria.ativa" => 1,
                    "Parceiro.ativo" => 1,
                    "Parceria.data_inicio < current_timestamp",
                    "coalesce(Parceria.data_fim,current_timestamp) >= current_timestamp",
                    "(Parceria.publica = 1 or ParceriaTurma.turma_id = {$this->data['turma_id']})"
                ),
                'fields' => array(
                    'Parceria.id',
                    'Parceria.titulo',
                    'Parceiro.logo'
                ),
                'order' => array("Parceria.id" => 'desc'),
                'group' => array('Parceria.id'),
                'limit' => "7",
                'joins' => array(
                    array(
                        "table" => "parcerias_turmas",
                        "type" => "left",
                        "alias" => "ParceriaTurma",
                        "conditions" => array(
                            "ParceriaTurma.parceria_id = Parceria.id",
                        )
                    ),
                    array(
                        "table" => "parcerias_categorias",
                        "type" => "inner",
                        "alias" => "ParceriaCategoria",
                        "conditions" => array(
                            "ParceriaCategoria.parceria_id = Parceria.id",
                        )
                    ),
                    array(
                        "table" => "categorias",
                        "type" => "inner",
                        "alias" => "Categoria",
                        "conditions" => array(
                            "ParceriaCategoria.categoria_id = Categoria.id",
                        )
                    )
                )
            );
            $parcerias = $this->Parceria->find("all",$options);
            $json['novidades'] = $parcerias;
        }
        echo json_encode($json);
    }
    
    function app_categorias() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array();
        if(isset($this->data['turma_id'])) {
            $this->Parceria->unbindModelAll();
            $this->Parceria->bindModel(array(
                "belongsTo" => array(
                    'Parceiro' => array(
                        "className" => "Parceiro",
                    )
                )
            ),false);
            $options = array(
                'conditions' => array(
                    "Parceria.ativa" => 1,
                    "Parceiro.ativo" => 1,
                    "Parceria.data_inicio < current_timestamp",
                    "coalesce(Parceria.data_fim,current_timestamp) >= current_timestamp",
                    "(Parceria.publica = 1 or ParceriaTurma.turma_id = {$this->data['turma_id']})"
                ),
                'fields' => array(
                    'Categoria.id',
                    'Categoria.nome',
                    'Categoria.ordem'
                ),
                'order' => array("Parceria.id" => 'desc'),
                'group' => array('Categoria.id'),
                'joins' => array(
                    array(
                        "table" => "parcerias_turmas",
                        "type" => "left",
                        "alias" => "ParceriaTurma",
                        "conditions" => array(
                            "ParceriaTurma.parceria_id = Parceria.id",
                        )
                    ),
                    array(
                        "table" => "parcerias_categorias",
                        "type" => "inner",
                        "alias" => "ParceriaCategoria",
                        "conditions" => array(
                            "ParceriaCategoria.parceria_id = Parceria.id",
                        )
                    ),
                    array(
                        "table" => "categorias",
                        "type" => "inner",
                        "alias" => "Categoria",
                        "conditions" => array(
                            "ParceriaCategoria.categoria_id = Categoria.id",
                        )
                    )
                )
            );
            $c = $this->Parceria->find("all",$options);
            $categorias = array();
            foreach($c as $categoria)
                $categorias[$categoria["Categoria"]["id"]] = $categoria["Categoria"];
            $json['categorias'] = $categorias;
        }
        echo json_encode($json);
    }
    
    function app_categoria_parcerias() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array();
        if(isset($this->data['turma_id']) && isset($this->data['categoria_id'])) {
            $this->Parceria->unbindModelAll();
            $this->Parceria->bindModel(array(
                "belongsTo" => array(
                    'Parceiro' => array(
                        "className" => "Parceiro",
                    )
                )
            ),false);
            $options = array(
                'conditions' => array(
                    "Parceria.ativa" => 1,
                    "Parceiro.ativo" => 1,
                    "Categoria.id" => $this->data['categoria_id'],
                    "Parceria.data_inicio < current_timestamp",
                    "coalesce(Parceria.data_fim,current_timestamp) >= current_timestamp",
                    "(Parceria.publica = 1 or ParceriaTurma.turma_id = {$this->data['turma_id']})"
                ),
                'fields' => array(
                    'Parceria.id',
                    'Parceria.titulo',
                    'Parceria.descricao',
                    'Parceiro.logo'
                ),
                'order' => array("Parceria.id" => 'desc'),
                'group' => array('Parceria.id'),
                'joins' => array(
                    array(
                        "table" => "parcerias_turmas",
                        "type" => "left",
                        "alias" => "ParceriaTurma",
                        "conditions" => array(
                            "ParceriaTurma.parceria_id = Parceria.id",
                        )
                    ),
                    array(
                        "table" => "parcerias_categorias",
                        "type" => "inner",
                        "alias" => "ParceriaCategoria",
                        "conditions" => array(
                            "ParceriaCategoria.parceria_id = Parceria.id",
                        )
                    ),
                    array(
                        "table" => "categorias",
                        "type" => "inner",
                        "alias" => "Categoria",
                        "conditions" => array(
                            "ParceriaCategoria.categoria_id = Categoria.id",
                        )
                    )
                )
            );
            $parcerias = $this->Parceria->find("all",$options);
            $json['parcerias'] = $parcerias;
        }
        echo json_encode($json);
    }
    
    function app_listar() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array();
        if(isset($this->data['turma_id'])) {
            $this->Parceria->unbindModelAll();
            $this->Parceria->bindModel(array(
                "belongsTo" => array(
                    'Parceiro' => array(
                        "className" => "Parceiro",
                    )
                )
            ),false);
            $options = array(
                'conditions' => array(
                    "Parceria.ativa" => 1,
                    "Parceiro.ativo" => 1,
                    "Parceria.data_inicio < current_timestamp",
                    "coalesce(Parceria.data_fim,current_timestamp) >= current_timestamp",
                    "(Parceria.publica = 1 or ParceriaTurma.turma_id = {$this->data['turma_id']})"
                ),
                'fields' => array(
                    'Categoria.id',
                    'Categoria.nome',
                    'Categoria.ordem'
                ),
                'order' => array("Parceria.id" => 'desc'),
                'group' => array('Categoria.id'),
                'joins' => array(
                    array(
                        "table" => "parcerias_turmas",
                        "type" => "left",
                        "alias" => "ParceriaTurma",
                        "conditions" => array(
                            "ParceriaTurma.parceria_id = Parceria.id",
                        )
                    ),
                    array(
                        "table" => "parcerias_categorias",
                        "type" => "inner",
                        "alias" => "ParceriaCategoria",
                        "conditions" => array(
                            "ParceriaCategoria.parceria_id = Parceria.id",
                        )
                    ),
                    array(
                        "table" => "categorias",
                        "type" => "inner",
                        "alias" => "Categoria",
                        "conditions" => array(
                            "ParceriaCategoria.categoria_id = Categoria.id",
                        )
                    )
                )
            );
            $c = $this->Parceria->find("all",$options);
            $categorias = array();
            foreach($c as $categoria)
                $categorias[$categoria["Categoria"]["id"]] = $categoria["Categoria"];
            $options['fields'] = array(
                'Parceria.id',
                'Parceria.titulo',
                'Parceria.descricao',
                'Categoria.id',
            );
            $options['group'] = array('Parceria.id,Categoria.id');
            $this->Parceria->bindModel(array(
                "belongsTo" => array(
                    'Parceiro' => array(
                        "className" => "Parceiro",
                        'fields' => array(
                            'Parceiro.logo',
                        )
                    )
                )
            ),false);
            $parcerias = $this->Parceria->find("all",$options);
            $json['categorias'] = $categorias;
            $json['parcerias'] = $parcerias;
        }
        echo json_encode($json);
    }
    
    function formando_show($parceria) {
        $this->layout = false;
        $parceria = $this->Parceria->read(null,$parceria);
        $this->set("parceria",$parceria);
    }
    
    function formando_categorias() {
        $this->layout = false;
        $formando = $this->obterUsuarioLogado();
        $categorias = $this->Categoria->find("all",array(
            'conditions' => array(
                "Categoria.ativa" => 1
            ),
            //'limit' => 1
        ));
        $this->Parceria->unbindModel(array("hasAndBelongsToMany" => array('Turma',"Categoria")),false);
        $options = array(
            'conditions' => array(
                "Parceria.ativa" => 1,
                "Parceiro.ativo" => 1,
                "Parceria.data_inicio < current_timestamp",
                "coalesce(Parceria.data_fim,current_timestamp) >= current_timestamp",
                "(Parceria.publica = 1 or ParceriaTurma.turma_id = {$formando["ViewFormandos"]["turma_id"]})"
            ),
            'order' => array("Parceria.id" => 'desc'),
            'limit' => "7",
            'group' => array('Parceria.id'),
            'joins' => array(
                array(
                    "table" => "parcerias_turmas",
                    "type" => "left",
                    "alias" => "ParceriaTurma",
                    "conditions" => array(
                        "ParceriaTurma.parceria_id = Parceria.id",
                    )
                ),
                array(
                    "table" => "parcerias_categorias",
                    "type" => "inner",
                    "alias" => "ParceriaCategoria",
                    "conditions" => array(
                        "ParceriaCategoria.parceria_id = Parceria.id",
                    )
                ),
            )
        );
                
        $novidades = $this->Parceria->find("all",$options);
        $this->set("novidades",$novidades);
        
        unset($options['limit']);
                
        $menu = array();
        
        foreach($categorias as $i => $categoria) {
            $options['conditions']['categoria_id'] = $categoria["Categoria"]["id"];
            $menu[] = $categoria["Categoria"];
            $categorias[$i]['parcerias'] = $this->Parceria->find("all",$options);
        }
        
        $this->set("categorias",$categorias);
        $this->set("menu",$menu);
    }
    
    function formando_categoria($categoriaId) {
        $this->layout = false;
        $formando = $this->obterUsuarioLogado();
        $this->Parceria->unbindModel(array("hasAndBelongsToMany" => array('Turma',"Categoria")),false);
        $categoria = $this->Categoria->read(null,$categoriaId);
        $categorias = $this->Categoria->find("all",array(
            'conditions' => array(
                "Categoria.ativa" => 1,
                "Categoria.id <> {$categoriaId}"
            )
        ));

        $menu = array();
        
        foreach($categorias as $c)
            $menu[] = $c["Categoria"];

        $this->set("menu",$menu);

        $options = array(
            'conditions' => array(
                "Parceria.ativa" => 1,
                "Parceiro.ativo" => 1,
                "Parceria.data_inicio < current_timestamp",
                "coalesce(Parceria.data_fim,current_timestamp) >= current_timestamp",
                "(Parceria.publica = 1 or ParceriaTurma.turma_id = {$formando["ViewFormandos"]["turma_id"]})"
            ),
            'order' => array("Parceria.id" => 'desc'),
            'group' => array('Parceria.id'),
            'joins' => array(
                array(
                    "table" => "parcerias_turmas",
                    "type" => "left",
                    "alias" => "ParceriaTurma",
                    "conditions" => array(
                        "ParceriaTurma.parceria_id = Parceria.id",
                    )
                ),
                array(
                    "table" => "parcerias_categorias",
                    "type" => "inner",
                    "alias" => "ParceriaCategoria",
                    "conditions" => array(
                        "ParceriaCategoria.parceria_id = Parceria.id",
                    )
                ),
            )
        );
        $options['conditions']['categoria_id'] = $categoriaId;
        $parcerias = $this->Parceria->find("all",$options);
        $this->set("categoria",$categoria);
        $this->set("parcerias",$parcerias);
    }
    
    function formando_parceria_fotos($parceriaId) {
        $this->layout = false;
        $fotos = $this->FotoParceria->find("all",array(
            "conditions" => array(
                "parceria_id" => $parceriaId
            )
        ));
        $this->set("fotos",$fotos);
    }
    
    function marketing_categorias() {
        if($this->data) {
            $this->autoRender = false;
            Configure::write("debug",0);
            try {
                if(isset($this->data["Categoria"])) {
                    foreach($this->data["Categoria"] as $categoria)
                        $this->Categoria->save($categoria);
                }
                if(isset($this->data["adicionar"])) {
                    foreach($this->data["adicionar"] as $adicionar) {
                        $this->Categoria->create();
                        $adicionar['data_cadastro'] = date("Y-m-d H:i:s");
                        $this->Categoria->save($adicionar);
                    }
                }
                if(isset($this->data["remover"])) {
                    foreach($this->data["remover"] as $categoria)
                        $this->Categoria->delete($categoria);
                }
                $this->Session->setFlash("Dados inseridos com sucesso", 'metro/flash/success');
            } catch (Exception $ex) {
                $this->Session->setFlash("Erro ao inserir dados", 'metro/flash/error');
            }
            echo json_encode(array());
        } else {
            $categorias = $this->Categoria->find("all");
            $this->set("categorias",$categorias);
        }
    }
    
    function marketing_listar() {
        if(isset($this->data["filtro"])) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $this->Session->write("filtros.marketing.parcerias",
                $this->data["filtro"]);
        } else {
            if($this->Session->check("filtros.marketing.parcerias")) {
                $filtro = strtolower($this->Session->read("filtros.marketing.parcerias"));
                $this->set('filtro', $this->Session->read("filtros.marketing.parcerias"));
                $this->paginate['Parceria'] = array(
                    "conditions" => array(
                        "lower(Parceria.titulo) like('%{$filtro}%') or lower(Parceiro.nome) like('%{$filtro}%')"
                    )
                );
            }
            $parcerias = $this->paginate('Parceria');
            $this->set('parcerias', $parcerias);
        }
    }
    
    function marketing_editar($id = false) {
        if($id) {
            $parceria = $this->Parceria->read(null,$id);
            if($parceria) {
                $parceria["Parceria"]['data_inicio'] = date('d/m/Y H:i',strtotime($parceria["Parceria"]['data_inicio']));
                if(!empty($parceria["Parceria"]["data_fim"]))
                    $parceria["Parceria"]['data_fim'] = date('d/m/Y H:i',strtotime($parceria["Parceria"]['data_fim']));
            }
        } else
            $parceria = false;
        if($this->data) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $data = $this->data;
            $this->loadModel("Arquivo");
            if(!empty($data["Parceria"]["data_inicio"])) {
                $dateTime = str_replace('/','-',$data["Parceria"]["data_inicio"]);
                $dateTime = $this->create_date_time_from_format('d-m-Y', $dateTime);
                $data["Parceria"]["data_inicio"] = date_format($dateTime, 'Y-m-d');
            } else {
                unset($data["Parceria"]["data_inicio"]);
            }
            if(!empty($data["Parceria"]["data_fim"])) {
                $dateTime = str_replace('/','-',$data["Parceria"]["data_fim"]);
                $dateTime = $this->create_date_time_from_format('d-m-Y', $dateTime);
                $data["Parceria"]["data_fim"] = date_format($dateTime, 'Y-m-d');
            } else {
                unset($data["Parceria"]["data_fim"]);
            }
            $data['ParceriaCategorias'] = $data['Parceria']['categorias'];
            unset($data['Parceria']['categorias']);
            if($this->Parceria->save($data['Parceria'])) {
                $parceriaId = !empty($data["Parceria"]["id"]) ? $data["Parceria"]["id"] : $this->Parceria->getLastInsertId();
                if(isset($data["fotos"])) {
                    $this->loadModel("FotoParceria");
                    $path = WWW_ROOT . "img/parcerias/fotos/{$parceriaId}/";
                    $c = true;
                    if(!is_dir($path))
                        $c = mkdir($path);
                    if($c)
                        foreach($data["fotos"] as $foto) {
                            $nome = String::uuid().$this->FotoParceria->obterExtensaoDoArquivo($foto["name"]);
                            if($this->Arquivo->saveBase64File($foto['data'], $path.$nome)) {
                                $this->FotoParceria->create();
                                $this->FotoParceria->save(array(
                                    "FotoParceria" => array(
                                        "parceria_id" => $parceriaId,
                                        "nome" => $nome
                                    )
                                ));
                            }
                        }
                }
                if(isset($data["turmas"])) {
                    foreach($data['turmas'] as $turma) {
                        $this->ParceriaTurma->create();
                        $this->ParceriaTurma->save(array(
                            "ParceriaTurma" => array(
                                "turma_id" => $turma,
                                "parceria_id" => $parceriaId
                            )
                        ));
                    }
                }
                if(isset($data["ParceriaCategorias"])) {
                    foreach($data as $categoria) {
                        $save = $this->ParceriaCategoria->get(array(
                            "categoria_id" => $categoria,
                            "parceria_id" => $parceriaId
                        ),array(
                            "categoria_id" => $categoria,
                                "parceria_id" => $parceriaId,
                                'data_cadastro' => date("Y-m-d H:i:s")
                        ));
                        if($save){
                            $this->ParceriaCategoria->save($save);
                        }
                    }
                }
                if(isset($data["remover_categorias"]))
                    foreach($data['remover_categorias'] as $categoria)
                        $this->ParceriaCategoria->delete($categoria);
                if(isset($data["remover_turmas"]))
                    foreach($data['remover_categorias'] as $turma)
                        $this->ParceriaTurma->delete($turma);
                if(isset($data["remover_fotos"]))
                    foreach($data['remover_fotos'] as $foto)
                        $this->FotoParceria->delete($foto);
                $this->Session->setFlash("Dados inseridos com sucesso", 'metro/flash/success');
            } else {
                $this->Session->setFlash("Erro ao enviar dados", 'metro/flash/error');
            }
            echo json_encode(array());
        } else {
            $parceiros = $this->Parceiro->find('list',array(
                "fields" => array('nome')
            ));
            $this->set("parceiros",$parceiros);
            $categorias = $this->Categoria->find("list",array(
                "conditions" => array(
                    "ativa" => 1
                ),
                "fields" => array("nome")
            ));
            $this->set("categorias",$categorias);
            $this->Turma->unbindModelAll();
            $turmas = $this->Turma->find("all",array(
                "conditions" => array(
                    "status" => 'fechada'
                )
            ));
            $t = array();
            foreach($turmas as $turma)
                $t[$turma["Turma"]["id"]] = "{$turma['Turma']['id']} - {$turma['Turma']['nome']}";
            $this->set("turmas",$t);
            $this->data = $parceria;
        }
        $this->set("parceria",$parceria);
    }

    function marketing_index() {
        $parcerias = $this->paginate('Parceria');
        $this->set('parcerias', $parcerias);
    }

    function marketing_visualizar($id = null) {
        $parceria = $this->Parceria->find(array('Parceria.id' => $id));

        App::Import('Controller', 'Parceiros');
        $parceirosController = new ParceirosController();
        $this->set('caminho_raiz_logos', $parceirosController->caminho_raiz_logos);
        $this->set('caminho_raiz_fotos_parceiro', $parceirosController->caminho_raiz_fotos);
        $this->set('caminho_raiz_fotos_parceria', $this->caminho_raiz_fotos);

        if (!$parceria) {
            $this->Session->setFlash('Parceria não existente.', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/parcerias");
        } else {
            $parceiro = $this->Parceiro->findById($parceria['Parceiro']['id']);
            $this->set('parceria', $parceria);
            $this->set('parceiro', $parceiro);
        }
    }

    function marketing_adicionar() {
        $parceiros = $this->Parceria->Parceiro->find('list', array(
            'fields' => array('Parceiro.id', 'Parceiro.nome'),
            'conditions' => array('Parceiro.ativo' => 1)));

        $this->set('parceiros', $parceiros);

        if (!empty($this->data)) {

            $inicio_dateTime = $this->create_date_time_from_format("d-m-Y", $this->data['Parceria']['data-inicio']);
            $fim_dateTime = null;
            if (!empty($this->data['Parceria']['data-fim']))
                $fim_dateTime = $this->create_date_time_from_format("d-m-Y", $this->data['Parceria']['data-fim']);

            $this->data['Parceria']['data_inicio'] = $inicio_dateTime != null ? date_format($inicio_dateTime, 'Y-m-d') : null;
            $this->data['Parceria']['data_fim'] = $fim_dateTime != null ? date_format($fim_dateTime, 'Y-m-d') : null;

            $parceria = $this->data;
            $this->Parceria->set($parceria);

            $id_arquivos = md5(date('YmdHisu'));
            $parceria['Parceria']['id_arquivos'] = $id_arquivos;

            $fotos_salvas = array();

            if ($this->Parceria->validates()) {

                $fotos_salvas = $this->uploadFiles('img/' . $this->caminho_raiz_fotos, $parceria['Fotos'], $id_arquivos);

                if (array_key_exists('urls', $fotos_salvas)) {
                    for ($i = 0; $i < count($fotos_salvas['urls']); $i++) {
                        $componentes_caminho_foto = explode('/', $fotos_salvas['urls'][$i]);
                        $pasta_arquivo = array_slice($componentes_caminho_foto, -2);
                        $caminho_final = implode('/', $pasta_arquivo);

                        $parceria['FotoParceria'][$i]['nome'] = $caminho_final;
                    }
                }
            }

            $msg_erro = "Ocorreu um erro ao salvar a parceria.";
            $erro_nos_arquivos = false;

            if (array_key_exists('errors', $fotos_salvas)) {
                $erro_nos_arquivos = true;
                $msg_erro = "Ocorreu um erro ao salvar as fotos do Parceiro.";
            }

            if ($this->Parceria->saveAll($parceria) && !$erro_nos_arquivos) {
                $this->Session->setFlash('A parceria foi salva corretamente.', 'flash_sucesso');
                $this->redirect("/{$this->params['prefix']}/parcerias/visualizar/{$this->Parceria->id}");
            }
            else
                $this->Session->setFlash($msg_erro, 'flash_erro');
        }
    }

    function marketing_ativar($id = null) {
        if ($id) {
            if ($this->Parceria->updateAll(array("Parceria.ativa" => 1), array("Parceria.id" => $id)))
                $this->Session->setFlash('Parceria ativada com sucesso.', 'flash_sucesso');
            else
                $this->Session->setFlash('Erro ao ativar parceria.', 'flash_erro');
        } else {
            $this->Session->setFlash('Parceria não encontrada', 'flash_erro');
        }
        $this->redirect("/{$this->params['prefix']}/parcerias");
    }

    function marketing_desativar($id = null) {
        if ($id) {
            if ($this->Parceria->updateAll(array("Parceria.ativa" => 0), array("Parceria.id" => $id)))
                $this->Session->setFlash('Parceria desativada com sucesso.', 'flash_sucesso');
            else
                $this->Session->setFlash('Erro ao desativar parceria.', 'flash_erro');
        } else {
            $this->Session->setFlash('Parceria não encontrada', 'flash_erro');
        }
        $this->redirect("/{$this->params['prefix']}/parcerias");
    }

    function marketing_apagar_foto($id = null) {
        Configure::write('debug', 0);
        $this->layout = 'ajax';

        $id = $this->data['id'];

        if ($id && $this->Parceria->FotoParceria->delete($id)) {
            $this->set('resposta', 'sucesso');
        } else {
            $this->set('resposta', 'erro');
        }

        $this->render('apagar_foto');
    }

    function marketing_vincular($idDaParceria) {

        $this->paginate = array(
            'limit' => 100,
            'order' => array(
                'Parceria.id' => 'desc'
            ),
            'conditions' => array('parceria_id' => $idDaParceria)
        );

        $this->Parceria->contain();
        $this->set('parceria', $this->Parceria->findById($idDaParceria));

        $this->set('listaDeTurmasDaParceria', $this->paginate('ParceriaTurma'));

        $this->enviarTurmasNaoAtreladasAParceria($idDaParceria);
    }

    private function enviarTurmasNaoAtreladasAParceria($idDaParceria) {
        $turmasAtreladasAParceria = $this->ParceriaTurma->find('list', array(
            'fields' => array('turma_id'),
            'conditions' => array('parceria_id' => $idDaParceria)
        ));

        $turmasNaoAtreladasAParceria = $this->ParceriaTurma->Turma->find('list', array(
            'order' => array('id desc'),
            'fields' => array('id', 'nome'),
            'conditions' => array('not' => array('id' => $turmasAtreladasAParceria))
        ));

        foreach ($turmasNaoAtreladasAParceria as $codigoTurma => $nomeTurma) {
            $turmasNaoAtreladasAParceria[$codigoTurma] = $codigoTurma . " - " . $nomeTurma;
        }

        $this->set('turmasNaoAtreladasAParceria', $turmasNaoAtreladasAParceria);
    }

    function marketing_vincular_remover($idDaParceriaTurma) {
        $status = "erro";

        if ($idDaParceriaTurma) {
            if ($this->ParceriaTurma->delete($idDaParceriaTurma))
                $status = "sucesso";
        }

        $this->layout = false;
        $this->autoRender = false;
        $this->RequestHandler->respondAs('text');
        Configure::write('debug', 0);
        echo $status;
    }

    function marketing_vincular_adicionar($idDaParceria, $idDaTurma) {
        $status = "erro";
        if ($idDaParceria && $idDaTurma) {
            $linkExistente = $this->ParceriaTurma->find('count', array('conditions' => array('parceria_id' => $idDaParceria, 'turma_id' => $idDaTurma)));
            if ($linkExistente == 0)
                if ($this->ParceriaTurma->save(array('ParceriaTurma' => array('parceria_id' => $idDaParceria, 'turma_id' => $idDaTurma)))) {
                    $this->set('parceriaTurma', $this->ParceriaTurma->read());
                    $status = "sucesso";
                }
        }

        $this->layout = false;
        Configure::write('debug', 0);
        $this->set('status', $status);
    }

    private function _listar() {
        $this->layout = false;
        App::Import('Controller', 'Parceiros');
        $parceirosController = new ParceirosController();
        $turmaLogada = $this->obterTurmaLogada();

        $this->ParceriaTurma->contain();
        $this->ParceriaTurma->recursive = 0;
        $parceriasTurmas = $this->ParceriaTurma->find('list', array(
            'fields' => array('parceria_id'),
            'conditions' => array('ParceriaTurma.turma_id' => $turmaLogada['Turma']['id'])));

        $this->Parceria->contain('Parceiro');
        $parcerias = $this->Parceria->find('all', array(
            'conditions' => array(
                'OR' => array('Parceria.id' => $parceriasTurmas, 'Parceria.publica' => 1),
                'Parceria.ativa' => 1,
                'Parceiro.ativo' => 1,
                'Parceria.data_inicio <=' => date('Y-m-d', strtotime("now")),
                'IFNULL(Parceria.data_fim, DATE_ADD(NOW(), INTERVAL 1 MONTH)) >=' => date('Y-m-d', strtotime("now"))
        )));

        $this->set('parcerias', $parcerias);
        $this->set('caminho_raiz_logos', $parceirosController->caminho_raiz_logos);
    }
    
    function formando_listar() {
        $this->_listar();
    }

    function planejamento_listar() {
        $this->_listar();
    }

    function comissao_listar() {
        $this->_listar();
    }

    function super_listar() {
        $this->_listar();
    }

    function atendimento_listar() {
        $this->_listar();
    }
    
    private function _exibir($id = false){
        $this->layout = false;
        $this->Parceria->contain('FotoParceria', 'Parceiro');
        $parcerias = $this->Parceria->read(null, $id);
        $this->set('parcerias', $parcerias);
        if ($parcerias != null) {
            App::Import('Controller', 'Parceiros');
            $parceirosController = new ParceirosController();
            $this->set('caminho_raiz_logos', $parceirosController->caminho_raiz_logos);
            $this->set('caminho_raiz_fotos_parceiro', $parceirosController->caminho_raiz_fotos);
            $this->set('caminho_raiz_fotos_parceria', $this->caminho_raiz_fotos);
            $this->Parceiro->contain('FotoParceiro');
            $parceiro = $this->Parceiro->findById($parcerias['Parceiro']['id']);
            $this->set('parcerias', $parcerias);
            $this->set('parceiro', $parceiro);
        } else {
            $this->Session->setFlash('Parceria não existente.', 'flash_erro');
        }
    }    
    
    function formando_exibir($id = false){
        $this->_exibir($id);
    }
    
    function comissao_exibir($id = false){
        $this->_exibir($id);
    }

    function formando_index() {
        App::Import('Controller', 'Parceiros');
        $parceirosController = new ParceirosController();
        $turmaLogada = $this->obterTurmaLogada();

        $this->ParceriaTurma->contain();
        $this->ParceriaTurma->recursive = 0;
        $parceriasTurmas = $this->ParceriaTurma->find('list', array(
            'fields' => array('parceria_id'),
            'conditions' => array('ParceriaTurma.turma_id' => $turmaLogada['Turma']['id'])));

        $this->Parceria->contain('Parceiro');
        $parcerias = $this->Parceria->find('all', array(
            'conditions' => array(
                'OR' => array('Parceria.id' => $parceriasTurmas, 'Parceria.publica' => 1),
                'Parceria.ativa' => 1,
                'Parceiro.ativo' => 1,
                'Parceria.data_inicio <=' => date('Y-m-d', strtotime("now")),
                'IFNULL(Parceria.data_fim, DATE_ADD(NOW(), INTERVAL 1 MONTH)) >=' => date('Y-m-d', strtotime("now"))
        )));

        $this->set('parcerias', $parcerias);
        $this->set('caminho_raiz_logos', $parceirosController->caminho_raiz_logos);
    }

    function planejamento_index() {
        $this->formando_index();
        $this->render('formando_index');
    }

    function comissao_index() {
        $this->formando_index();
        $this->render('formando_index');
    }

    function super_index() {
        $this->formando_index();
        $this->render('formando_index');
    }

    function atendimento_index() {
        $this->formando_index();
        $this->render('formando_index');
    }
    
    private function _visualizar($idDaParceria) {
        $turmaLogada = $this->obterTurmaLogada();

        $this->ParceriaTurma->contain();
        $this->ParceriaTurma->recursive = 0;
        $parceriasTurmas = $this->ParceriaTurma->find('list', array(
            'fields' => array('parceria_id'),
            'conditions' => array('ParceriaTurma.turma_id' => $turmaLogada['Turma']['id'])));
        $this->Parceria->contain('FotoParceria', 'Parceiro');
        $parceria = $this->Parceria->recursive = 1;
        $parceria = $this->Parceria->find(array(
            'Parceria.id' => $idDaParceria,
            'OR' => array('Parceria.id' => $parceriasTurmas, 'Parceria.publica' => 1)));

        if ($parceria != null) {
            App::Import('Controller', 'Parceiros');
            $parceirosController = new ParceirosController();
            $this->set('caminho_raiz_logos', $parceirosController->caminho_raiz_logos);
            $this->set('caminho_raiz_fotos_parceiro', $parceirosController->caminho_raiz_fotos);
            $this->set('caminho_raiz_fotos_parceria', $this->caminho_raiz_fotos);
            $this->Parceiro->contain('FotoParceiro');
            $parceiro = $this->Parceiro->findById($parceria['Parceiro']['id']);
            $this->set('parceria', $parceria);
            $this->set('parceiro', $parceiro);
        } else {
            $this->Session->setFlash('Parceria não existente.', 'flash_erro');
        }
        $this->render('_visualizar');
    }

    function formando_visualizar($idDaParceria) {
        $this->_visualizar($idDaParceria);
    }

    function planejamento_visualizar($idDaParceria) {
        $this->_visualizar($idDaParceria);
    }

    function comissao_visualizar($idDaParceria) {
        $this->_visualizar($idDaParceria);
    }

    function super_visualizar($idDaParceria) {
        $this->_visualizar($idDaParceria);
    }

    function atendimento_visualizar($idDaParceria) {
        $this->_visualizar($idDaParceria);
    }
    
    private function _voucher($idParceria) {
        Configure::write('debug', 0);
        $this->layout = 'impressao_talao';
        App::Import('Controller', 'Parceiros');
        $parceirosController = new ParceirosController();
        $usuario = $this->Session->read('Usuario');
        $this->loadModel('ParceriaUsuario');
        if ($this->ParceriaUsuario->save(array('parceria_id' => $idParceria, 'usuario_id' => $usuario['Usuario']['id']))) {
            $this->set('caminho_raiz_logos', $parceirosController->caminho_raiz_logos);
            $this->set('caminho_raiz_fotos_parceiro', $parceirosController->caminho_raiz_fotos);
            $this->set('caminho_raiz_fotos_parceria', $this->caminho_raiz_fotos);
            $this->Parceiro->contain('FotoParceiro');
            $parceria = $this->Parceria->findById($idParceria);
            $this->set("parceria", $parceria);
            $formando = $this->FormandoProfile->find('first', array('conditions' => array('FormandoProfile.usuario_id' => $usuario['Usuario']['id'])));
            $this->set("formando", $formando);
            $this->set('codigoFormando', $formando['CursoTurma']['Turma']['id'] . str_pad($formando['FormandoProfile']['codigo_formando'], 3, '0', STR_PAD_LEFT));
            if (!empty($formando['FormandoProfile']['diretorio_foto_perfil']))
                $this->set("foto_formando", $this->webroot . $formando['FormandoProfile']['diretorio_foto_perfil']);
            else
                $this->set("foto_formando", $this->webroot . "img/uknown_user.gif");
            $this->set("logoas", $this->webroot . "img/logo_rifa.png");
        } else {
            $this->Session->setFlash('Erro ao gerar voucher.', 'flash_erro');
        }
        $this->render('_voucher');
    }

    function formando_voucher($idParceria) {
        $this->_voucher($idParceria);
    }

    function planejamento_voucher($idParceria) {
        $this->_voucher($idParceria);
    }

    function comissao_voucher($idParceria) {
        $this->_voucher($idParceria);
    }

    function super_voucher($idParceria) {
        $this->_voucher($idParceria);
    }

    function atendimento_voucher($idParceria) {
        $this->_voucher($idParceria);
    }
    
    function marketing_relatorio($id = null){
        $parceria = $this->Parceria->find(array('Parceria.id' => $id));
        $this->loadModel('ViewFormandos');
        $relatorio = $this->ViewFormandos->query('select f.turma_id, f.codigo_formando, f.nome, po.nome "parceria", pu.data_cadastro
                                                from
                                                vw_formandos f
                                                inner join
                                                parcerias_usuarios pu
                                                on 
                                                pu.usuario_id = f.id
                                                inner join
                                                parcerias p
                                                on
                                                p.id = pu.parceria_id
                                                inner join
                                                parceiros po
                                                on
                                                po.id = p.parceiro_id
                                                where
                                                p.id = '.$parceria['Parceria']['id'] .' group by codigo_formando order by f.codigo_formando');
        $this->set('relatorio', $relatorio);
    }

}

?>