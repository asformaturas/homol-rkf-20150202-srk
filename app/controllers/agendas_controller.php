<?php

/**
 * Controle da Agenda que gerencia os eventos. A pessoa que cria um agendamento
 * pode é a única que pode edita-la. Os agendamentos podem ser compartilhados
 * com outros usuários.
 *
 * @author Alexandre Minoru Sugano
 */
class AgendasController extends AppController {
/* teste da svn */
    var $name = 'Agendas';
    var $helpers = array('Calendario', 'ListaAgenda', 'Html');
    var $components = array('Session');
    var $uses = array('Agenda','TurmasUsuario','Usuario');

    function super_index() {
        $this->_index();
    }

    function super_calendario($year, $month) {
        $this->_calendario($year, $month);
    }

    function super_lista() {
        $this->_lista();
    }

    function super_evento($id) {
        $this->_evento($id);
    }

    function super_adicionar() {
        $this->_adicionar();
    }

    function super_editar($id = null) {
        $this->_editar($id);
    }

    function super_remover($id) {
        $this->_remover($id);
    }

    function comercial_index() {
        $this->_index();
    }

    function comercial_calendario($year, $month) {
        $this->_calendario($year, $month);
    }

    function comercial_lista() {
        $this->_lista();
    }

    function comercial_evento($id) {
        $this->_evento($id);
    }

    function comercial_adicionar() {
        $this->_adicionar();
    }

    function comercial_editar($id = null) {
        $this->_editar($id);
    }

    function comercial_remover($id) {
        $this->_remover($id);
    }

	function comissao_index() {
		$this->_index();
	}
	
	function comissao_calendario($year, $month) {
        $this->_calendario($year, $month);
    }

    function comissao_lista() {
        $this->_lista();
    }

    function comissao_evento($id) {
        $this->_evento($id);
    }

    function comissao_adicionar() {
        $this->_adicionar();
    }

    function comissao_editar($id = null) {
        $this->_editar($id);
    }

    function comissao_remover($id) {
        $this->_remover($id);
    }
    
    function planejamento_index() {
        $this->_index();
    }

    function planejamento_calendario($year, $month) {
        $this->_calendario($year, $month);
    }

    function planejamento_lista() {
        $this->_lista();
    }

    function planejamento_evento($id) {
        $this->_evento($id);
    }

    function planejamento_adicionar() {
        $this->_adicionar();
    }

    function planejamento_editar($id = null) {
        $this->_editar($id);
    }

    function planejamento_remover($id) {
        $this->_remover($id);
    }

    /* Common functions */

    function _index() {
        $this->_calendario(date('Y'), date('m'));
    }

    function _calendario($year, $month) {
        if ($year == null || $year == '' || $month == null || $month == '') {
            $year = date('Y');
            $month = date('m');
        }
        $events = $this->__getEvents($year, $month);
        $sharedEvents = $this->__getSharedEvents($year, $month);
        $turmaEvents = $this->__getTurmaEvents($year, $month);

        $data = array();
        foreach ($events as $event) {
            $dateTime = $this->create_date_time_from_format('Y-m-d H:i:s', $event['Agenda']['data']);
            $day = date_format($dateTime, 'j ');
            $data[intval($day)][] = $event;
        }
        foreach ($sharedEvents as $event) {
            $dateTime = $this->create_date_time_from_format('Y-m-d H:i:s', $event['Agenda']['data']);
            $day = date_format($dateTime, 'j ');
            $data[intval($day)][] = $event;
        }
        foreach ($turmaEvents as $event) {
            $dateTime = $this->create_date_time_from_format('Y-m-d H:i:s', $event['Agenda']['data']);
            $day = date_format($dateTime, 'j ');

            //verifica um evento duplicado
            $isDuplicated = false;
            if(isset($data[intval($day)])) {
                foreach ($data[intval($day)] as $e) {
                    if ($e['Agenda']['id'] == $event['Agenda']['id']) {
                        $isDuplicated = true;
                        break;
                    }
                }
            }
            if (!$isDuplicated) {
                $data[intval($day)][] = $event;
            }
        }

        $this->set('year', $year);
        $this->set('month', $month);
        $this->set('data', $data);
    }

    function _lista() {
        $events = $this->__getFutureEvents();
        $sharedEvents = $this->__getFutureSharedEvents();
        $turmaEvents = $this->__getFutureTurmaEvents();

        $data = array();
        foreach ($events as $event) {
            $dateTime = $this->create_date_time_from_format('Y-m-d H:i:s', $event['Agenda']['data']);
            $date = date_format($dateTime, 'Ymd');
            $data[intval($date)][] = $event;
        }
        foreach ($sharedEvents as $event) {
            $dateTime = $this->create_date_time_from_format('Y-m-d H:i:s', $event['Agenda']['data']);
            $date = date_format($dateTime, 'Ymd');
            $data[intval($date)][] = $event;
        }
        foreach ($turmaEvents as $event) {
            $dateTime = $this->create_date_time_from_format('Y-m-d H:i:s', $event['Agenda']['data']);
            $date = date_format($dateTime, 'Ymd');

            //verifica um evento duplicado
            $isDuplicated = false;
            if (isset($data[intval($date)])) {
                foreach ($data[intval($date)] as $e) {
                    if ($e['Agenda']['id'] == $event['Agenda']['id']) {
                        $isDuplicated = true;
                        break;
                    }
                }
            }
            if (!$isDuplicated) {
                $data[intval($date)][] = $event;
            }
        }
        ksort($data);
        $this->set('data', $data);
    }

    function _evento($id = null) {
        $agenda = $this->Agenda->findById($id);

        if (!$agenda) {
            $this->Session->setFlash(__('Agenda inexistente', true), 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/agendas");
        }
        $this->set('evento', $agenda);
    }

    function _adicionar() {
        if (!empty($this->data)) {
            $dados = $this->data;
            if(preg_match('/^[0-9]{2}-[0-9]{2}-[0-9]{4} [0-9]{2}:[0-9]{2}$/i', $dados['Agenda']['data-hora'])) {
                $dateTime = $this->create_date_time_from_format('d-m-Y H:i', $dados['Agenda']['data-hora']);
                $dados['Agenda']['data'] = date_format($dateTime, 'Y-m-d H:i:s');
            } else {
                $dados['Agenda']['data'] = ' ';
                $this->data['Agenda']['data-hora'] = date('d-m-Y H:i');
            }
            
            $usuario = $this->Session->read('Usuario');
            $chave = 'visivel' . ucfirst($usuario['Usuario']['grupo']);
            $dados['Agenda'][$chave] = 1;
			
			if(empty($dados['Agenda']['turmas_id'])) {
				$turma = $this->Session->read('turma');
				$dados['Agenda']['turmas_id'] = $turma['Turma']['id'];
			}
			
            $this->Agenda->create();
            if ($this->Agenda->saveAll($dados)) {
                $this->Session->setFlash(__('O evento da agenda foi criado corretamente', true), 'flash_sucesso');
                $this->redirect('/' . $this->params['prefix'] . '/agendas/evento/' . $this->Agenda->id);
            } else {
                $this->Session->setFlash(__('Ocorreu um erro ao salvar o agendamento', true), 'flash_erro');
            }
        } else {
            $usuario = $this->Session->read('Usuario');
            $this->set('usuario_id', $usuario['Usuario']['id']);
            
            $this->data['Agenda']['data-hora'] = date('d-m-Y H:i');
        }

        $usuarios = $this->Agenda->Compartilhado->find('list', array(
            'conditions' => array(
                'NOT' => array('id' => $usuario['Usuario']['id'])),
            'fields' => array('id', 'nome', 'nivel')));
        $this->set('usuarios', $usuarios);
		
		
		//TODO :  tentar fazer usando bindModel ou relações entre models
		$sql = "SELECT turma_id FROM turmas_usuarios WHERE usuario_id = " . $usuario['Usuario']['id'];
		$turmas_do_usuario_query = $this->Turma->query($sql);
	
		$turmas_do_usuario = array();
		foreach($turmas_do_usuario_query as $turma_aux) {
			$turmas_do_usuario[] = $turma_aux['turmas_usuarios']['turma_id'];
		}

        $turmas = $this->Agenda->Turma->find('list', array(
			'fields' => array('id', 'nome'), 
			'conditions' => array(
				'id' => $turmas_do_usuario,
				'status' => ($usuario['Usuario']['grupo'] == 'comercial') ? 'aberta' : 'fechada'
				)
		));
		
		if(sizeof($turmas) == 0 ) {
			//usuario nao esta vinculado a nenhuma turma
			$this->Session->setFlash('Você não está vinculado a nenhuma turma em andamento.' , 'flash_erro');
			$this->redirect("/{$this->params['prefix']}/agendas");
		}
		$this->set('turmas', $turmas);
    }

    function _editar($id = null) {
        $usuario = $this->Session->read('Usuario');
        if (!empty($this->data)) {
            $old = $this->Agenda->findById($this->data['Agenda']['id']);
            if ($old['Criador']['id'] != $usuario['Usuario']['id']) {
                $this->Session->setFlash(__('O evento da agenda não pode ser editado', true), 'flash_erro');
                $this->redirect('/' . $this->params['prefix'] . '/agendas/evento/' . $this->Agenda->id);
            }
            $dados = $this->data;
            $dateTime = $this->create_date_time_from_format('d-m-Y H:i', $dados['Agenda']['data-hora']);
            $dados['Agenda']['data'] = date_format($dateTime, 'Y-m-d H:i:s');
            unset($dados['Agenda']['data-hora']);
            if ($this->Agenda->saveAll($dados)) {
                $this->Session->setFlash(__('O evento da agenda foi editado corretamente', true), 'flash_sucesso');
                $this->redirect('/' . $this->params['prefix'] . '/agendas/evento/' . $this->Agenda->id);
            } else {
                $this->Session->setFlash(__('Ocorreu um erro ao salvar o agendamento', true), 'flash_erro');
            }
        } else {
            $this->data = $this->Agenda->findById($id);

            if (!$this->data) {
                $this->Session->setFlash('Agenda não existente,', 'flash_erro');
                $this->redirect('/' . $this->params['prefix'] . '/agendas');
            }

            $dateTime = $this->create_date_time_from_format('Y-m-d H:i:s', $this->data['Agenda']['data']);
            $this->data['Agenda']['data-hora'] = date_format($dateTime, 'd-m-Y H:i');
            $this->set('usuario_id', $usuario['Usuario']['id']);
        }

        $usuarios = $this->Agenda->Compartilhado->find('list', array(
            'conditions' => array(
                'NOT' => array('id' => $usuario['Usuario']['id'])),
            'fields' => array('id', 'nome', 'nivel')));
        $this->set('usuarios', $usuarios);
        $turmas = $this->Agenda->Turma->find('list', array('fields' => array('id', 'nome')));
        $this->set('turmas', $turmas);
    }

    function _remover($id = null) {
        $usuario = $this->Session->read('Usuario');
        $old = $this->Agenda->findById($id);

        if ($old['Criador']['id'] == $usuario['Usuario']['id']) {
            if ($this->Agenda->delete($id, true))
                $this->Session->setFlash('Agendamento excluido com sucesso.', 'flash_sucesso');
            else
                $this->Session->setFlash('Ocorreu um erro ao excluir o agendamento', 'flash_erro');
        }

        $this->redirect('/' . $this->params['prefix'] . '/agendas');
    }

    /* Auxiliar functions */

    function __getEvents($year, $month) {
        $usuario = $this->Session->read('Usuario');
        $listEvents = $this->Agenda->find('all', array(
            'conditions' => array(
                'Agenda.usuarios_id' => $usuario['Usuario']['id'],
                'Agenda.data >=' => date("Y-m-01 H:i:s", mktime(0, 0, 0, $month, 1, $year)),
                'Agenda.data <=' => date("Y-m-t H:i:s", mktime(23, 59, 59, $month, 1, $year))),
            'fields' => array(
                'Agenda.id', 'Agenda.titulo', 'Agenda.descricao',
                'Agenda.local', 'Agenda.data', 'Criador.id',
                'Criador.nome', 'Criador.grupo', 'Turma.id')
                )
        );
        return $listEvents;
    }

    function __getSharedEvents($year, $month) {
        $usuario = $this->Session->read('Usuario');
        $sql = " SELECT ";
        $sql .= " Compartilhado.agenda_id as AgendaId";
        $sql .= " FROM agendas_usuarios as Compartilhado ";
        $sql .= " LEFT JOIN agendas ON agendas.id = Compartilhado.agenda_id";
        $sql .= " WHERE usuario_id = " . $usuario['Usuario']['id'];
        $sql .= " AND agendas.data >= '" . date("Y-m-01 H:i:s", mktime(0, 0, 0, $month, 1, $year)) . "'";
        $sql .= " AND agendas.data <= '" . date("Y-m-t H:i:s", mktime(23, 59, 59, $month, 1, $year)) . "'";
        $shared = $this->Agenda->query($sql);
        $listSharedEvents = array();
        foreach ($shared as $evento) {
            $listSharedEvents[] = $this->Agenda->findById($evento['Compartilhado']['AgendaId']);
        }
        return $listSharedEvents;
    }

    function __getTurmaEvents($year, $month) {
        $usuario = $this->Session->read('Usuario');
		$turma = $this->Session->read('turma');

        $sql = " SELECT Agenda.*";
        $sql .= " FROM agendas as Agenda";
        $sql .= " LEFT JOIN turmas ON turmas.id = Agenda.turmas_id";
        $sql .= " LEFT JOIN turmas_usuarios ON turmas.id = turmas_usuarios.turma_id";
        $sql .= " LEFT JOIN usuarios ON usuarios.id = turmas_usuarios.usuario_id";
        $sql .= " WHERE usuarios.id = " . $usuario['Usuario']['id'] . " AND";
        $sql .= " ((usuarios.grupo='comercial' AND Agenda.visivelComercial=1) OR";
        $sql .= " (usuarios.grupo='planejamento' AND Agenda.visivelPlanejamento=1) OR";
        $sql .= " (usuarios.grupo='atendimento' AND Agenda.visivelAtendimento=1) OR";
        $sql .= " (usuarios.grupo='super' AND Agenda.visivelSuper=1) OR";
		$sql .= " (usuarios.grupo='comissao'))";
        $sql .= " AND Agenda.data >= '" . date("Y-m-01 H:i:s", mktime(0, 0, 0, $month, 1, $year)) . "'";
        $sql .= " AND Agenda.data <= '" . date("Y-m-t H:i:s", mktime(23, 59, 59, $month, 1, $year)) . "'";
        $sql .= " AND Agenda.turmas_id = ". $turma['Turma']['id'];

        $turmaShare = $this->Agenda->query($sql);

        $listTurmaEvents = array();
        foreach ($turmaShare as $evento) {
            $listTurmaEvents[] = $this->Agenda->findById($evento['Agenda']['id']);
        }
        return $listTurmaEvents;
    }

    function __getFutureEvents($days=0) {

        $usuario = $this->Session->read('Usuario');
        $condition = array();
        if ($days > 0) {
            $limit = time() + ($days * 24 * 60 * 60);
            $condition = array(
                'Agenda.usuarios_id' => $usuario['Usuario']['id'],
                'Agenda.data >=' => date("Y-m-d H:i:00"),
                'Agenda.data <=' => date("Y-m-d H:i:59", $limit));
        } else {
            $condition = array(
                'Agenda.usuarios_id' => $usuario['Usuario']['id'],
                'Agenda.data >=' => date("Y-m-d H:i:00"));
        }

        $listEvents = $this->Agenda->find('all', array(
            'conditions' => $condition,
            'fields' => array(
                'Agenda.id', 'Agenda.titulo', 'Agenda.descricao',
                'Agenda.local', 'Agenda.data', 'Criador.id',
                'Criador.nome', 'Criador.grupo'))
        );

        return $listEvents;
    }

    function __getFutureSharedEvents($days=0) {

        $usuario = $this->Session->read('Usuario');
        $sql = " SELECT ";
        $sql .= " Compartilhado.agenda_id as AgendaId";
        $sql .= " FROM agendas_usuarios as Compartilhado ";
        $sql .= " LEFT JOIN agendas ON agendas.id = Compartilhado.agenda_id";
        $sql .= " WHERE usuario_id = " . $usuario['Usuario']['id'];
        $sql .= " AND agendas.data >= '" . date("Y-m-d H:i:00") . "'";
        if ($days > 0) {
            $limit = time() + ($days * 24 * 60 * 60);
            $sql .= " AND agendas.data <= '" . date("Y-m-d H:i:59", $limit) . "'";
        }
        $shared = $this->Agenda->query($sql);
        $listSharedEvents = array();
        foreach ($shared as $evento) {
            $listSharedEvents[] = $this->Agenda->findById($evento['Compartilhado']['AgendaId']);
        }
        return $listSharedEvents;
    }

    function __getFutureTurmaEvents($days=0) {
        $usuario = $this->Session->read('Usuario');
		$turma = $this->Session->read('turma');
        $sql = " SELECT Agenda.*";
        $sql .= " FROM agendas as Agenda";
        $sql .= " LEFT JOIN turmas ON turmas.id = Agenda.turmas_id";
        $sql .= " LEFT JOIN turmas_usuarios ON turmas.id = turmas_usuarios.turma_id";
        $sql .= " LEFT JOIN usuarios ON usuarios.id = turmas_usuarios.usuario_id";
        $sql .= " WHERE usuarios.id = " . $usuario['Usuario']['id'] . " AND";
        $sql .= " ((usuarios.grupo='comercial' AND Agenda.visivelComercial=1) OR";
        $sql .= " (usuarios.grupo='planejamento' AND Agenda.visivelPlanejamento=1) OR";
        $sql .= " (usuarios.grupo='atendimento' AND Agenda.visivelAtendimento=1) OR";
        $sql .= " (usuarios.grupo='super' AND Agenda.visivelSuper=1) OR";
		$sql .= " (usuarios.grupo='comissao'))";
        $sql .= " AND Agenda.turmas_id = ". $turma['Turma']['id'];
        $sql .= " AND Agenda.data >= '" . date("Y-m-d H:i:00") . "'";
        if ($days > 0) {
            $limit = time() + ($days * 24 * 60 * 60);
            $sql .= " AND agendas.data <= '" . date("Y-m-d H:i:59", $limit) . "'";
        }
        $turmaShare = $this->Agenda->query($sql);

        $listTurmaEvents = array();
        foreach ($turmaShare as $evento) {
            $listTurmaEvents[] = $this->Agenda->findById($evento['Agenda']['id']);
        }
        return $listTurmaEvents;
    }

}

?>
