<?php

App::import('Component', 'Protocolo');

class SolicitacoesController extends AppController {

    var $name = 'Solicitacoes';
    var $uses = array('Protocolo', 'Despesa','Pagamento', 'ViewFormandos');
    var $nomeDoTemplateSidebar = 'Solicitacoes';

    function _index() {
        $usuario = $this->Session->read('Usuario');
        if ($this->data) {
            if (!$this->data['Solicitacoes']['protocolo']) {
                $this->Session->setFlash("Protocolo {$protocolo} n&atilde;o encontrado. Por favor entre em contato conosco.", 'flash_erro');
            } else {
                $protocolo = $this->data['Solicitacoes']['protocolo'];
                if ($this->Protocolo->find('first', array('conditions' => array('protocolo' => $protocolo))))
                    $this->redirect("/{$this->params['prefix']}/solicitacoes/visualizar/{$this->data['Solicitacoes']['protocolo']}");
                else
                    $this->Session->setFlash("Protocolo {$protocolo} n&atilde;o encontrado. Por favor entre em contato conosco.", 'flash_erro');
            }
        }
    }

    function atendimento_index() {
        $this->_index();
    }

    function formando_index() {
        $this->_index();
    }

    function producao_index() {
        $this->_index();
    }

    function planejamento_index() {
        $this->_index();
    }

    function _visualizar($protocolo) {
        $this->autoRender = false;
        $dados = $this->Protocolo->find('first', array('conditions' => array('Protocolo.protocolo' => $protocolo, 'Protocolo.grupo' => 'formando')));
        if (empty($dados)) {
            $this->Session->setFlash("Protocolo {$protocolo} n&atilde;o encontrado. Por favor entre em contato conosco.", 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/solicitacoes");
        } else {
            $this->redirect("/{$this->params['prefix']}/solicitacoes/{$dados['Protocolo']['tipo']}/$protocolo");
        }
    }

    function producao_visualizar($protocolo) {
        $this->_visualizar($protocolo);
    }

    function atendimento_visualizar($protocolo) {
        $this->_visualizar($protocolo);
    }

    function planejamento_visualizar($protocolo) {
        $this->_visualizar($protocolo);
    }

    function formando_visualizar($protocolo = FALSE) {
        $this->_visualizar($protocolo);
    }

    function atendimento_cancelamento($protocolo) {
        $this->formando_cancelamento($protocolo);
    }

    function formando_cancelamento($protocolo) {
        $this->loadModel('ProtocoloCancelamento');
        $dados = $this->ProtocoloCancelamento->find('first', array('conditions' => array('Protocolo.protocolo' => $protocolo, 'Protocolo.grupo' => 'formando')));
        $this->ProtocoloCancelamento->id = $dados['ProtocoloCancelamento']['id'];
        $protocoloCancelamento = $this->ProtocoloCancelamento->read();
        $this->Usuario->recursive = 1;
        $this->Usuario->unbindModel(array('hasAndBelongsToMany' => array('Campanhas')));
        $this->Usuario->unbindModel(array('hasMany' => array('Despesa')));
        $formando = $this->Usuario->find('first', array('conditions' => array('Usuario.id' => $protocoloCancelamento['Protocolo']['usuario_id'])));
        $usuarioCriador = $this->Usuario->find('first', array('conditions' => array('Usuario.id' => $protocoloCancelamento['Protocolo']['usuario_criador'])));
        $this->set('formando', $formando);
        $this->set('usuarioCriador', $usuarioCriador);
        if (!empty($this->data)) {
            if (!$this->ProtocoloCancelamento->save($this->data['Solicitacao']))
                $this->Session->setFlash('Erro ao salvar dados banc&aacute;rios. Tente novamente mais tarde ou entre em contato conosco.', 'flash_erro');
            else
                $this->Session->setFlash('Dados banc&aacute;rios salvo com sucesso.', 'flash_sucesso');
            $protocoloCancelamento = $this->ProtocoloCancelamento->read();
        }
        $this->Despesa->recursive = 2;
        $despesasComAdesao = $this->Despesa->find('all', array('conditions' => array('Despesa.usuario_id' => $protocoloCancelamento['Protocolo']['usuario_id'])));
        $saldoEmAberto = 0;
        if (!empty($despesasComAdesao))
            $dadosBancarios = TRUE;
        else
            $dadosBancarios = FALSE;
        foreach ($despesasComAdesao as $despesaComAdesao) {
            if ($dadosBancarios === TRUE && $despesaComAdesao['Despesa']['tipo'] == 'cancelamento')
                $dadosBancarios = FALSE;
            if ($despesaComAdesao['Despesa']['status'] == 'aberta') {
                $saldoEmAberto+= $despesaComAdesao['Despesa']['valor'];
            } elseif (isset($despesaComAdesao['DespesaPagamento'][0]['Pagamento']) && $despesaComAdesao['Despesa']['status'] == 'paga') {
                $valorPago = $despesaComAdesao['DespesaPagamento'][0]['Pagamento']['valor_nominal'];
                $valorPago = $valorPago > $despesaComAdesao['Despesa']['valor'] ? $despesaComAdesao['Despesa']['valor'] : $valorPago;
                $valo_igmp = 0;
                $saldoEmAberto += ($despesaComAdesao['Despesa']['valor'] + $valo_igmp) - $valorPago;
            }
        }
        $pagamentos = $this->Pagamento->find('all', array('conditions' => array('Pagamento.status' => 'pago', 'Pagamento.usuario_id' => $protocoloCancelamento['Protocolo']['usuario_id'])));
        $totalPago = 0;
        foreach ($pagamentos as $pagamento) {
            if (sizeof($pagamento['DespesaPagamento']) == 0) {
                if ($pagamento['Pagamento']['codigo'] != "") {
                    if ((int) substr($pagamento['Pagamento']['codigo'], 1, 1) < 5)
                        $totalPago+= $pagamento['Pagamento']['valor_nominal'];
                }
            } else {
                $totalPago+= $pagamento['Pagamento']['valor_nominal'];
            }
        }
        $saldoCancelamento = ($formando['FormandoProfile']['valor_adesao'] * 0.2) - $totalPago;
        $this->set('saldoCancelamento', $saldoCancelamento);
        $this->set('dadosBancarios', $dadosBancarios);
        $this->set('protocoloCancelamento', $protocoloCancelamento);
        $this->render('formando_cancelamento');
    }

    function atendimento_cancelamento_isento() {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('error' => true);
        if (isset($_POST['protocolo'])) {
            $protocolo = $_POST['protocolo'];
            $this->loadModel('ProtocoloCancelamento');
            $protocoloCancelamento = $this->ProtocoloCancelamento->find('first', array('conditions' => array('Protocolo.protocolo' => $protocolo)));
            if (!empty($protocoloCancelamento)) {
                if (!$this->ProtocoloCancelamento->updateAll(
                                array('ProtocoloCancelamento.observacao' => "'{$_POST['obs']}'"), array('Protocolo.protocolo' => $protocolo
                        )))
                    $json['message'] = "Erro ao atualizar protocolo";
                elseif (!$this->Protocolo->updateAll(
                                array('Protocolo.status' => "'finalizado'"), array('Protocolo.protocolo' => $protocolo
                        )))
                    $json['message'] = "Erro ao atualizar o protocolo";
                else
                    $json['error'] = false;
            } else {
                $json['message'] = "Protocolo de cancelamento não encontrato";
            }
            echo json_encode($json);
        }
    }

    function formando_checkout($protocolo) {
        $this->layout = 'metro/externo';
        $this->loadModel('ProtocoloCheckoutFormando');
        $this->loadModel('ViewFormandos');
        $this->loadModel('CampanhasUsuario');
        $this->loadModel('CheckoutFormandoPagamento');
        $this->loadModel('CheckoutUsuarioItem');
        $protocoloCheckout = $this->ProtocoloCheckoutFormando->find('first', array('conditions' => array('Protocolo.protocolo' => $protocolo)));
        if (!empty($protocoloCheckout)) {
            $this->set('protocolo', $protocoloCheckout);
            $formando = $this->ViewFormandos->find('first', array('conditions' => array('ViewFormandos.id' => $protocoloCheckout['Protocolo']['usuario_id'])));
            $this->set('formando', $formando["ViewFormandos"]);
            $turma = $this->Turma->find('first', array('conditions' => array('id' => $formando['ViewFormandos']['turma_id'])));
            $this->set('turma', $turma);
            $usuarioCriador = $this->Usuario->find('first', array('conditions' => array('Usuario.id' => $protocoloCheckout['Protocolo']['usuario_criador'])));
            $this->set('usuarioCriador', $usuarioCriador);
            $pagamentosCheckout = $this->CheckoutFormandoPagamento->find('all', array('conditions' => array('protocolo_id' => $protocoloCheckout['Protocolo']['id'])));
            $this->set('pagamentosCheckout', $pagamentosCheckout);
            $totalPagoCheckout = 0;
            foreach ($pagamentosCheckout as $pagamentoCheckout)
                if ($pagamentoCheckout['CheckoutFormandoPagamento']['tipo'] != 'comprovante')
                    $totalPagoCheckout+= $pagamentoCheckout['CheckoutFormandoPagamento']['valor'];
            $this->set('totalPagoCheckout', $totalPagoCheckout);
            $itens = array('retirados' => array(), 'a_retirar' => array());
            $tituloContrato = "{$formando["ViewFormandos"]['convites_contrato']} convites e " .
                    "{$formando["ViewFormandos"]['mesas_contrato']} mesas referentes a contrato";
            if ($formando["ViewFormandos"]['retirou_itens_contrato'] == 0)
                $itens['a_retirar'][] = array('tipo' => 'contrato',
                    'descricao' => $tituloContrato, 'id' => $formando["ViewFormandos"]['id']);
            else
                $itens['retirados'][] = array('descricao' => $tituloContrato);
            $itensCheckout = $this->CheckoutUsuarioItem->find('all', array(
                'conditions' => array('protocolo_id' => $protocoloCheckout['Protocolo']['id'])));
            $totalItensCheckout = 0;
            foreach ($itensCheckout as $itemCheckout) {
                $totalItensCheckout+= $itemCheckout['CheckoutUsuarioItem']['saldo'];
                if($itemCheckout['CheckoutUsuarioItem']['pos_checkout'] == 1)
                    $descricao = "{$itemCheckout['CheckoutUsuarioItem']['quantidade']} " .
                            "{$itemCheckout['CheckoutItem']['titulo']} comprado(s) pós checkout";
                else
                    $descricao = "{$itemCheckout['CheckoutUsuarioItem']['quantidade']} " .
                        "{$itemCheckout['CheckoutItem']['titulo']} comprado(s) durante o checkout";
                if ($itemCheckout['CheckoutUsuarioItem']['retirou'] == 1)
                    $itens['retirados'][] = array('descricao' => $descricao);
                else
                    $itens['a_retirar'][] = array(
                        'descricao' => $descricao, 'tipo' => 'checkout', 'id' => $itemCheckout['CheckoutUsuarioItem']['id']);
            }
            $itensRetirar = $this->Session->read('CheckoutItensRetirar');
            if ($itensRetirar)
                $itens['a_retirar'] = $itensRetirar;
            $totalItensCheckout*= -1;
            $this->Despesa->recursive = 2;
            $adesoes = $this->Despesa->find('all', array('conditions' =>
                array('Despesa.usuario_id' => $formando['ViewFormandos']['id'], 'Despesa.tipo' => 'adesao', "Despesa.status != 'cancelada'")));
            $this->set('despesas_adesao', $adesoes);
            $this->Despesa->bindModel(array('belongsTo' => array('CampanhasUsuario')), false);
            $this->Despesa->recursive = 2;
            $this->CampanhasUsuario->bindModel(array('belongsTo' => array('Campanha')), false);
            $extras = $this->Despesa->find('all', array('conditions' => array('Despesa.usuario_id' => $formando['ViewFormandos']['id'], 'Despesa.tipo' => 'extra')));
            $this->set('despesas_extras', $extras);
            $totalDespesasAdesao = 0;
            $totalDespesasExtras = 0;
            $totalIGPM = array('despesa_aberta' => 0, 'despesa_paga' => 0);
            $totalPagoAdesao = 0;
            $totalPagoExtras = 0;
            $totalPagoNaoVinculado = 0;

            foreach ($adesoes as $adesao) {
                $totalDespesasAdesao += $adesao['Despesa']['valor'];
                if ($adesao['Despesa']['status'] == 'aberta' && $adesao['Despesa']['status_igpm'] != "pago")
                    $totalIGPM['despesa_aberta'] +=!empty($adesao['Despesa']['correcao_igpm']) ? $adesao['Despesa']['correcao_igpm'] : 0;
                elseif ($adesao['Despesa']['status'] == 'paga' && $adesao['Despesa']['status_igpm'] == 'pago')
                    $totalIGPM['despesa_paga'] +=!empty($adesao['Despesa']['correcao_igpm']) ? $adesao['Despesa']['correcao_igpm'] : 0;
                if (sizeof($adesao['DespesaPagamento']) != 0 && $adesao['Despesa']['status'] == 'paga') {
                    $valorPago = $adesao['DespesaPagamento'][0]['Pagamento']['valor_nominal'] > $adesao['DespesaPagamento'][0]['Pagamento']['valor_pago'] ? $adesao['DespesaPagamento'][0]['Pagamento']['valor_pago'] : $adesao['DespesaPagamento'][0]['Pagamento']['valor_nominal'];
                    $valorPago = $valorPago > $adesao['Despesa']['valor'] ? $adesao['Despesa']['valor'] : $valorPago;
                    $totalPagoAdesao+= $valorPago;
                }
            }

            $despesaIgpm = $this->Despesa->find('first', array('conditions' => array('Despesa.tipo' => 'igpm',
                    'Despesa.usuario_id' => $formando['ViewFormandos']['id'])));
            if ($despesaIgpm)
                $totalIGPM['despesa_paga'] = $despesaIgpm['Despesa']['valor'];
            $this->set('despesaIgpm', $despesaIgpm);

            $this->Despesa->bindModel(array('belongsTo' => array('CampanhasUsuario')), false);
            $this->Despesa->recursive = 2;
            $this->CampanhasUsuario->bindModel(array('belongsTo' => array('Campanha')), false);
            $extras = $this->Despesa->find('all', array('conditions' => array('Despesa.usuario_id' => $formando['ViewFormandos']['id'], 'Despesa.tipo' => 'extra')));
            $this->set('despesas_extras', $extras);

            foreach ($extras as $extra) {
                if ($extra['Despesa']['status'] != "cancelada")
                    $totalDespesasExtras += $extra['Despesa']['valor'];
                if (sizeof($extra['DespesaPagamento']) != 0 && $extra['Despesa']['status'] != 'aberta') {
                    if ($extra['DespesaPagamento'][0]['Pagamento']['status'] == "pago") {
                        $valorPago = $extra['DespesaPagamento'][0]['Pagamento']['valor_nominal'] > $extra['DespesaPagamento'][0]['Pagamento']['valor_pago'] ? $extra['DespesaPagamento'][0]['Pagamento']['valor_pago'] : $extra['DespesaPagamento'][0]['Pagamento']['valor_nominal'];
                        $valorPago = $valorPago > $extra['Despesa']['valor'] ? $extra['Despesa']['valor'] : $valorPago;
                        $totalPagoExtras+= $valorPago;
                    }
                }
            }

            $pagamentos = $this->Pagamento->find('all', array('conditions' => array('Pagamento.status' => 'pago', 'Pagamento.usuario_id' => $formando['ViewFormandos']['id'])));
            $pagamentos_nao_vinculados = array();
            foreach ($pagamentos as $pagamento) {
                if (sizeof($pagamento['DespesaPagamento']) == 0) {
                    $pagamentos_nao_vinculados[] = $pagamento;
                    $totalPagoNaoVinculado += $pagamento['Pagamento']['valor_nominal'];
                }
            }

            $this->loadModel("ViewRelatorioExtras");
            $relatorioExtras = $this->ViewRelatorioExtras->find('all', array(
                'conditions' => array('usuario_id' => $formando['ViewFormandos']['id'], 'cancelada' => 0)));
            foreach ($relatorioExtras as $itemExtra) {
                $descricao = "{$itemExtra['ViewRelatorioExtras']['quantidade_total']} {$itemExtra['ViewRelatorioExtras']['extra']} " .
                        "comprado(s) durante a campanha {$itemExtra['ViewRelatorioExtras']['campanha']} para o evento " .
                        $itemExtra['ViewRelatorioExtras']['evento'];
                if ($itemExtra['ViewRelatorioExtras']['quantidade_retirada'] == 0)
                    $itens['a_retirar'][] = array(
                        'descricao' => $descricao, 'tipo' => 'campanha', 'id' => $itemExtra['ViewRelatorioExtras']['id']);
                else
                    $itens['retirados'][] = array('descricao' => $descricao);
            }
            $this->set('resumoExtras', $relatorioExtras);
            $this->set('itens', $itens);
            $totalPago = $totalPagoAdesao + $totalPagoExtras + $totalPagoNaoVinculado;
            $totalDespesas = $totalDespesasExtras + $totalDespesasAdesao + array_sum($totalIGPM);
            $saldoAdesao = $totalPagoAdesao - ($totalDespesasAdesao + array_sum($totalIGPM));
            $saldoExtras = $totalPagoExtras - $totalDespesasExtras;
            $saldoTotal = $totalPago - $totalDespesas;

            $totalDespesas = (float) $totalDespesasExtras + (float) $totalDespesasAdesao + (float) array_sum($totalIGPM);
            $pagamento = (float) $totalPago + (float) $totalPagoCheckout;
            $this->set('totalPago', $totalPago);
            $this->set('itensCheckout', $itensCheckout);
            $this->set('totalItensCheckout', $totalItensCheckout);
            $this->set('totalDespesasAdesao', $totalDespesasAdesao);
            $this->set('totalDespesasExtras', $totalDespesasExtras);
            $this->set('totalDespesas', $totalDespesas);
            $this->set('totalIGPM', array_sum($totalIGPM));
            $this->set('saldoTotal', 0);
        } else {
            $this->Session->setFlash("Protocolo {$protocolo} n&atilde;o encontrato. Por favor entre em contato conosco.", 'metro/flash/error');
        }
    }

    function atendimento_checkout($protocolo) {
        $this->formando_checkout($protocolo);
    }

    function planejamento_checkout($protocolo) {
        $this->formando_checkout($protocolo);
    }
    
    function atendimento_imprimir_checkout($protocoloId) {
        $this->layout = 'metro/externo';
        $protocolo = $this->Protocolo->read(null,$protocoloId);
        if(!empty($protocolo)) {
            ini_set('memory_limit', '1024M');
            set_time_limit(0);
            $this->loadModel('CampanhasUsuario');
            $this->loadModel('CheckoutFormandoPagamento');
            $this->loadModel('CheckoutUsuarioItem');
            $this->loadModel('CampanhasUsuarioCampanhasExtra');
            $this->loadModel('CampanhasExtra');
            $this->loadModel('CampanhasUsuario');
            App::import('Component', 'Financeiro');
            $financeiro = new FinanceiroComponent();
            $this->set('protocolo', $protocolo);
            $formando = $this->ViewFormandos->read(null,$protocolo['Protocolo']['usuario_id']);
            $this->set('formando', $formando["ViewFormandos"]);
            $turma = $this->Turma->read(null,$formando['ViewFormandos']['turma_id']);
            $this->set('turma', $turma);
            $atendente = $this->Usuario->read(null,$protocolo['Protocolo']['usuario_criador']);
            $this->set('atendente', $atendente);
            $pagamentosCheckout = $this->CheckoutFormandoPagamento->find('all', array(
                'conditions' => array(
                    'CheckoutFormandoPagamento.protocolo_id' => $protocolo['Protocolo']['id'],
                    'CheckoutFormandoPagamento.tipo <>' => 'comprovante'
                )
            ));

            $totalPagoCheckout = 0;
            foreach($pagamentosCheckout as $pagamentoCheckout)
                $totalPagoCheckout += $pagamentoCheckout['CheckoutFormandoPagamento']['valor'];
            $this->set('totalPagoCheckout', $totalPagoCheckout);
            $this->set('pagamentosCheckout', $pagamentosCheckout);
            $itensCheckout = $this->CheckoutUsuarioItem->find('all', array(
                'conditions' => array(
                    'protocolo_id' => $protocolo['Protocolo']['id'],
                    'pos_checkout' => 0,
                )
            ));
            $mesas = array();
            $convites = array();
            $totalMeias = 0;
            if(!isset($this->data['itens'])) {
                $itens = array();
                $itensRetirados = array();
                $itensPosCheckoutRetirados = array();
                $tituloContrato = "{$formando["ViewFormandos"]['mesas_contrato']} mesa(s) e " .
                        "{$formando["ViewFormandos"]['convites_contrato']} convite(s) referentes a contrato";
                $itensRetirados[] = array(
                    'tipo' => 'contrato',
                    'descricao' => $tituloContrato,
                    'id' => $formando["ViewFormandos"]['id']
                );
                $convites = $formando["ViewFormandos"]['convites_contrato'];
                $mesas = $formando["ViewFormandos"]['mesas_contrato'];
                $totalConvites = $convites;
                $totalMesas = $mesas;
                $totalItensCheckout = 0;

                foreach ($itensCheckout as $itemCheckout) {
                    $totalItensCheckout+= $itemCheckout['CheckoutUsuarioItem']['saldo'];
                    $convites = $itemCheckout['CheckoutUsuarioItem']['quantidade'] * $itemCheckout['CheckoutItem']['quantidade_convites'];
                    $mesas = $itemCheckout['CheckoutUsuarioItem']['quantidade'] * $itemCheckout['CheckoutItem']['quantidade_mesas'];
                    if($convites == 0 && $mesas == 0){
                        $descricao = "{$mesas} mesa(s) e {$itemCheckout['CheckoutUsuarioItem']['quantidade']} convite(s) " .
                            "{$itemCheckout['CheckoutItem']['titulo']} comprado(s) durante o checkout";
                            if(strpos($itemCheckout['CheckoutItem']['titulo'], 'Meia') !== false ||
                               strpos($itemCheckout['CheckoutItem']['titulo'], 'MEIA') !== false ||
                               strpos($itemCheckout['CheckoutItem']['titulo'], 'Infantil') !== false ||
                               strpos($itemCheckout['CheckoutItem']['titulo'], 'INFANTIL') !== false)
                                $totalMeias++;
                    }else{
                        $descricao = "{$mesas} mesa(s) e {$convites} convite(s) " .
                                "{$itemCheckout['CheckoutItem']['titulo']} comprado(s) durante o checkout";
                    }
                    $itensRetirados[] = array(
                        'descricao' => $descricao,
                        'tipo' => 'checkout',
                        'id' => $itemCheckout['CheckoutUsuarioItem']['id']
                    );
                    $totalConvites += $convites;
                    $totalMesas += $mesas;
                }
            } else {
                $itens = $this->data['itens'];
            }
            $totalPagoCheckout = 0;
            $totalComprovanteCheckout = 0;
            foreach ($pagamentosCheckout as $pagamentoCheckout)
                if ($pagamentoCheckout['CheckoutFormandoPagamento']['tipo'] != 'comprovante')
                    $totalPagoCheckout+= $pagamentoCheckout['CheckoutFormandoPagamento']['valor'];
                else
                    $totalComprovanteCheckout += $pagamentoCheckout['CheckoutFormandoPagamento']['valor'];
            $this->set('totalPagoCheckout', $totalPagoCheckout);
            $totalItensCheckout = 0;
            foreach ($itensCheckout as $itemCheckout)
                $totalItensCheckout+= $itemCheckout['CheckoutUsuarioItem']['saldo'];
            $this->CampanhasUsuarioCampanhasExtra->unbindModel(array(
                'belongsTo' => array('CampanhasUsuario')
            ),false);
            $this->CampanhasUsuario->bindModel(array(
                'hasMany' => array(
                    'CampanhasUsuarioCampanhasExtra' => array(
                        'className' => 'CampanhasUsuarioCampanhasExtra',
                        'joinTable' => 'campanhas_usuarios_campanhas_extras',
                        'foreignKey' => 'campanhas_usuario_id'
                    )
                )
            ),false);
            $this->CampanhasUsuario->recursive = 3;
            $campanhas = $this->CampanhasUsuario->find('all',array(
                'conditions' => array(
                    'usuario_id' => $formando['ViewFormandos']['id'],
                    'cancelada' => 0
                )
            ));
            $itensCampanhaRetirados = array();
            foreach($campanhas as $campanha) {
                foreach($campanha['CampanhasUsuarioCampanhasExtra'] as $extra) {
                    $mesas = $extra['CampanhasExtra']['quantidade_mesas'] * $extra['quantidade'];
                    $convites = $extra['CampanhasExtra']['quantidade_convites'] * $extra['quantidade'];
                    if($mesas == 0 && $convites == 0){
                        $descricao = "{$extra['quantidade']} {$extra['CampanhasExtra']['Extra']['nome']} comprado(s) durante "
                        . "a campanha {$extra['CampanhasExtra']['Campanha']['nome']}";
                        if(strpos($extra['CampanhasExtra']['Extra']['nome'], 'Meia') !== false ||
                           strpos($extra['CampanhasExtra']['Extra']['nome'], 'MEIA') !== false ||
                           strpos($extra['CampanhasExtra']['Extra']['nome'], 'Infantil') !== false ||
                           strpos($extra['CampanhasExtra']['Extra']['nome'], 'INFANTIL') !== false)
                            $totalMeias++;
                    }else{
                        $descricao = "{$mesas} mesa(s) e {$convites} convite(s) comprado(s) durante "
                        . "a campanha {$extra['CampanhasExtra']['Campanha']['nome']}";
                    }
                    $itensCampanhaRetirados[] = array(
                        'descricao' => $descricao,
                        'tipo' => 'campanha',
                    );
                    $totalConvites += $convites;
                    $totalMesas += $mesas;
                }
            }
            $this->set('itens', $itens);
            $this->set('itensRetirados', $itensRetirados);
            $this->set('itensCampanhaRetirados', $itensCampanhaRetirados);
            $this->set('totalPago', $financeiro->obterTotalPago($formando['ViewFormandos']['id']) + $totalComprovanteCheckout);
            $this->set('valorAdesao', $financeiro->obterValorAdesao($formando['ViewFormandos']['id']));
            $this->set('valorCampanhas', $financeiro->obterValorExtras($formando['ViewFormandos']['id']));
            $this->set('itensCheckout', $itensCheckout);
            $this->set('totalConvites', $totalConvites);
            $this->set('totalMesas', $totalMesas);
            $this->set('totalMeias', $totalMeias);
            $this->set('totalItensCheckout', $totalItensCheckout);
            $this->set('totalIGPM', $financeiro->obterValorIgpm($formando['ViewFormandos']['id']));
            $this->set('saldoTotal', 0);
        } else {
            $this->Session->setFlash("Protocolo {$protocolo} n&atilde;o encontrado. Por favor entre em contato conosco.", 'metro/flash/error');
        }
    }

    function financeiro_imprimir_checkout($protocoloId){

        $this->atendimento_imprimir_checkout($protocoloId);
    }
    
    function atendimento_imprimir_pos_checkout($protocoloId){
        $this->layout = 'metro/externo';
        $protocolo = $this->Protocolo->read(null,$protocoloId);
        if(!empty($protocolo)) {
            $this->loadModel('CampanhasUsuario');
            $this->loadModel('CheckoutFormandoPagamento');
            $this->loadModel('CheckoutUsuarioItem');
            $this->set('protocolo', $protocolo);
            $formando = $this->ViewFormandos->read(null, $protocolo['Protocolo']['usuario_id']);
            $this->set('formando', $formando["ViewFormandos"]);
            $turma = $this->Turma->read(null, $formando['ViewFormandos']['turma_id']);
            $this->set('turma', $turma);
            $itensPosCheckout = $this->CheckoutUsuarioItem->find('all', array(
                'conditions' => array(
                    'CheckoutUsuarioItem.protocolo_id' => $protocolo['Protocolo']['id'],
                    'CheckoutUsuarioItem.retirou' => 1,
                    'CheckoutUsuarioItem.pos_checkout' => 1,
                )
            ));
            $this->CheckoutFormandoPagamento->unbindModelAll();
            $pagamentosCheckout = $this->CheckoutFormandoPagamento->find('all', array(
                'conditions' => array(
                    'CheckoutFormandoPagamento.protocolo_id' => $protocolo['Protocolo']['id'],
                    'CheckoutFormandoPagamento.pos_checkout' => 1
                )
            ));
            $pagamentos = array();
            foreach($pagamentosCheckout as $pagamentoCheckout){
                $atendente = $this->Usuario->findById($pagamentoCheckout['CheckoutFormandoPagamento']['usuario_id']);
                $pagamentos[] = array(
                    'valor' => number_format($pagamentoCheckout['CheckoutFormandoPagamento']['valor'], 2, ',', '.'),
                    'atendente' => ucfirst($atendente['Usuario']['nome']),
                    'data_cadastro' => date('d/m/Y &\a\g\r\a\v\e;\s H:i:s', strtotime($pagamentoCheckout['CheckoutFormandoPagamento']['data_cadastro'])),
                    'tipo' => $pagamentoCheckout['CheckoutFormandoPagamento']['tipo'],
                    'referente' => implode(" - ", array_map("ucfirst", explode(";", $pagamentoCheckout['CheckoutFormandoPagamento']['referente']))),
                    'vencimento' => date('d/m/Y', strtotime($pagamentoCheckout['CheckoutFormandoPagamento']['data_vencimento']))
                );
            }
            $this->set('pagamentos', $pagamentos);
            foreach ($itensPosCheckout as $itemPosCheckout) {
                $descricao = "{$itemPosCheckout['CheckoutUsuarioItem']['quantidade']} " .
                        "{$itemPosCheckout['CheckoutItem']['titulo']} comprado(s) pós checkout";
                if ($itemPosCheckout['CheckoutUsuarioItem']['retirou'] == 1 && $itemPosCheckout['CheckoutUsuarioItem']['pos_checkout'] == 1){
                    $atendente = $this->Usuario->findById($itemPosCheckout['CheckoutUsuarioItem']['atendente_id']);
                    $itens[] = array(
                        'tipo' => 'pós checkout',
                        'atendente' => $atendente['Usuario']['nome'],
                        'data_cadastro' => date('d/m/Y &\a\g\r\a\v\e;\s H:i:s', strtotime($itemPosCheckout['CheckoutUsuarioItem']['data_cadastro'])),
                        'descricao' => $descricao,
                        'id' => $formando["ViewFormandos"]['id']
                    );
                }
            }
            $this->set('itens',$itens);
        }
    }
    
    function atendimento_imprimir_compra_pos_checkout($protocoloId){
        $this->layout = 'metro/externo';
        $protocolo = $this->Protocolo->read(null,$protocoloId);
        if(!empty($protocolo)) {
            $this->loadModel('CampanhasUsuario');
            $this->loadModel('CheckoutFormandoPagamento');
            $this->loadModel('CheckoutUsuarioItem');
            $this->set('protocolo', $protocolo);
            $formando = $this->ViewFormandos->read(null,$protocolo['Protocolo']['usuario_id']);
            $this->set('formando', $formando["ViewFormandos"]);
            $turma = $this->Turma->read(null,$formando['ViewFormandos']['turma_id']);
            $this->set('turma', $turma);
            $atendente = $this->Usuario->read(null,$protocolo['Protocolo']['usuario_criador']);
            $this->set('atendente', $atendente);
            $date = date('Y-m-d H:i');
            $itensPosCheckout = $this->CheckoutUsuarioItem->find('all', array(
                'conditions' => array(
                    'CheckoutUsuarioItem.protocolo_id' => $protocolo['Protocolo']['id'],
                    'CheckoutUsuarioItem.retirou' => 1,
                    'CheckoutUsuarioItem.pos_checkout' => 1,
                    'CheckoutUsuarioItem.data_cadastro LIKE' => "%{$date}%"
                )
            ));
            $this->CheckoutFormandoPagamento->unbindModelAll();
            $pagamentosCheckout = $this->CheckoutFormandoPagamento->find('all', array(
                'conditions' => array(
                    'CheckoutFormandoPagamento.protocolo_id' => $protocolo['Protocolo']['id'],
                    'CheckoutFormandoPagamento.data_cadastro LIKE' => "%{$date}%"
                )
            ));
            $this->set('pagamentosCheckout', $pagamentosCheckout);
            foreach ($itensPosCheckout as $itemPosCheckout) {
                $descricao = "{$itemPosCheckout['CheckoutUsuarioItem']['quantidade']} " .
                        "{$itemPosCheckout['CheckoutItem']['titulo']} comprado(s) pós checkout";
                if ($itemPosCheckout['CheckoutUsuarioItem']['retirou'] == 1 && $itemPosCheckout['CheckoutUsuarioItem']['pos_checkout'] == 1){
                    $itens[] = array(
                        'tipo' => 'pós checkout',
                        'descricao' => $descricao,
                        'id' => $formando["ViewFormandos"]['id']
                    );
                }
            }
            $this->set('itens',$itens);
        }
    }

    function formando_checkout_imprimir($protocolo) {
        $this->layout = 'impressao';
        $this->formando_checkout($protocolo);
        $this->Session->delete('CheckoutItensRetirar');
    }

    function atendimento_checkout_imprimir($protocolo) {
        $this->formando_checkout_imprimir($protocolo);
    }

    function planejamento_checkout_imprimir($protocolo) {
        $this->formando_checkout_imprimir($protocolo);
    }

}
