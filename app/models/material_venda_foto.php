<?php

class MaterialVendaFoto extends AppModel {

    var $name = 'MaterialVendaFoto';
    var $useTable = 'material_venda_fotos';
    var $belongsTo = 'MaterialVenda';
    var $order = "ordem";
    
    function afterSave($created) {
        parent::afterSave($created);
        if($created && isset($this->data["MaterialVendaFoto"]['src']) && isset($this->data["MaterialVendaFoto"]['ext'])) {
            $path = WWW_ROOT . "upload/material_venda_fotos/{$this->id}/";
            $salvou = false;
            if(mkdir($path,0777,true)) {
                $file = "{$path}default.{$this->data["MaterialVendaFoto"]['ext']}";
                if ($f = fopen($file, 'a'))
                    if (fwrite($f, base64_decode($this->data["MaterialVendaFoto"]['src'])))
                        $salvou = true;
            }
            if(!$salvou) $this->delete($this->id);
        }
        return true;
    }
    
    function afterDelete() {
        parent::afterDelete();
        $path = WWW_ROOT . "upload/material_venda_fotos/{$this->id}/";
        exec("rm -rf $path");
    }

}

?>