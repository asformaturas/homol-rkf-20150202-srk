<?php

class Item extends AppModel {

    var $name = 'Item';
    var $useTable = 'itens';
    //var $hasMany = array('Assunto');
    var $actsAs = array('Containable');
    var $grupos = array(
        'comercial' => 'Comercial',
        'planejamento' => 'Planejamento',
        'atendimento' => 'Atendimento');

    /*
     * Tabela no banco está assim:
     * id INT(10)
     * area ENUM('comercial', 'planejamento', 'atendimento')
     * nome VARCHAR(45)
     * descricao TEXT
     */
    var $validate = array(
        'nome' => array(
            'vazio' => array(
                'rule' => 'notEmpty',
                'message' => 'Preencha o nome.'
            )
        )
    );
    
    function obterPorTurmaId($turma_id) {
        return $this->query("SELECT DISTINCT Item.* FROM itens as Item INNER JOIN assuntos ON Item.id = assuntos.item_id " .
                        "WHERE assuntos.turma_id = " . $turma_id . " order by Item.grupo desc");
    }
    
    function obterPorTurmaEGrupo($turma_id,$grupo) {
        return $this->query("SELECT DISTINCT Item.* FROM itens as Item INNER JOIN assuntos ON Item.id = assuntos.item_id " .
                        "WHERE assuntos.turma_id = $turma_id and Item.grupo = '$grupo' order by Item.grupo desc");
    }
    
    function obterNaoResolvidosPorTurmaId($turma_id) {
        return $this->query("SELECT DISTINCT Item.* FROM itens as Item INNER JOIN assuntos ON Item.id = assuntos.item_id " .
                        "WHERE assuntos.turma_id = " . $turma_id .
                        " and assuntos.resolvido = 0" . " order by Item.grupo desc");
    }

}

?>