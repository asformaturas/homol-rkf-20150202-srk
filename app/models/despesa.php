<?php

App::import('Helper', 'Xml');

class Despesa extends AppModel {
	var $name = 'Despesa';
	
	var $useTable = 'despesas';
	
    var $hasMany = array(
        'DespesaPagamento' =>
			array(
                'className'              => 'DespesaPagamento',
                'joinTable'              => 'despesas_pagamentos',
                'foreignKey'             => 'despesa_id',
                'unique'                 => true,
			)
	);
	
	const MULTA_PERCENTUAL = 10 ; // Aplicar multa de 10% em despesas atrasadas
	const MULTA_POR_DIA = 0.10 ;

	public function getBoleto($despesa_id, $usuario){

        $conditions = [
            'Despesa.id' => $despesa_id,
        ];

        // if(in_array($usuario['nivel'], ['comissao', 'formando'])){
        //     $conditions['Usuario.id'] = $usuario['id'];
        // }

        return $this->find('first',[
            'fields' => '*',
            'conditions' => $conditions,
            'joins' => [
                [
                    'table' => 'vw_formandos',
                    'alias' => 'ViewFormando',
                    'type' => 'INNER',
                    'conditions' => 'ViewFormando.id = Despesa.usuario_id'
                ],
                [
                    'table' => 'turmas_usuarios',
                    'alias' => 'TurmasUsuario',
                    'type' => 'INNER',
                    'conditions' => 'TurmasUsuario.usuario_id = ViewFormando.id'
                ],
                [
                    'table' => 'turmas',
                    'alias' => 'Turma',
                    'type' => 'INNER',
                    'conditions' => 'Turma.id = TurmasUsuario.turma_id'
                ],
                [
                    'table' => 'despesas_pagamentos',
                    'alias' => 'DespesasPagamento',
                    'type' => 'LEFT',
                    'conditions' => 'DespesasPagamento.despesa_id = Despesa.id'
                ],
                [
                    'table' => 'pagamentos',
                    'alias' => 'Pagamento',
                    'type' => 'LEFT',
                    'conditions' => 'Pagamento.id = DespesasPagamento.pagamento_id'
                ],
            ]
        ]);

    }

    public function getDataDespesaBoleto( $conditions )
    {

		$this->Pagamento = ClassRegistry::init('Pagamento');

        $despesa = $this->find('first',[
            'fields' => '*',
            'conditions' => $conditions,
            'joins' => [
                [
                    'table' => 'vw_formandos',
                    'alias' => 'ViewFormando',
                    'type' => 'INNER',
                    'conditions' => 'ViewFormando.id = Despesa.usuario_id'
                ],
                [
                    'table' => 'turmas_usuarios',
                    'alias' => 'TurmasUsuario',
                    'type' => 'INNER',
                    'conditions' => 'TurmasUsuario.usuario_id = ViewFormando.id'
                ],
                [
                    'table' => 'turmas',
                    'alias' => 'Turma',
                    'type' => 'INNER',
                    'conditions' => 'Turma.id = TurmasUsuario.turma_id'
                ],
                [
                    'table' => 'despesas_pagamentos',
                    'alias' => 'DespesasPagamento',
                    'type' => 'LEFT',
                    'conditions' => 'DespesasPagamento.despesa_id = Despesa.id'
                ],
                [
                    'table' => 'pagamentos',
                    'alias' => 'Pagamento',
                    'type' => 'LEFT',
                    'conditions' => 'Pagamento.id = DespesasPagamento.pagamento_id'
                ],
            ]
        ]);

        $despesa['Turma']['juros_mora'] = self::RealToDb(str_replace('%', '', $despesa['Turma']['juros_mora']));
        $despesa['Turma']['multa_por_atraso'] =  str_replace('%', '', $despesa['Turma']['multa_por_atraso']);

        if(empty($despesa))
        {
            return [];
        }        

        $dados_boleto["data_documento"] = date("d/m/Y"); // Data de emissão do Boleto
        $dados_boleto["data_processamento"] = date("d/m/Y"); // Data de processamento do boleto (opcional)
        $dados_boleto["quantidade"] = NULL;
        $dados_boleto["valor_unitario"] = "";
        $dados_boleto["aceite"] = "N";
        $dados_boleto["especie"] = "R$";
        $dados_boleto["especie_doc"] = NULL;
        $dados_boleto["endereco1"] = "";
        $dados_boleto["endereco2"] = "";

        $dados_boleto['valor_cobrado'] = number_format($despesa['Despesa']['valor'], 2, ',', '');
        $dados_boleto['valor_boleto'] = $dados_boleto['valor_cobrado'];
        $dados_boleto['valor_multa'] = 0;
        $dados_boleto['turma_id'] = $despesa['Turma']['id'];
        $dados_boleto['codigo_formando'] = $despesa['Turma']['id'] . '' . $despesa['ViewFormando']['codigo_formando'];
        
        $dados_boleto['ordem_boleto'] = $this->Pagamento->find('first', [
            'fields' => [
                'SUBSTRING(MAX(Pagamento.codigo), 10) as ordem_boleto'
            ],
            'conditions' => [
                'Pagamento.usuario_id' => $despesa['ViewFormando']['id']
            ]
        ]);

        $dados_boleto['ordem_boleto'] = $dados_boleto['ordem_boleto'][0]['ordem_boleto'] + 1;

        $dados_boleto['numero_documento'] = str_pad($despesa['ViewFormando']['id'], 5, 0, STR_PAD_RIGHT) . str_pad($dados_boleto['ordem_boleto'], 3, 0, STR_PAD_LEFT);

        $dados_boleto['nosso_numero'] = str_pad($despesa['ViewFormando']['id'], 5, 0, STR_PAD_RIGHT) . str_pad($dados_boleto['ordem_boleto'], 3, 0, STR_PAD_LEFT);
        

        $dados_boleto['sacado'] = $despesa['ViewFormando']['nome'];
        $dados_boleto['pedido'] = $despesa['Despesa']['id'];
        $dados_boleto['despesa_id'] = $despesa['Despesa']['id'];
        $dados_boleto['usuario_id'] = $despesa['ViewFormando']['id'];
        
        $despesa['Despesa']['data_vencimento'] = $this->dataValida($despesa['Despesa']['data_vencimento']);

        $data_atual = date('Y-m-d');
        // $data_atual = '2018-02-12';

        $vencido = (strtotime($data_atual) > strtotime($despesa['Despesa']['data_vencimento']));

        if( $despesa['Turma']['juros_mora'] >= 1 ){
            $despesa['Turma']['juros_mora'] = substr($despesa['Turma']['juros_mora'], 0, 4);
        }

        $dados_boleto['vencido'] = false;
        if($vencido){

            $dia_vencimento = strftime('%w', strtotime($despesa['Despesa']['data_vencimento']));
            $dia_atual = strftime('%w', strtotime($data_atual));

            $diferenca_dias = (strtotime($data_atual) - strtotime($despesa['Despesa']['data_vencimento'])) / 86400;

            $mora = 0;
            $multa = $despesa['Turma']['multa_por_atraso'];

            $mora = $diferenca_dias * $despesa['Turma']['juros_mora'];

            $valor_cobrado = $despesa['Despesa']['valor'];
            $multa = number_format($despesa['Despesa']['valor'] * $multa / 100, 2);

            if(in_array($dia_vencimento, [0,6]) && in_array($dia_atual, [0,1,6]) && $diferenca_dias <= 2){
                $dados_boleto['valor_cobrado'] = number_format($valor_cobrado, 2, ',', '');
            }else{
                $dados_boleto['valor_multa'] = number_format($multa + $mora, 2, ',', '');
            }    

            $dados_boleto['valor_boleto'] = $dados_boleto['valor_cobrado'];           
            $dados_boleto['vencido'] = true;

            $despesa['Despesa']['data_vencimento'] = $data_atual;
        }

        $dados_boleto['valor_cobrado'] = number_format($this->RealToDb($dados_boleto['valor_boleto']) + $this->RealToDb($dados_boleto['valor_multa']), 2, ',', '');

        $dados_boleto['data_vencimento'] = date("Y-m-d", strtotime($despesa['Despesa']['data_vencimento']));

        $despesa['ContasEmpresa'] = [
        	'banco_id' => 59,
        	'cedente' => 'RK EVENTOS E FORMATURAS LTDA',
        	'cpf_cnpj' => 09551738000111 ,
        	'codigo_cliente' => 34770,
        	// 'agencia' => 1324,
        	// 'agencia_dv' => 2,
        	// 'conta' => 3477,
        	// 'conta_corrente' => '5967-6',
            'agencia' => 1787,
            'agencia_dv' => 0,
            'conta' => 3477,
            'conta_corrente' => '255967-6',
        	'conta_dv' => 0,
        	'carteira' => 16,
        	'carteira_descricao' => 'CobranÃ§a Simples',
        	'especie' => 'R$',
        	'especie_doc' => '',
        	'quantidade' => '',
        	'instrucoes_1' => 'Após o vencimento aplicar multa de %s%% sobre o valor.',
        	'instrucoes_2' => 'Após o vencimento aplicar juros mora de R$ %s ao dia.',
        	'aceite' => 'N',
        	'endereco' => 'Avenida Senador Casemiro da Rocha, 1000 - Planalto Paulista',
        	'cidade' => 'São Paulo',
        	'uf' => 'SP',

        ];

        $dados_boleto = array_merge($dados_boleto, $despesa['ContasEmpresa']);
        $dados_boleto['nome_configuracao'] = "Banco Bradesco S.A.";
        $dados_boleto["codigo_cliente"] = $despesa['ContasEmpresa']['codigo_cliente'];
        $dados_boleto["codigo_cedente"] = $despesa['ContasEmpresa']['codigo_cliente'];
        $dados_boleto["carteira"] = $despesa['ContasEmpresa']['carteira'];
        $dados_boleto["ponto_venda"] = $despesa['ContasEmpresa']['agencia'];
        $dados_boleto["codigo_banco"] = 237;
        $dados_boleto["carteira_descricao"] = strtoupper($despesa['ContasEmpresa']['carteira_descricao']);
        $dados_boleto["identificacao"] = $despesa['ContasEmpresa']['cedente'];
        $dados_boleto['demonstrativo1'] = "BOLETO REFERENTE A EVENTOS DE FORMATURA.";

        $dados_boleto['demonstrativo2'] = sprintf($despesa['ContasEmpresa']['instrucoes_1'], $despesa['Turma']['multa_por_atraso']);
        $dados_boleto['demonstrativo3'] = "";
        $dados_boleto['instrucoes_1'] = sprintf($despesa['ContasEmpresa']['instrucoes_2'], $despesa['Turma']['juros_mora']);
        $dados_boleto['instrucoes3'] = "";
        $dados_boleto['instrucoes4'] = "";
        
        
        $dados_boleto["cpf_cnpj"] = "CNPJ: 18.405.543/0001-54";
        $dados_boleto["endereco"] = $despesa['ContasEmpresa']['endereco'];
        $dados_boleto["cidade_uf"] = $despesa['ContasEmpresa']['cidade']." / ".$despesa['ContasEmpresa']['uf'];
        $dados_boleto["mora"] = $despesa['Turma']['juros_mora'];
        $dados_boleto["multa"] =  $despesa['Turma']['multa_por_atraso'];

        return $dados_boleto;
    }

    /**
    * Verifica se é final de semana ou feriado
    */
    public function dataValida( $data_despesa )
    {
        if($this->isFeriado($data_despesa)){
            $data_despesa = date("Y-m-d", strtotime($data_despesa . "+1 days"));
        }

        $dia_da_semana = new DateTime( $data_despesa );
        $dia_da_semana = $dia_da_semana->format('w');

        // Se fds 
        if( $dia_da_semana == 6 || $dia_da_semana == 0 ){
            if($dia_da_semana == 6){
                $data_despesa = date("Y-m-d", strtotime($data_despesa. "+2 days"));
            }elseif($dia_da_semana == 0){
                $data_despesa = date("Y-m-d", strtotime($data_despesa. "+1 days"));
            }

            if( $this->isFeriado($data_despesa) ){
                $data_despesa = date("Y-m-d", strtotime($data_despesa. "+1 days"));
            }
        }

        return $data_despesa;
    }

    public function getFeriados($uf = 'SP'){

        $feriados = [];

        $url = 'https://api.calendario.com.br/?ano='.date('Y').'&estado=SP&cidade=SAO_PAULO&token=ZGF5Z2xvci5jYW1wb3NAZXZlbnRvcy5hcyZoYXNoPTE0MjU1MjcxNQ==';

        $xml = simplexml_load_file($url);
    	
        foreach((array)$xml['events']['event'] as $feriado){

            $feriados[$feriado['date']] = $feriado['name'];
        }

        return $feriados;
    }

    public function isFeriado($dataDespesa){

        $feriados = $this->getFeriados();

        $isFeriado = false;

        foreach($feriados as $date => $name){
            
            if($date == date('Y/m/d', strtotime($dataDespesa))){

                $isFeriado = true;
            }
        }

        return $isFeriado;
    }

    public function boletoValido($oldBoleto, $despesa, $valor_cobrado){

        if(!isset($oldBoleto['Boleto']) || empty($oldBoleto) || $oldBoleto['Pagamento']['url'] == ''){

            return false;
        }

        $currentDate = Date('Y-m-d', strtotime('now' . '-59 days'));
        if((strtotime($oldBoleto['Pagamento']['dt_vencimento']) < strtotime($currentDate))){
            return false;
        }

        if(strtotime(date('Y-m-d')) > $oldBoleto['Boleto']['data_vencimento'] && $oldBoleto['Boleto']['valor_boleto'] < 800){
            return false;
        }

        $scopusComProblemaInicio = strtotime( date('2018-03-09 00:00:00') );
        $scopusComProblemaFim  = strtotime( date('2018-03-09 11:30:00') );
        $dataDeCriacaoDoBoleto = strtotime( $oldBoleto['Pagamento']['data_documento'] );
        if( $dataDeCriacaoDoBoleto < $scopusComProblemaFim && $dataDeCriacaoDoBoleto > $scopusComProblemaInicio ){
            return false;
        }

        $dataMudou = (strtotime($oldBoleto['Pagamento']['despesa_vencimento']) != strtotime($despesa['Despesa']['data_vencimento']));

        if($dataMudou){
            return false;
        }

        $valorDiferente = ($oldBoleto['Pagamento']['valor_nominal'] != $this->RealToDb($valor_cobrado));
        $boletoVencido =  (strtotime($oldBoleto['Pagamento']['dt_vencimento']) < strtotime(date('Y-m-d')));
        if($valorDiferente && !$boletoVencido ){
            return false;
        }

        $oldBoleto['Turma']['juros_mora'] = self::RealToDb(str_replace('%', '', $oldBoleto['Turma']['juros_mora']));
        $oldBoleto['Turma']['multa_por_atraso'] =  str_replace('%', '', $oldBoleto['Turma']['multa_por_atraso']);

        $despesa['Turma']['juros_mora'] = self::RealToDb(str_replace('%', '', $despesa['Turma']['juros_mora']));
        $despesa['Turma']['multa_por_atraso'] =  str_replace('%', '', $despesa['Turma']['multa_por_atraso']);
        
        if( $oldBoleto['Turma']['multa_por_atraso'] != $despesa['Turma']['multa_por_atraso'] ){
            return false;
        }

        if($despesa['Despesa']['valor'] != $oldBoleto['Pagamento']['despesa_valor'] ){
            return false;
        }

        if($oldBoleto['Turma']['juros_mora'] != $despesa['Turma']['juros_mora']){
            return false;
        }

        // Data de corte, nesse dia viramos o boleto para registrado
        if($oldBoleto['Pagamento']['data_documento'] < '2018-02-15 11:00:00'){
            return false;
        }

        return true;
    }

    /**
     * Recupera o último boleto a partir da despesa id 
     */
    public function getOldBoletoByDespesaId($despesa, $valor_cobrado){
    
        $this->Pagamento = ClassRegistry::init('Pagamento');

        $oldBoleto = $this->Pagamento->find('first', [
            'fields' => '*',
            'conditions' => [
                'DespesasPagamento.despesa_id' => $despesa['Despesa']['id']
            ],
            'joins' => array(
                array(
                    "table" => "despesas_pagamentos",
                    "type" => "INNER",
                    "alias" => "DespesasPagamento",
                    "conditions" => array(
                        "DespesasPagamento.pagamento_id = Pagamento.id",
                    )
                ),
                array(
                    "table" => "turmas_usuarios",
                    "type" => "INNER",
                    "alias" => "TurmasUsuario",
                    "conditions" => array(
                        "TurmasUsuario.usuario_id = Pagamento.usuario_id",
                    )
                ),
                array(
                    "table" => "turmas",
                    "type" => "INNER",
                    "alias" => "Turma",
                    "conditions" => array(
                        "Turma.id = TurmasUsuario.turma_id",
                    )
                )
            ),
            'order' => [
                'Pagamento.id' => 'DESC'
            ]
        ]);

        if (!$this->boletoValido($oldBoleto, $despesa, $valor_cobrado)){

            return false;
        }

        return $oldBoleto;
    }

    public function numeroBoletoExists($nosso_numero){

        $this->Pagamento = ClassRegistry::init('Pagamento');

        $numeroBoletoExiste = $this->Pagamento->find('count', [
            'conditions' => [
                'Pagamento.nosso_numero LIKE ' => '%'.$nosso_numero.'%'
            ]
        ]);

        // while( $numeroBoletoExiste ){

        //     $nosso_numero = ++$nosso_numero;  

        //     $numeroBoletoExiste = $this->find('count', [
        //         'conditions' => [
        //             'Boleto.nosso_numero LIKE ' => '%'.$nosso_numero.'%'
        //         ]
        //     ]);
        // }

        // return $nosso_numero;

        return $numeroBoletoExiste;
    }

    public function RealToDb($real = null) {

        $number = str_replace('R$','', $real);
        $number = str_replace('.','',$number);
        $number = str_replace(',','.',$number);
        $number = trim($number);

        return $number;
    }

    public function OnlyNumbers($field = null) {

        return preg_replace("/[^0-9]/", '', $field);
    }

    public function Cnpj($cnpj = null) {

        return is_numeric($cnpj) ? self::_mask($cnpj, '##.###.###/####-##') : $cnpj ;
    }

    public function Cpf($cpf = null) {

        return is_numeric($cpf) ? self::_mask($cpf, '###.###.###-##') : $cpf;
    }

    private static function _mask($val, $mask) {

        $maskared = '';
        $k = 0;

        for($i = 0; $i <= strlen($mask)-1; $i++) {

            if($mask[$i] == '#'){
                if(isset($val[$k]))
                    $maskared .= $val[$k++];
            }
            else {
                if(isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }

        return $maskared;
    }
}
?>