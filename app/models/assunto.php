<?php

class Assunto extends AppModel {

    var $name = 'Assunto';
    var $belongsTo = array('Item', 'Turma');
    var $hasMany = array('Mensagem');

    /*
     * Tabela no banco está assim:
     * id INT(10)
     * assunto_id INT(10)
     * turma_id INT(10)
     * usuario_id INT(10)
     * formando_id INT(10)
     */
    var $pendencias = array(
        'Rk' => 'RK',
        'Comissao' => 'Comissão',
        'Informativo' => 'Informativo'
    );
    
    var $atendimento = array(
        'sugestao' => "Sugestão",
        'reclamacao' => "Reclamação",
        'outros' => "Outros"
    );
    
    var $validate = array(
        'nome' => array(
            'unique' => array(
                "rule" => array(
                    "checkUnique", array("nome", "turma_id", "item_id")),
                "message" => "Assunto já existente dentro deste item."
            ),
            'required' => array(
                'rule' => 'notEmpty',
                'message' => 'Campo obrigatório'
            )
        )
    );
    var $recursive = 2;

}

?>