<?php
class Universidade extends AppModel {
    var $name = 'Universidade';
	var $hasMany = 'Faculdade';
	var $actsAs = array('Containable');
	
	/*
	 * Tabela no banco está assim:
	 * id INT
	 * nome VARCHAR(100)
	 * sigla VARCHAR(10)
	 */
	 
	var $validate = array(
		'nome' => array(
			'vazio' => array(
                'rule' => 'notEmpty',
                'message' => 'Preencha o nome.'
            )
		)	
    );
}
?>