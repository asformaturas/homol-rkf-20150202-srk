<?php

class Mensagem extends AppModel {

    var $name = 'Mensagem';
    var $useTable = 'mensagens';
    var $hasAndBelongsToMany = array(
        'Arquivo' =>
        array(
            'className' => 'Arquivo',
            'joinTable' => 'arquivos_mensagens',
            'foreignKey' => 'mensagem_id',
            'associationForeignKey' => 'arquivo_id',
            'unique' => true
        )
    );
    
    var $belongsTo = array('Usuario');
    var $actsAs = array('Containable');

    /*
     * Tabela no banco está assim:
     * id INT(10)
     * assunto_id INT(10)
     * turma_id INT(10)
     * usuario_id INT(10)
     * formando_id INT(10)
     */
    var $recursive = 2;
    var $validate = array(
//		'assunto_id' => array(
//			'required' => array(
//				'rule' => 'required',
//				'message' => 'Mensagem deve ter assunto.'
//			)
//		),
        'texto' => array(
            'minLength' => array(
                'rule' => array('minLength', 10),
                'message' => 'Mensagem deve possuir texto.'
            )
        )
    );
    var $pendencias = array(
        'Rk' => 'RK',
        'Comissao' => 'Comissão',
        'Informativo' => 'Informativo'
    );
    var $pendencias_comercial = array(
        'Rk' => 'RK',
        'Informativo' => 'Informativo'
    );

}

?>