<?php

class EventoMapaLocal extends AppModel {

    var $name = 'EventoMapaLocal';
    var $useTable = 'evento_mapa_locais';
    var $belongsTo = array(
        'EventoMapaColuna',
        'Formando' => array(
            'className' => 'ViewFormandos',
            'foreignKey' => 'usuario_id'
        )
    );

}

?>