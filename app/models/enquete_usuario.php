<?php

class EnqueteUsuario extends AppModel {

    var $name = 'EnqueteUsuario';
    var $belongsTo = array('Usuario','Enquete');
    var $hasMany = array('EnqueteUsuarioResposta');
    
    function getResumoTotais($enqueteId){
        $totais = $this->query("SELECT
            (SELECT count(id) FROM enquete_usuarios 
            WHERE enquete_id = {$enqueteId} AND adiou = 1) AS adiou,
            (SELECT count(id) FROM enquete_usuarios 
            WHERE enquete_id = {$enqueteId} AND finalizou = 1) AS finalizou,
            (SELECT count(id) FROM enquete_usuarios 
            WHERE enquete_id = {$enqueteId} AND rejeitou = 1) AS rejeitou");
        return $totais[0][0];
    }
    
    function getResumoPerguntas($enqueteId){
        $perguntas = $this->query("SELECT EnquetePerguntas.id AS pergunta_id, EnquetePerguntas.texto AS pergunta_texto, EnqueteAlternativas.id AS 
            alternativa_id, EnqueteAlternativas.texto AS alternativa, COUNT(EnqueteUsuarioRespostas.id) AS total_respostas FROM enquete_usuario_respostas EnqueteUsuarioRespostas
            INNER JOIN enquete_alternativas EnqueteAlternativas ON EnqueteUsuarioRespostas.enquete_alternativa_id = 
            EnqueteAlternativas.id INNER JOIN enquete_perguntas EnquetePerguntas ON EnqueteAlternativas.enquete_pergunta_id = 
            EnquetePerguntas.id and EnquetePerguntas.enquete_id = {$enqueteId}
            GROUP BY EnqueteAlternativas.id
            ORDER BY EnquetePerguntas.id, EnqueteAlternativas.id desc");
        $array = array();
        foreach($perguntas as $pergunta){
            $perguntaId = $pergunta['EnquetePerguntas']['pergunta_id'];
            $perguntaTexto = $pergunta['EnquetePerguntas']['pergunta_texto'];
            $alternativaId = $pergunta['EnqueteAlternativas']['alternativa_id'];
            $array[$perguntaId]['pergunta_texto'] = $perguntaTexto;
            $array[$perguntaId]['alternativas'][$alternativaId]['alternativa_texto'] = $pergunta['EnqueteAlternativas']['alternativa'];
            $array[$perguntaId]['alternativas'][$alternativaId]['total_respostas'] = $pergunta[0]['total_respostas'];
        }
        return $array;
    }
    
    function getResumoPorFormando($enqueteId){
        $resumo = $this->query("SELECT ViewFormandos.id, ViewFormandos.codigo_formando, ViewFormandos.nome, EnquetePerguntas.texto, EnqueteAlternativas.texto, EnqueteUsuarios.rejeitou, EnqueteUsuarios.observacoes FROM enquete_usuario_respostas as EnqueteUsuarioRespostas
            INNER JOIN enquete_alternativas EnqueteAlternativas ON EnqueteUsuarioRespostas.enquete_alternativa_id = EnqueteAlternativas.id
            INNER JOIN enquete_perguntas EnquetePerguntas ON EnqueteAlternativas.enquete_pergunta_id = EnquetePerguntas.id AND EnquetePerguntas.enquete_id = {$enqueteId}
            INNER JOIN enquete_usuarios EnqueteUsuarios ON EnqueteUsuarios.id = EnqueteUsuarioRespostas.enquete_usuario_id
            INNER JOIN vw_formandos ViewFormandos ON ViewFormandos.id = EnqueteUsuarios.usuario_id
            ORDER BY ViewFormandos.codigo_formando, EnquetePerguntas.id");
        $array = array();
        foreach($resumo as $i => $r){
            $uid = $r['ViewFormandos']['id'];
            $array[$uid]['EnqueteUsuarios']['codigo_formando'] = $r['ViewFormandos']['codigo_formando'];
            $array[$uid]['EnqueteUsuarios']['nome'] = $r['ViewFormandos']['nome'];
            $array[$uid]['EnqueteUsuarios']['rejeitou'] = $r['EnqueteUsuarios']['rejeitou'];
            $array[$uid]['EnqueteUsuarios']['observacoes'] = $r['EnqueteUsuarios']['observacoes'];
            $array[$uid]['EnquetePerguntas'][] = $r['EnquetePerguntas']['texto'];
            $array[$uid]['EnqueteAlternativas'][] = $r['EnqueteAlternativas']['texto'];
        }
        return $array;
    }
    
}