<?php
class DespesaPagamento extends AppModel {
	var $name = 'DespesaPagamento';
	var $useTable = 'despesas_pagamentos';
	
	var $belongsTo = array(
		'Despesa', 'Pagamento'
	);
}