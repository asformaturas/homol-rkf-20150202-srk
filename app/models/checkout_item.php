<?php

class CheckoutItem extends AppModel {
	var $nome = "CheckoutItem";
	var $useTable = "checkout_itens";
	
	var $validate = array(
		'item_por_pessoa' => array('rule' => 'numeric','message' => 'Digite quantidade de itens por pessoa'),
		'valor' => array('rule' => array('decimal',2),'message' => 'Digite o valor do convite. Ex 100,00'),
		'quantidade' => array('rule' => 'numeric','message' => 'Digite a quantidade de itens pra venda')
	);
}