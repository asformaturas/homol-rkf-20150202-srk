<?php

class FormandoProfile extends AppModel {

    var $name = 'FormandoProfile';
    var $useTable = 'formandos_profile';
    var $belongsTo = array('Usuario', 'CursoTurma');
    var $actsAs = array('Containable','Multivalidatable');
    
    var $recursive = 2;
    
    var $fields = array(
    		'tam_camiseta' => array(
    				'PP'=>'PP',
    				'P'=>'P',
    				'M' =>'M',
    				'G' => 'G',
    				'GG' => 'GG'
    		),
                'tam_moleton' => array(
                                        'PP'=>'PP',
                                        'P'=>'P',
                                        'M' =>'M',
                                        'G' => 'G',
                                        'GG' => 'GG'
                        )
    );
    
    // Validate usado na migração
    /*
    var $validate_migracao = array(
        'curso_turma_id' => array(
            'rule' => 'numeric',
            'message' => 'Escolha uma opção da lista de cursos.'

        ),
        'parcelas_adesao' => array(
        	'rule' => 'numeric',
        	'required' => false,
        	'messagem' => 'Opção de parcelamento inválida.'
        ),
        'sexo' => array(
            'rule' => array('inList', array('M', 'F', null)),
            'message' => 'Escolha o seu genero sexual'
        ),
        'rg' => array(
            'numeric' => array(
                'rule' => 'alphanumeric',
                'required' => false,
                'allowEmpty' => true,
                'message' => 'Preencha seu RG somente com números e letras.'
            )
        ),
        'cpf' => array(
            'numeric' => array(
                'rule' => 'numeric',
                'message' => 'Preencha seu CPF somente com numeros.'
            )
        ),
        'tel_residencial' => array(
            'rule' => array('minLength', 7),
            'required' => false,
            'allowEmpty' => true,
            'message' => 'Telefone Residencial deve possuir no mínimo 7 caracteres'
        ),
        'tel_celular' => array(
            'rule' => array('minLength', 7),
            'required' => false,
            'allowEmpty' => true,
            'message' => 'Telefone Celular deve possuir no mínimo 7 caracteres'
        ),
        'numero_havaiana' => array(
            'rule' => array('inList', array(19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46, null)),
            'required' => false,
            'message' => 'Escolha um número de calçado adequado.'
        ),
        'tam_camiseta' => array(
            'rule' => array('inList', array('PP','P','M','G','GG', null)),
            'required' => false,
            'message' => 'Escolha o tamanho de camiseta adequadoa'
        ),
        'end_numero' => array(
            'rule' => 'numeric',
            'required' => false,
            'allowEmpty' => true,
            'message' => 'Preencha o número de seu endereço ou 0 caso não haja numeração'
        ),

        'end_uf' => array(
            'rule' => array('inList', array('AC','AL','AP','AM','BA','CE','DF','ES','GO','MA','MT','MS','MG','PA','PB','PR','PE','PI','RJ','RN','RS','RO','RR','SC','SP','SE','TO')),
            'required' => false,
            'message' => 'Selecione o estado de seu endereço'
        ),
        'end_cep' => array(
            'rule' => 'numeric',
            'required' => false,
            'allowEmpty' => true,
            'message' => 'Preencha o CEP do endereço somente com número'
        )
    );
    */
    
    // Validate Original ( retornar para ele após a migração)
    var $validate = array(
        'parcelas' => array(
        	'rule' => 'numeric',
        	'required' => true,
        	'messagem' => 'Opção de parcelamento inválida.'
        ),
        'forma_pagamento' => array(
        	'rule' => array('inList', array('boleto_online', 'boleto_impresso','cheque')),
            'message' => 'Forma de pagamento inválida.'
        ),
        'data_nascimento' => array(
            'rule' => array('date', 'ymd'),
            'required' => true,
            'message' => 'Escolha uma data válida'
        ),
        'sexo' => array(
            'rule' => array('multiple', array('in' => array('M', 'F'))),
            'required' => true,
            'message' => 'Escolha masculino ou feminino.'
        ),
        'tel_residencial' => array(
            'rule' => array('minLength', 7),
            'required' => true,
            'message' => 'Telefone Residencial deve possuir no mínimo 7 caracteres'
        ),
        'tel_celular' => array(
            'rule' => array('minLength', 7),
            'required' => true,
            'message' => 'Telefone Celular deve possuir no mínimo 7 caracteres'
        ),
        /*
        'numero_havaiana' => array(
            'rule' => array('inList', array(19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46)),
            'required' => true,
            'allowEmpty' => false,
            'message' => 'Escolha um número de calçado adequado.'
        ),
        'tam_camiseta' => array(
            'rule' => array('inList', array('PP','P','M','G','GG')),
            'required' => true,
            'message' => 'Escolha o tamanho de camiseta adequadoa'
        ),
        */
        'end_rua' => array(
            'rule' => 'notEmpty',
            'required' => true,
            'message' => 'Preencha o nome da rua de seu endereço.'
        ),
        'end_bairro' => array(
            'rule' => 'notEmpty',
            'required' => true,
            'allowEmpty' => false,
            'message' => 'Preencha o bairro de seu endereço'
        ),
        'end_cidade' => array(
            'rule' => 'notEmpty',
            'required' => true,
            'message' => 'Preencha a cidade de seu endereço'
        ),
        'end_uf' => array(
            'rule' => array('inList', array('AC','AL','AP','AM','BA','CE','DF','ES','GO','MA','MT','MS','MG','PA','PB','PR','PE','PI','RJ','RN','RS','RO','RR','SC','SP','SE','TO')),
            'required' => true,
            'message' => 'Selecione o estado de seu endereço'
        ),
        'end_cep' => array(
            'rule' => 'numeric',
            'required' => true,
            'message' => 'Preencha o CEP do endereço somente com número'
        )
    );

	/**
	 * Custom validation rulesets
	 */
	var $validationSets = array(
	    'comissao_editar' => array(
	        'tel_residencial' => array(
	            'rule' => array('minLength', 7),
	            'required' => true,
	            'message' => 'Telefone Residencial deve possuir no mínimo 7 caracteres'
	        ),
	       	'tel_celular' => array(
	            'rule' => array('minLength', 7),
	            'required' => true,
	            'message' => 'Telefone Celular deve possuir no mínimo 7 caracteres'
	        ),
	        'tel_comercial' => array(
	            'rule' => array('minLength', 7),
	            'required' => false,
	            'allowEmpty' => true,
	            'message' => 'Telefone Comercial deve possuir no mínimo 7 caracteres'
	        ),
	        'rg' => array(
	            'between' => array(
	                'rule' => array('between', 5, 15),
	                'message' => 'O preenchimento do RG deve possuir de 5 a 15 numeros.'
	            ),
	            'numeric' => array(
	                'rule' => 'numeric',
	                'required' => true,
	                'message' => 'Preencha seu RG somente com numeros.'
	            )
	        )
	    )
	);


}

?>