<?php

class Campanha extends AppModel {
	var $name = 'Campanha';
	//var $useTable = 'itens';
	// 
	//var $hasMany = array('CampanhaItens');
	//var $actsAs = array('Containable');
	
	/*
	 * Tabela no banco está assim:
	  `id` int(11) NOT NULL,
	  `nome` varchar(70),
	  `descricao` text,
          `max_parcelas` int(11) NOT NULL,
          `data_inicio` DATETIME,
          `data_fim` DATETIME
         * 
	 */
	
	var $hasAndBelongsToMany = array(
        'Extra' =>
        array(
            'className' => 'Extra',
            'joinTable' => 'campanhas_extras',
            'foreignKey' => 'extra_id',
            'associationForeignKey' => 'campanha_id',
            'unique' => false,
        ),
		'Usuario' =>
        array(
            'className' => 'Usuario',
            'joinTable' => 'campanhas_usuarios',
            'foreignKey' => 'usuario_id',
            'associationForeignKey' => 'campanha_id',
            'unique' => false,
        )
    );
	
	var $validate = array(
		'nome' => array(
			'vazio' => array(
                'rule' => 'notEmpty',
                'message' => 'Preencha o nome.'
            )
		),

                'max_parcelas' => array(
                        'numeric' => array(
                                'rule' => array('numeric'),
                                'message' => 'Máximo de parcelas deve ser número.'
                        ),
                        'comparison' => array(
                                'rule' => array('comparison', '>', 0),
                                'message' => 'Máximo de parcelas deve ser maior que zero.'
                        )
                ),
                'data_inicio' => array(
                        'comparar' => array(
                                'rule' => array('comparar'),
                                'message' => 'Data inicial deve ser inferior à data final.'
                        )
                )
                
	);
        
        function comparar($check) {
            if($this->data['Campanha']['data_inicio'] < $check) {
                return true;
            }
            else {
                return false;
            }
        }
}
?>