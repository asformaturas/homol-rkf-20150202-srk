<?php

class TurmaMapa extends AppModel {

    var $name = 'TurmaMapa';
    var $belongsTo = array('Evento','Mapa');

    function bindTurma(){
    	$this->bindModel(
    		array(
    			'belongsTo' => array(
    				'Turma' => array(
    					'foreignKey' => false,
    					'conditions' => 'TurmaMapa.turma_id = Turma.id'
    				)
    			)
    		)
    	);
    }

    function bindEvento(){
        $this->bindModel(
            array(
                'belongsTo' => array(
                    'Evento' => array(
                        'foreignKey' => false,
                        'conditions' => 'TurmaMapa.evento_id = Evento.id'
                    )
                )
            )
        );
    }

    function mudar_status($turma_mapa_id, $status = 1){
        $this->id = $turma_mapa_id;

    	return $this->saveField('status', $status);
    } 
}

?>