<?php

class CheckoutUsuarioItem extends AppModel {

    var $nome = "CheckoutUsuarioItem";
    var $useTable = "checkout_usuario_itens";
    var $belongsTo = array(
        'CheckoutItem' => array(
            'className' => 'CheckoutItem',
            'foreignKey' => 'checkout_item_id'
        )
    );

}
