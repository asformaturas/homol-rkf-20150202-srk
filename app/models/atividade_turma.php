<?php

class AtividadeTurma extends AppModel {

    var $name = 'AtividadeTurma';

    var $belongsTo = array(
        'Atividade' => array(
            'className' => 'Atividade',
            'foreignKey' => 'atividade_id'
        ),
        'Turma' => array(
            'className' => 'Turma',
            'foreignKey' => 'turma_id'
        )
    );

}

?>