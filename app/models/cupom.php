<?php

class Cupom extends AppModel {
	var $name = 'Cupom';
	var $actsAs = array('Containable');
	
	var $belongsTo = array(
            'Rifa',
            'Usuario' => array(
                'className' => 'ViewFormandos'
            )
        );
	
	var $useTable = 'cupons';
}
?>
