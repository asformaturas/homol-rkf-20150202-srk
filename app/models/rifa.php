<?php

class Rifa extends Model {

    var $name = "Rifa";
    
    var $actsAs = array('Containable');
    var $hasMany = array(
        'Cupom' => array(
            'className' => 'Cupom',
            'foreignKey' => 'usuario_id',
            'unique' => false
        ),
        'RifaTurma' => array(
            'className' => 'RifaTurma',
            'foreignKey' => 'rifa_id',
            'unique' => false
        )
    );
    var $numero_maximo = 75000;

}

?>
