<?php

class Parceiro extends AppModel {
	var $name = 'Parceiro';
	
	var $actsAs = array('Containable');
	
	var $hasMany = array('Parceria', 'FotoParceiro');
	
	var $validate = array (
		'nome' => array(
			'vazio' => array(
				'rule' => 'notEmpty',
				'message' => 'Preencha o nome'
			)
		),
		
		'descricao' => array(
			'comprimento_minimo' => array(
				'rule' => array('minLength', 5),
				'message' => 'Preencha a descrição com no mínimo 5 caracteres'
			)
		)
	);
}

?>