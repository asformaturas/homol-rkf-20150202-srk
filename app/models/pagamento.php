<?php

class Pagamento extends AppModel {
	var $name = 'Pagamento';
	
	var $useTable = 'pagamentos';
	
    var $hasMany = array(
        'DespesaPagamento' =>
			array(
                'className'              => 'DespesaPagamento',
                'joinTable'              => 'despesas_pagamentos',
                'foreignKey'             => 'pagamento_id',
                'unique'                 => true,
			)
	);
	
	const TIPO_BOLETO = 'boleto';
	const TIPO_BOLETO_NAO_CADASTRADO = 'boleto_nao_cadastrado';
	const TIPO_BOLETO_ANTIGO_CONTRATO = 'boleto_antigo_contrato';
	const TIPO_BOLETO_ANTIGO_EXTRA_BAILE = 'boleto_antigo_extra_baile';
	const TIPO_BOLETO_ANTIGO_EXTRA_EVENTOS = 'boleto_antigo_extra_eventos';
	const TIPO_CHEQUE_EXTRA = 'cheque_extra';
	const TIPO_CHEQUE_CONTRATO = 'cheque_contrato';
	const TIPO_CHEQUE_PERDIDO = 'cheque_perdido';
	const TIPO_DINHEIRO = 'dinheiro';
	const TIPO_BOLETO_IGPM = 'boleto_igpm';
	const TIPO_BOLETO_DUPLICADO = 'boleto_duplicado';
	
	const STATUS_ABERTO = 'aberto';
	const STATUS_VENCIDO = 'vencido';
	const STATUS_PAGO = 'pago';
	const STATUS_CANCELADO = 'cancelado';

}
?>