<?php

class AtividadeUsuario extends AppModel {

    var $name = 'AtividadeUsuario';

    var $belongsTo = array(
        'Atividade' => array(
            'className' => 'Atividade',
            'foreignKey' => 'atividade_id'
        ),
        'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'usuario_id'
        )
    );

}

?>