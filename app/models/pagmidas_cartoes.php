<?php

class PagmidasCartoes extends Model {
	var $name = "PagmidasCartoes";
	var $useTable = 'pagmidas_cartoes';
	var $validate = array (
		'nome' => array(
			'rule' => 'notEmpty',
			'required' => true,
			'message' => 'Digite o nome do titular do cartão.'
		),
		'nome_cartao' => array(
			'rule' => 'notEmpty',
			'required' => true,
			'message' => 'Digite o nome como aparece no cartão.'
		),
		'sexo' => array(
			'rule' => array('multiple', array('in' => array('M', 'F'))),
			'required' => true,
			'message' => 'Escolha masculino ou feminino.'
		),
		'tel_residencial' => array(
			'rule' => array('minLength', 7),
			'required' => true,
			'message' => 'Telefone Residencial deve possuir no mínimo 7 caracteres'
		),
		'tel_celular' => array(
			'rule' => array('minLength', 7),
			'message' => 'Telefone Celular deve possuir no mínimo 7 caracteres'
		),
		'end_rua' => array(
			'rule' => 'notEmpty',
			'required' => true,
			'message' => 'Preencha o seu endereço.'
		),
		'end_numero' => array(
			'rule' => 'numeric',
			'required' => true,
			'message' => 'Preencha o número de seu endereço ou 0 caso não haja numeração'
		),
		'end_bairro' => array(
			'rule' => 'notEmpty',
			'required' => true,
			'allowEmpty' => false,
			'message' => 'Preencha o bairro de seu endereço'
		),
		'end_cidade' => array(
			'rule' => 'notEmpty',
			'required' => true,
			'message' => 'Preencha a cidade de seu endereço'
		),
		'end_uf' => array(
			'rule' => array('inList', array('AC','AL','AP','AM','BA','CE','DF','ES','GO','MA','MT','MS','MG','PA','PB','PR','PE','PI','RJ','RN','RS','RO','RR','SC','SP','SE','TO')),
			'required' => true,
			'message' => 'Selecione o estado de seu endereço'
		),
		'end_cep' => array(
			'rule' => 'numeric',
			'required' => true,
			'message' => 'Preencha o CEP do endereço somente com número'
		),
		'bandeira_id' => array(
			'rule' => 'notEmpty',
			'required' => true,
			'message' => 'Selecione a bandeira do pagamento.'
		),
		'numero_cartao' => array(
			'vazio' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'allowEmpty' => false,
				'message' => 'Digite o número do cartão.'
			),
			'numerico' => array(
				'rule' => 'numeric',
				'required' => true,
				'message' => 'O número do cartão deve conter apenas números.'
			),
			'invalido' => array(
				'rule' => array('cc', array('visa','maestro','electron','diners'), false, null),
				'required' => true,
				'message' => 'O número do cartão digitado é inválido.'
			)
		),
		'codigo_seguranca' => array(
			'vazio' => array(
				'rule' => 'notEmpty',
				'required' => true,
				'message' => 'Preencha o código de segurança.'
			),
			'numerico' => array(
				'rule' => 'numeric',
				'required' => true,
				'message' => 'O código de segurança deve conter apenas números.'
			)
		),
		'data_validade' => array(
			'rule' => 'notEmpty',
			'required' => true,
			'message' => 'Digite a data de validade do cartão.'
		),
		'parcelas' => array (
			'rule' => 'notEmpty',
			'required' => true,
			'message' => 'Selecione o número de parcelas'
		)
	);
}
?>