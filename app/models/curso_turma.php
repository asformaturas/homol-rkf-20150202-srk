<?php
class CursoTurma extends AppModel {
	var $name = 'CursoTurma';
	var $useTable = 'cursos_turmas';
	var $actsAs = array('Containable');
	
	var $belongsTo = array(
		'Curso', 'Turma'
	);
	var $periodos = array('matutino','vespertino','noturno','integral','outros');
	
	var $hasMany = array('FormandoProfile' => array('foreignKey' => 'curso_turma_id'));
	
	
	function getPeriodos() {
		return $this->periodos;
	}
}
?>