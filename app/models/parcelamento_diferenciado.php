<?php

class ParcelamentoDiferenciado extends AppModel {

    var $name = 'ParcelamentoDiferenciado';
    var $useTable = "parcelamentos_diferenciados";
    var $belongsTo = array(
        'Formando' => array(
            'className' => 'ViewFormandos',
            'foreignKey' => 'usuario_id'
        ),
        'Atendente' => array(
            'className' => 'Usuario',
            'foreignKey' => 'atendente_id'
        )
    );

}

?>