<?php
class CampanhasExtra extends AppModel {
	var $name = 'CampanhasExtra';
	var $useTable = 'campanhas_extras';
	
	var $belongsTo = array(
		'Campanha', 'Extra'
	);
	
	var $hasAndBelongsToMany = array(
        'CampanhasUsuarioCampanhaExtra' =>
            array(
                'className'              => 'CampanhasUsuarioCampanhasExtra',
                'joinTable'              => 'campanhas_usuarios_campanhas_extras',
                'foreignKey'             => 'campanhas_usuario_id',
                'associationForeignKey'  => 'campanhas_extra_id',
                'unique'                 => false
            )
    );
}