<?php

class TiposContrato extends AppModel {

    var $name = 'TiposContrato';
    var $useTable = 'tipos_contrato';
    
    var $hasMany = array(
        'TiposContratoAnexos' => array(
            'className' => 'TiposContratoAnexos',
            'joinTable' => 'tipos_contrato_anexos',
            'foreignKey' => 'tipos_contrato_id',
            'unique' => true,
        )
    );
    var $tipos_contrato = array(
            // '0' => 'Nenhum contrato selecionado',
            // '1' => 'Contrato Festa e Foto com Arrecadação',
         //   '2' => 'Contrato de Arrecadação com Gestão',
            // '3' => 'Contrato Foto (Exclusividade)',
            // '4' => 'Contrato Festa e Foto sem Arrecadação Exclusiva',
            // '5' => 'Contrato Festa sem Foto com Arrecadação',
         // '6' => 'Contrato de Arrecadação com Transferência',
            '7' => 'Contrato Principal',
        );
    
}

?>