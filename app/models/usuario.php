<?php

class Usuario extends AppModel {

    var $name = 'Usuario';
    var $actsAs = array('Containable', 'Multivalidatable');
    var $hasAndBelongsToMany = array(
        'Turma' => array(
            'className' => 'Turma',
            'joinTable' => 'turmas_usuarios',
            'foreignKey' => 'usuario_id',
            'associationForeignKey' => 'turma_id',
            'unique' => true
        ),
        'Campanhas' => array(
            'className' => 'Campanhas',
            'joinTable' => 'campanhas_usuarios',
            'foreignKey' => 'usuario_id',
            'associationForeignKey' => 'campanha_id',
            'unique' => false
        )
    );
    var $hasOne = array('FormandoProfile');
    var $belongsTo = array(
        'ViewFormandos' => array(
            'className' => 'ViewFormandos',
            'foreignKey' => 'id'
        )
    );
    var $hasMany = array(
        'Despesa' => array(
            'className' => 'Despesa',
            'foreignKey' => 'usuario_id',
            'unique' => false,
        ),
        'UsuarioConta' => array(
            'className' => 'UsuarioConta',
            'foreignKey' => 'usuario_id',
            'unique' => false,
        ),
        'FormandoFotoTelao' => array(
            'className' => 'FormandoFotoTelao',
            'foreignKey' => 'usuario_id',
            'unique' => false,
        ),
        'Cupom' => array(
            'className' => 'Cupom',
            'foreignKey' => 'usuario_id',
            'unique' => false
        )
    );
    
    var $grupos = array(
        'formandos' => array(
            'comissao' => 'Comissão',
            'formando' => 'Formando'
        ),
        'funcionarios' => array(
            'comercial' => 'Comercial',
            'planejamento' => 'Planejamento',
            'atendimento' => 'Atendimento',
            'financeiro' => 'Financeiro',
            'marketing' => 'Marketing',
            'criacao' => 'Criação',
            'producao' => 'Produção',
            'foto' => 'Foto',
            'video' => 'Video',
            'contabilidade' => 'Contabilidade',
            'rh' => 'RH',
            'administrativo' => 'Administrativo',
            'vendedor' => 'Vendedor'
        )
    );
    
    var $niveis = array(
        'basico' => 'Básico',
        'administrador' => 'Administrador',
        'gerencial' => 'Gerencial');
    
    var $validate = array(
        'email' => array(
            'email' => array(
                'rule' => 'email',
                'message' => 'Email inválido.',
                'required' => 'false',
                'allowEmpty' => true
            ),
            'isUnique' => array(
                'rule' => 'isUnique',
                'message' => 'Email já cadastrado.'
            )
        ),
        'nome' => array(
            'vazio' => array(
                'rule' => 'notEmpty',
                'message' => 'Preencha o nome.'
            )
        ),
        'senha' => array(
            'comparar' => array(
                'rule' => array('comparar'),
                'message' => 'Senhas diferentes.'
            ),
        ),
        'confirmar' => array(
            'alphaNumeric' => array(
                'rule' => 'alphaNumeric',
                'message' => 'Letras e números apenas.'
            ),
            'between' => array(
                'rule' => array('between', 5, 45),
                'message' => 'Senha deve possuir de 5 a 45 caracteres.'
            ),
            'required' => array(
                'rule' => array('required', true),
                'message' => 'Senha deve possuir de 5 a 45 caracteres.'
            )
        )
    );
    
    var $validationSets = array(
        'comissao_editar' => array(
            'email' => array(
                'email' => array(
                    'rule' => 'email',
                    'message' => 'Email inválido.',
                    'required' => 'false',
                    'allowEmpty' => true
                ),
                'isUnique' => array(
                    'rule' => 'isUnique',
                    'message' => 'Email já cadastrado.'
                )
            ),
            'nome' => array(
                'tamanho_minimo' => array(
                    'rule' => array('minLength', 5),
                    'required' => true,
                    'message' => 'Nome deve possuir no mínimo 5 caracteres'
                )
            )
        )
    );

    function grupos($tipo = false) {
        if ($tipo == 'todos')
            return array_merge($this->grupos['funcionarios'], $this->grupos['formandos']);
        elseif ($tipo)
            return $this->grupos[$tipo];
        else
            return $this->grupos;
    }

    function comparar($check) {
        if (Security::hash($this->data['Usuario']['confirmar'], 'sha1', true) == $check['senha']) {
            return true;
        } else {
            return false;
        }
    }
    
    function urlAcessoDeslogado($usuarioId) {
        $id = $usuarioId;
        return Configure::read('url_site') . "usuarios/entrar_deslogado/{$id}";
    }
    
    function obterAtendenteResponsavel($usuarioId) {
        $usuario = $this->read(null,$usuarioId);
        if($usuario) {
            $this->bindModel(array('hasOne' => array('TurmasUsuario')), false);
            $atendente = $this->find('first',array(
                'conditions' => array(
                    'TurmasUsuario.turma_id' => $usuario['ViewFormandos']['turma_id'],
                    'Usuario.grupo' => 'atendimento',
                    'Usuario.ativo' => 1
                )
            ));
            return $atendente;
        } else
            return false;
    }

}

?>
