<?php

class FormandoFotoTelao extends AppModel {

    var $name = 'FormandoFotoTelao';
    var $useTable = 'formandos_fotos_telao';
    var $belongsTo = array(
        'Usuario' => array(
            'className' => 'ViewFormandos',
            'foreignKey' => 'usuario_id'
        ),
    );
    var $tipos = array('adulto', 'crianca');

}

?>