<?php

class MapaMesasAgendamento extends AppModel {

    var $belongsTo = array('TurmaMapa');

    function bindTurmaMapa(){
    	$this->bindModel(
    		array(
    			'belongsTo' => array(
    				'TurmaMapa' => array(
    					'foreignKey' => false,
    					'conditions' => 'TurmaMapa.turma_id = Turma.id'
    				)
    			)
    		)
    	);
    }
    
    function mudar_status($turma_mapa_id, $status = 1){
        $this->id = $turma_mapa_id;
    	return $this->saveField('status', $status);
    }

    function cancelarAgendamento($turmaMapaId) {
        $campos = array(
            'status' => 0
        );
        $condicoes = array(
                'turma_mapa_id'=>$turmaMapaId
            );
        $this->updateAll($campos, $condicoes);
        return $this;
    }
}
?>