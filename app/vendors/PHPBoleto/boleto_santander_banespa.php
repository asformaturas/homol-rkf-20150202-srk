<?php

function imprimirBoleto($dadosboleto, $webroot) {
    include_once("include/funcoes_santander_banespa.php");
    include("include/config_santander_banespa.php");
    include("include/layout_santander_banespa.php");
}

function obterLinhaDigitavel($dadosboleto) {
    include_once("include/funcoes_santander_banespa.php");
    include("include/config_santander_banespa.php");
    return $dadosboleto["linha_digitavel"];
}

function obterDados($dadosboleto) {
    include_once("include/funcoes_santander_banespa.php");
    include("include/config_santander_banespa.php");
    return $dadosboleto;
}

?>
