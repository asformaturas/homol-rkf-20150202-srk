<?php

require_once(LIBS . 'view' . DS . 'helper.php');

class AppHelper extends Helper {
    
    var $diasDaSemana = array(
        'domingo',
        'segunda-feira',
        'terça-feira',
        'quarta-feira',
        'quinta-feira',
        'sexta-feira',
        'sábado'
    );
    
    var $meses = array(
        'janeiro',
        'fevereiro',
        'março',
        'abril',
        'maio',
        'junho',
        'julho',
        'agosto',
        'setembro',
        'outubro',
        'novembro',
        'dezembro'
    );

    function listaUF() {
        return array(
            'AC' => 'Acre',
            'AL' => 'Alagoas',
            'AP' => 'Amapá',
            'AM' => 'Amazonas',
            'BA' => 'Bahia',
            'CE' => 'Ceará',
            'DF' => 'Distrito Federal',
            'ES' => 'Espírito Santo',
            'GO' => 'Goiás',
            'MA' => 'Maranhão',
            'MT' => 'Mato Grosso',
            'MS' => 'Mato Grosso do Sul',
            'MG' => 'Minas Gerais',
            'PA' => 'Pará',
            'PB' => 'Paraíba',
            'PR' => 'Paraná',
            'PE' => 'Pernambuco',
            'PI' => 'Piauí',
            'RJ' => 'Rio de Janeiro',
            'RN' => 'Rio Grande do Norte',
            'RS' => 'Rio Grande do Sul',
            'RO' => 'Rondonia',
            'RR' => 'Roraima',
            'SC' => 'Santa Catarina',
            'SP' => 'São Paulo',
            'SE' => 'Sergipe',
            'TO' => 'Tocantins');
    }
    
    function validarData($data,$formato) {
        return $data == date($formato, strtotime($data));
    }
    
    function diaDaSemana($num) {
        return $this->diasDaSemana[$num];
    }
    
    function tipoArquivoPorMime($mimeStr, $ext = false) {
        $tipo = "undefined";
        if(preg_match('/(image)/i', $mimeStr))
            $tipo = "imagem";
        elseif(preg_match('/(video)/i', $mimeStr))
            $tipo = "video";
        elseif(preg_match('/(audio)/i', $mimeStr))
            $tipo = "audio";
        elseif(preg_match('/(pdf)/i', $mimeStr))
            $tipo = "pdf";
        elseif(preg_match('/application.*(excel|sheet)$/i', $mimeStr))
            $tipo = "excel";
        elseif(preg_match('/application.*(powerpoint|presentation)$/i', $mimeStr))
            $tipo = "powerpoint";
        elseif(preg_match('/application.*(word)$/i', $mimeStr))
            $tipo = "word";
        elseif(preg_match('/application.*(zip)$/i', $mimeStr))
            $tipo = "zip";
        elseif($mimeStr == '' && $ext == 'xls'){
            $tipo = 'excel';
        }
        return $tipo;
    }

}