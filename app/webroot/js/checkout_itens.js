var checkout_itens = {
	itens : [],
	adicionar : function(item) {
		this.itens.push(item);
	},
	remover : function(id) {
		$.each(this.itens,function(index,item) {
			if(item.item == id) {
				checkout_itens.itens.splice(index,1);
				return true;
			}
		});
		$('tr[id='+id+']').fadeOut(500,function() { $(this).remove()});
	},
	criar : function(item) {
		this.adicionar(item);
		var html = "<tr id='" + item.item + "' style='display:none'>";
		html+= "<td><input name='data[CheckoutItem][itens][" + item.item + "][titulo]' type='text' size='20' class='titulo' /></td>";
		html+= "<td><input name='data[CheckoutItem][itens][" + item.item + "][valor]' type='text' size='5' class='valor' /></td>";
		html+= "<td><input name='data[CheckoutItem][itens][" + item.item + "][quantidade]' type='text' size='5' class='quantidade' /></td>";
		html+= "<td><input name='data[CheckoutItem][itens][" + item.item + "][itens_por_pessoa]' type='text' size='5' class='itens_por_pessoa' /></td>";
                html+= "<td><div class='item'><span class='remover'>Remover</span></div></td>";
		html+= "</tr>";
		$("#container-itens table tbody").append(html);
		$('tr[id='+item.item+']').fadeIn(500);
	}
}