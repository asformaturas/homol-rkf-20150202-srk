//Namespace para modulo Mapas de Mesas
var MapaDeMesa = MapaDeMesa || {

    // Namespace para aplicação de construção
    ConstrutorLayout: {}
};

var ConstrutorLayout = MapaDeMesa.ConstrutorLayout;

/**
 * Objeto responsável por controlar a aplicação
 * @param {[String]} canvas_id ID da tag do canvas
 */
ConstrutorLayout.Palco = function(canvas_id) {
    MapaDeMesa.Palco.call(this,canvas_id);

    this.pressionandoShift = false;

    this.removerGrupoSelecionado = function() {
        if(this.objetoSelecionado == undefined)
            return this;

        var nomeGrupo = this.objetoSelecionado.layer.name;
        var index = this.listaObjetos.indexOf(this.objetoSelecionado);
        for(i in this.objetoSelecionado.objetosFilhos){
            var nome = this.objetoSelecionado.objetosFilhos[i].nome;
            this.canvas.removeLayer(nome);
        }

        if(this.objetoSelecionado.tipo == "retangulo"){
            this.canvas.removeLayer(this.objetoSelecionado.layerTexto.name);
        }

        this.ajustarObjetoSelecionado(undefined);

        if(index >= 0){
            this.listaObjetos.splice(index, 1);
        }
        this.canvas.removeLayer(nomeGrupo);
        this.canvas.removeLayerGroup(nomeGrupo);
        this.mapearGrupoMesas();
        this.canvas.drawLayers();
        return this;
    }

    this.objetoDeselecionadoHandler = function(){
        if(this.objetoSelecionado == undefined)
            return;

        var objeto = this.objetoSelecionado;
        if(objeto.tipo == "grupo_mesas"){
            var forma = objeto.retornarFormaReferencia();
            forma.strokeStyle = null;
            forma.strokeWidth = 0;
            this.canvas.setLayer(objeto.nome, forma).drawLayer(objeto.nome);
        }else if(objeto.tipo == "retangulo"){
            objeto.ajustarLarguraBorda(0).desenhar();
        }
    }

    this.objetoSelecionadoHandler = function(objeto){
        this.canvas.focus();
        var infoObjeto = "<p><strong>Tipo:</strong> "+objeto.tipo+"</p>";

        if(objeto.tipo == "grupo_mesas"){
            var forma = objeto.retornarFormaReferencia();
            forma.strokeStyle = "#000";
            forma.strokeWidth = 2;
            this.canvas.setLayer(objeto.nome, forma).drawLayer(objeto.nome);
            infoObjeto += "<p><strong>Setor:</strong> "+objeto.setor+"</p>";
            infoObjeto += "<p><strong>Linhas:</strong> "+objeto.linhas+" <strong>Colunas:</strong> "+objeto.colunas+"</p>";
        }else if(objeto.tipo == "retangulo"){
            objeto.ajustarLarguraBorda(2).desenhar();
            infoObjeto += "<p><strong>Texto:</strong> "+objeto.texto+"</p>";
        }

        $("#infoObjeto").html(infoObjeto);
    };
}

/**
 * Objeto que representa uma mesa
 * @param {[ConstrutorLayout.Palco]} palco [description]
 * @param {[String]}                 nome  [description]
 */
ConstrutorLayout.Mesa = function(palco,nome,raio) {
    MapaDeMesa.Mesa.call(this,palco,nome,raio);

     this.cores = {
        'preenchimento' : '#000',
        'contorno' : '#AA2222'
    }

    this.retornarForma = function() {
       forma = {
            type: 'arc',
            name: this.nome,
            layer: true,
            strokeStyle: this.cores['preenchimento'],
            fillStyle: this.cores['contorno'],
            strokeWidth: 1,
            x: this.x, y: this.y,
            radius: this.raio,
        };
        return forma;
    }
}

/**
 * Objeto usado para criar grupo de mesas
 * @param {[Palco]} palco  Objeto de palco
 * @param {[String]} nomeGrupo     Nome do grupo que será criado
 */
ConstrutorLayout.GrupoMesas = function(palco,nomeGrupo) {
    MapaDeMesa.GrupoMesas.call(this,palco,nomeGrupo);

    this.dragstopCallBack = function(layer){
        var diametroReferencia  = 2 * this.raioReferencia;
        
        var x = layer.x + diametroReferencia;
        var y = layer.y + diametroReferencia;
        if(this.tipoReferencia == MapaDeMesa.GRUPO_REFERENCIA_BASE){
            y = layer.y - (this.linhas * this.raioMesas * 2) - ((this.linhas - 1) * this.distanciaMesas) + this.raioReferencia;
        }

        this.posicionar(x,y);
        this.calcularReferencia();
        this.formaReferencia.x = x - diametroReferencia;
        this.formaReferencia.y = this.referencia;

        var formasMesas = this.formasMesas;

        for(var i in formasMesas){
            var layerFilho = palco.canvas.getLayer(formasMesas[i].name);
            layerFilho.x = formasMesas[i].x = layer.x + layerFilho.dx;
            layerFilho.y = formasMesas[i].y = layer.y + layerFilho.dy;
        }

        palco.mapearGrupoMesas();
    }

    this.keyPressed = function(keyCode){
        var layer = this.palco.canvas.getLayer(this.formaReferencia.name);
        var diametroReferencia  = 2 * this.raioReferencia;

        var moverX = 0;
        var moverY = 0;
        switch(keyCode){
            case 37: moverX = -1; break;
            case 38: moverY = -1; break;
            case 39: moverX = 1; break;
            case 40: moverY = 1; break;
        }
        
        var x = layer.x + diametroReferencia + moverX;
        var y = layer.y + diametroReferencia + moverY;
        if(this.tipoReferencia == MapaDeMesa.GRUPO_REFERENCIA_BASE){
            y = layer.y - (this.linhas * this.raioMesas * 2) - ((this.linhas - 1) * this.distanciaMesas) + this.raioReferencia + moverY;
        }

        this.posicionar(x,y);
        this.calcularReferencia();
        this.formaReferencia.x = x - diametroReferencia;
        this.formaReferencia.y = this.referencia;

        var formasMesas = this.formasMesas;

        for(var i in formasMesas){
            var layerFilho = palco.canvas.getLayer(formasMesas[i].name);
            layerFilho.x = formasMesas[i].x = layer.x + layerFilho.dx;
            layerFilho.y = formasMesas[i].y = layer.y + layerFilho.dy;
        }

        palco.canvas.drawLayers();
        palco.mapearGrupoMesas();
    }

    this.desenhar = function() {
        var formaMesa = {
            type: 'arc',
            layer: true,
            strokeStyle: this.corMesa.contorno,
            fillStyle: this.corMesa.preenchimento,
            radius: this.raioMesas,
            groups: [this.nome]
        };
        this.formaReferencia = {
            name: this.nome,
            type: 'arc',
            layer: true,
            fillStyle: this.corReferencia,
            radius: this.raioReferencia,
            draggable: this.arrastavel,
            drag: (function(caller) { return function() { caller.dragstopCallBack.apply(caller, arguments); } })(this),
            click: (function(caller) { return function() { caller.aoClicar.apply(caller, arguments); } })(this)
        };
        var diametroMesa        = 2 * formaMesa.radius;
        var diametroReferencia  = 2 * this.formaReferencia.radius;
        this.calcularReferencia();
        
        this.formaReferencia.x = this.x - diametroReferencia;
        this.formaReferencia.y = this.referencia;

        var forma = this.palco.canvas.getLayer(this.formaReferencia.name);
        if(forma){
            this.palco.canvas.setLayer(this.formaReferencia.name, this.formaReferencia);
        }else{
            this.palco.canvas.addLayer(this.formaReferencia);
        }
        this.palco.canvas.drawLayer(this.nome);

        this.formasMesas = [];
        for(i = 0; i < this.linhas; i++) {
            formaMesa.y = this.y + (i*diametroMesa) + (i*this.distanciaMesas);

            for (j = 0; j < this.colunas; j++){
                formaMesa.x = this.x + (j*diametroMesa) + (j*this.distanciaMesas);
                formaMesa.dx = formaMesa.x - this.formaReferencia.x;
                formaMesa.dy = formaMesa.y - this.formaReferencia.y;
                formaMesa.name = this.nome+"_"+i+"_"+j;

                var forma = this.palco.canvas.getLayer(formaMesa.name);
                if(forma){
                    this.palco.canvas.setLayer(formaMesa.name, formaMesa);
                }else{
                    this.palco.canvas.addLayer(formaMesa);                    
                }
                this.formasMesas.push($.extend(true, {}, formaMesa));
                this.palco.canvas.drawLayer(this.nome+"_"+i+"_"+j);
            }
        }

        this.layer = this.palco.canvas.getLayer(this.nome);

        return this;
    }
    /**
     * [aoClicar description]
     * @param  {[JCanvas.G]} layer [description]
     */
    this.aoClicar = function(layer) {
        this.palco.ajustarObjetoSelecionado(this);
    }
}

/**
 * Objeto usado para agrupar outro objetos
 * @param {[Palco]} palco  Objeto de palco
 * @param {[String]} nomeGrupo     Nome do grupo que será criado
 */
ConstrutorLayout.GrupoObjetos = function(palco,nomeGrupo) {
    MapaDeMesa.GrupoObjetos.call(this,palco,nomeGrupo);

    this.retornarForma = function() {
        return {
            type: 'arc',
            name: this.nome,
            layer: true,
            fillStyle: '#474',
            x: this.x, y: this.y,
            radius: 10,
            rotate: this.rotacao,
            click: (function(caller) { return function() { caller.aoClicar.apply(caller, arguments); } })(this)
        };
    }

    /**
     * [aoClicar description]
     * @param  {[JCanvas.G]} layer [description]
     */
    this.aoClicar = function(layer) {
        this.palco.ajustarObjetoSelecionado(this);
    }
}

/**
 * [MesaFactory description]
 * @param {[type]} palco [description]
 */
ConstrutorLayout.MesaFactory = function(palco,raioMesa,distancia) {
    MapaDeMesa.IMesaFactory.call(this,palco,raioMesa,distancia);

    /**
     * [criarMesa description]
     * @param  {[int]} x [description]
     * @param  {[int]} y [description]
     * @return {[MapaDeMesa.Mesa]} [description]
     */
    this.criarMesa = function(x,y) {
        nomeLayer = 'mesa'+(Math.random() * 100) + 1;
        var mesa = new ConstrutorLayout.Mesa(this.palco,nomeLayer);
        mesa.raio = this.raioMesa;
        mesa.posicionar(x,y);
        return mesa;
    };

    /**
     * [criarGrupoMesa description]
     * @return {[type]} [description]
     */
    this.criarGrupoMesa = function(objeto_grupo) {

        var nomeGrupo = objeto_grupo.nome || 'grupo_mesas_'+(new Date().getTime());

        // Cria o objeto que manipula o grupo de mesas
        grupo = new ConstrutorLayout.GrupoMesas(this.palco, nomeGrupo);
        grupo.definirNome(nomeGrupo);
        grupo.ajustarLinhas(objeto_grupo.linhas)
            .ajustarColunas(objeto_grupo.colunas)
            .ajustarRaioMesas(objeto_grupo.raioMesas)
            .ajustarDistanciaMesas(objeto_grupo.distanciaMesas)
            .ajustarTipoReferencia(objeto_grupo.tipoReferencia)
            .ajustarSetor(objeto_grupo.setor);

        if($.inArray(objeto_grupo.setor, this.palco.setores) == -1){
            this.palco.setores.push(objeto_grupo.setor);
        }
        
        grupo.ajustarCorReferencia(this.palco.setoresCores[this.palco.setores.indexOf(objeto_grupo.setor)]);
        grupo.ajustarCorMesa(this.palco.setoresMesasCores[this.palco.setores.indexOf(objeto_grupo.setor)]);

        grupo.x = 100;
        grupo.y = 100; 
        
        return grupo;
    };

    this.editarGrupoMesa = function(objeto_grupo){
        var palco = this.palco;
        if(palco.objetoSelecionado.tipo == "grupo_mesas"){
            var gruposMesas = palco.retornarObjetosPorTipo("grupo_mesas");
            $.each(gruposMesas, function(i,v){
                v.ajustarRaioMesas(objeto_grupo.raioMesas)
                    .ajustarDistanciaMesas(objeto_grupo.distanciaMesas)
                    .ajustarTipoReferencia(objeto_grupo.tipoReferencia)
                    .desenhar();
            });

            $.each(palco.objetoSelecionado.formasMesas, function(i,v){
                palco.canvas.removeLayer(v.name);
            });

            palco.objetoSelecionado.ajustarLinhas(objeto_grupo.linhas)
                .ajustarColunas(objeto_grupo.colunas)
                .ajustarRaioMesas(objeto_grupo.raioMesas)
                .ajustarDistanciaMesas(objeto_grupo.distanciaMesas)
                .ajustarTipoReferencia(objeto_grupo.tipoReferencia)
                .ajustarSetor(objeto_grupo.setor)
                .desenhar();

            palco.objetoSelecionadoHandler(palco.objetoSelecionado);
        }
    };
}

MapaDeMesa.MesaFactory = ConstrutorLayout.MesaFactory;

ConstrutorLayout.RectangleFactory = function(palco){
    MapaDeMesa.IRectangleFactory.call(this,palco);

    this.criarRetangulo = function(texto,corPreenchimento,corTexto,altura,largura,x,y,nome){
        nome = nome || 'retangulo_'+(new Date().getTime());

        var altura = altura || 50;
        var largura = largura || 100;

        var retangulo = new ConstrutorLayout.Rectangle(this.palco, nome);
        retangulo.ajustarTexto(texto).ajustarCorPreenchimento(corPreenchimento).ajustarAltura(altura).ajustarLargura(largura).ajustarCorTexto(corTexto);
        retangulo.x = x || 100;
        retangulo.y = y || 100;

        return retangulo;
    };

    this.editarRetangulo = function(texto, corPreenchimento, corTexto){
        if(this.palco.objetoSelecionado.tipo == "retangulo"){
            this.palco.objetoSelecionado.ajustarTexto(texto).ajustarCorPreenchimento(corPreenchimento).ajustarCorTexto(corTexto).desenhar();
            this.palco.ajustarObjetoSelecionado(this.palco.objetoSelecionado);
        }
    };
}

/**
 * Classe responśavel por 
 * @param {[MapaDeMesa.Palco]} palco [description]
 */
ConstrutorLayout.GerenciadorLayout = function(palco) {
    MapaDeMesa.GerenciadorLayout.call(this,palco);

    /**
     * [desserializarLayout description]
     * @param  {[String]} json [description]
     * @return {[MapaDeMesa.GerenciadorLayout]} [description]
     */
    this.desserializarLayout = function(json,factory, factoryRetangulo) {
        var factory =  factory || new MapaDeMesa.MesaFactory(this.palco);
        var factoryRetangulo =  factoryRetangulo || new ConstrutorLayout.RectangleFactory(this.palco);

        if (!json)
            return this;

        var grupos = JSON.parse( json );

        for(i in grupos) {
            g = grupos[i];

            if(g.tipo == "grupo_mesas"){
                var grupo = factory.criarGrupoMesa(g);
                grupo.posicionar(g.x,g.y);

                this.palco.adicionarObjeto(grupo);
            }else if(g.tipo == "retangulo"){
                var retangulo = factoryRetangulo.criarRetangulo(g.texto, g.corPreenchimento, g.corTexto, g.altura, g.largura, g.x, g.y, g.nome)
                this.palco.adicionarObjeto(retangulo);
            }
        }
        
        this.palco.mapearGrupoMesas();

        return this;
    }
}

ConstrutorLayout.Rectangle = function(palco,nome){
    MapaDeMesa.Rectangle.call(this,palco,nome);

    this.retornarForma = function(){
        var self = this;
        var forma = {
            type: 'rectangle',
            name: this.nome,
            layer: true,
            strokeStyle: this.corBorda,
            fillStyle: this.corPreenchimento,
            strokeWidth: this.larguraBorda,
            x: this.x, 
            y: this.y,
            width: this.largura,
            height: this.altura,
            handle: {
                type: 'arc',
                fillStyle: '#474',
                strokeStyle: '#000',
                strokeWidth: this.larguraBorda,
                radius: 5
            },
            handlemove: function(layer) {
                self.ajustarLargura(self.layer.width).ajustarAltura(self.layer.height);
                self.x = self.layer.x;
                self.y = self.layer.y;
                self.layerTexto.x = self.x;
                self.layerTexto.y = self.y;
            },
            resizeFromCenter: false
        };
        return forma;
    }

    this.retornarFormaTexto = function(){
        var formaTexto = {
            type: 'text',
            name: this.nome+"_texto",
            layer: true,
            fillStyle: this.corTexto,
            strokeWidth: 1,
            x: this.x, 
            y: this.y,
            fontSize: 14,
            fontFamily: 'Verdana, sans-serif',
            text: this.texto
        };
        return formaTexto;
    }

    this.dragstopCallBack = function(layer){
        if (this.arrastavel) {
            this.posicionar(layer.x,layer.y);
            this.layerTexto.x = this.x;
            this.layerTexto.y = this.y;
        }
    }

    this.aoClicar = function(layer) {
        this.palco.ajustarObjetoSelecionado(this);
    }

    this.desenhar = function(){
        this.forma = this.retornarForma();
        this.formaTexto = this.retornarFormaTexto();

        this.forma['draggable'] = this.arrastavel;
        this.forma['drag'] = (function(caller) { return function() { caller.dragstopCallBack.apply(caller, arguments); } })(this);
        this.forma['click'] = (function(caller) { return function() { caller.aoClicar.apply(caller, arguments); } })(this);

        if (!this.layer)
            this.palco.canvas.addLayer(this.forma);
        else
            this.palco.canvas.setLayer(this.nome, this.forma);
        this.layer = this.palco.canvas.getLayer(this.nome);

        if (!this.layerTexto)
            this.palco.canvas.addLayer(this.formaTexto);
        else
            this.palco.canvas.setLayer(this.nome+"_texto", this.formaTexto);
        this.layerTexto = this.palco.canvas.getLayer(this.nome+"_texto");

        this.palco.canvas.drawLayer(this.nome).drawLayer(this.nome+"_texto");

        return this;
    }
}