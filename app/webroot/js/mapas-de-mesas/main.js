//Namespace para modulo Mapas de Mesas
var MapaDeMesa = MapaDeMesa || {

    // Namespace para aplicação de construção
    ConstrutorLayout: {},

    // Namespace para aplicação de agendamento de mesas
    AgendadorMesas: {}
};

MapaDeMesa.GRUPO_REFERENCIA_TOPO = 1;
MapaDeMesa.GRUPO_REFERENCIA_BASE = 2;

MapaDeMesa.RAIO_MESAS = 15;
MapaDeMesa.RAIO_REFERENCIA = 10;
MapaDeMesa.DISTANCIA_MESAS = 10;

/**
 * [Grade description]
 * @param {[int]} snapX Lagura de cada unidade de grade
 * @param {[int]} snapY Altura de cada unidade de grade
 */
MapaDeMesa.Grade = function(snapX,snapY) {
    this.snapX = snapX;
    this.snapY = snapY;
}

/**
 * Objeto responsável por controlar a aplicação
 * @param {[String]} canvas_id ID da tag do canvas
 */
MapaDeMesa.Palco = function(canvas_id) {

    this.canvas_id = canvas_id;
    this.canvas = $('#'+canvas_id);

    this.grade  = new MapaDeMesa.Grade(10,10);
    this.escala = 1;
    this.listaObjetos = [];
    this.indiceListaObjetos = 0;

    this.matriz = [];

    this.objetoSelecionado = undefined;

    this.mapearReferencia = true;

    this.mapaLetras = [
        'A','B','C','D','E','F','G','H','I','J','K','L','M',
        'N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
        'AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ',
        'AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT',
        'AU','AV','AW','AX','AY','AZ'];
    this.indiceLetras;
    this.indiceNumeros;

    this.setoresCores = ['#483D8B','#008B8B','#DA70D6','#8B0000','#FF4500','#FFD700','#046C15','#6D6F04'];
    this.setoresMesasCores = ['#776DB5','#1CB2B2','#F49CF0','#B21A1A','#FC7D4E','#F7DC4B','#1F9332','#96991E'];
    this.setores = [];

    this.drawGroupLayers = function(group_name){
        var canvas = this.canvas;
        var layers = this.canvas.getLayerGroup(group_name);
        if(layers){
            $.each(layers, function(i,v){
                canvas.drawLayer(v.name);
            });
        }

        return this;
    };

    /**
     * Metodo auxiliar para retornar os grupos ordenados
     * levando em consideração suas posições
     * @return {[Array]} Lista de Grupos
     */
    this.ordenarPorPosicao = function(){
        var listaOrdenada = this.listaObjetos;
        listaOrdenada.sort(function(a,b){
            d = Math.abs(a.y-b.y);
            if(d <= 20)
                return a.x - b.x;
            return b.y - a.y;
        });

        return listaOrdenada;
    }

    this.objetoSelecionadoHandler = function(){};

    this.objetoDeselecionadoHandler = function(){};

    /**
     * Inicia procedimento de desenho da imagem de background
     * @param  {[String]} fonte URL da imagem a ser desenhada como fundo
     * @return {[MapaDeMesa.Palco]} [description]
     */
    this.desenharBackground = function(img_id,escala) {
        var img_id = img_id || 'mapa_imagem';
        var img = $('#'+img_id);

        var escala = escala || 1;

        this.canvas.addLayer({
            type: 'image',
            layer: true,
            name: 'plantaFundo',
            visible: true,
            fromCenter: false,
            scale: escala,
            data: {
                    img_id: img_id
                },
            source: img.attr('src'),
            x: 0, y: 0,
            load: (function(caller) { return function() { caller.posicionarBackground.apply(caller, arguments); } })(this),
            click: (function(caller) { return function() { caller.aoClicarBackground.apply(caller, arguments); } })(this)

        }).drawLayers();
        return this;
    };

    /**
     * Metodo de Callback que ajusta camanda de background
     * @param  {[JCanvas.Layer]} Ao ser invocado é passado o Layer criado para o fundo
     */
    this.posicionarBackground = function(layer) {

        var escala = layer.scale;
        var img = $('#'+layer.data.img_id);

        var width  = img.width();
        var height = img.height();
        var escala_prop = null;

        if (this.canvas.width() >= this.canvas.height()){
            escala_prop = this.canvas.width()/img.width();
        }
        else {
            escala_prop = this.canvas.height()/img.height();
        }

        var dx = (width/2)*escala;
        var dy = (height/2)*escala;

        layer.width = width*escala_prop;
        layer.height = height*escala_prop;
        layer.x = dx-(width/2);
        layer.y = dy-(height/2);
        
        this.canvas.attr('width',layer.width);
        this.canvas.attr('height',layer.height);

        this.canvas.moveLayer('plantaFundo',0).drawLayers();
    }

    this.aoClicarBackground = function(layer){
        this.ajustarObjetoSelecionado(undefined);
    }

    /**
     * [ajustarObjetoSelecionado description]
     * @param  {[MapaDeMesa.IObjeto]} objeto [description]
     * @return {[type]}        [description]
     */
    this.ajustarObjetoSelecionado = function(objeto) {
        if(objeto != this.objetoSelecionado){
            this.objetoDeselecionadoHandler();
            this.objetoSelecionado = objeto;
            if(typeof objeto != "undefined"){
                this.objetoSelecionadoHandler(objeto);
                $("#infoObjetoDiv").removeClass("hide");
            }else{
                $("#infoObjetoDiv").addClass("hide");
            }
        }
    }

    this.ajustarMapearReferencia = function(mapearReferencia){
        this.mapearReferencia = mapearReferencia;
        this.mapearGrupoMesas();
        if(!this.mapearReferencia){
            this.canvas.removeLayerGroup('Letras').removeLayerGroup('Numeros').drawLayers();
        }
        return this;
    }

    /**
     * Adiciona e desenha objeto de mapa de mesa ao canvas
     * @param  {[IObjeto]} objeto   Objeto a ser adiconado
     * @return {[MapaDeMesa.Palco]} [description]
     */
    this.adicionarObjeto = function(objeto) {
        this.listaObjetos.push(objeto);
        objeto.desenhar();
        return this;
    }

    /**
     * [adicionarObjetos description]
     * @param  {[Array]} listaObjetos       [description]
     * @return {[MapaDeMesa.Palco]} [description]
     */
    this.adicionarObjetos = function(listaObjetos) {
        for(i in listaObjetos)
            this.adicionarObjeto(listaObjetos[i]);
        return this;
    }

    this.retornarObjetoPeloNome = function(nome) {
        var lista = this.listaObjetos;

        for(i in lista) {
            if (lista[i].nome == nome)
                return lista[i];

            if (!lista[i].objetosFilhos)
                continue;

            filhos = lista[i].objetosFilhos;
            for(j in filhos) {
                filho = filhos[j];
                if(filho.nome == nome)
                    return filho;
            }
        }
        return false;
    }

    this.retornarObjetosPorTipo = function(tipo){
        var lista = this.listaObjetos;
        var objetos = [];

        for(var i in lista){
            if(lista[i].tipo == tipo){
                objetos.push(lista[i]);
            }
        }

        return objetos;
    }

    this.escalarMapa = function(valor) {}

    this.mapearGrupoMesas = function(){
        if(this.mapearReferencia){
            var gruposMesas = this.retornarObjetosPorTipo('grupo_mesas');
            this.mapearLetrasGrupoMesas(gruposMesas)
                .mapearNumerosGrupoMesas(gruposMesas)
                .drawGroupLayers('Letras')
                .drawGroupLayers('Numeros');
        }
    }

    this.mapearLetrasGrupoMesas = function(gruposMesas){
        var subGruposY = [];
        var tipoReferencia = null;
        this.indiceLetras = 0;
        var raio = null;

        this.canvas.removeLayerGroup('Letras');

        gruposMesas.sort(function(a,b){
            return a.y - b.y;
        });

        for(var i in gruposMesas){
            for(var x = 0; x < parseInt(gruposMesas[i].linhas); x++){
                for(var y = 0; y < parseInt(gruposMesas[i].colunas); y++){
                    var formaMesa = gruposMesas[i].retornarFormaMesaPorPosicao(x,y);
                    if(raio === null){
                        raio = (gruposMesas[i].raioMesas * 2 > 20 ? 20 : gruposMesas[i].raioMesas * 2);
                    }
                    if(tipoReferencia == null){
                        tipoReferencia = gruposMesas[i].tipoReferencia;
                    }
                    var range = Math.floor(((parseInt(gruposMesas[i].raioMesas) * 2) + parseInt(gruposMesas[i].distanciaMesas)) / 2);
                    var posY = Math.floor(Math.floor(formaMesa.y / range) * range);
                    if(typeof subGruposY[posY] == 'undefined'){
                        subGruposY[posY] = {'y': formaMesa.y, 'x': formaMesa.x - 26, 'mesas': []};
                    }else{
                        if(formaMesa.x - 26 < subGruposY[posY].x) { 
                            subGruposY[posY].x =  formaMesa.x - 26;
                        }
                    }
                    subGruposY[posY].mesas.push(formaMesa);
                }
            }
        }

        subGruposY.sort(function(a,b){
            return tipoReferencia == MapaDeMesa.GRUPO_REFERENCIA_BASE ? (b.y - a.y) : (a.y - b.y);
        });

        for(var i in subGruposY){
            this.canvas.drawRect({
                layer: true,
                fillStyle: '#000',
                x: subGruposY[i].x, y: subGruposY[i].y,
                width: raio,
                height: raio,
                cornerRadius: 8,
                groups: ['Letras'],
                name: this.mapaLetras[this.indiceLetras]+'_rect'
            });
            this.canvas.drawText({
                layer: true,
                fillStyle: '#fff',
                x: subGruposY[i].x, y: subGruposY[i].y,
                fontSize: raio * 0.7,
                fontFamily: 'Verdana, sans-serif',
                text: this.mapaLetras[this.indiceLetras],
                groups: ['Letras'],
                name: this.mapaLetras[this.indiceLetras]+'_text'
            });
            for(var j in subGruposY[i].mesas){
                subGruposY[i].mesas[j].letra = this.mapaLetras[this.indiceLetras];
            }
            this.indiceLetras++;
        }

        return this;
    }

    this.mapearNumerosGrupoMesas = function(gruposMesas){
        var subGruposX = [];
        this.indiceNumeros = 1;
        var raio = null;

        this.canvas.removeLayerGroup('Numeros');

        gruposMesas.sort(function(a,b){
            return a.x - b.x;
        });

        for(var i in gruposMesas){
            for(var x = 0; x < gruposMesas[i].linhas; x++){
                for(var y = 0; y < gruposMesas[i].colunas; y++){
                    var formaMesa = gruposMesas[i].retornarFormaMesaPorPosicao(x,y);
                    if(raio === null){
                        raio = (gruposMesas[i].raioMesas * 2 > 20 ? 20 : gruposMesas[i].raioMesas * 2);
                    }
                    var range = ((parseInt(gruposMesas[i].raioMesas) * 2) + parseInt(gruposMesas[i].distanciaMesas)) / 2;
                    var posX = Math.floor(Math.floor(formaMesa.x / range) * range);
                    if(typeof subGruposX[posX] == 'undefined'){
                        subGruposX[posX] = {'y': formaMesa.y - 26, 'x': formaMesa.x, 'mesas': []};
                    }else{
                        if(formaMesa.y - 26 < subGruposX[posX].y) { 
                            subGruposX[posX].y =  formaMesa.y - 26;
                        }
                    }
                    subGruposX[posX].mesas.push(formaMesa);
                }
            }
        }

        subGruposX.sort(function(a,b){
            return a.x - b.x;
        });

        for(var i in subGruposX){
            this.canvas.drawRect({
                layer: true,
                fillStyle: '#000',
                x: subGruposX[i].x, y: subGruposX[i].y,
                width: raio,
                height: raio,
                cornerRadius: 8,
                groups: ['Numeros'],
                name: this.indiceNumeros+'_rect'
            });
            this.canvas.drawText({
                layer: true,
                fillStyle: '#fff',
                x: subGruposX[i].x, y: subGruposX[i].y,
                fontSize: raio * 0.7,
                fontFamily: 'Verdana, sans-serif',
                text: this.indiceNumeros,
                groups: ['Numeros'],
                name: this.indiceNumeros+'_text'
            });
            for(var j in subGruposX[i].mesas){
                subGruposX[i].mesas[j].numero = this.indiceNumeros;
            }
            this.indiceNumeros++;
        }

        return this;
    }
}

/**
 * Classe abstrata base para objetos desenhados no Canvas
 * @param {[Palco]} palco Objeto gerenciador de canvas
 * @param {[String]}        nome  Nome (ID) identificador para manipulação de objeto
 */
MapaDeMesa.IObjeto = function(palco,nome) {

    this.nome = nome;
    this.x = 0;
    this.y = 0;
    this.largura = 0;
    this.altura  = 0;
    this.transladoX = 0;
    this.transladoY = 0;
    this.rotacao = 0;

    this.palco = palco;
    this.tipo = 'objeto_simples';
    this.grupos = [];

    this.layer;
    this.snapX = palco.grade.snapX;
    this.snapY = palco.grade.snapY;

    this.grudarGrade = false;
    this.arrastavel = true;
    
    /**
     * Metodo abstrato para definição de forma do objeto
     * @return {[JCanvas.Layer]} Retorna um ojeto Layer do JCanvas para ser desenhado
     */
    this.retornarForma = function(){
        return {};
    };

    // Metodo abstrato
    this.forma = this.retornarForma();

    this.dragstopCallBack = function(layer){
        if (this.arrastavel) {
            this.posicionar(layer.x,layer.y);
        }
    }

    /**
     * Desenha no canvas a forma definida no metodo "this.forma()"
     * @return {[IObjeto]} Retorna proprio objeto para possibilitar chain
     */
    this.desenhar = function() {
        this.forma = this.retornarForma();

        this.forma['name'] = this.nome;
        this.forma['layer'] = true;
        this.forma['draggable'] = this.arrastavel;
        this.forma['x'] = this.x;
        this.forma['y'] = this.y;
        this.forma['scale'] = this.escala;
        this.forma['translateX'] = this.transladoX;
        this.forma['translateY'] = this.transladoY;
        this.forma['rotate'] = this.rotacao;
        this.forma['drag'] = (function(caller) { return function() { caller.dragstopCallBack.apply(caller, arguments); } })(this);

        if (!this.layer)
            this.palco.canvas.addLayer(this.forma);
        else
            this.palco.canvas.setLayer(this.nome, this.forma);
        this.layer = this.palco.canvas.getLayer(this.nome);

        return this;
    };

    /**
     * [definirNome description]
     * @param  {[String]} nome [description]
     * @return {[type]}        [description]
     */
    this.definirNome = function(nome) {
        this.nome = nome;

        if(this.layer)
            this.layer.name = nome;
    }

    /**
     * Posiciona o objeto no canvas
     * @param  {[int]} x            Posição x no canvas
     * @param  {[int]} y            Posição y no canvas
     * @return {[IObjeto]} Retorna proprio objeto para possibilitar chain
     */
    this.posicionar = function(x,y) {
        x = x || this.x;
        y = y || this.y;

        this.x = this.grudarGrade ? Math.floor(x/this.snapX)*this.snapX : x;
        this.y = this.grudarGrade ? Math.floor(y/this.snapY)*this.snapY : y;

        this.x = (this.x < 0) ? 0 : this.x;
        this.y = (this.y < 0) ? 0 : this.y;

        // if(this.layer) {
        //     this.layer.x = this.x;
        //     this.layer.y = this.y;
        // }
        return this;
    }

    this.rotacionar = function(angulo,x,y) {
        this.rotacao = angulo;
        this.transladoX = x || this.transladoX;
        this.transladoY = y || this.transladoY;

        return this;
    }

    /**
     * [ajustarArraste description]
     * @param  {[Boolean]} flag Determina se objeto deve ou não ser arrastavel
     * @return {[type]}      [description]
     */
    this.ajustarArraste = function(flag) {
        this.palco.canvas.setLayer(this.nome,{
            draggable: flag
        });
        return this;
    }
}

MapaDeMesa.GrupoMesas = function(palco,nomeGrupo){
    MapaDeMesa.IObjeto.call(this,palco,nomeGrupo);

    this.tipo            = 'grupo_mesas';
    this.linhas          = 0;
    this.colunas         = 0;
    this.raioMesas       = MapaDeMesa.RAIO_MESAS;
    this.distanciaMesas  = MapaDeMesa.DISTANCIA_MESAS;

    this.tipoReferencia  = MapaDeMesa.GRUPO_REFERENCIA_TOPO;
    this.referencia      = 0;
    this.raioReferencia  = MapaDeMesa.RAIO_REFERENCIA;
    this.formaReferencia = null;
    this.formasMesas     = [];

    this.setor           = "A";

    this.corMesa         = {
        'preenchimento':'#AA2222',
        'contorno':'#000'
    };
    this.corReferencia   = '#474';

    this.calcularReferencia = function() {
        this.referencia = this.y - (this.raioReferencia * 2);
        if(this.tipoReferencia == MapaDeMesa.GRUPO_REFERENCIA_BASE){
            this.referencia = this.y + (this.linhas * this.raioMesas * 2) + ((this.linhas - 1) * this.distanciaMesas) - this.raioReferencia;
        }
        return this;
    }

    this.calcularAltura = function(){
        this.altura = (this.linhas * this.raioMesas * 2) + ((this.linhas - 1) * this.distanciaMesas);
        return this;
    }

    this.calcularLargura = function(){
        this.largura = (this.colunas * this.raioMesas * 2) + ((this.colunas - 1) * this.distanciaMesas);
        return this;
    }

    /**
     * Ajusta o número de linhas da matriz de mesas
     * @param  {int} linhas             [description]
     * @return {MapaDeMesa.GrupoMesas}  [description]
     */
    this.ajustarLinhas = function(linhas){
        this.linhas = linhas;
        this.calcularAltura();
        return this;
    }
    /**
     * Ajusta o número de colunas da matriz de mesas
     * @param  {int} colunas           [description]
     * @return {MapaDeMesa.GrupoMesas} [description]
     */
    this.ajustarColunas = function(colunas){
        this.colunas = colunas;
        this.calcularLargura();
        return this;
    }
    /**
     * Ajusta o tipo de referencia (Onde vai ficar a referencia
     * para calulo da matriz de posição e definição de id de mesa)
     * @param  int tipoReferencia      [description]
     * @return MapaDeMesa.GrupoObjetos [description]
     */
    this.ajustarTipoReferencia = function(tipoReferencia){
        this.tipoReferencia = tipoReferencia;
        return this;
    }

    this.ajustarRaioMesas = function(raioMesas){
        this.raioMesas = raioMesas || this.raioMesas;
        return this;
    }

    this.ajustarDistanciaMesas = function(distanciaMesas){
        this.distanciaMesas = distanciaMesas || this.distanciaMesas;
        return this;
    }

    this.ajustarCorMesa = function(preenchimento, contorno){
        this.corMesa         = {
            'preenchimento': typeof preenchimento != 'undefined' ? preenchimento : '#AA2222',
            'contorno': typeof contorno != 'undefined' ? contorno : '#000',
        };
        return this;
    }

    this.ajustarCorReferencia = function(cor){
        this.corReferencia = typeof cor != 'undefined' ? cor : '#474';
        return this;
    }

    this.retornarFormaMesaPorPosicao = function(linha, coluna){
        if(linha <= this.linhas && coluna <= this.colunas){
            var x = this.x + (coluna * (this.raioMesas * 2)) + (coluna * this.distanciaMesas);
            var y = this.y + (linha * (this.raioMesas * 2)) + (linha * this.distanciaMesas);
            var mesa_nome = this.nome+"_"+linha+"_"+coluna;
            var formaMesa = $.grep(this.formasMesas, function(v,i){
                return (v.name == mesa_nome);
            });
            return (formaMesa.length > 0 ? formaMesa[0] : false);
        }
        return false;
    }

    this.retornarFormaReferencia = function(){
        return this.formaReferencia;
    }

    this.ajustarSetor = function(setor){
        this.setor = setor;
        return this;
    }
}

/**
 * Objeto usado para agrupar outro objetos
 * @param {[Palco]} palco  Objeto de palco
 * @param {[String]} nomeGrupo     Nome do grupo que será criado
 */
MapaDeMesa.GrupoObjetos = function(palco,nomeGrupo) {

    MapaDeMesa.IObjeto.call(this,palco,nomeGrupo);

    this.tipo = 'grupo';
    this.objetosFilhos = [];

    this.tipoReferencia = MapaDeMesa.GRUPO_REFERENCIA_BASE;
    this.referencia     = 0;

    this.oldDragstopCallBack = this.dragstopCallBack;
    this.dragstopCallBack = function(layer){

        this.palco.ajustarObjetoSelecionado(this);
        this.posicionar(layer.x,layer.y);

        for(j in this.grupos) {
            grupoNome = this.grupos[j];
            grupo = this.palco.retornarObjetoPeloNome(grupoNome);
            for(i in grupo.objetosFilhos) {
                filho = grupo.objetosFilhos[i];
                if(this.nome == filho.nome)
                    continue;
                filho.posicionar(filho.x+layer.dx,filho.y+layer.dy);
            }
        }
    }

    this.retornarForma = function() {
        return {
            type: 'arc',
            name: this.nome,
            layer: true,
            fillStyle: '#474',
            x: this.x, y: this.y,
            radius: 10,
            rotate: this.rotacao,
            click: (function(caller) { return function() { caller.aoClicar.apply(caller, arguments); } })(this)
        };
    }

    /**
     * [aoClicar description]
     * @param  {[JCanvas.G]} layer [description]
     */
    this.aoClicar = function(layer) {
        this.palco.ajustarObjetoSelecionado(this);
    }

    this.adicionarObjeto = function(objeto) {
        objeto.grupos[objeto.grupos.length] = this.nome;

        var ox = objeto.x;
        var oy = objeto.y;


        objeto.x = this.x;
        objeto.y = this.y;

        objeto.transladoX = ox+this.transladoX;
        objeto.transladoY = oy+this.transladoY;

        objeto.rotacao = this.rotacao;
        objeto.scale = this.escala;

        objeto.dragstopCallBack = this.dragstopCallBack;
        this.objetosFilhos[this.objetosFilhos.length] = objeto;
    }

    this.antigoRotacionar = this.rotacionar;
    this.rotacionar = function(angulo,x,y) {

        paiOldTx = this.transladoX;
        paiOldTy = this.transladoY;

        this.antigoRotacionar(angulo,x,y);

        for(i in this.objetosFilhos) {
            filho = this.objetosFilhos[i];

            tx = filho.transladoX + (paiOldTx - this.transladoX);
            ty = filho.transladoY + (paiOldTy - this.transladoY);

            filho.rotacionar(angulo);
        }
        return this;
    }

    this.antigoPosicionar = this.posicionar
    this.posicionar = function(x,y) {
        this.antigoPosicionar(x,y);

        for(i in this.objetosFilhos) {
            filho = this.objetosFilhos[i];
            filho.posicionar(x,y);
        }
        return this;
    }

    this.antigoDesenhar = this.desenhar;
    this.desenhar = function() {
        this.antigoDesenhar();
        if(this.objetosFilhos.length>0)
            for(i in this.objetosFilhos) {
                this.objetosFilhos[i].desenhar();
            }
        return this;
    }
    /**
     * Ajusta o tipo de referencia (Onde vai ficar a referencia
     * para calulo da matriz de posição e definição de id de mesa)
     * @param  int tipoReferencia      [description]
     * @return MapaDeMesa.GrupoObjetos [description]
     */
    this.ajustarTipoReferencia = function(tipoReferencia){
        this.tipoReferencia = tipoReferencia;
        return this;
    }
}

/**
 * Objeto que representa uma mesa
 * @param {[type]}   palco [description]
 * @param {[String]} nome  [description]
 */
MapaDeMesa.Mesa = function(palco,nome,raio) {

    MapaDeMesa.IObjeto.call(this,palco,nome);
    this.forma = this.retornarForma();

    this.raio = raio || 25;

    this.tipo = 'mesa';
    this.layer = undefined;

    this.cores = {
        'preenchimento' : '#000',
        'contorno' : '#AA2222'
    }

    this.retornarForma = function() {
       forma = {
            type: 'arc',
            name: this.nome,
            layer: true,
            strokeStyle: this.cores['preenchimento'],
            fillStyle: this.cores['contorno'],
            strokeWidth: 3,
            x: this.x, y: this.y,
            radius: this.raio,
        };
        return forma;
    }
}

/**
 * [MesaFactory description]
 * @param {[type]} palco [description]
 */
MapaDeMesa.IMesaFactory = function(palco,raioMesa,distancia) {

    this.palco = palco;

    this.raioMesa = raioMesa || 18;

    this.distX = (typeof distancia == 'undefined' ? 26 : distancia);
    this.distY = (typeof distancia == 'undefined' ? 26 : distancia);

    /**
     * [criarMesa description]
     * @param  {[int]} x [description]
     * @param  {[int]} y [description]
     * @return {[MapaDeMesa.Mesa]} [description]
     */
    this.criarMesa = function(x,y) {};

    /**
     * [criarGrupoMesa description]
     * @return {[type]} [description]
     */
    this.criarGrupoMesa = function(linhas,colunas,nomeGrupo) {}
}

MapaDeMesa.IRectangleFactory = function(palco){

    this.palco = palco;

    this.criarRetangulo = function(x,y,nome){};
}

MapaDeMesa.Rectangle = function(palco,nome){
    MapaDeMesa.IObjeto.call(this,palco,nome);

    this.tipo = 'retangulo';
    this.corPreenchimento = "#aaa";
    this.corBorda = "#000";
    this.corTexto = "#000";
    this.larguraBorda = 0;

    this.texto = "Teste";
    this.formaTexto = this.retornarFormaTexto;

    this.layerTexto;

    this.retornarForma = function(){
        return {};
    }

    this.retornarFormaTexto = function(){
        return {};
    }

    this.desenhar = function(){ }

    this.ajustarAltura = function(altura){
        this.altura = altura;
        return this;
    }

    this.ajustarLargura = function(largura){
        this.largura = largura;
        return this;
    }

    this.ajustarCorPreenchimento = function(cor){
        this.corPreenchimento = cor;
        return this;
    }

    this.ajustarCorBorda = function(cor){
        this.corBorda = cor;
        return this;
    }

    this.ajustarCorTexto = function(cor){
        this.corTexto = cor;
        return this;
    }

    this.ajustarLarguraBorda = function(larguraBorda){
        this.larguraBorda = larguraBorda;
        return this;
    }

    this.ajustarTexto = function(texto){
        this.texto = texto;
        return this;
    }
}


/**
 * Classe responśavel por 
 * @param {[MapaDeMesa.Palco]} palco [description]
 */
MapaDeMesa.GerenciadorLayout = function(palco) {

    this.palco = palco;
    this.grupos = [];

    this.objetoInfo = function(objeto) {
        var info = {};
        if(objeto.tipo == "grupo_mesas"){
            info = {
                'nome': objeto.nome,
                'tipo': objeto.tipo,
                'x': objeto.x, 'y': objeto.y,
                'tipoReferencia': objeto.tipoReferencia,
                'raioMesas': objeto.raioMesas,
                'distanciaMesas': objeto.distanciaMesas,
                'raioReferencia': objeto.raioReferencia,
                'linhas': objeto.linhas,
                'colunas': objeto.colunas,
                'setor': objeto.setor
            }
        }else if(objeto.tipo == "retangulo"){
            info = {
                'nome': objeto.nome,
                'tipo': objeto.tipo,
                'x': objeto.x, 'y': objeto.y,
                'altura': objeto.altura,
                'largura': objeto.largura,
                'texto': objeto.texto,
                'corPreenchimento': objeto.corPreenchimento,
                'corTexto': objeto.corTexto,
                'corBorda': objeto.corBorda,
            }
        }

        return info;
    }

    /**
     * [serializarLayout description]
     * @return {[String]} Retorna as informações serializadas
     */
    this.serializarLayout = function() {
        this.grupos = [];
        listaGrupos = this.palco.listaObjetos;
        for(i in listaGrupos) {
            g = listaGrupos[i];

            grupo = this.objetoInfo(g);
            this.grupos[this.grupos.length] = grupo;
        }
        return JSON.stringify(this.grupos);
    }

    /**
     * [desserializarLayout description]
     * @param  {[String]} json [description]
     * @return {[MapaDeMesa.GerenciadorLayout]} [description]
     */
    this.desserializarLayout = function(json,factory) {
        var factory =  factory || new MapaDeMesa.MesaFactory(this.palco);

        if (!json)
            return this;

        var grupos = JSON.parse( json );
        for(i in grupos) {
            g = grupos[i];

            if(g.tipo == "grupo_mesas"){
                grupo = factory.criarGrupoMesa(g.x,g.y,g.linhas,g.colunas,g.raioMesas,g.distanciaMesas,g.tipoReferencia,g.nome);
                grupo.posicionar(g.x,g.y);
                grupo.transladoX = g.transladoX;
                grupo.transladoY = g.transladoY;
                grupo.rotacionar(g.rotacao);

                this.palco.adicionarObjeto(grupo);
            }
        }
        return this;
    }
}