(function($) {
    $(document).ready(function() {
        
        var context = ko.contextFor($("#content-body")[0]);
        var months = ["Janeiro", "Fevereiro", "Mar&ccedil;o", "Abril", "Maio",
            "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
        var days = ['Domingo', 'Segunda', "Terca", 'Quarta', 'Quinta', 'Sexta', 'Sabado'];
        var daysShort = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'];
        var date = new Date();
        var url = context.$data.page() + "/";
         
        $("#calendar").bind('carregado',function() {
            $('#calendar').fullCalendar({
                header: {
                    left: false,
                    center: false,
                    right: false
                },
                viewRender: function(view,element) {
                    listar(view.start.getTime()/1000,view.end.getTime()/1000);
                },
                viewDisplay: function(view) {
                    $("#current-month").html(months[$.fullCalendar.formatDate($('#calendar').fullCalendar('getDate'), 'M') - 1]);
                    $("#current-year").html($.fullCalendar.formatDate($('#calendar').fullCalendar('getDate'), 'yyyy'));
                },
                allDayText: "Dia Inteiro",
                dayNames: days,
                dayNamesShort: daysShort,
                editable: false,
                allDaySlot : false,
                eventClick: function(calEvent, jsEvent, view) {
                    carregar(calEvent.usuario_id,calEvent.data);
                }
            });
            
            setTimeout(function() {
                context.$data.loaded(true);
                $(".carregando-eventos").fadeOut(500);
                
            },500);
            
            $('#custom-prev').click(function() {
                $('#calendar').fullCalendar('prev');
            });

            $('#custom-next').click(function() {
                $('#calendar').fullCalendar('next');
            });
            
        });
        
        function listar(inicio,fim) {

            $(".carregando-eventos").fadeIn(500,function() {
                $.ajax({
                    url : url,
                    type : 'POST',
                    dataType : 'json',
                    data : {
                        data : {
                            inicio : inicio,
                            fim : fim
                        }
                    },
                    success : function(response) {
                        $("#calendar").fullCalendar('removeEvents');
                        $.each(response.lista, function(i, item) {
                            item.start = new Date(item.data*1000);
                            item.end = new Date(item.data*1000);
                            $("#calendar").fullCalendar('renderEvent',item,false);
                        });
                        $(".carregando-eventos").fadeOut(500);
                        if($('#content-body').height() < $('.calendar-container').height()){
                            atualizaAltura($('#content-body').height()+($('.calendar-container').height() - $('#content-body').height()));
                        }else{
                            atualizaAltura($('.calendar-container').height()+50);
                        }
                    }
                });
            });
        }
        
        function carregar(atendenteId,data) {
            bootbox.dialog('Carregando',[{
                label: 'Cancelar'
            }],{
                remote: url + atendenteId + "/" + data
            });
        }
        
    });
})(jQuery);