$(document).ready(function() {
    
    ko.bindingHandlers.stopBinding = {
        init: function() {
            return { controlsDescendantBindings: true };
        }
    };
    
    ko.bindingHandlers.fadeVisible = {
        init: function(element, valueAccessor) {
            var value = valueAccessor();
            $(element).toggle(ko.utils.unwrapObservable(value));
        },
        update: function(element, valueAccessor) {
            var value = valueAccessor();
            ko.utils.unwrapObservable(value) ? $(element).fadeIn() : $(element).fadeOut();
        }
    };
    
    var ContentView = function() {

        var self = this;
        self.page = ko.observable(false).extend({notify: 'always'});
        self.minHeight = 0;
        self.avisos = ko.observableArray();
        self.avisoSelecionado = ko.observable(false);
        self.cache;

        self.setMinHeight = function() {
            height = parseInt($(window).height()) -
                    (65 + parseInt($("#conteudo").offset().top));
            if(height < $("#content-home").height())
                height = $("#content-home").height();
            self.minHeight = height;
            $("#conteudo,#content-body").css('min-height', self.minHeight);
            //$(".util.active .data").css('min-height', self.minHeight-134);
        }

        self.init = function() {
            self.setMinHeight();
            self.cache = !(/msie/.test(navigator.userAgent.toLowerCase()));
        }

        self.getLoaderHeight = function() {
            if ($("#content-body").height() < self.minHeight)
                return self.minHeight;
            else
                return $("#content-body").height();
        }

        self.loaded = ko.observable(false).extend({notify: 'always'});

        self.reload = function() {
            if (self.page())
                self.page(self.page());
        }

        self.load = ko.computed(function() {
            if (self.page()) {
                self.loaded(false);
                $("#content-loading").css('min-height', self.getLoaderHeight())
                        .stop().fadeIn(500, function() {
                    $("#content-body").fadeOut(500, function() {
                        $.ajax({
                            url: self.page(),
                            async : true,
                            cache:self.cache,
                            dataType : 'html',
                            success: function(data) {
                                $("#content-body").html(data).fadeIn(500, function() {
                                    ko.applyBindings(self, $("#content-body").get(0));
                                    self.loaded(true);
                                });
                            },
                            error: function(xhr,status,textStatus) {
                                self.hideHomeButton();
                            }
                        });
                    })
                })
            }
        }, self);
        
        self.loadThis = function(elem,event) {
            if($(event.target).attr('href') != undefined)
                self.page($(event.target).attr('href'));
        };

        self.showLoading = function(callback) {
            $("#content-loading").css('min-height', self.getLoaderHeight())
                    .stop().fadeIn(500, function() {
                $("#content-body").fadeOut(500, function() {
                    $("#conteudo").animate({
                        height: self.minHeight
                    }, 600, function() {
                        if (typeof callback == "function")
                            callback();
                    });
                });
            });
        }

        self.updateContentHeight = ko.computed(function() {
            if (self.loaded()) {
                var curHeight = $("#conteudo").height();
                var newHeight = $("#content-body").height();
                if (newHeight > self.minHeight)
                    var height = newHeight;
                else
                    var height = curHeight > self.minHeight ? self.minHeight : 0;
                if (height > 0)
                    $("#conteudo").animate({
                        height: height
                    }, 600);
            }
        }, self);

        self.showHomeButton = ko.computed(function() {
            if (self.loaded() && self.page() && !$('#homeButton').is(":visible")) {
                do {
                    if($('#content-body').is(":visible"))
                        $("#header-container").stop().animate({
                            'margin-left': 50
                        }, 500, function() {
                            $('#homeButton').stop(true, true).fadeIn(500);
                        });
                } while(!$('#content-body').is(":visible"));
            }
        }, self);
        
        self.hideHomeButton = function() {
            $('#homeButton').stop().fadeOut(500, function() {
                $("#header-container").animate({
                    'margin-left': '0'
                }, 600, function() {
                    $("#content-body").stop().fadeOut(500, function() {
                        self.page(false);
                        $("#content-loading").fadeOut(500, function() {
                            $("#conteudo").animate({
                                height: self.minHeight
                            }, 500);
                        });
                    });
                });
            });
        }
        
        self.carregarAvisos = function() {
            $.ajax({
                url: "/avisos/carregar",
                dataType : 'json',
                success: function(data) {
                    self.avisos(data.avisos);
                }
            });
        };
        
        self.selecionarAviso = function() {
            self.avisoSelecionado(this);
        };
        
        self.exibirAviso = ko.computed(function() {
            if(self.avisoSelecionado()) {
                bootbox.dialog('Carregando',[{
                    label: 'Cancelar',
                    callback: function() {
                        self.avisoSelecionado(false);
                    }
                }],{
                    remote: self.avisoSelecionado().link
                });
            } else {
                bootbox.hideAll();
            }
        }, self);
        
        self.removerAviso = function(id,tipo) {
            $.each(self.avisos(),function(i,aviso) {
                if(aviso.id == id && aviso.tipo == tipo) {
                    self.avisos.remove(aviso);
                    return false;
                }
            });
        };
    }
    var contentView = new ContentView();
    contentView.init();
    setTimeout(function() {
        contentView.carregarAvisos();
    },2000);
    ko.applyBindings(contentView, $("#content-container").get(0));
    ko.applyBindings(contentView, $("header").get(0));
    ko.applyBindings(contentView, $("#menu-topo").get(0));
    if($("#redirecionar-para-pagina").length > 0) {
        contentView.page($("#redirecionar-para-pagina").val());
    }
});