$(document).ready(function() {
    
    var RenegociacaoModel = function() {
        var self = this;
        self.dados;
        self.pagamentos = ko.observableArray();
        self.planos = ko.observableArray();
        self.erroPlanos = ko.observable(false);
        self.plano = ko.observable();
        self.meses = ko.observableArray();
        self.mes = ko.observable();
        self.parcelamentos = ko.observableArray();
        self.parcelamento = ko.observable();
        self.dia = ko.observable(0);
        self.total = ko.observable(0);
        self.parcelas = ko.observableArray();
        self.somaPagamentos = ko.observable(0);
        self.saldo = ko.observable(0);
        self.planoDiferenciado = ko.observable(false);
        self.textoSaldo = ko.observable('');
        self.contentContext = ko.contextFor($("#content-body")[0]);
        
        self.formatarValor = function(valor) {
            if(valor != 0)
                return number_format(Math.abs(valor),2,',','.');
            else
                return '-';
        };
        
        self.iniciar = function() {
            var url = "/area_financeira/planos/"+$("#usuario-id").val();
            $.getJSON(url).done(function(response) {
                if(Object.keys(response.planos).length) {
                    self.dados = response.planos;
                    var planos = [];
                    $.each(response.planos,function(c,v) {
                        planos.push(c);
                    });
                    self.planos(planos);
                    $('#planos').val('').selectpicker('refresh');
                } else {
                    self.erroPlanos(true);
                }
            }).fail(function() {
                alert('Erro ao conectar servidor');
            });
        };
        
        self.planoAlterado = ko.computed(function () {
            if(self.plano() != undefined) {
                var meses = [];
                $.each(self.dados[self.plano()],function(c,v) {
                    var d = c.split("-");
                    d.pop();
                    meses.push({
                        value : c,
                        text : d.reverse().join("/")
                    });
                });
                self.meses(meses);
                $('#meses').val('').selectpicker('refresh');
            }
        },self);
        
        self.mesAlterado = ko.computed(function () {
            if(self.mes() != undefined) {
                var parcelamentos = [];
                $.each(self.dados[self.plano()][self.mes()],function(c,v) {
                    parcelamentos.push({
                        value : c,
                        text : v.Parcelamento.parcelas + "x"
                    });
                });
                self.parcelamentos(parcelamentos);
                $('#parcelamentos').val('').selectpicker('refresh');
            }
        },self);
        
        self.parcelamentoAlterado = ko.computed(function () {
            if(self.parcelamento() != undefined)
                self.total(self.formatarValor(self.dados[self.plano()][self.mes()][self.parcelamento()].Parcelamento['valor']));
            self.parcelas([]);
        },self);
        
        self.diaAlterado = ko.computed(function () {
            if(self.dia() != undefined)
                self.parcelas([]);
        },self);
        
        self.totalAlterado = ko.computed(function () {
            self.planoDiferenciado(false);
            if(self.total() != undefined) {
                if(self.total() != 0) {
                    var valorTotal = stringMoneyToNumber(self.total()),
                        valorPlano = self.dados[self.plano()][self.mes()][self.parcelamento()].Parcelamento['valor'],
                        valorPago = parseFloat($("#total-pago").val()),
                        saldo = valorTotal-valorPago;
                    self.planoDiferenciado(valorPlano != valorTotal);
                    self.saldo(parseFloat(saldo.toFixed(2)));
                    self.parcelas([]);
                }
            }
        },self);
        
        self.adicionarParcela = function() {
            var p = self.parcelas();
            p.push({
                valor : ko.observable(0),
                    data_vencimento : ko.observable("")
            });
            self.parcelas(p);
            self.planoDiferenciado(true);
            self.recalcularParcelas();
            self.contentContext.$data.loaded(true);
        };
        
        self.gerarParcelas = function() {
            var data = self.mes().split('-'),
                valorPlano = self.dados[self.plano()][self.mes()][self.parcelamento()].Parcelamento['valor'],
                valorTotal = stringMoneyToNumber(self.total());
            self.planoDiferenciado(valorPlano != valorTotal);
            data.pop();
            data.push(parseInt(self.dia())+1);
            self.parcelas([]);
            var primeiroMes = data[1]-1,
                dia = data[2],
                ano = data[0],
                qtdeParcelas = self.dados[self.plano()][self.mes()][self.parcelamento()].Parcelamento['parcelas'],
                valorTotal = stringMoneyToNumber(self.total()),
                valorPago = parseFloat($("#total-pago").val()),
                saldo = valorTotal-valorPago,
                valorParcela = parseFloat(parseFloat(saldo/qtdeParcelas).toFixed(2)),
                somaParcelas = 0;
            self.saldo(parseFloat(saldo.toFixed(2)));
            self.parcelar(self.dados[self.plano()][self.mes()][self.parcelamento()].Parcelamento['parcelas'],
                saldo,valorParcela,primeiroMes,dia,ano);
        };
        
        self.recalcularParcelas = function() {
            var data = self.mes().split('-'),
                valorPlano = self.dados[self.plano()][self.mes()][self.parcelamento()].Parcelamento['valor'],
                qtdeParcelas = self.parcelas().length,
                valorTotal = stringMoneyToNumber(self.total());
            self.planoDiferenciado(valorPlano != valorTotal || qtdeParcelas != self.dados[self.plano()][self.mes()][self.parcelamento()].Parcelamento['parcelas']);
            data.pop();
            data.push(parseInt(self.dia())+1);
            var primeiroMes = data[1]-1,
                dia = data[2],
                ano = data[0],
                valorTotal = stringMoneyToNumber(self.total()),
                valorPago = parseFloat($("#total-pago").val()),
                saldo = valorTotal-valorPago,
                valorParcela = parseFloat(parseFloat(saldo/qtdeParcelas).toFixed(2)),
                somaParcelas = 0;
            self.saldo(parseFloat(saldo.toFixed(2)));
            self.parcelar(qtdeParcelas,saldo,valorParcela,primeiroMes,dia,ano);
        };
        
        self.parcelar = function(qtdeParcelas,saldo,valorParcela,mes,dia,ano) {
            var somaParcelas = 0,
                p = [];
            for(var a = 1; a <= qtdeParcelas; a++) {
                if(a == qtdeParcelas)
                    valorParcela = saldo-somaParcelas;
                somaParcelas+= valorParcela;
                var d = new Date(ano, mes, dia, 0, 0, 0, 0);
                mes++;
                p.push({
                    valor : ko.observable(Math.abs(valorParcela).toFixed(2)),
                    data_vencimento : ko.observable(str_pad(d.getDate(),2,'0','STR_PAD_LEFT') +
                            "/" + str_pad((d.getMonth()+1),2,'0','STR_PAD_LEFT') + "/" + d.getFullYear())
                });
            }
            self.parcelas(p);
            self.contentContext.$data.loaded(true);
        };
        
        self.parcelasAlteradas = ko.computed(function () {
            var soma = 0;
            $.each(self.parcelas(),function(i,p) {
                if(p.valor() != "") {
                    soma+= p.valor().indexOf(',') >= 0 ? parseFloat(p.valor().replace('.','').replace(',','.')) : parseFloat(p.valor().replace(',','.'));
                }
            });
            soma = parseFloat(soma.toFixed(2));
            self.somaPagamentos(soma);
        },self);
        
        self.camposPagamento = function(e) {
            $(e).find('.datepicker').datetimepicker({
                format: 'dd/mm/yyyy',
                autoclose : true,
                language : 'pt-BR',
                minView : 2
            });
            $(e).find('.valor').setMask();
        };
        
        self.enviarParcelas = function() {
            if(self.planoDiferenciado() && $("#justificativa").val() == "") {
                bootbox.alert("Digite a justificativa para plano diferenciado",function() {
                    $("#justificativa").focus();
                });
            } else {
                var dataVencimento = true;
                $.each(self.parcelas(),function(i,p) {
                    if(p.data_vencimento() == "")
                        dataVencimento = false;
                });
                if(!dataVencimento) {
                    bootbox.alert("Complete a data de vencimento de todas as parcelas");
                } else {
                    var dados = {
                        parcelamento : self.dados[self.plano()][self.mes()][self.parcelamento()].Parcelamento['id'],
                        //valor : self.dados[self.plano()][self.mes()][self.parcelamento()].Parcelamento['valor'],
                        valor : stringMoneyToNumber(self.total()),
                        despesas : [],
                        diferenciado : self.planoDiferenciado() ? 1 : 0,
                        justificativa : $("#justificativa").val(),
                        valor_pago : parseFloat($("#total-pago").val())
                    };
                    $.each(self.parcelas(),function(i,p) {
                        dados.despesas.push({
                            valor : p.valor().indexOf(',') >= 0 ? parseFloat(p.valor().replace('.','').replace(',','.')) : parseFloat(p.valor().replace(',','.')),
                            data : p.data_vencimento().split('/').reverse().join('-')
                        });
                    });
                    $.ajax({
                        url : 'super/area_financeira/inserir_despesas/' + $("#usuario-id").val(),
                        type : 'POST',
                        data : {
                            data : dados
                        },
                        dataType : 'json',
                        success : function(response) {
                            console.log(response);
                        },
                        error : function() {
                            console.log('error');
                        },
                        complete : function() {
                            //self.contentContext.$data.page('/'+self.contentContext.$data.page().split('/')[0]+'/formandos/listar');
                            $('.flash').fadeIn('slow');
                        }
                    });
                    return false;
                }
            }
        };
    };
    var renegociacao = new RenegociacaoModel();
    renegociacao.iniciar();
    ko.applyBindings(renegociacao, document.getElementById('renegociacao'));
});