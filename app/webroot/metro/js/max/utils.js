function atualizaAltura(height) {
    var minHeight = parseInt($("#conteudo").css('min-height'));
    height = height < minHeight ? minHeight : height;
    var diferenca = Math.abs($("#conteudo").height() - height);
    if (diferenca > 10)
        $("#conteudo").animate({
            height: height
        }, 600);
}

function buscarCep(cep) {
    var url = "http://cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep=" + cep;
    var error = true;
    var endereco = false;
    $.getScript(url, function() {
        if (resultadoCEP["resultado"] && resultadoCEP["tipo_logradouro"] != "") {
            error = false;
            endereco = resultadoCEP;
        }
        $("#busca-cep").trigger({
            type: 'resposta-busca',
            error: error,
            endereco: endereco
        });
    });
}

function getBase64Image(img) {
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);
    var dataURL = canvas.toDataURL("image/png");
    return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
}

function number_format(number, decimals, dec_point, thousands_sep) {
    var n = number, prec = decimals;
    n = !isFinite(+n) ? 0 : +n;
    prec = !isFinite(+prec) ? 0 : Math.abs(prec);
    var sep = (typeof thousands_sep == "undefined") ? ',' : thousands_sep;
    var dec = (typeof dec_point == "undefined") ? '.' : dec_point;

    var s = (prec > 0) ? n.toFixed(prec) : Math.round(n).toFixed(prec); //fix for IE parseFloat(0.55).toFixed(0) = 0;

    var abs = Math.abs(n).toFixed(prec);
    var _, i;

    if (abs >= 1000) {
        _ = abs.split(/\D/);
        i = _[0].length % 3 || 3;

        _[0] = s.slice(0, i + (n < 0)) +
                _[0].slice(i).replace(/(\d{3})/g, sep + '$1');

        s = _.join(dec);
    } else {
        s = s.replace('.', dec);
    }

    return s;
}

function ucfirst(str) {
    return str.substr(0, 1).toUpperCase() + str.substr(1)
}

function stringMoneyToNumber(stringMoney) {
    number = stringMoney.replace('R$', '');
    number = number.replace('.', '');
    number = number.replace(',', '.');
    return number;
}

function stringToFloat(string, decimals) {
    number = string;
    number = parseFloat(number).toFixed(decimals);
    number = parseFloat(number);
    return number;
}

function decode(s) {
    return $("<div/>").html(s).text();
}

function str_pad(input, pad_length, pad_string, pad_type) {
    
    var half = '',
            pad_to_go;

    var str_pad_repeater = function(s, len) {
        var collect = '',
                i;

        while (collect.length < len) {
            collect += s;
        }
        collect = collect.substr(0, len);

        return collect;
    };

    input += '';
    pad_string = pad_string !== undefined ? pad_string : ' ';

    if (pad_type !== 'STR_PAD_LEFT' && pad_type !== 'STR_PAD_RIGHT' && pad_type !== 'STR_PAD_BOTH') {
        pad_type = 'STR_PAD_RIGHT';
    }
    if ((pad_to_go = pad_length - input.length) > 0) {
        if (pad_type === 'STR_PAD_LEFT') {
            input = str_pad_repeater(pad_string, pad_to_go) + input;
        } else if (pad_type === 'STR_PAD_RIGHT') {
            input = input + str_pad_repeater(pad_string, pad_to_go);
        } else if (pad_type === 'STR_PAD_BOTH') {
            half = str_pad_repeater(pad_string, Math.ceil(pad_to_go / 2));
            input = half + input + half;
            input = input.substr(0, pad_length);
        }
    }

    return input;
}

function camelToUnderscore(string){
    return string.replace(/([A-Z])/g, function($1){return "_"+$1.toLowerCase();});
};