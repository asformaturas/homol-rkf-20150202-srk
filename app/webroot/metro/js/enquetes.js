function exibirEnquete(enquete) {
    var context = ko.contextFor($("#content-body")[0]);
    var ultimaPergunta = enquete['EnquetePergunta'].length-1;
    var div = $("<div>");
    div.append($("<h3>",{
        html : enquete['Enquete']['texto'] + "<br />"
    }));
    bootbox.dialog(div.html(),[{
        label: 'Responder',
        'class': 'bg-color-green',
        callback : function() {
            var modal = $(".bootbox.modal"),
                modalBody = modal.find('.modal-body'),
                confirmar = $("<a/>",{
                    class : 'button bg-color-orange',
                    href : 'javascript:',
                    text : 'Próxima'
                }),
                finalizar = $("<a/>",{
                    class : 'button bg-color-green',
                    style : 'display:none',
                    href : 'javascript:',
                    text : 'Finalizar'
                }),
                respostas = [];
            modal.find('.modal-footer').html([confirmar,finalizar]);
            modal.on('click','.alternativa',function() {
                if(enquete['EnquetePergunta'][modal.attr('data-pergunta')]['quantidade_escolhas'] > 1) {
                    if(modal.find('.alternativa.selecionada').length >=
                            enquete['EnquetePergunta'][modal.attr('data-pergunta')]['quantidade_escolhas'] &&
                            !$(this).hasClass("selecionada"))
                        return;
                    $(this).toggleClass("selecionada").toggleClass("fg-color-blue");
                } else {
                    modal.find('.alternativa').removeClass('selecionada').removeClass('fg-color-blue');
                    $(this).addClass('selecionada').addClass('fg-color-blue');
                }
                if(modal.find('.alternativa.selecionada').length > 0)
                    modal.find('.modal-footer').css('opacity',1);
                else
                    modal.find('.modal-footer').css('opacity',0);
            });
            confirmar.click(function(e) {
                e.preventDefault();
                var pergunta = enquete['EnquetePergunta'][modal.attr('data-pergunta')],
                    alternativa = pergunta['EnqueteAlternativa'][modalBody
                        .find('.alternativa.selecionada').attr('data-alternativa')];
                modalBody.find('.alternativa.selecionada').each(function() {
                    respostas.push(pergunta['EnqueteAlternativa'][$(this).attr('data-alternativa')]['id']);
                });
                if(modal.attr('data-pergunta') == ultimaPergunta) {
                    exibirObservacoes();
                } else {
                    var proxima = parseInt(modal.attr('data-pergunta'))+1;
                    if(alternativa['ir_para'])
                        $.each(enquete['EnquetePergunta'],function(i,p) {
                            if(p['id'] == alternativa['ir_para']) {
                                proxima = i;
                                return false;
                            }
                        });
                    exibirPergunta(proxima);
                }
                return false;
            });
            finalizar.click(function(e) {
                e.preventDefault();
                var url = '/enquetes/responder/' + enquete['Enquete']['id'],
                    data = {
                        respostas : respostas,
                        observacoes : $("#observacoes").val()
                    };
                modal.find('.modal-body,.modal-footer').css('opacity',0);
                $.ajax({
                    url : url,
                    type : 'POST',
                    dataType : 'json',
                    data : {
                        data : data
                    },
                    success : function(response) {
                        if(response.erro)
                            modalBody.html($("<h3>",{
                                class : 'fg-color-red',
                                html : "Erro ao responder enquete<br />Tente novamente mais tarde<br />"
                            }));
                        else
                            modalBody.html($("<h3>",{
                                class : 'fg-color-green',
                                html : "Obrigado por responder a enquete.<br />"
                            }));
                    },
                    error : function() {
                        modalBody.html($("<h3>",{
                            class : 'fg-color-red',
                            html : "Erro ao enviar dados<br />Tente novamente mais tarde<br />"
                        }));
                    },
                    complete : function() {
                        modalBody.css('opacity',1);
                        context.$data.reload();
                        setTimeout(function() {
                            bootbox.hideAll();
                        },2000);
                    }
                });
                return false;
            });
            function exibirPergunta(index) {
                var pergunta = enquete['EnquetePergunta'][index];
                modal.find('.modal-body,.modal-footer').css('opacity',0);
                modal.attr('data-pergunta',index);
                var h3 = $("<h3>",{class : 'fg-color-darken'});
                if(enquete['Enquete']['texto_pergunta'])
                    h3.append(enquete['Enquete']['texto_pergunta'] + "<br /><br />");
                h3.append(pergunta['texto'] + "<br /><br />");
                modalBody.html(h3);
                $.each(pergunta['EnqueteAlternativa'],function(i,a) {
                    var texto = (i+1) + ") " + a['texto'];
                    if(!isNaN(a['texto']))
                        texto = a['texto'] + (a['texto'] > 0 ? " Ponto" + (a['texto'] > 1 ? "s" : "") : "");
                    modalBody.append($("<p>",{
                        'data-alternativa' : i,
                        class : 'alternativa',
                        style : 'font-size:16px; cursor:pointer',
                        html : texto
                    }));
                });
                modalBody.css('opacity',1);
            }
            function exibirObservacoes() {
                modal.find('.modal-body,.modal-footer').css('opacity',0);
                confirmar.hide();
                finalizar.show();
                var h3 = $("<h3>",{
                    class : 'fg-color-darken',
                    html : "Caso queira nos dizer qualquer coisa, use o espaço abaixo<br />"
                });
                modalBody.html(h3);
                var textarea = $("<textarea>",{
                    style : 'resize: none;',
                    rows : 6,
                    class : 'form-control input-block-level',
                    id : 'observacoes'
                });
                modalBody.append(textarea);
                modal.find('.modal-body,.modal-footer').css('opacity',1);
            }
            exibirPergunta(0);
            return false;
        }
    },{
        label: 'Responder depois',
        'class': 'bg-color-orange depois',
        callback : function() {
            var url = '/enquetes/adiar/' + enquete['Enquete']['id'];
            $.getJSON(url).always(function() {
                bootbox.hideAll();
            });
        }
    },{
        label: 'Não quero responder',
        class: 'bg-color-red rejeitar',
        callback : function() {
            var url = '/enquetes/rejeitar/' + enquete['Enquete']['id'];
            $.getJSON(url).always(function() {
                bootbox.hideAll();
            });
        }
    }]);
}